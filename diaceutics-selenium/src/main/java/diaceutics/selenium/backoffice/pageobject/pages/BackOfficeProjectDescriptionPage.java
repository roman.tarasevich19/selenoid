package diaceutics.selenium.backoffice.pageobject.pages;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;
import java.util.List;

public class BackOfficeProjectDescriptionPage extends BaseMarketplaceForm {

    private static final String TEXT_CONTENT_ROW = "//div[@class='note-editable']//li";
    private static final String DESCRIPTION_IFRAME_ID = "id_description_iframe";
    private static final String CONTROL_BAR_BUTTON = "//input[@value='%s']";

    public BackOfficeProjectDescriptionPage(){
        super(By.xpath("//h1[text()='Change project']"), "Back office Project description page");
    }

    public String getTextContentFromDescriptionArea(){
        AqualityServices.getBrowser().getDriver().switchTo().frame(DESCRIPTION_IFRAME_ID);
        StringBuilder stringBuilder = new StringBuilder();
        List<ILabel> textContentRows = getElementFactory().findElements(By.xpath(TEXT_CONTENT_ROW), ElementType.LABEL);
        textContentRows.forEach(e -> stringBuilder.append(e.getText()));
        return stringBuilder.toString();
    }

    public void updateTextContentInLastRow(String text){
        AqualityServices.getBrowser().getDriver().switchTo().frame(DESCRIPTION_IFRAME_ID);
        List<ITextBox> lastRow = getElementFactory().findElements(By.xpath(TEXT_CONTENT_ROW),  ElementType.TEXTBOX);
        lastRow.get(lastRow.size()-1).clearAndType(text);
    }

    public void clickOnControlBarButton(String buttonName){
        AqualityServices.getBrowser().getDriver().switchTo().defaultContent();
        IButton button = getElementFactory().getButton(By.xpath(String.format(CONTROL_BAR_BUTTON, buttonName)), "Control bar button");
        button.clickAndWait();
    }

}
