package diaceutics.selenium.backoffice.pageobject.pages;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class BackOfficeProjectsPage extends BaseMarketplaceForm {

    public BackOfficeProjectsPage(){
        super(By.xpath("//h1[text()='Select project to change']"), "Select Project page");
    }
}
