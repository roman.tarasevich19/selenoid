package diaceutics.selenium.backoffice.pageobject.pages;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class BackOfficeMainPage extends BaseMarketplaceForm {

    public BackOfficeMainPage() {
        super(By.id("site-wrapper"), "Back office main page");
    }

}
