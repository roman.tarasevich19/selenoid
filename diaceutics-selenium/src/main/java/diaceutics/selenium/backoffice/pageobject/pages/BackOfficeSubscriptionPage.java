package diaceutics.selenium.backoffice.pageobject.pages;

import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class BackOfficeSubscriptionPage extends BaseMarketplaceForm {

    private static final String SUBSCRIPTION_ITEM = "//div[contains(@class,'app-subscriptions')]//a[contains(text(),'%s')]";

    public BackOfficeSubscriptionPage() {
        super(By.xpath("//h1[text()='Subscriptions administration']"), "Back office subscription page");
    }

    @Override
    public void clickByLink(String linkName){
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(SUBSCRIPTION_ITEM, linkName)), linkName);

        link.clickAndWait();
    }
}
