package diaceutics.selenium.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Lab extends BaseModel {
    private String name;
    private String labType;
    private String ownership;
    private List<Address> addresses = new ArrayList<>();
    private List<Assay> assays = new ArrayList<>();
    private List<Volume> volumes = new ArrayList<>();

    public void addLocation(Address address) {
        addresses.add(address);
    }

    public void addAssay(Assay assay) {
        assays.add(assay);
    }

}
