package diaceutics.selenium.models;

import diaceutics.selenium.enums.fieldvalues.Classifications;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.utilities.DateUtil;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class Assay extends BaseModel {
    private String assayName;
    private String assayDescription;
    private String inHouseOrSendOut;
    private String sendOutLab = "Not applicable";
    private String testingPurpose = FieldValues.UNSPECIFIED.toString();
    private String detects = FieldValues.UNSPECIFIED.toString();
    private String specimensTested = FieldValues.UNSPECIFIED.toString();
    private String method = FieldValues.UNSPECIFIED.toString();
    private String methodDescription = FieldValues.UNSPECIFIED.toString();
    private String turnAroundTime = FieldValues.UNSPECIFIED.toString();
    private String ontology = FieldValues.UNSPECIFIED.toString();
    private String sensitivity = FieldValues.UNSPECIFIED.toString();
    private String scoringMethod = FieldValues.UNSPECIFIED.toString();
    private String regulatoryStatus;
    private String reportFormat = FieldValues.UNSPECIFIED.toString();
    private String classification;
    private String classifications;
    private String fda510KApprovedKit;
    private String fdaPmaApprovedKit;
    private String ivdCe;
    private String ruoIuo;
    private String biomarkerName;
    private String commercialAssays = FieldValues.UNSPECIFIED.toString();
    private String status = "ACTIVE";
    private String yearFrom = "2019";
    private String monthFrom = "January";
    private String yearTo;
    private String monthTo;
    private String statusFrom;
    private String statusTo;
    private List<Biomarker> biomarkers = new ArrayList<>();
    private List<StatusModel> statuses;

    public void addBiomarker(Biomarker biomarker) {
        biomarkers.add(biomarker);
    }

    public String addClassifications() {
        if (getClassification() != null) {
            if (getClassification().equals(Classifications.COMMERCIAL_ASSAY.getName())) {
                List<String> classificationList = Arrays.stream(Classifications.values())
                        .filter(c -> Boolean.parseBoolean(getReflectionFieldValue(c.getModelField())))
                        .map(Classifications::getName).collect(Collectors.toList());

                return String.join(", ", classificationList);
            }

            return Classifications.LABORATORY_DEVELOPED_TEST.getName();
        }
        return FieldValues.UNSPECIFIED.toString();
    }

    public void setStatuses() {
        status = getStatus().toUpperCase(Locale.ROOT);
        statusFrom = String.join(" ", DateUtil.convertMonth(monthFrom), yearFrom);
        if (yearTo != null) {
            statusTo = String.join(" ", DateUtil.convertMonth(monthTo), yearTo);
        } else statusTo = "";
    }
}
