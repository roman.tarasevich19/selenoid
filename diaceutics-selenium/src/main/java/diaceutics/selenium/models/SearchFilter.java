package diaceutics.selenium.models;

import diaceutics.selenium.utilities.DateUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class SearchFilter extends BaseModel {
    private String project;
    private String country;
    private String diseases;
    private String criteriaType;
    private String method;
    private String biomarkerName;
    private String yearFrom;
    private String monthFrom;
    private String yearTo;
    private String monthTo;
    private String dateRange;

    public void setDateRange() {
        this.dateRange = String.format(
                "%s %s - %s %s", DateUtil.convertMonth(monthFrom), yearFrom, DateUtil.convertMonth(monthTo), yearTo);
    }

}
