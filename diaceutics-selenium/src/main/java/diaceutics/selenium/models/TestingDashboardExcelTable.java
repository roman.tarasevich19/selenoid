package diaceutics.selenium.models;

import diaceutics.selenium.enums.charts.UsStates;
import diaceutics.selenium.utilities.ExelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.ArrayList;
import java.util.List;

public class TestingDashboardExcelTable {

    private final String fileName;

    public TestingDashboardExcelTable(String fileName) {
        this.fileName = fileName;
    }


    public List<UsState> getAllListStates() {
        List<UsState> usStatesList = new ArrayList<>();
        XSSFSheet sheet = ExelUtil.getXSSFSheet(fileName, 0);
        for (Row row : sheet) {
            UsState usState = new UsState();
            for (UsStates state : UsStates.values()) {
                Cell stateTitle = row.getCell(0);
                Cell stateValue = row.getCell(1);
                String title = ExelUtil.getStringValueFromCell(stateTitle);
                String value = ExelUtil.getStringValueFromCell(stateValue);
                usState.setReflectionFieldValue(state.getModelField(), title);
                usState.setReflectionFieldValue("value", value);
            }
            usStatesList.add(usState);
        }
        usStatesList.remove(0);

        return usStatesList;
    }
}
