package diaceutics.selenium.models;

import diaceutics.selenium.enums.pagefields.physicianmapping.AGGridPhysicianSummaryFormFields;
import diaceutics.selenium.pageobject.forms.physicianmapping.AGGridPhysicianSummaryForm;
import diaceutics.selenium.utilities.ExelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.ArrayList;
import java.util.List;

public class PhysicianExelTable {
    private final String fileName;

    public PhysicianExelTable(String fileName) {
        this.fileName = fileName;
    }

    public List<Physician> getAllListPhysician() {
        List<Physician> physicianList = new ArrayList<>();
        XSSFSheet sheet = ExelUtil.getXSSFSheet(fileName, 0);
        for (Row row : sheet) {
            Physician physician = new Physician();
            for (AGGridPhysicianSummaryFormFields constant : AGGridPhysicianSummaryFormFields.values()) {
                Cell cell = row.getCell(constant.getIndex());
                String value = ExelUtil.getStringValueFromCell(cell);
                physician.setReflectionFieldValue(constant.getModelField(), value);
            }
            physicianList.add(physician);
        }
        physicianList.remove(0);

        return physicianList;
    }

    public List<Physician> getAllListPhysicianWithoutColumn(String columnsToExclude) {
        List<Physician> physicianList = new ArrayList<>();
        AGGridPhysicianSummaryForm grid = new AGGridPhysicianSummaryForm();
        List<AGGridPhysicianSummaryFormFields> allColumns = grid.getColumnsToExclude(columnsToExclude);
        XSSFSheet sheet = ExelUtil.getXSSFSheet(fileName, 0);
        for (Row row : sheet) {
            int i = 0;
            Physician physician = new Physician();
            for (AGGridPhysicianSummaryFormFields constant : allColumns) {
                constant.setIndex(i);
                Cell cell = row.getCell(constant.getIndex());
                i++;
                String value = ExelUtil.getStringValueFromCell(cell);
                physician.setReflectionFieldValue(constant.getModelField(), value);
            }
            physicianList.add(physician);
        }
        physicianList.remove(0);
        return physicianList;
    }
}
