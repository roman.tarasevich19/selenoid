package diaceutics.selenium.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Address extends BaseModel {
    private String locationName;
    private String addressOne;
    private String addressTwo;
    private String cityTown;
    private String region;
    private String country;
    private String postalCode;
}
