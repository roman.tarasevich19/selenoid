package diaceutics.selenium.models;

import lombok.Data;

@Data
public class Physician extends BaseModel {
    private String physicianNPI;
    private String organizationAffiliation;
    private String physicianFirstName;
    private String physicianLastName;
    private String physicianPrimarySpecialty;
    private String physicianAddressLine1;
    private String physicianCity;
    private String physicianState;
    private String physicianZip;
    private String physicianPhoneNumber;
    private String diseasePatientCountQuintile;
    private String diseasePatientCount;
    private String diseaseTestingRatePercentage;
    private String biomarkerPatientCount;
    private String biomarkerTestingRatePercentage;
    private String preferredLabName;
    private String preferredLabClassification;
    private String preferredLabAddressLine1;
    private String preferredLabCity;
    private String preferredLabState;
    private String preferredLabZip;
    private String readablePreferredLabOffersBiomarkerTesting;
}
