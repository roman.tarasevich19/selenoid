package diaceutics.selenium.models;

import lombok.Data;

@Data
public class BiomarkerVolume extends BaseModel implements Comparable<BiomarkerVolume> {
    private String biomarkerName;
    private String volume;

    public BiomarkerVolume(String biomarkerName, String volume) {
        this.biomarkerName = biomarkerName;
        this.volume = volume;
    }

    public BiomarkerVolume() {
    }

    @Override
    public int compareTo(BiomarkerVolume biomarkerVolume) {
        return this.biomarkerName.compareTo(biomarkerVolume.getBiomarkerName());
    }
}
