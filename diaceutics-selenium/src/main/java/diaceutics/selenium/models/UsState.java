package diaceutics.selenium.models;

import lombok.Data;

@Data
public class UsState extends BaseModel {
    private String title;
    private String value;
}
