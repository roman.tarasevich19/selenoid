package diaceutics.selenium.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Biomarker extends BaseModel {
    private String biomarkerName;
    private String variants;
    private List<String> listOfVariants = new ArrayList<>();

    public void addVariant(String variant) {
        listOfVariants.add(variant);
    }

    public void removeVariant(String variant) {
        this.listOfVariants.remove(variant);
    }

    public void removeAllVariants() {
        this.listOfVariants.clear();
    }
}
