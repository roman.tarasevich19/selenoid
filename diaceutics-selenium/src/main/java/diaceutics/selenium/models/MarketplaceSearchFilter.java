package diaceutics.selenium.models;

import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class MarketplaceSearchFilter extends BaseModel {
    private String location = "";
    private String type = "Collaboration";
    private String sponsored = String.valueOf(Boolean.FALSE);
    private String testReview = String.valueOf(Boolean.FALSE);
    private String offered = String.valueOf(Boolean.FALSE);
    private String requested = String.valueOf(Boolean.FALSE);
    private String testingIconGroupOne = String.valueOf(Boolean.FALSE);
    private String testingIconGroupTwo = String.valueOf(Boolean.FALSE);
    private String laboratory = String.valueOf(Boolean.FALSE);
    private String diagnostics = String.valueOf(Boolean.FALSE);
    private String pharmaceutical = String.valueOf(Boolean.FALSE);
    private String serviceProvider = String.valueOf(Boolean.FALSE);
    private String diaceutics = String.valueOf(Boolean.FALSE);
    private String other = String.valueOf(Boolean.FALSE);
    private String keywords = "";
}
