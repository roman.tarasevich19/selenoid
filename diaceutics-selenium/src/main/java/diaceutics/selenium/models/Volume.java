package diaceutics.selenium.models;

import diaceutics.selenium.enums.date.DateFormat;
import diaceutics.selenium.utilities.DateUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Volume extends BaseModel {
    private String timePeriod;
    private String year;
    private String quarter;
    private String disease;
    private String diseaseVolume;
    private String biomarkerDetails;
    private String updated;
    private List<BiomarkerVolume> biomarkerVolumes = new ArrayList<>();

    public Volume() {
        this.updated = DateUtil.getCurrentDate(DateFormat.YYYY_MM_DD.toString());
    }

    public String getTimePeriod() {
        return String.format("%s-%s", year, quarter);
    }

    public void addBiomarkerVolumes(BiomarkerVolume biomarkerVolume) {
        biomarkerVolumes.add(biomarkerVolume);
    }
}
