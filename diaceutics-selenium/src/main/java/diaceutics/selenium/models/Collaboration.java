package diaceutics.selenium.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Collaboration extends BaseModel {
    //Description
    private String title;
    private String description;
    private String additionalInformation;
    private String category;
    private String status;
    //media
    private String youtubeUrl;
    //location
    private String country;
    private String city;
    private String zip;
    private String number;
    private String route;
    private String additionalLocationInformation;
    private String iAcceptTheTermsAndConditions;

    public Collaboration() {
    }

    public Collaboration(Collaboration collaboration) {
        this.title = collaboration.title;
        this.description = collaboration.description;
        this.additionalInformation = collaboration.additionalInformation;
        this.category = collaboration.category;
        this.status = collaboration.status;
        this.youtubeUrl = collaboration.youtubeUrl;
        this.country = collaboration.country;
        this.city = collaboration.city;
        this.zip = collaboration.zip;
        this.number = collaboration.number;
        this.route = collaboration.route;
        this.additionalLocationInformation = collaboration.additionalLocationInformation;
        this.iAcceptTheTermsAndConditions = collaboration.iAcceptTheTermsAndConditions;
    }
}
