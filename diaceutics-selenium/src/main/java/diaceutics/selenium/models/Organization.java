package diaceutics.selenium.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Organization extends BaseModel {

    private String name;
    private String location;
    private String organizationType;
}
