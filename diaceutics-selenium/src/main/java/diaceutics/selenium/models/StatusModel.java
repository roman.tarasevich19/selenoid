package diaceutics.selenium.models;

import lombok.Data;

@Data
public class StatusModel extends BaseModel implements Comparable<StatusModel> {
    private String status;
    private String active;
    private String inactive;
    private String updated;

    @Override
    public int compareTo(StatusModel status) {
        return this.status.compareTo(status.getStatus());
    }
}
