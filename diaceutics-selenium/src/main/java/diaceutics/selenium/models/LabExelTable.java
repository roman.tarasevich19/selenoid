package diaceutics.selenium.models;

import diaceutics.selenium.enums.pagefields.labmapping.AGGridLabSummaryFormAssayDetailsFields;
import diaceutics.selenium.utilities.ExelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.ArrayList;
import java.util.List;

public class LabExelTable {
    private final String fileName;

    public LabExelTable(String fileName) {
        this.fileName = fileName;
    }

    public List<Assay> getListAssay() {
        List<Assay> assayList = new ArrayList<>();
        XSSFSheet sheet = ExelUtil.getXSSFSheet(fileName, 0);
        for (Row row : sheet) {
            Assay assay = new Assay();
            for (AGGridLabSummaryFormAssayDetailsFields constant : AGGridLabSummaryFormAssayDetailsFields.values()) {
                String value = ExelUtil.getStringValueFromCell(row.getCell(constant.getIndex()));
                assay.setReflectionFieldValue(constant.getModelField(), value);
            }
            assayList.add(assay);
        }
        return assayList;
    }

    public List<Lab> getListLab() {
        List<Lab> labList = new ArrayList<>();
        List<Integer> index = new ArrayList<>();
        List<Assay> listAssay = getListAssay();
        for (int i = 0; i < listAssay.size(); i++) {
            if (listAssay.get(i).getAssayName().contains("->")) {
                index.add(i);
                Lab lab = new Lab();
                lab.setName(listAssay.get(i).getAssayName().substring(3));
                labList.add(lab);
            }
        }
        index.add(listAssay.size());

        for (int i = 0; i < index.size() - 1; i++) {
            List<Assay> listAssayForLab = listAssay.subList(index.get(i) + 1, index.get(i + 1));
            Lab lab = labList.get(i);
            lab.setAssays(listAssayForLab);
        }
        return labList;
    }

}
