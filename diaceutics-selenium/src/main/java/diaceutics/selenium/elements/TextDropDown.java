package diaceutics.selenium.elements;

import aquality.selenium.core.elements.ElementState;
import aquality.selenium.elements.Element;
import aquality.selenium.elements.interfaces.IElement;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import org.openqa.selenium.By;

public class TextDropDown extends Element implements IElement {

    private final String optionTemplate;

    public TextDropDown(By locator, String name, ElementState state, String optionTemplate) {
        super(locator, name, state);
        this.optionTemplate = optionTemplate;
    }

    protected String getElementType() {
        return this.getLocalizationManager().getLocalizedMessage("loc.textDropdown");
    }

    public String selectByText(String value) {
        ITextBox textDropDown = getElementFactory().getTextBox(getLocator(), getName());
        textDropDown.clearAndType(value);
        ILabel optionLabel = getElementFactory().getLabel(By.xpath(optionTemplate), getName());
        value = optionLabel.getText();
        optionLabel.clickAndWait();
        return value;
    }
}
