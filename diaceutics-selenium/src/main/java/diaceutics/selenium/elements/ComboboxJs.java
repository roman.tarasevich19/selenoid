package diaceutics.selenium.elements;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.core.elements.ElementState;
import aquality.selenium.elements.Element;
import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IElement;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ComboboxJs extends Element implements IElement {

    private static final String OPTION_TEMPLATE = "//div[(@role='option') and contains(@id,'-%s')]";
    private static final String OPTION_TEXT_TEMPLATE = "//div[@role='option']//span[normalize-space(text())='%s']";
    private static final String SELECTED_TEXT_TEMPLATE = "//div[@aria-selected='true']//span";
    private static final String VISIBLE_OPTION_TEMPLATE = "//span[contains(@class,'ng-option')]";
    private static final int MAX_OPTION_COUNT = 1500;

    public ComboboxJs(By locator, String name, ElementState state) {
        super(locator, name, state);
    }

    protected String getElementType() {
        return this.getLocalizationManager().getLocalizedMessage("loc.combobox");
    }

    public void selectByText(String value) {
        this.clickAndWait();
        ILink optionLink = getElementFactory().getLink(By.xpath(String.format(OPTION_TEXT_TEMPLATE, value)), value);
        int count = 0;
        while (!optionLink.state().isDisplayed() && count < MAX_OPTION_COUNT) {
            Actions act = new Actions(AqualityServices.getBrowser().getDriver());
            act.sendKeys(Keys.ARROW_DOWN).perform();
            count++;
        }
        optionLink.getJsActions().clickAndWait();
    }

    public String getRandomValue() {
        List<String> options = getStringListVisibleOptions();
        int randomIndex = new Random().nextInt(options.size());
        return options.get(randomIndex);
    }

    public List<String> getStringListVisibleOptions() {
        this.clickAndWait();
        ILink optionLink = getElementFactory().getLink(By.xpath(VISIBLE_OPTION_TEMPLATE), "option link");
        optionLink.state().waitForDisplayed();
        List<String> options = getElementFactory().findElements(By.xpath(VISIBLE_OPTION_TEMPLATE), ElementType.LINK)
                .stream().map(IElement::getText).collect(Collectors.toList());

        this.clickAndWait();
        return options;
    }

    public List<String> getStringListAllOptions() {
        this.clickAndWait();
        List<String> options = new ArrayList<>();
        int optionIndex = 0;
        while (!getElementFactory().findElements(By.xpath(String.format(OPTION_TEMPLATE, optionIndex)), ElementType.LINK).isEmpty()) {
            ILink optionLink = getElementFactory().getLink(By.xpath(String.format(OPTION_TEMPLATE, optionIndex)), "option");
            options.add(optionLink.getText());
            optionIndex++;
            Actions act = new Actions(AqualityServices.getBrowser().getDriver());
            act.sendKeys(Keys.ARROW_DOWN).perform();
        }

        this.clickAndWait();
        return options;
    }

    public String getSelectedText() {
        this.clickAndWait();
        ILabel selectedTextLabel = getElementFactory().getLabel(By.xpath(SELECTED_TEXT_TEMPLATE), "Selected Text");
        String selectedText = selectedTextLabel.getText();
        this.clickAndWait();
        return selectedText;
    }

}
