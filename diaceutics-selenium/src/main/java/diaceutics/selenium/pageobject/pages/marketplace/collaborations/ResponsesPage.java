package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ResponsesPage extends BaseMarketplaceForm {

    private static final String COUNT_RESPONSES_LABEL_LOCATOR = "//h2[contains(@class,'text-weight-bold')]";
    private static final String RESPONSE_LABEL_TEMPLATE = "//div[contains(@class,'card-body')][.//h2[contains(text(),'%s')]]";
    private static final String SHOW_RESPONSE_BUTTON_TEMPLATE = RESPONSE_LABEL_TEMPLATE + "//a[.='Show']";
    private static final String RESPONSE_STATUS_LABEL_TEMPLATE = RESPONSE_LABEL_TEMPLATE + "//div[@data-toggle='tooltip']";

    private final IButton removeResponseBtn = getElementFactory().getButton(By.id("btn-remove-asker-interest"),
            "Remove response button");

    private final ILabel countResponsesLabel = getElementFactory().getLabel(By.xpath(COUNT_RESPONSES_LABEL_LOCATOR),
            "Count responses label");

    public ResponsesPage() {
        super(By.xpath("//h2[contains(text(),'Responses')]"), "Responses page");
    }

    public void removeAllResponses() {
        int numberOfResponses = getNumberOfResponsesFromHead();
        for (int i = 0; i < numberOfResponses; i++) {
            int countBeforeDeleting = getNumberOfResponsesFromHead();
            removeResponseBtn.clickAndWait();
            AqualityServices.getConditionalWait().waitFor(ExpectedConditions.textToBePresentInElementLocated(
                    By.xpath(COUNT_RESPONSES_LABEL_LOCATOR), String.format("%s Responses", countBeforeDeleting - 1)));
        }
    }

    public int getNumberOfResponsesFromHead() {
        return Integer.parseInt(RegExUtil.getNumbersFromString(countResponsesLabel.getText()));
    }

    public boolean isResponseFromCollaborationDisplayed(String collaborationName) {
        ILabel responseLabel = getElementFactory().getLabel(
                By.xpath(String.format(RESPONSE_LABEL_TEMPLATE, collaborationName)),
                "Response label");

        return responseLabel.state().waitForDisplayed();
    }

    public void clickShowButtonForCollaboration(String title) {
        IButton showBtn = getElementFactory().getButton(
                By.xpath(String.format(SHOW_RESPONSE_BUTTON_TEMPLATE, title)),
                "Show button");

        showBtn.clickAndWait();
    }

    public String getStatusForResponseFromCollaboration(String title) {
        ILabel responseStatusLabel = getElementFactory().getLabel(
                By.xpath(String.format(RESPONSE_STATUS_LABEL_TEMPLATE, title)),
                "Response Status label");

        return RegExUtil.getLastWordFromString(responseStatusLabel.getText());
    }
}
