package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class ResponseShowPage extends BaseMarketplaceForm {

    private static final String BUTTON_SHORTLIST_TEMPLATE = "//button[contains(text(),'%s')]";
    private final ILabel countryLabel = getElementFactory().getLabel(
            By.xpath("//div[contains(@class,'card-body')]//address"), "Country label");

    private final ILabel emailLabel = getElementFactory().getLabel(By.xpath("//dd//a"), "Email label");

    public ResponseShowPage() {
        super(By.xpath("//div[@class='container'][.//span[.='Responses']]"), "Response show page");
    }

    public String getCountry() {
        return countryLabel.getText();
    }

    public String getUserEmail() {
        return emailLabel.getText();
    }

    public void clickBy(String buttonName) {
        IButton btn = getElementFactory().getButton(By.xpath(String.format(BUTTON_SHORTLIST_TEMPLATE, buttonName)), buttonName);
        btn.clickAndWait();
    }
}
