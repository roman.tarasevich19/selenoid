package diaceutics.selenium.pageobject.pages.marketplace;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class LoginPage extends BaseMarketplaceForm {

    private final ITextBox usernameTxb = getElementFactory().getTextBox(By.id("email"), "Username");
    private final ITextBox passwordTxb = getElementFactory().getTextBox(By.id("password"), "Password");
    private final IButton loginBtn = getElementFactory().getButton(By.id("loginButton"), "Login button");

    public LoginPage() {
        super(By.id("loginContainer"), "Login page");
    }

    public void logInAs(User user) {
        usernameTxb.clearAndType(user.getEmail());
        passwordTxb.clearAndType(user.getPassword());
        clickLoginButton();
    }

    public void clickLoginButton() {
        loginBtn.clickAndWait();
    }
}
//required