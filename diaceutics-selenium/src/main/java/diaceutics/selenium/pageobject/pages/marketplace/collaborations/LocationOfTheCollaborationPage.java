package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class LocationOfTheCollaborationPage extends BaseMarketplaceForm {

    public LocationOfTheCollaborationPage() {
        super(By.xpath("//h2[contains(text(),'Our location')]"), "Our location");
    }
}
