package diaceutics.selenium.pageobject.pages.newregistration;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.pageobject.forms.BaseForm;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class ResetPasswordPage extends BaseMarketplaceForm {

    private final IButton requestResetBtn = getElementFactory().getButton(By.id("requestResetButton"), "Request reset button");
    private final ITextBox accountEmail = getElementFactory().getTextBox(By.id("resetEmail"), "Account email");
    private final IButton backToLoginBtn = getElementFactory().getButton(By.xpath("//span[text()='Back to login']"), "Back to login button");

    public ResetPasswordPage() {
        super(By.xpath("//h1[text()='Forgot password?']"), "Reset password page");
    }

    public void requestReset(){
        requestResetBtn.clickAndWait();
    }

    public void setAccountEmailValue(String email){
        accountEmail.clearAndType(email);
    }

    public void backToLogin(){
        backToLoginBtn.clickAndWait();
    }
}
