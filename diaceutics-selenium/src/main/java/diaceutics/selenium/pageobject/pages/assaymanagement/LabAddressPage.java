package diaceutics.selenium.pageobject.pages.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class LabAddressPage extends BaseToolForm {

    public LabAddressPage() {
        super(By.xpath("//h3[contains(text(),'Lab Address')]"), "Lab Address");
    }

}
