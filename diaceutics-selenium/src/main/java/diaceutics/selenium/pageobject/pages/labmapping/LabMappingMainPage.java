package diaceutics.selenium.pageobject.pages.labmapping;

import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.AboutThisDataDescriptionForm;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class LabMappingMainPage extends BaseToolForm {

    public LabMappingMainPage() {
        super(By.id("labMappingSearchContainer"), "lab Mapping Search");
    }

    public AboutThisDataDescriptionForm getAboutThisDataDescriptionForm(){
        return new AboutThisDataDescriptionForm();
    }

    public final ILabel serviceExplainerLabel = getElementFactory().getLabel(
            By.cssSelector("div[id='titleArea'] p"), "Service Explainer Label");

    public String getTextFromSeviceExplainer(){
        return serviceExplainerLabel.getText();
    }
}
