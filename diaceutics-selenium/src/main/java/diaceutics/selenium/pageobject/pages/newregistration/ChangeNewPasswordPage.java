package diaceutics.selenium.pageobject.pages.newregistration;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.utilities.JavaScriptUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class ChangeNewPasswordPage extends BaseMarketplaceForm {

    private static final String MESSAGE_TEMPLATE = "//*[contains(text(),'%s')]";
    private static final String PASSWORD_DIV_BOX =
            "//div[contains(@class,'lock-input-wrap')][.//input[@placeholder='your new password']]";

    private static final String CONFIRM_PASSWORD_DIV_BOX =
            "//div[contains(@class,'lock-input-wrap')][.//input[@placeholder='confirm your new password']]";

    private static final String ERROR_MESSAGE_PASSWORD = "//div[contains(@class,'animated fadeIn')]//li[text()='%s']";

    private final ITextBox password = getElementFactory().getTextBox(
            By.xpath(PASSWORD_DIV_BOX + "//input"), "Password field");

    private final ITextBox confirmPassword = getElementFactory().getTextBox(
            By.xpath(CONFIRM_PASSWORD_DIV_BOX + "//input"), "Confirm password field");

    private final IButton submitBtn = getElementFactory().getButton(
            By.xpath("//button[@class='auth0-lock-submit']"), "Submit button");

    public ChangeNewPasswordPage() {
        super(By.xpath("//div[@class='auth0-lock-header-welcome']"), "Change new password page");
    }

    public void setPassword(String value) {
        password.getElement().sendKeys(Keys.BACK_SPACE);
        password.clearAndType(value);
    }

    public void setConfirmPassword(String value) {
        confirmPassword.clearAndType(value);
    }

    public void submit() {
        submitBtn.clickAndWait();
    }

    public boolean isRedBorderForPasswordField() {
        WebElement elementBox = AqualityServices.getBrowser().getDriver().findElement(By.xpath(PASSWORD_DIV_BOX));
        AqualityServices.getConditionalWait().waitFor(elementBox::isDisplayed);
        return JavaScriptUtil.getBorderTopColor(elementBox).contains(ElementColor.RED_AUTH_ZERO.getColorCode());
    }

    public boolean isRedBorderForConfirmPasswordField() {
        WebElement elementBox = AqualityServices.getBrowser().getDriver().findElement(By.xpath(CONFIRM_PASSWORD_DIV_BOX));
        return JavaScriptUtil.getBorderTopColor(elementBox).contains(ElementColor.RED_AUTH_ZERO.getColorCode());
    }

    public String getMessageTextColor(String message) {
        ILabel errorMessageForPassword = getElementFactory().getLabel(By.xpath(String.format(ERROR_MESSAGE_PASSWORD, message)), "error message for password");
        return errorMessageForPassword.getCssValue("color");
    }

    public boolean isMessageOnPage(String messageText) {
        ILabel message = getElementFactory().getLabel(By.xpath(String.format(MESSAGE_TEMPLATE, messageText)), "Message");
        return message.state().isDisplayed();
    }
}