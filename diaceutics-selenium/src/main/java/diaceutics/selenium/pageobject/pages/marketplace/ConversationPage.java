package diaceutics.selenium.pageobject.pages.marketplace;

import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class ConversationPage extends BaseMarketplaceForm {

    private static final String MESSAGE_LABEL_TEMPLATE = "//div[contains(@class,'card-body')][.//p[contains(text(),'%s')]]";
    private static final String FILE_LINK_TEMPLATE = MESSAGE_LABEL_TEMPLATE +
            "//div[contains(@class,'message-linked-attachments')]//a";

    public static final String USER_NAME_LABEL_TEMPLATE = MESSAGE_LABEL_TEMPLATE +
            "//a/span[contains(text(),'%s %s.')]";

    public ConversationPage() {
        super(By.xpath("//div[@class='container'][.//span[contains(text(),'Discussion')]]"), "Conversation page");
    }

    public String getAttachmentFileNameForMessage(String message) {
        ILink fileLink = getElementFactory().getLink(
                By.xpath(String.format(FILE_LINK_TEMPLATE, message)), "File link");
        return fileLink.getText().trim();
    }

    public boolean isMessageDisplayedFromUser(String message, User user) {
        ILabel messageLabel = getElementFactory().getLabel(By.xpath(
                String.format(USER_NAME_LABEL_TEMPLATE, message, user.getFirstName(), user.getFirstLetterFromLastName())),
                message);

        return messageLabel.state().waitForDisplayed();
    }
}
