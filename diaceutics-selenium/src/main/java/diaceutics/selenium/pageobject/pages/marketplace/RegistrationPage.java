package diaceutics.selenium.pageobject.pages.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.pageobject.forms.marketplace.PersonalDetailsForm;
import org.openqa.selenium.By;

public class RegistrationPage extends BaseMarketplaceForm {

    public RegistrationPage() {
        super(By.id("form-registerlogin"), "Registration");
    }

    public PersonalDetailsForm getPersonalDetailsForm() {
        return new PersonalDetailsForm();
    }
}
