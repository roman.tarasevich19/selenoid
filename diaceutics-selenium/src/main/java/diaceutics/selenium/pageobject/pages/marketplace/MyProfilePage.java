package diaceutics.selenium.pageobject.pages.marketplace;

import aquality.selenium.elements.interfaces.IButton;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.pageobject.forms.marketplace.UserEditIdentityForm;
import diaceutics.selenium.pageobject.forms.marketplace.UserSettingsForm;
import org.openqa.selenium.By;

public class MyProfilePage extends BaseMarketplaceForm {

    public MyProfilePage() {
        super(By.name("user"), "My profile");
    }

    public IButton settingsBtn = getElementFactory().getButton(By.xpath("//span[contains(text(),'Settings')]"), "Settings button");

    public UserEditIdentityForm getUserEditIdentityForm() {
        return new UserEditIdentityForm();
    }

    public UserSettingsForm getUserSettingsForm() {
        return new UserSettingsForm();
    }

    public void openSettings(){
        settingsBtn.clickAndWait();
    }
}
