package diaceutics.selenium.pageobject.pages.marketplace.organizations;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.Label;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.models.Organization;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class OrganizationsListingPage extends BaseMarketplaceForm {

    private static final String ORGANISATIONS_TEMPLATE = "//div[contains(@class,'column-content-item')]";
    private static final String ORGANISATION_NAME_TEMPLATE = ".//h3/a";
    private static final String ORGANISATION_TYPE_TEMPLATE = ".//div[contains(@class,'default')]";
    private static final String ORGANISATION_LOCATION_TEMPLATE = ".//div[contains(@class,'uppercase')]";
    private static final String ORGANISATION_NAMES_TEMPLATE = ORGANISATIONS_TEMPLATE + "//h3/a";

    public OrganizationsListingPage() {
        super(By.xpath("//div[contains(@class,'column-content')]"), "Organizations Listing page");
    }

    public boolean areOrganizationsDisplayed() {
        List<Label> organizationsLabel = getElementFactory().findElements(By.xpath(ORGANISATIONS_TEMPLATE), ElementType.LABEL);
        return !organizationsLabel.isEmpty();
    }

    public List<Organization> getListOrganization() {
        List<Organization> organizations = new ArrayList<>();
        getElementFactory().findElements(By.xpath(ORGANISATIONS_TEMPLATE), ElementType.LABEL)
                .forEach(e -> organizations.add(Organization.builder()
                        .name(e.findChildElement(By.xpath(ORGANISATION_NAME_TEMPLATE), ElementType.LABEL).getText().trim())
                        .organizationType(e.findChildElement(By.xpath(ORGANISATION_TYPE_TEMPLATE), ElementType.LABEL).getText())
                        .location(e.findChildElement(By.xpath(ORGANISATION_LOCATION_TEMPLATE), ElementType.LABEL).getText())
                        .build()));
        return organizations;
    }

    public void openRandomOrganization() {
        ILink collaborationTitleLabel = getElementFactory().getLink(By.xpath(ORGANISATION_NAMES_TEMPLATE),
                "Organization name link");
        collaborationTitleLabel.clickAndWait();
    }
}
