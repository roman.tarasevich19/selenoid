package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.ChangeAssayStatusForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.DeleteAssayForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.LabTestBiomarkerForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.ReactivateThisAssayForm;
import org.openqa.selenium.By;


public class EditAssayPage extends BaseToolForm {

    private final IButton deleteBtn = getElementFactory().getButton(
            By.xpath("//div[contains(@class,'delete-button')]"), "Delete");

    public EditAssayPage() {
        super(By.xpath("//h1[.='Edit assay']"), "Edit assay");
    }

    public DeleteAssayForm getDeleteAssayForm() {
        return new DeleteAssayForm();
    }

    public ReactivateThisAssayForm getReactivateThisAssayForm() {
        return new ReactivateThisAssayForm();
    }

    public ChangeAssayStatusForm getChangeAssayStatusForm() {
        return new ChangeAssayStatusForm();
    }

    public LabTestBiomarkerForm getLabTestBiomarkerForm() {
        return new LabTestBiomarkerForm();
    }

    public void clickDelete() {
        deleteBtn.clickAndWait();
    }

    public boolean isDisplayedDeleteButton() {
        return deleteBtn.state().isDisplayed();
    }
}
