package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.Attributes;
import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.Link;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.utilities.JavaScriptUtil;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LabsPage extends BaseToolForm {

    private static final String ALL_LAB_TEMPLATE = "//div[contains(@class,'result')]//div//a";
    private static final String LAB_TEMPLATE = ALL_LAB_TEMPLATE + "[.='%s']";
    private final static String LAB_COUNT_REGEX = "(?<=of )(\\d+)";
    private final static String MIN_RANGE_REGEX = "\\d+";
    private final static String MAX_RANGE_REGEX = "(?<=- )(\\d+)";
    private final static String LAB_RESULT_BLOCK = "div[class='result']";
    private final static String CURRENT_PAGE_REGEX = "\\d+";

    private final ILabel labLabel = getElementFactory().getLabel(By.xpath(ALL_LAB_TEMPLATE), "Lab");
    private final ILabel countryNameLabel = getElementFactory().getLabel(By.xpath("//span//strong"), "Country name");
    private final ILabel labsCountLabel = getElementFactory().getLabel(
            By.xpath("//p//strong[contains(text(),'labs')]"), "Country name");

    private final ILabel currentPageLabel = getElementFactory().getLabel(
            By.xpath("//span[@class='currentPage']"), "Current page");

    private final ILink nextLink = getElementFactory().getLink(
            By.xpath("//div[@class='navigationButtons']//span[.='Next >']"), "Next");

    private final ILink firstLink = getElementFactory().getLink(
            By.xpath("//div[@class='navigationButtons']//span[.='First']"), "First");

    private final ILabel loaderLabel = getElementFactory().getLabel(
            By.xpath("//ui-skeleton-loader//span[contains(@class,'loader')]"), "Loader");

    public LabsPage() {
        super(By.xpath("//h1[.='Labs']"), "Labs");
    }

    public void clickByLabLink(String labName) {
        ILink labLink = getElementFactory().getLink(
                By.xpath(String.format(LAB_TEMPLATE, labName)),
                labName);

        labLink.clickAndWait();
    }

    public boolean isLabDisplayedIndFilterResults(String labName) {
        firstLink.clickAndWait();
        ILink labLink = getElementFactory().getLink(By.xpath(String.format(LAB_TEMPLATE, labName)), labName);
        while (!labLink.state().waitForDisplayed()
                && !nextLink.getAttribute(Attributes.CLASS.toString()).contains("disabled")) {
            nextLink.clickAndWait();
        }

        return labLink.state().isDisplayed();
    }

    public List<String> getTextFromEachEntry() {
        AqualityServices.getConditionalWait().waitFor(JavaScriptUtil::waitForAngular);
        List<ILabel> labLabels = getElementFactory().findElements(By.cssSelector(LAB_RESULT_BLOCK), ElementType.LABEL);
        List<String> searchedText = new ArrayList<>();
        labLabels.forEach(labText -> searchedText.add(labText.getText()));
        return searchedText;
    }

    public String getRandomLabName() {
        waitForLabsDisplayed();
        List<Link> labLinks = getElementFactory().findElements(By.xpath(ALL_LAB_TEMPLATE), ElementType.LINK);
        List<String> labs = new ArrayList<>();
        labLinks.forEach(option -> labs.add(option.getText()));
        int randomIndex = new Random().nextInt(labs.size());
        return labs.get(randomIndex);
    }

    public String getLabsCount() {
        return RegExUtil.regexGetMatchGroup(labsCountLabel.getText(), LAB_COUNT_REGEX, 1).trim();
    }

    public String getCountryName() {
        return countryNameLabel.getText();
    }

    public void clickNext() {
        nextLink.clickAndWait();
    }

    public void clickFirst() {
        firstLink.clickAndWait();
    }

    public String getMinRange() {
        waitForLabsDisplayed();
        return RegExUtil.regexGetMatchGroup(labsCountLabel.getText(), MIN_RANGE_REGEX, 0).trim();
    }

    public String getMaxRange() {
        waitForLabsDisplayed();
        return RegExUtil.regexGetMatchGroup(labsCountLabel.getText(), MAX_RANGE_REGEX, 0).trim();
    }

    public void clickNext(int numberOfTimes) {
        for (int i = 0; i < numberOfTimes; i++) {
            waitForLabsDisplayed();
            nextLink.clickAndWait();
        }
    }

    public int getCurrentNumberOfPage() {
        waitForLabsDisplayed();
        return Integer.parseInt(RegExUtil.regexGetMatchGroup(currentPageLabel.getText(), CURRENT_PAGE_REGEX, 0));
    }

    public void waitForLabsDisplayed() {
        loaderLabel.state().waitForNotDisplayed();
        labLabel.state().waitForDisplayed();
    }
}
