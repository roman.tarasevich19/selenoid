package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.Label;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

import java.util.List;

public class CollaborationsListingPage extends BaseMarketplaceForm {

    private static final String COLLABORATIONS_TEMPLATE = "//div[@class='card-body pt-1_5']";
    private static final String COLLABORATION_TITLES_TEMPLATE = "//div[@class='card-body pt-1_5']//h3/a";
    private static final String COLLABORATION_TEMPLATE = "//div[@class='card-body pt-1_5'][.//a[contains(text(),'%s')]]";
    private static final String OPEN_COLLABORATION_BUTTON_TEMPLATE = COLLABORATION_TEMPLATE +
            "//div[contains(text(),'OPEN')]";

    public CollaborationsListingPage() {
        super(By.xpath("//body[contains(@class,'result-page')]"), "Collaborations Listing");
    }

    public void openCollaboration(String collaborationTitle) {
        IButton editCollaborationBtn = getElementFactory().getButton(
                By.xpath(String.format(OPEN_COLLABORATION_BUTTON_TEMPLATE, collaborationTitle)), collaborationTitle);

        editCollaborationBtn.clickAndWait();
    }

    public boolean areCollaborationsDisplayed() {
        List<Label> collaborationLabel = getElementFactory().findElements(By.xpath(COLLABORATIONS_TEMPLATE), ElementType.LABEL);
        return !collaborationLabel.isEmpty();
    }

    public boolean isCollaborationDisplayed(String title) {
        ILabel collaborationLabel = getElementFactory().getLabel(By.xpath(String.format(COLLABORATION_TEMPLATE, title)), title);
        return collaborationLabel.state().waitForDisplayed();
    }

    public void openRandomCollaboration() {
        ILink collaborationTitleLabel = getElementFactory().getLink(By.xpath(COLLABORATION_TITLES_TEMPLATE),
                "Collaboration title link");
        collaborationTitleLabel.clickAndWait();
    }
}
