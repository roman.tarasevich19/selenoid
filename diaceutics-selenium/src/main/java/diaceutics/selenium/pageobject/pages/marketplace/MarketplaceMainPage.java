package diaceutics.selenium.pageobject.pages.marketplace;

import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.pageobject.forms.marketplace.footer.CookieProForm;
import org.openqa.selenium.By;

public class MarketplaceMainPage extends BaseMarketplaceForm {

    private static final String LINK_OF_THE_MIDDLE_TEMPLATE = "//div[contains(@class,'col-lg-10')]//a[contains(text(),'%s')]";

    public MarketplaceMainPage() {
        super(By.xpath("//div[contains(@class,'jumbotron-holder')]"), "Marketplace");
    }

    public CookieProForm getCookieProForm() {
        return new CookieProForm();
    }

    public void clickByLinkOnTheMiddleOfThePage(String linkName) {
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(LINK_OF_THE_MIDDLE_TEMPLATE, linkName)), linkName);

        link.clickAndWait();
    }
}
