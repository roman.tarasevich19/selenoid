package diaceutics.selenium.pageobject.pages.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class AddALocationPage extends BaseToolForm {

    public AddALocationPage() {
        super(By.xpath("//h1[.='Add a location']"), "Add a location");
    }

}
