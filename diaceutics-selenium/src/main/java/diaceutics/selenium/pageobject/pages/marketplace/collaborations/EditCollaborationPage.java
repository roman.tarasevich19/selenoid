package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.pageobject.forms.marketplace.CategoryForm;
import diaceutics.selenium.pageobject.forms.marketplace.MyLocationForm;
import diaceutics.selenium.pageobject.forms.marketplace.PresentationForm;
import org.openqa.selenium.By;

public class EditCollaborationPage extends BaseMarketplaceForm {

    private static final String LINK_TEMPLATE = "//li[@class='mb-1']//a[contains(text(),'%s')]";
    private static final String GROUP_LINK_TEMPLATE = "//div[contains(@class,'align-items-center')]//a[contains(text(),'%s')]";

    private final IButton saveChangesBtn = getElementFactory().getButton(
            By.xpath("//div//button[contains(text(),'Save changes')]"), "Save changes button");

    public EditCollaborationPage() {
        super(By.id("asideNavAccordion"), "Edit collaboration");
    }

    public PresentationForm getPresentationForm() {
        return new PresentationForm();
    }

    public CategoryForm getCategoryForm() {
        return new CategoryForm();
    }

    public MyLocationForm getMyLocationForm() {
        return new MyLocationForm();
    }

    public void clickLink(String linkName) {
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);

        link.clickAndWait();
    }

    public void clickGroupLink(String linkName) {
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(GROUP_LINK_TEMPLATE, linkName)), linkName);

        link.clickAndWait();
    }

    public void clickSaveChangesButton() {
        saveChangesBtn.clickAndWait();
    }

}
