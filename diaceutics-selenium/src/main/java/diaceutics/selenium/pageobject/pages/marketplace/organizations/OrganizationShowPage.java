package diaceutics.selenium.pageobject.pages.marketplace.organizations;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class OrganizationShowPage extends BaseMarketplaceForm {

    public OrganizationShowPage() {
        super(By.xpath("//div[contains(@class,'organization_show')]"), "Organization Show page");
    }
}
