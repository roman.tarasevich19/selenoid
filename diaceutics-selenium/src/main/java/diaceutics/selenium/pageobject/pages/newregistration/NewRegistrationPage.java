package diaceutics.selenium.pageobject.pages.newregistration;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.configuration.Configuration;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.pageobject.forms.newregistration.NewPersonalDetailsForm;
import org.openqa.selenium.By;

public class NewRegistrationPage extends BaseMarketplaceForm {

    public static final String LABEL_TEMPLATE = "//*[contains(text(),'%s')]";

    public NewRegistrationPage() {
        super(By.tagName("app-registration-stepper"), "Registration form");
    }

    public NewPersonalDetailsForm getNewPersonalDetailsForm() {
        return new NewPersonalDetailsForm();
    }

    public boolean isLabelDisplayed(String message){
        ILabel label = getElementFactory().getLabel(By.xpath(String.format(LABEL_TEMPLATE, message)), "Message label");
        return label.state().isDisplayed();
    }

    public void openNewRegistrationPage(){
        AqualityServices.getBrowser().goTo(Configuration.getUrl(Configuration.REGISTRATION_UI_URL));
    }
}
