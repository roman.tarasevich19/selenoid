package diaceutics.selenium.pageobject.pages.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class SearchResultsPage extends BaseMarketplaceForm {

    public SearchResultsPage() {
        super(By.xpath("//div[@id='labSearchContainer']"), "Search Results");
    }

}
