package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.BiomarkersAndVariantsGridForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.MoveThisAssayForm;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;

public class AssayDescriptionPage extends BaseToolForm {

    private static final String TEXT_H4_TEMPLATE = "//h4[contains(text(),'%s')]/following-sibling::*[1]";
    private static final String TEXT_GRID_TEMPLATE = "//strong[contains(text(),'%s')]/ancestor::ui-card-grid-row/child::*[2]";

    private final static String MONTH_REGEX = "(?<=: )(\\w+)";

    private final ILink editDetailsLink = getElementFactory().getLink(
            By.xpath("//a//span[.='Edit Details']"), "Edit Details");

    private final IButton moveThisAssayBtn = getElementFactory().getButton(
            By.xpath("//span[text()='Move this assay']"), "Move this assay");

    public AssayDescriptionPage() {
        super(By.xpath("//h4[.='Assay description']"), "Assay description");
    }

    public MoveThisAssayForm getMoveThisAssayForm() {
        return new MoveThisAssayForm();
    }

    public BiomarkersAndVariantsGridForm getBiomarkersAndVariantsGridForm() {
        return new BiomarkersAndVariantsGridForm();
    }

    public void clickEditDetails() {
        editDetailsLink.clickAndWait();
    }

    public void clickMoveThisAssay() {
        moveThisAssayBtn.clickAndWait();
    }

    @Override
    public String getFieldValue(FormFieldInterface field) {
        String value = null;
        switch (field.getFieldType()) {
            case TEXT_H1:

            case ASSAY_STATUS:
                value = getElementFactory().getLabel(By.xpath(field.getLocator()), field.getFriendlyName()).getText();
                break;

            case TEXT_H4:
                value = getElementFactory().getLabel(By.xpath(String.format(TEXT_H4_TEMPLATE, field.getLocator())),
                        field.getFriendlyName()).getText();
                break;

            case TEXT_GRID:
                value = getElementFactory().getLabel(By.xpath(String.format(TEXT_GRID_TEMPLATE, field.getLocator())),
                        field.getFriendlyName()).getText();
                break;

            case YEAR:
                value = RegExUtil.getNumbersFromString(
                        getElementFactory().getLabel(By.xpath(field.getLocator()), field.getFriendlyName()).getText());
                break;

            case MONTH:
                value = RegExUtil.regexGetMatchGroup(
                        getElementFactory().getLabel(By.xpath(field.getLocator()), field.getFriendlyName()).getText(),
                        MONTH_REGEX, 0);
                break;

            default:
                break;
        }

        return value;
    }
}
