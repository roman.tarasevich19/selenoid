package diaceutics.selenium.pageobject.pages.marketplace.organizations;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.pageobject.forms.marketplace.OrganizationEditIdentityForm;
import org.openqa.selenium.By;

public class OrganizationPage extends BaseMarketplaceForm {

    public OrganizationPage() {
        super(By.id("main-form"), "Organization");
    }

    public OrganizationEditIdentityForm getOrganizationEditIdentityForm() {
        return new OrganizationEditIdentityForm();
    }
}
