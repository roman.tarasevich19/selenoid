package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.BiomarkerVolumeForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.DeleteVolumeForm;
import org.openqa.selenium.By;


public class EditVolumePage extends BaseToolForm {

    private final IButton deleteVolumeBtn = getElementFactory().getButton(
            By.xpath("//div[contains(@class,'deleteButton')]"), "Delete Button");

    private final ILink viewLogLink = getElementFactory().getLink(By.xpath("//ui-alert//span/a"), "View log");

    public EditVolumePage() {
        super(By.xpath("//h1[.='Edit volume']"), "Edit volume");
    }

    public BiomarkerVolumeForm getBiomarkerVolumeForm() {
        return new BiomarkerVolumeForm();
    }

    public DeleteVolumeForm getDeleteVolumeForm() {
        return new DeleteVolumeForm();
    }

    public void clickDeleteVolume() {
        deleteVolumeBtn.clickAndWait();
    }

    public void clickViewLog() {
        viewLogLink.clickAndWait();
    }
}
