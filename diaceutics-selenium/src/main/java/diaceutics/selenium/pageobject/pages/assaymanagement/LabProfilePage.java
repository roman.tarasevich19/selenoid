package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.enums.labs.LabTypes;
import diaceutics.selenium.models.*;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.*;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;

import java.util.List;

public class LabProfilePage extends BaseToolForm {

    private static final String ADD_BUTTON_TEMPLATE = "//div[contains(@class,'titleArea')]//button[.='%s']";
    private static final String SEARCH_BUTTON_TEMPLATE = "//ui-search[./input[contains(@placeholder,'%s')]]/ui-icon";
    private final static String LAB_TYPE_REGEX = "\\((.*?)\\)";

    private final ILink backLink = getElementFactory().getLink(By.xpath("//a[contains(text(),'Back')]"), "Back");
    private final ILink editDetailsLink = getElementFactory().getLink(
            By.xpath("//a//span[.='Edit Details']"), "Edit Details");

    private final ILink labNameLink = getElementFactory().getLink(
            By.xpath("//div[contains(@class,'titleArea')]//h1"), "Lab name");

    private final ILink labLocationLink = getElementFactory().getLink(
            By.xpath("//div[contains(@class,'details')]//span[contains(text(),'|')]"),
            "Location");

    private final List<IElement> labTypeLink = getElementFactory().findElements(By.xpath
            ("//div[contains(@class,'details')]//span"), ElementType.LINK);

    private final ILabel accessIsDenied = getElementFactory().getLabel(By.xpath("//div[@class='accessDeniedContainer']"), "Access is denied");

    public LabProfilePage() {
        super(By.id("viewLabContainer"), "LabProfile");
    }

    public PatientVolumeLogGridForm getPatientVolumeLogGridForm() {
        return new PatientVolumeLogGridForm();
    }

    public AssaysGridForm getAssaysGridForm() {
        return new AssaysGridForm();
    }

    @Override
    public boolean isDisplayed() {
        editDetailsLink.state().waitForDisplayed();
        editDetailsLink.state().waitForClickable();
        return super.isDisplayed();
    }

    public boolean hideVersionOfPageIsDisplayed() {
        labNameLink.state().waitForDisplayed();
        return labNameLink.state().isDisplayed();
    }

    public boolean assayBlockIsPresent() {
        List<ILabel> assayBlock = getElementFactory().findElements(By.cssSelector("app-view-lab-tests"), ElementType.LABEL);
        return assayBlock.size() > 0;
    }

    public boolean volumeBlockIsPresent() {
        List<ILabel> volumeBlock = getElementFactory().findElements(By.cssSelector("app-view-volumes"), ElementType.LABEL);
        return volumeBlock.size() > 0;
    }

    public boolean isAccessIsDenied() {
        accessIsDenied.state().waitForDisplayed();
        return accessIsDenied.state().isDisplayed();
    }

    public boolean editDetailsIsPresent() {
        List<ILabel> editDetails = getElementFactory().findElements(By.xpath("//a//span[.='Edit Details']"), ElementType.LABEL);
        return editDetails.size() > 0;
    }

    public boolean addAssayIsPresent() {
        List<ILabel> addAssay = getElementFactory()
                .findElements(By.xpath("//div[contains(@class,'titleArea')]//button[.='Add assay']"), ElementType.LABEL);
        return addAssay.size() > 0;
    }

    public boolean addVolumeIsPresent() {
        List<ILabel> addVolume = getElementFactory()
                .findElements(By.xpath("//div[contains(@class,'titleArea')]//button[.='Add volume']"), ElementType.LABEL);
        return addVolume.size() > 0;
    }

    public void clickEditDetails() {
        editDetailsLink.clickAndWait();
    }

    public String getLabNameFromPage() {
        return labNameLink.getText();
    }

    public String getLabTypeFromPage() {
        String labType = LabTypes.UNSPECIFIED.toString();
        if (labTypeLink.size() > 2) {
            labType = RegExUtil.regexGetMatchGroup(labTypeLink.get(1).getText(), LAB_TYPE_REGEX, 1) + " Lab";
        }
        return labType;
    }

    public Lab getLabFromPage() {
        Lab lab = new Lab();
        lab.setName(getLabNameFromPage());
        lab.setLabType(getLabTypeFromPage());
        return lab;
    }

    public Address getLabLocationFromPage() {
        Address address = new Address();
        String[] labLocation = labLocationLink.getText().replace("|", "").split(",");
        if (labLocation.length > 1) {
            address.setCountry(labLocation[1].trim());
            address.setRegion(labLocation[0].trim());
        } else {
            address.setCountry(labLocation[0].trim());
        }
        return address;
    }

    public void clickAdd(String buttonName) {
        IButton addBtn = getElementFactory().getButton(
                By.xpath(String.format(ADD_BUTTON_TEMPLATE, buttonName)), buttonName);

        addBtn.clickAndWait();
    }

    public void clickBack() {
        backLink.clickAndWait();
    }

    public void clickSearch(String searchFieldName) {
        IButton searchBtn = getElementFactory().getButton(By.xpath(
                String.format(SEARCH_BUTTON_TEMPLATE, searchFieldName)), searchFieldName);
        searchBtn.clickAndWait();
    }
}
