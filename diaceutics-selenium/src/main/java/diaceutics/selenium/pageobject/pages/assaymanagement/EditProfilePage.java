package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.LabDetailsForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.LocationForm;
import org.openqa.selenium.By;


public class EditProfilePage extends BaseToolForm {

    private static final String LINK_TEMPLATE =
            "//div[contains(@class,'titleContainer')]//span[contains(text(),'%s')]";

    private final ILink returnToProfileLink = getElementFactory().getLink(
            By.xpath("//ui-alert//span/a[.='Return to profile']"), "Return to profile");

    private final IButton addNewLocationBtn = getElementFactory().getButton(
            By.xpath("//div[contains(@class,'buttons')]//ul[contains(@class,'tabButtons')]"), "Add new location");

    public EditProfilePage() {
        super(By.xpath("//div/h2[.='Lab Details']"), "Edit Profile Lab Details");
    }

    public LabDetailsForm getLabDetailsForm() {
        return new LabDetailsForm();
    }

    public LocationForm getLocationForm() {
        return new LocationForm();
    }

    public void clickReturnToProfile() {
        returnToProfileLink.clickAndWait();
    }

    public void clickAddNewLocation() {
        addNewLocationBtn.clickAndWait();
    }

    public void clickLink(String linkName) {
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);

        link.clickAndWait();
    }
}
