package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IElement;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.enums.pagefields.assaymanagement.AssayManagementPageFields;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class AssayManagementPage extends BaseToolForm {

    private static final String LIST_COUNTRIES_TEMPLATE = "//div[contains(@class,'byCountry')]//span";
    private static final String COUNTRY_TEMPLATE = LIST_COUNTRIES_TEMPLATE + "/a[.='%s']";
    private static final String LABS_COUNT_TEMPLATE = LIST_COUNTRIES_TEMPLATE + "[./a[.='%s']]";
    private final static String COUNTRY_REGEX = "(.*)(\\(.*\\))(.*)";

    public AssayManagementPage() {
        super(By.xpath("//h1[.='Assay Management']"), "Assay Management");
    }

    public void clickByCountryLink(String country) {
        ILink countryLink = getElementFactory().getLink(By.xpath(String.format(COUNTRY_TEMPLATE, country)), country);
        countryLink.clickAndWait();
    }

    public List<String> getStringListCountries() {
        List<IElement> countriesLinks = getElementFactory().findElements(By.xpath(LIST_COUNTRIES_TEMPLATE), ElementType.LINK);
        List<String> countries = new ArrayList<>();
        countriesLinks.forEach(option -> countries.add(
                RegExUtil.regexGetMatchGroup(option.getText(), COUNTRY_REGEX, 1).trim()));

        return countries;
    }

    public String getLabsCountForCountry(String country) {
        ILink labCountLink = getElementFactory().getLink(By.xpath(String.format(LABS_COUNT_TEMPLATE, country)), country);
        return RegExUtil.getNumbersFromString(labCountLink.getText());
    }

    public String getRandomCountry() {
        List<String> countries = getStringListCountries();
        int randomIndex = new Random().nextInt(countries.size());
        return countries.get(randomIndex);
    }

    public List<String> getListCountriesFromDropDown() {
        List<String> countries = getListOptionsFromField(AssayManagementPageFields.COUNTRY);
        countries.remove(0);
        return countries;
    }
}
