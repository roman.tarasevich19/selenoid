package diaceutics.selenium.pageobject.pages.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class ContactUsPage extends BaseMarketplaceForm {

    public ContactUsPage() {
        super(By.xpath("//h1[text()='Get in Touch']"), "Privacy Statement page");
    }
}
