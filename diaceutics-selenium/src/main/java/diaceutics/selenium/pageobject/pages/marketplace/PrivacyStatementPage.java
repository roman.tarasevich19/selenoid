package diaceutics.selenium.pageobject.pages.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class PrivacyStatementPage extends BaseMarketplaceForm {

    public PrivacyStatementPage() {
        super(By.xpath("//h1[text()='DXRX – The Diagnostic Network® Privacy Statement']"), "Privacy Statement page");
    }
}
