package diaceutics.selenium.pageobject.pages.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class TermsPage extends BaseMarketplaceForm {

    public TermsPage() {
        super(By.xpath("//h1[text()='DXRX Terms Index']"), "Terms page");
    }
}
