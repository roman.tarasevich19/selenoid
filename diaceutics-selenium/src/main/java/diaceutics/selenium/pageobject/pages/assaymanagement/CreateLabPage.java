package diaceutics.selenium.pageobject.pages.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class CreateLabPage extends BaseToolForm {

    public CreateLabPage() {
        super(By.xpath("//h1[text()='Create a Lab']"), "Create a Lab");
    }

}
