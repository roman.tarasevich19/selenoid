package diaceutics.selenium.pageobject.pages.physicianmapping;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseMappingForm;
import diaceutics.selenium.pageobject.forms.physicianmapping.AGGridPhysicianSummaryForm;
import diaceutics.selenium.pageobject.forms.physicianmapping.PhysicianMappingChartsForm;
import org.openqa.selenium.By;

public class PhysicianMappingPage extends BaseMappingForm {

    private final IButton exportDataBtn = getElementFactory().getButton(
            By.xpath("//div[contains(text(),'Export data')]"), "Export data");

    private final ILink mainPageLink = getElementFactory().getLink(
            By.xpath("//a[@class='ng-star-inserted']"), "Main page");

    public PhysicianMappingPage() {
        super(By.id("physicianMappingResultsContainer"), "Physician Mapping Results");
    }

    public AGGridPhysicianSummaryForm getAGGridPhysicianSummaryForm() {
        return new AGGridPhysicianSummaryForm();
    }

    public PhysicianMappingChartsForm getPhysicianMappingChartsForm() {
        return new PhysicianMappingChartsForm();
    }

    public void clickExportData() {
        exportDataBtn.clickAndWait();
    }

    public void clickMainPage() {
        mainPageLink.clickAndWait();
    }
}
