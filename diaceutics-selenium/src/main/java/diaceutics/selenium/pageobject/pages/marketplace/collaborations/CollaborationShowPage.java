package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.elements.interfaces.IButton;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import diaceutics.selenium.pageobject.forms.marketplace.InformForm;
import diaceutics.selenium.pageobject.forms.marketplace.QuestionForm;
import org.openqa.selenium.By;

public class CollaborationShowPage extends BaseMarketplaceForm {

    private final IButton askAQuestionBtn = getElementFactory().getButton(
            By.id("contact_drop"), "Ask a question Button");

    private final IButton submitAResponseBtn = getElementFactory().getButton(
            By.xpath("//a[@data-id='submit-booking']"), "Ask a question Button");

    public CollaborationShowPage() {
        super(By.xpath("//div[contains(@class,'cocorico_listing_show')]"), "Collaboration Show page");
    }

    public QuestionForm getQuestionForm() {
        return new QuestionForm();
    }

    public void clickByAskAQuestionButton() {
        askAQuestionBtn.clickAndWait();
    }

    public void clickBySubmitAResponseButton() {
        submitAResponseBtn.clickAndWait();
    }
}
