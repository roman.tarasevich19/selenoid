package diaceutics.selenium.pageobject.pages.assaymanagement;

import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.models.Assay;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.DiscardForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.LabTestBiomarkerForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.UploadBiomarkerForm;
import org.openqa.selenium.By;


public class AddAnAssayPage extends BaseToolForm {

    private static final String COMMERCIAL_ASSAY_BIOMARKER_TEMPLATE =
            "//div[contains(@class,'commercialAssayBiomarkers')]//span[contains(text(),'%s')]";

    public AddAnAssayPage() {
        super(By.xpath("//h1[.='Add an assay']"), "Add an assay");
    }

    public LabTestBiomarkerForm getLabTestBiomarkerForm() {
        return new LabTestBiomarkerForm();
    }

    public UploadBiomarkerForm getUploadBiomarkerForm() {
        return new UploadBiomarkerForm();
    }

    public DiscardForm getDiscardForm() {
        return new DiscardForm();
    }

    public boolean isCommercialAssayBiomarkerAdded(Assay assay) {
        ILabel commercialAssayBiomarkerLabel = getElementFactory().getLabel(
                By.xpath(String.format(COMMERCIAL_ASSAY_BIOMARKER_TEMPLATE, assay.getCommercialAssays())),
                assay.getCommercialAssays());

        return commercialAssayBiomarkerLabel.state().waitForDisplayed();
    }
}
