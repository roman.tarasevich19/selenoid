package diaceutics.selenium.pageobject.pages.marketplace;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.Label;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

import java.util.List;

public class RegistrationConfirmedPage extends BaseMarketplaceForm {

    private static final String MESSAGE_TEMPLATE = "//section//div[.='%s']";

    public RegistrationConfirmedPage() {
        super(By.xpath("//span[contains(@class,'success-lock')]"), "Register confirmed");
    }

    public boolean isMessageDisplayed(String message) {
        List<Label> messageLink = getElementFactory().findElements(
                By.xpath(String.format(MESSAGE_TEMPLATE, message)), ElementType.LABEL);
        return messageLink.size() > 0;
    }
}
