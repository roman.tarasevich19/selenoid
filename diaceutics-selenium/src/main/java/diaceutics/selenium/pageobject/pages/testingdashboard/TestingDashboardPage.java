package diaceutics.selenium.pageobject.pages.testingdashboard;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseMappingForm;
import diaceutics.selenium.pageobject.forms.testingdashboard.TestingDashboardChartsForm;
import org.openqa.selenium.By;

public class TestingDashboardPage extends BaseMappingForm {

    public final ILabel numberOfLabsLabel = getElementFactory().getLabel(
            By.xpath("//div[@class='bannerContent']//span"), "Number Of Labs");

    public final ILabel labsCountLoadingLabel = getElementFactory().getLabel(
            By.xpath("//div[@class='bannerContent']//span[text()='...']"), "Labs count loading");

    public TestingDashboardPage() {
        super(By.xpath("//h1[contains(text(),'Testing Dashboard')]"), "Testing Dashboard");
    }

    public TestingDashboardChartsForm getTestingDashboardChartsForm() {
        return new TestingDashboardChartsForm();
    }

    public Integer getNumberOfLabs() {
        labsCountLoadingLabel.state().waitForNotDisplayed();
        numberOfLabsLabel.state().waitForDisplayed();
        return Integer.valueOf(numberOfLabsLabel.getText());
    }

    @Override
    public boolean isDisplayed() {
        chartsLoadingSpinner.state().waitForNotDisplayed();
        return super.isDisplayed();
    }
}
