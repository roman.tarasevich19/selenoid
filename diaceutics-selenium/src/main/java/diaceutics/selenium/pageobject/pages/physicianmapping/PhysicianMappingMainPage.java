package diaceutics.selenium.pageobject.pages.physicianmapping;

import diaceutics.selenium.pageobject.forms.AboutThisDataDescriptionForm;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class PhysicianMappingMainPage extends BaseToolForm {

    public PhysicianMappingMainPage() {
        super(By.id("physicianMappingSearchContainer"), "Physician Mapping Main");
    }

    public AboutThisDataDescriptionForm getAboutThisDataDescriptionForm(){
        return new AboutThisDataDescriptionForm();
    }

}
