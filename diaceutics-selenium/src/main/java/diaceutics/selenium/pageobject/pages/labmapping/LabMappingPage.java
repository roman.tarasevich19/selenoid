package diaceutics.selenium.pageobject.pages.labmapping;

import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.pageobject.forms.BaseMappingForm;
import diaceutics.selenium.pageobject.forms.labmapping.AGGridLabSummaryForm;
import diaceutics.selenium.pageobject.forms.labmapping.LabMappingChartsForm;
import org.openqa.selenium.By;

public class LabMappingPage extends BaseMappingForm {

    private final IButton exportDataBtn = getElementFactory().getButton(
            By.xpath("//div[contains(text(),'Export data')]"), "Export data");

    public LabMappingPage() {
        super(By.id("labMappingResultsContainer"), "lab Mapping Results");
    }

    private final ILabel lmLabel = getElementFactory().getLabel(
            By.xpath("//h1[text()='Lab Mapping']"), "LM label");

    public AGGridLabSummaryForm getAGGridLabSummaryForm() {
        return new AGGridLabSummaryForm();
    }

    public LabMappingChartsForm getLabMappingChartsForm() {
        return new LabMappingChartsForm();
    }

    public void clickExportData() {
        exportDataBtn.clickAndWait();
    }

    @Override
    public boolean isDisplayed() {
        labelLargeSpinner.state().waitForNotDisplayed();
        loading.state().waitForNotDisplayed();
        return super.isDisplayed();
    }

    public void clickOnLMLabel() {
        lmLabel.click();
    }
}
