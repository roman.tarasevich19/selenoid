package diaceutics.selenium.pageobject.pages.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class CookiesPolicyPage extends BaseMarketplaceForm {

    public CookiesPolicyPage() {
        super(By.xpath("//h1[text()='DXRX – The Diagnostic Network® Cookies Policy']"), "Privacy Statement page");
    }
}
