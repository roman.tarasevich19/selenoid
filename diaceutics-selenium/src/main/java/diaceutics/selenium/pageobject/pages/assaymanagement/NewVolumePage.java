package diaceutics.selenium.pageobject.pages.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import diaceutics.selenium.pageobject.forms.assaymanagement.BiomarkerVolumeForm;
import org.openqa.selenium.By;


public class NewVolumePage extends BaseToolForm {

    public NewVolumePage() {
        super(By.xpath("//h1[.='New volume']"), "New volume");
    }

    public BiomarkerVolumeForm getBiomarkerVolumeForm() {
        return new BiomarkerVolumeForm();
    }
}
