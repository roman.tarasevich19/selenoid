package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class MessagesPage extends BaseMarketplaceForm {

    public static final String MESSAGE_LABEL_TEMPLATE =
            "//div[contains(@class,'border-bottom')][.//span[contains(text(),'%s %s.: %s')]]";

    public static final String DATE_LABEL_TEMPLATE = MESSAGE_LABEL_TEMPLATE + "//time";
    public static final String MESSAGE_BUTTON_TEMPLATE = MESSAGE_LABEL_TEMPLATE + "//a/i[@class='icon-comments']";

    public MessagesPage() {
        super(By.xpath("//div[@class='container'][.//span[.='Messages']]"), "Messages page");
    }

    public boolean isMessageDisplayedFromUser(String message, User user) {
        ILabel messageLabel = getElementFactory().getLabel(By.xpath(
                String.format(MESSAGE_LABEL_TEMPLATE, user.getFirstName(), user.getFirstLetterFromLastName(), message)),
                message);

        return messageLabel.state().waitForDisplayed();
    }

    public String getDateForMessageFromUser(String message, User user) {
        ILabel dateLabel = getElementFactory().getLabel(By.xpath(
                String.format(DATE_LABEL_TEMPLATE, user.getFirstName(), user.getFirstLetterFromLastName(), message)),
                message);

        return dateLabel.getText().trim();
    }

    public void clickMessageButton(String message, User user) {
        IButton messageBtn = getElementFactory().getButton(By.xpath(
                String.format(MESSAGE_BUTTON_TEMPLATE, user.getFirstName(), user.getFirstLetterFromLastName(), message)),
                message);

        messageBtn.clickAndWait();
    }
}
