package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.IComboBox;
import aquality.selenium.elements.interfaces.IElement;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class MyCollaborationsPage extends BaseMarketplaceForm {

    private static final String COLLABORATION_TEMPLATE = "//div[@class='card-body pt-1_5'][.//a[contains(text(),'%s')]]";
    private static final String COLLABORATION_BUTTON_TEMPLATE = COLLABORATION_TEMPLATE + "//span[contains(text(),'%s')]";
    private static final String COLLABORATION_TITLE_TEMPLATE = "//div[@class='card-body pt-1_5']//h3/a";

    private final IComboBox statusCombobox = getElementFactory().getComboBox(By.id("status"), "Status combobox");
    private final IButton applyBtn = getElementFactory().getButton(By.xpath("//input[@value='Apply']"), "Apply button");

    public MyCollaborationsPage() {
        super(By.id("breadcrumbs"), "My Collaborations");
    }

    public void clickByButtonForCollaboration(String buttonName, String title) {
        IButton editCollaborationBtn = getElementFactory().getButton(
                By.xpath(String.format(COLLABORATION_BUTTON_TEMPLATE, title, buttonName)), title);
        editCollaborationBtn.clickAndWait();
    }

    public int getNumberOfCollaborationsWithTitle(String title) {
        List<ILabel> collaborationLabel = getElementFactory().findElements(
                By.xpath(String.format(COLLABORATION_TEMPLATE, title)), ElementType.LABEL);
        return collaborationLabel.size();
    }

    public void setStatus(String status) {
        statusCombobox.selectByContainingText(status);
    }

    public void clickApplyButton() {
        applyBtn.clickAndWait();
    }

    public boolean isCollaborationDisplayed(String title) {
        ILabel collaborationLabel = getElementFactory().getLabel(By.xpath(String.format(COLLABORATION_TEMPLATE, title)), title);
        return collaborationLabel.state().waitForDisplayed();
    }

    public String getRandomCollaborationTitle() {
        List<String> titles = getListCollaborationTitles();
        int randomIndex = new Random().nextInt(titles.size());
        return titles.get(randomIndex);
    }

    private List<String> getListCollaborationTitles() {
        return getElementFactory().findElements(By.xpath(COLLABORATION_TITLE_TEMPLATE), ElementType.LABEL)
                .stream().map(IElement::getText).collect(Collectors.toList());
    }
}
