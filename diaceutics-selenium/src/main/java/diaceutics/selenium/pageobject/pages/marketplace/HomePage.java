package diaceutics.selenium.pageobject.pages.marketplace;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

import java.util.List;

public class HomePage extends BaseMarketplaceForm {

    private static final String ADMIN_MESSAGE_TEMPLATE = "//div[contains(@class,'text-center')]/p[text()='%s']";
    private static final String TOOLS_LINK_TEMPLATE =
            "//div[contains(@class,'card-body')][.//h2[contains(text(),'%s')]]//a[contains(text(),'Launch tool')]";

    private final IButton startCollaborationBtn = getElementFactory().getButton(
            By.xpath("//a[contains(@class,'btn-collaboration')]"), "Start collaboration");

    private final IButton viewAllPublicCollaborations = getElementFactory().getButton(
            By.xpath("//a[contains(text(),'View all public collaborations')]"), "View all public collaboration");

    public HomePage() {
        super(By.id("main"), "Home");
    }

    public void openTool(String tool) {
        ILink toolLink = getElementFactory().getLink(By.xpath(String.format(TOOLS_LINK_TEMPLATE, tool)), tool);
        toolLink.getJsActions().scrollToTheCenter();
        toolLink.clickAndWait();
    }

    public boolean isToolPresent(String tool){
        List<ILink> tools = getElementFactory().findElements(
                By.xpath(String.format(TOOLS_LINK_TEMPLATE, tool)), ElementType.LINK);
       return tools.size() > 0;
    }

    public void clickStartCollaboration(){
        startCollaborationBtn.clickAndWait();
    }

    public void clickViewAllPublicCollaborations(){
        viewAllPublicCollaborations.clickAndWait();
    }

    public boolean isAdminMessageDisplayed(String message) {
        ILabel adminMessageLabel = getElementFactory().getLabel(
                By.xpath(String.format(ADMIN_MESSAGE_TEMPLATE, message)), message);

        return adminMessageLabel.state().waitForDisplayed();
    }
}
