package diaceutics.selenium.pageobject.pages.marketplace.collaborations;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class YourResponsePage extends BaseMarketplaceForm {

    private final ITextBox messageTextBox = getElementFactory().getTextBox(By.id("booking_new_message"), "Message texBox");

    private final IButton continueBtn = getElementFactory().getButton(
            By.xpath("//input[@value='Continue']"), "Continue button");

    public YourResponsePage() {
        super(By.xpath("//h1[contains(text(),'Your response')]"), "Your response page");
    }

    public void putMessage(String message) {
        messageTextBox.clearAndType(message);
    }

    public void clickContinueButton() {
        continueBtn.clickAndWait();
    }
}
