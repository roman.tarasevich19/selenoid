package diaceutics.selenium.pageobject.pages.testingdashboard;

import aquality.selenium.browser.AqualityServices;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TestingDashboardMainPage extends BaseToolForm {

    public TestingDashboardMainPage() {
        super(By.id("testDashboardSearchContainer"), "Testing Dashboard Search");
    }

    @Override
    public boolean isDisplayed() {
        AqualityServices.getConditionalWait().waitFor(ExpectedConditions.visibilityOfAllElementsLocatedBy
                (By.xpath("//p[text()=' A service for configuring and outputting Testing Dashboard data. ']")));
        return super.isDisplayed();
    }
}
