package diaceutics.selenium.pageobject.forms.marketplace;

import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;


public class PersonalDetailsForm extends BaseMarketplaceForm {

    private static final String MESSAGE_TEMPLATE = "//div[contains(text(),'%s')]";
    private final IButton registerBtn = getElementFactory().getButton(By.id("button-register"), "Register");

    public PersonalDetailsForm() {
        super(By.id("user_registration_firstName"), "Personal Details");
    }

    public void clickRegister() {
        registerBtn.getJsActions().click();
    }

    public boolean isMessageDisplayed(String message) {
        ILabel messageLabel = getElementFactory().getLabel(
                By.xpath(String.format(MESSAGE_TEMPLATE, message)), message);
        return messageLabel.state().isDisplayed();
    }

}
