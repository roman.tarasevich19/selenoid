package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IElement;
import com.google.common.collect.Ordering;
import diaceutics.selenium.models.Biomarker;
import diaceutics.selenium.pageobject.forms.BaseForm;
import org.openqa.selenium.By;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BiomarkersAndVariantsGridForm extends BaseForm {

    private static final String BIOMARKER_TEMPLATE = "//tr[.//span[.='%s']]";
    private static final String VARIANTS_TEMPLATE = BIOMARKER_TEMPLATE + "//td[2]";
    private static final String ALL_BIOMARKERS = "//table[.//span[text()='Biomarker']]//tbody//td[1]//span[contains(@class,'ng-tns')]";

    public BiomarkersAndVariantsGridForm() {
        super(By.xpath("//h4[.='Biomarkers and variants']"), "Biomarkers and variants");
    }

    public boolean isBiomarkerDisplayed(Biomarker biomarker) {
        List<IElement> volumeLink = getElementFactory().findElements(By.xpath
                (String.format(BIOMARKER_TEMPLATE, biomarker.getBiomarkerName())), ElementType.LINK);

        return !volumeLink.isEmpty();
    }

    public List<String> getAllBiomarkersFromGrid() {
        return getElementFactory().findElements(By.xpath
                (ALL_BIOMARKERS), ElementType.LABEL)
                .stream()
                .map(IElement::getText)
                .collect(Collectors.toList());
    }

    public boolean areBiomarkersSorted() {
        return Ordering.natural().isOrdered(getAllBiomarkersFromGrid());
    }

    public List<String> getVariantsForBiomarker(String biomarker) {
        String variants = getElementFactory().getLabel(
                By.xpath(String.format(VARIANTS_TEMPLATE, biomarker)), "Variants label").getText();
        return Arrays.asList(variants.split(", "));
    }

    public boolean isVariantsSortedForBiomarker(String biomarker) {
        return Ordering.natural().isOrdered(getVariantsForBiomarker(biomarker));
    }

    public boolean isVariantsColumnEmptyForBiomarker(String biomarker) {
        return getVariantsForBiomarker(biomarker).get(0).isEmpty();
    }
}
