package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class DiscardForm extends BaseToolForm {

    public DiscardForm() {
        super(By.cssSelector("lib-confirm-modal"), "Discard form");
    }

    private static final String BUTTON_TEMPLATE = "//lib-confirm-modal//div[text()='%s']";

    @Override
    public void clickByButton(String name){
        IButton btn = getElementFactory().getButton(By.xpath(String.format(BUTTON_TEMPLATE, name)), name);
        btn.clickAndWait();
    }
}
