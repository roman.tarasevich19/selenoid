package diaceutics.selenium.pageobject.forms.physicianmapping;


import diaceutics.selenium.pageobject.forms.BaseChartsForm;
import org.openqa.selenium.By;

public class PhysicianMappingChartsForm extends BaseChartsForm {

    public PhysicianMappingChartsForm() {
        super(By.id("chartsTab"), "Physician Mapping Charts");
    }

}
