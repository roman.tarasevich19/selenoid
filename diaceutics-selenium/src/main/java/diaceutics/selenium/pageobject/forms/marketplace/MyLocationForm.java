package diaceutics.selenium.pageobject.forms.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class MyLocationForm extends BaseMarketplaceForm {

    public MyLocationForm() {
        super(By.xpath("//h4[contains(text(),'My location')]"), "My location");
    }
}
