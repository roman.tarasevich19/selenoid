package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.IElement;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseForm;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseGridForm extends BaseForm {

    private static final String SORT_COLUMN_BUTTON_TEMPLATE = "%s//th[.//span[.='%s']]//ui-icon";
    private static final String GRID_MESSAGE_TEMPLATE = "//ui-table[..//h3[.='%s']]//span[text()='%s']";
    private static final String COLUMN_VALUE_TEMPLATE = "%s%s//td[count(//th[.='%s']/preceding-sibling::*)+1]";
    private static final String ROW_TEMPLATE = "%s//tr[not(th)]";
    private static final String COLUMN_VALUES_TEMPLATE =
            "%s//tr[not(.//div[@class='biomarker'])]//td[count(//th[.='%s']/preceding-sibling::*)+1]";

    protected BaseGridForm(By locator, String name) {
        super(locator, name);
    }

    public void clickSortColumnInGrid(String column, String gridLocator) {
        IButton sortColumnBtn = getElementFactory().getButton(
                By.xpath(String.format(SORT_COLUMN_BUTTON_TEMPLATE, gridLocator, column)),
                String.format("Sort %s Column", column));
        sortColumnBtn.clickAndWait();
    }

    public boolean isMessageDisplayedInGrid(String message, String gridName) {
        labelLargeSpinner.state().waitForNotDisplayed();
        ILabel alertLabel = getElementFactory().getLabel(
                By.xpath(String.format(GRID_MESSAGE_TEMPLATE, gridName, message)), message);
        return alertLabel.state().waitForDisplayed();
    }

    public boolean isDataInColumnInGridSorted(String column, String gridLocator) {
        List<String> list = getValuesFromColumn(column, gridLocator);
        return list.stream().sorted().collect(Collectors.toList()).equals(list);
    }

    public List<String> getValuesFromColumn(String column, String gridLocator) {
        List<IElement> valueLinks = getElementFactory().findElements(
                By.xpath(String.format(COLUMN_VALUES_TEMPLATE, gridLocator, column)), ElementType.LINK);

        return valueLinks.stream().map(IElement::getText).collect(Collectors.toList());
    }

    public String getValueFromColumn(String gridLocator, String column, String rowLocator) {
        ILabel labUrlLink = getElementFactory().getLabel(
                By.xpath(String.format(COLUMN_VALUE_TEMPLATE, gridLocator, rowLocator, column)), column);

        return labUrlLink.getText();
    }

    public String getNumberOfRowsInGrid(String gridLocator) {
        List<IElement> rowLabels = getElementFactory().findElements(
                By.xpath(String.format(ROW_TEMPLATE, gridLocator)), ElementType.LABEL);
        return String.valueOf(rowLabels.size());
    }
}
