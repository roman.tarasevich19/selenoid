package diaceutics.selenium.pageobject.forms.marketplace;

import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class InformForm extends BaseMarketplaceForm {

    private final ILabel successMessageLabel = getElementFactory().getLabel(
            By.xpath("//div[contains(@class,'alert')]//h3"), "Success message");

    public InformForm() {
        super(By.id("flashes"), "Inform Form");
    }

    public String getSuccessMessage() {
        return successMessageLabel.getText().trim();
    }
}
