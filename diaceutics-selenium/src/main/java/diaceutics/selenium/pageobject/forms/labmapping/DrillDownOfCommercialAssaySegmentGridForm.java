package diaceutics.selenium.pageobject.forms.labmapping;

import diaceutics.selenium.pageobject.forms.assaymanagement.BaseGridForm;
import org.openqa.selenium.By;

public class DrillDownOfCommercialAssaySegmentGridForm extends BaseGridForm {

    private static final String ROW_TEMPLATE = "//tr[.//span[.='%s']]";

    public DrillDownOfCommercialAssaySegmentGridForm() {
        super(By.xpath("//ui-modal[contains(@class,'open')]//div[contains(@class,'header')]/span[text()='Drill down of Commercial Assay segment']"),
                "Drill down of Commercial Assay segment form");
    }

    public String getValueFromColumnForRow(String gridLocator, String column, String row) {
        return getValueFromColumn(gridLocator, column, String.format(ROW_TEMPLATE, row));
    }
}
