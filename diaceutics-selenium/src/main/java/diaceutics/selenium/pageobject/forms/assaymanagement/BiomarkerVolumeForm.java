package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.Attributes;
import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IElement;
import diaceutics.selenium.models.BiomarkerVolume;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

public class BiomarkerVolumeForm extends BaseToolForm {

    private static final String VOLUME_LABEL = "//ui-input[@formcontrolname='volume']/input";
    private static final String BIOMARKER_LABEL =
            "//ui-select[@placeholder='-- select biomarker --']//span[contains(@class,'ng-value-label')]";

    public BiomarkerVolumeForm() {
        super(By.xpath("//app-biomarker-volume-control[@formcontrolname='biomarkerVolumes']"),
                "Biomarker volume");
    }

    public List<BiomarkerVolume> getBiomarkerVolumes() {
        List<BiomarkerVolume> biomarkerVolumes = new ArrayList<>();
        waitForBiomarkersLoaded();
        List<IElement> biomarkerLabels = getElementFactory().findElements(By.xpath(BIOMARKER_LABEL), ElementType.LABEL);
        List<IElement> volumeLabels = getElementFactory().findElements(By.xpath(VOLUME_LABEL), ElementType.LABEL);
        for (int i = 0; i < biomarkerLabels.size(); i++) {
            biomarkerVolumes.add(new BiomarkerVolume(
                    biomarkerLabels.get(i).getText(),
                    volumeLabels.get(i).getAttribute(Attributes.VALUE.toString())));
        }

        return biomarkerVolumes;
    }

    private void waitForBiomarkersLoaded() {
        AqualityServices.getConditionalWait().waitFor(
                ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(BIOMARKER_LABEL)));
    }
}
