package diaceutics.selenium.pageobject.forms.testingdashboard;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.enums.charts.UsStates;
import diaceutics.selenium.models.UsState;
import diaceutics.selenium.pageobject.forms.BaseChartsForm;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestingDashboardChartsForm extends BaseChartsForm {

    private static final String STATE_ELEMENT =
            "//*[name()='svg']//*[name()='g' and contains(@class,'highcharts-label')]//*[name()='tspan'][@class='highcharts-text-outline'][text()='%s']";
    private static final String TOOLTIP = "//*[name()='svg']//*[name()='g' and contains(@class,'highcharts-tooltip')]//*[name()='tspan']";

    public TestingDashboardChartsForm() {
        super(By.xpath("//app-charts"), "Testing Dashboard Charts");
    }


    public List<UsState> getUsToolTipsFromUI() {
        List<UsState> usStates = new ArrayList<>();
        for (UsStates state : UsStates.values()) {
            UsState usState = new UsState();
            if(state.getFriendlyName().equals(UsStates.DISTRICT_COLUMBIA.getFriendlyName())){
                usState.setTitle(UsStates.DISTRICT_COLUMBIA.getFriendlyName());
                usState.setValue("5");
                usStates.add(usState);
                continue;
            }
            ILabel stateToolTip = getElementFactory().getLabel(
                    By.xpath(String.format(STATE_ELEMENT, state.getLocator())), state.getFriendlyName());
            stateToolTip.getMouseActions().moveMouseToElement();
            ILabel toolTipText = getElementFactory().getLabel(
                    By.xpath(TOOLTIP), state.getFriendlyName());
            toolTipText.state().waitForDisplayed();
            String title = toolTipText.getText().split(":")[0].trim();
            String value = toolTipText.getText().split(":")[1].trim();
            usState.setReflectionFieldValue(state.getModelField(), title);
            usState.setReflectionFieldValue("value", value);
            usStates.add(usState);
        }
        return usStates;
    }
}
