package diaceutics.selenium.pageobject.forms;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import org.openqa.selenium.By;

import java.util.List;

public class DataToolsHeaderForm extends BaseToolForm {

    private static final String LINK_TEMPLATE = "//div[@class='nav']//a[contains(text(),'%s')]";

    public DataToolsHeaderForm() {
        super(By.xpath("//ui-header//div[@class='header']"), "Header");
    }

    public void clickBy(String linkName) {
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);
        link.clickAndWait();
    }

    public boolean isLinkHighlightedInColor(String linkName, String expectedColor) {
        ILink link = getElementFactory().getLink(By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);
        String actualColorCode = link.getCssValue("color");
        String expectedColorCode = ElementColor.getEnumValue(expectedColor).getColorCode();
        return actualColorCode.equals(expectedColorCode);
    }

    public boolean isLinkAvailable(String tool) {
        List<ILink> optionLinks = getElementFactory().findElements(
                By.xpath(String.format(LINK_TEMPLATE, tool)), ElementType.LINK);
        return !optionLinks.isEmpty();
    }

}
