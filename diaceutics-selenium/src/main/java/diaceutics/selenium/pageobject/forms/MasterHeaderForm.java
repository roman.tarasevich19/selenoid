package diaceutics.selenium.pageobject.forms;

import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import org.openqa.selenium.By;

public class MasterHeaderForm extends BaseToolForm {

    private static final String LINK_TEMPLATE = "//div[contains(@class,'header')]//a[contains(text(),'%s')]";
    private static final String LINK_RIGHT_TOP_CORNER_TEMPLATE = "//div//li//a[contains(text(),'%s')]";
    private static final String CONTEXT_MENU_ITEM = "//ui-context-menu-item/a[text()='%s']";
    private static final String USER_MENU_TEMPLATE = "//div[contains(@class,'dropdown-menu')]//a[contains(text(),'%s')]";
    private final ITextBox locationTextBox = getElementFactory().getTextBox(By.id("location"), "location");
    private final ILink userLink = getElementFactory().getLink(By.cssSelector("div[class*='user-avatar']"), "User");
    private final ILink logoLink = getElementFactory().getLink(By.cssSelector("header img"), "Logo");
    private final ILabel contextMenuLabel = getElementFactory().getLabel(
            By.xpath("//ul[@class='nav']//li//ui-context-menu"), "Context menu");

    public MasterHeaderForm() {
        super(By.xpath("//ui-header//div[@class='header']"), "Marketplace Header");
    }

    public void clickByLink(String linkName) {
        ILink link = getElementFactory().getLink(By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);
        link.clickAndWait();
    }

    public void clickByLinkFromDropdown(String linkName) {
        ILink link = getElementFactory().getLink(By.xpath(String.format(CONTEXT_MENU_ITEM, linkName)), linkName);
        link.clickAndWait();
    }

    public boolean isContextMenuDisplayed() {
        return contextMenuLabel.state().isDisplayed();
    }

    public boolean isLinkHighlightedInColor(String linkName, String expectedColor) {
        ILink link = getElementFactory().getLink(By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);
        String actualColorCode = link.getCssValue("color");
        String expectedColorCode = ElementColor.getEnumValue(expectedColor).getColorCode();
        return actualColorCode.equals(expectedColorCode);
    }

    public void openUserMenu(String menu) {
        userLink.clickAndWait();
        ILink menuLink = getElementFactory().getLink(
                By.xpath(String.format(USER_MENU_TEMPLATE, menu)), menu);
        menuLink.clickAndWait();
    }

    public void clickDXRXLogo() {
        logoLink.click();
        logoLink.clickAndWait();
    }

    public boolean isUserLogin() {
        return userLink.state().waitForDisplayed();
    }

    public void openSearchForm() {
        locationTextBox.click();
    }

    public void clickByLinkOnTheRightTopCorner(String linkName) {
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(LINK_RIGHT_TOP_CORNER_TEMPLATE, linkName)), linkName);
        link.clickAndWait();
    }
}
