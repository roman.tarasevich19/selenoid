package diaceutics.selenium.pageobject.forms;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.*;
import aquality.selenium.forms.Form;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseForm extends Form {

    private static final String ALERT_MESSAGE_TEMPLATE = "//*[contains(text(),\"%s\")]";
    private static final String REQUIRED_FIELD_ALERT_MESSAGE_TEMPLATE = "//li[contains(text(),'%s')]";
    private static final String BUTTON_TEMPLATE = "//button[.//*[contains(text(),'%s')]]";
    private static final String ADD_FILE_LOCATOR = "//input[@type='file']";
    private static final String SPINNER_DESCRIPTION = "//p[contains(text(),\"%s\")]";

    private final IButton searchBtn = getElementFactory().getButton(By.name("search"), "Search");
    public final ILabel labelSmallSpinner = getElementFactory().getLabel(
            By.xpath("//ui-spinner[contains(@class,'small')]"), "Small Spinner");

    public final ILabel labelLargeSpinner = getElementFactory().getLabel(
            By.xpath("//ui-spinner[contains(@class,'large')]"), "Large Spinner");

    protected BaseForm(By locator, String name) {
        super(locator, name);
    }

    public boolean isAlertMessageDisplayed(String message) {
        labelLargeSpinner.state().waitForNotDisplayed();
        ILabel alertLabel = getElementFactory().getLabel(
                By.xpath(String.format(ALERT_MESSAGE_TEMPLATE, message)), message);
        return alertLabel.state().waitForDisplayed();
    }

    public boolean isMessageDisplayedOnRequiredFields(String message) {
        ILabel messageLabel = getElementFactory().getLabel(
                By.xpath(String.format(REQUIRED_FIELD_ALERT_MESSAGE_TEMPLATE, message)), message);
        messageLabel.state().waitForDisplayed();
        return messageLabel.state().isDisplayed();
    }

    public void clickByButton(String buttonName) {
        IButton btn = getElementFactory().getButton(By.xpath(String.format(BUTTON_TEMPLATE, buttonName)), buttonName);
        btn.state().waitForClickable();
        btn.clickAndWait();
        labelLargeSpinner.state().waitForNotDisplayed();
    }

    public void clickByButtonWithoutWait(String buttonName) {
        IButton btn = getElementFactory().getButton(By.xpath(String.format(BUTTON_TEMPLATE, buttonName)), buttonName);
        btn.clickAndWait();
    }

    public List<Boolean> getSpinnerText(List<String> spinnerText) {
        boolean result;
        List<Boolean> resultList = new ArrayList<>();
        for(int i = 0; i < 3; i++) {
            ILabel spinnerDescriptionText = getElementFactory().getLabel(By.xpath(String.format(SPINNER_DESCRIPTION, spinnerText.get(i))), "Spinner description");
            spinnerDescriptionText.state().waitForDisplayed();
            result = spinnerDescriptionText.state().isDisplayed();
            resultList.add(result);
        }
        return resultList;
    }

    public void addFile(String filePath) {
        AqualityServices.getBrowser().getDriver().findElement(By.xpath(ADD_FILE_LOCATOR)).sendKeys(filePath);
    }

    public void clickSearch() {
        searchBtn.clickAndWait();
    }

    public String getIdFromUrl() {
        String url = AqualityServices.getBrowser().getDriver().getCurrentUrl();
        return RegExUtil.regexGetMatchGroup(url, "(?!.*/).+", 0);
    }

    public String getBrowserUrl() {
        return AqualityServices.getBrowser().getDriver().getCurrentUrl();
    }

}
