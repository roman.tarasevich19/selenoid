package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class ReactivateThisAssayForm extends BaseToolForm {

    private static final String BUTTON_TEMPLATE = "//div[@class='activeModal']//button//div[contains(text(),'%s')]";

    public ReactivateThisAssayForm() {
        super(By.xpath("//ui-modal[contains(@class,'open')]//span[.='Reactivate this assay']"),
                "Reactivate this assay form");
    }

    @Override
    public void clickByButton(String buttonName) {
        IButton btn = getElementFactory().getButton(By.xpath(String.format(BUTTON_TEMPLATE, buttonName)), buttonName);
        btn.clickAndWait();
    }
}

