package diaceutics.selenium.pageobject.forms.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class UploadBiomarkerForm extends BaseToolForm {

    public UploadBiomarkerForm() {
        super(By.xpath("//div[contains(@class,'header')]//span[.='Upload biomarkers']"), "Upload biomarkers");
    }
}

