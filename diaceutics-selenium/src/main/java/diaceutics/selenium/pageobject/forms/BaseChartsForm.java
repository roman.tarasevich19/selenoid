package diaceutics.selenium.pageobject.forms;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.IElement;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.enums.charts.ChartsInterface;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class BaseChartsForm extends BaseToolForm {

    private static final String ALL_HIGHCHARTS_TITLE_TEMPLATE = "//div[contains(@class,'titleContainer')]/span[@class='title']";
    private static final String HIGHCHARTS_TITLE_TEMPLATE = "%s" + ALL_HIGHCHARTS_TITLE_TEMPLATE;
    private static final String COUNTRY_CHART_TITLE = "%s" + "//div[contains(@class,'description')]";
    private static final String DOWNLOAD_CHART_BUTTON_TEMPLATE =
            "%s//div[text()='Download']";
    private static final String CHART_SUBTITLE_LABEL =
            "%s//*[name()='svg']//*[name()='text' and contains(@class,'highcharts-subtitle')]//*";

    private static final String EXPORT_DATA_CHART_BUTTON_TEMPLATE =
            "%s//div[contains(@class,'Container')]//*[text()='Export data']";

    private final IButton downloadAllChartsBtn = getElementFactory().getButton(
            By.xpath("//div[contains(text(),'Download all charts')]"), "Download all charts");

    private final ILabel activeFilters = getElementFactory().getLabel(
            By.cssSelector("ui-active-filters span"), "Active filters label");

    public BaseChartsForm(By locator, String name) {
        super(locator, name);
    }

    public void clickDownloadAllCharts() {
        downloadAllChartsBtn.clickAndWait();
    }

    public List<String> getStringListAllHighChartsTitle() {
        List<IElement> titleLabels = getElementFactory().findElements(By.xpath
                (ALL_HIGHCHARTS_TITLE_TEMPLATE), ElementType.LABEL);

        List<String> titles = new ArrayList<>();
        titleLabels.forEach(option -> titles.add(option.getText()));

        return titles;
    }

    public void clickDownloadAChart(ChartsInterface chart) {
        IButton downloadChartsBtn = getElementFactory().getButton(
                By.xpath(String.format(DOWNLOAD_CHART_BUTTON_TEMPLATE, chart.getLocator())),
                String.format("Download %s", chart.getFriendlyName()));

        downloadChartsBtn.clickAndWait();
    }

    public Integer getTotalCountOfLabsFromChartSubTitle(ChartsInterface chart) {
        ILabel totalCountOfLabs = getElementFactory().getLabel(
                By.xpath(String.format(CHART_SUBTITLE_LABEL, chart.getLocator())),
                "Chart subtitle for: " + chart.getFriendlyName());

        return Integer.valueOf(RegExUtil.getNumbersFromString(totalCountOfLabs.getText()));
    }

    public String getChartSubTitle(ChartsInterface chart) {
        List<ILabel> allStringsSubTitle = getElementFactory()
                .findElements(By.xpath(String.format(CHART_SUBTITLE_LABEL, chart.getLocator())), ElementType.LABEL);
        return allStringsSubTitle.stream()
                .map(IElement::getText)
                .collect(Collectors.joining(" "));
    }

    public void clickExportDataChart(ChartsInterface chart) {
        IButton exportDataChartsBtn = getElementFactory().getButton(
                By.xpath(String.format(EXPORT_DATA_CHART_BUTTON_TEMPLATE, chart.getLocator())),
                String.format("Export data %s", chart.getFriendlyName()));

        exportDataChartsBtn.clickAndWait();
    }

    public String getHighchartsTitle(ChartsInterface chart) {
        ILabel chartTitleLabel;
        if (chart.getFriendlyName().startsWith("Country")) {
            chartTitleLabel = getElementFactory().getLabel(
                    By.xpath(String.format(COUNTRY_CHART_TITLE, chart.getLocator())), chart.getFriendlyName());
        } else {
            chartTitleLabel = getElementFactory().getLabel(
                    By.xpath(String.format(HIGHCHARTS_TITLE_TEMPLATE, chart.getLocator())), chart.getFriendlyName());
        }
        return chartTitleLabel.getText();
    }

    public String getActiveLabels() {
        return activeFilters.getText();
    }
}
