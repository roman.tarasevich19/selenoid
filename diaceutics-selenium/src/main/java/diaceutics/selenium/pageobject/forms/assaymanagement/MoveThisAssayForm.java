package diaceutics.selenium.pageobject.forms.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class MoveThisAssayForm extends BaseToolForm {

    public MoveThisAssayForm() {
        super(By.xpath("//div[contains(@class,'header')]//span[.='Move this assay']"), "Move this assay form");
    }
}
