package diaceutics.selenium.pageobject.forms.assaymanagement;

import diaceutics.selenium.enums.grids.Grids;
import diaceutics.selenium.enums.grids.LogOfAssayStatusChangesGridColumns;
import diaceutics.selenium.models.StatusModel;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class LogAssayStatusChangesGridForm extends BaseGridForm {

    public LogAssayStatusChangesGridForm() {
        super(By.xpath("//div[contains(@class,'statusHistoryBody')]/span[text()='Log of assay status changes']"),
                "Log of assay status changes Grid");
    }

    public List<StatusModel> getStatuses() {
        List<StatusModel> statuses = new ArrayList<>();
        int numberOfRows = Integer.parseInt(getNumberOfRowsInGrid(Grids.LOG_OF_ASSAY_STATUS_CHANGES.getLocator()));
        for (int i = 0; i < numberOfRows; i++) statuses.add(new StatusModel());
        for (LogOfAssayStatusChangesGridColumns column : LogOfAssayStatusChangesGridColumns.values()) {
            List<String> columnValues = getValuesFromColumn(column.getColumnName(), Grids.LOG_OF_ASSAY_STATUS_CHANGES.getLocator());
            for (int i = 0; i < numberOfRows; i++) {
                statuses.get(i).setReflectionFieldValue(column.getModelField(), columnValues.get(i));
            }
        }

        return statuses;
    }

}
