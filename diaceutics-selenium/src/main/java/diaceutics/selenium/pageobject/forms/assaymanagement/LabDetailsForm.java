package diaceutics.selenium.pageobject.forms.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class LabDetailsForm extends BaseToolForm {

    public LabDetailsForm() {
        super(By.xpath("//h2[contains(text(),'Lab Details')]"), "Lab Details");
    }

}
