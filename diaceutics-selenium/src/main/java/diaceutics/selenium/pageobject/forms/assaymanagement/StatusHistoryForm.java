package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IElement;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

import java.util.List;

public class StatusHistoryForm extends BaseToolForm {

    private final List<IElement> footerLabels = getElementFactory().findElements(By.xpath
            ("//div[@class='statusHistoryFooter']/div"), ElementType.LABEL);

    public StatusHistoryForm() {
        super(By.xpath("//ui-modal[contains(@class,'open')]//span[.='Status history']"),
                "Status History form");
    }

    public LogAssayStatusChangesGridForm getLogAssayStatusChangesGridForm() {
        return new LogAssayStatusChangesGridForm();
    }

    public String getAssayName() {
        return footerLabels.get(0).getText();
    }

    public String getLabName() {
        return footerLabels.get(1).getText();
    }
}
