package diaceutics.selenium.pageobject.forms;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.utilities.JavaScriptUtil;
import diaceutics.selenium.utilities.ResourceUtil;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ChatForm extends BaseToolForm {

    public ChatForm() {
        super(By.cssSelector("div[class*='home-screen']"), "Chat form");
    }

    private final IButton chatBtn = getElementFactory().getButton(By.xpath("//div[contains(@class,'icon-open')]"), "Chat form open button");
    public final ILabel continueTheConversationSection = getElementFactory().getLabel(
            By.xpath("//h2[text()='Continue the conversation']"),
            "Continue the conversation section");

    public final ILabel startAnotherConversationSection = getElementFactory().getLabel(
            By.xpath("//div[contains(@class,'intercom')][@height='186'][.//h2[text()='Start another conversation']]"),
            "Start another conversation section");

    public final ILabel findYourAnswersSection = getElementFactory().getLabel(By.xpath("//div[contains(@class,'intercom-messenger-card-body')]"),
            "Find your answers section");

    public final IButton seeAllYourConversations = getElementFactory().getButton(By.xpath("//a[text()='See all your conversations']"),
            "See all your conversations");

    public final ILabel yourConversationsLabel = getElementFactory().getLabel(By.xpath("//h2[text()='Your conversations']"),
            "Your conversations label");

    public final IButton backBtn = getElementFactory().getButton(By.xpath("//div[contains(@class,'header-buttons-back')]"),
            "Back button on chat form");

    public final IButton sendUsAMessageBtn = getElementFactory().getButton(By.xpath("//button[contains(@class,'new-conversation')]"),
            "Send us a message");

    public final ITextBox sendMessageInput = getElementFactory().getTextBox(By.xpath("//textarea[@name='message']"),
            "Send message input");

    public final ITextBox sendMessageBtn = getElementFactory().getTextBox(By.xpath("//button[contains(@class,'send-button')]"),
            "Send message button");

    public final ITextBox emojiBtn = getElementFactory().getTextBox(By.xpath("//button[contains(@class,'emoji-button')]"),
            "Emoji button");

    public final IButton uploadBtn = getElementFactory().getButton(By.xpath("//div[contains(@class,'upload-button')]"),
            "Upload button");

    public final IButton findYourAnswersSearchBtn = getElementFactory().getButton(By.cssSelector("button[aria-label='Submit search']"),
            "Find your answers search");

    public final ILabel searchErrorLabel = getElementFactory().getLabel(By.xpath("//div[text()='Please enter a search term']"),
            "Search error label");

    public final ITextBox selectFirstEmojiBtn = getElementFactory().getTextBox(By.xpath("//span[@title='thumbs_up']"),
            "First Emoji");

    public final ILabel emojiILoaded = getElementFactory().getLabel(By.xpath("//div[@role='img'][@aria-label='thumbs up']"),
            "Emoji loaded");

    public final ITextBox gifBtn = getElementFactory().getTextBox(By.xpath("//button[contains(@class,'gif-button')]"),
            "GIF button");

    public final ITextBox selectFirstGifBtn = getElementFactory().getTextBox(By.xpath("//div[@aria-label='Send gif']"),
            "First GIF");

    public final ILabel gifLoaded = getElementFactory().getLabel(By.xpath("//div[contains(@class,'intercom-comment')]//img"),
            "GIF loaded");

    public final ITextBox findYourAnswerInput = getElementFactory().getTextBox(By.xpath("//input[@placeholder='Search our articles']"),
            "Find your answers input");

    public final IButton loadingSpinner = getElementFactory().getButton(By.cssSelector("div[class*='loading-spinner']"),
            "Loading spinner chat form");

    public final ILabel answerForMessage = getElementFactory().getLabel(By.xpath("//div[contains(text(),'You’ll get replies here and in your email:')]"),
            "Answer for message");

    public final IButton continueLastConversationBtn = getElementFactory().getButton(By.cssSelector("div[class*='conversation-summary']"),
            "Continue last conversation label");

    public final IButton weRunOnIntercom = getElementFactory().getButton(By.xpath("//span[text()='We run on Intercom']"),
            "Run on intercom");

    public final ILabel answerForFindYourAnswerSection = getElementFactory().getLabel(
            By.xpath("//div[contains(@class,'intercom-messenger-card')][text()='What is assay management?']"),
            "Answer for find your answer section");

    private static final String INPUT_TEMPLATE = "//body/input[@type='file' and contains(@accept,'%s')]";
    public static final String MESSAGE_SENT = "//div[contains(@class,'intercom-comment')]//div[text()='%s']";

    @Override
    public boolean isDisplayed() {
        continueTheConversationSection.state().waitForDisplayed();
        return super.isDisplayed();
    }

    public void clickOnChatForm() {
        chatBtn.clickAndWait();
        customWait(4000);
    }

    public void switchToChatForm(int index) {
        List<WebElement> frame = AqualityServices.getBrowser().getDriver().findElements(By.cssSelector("iframe"));
        AqualityServices.getBrowser().getDriver().switchTo().frame(frame.get(index));
    }

    public void openSeeYourAllConversations() {
        seeAllYourConversations.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
    }

    public boolean isYourConversationsSectionOpened() {
        return yourConversationsLabel.state().isDisplayed();
    }

    public boolean isStartAnotherConversationPresent() {
        return startAnotherConversationSection.state().isDisplayed();
    }

    public boolean isContinueTheConversationPresent() {
        return continueTheConversationSection.state().isDisplayed();
    }

    public boolean isFindYourAnswerPresent() {
        return findYourAnswersSection.state().isDisplayed();
    }

    public void returnBack() {
        backBtn.clickAndWait();
        customWait(2000);
        loadingSpinner.state().waitForNotDisplayed();
    }

    public void sendUsAMessage() {
        sendUsAMessageBtn.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
    }

    public void setUpMessage(String message) {
        sendMessageInput.clearAndType(message);
        loadingSpinner.state().waitForNotDisplayed();
    }

    public void sendMessage() {
        sendMessageBtn.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
        answerForMessage.state().waitForDisplayed();
    }

    public boolean isAnswerPresent() {
        return answerForMessage.state().isDisplayed();
    }

    public boolean isSendMessagePresent() {
        List<ILabel> messageLabel =
                getElementFactory().findElements(By.xpath("//button[contains(@class,'new-conversation')]"), ElementType.LABEL);
        return !messageLabel.isEmpty();
    }

    public boolean isMessageSent(String message) {
        ILabel mes = getElementFactory().getLabel(By.xpath(String.format(MESSAGE_SENT, message)), "Message sent");
        customWait(2000);
        return mes.state().isDisplayed();
    }

    public void continueLastConversation() {
        continueLastConversationBtn.clickAndWait();
        emojiILoaded.state().waitForDisplayed();
    }

    public boolean isUserCanSeeAllHisPreviousConversations() {
        List<ILabel> conversations = getElementFactory().findElements(By.cssSelector("div[class*='conversation-summary']"), ElementType.LABEL);
        return conversations.size() > 3;
    }

    public void selectGIF() {
        gifBtn.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
        selectFirstGifBtn.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
    }

    public void selectEmoji() {
        emojiBtn.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
        selectFirstEmojiBtn.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
    }

    public void uploadFileOnChat(String fileName) {
        uploadBtn.clickAndWait();
        String extension = FilenameUtils.getExtension(fileName);
        WebElement chooseFile = AqualityServices.getBrowser().getDriver().findElement(
                By.xpath(String.format(INPUT_TEMPLATE, extension)));

        JavaScriptUtil.makeElementStyleVisible(chooseFile);
        chooseFile.sendKeys(ResourceUtil.getResourceUrl(fileName));
        gifLoaded.state().waitForDisplayed();
    }

    public boolean isDocumentUploaded(String fileName) {
        return gifLoaded.getAttribute("src").contains(fileName);
    }

    public void runOnIntercom() {
        weRunOnIntercom.clickAndWait();
    }

    public boolean isGifSent() {
        return gifLoaded.state().isDisplayed();
    }

    public boolean isEmojiSent() {
        return emojiILoaded.state().isDisplayed();
    }

    public boolean isSearchErrorAppears() {
        return searchErrorLabel.state().isDisplayed();
    }

    public void findYourAnswer(String answer) {
        findYourAnswerInput.clearAndType(answer);
    }

    public boolean isAnswerForFindYourAnswerPresent() {
        return answerForFindYourAnswerSection.state().isDisplayed();
    }

    public void findYourAnswersSearch() {
        findYourAnswersSearchBtn.getJsActions().scrollIntoView();
        findYourAnswersSearchBtn.clickAndWait();
        loadingSpinner.state().waitForNotDisplayed();
        customWait(2000);
    }
}
