package diaceutics.selenium.pageobject.forms.marketplace;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;


public class CollaborationHeaderForm extends BaseToolForm {

    public CollaborationHeaderForm() {
        super(By.xpath("//ul[contains(@class,'nav-fill')]"), "Collaboration Header");
    }
}
