package diaceutics.selenium.pageobject.forms;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import diaceutics.selenium.enums.table.TableFilter;
import diaceutics.selenium.utilities.JavaScriptUtil;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public abstract class BaseAGGridForm extends BaseToolForm {

    protected static final String COLUMN_TEMPLATE = "//div[@col-id='%s' and not(contains(@class,'ag-header-cell'))]";
    protected static final String COLUMN_TOOLTIP_TEXT = "//ng-component[contains(@class,'tooltip')]//span";
    protected static final String COLUMN_TITLE_TEMPLATE = "//div[@col-id='%s' and (contains(@class,'ag-header-cell'))]";
    protected static final String FILTER_TABLE_CHECKBOX = "//ag-grid-angular//div[contains(@class,'column-select-column')][.//span[text()='%s']]";
    protected static final String FILTER_TABLE_CHECKBOX_TEMPLATE = FILTER_TABLE_CHECKBOX + "//input";
    protected static final String FILTER_COLUMN_CHECKBOX = "//ag-grid-angular//div[contains(@class,'set-filter-item')][./div[text()='%s']]";
    protected static final String FILTER_COLUMN_CHECKBOX_TEMPLATE = FILTER_COLUMN_CHECKBOX + "//input";
    protected static final String COLUMN_PRESENT = "//span[@role='columnheader'][text()='%s']";
    protected static final String LIST_ITEM = "//div[contains(@class,'list-item')]//span[text()='%s']";
    protected static final String MENU_OPTION = "//div[contains(@class,'menu-option')]//span[text()='%s']";
    protected static final String ROW_TEMPLATE = "//div[@ref='eBodyViewport']//div[@role='row']";
    protected static final String CLEAR_FIELD_ICON = "%s//span[@title='Clear all']";
    protected static final String SORTED_BY_ICON = COLUMN_TITLE_TEMPLATE + "//span[contains(@class,'ag-icon-%s')]";
    protected static final String NUMBER_REGEX = "[-]?[0-9]+(.[0-9]+)?";
    protected static final String LAST_ROW = "//div[@role='rowgroup']//div[contains(@class,'ag-row-level-1') and contains(@class,'ag-row-last')]";
    protected static final String ROW_LEVEL_ONE = "//div[@row-index='%s'][contains(@class,'level-1')]";
    protected static final String ROW_LEVEL_ONE_СELL = ROW_LEVEL_ONE + COLUMN_TEMPLATE;

    protected final ILabel table = getElementFactory().getLabel(
            By.cssSelector("div[role='grid']"), "Table rows");
    protected final ITextBox input = getElementFactory().getTextBox(
            By.cssSelector("div[class='ag-filter-body'] input"), "Filter body input");

    protected final WebElement horizontalScrollBar = AqualityServices.getBrowser().getDriver().findElement(
            By.className("ag-body-horizontal-scroll-viewport"));

    private static final String FILTER_MENU = "//div[contains(@class,'ag-header-cell')][.//input[@aria-label='%s Filter Input']]//span";
    private static final String FILTER_INPUT = "//input[@aria-label='Search filter values']";
    private static final String COLUMN_MENU = "//div[contains(@class,'ag-header-cell')][.//span[text()='%s']]//span[contains(@class,'icon-menu')]";

    protected BaseAGGridForm(By locator, String name) {
        super(locator, name);
    }

    public void openFilterColumn(FormFieldInterface field) {
        int count = 0;
        IButton filterMenu = getElementFactory().getButton(
                By.xpath(String.format(FILTER_MENU, field.getFriendlyName())), "Filter Menu");
        while (!filterMenu.state().isDisplayed() && count < 16) {
            JavaScriptUtil.scrollHorizontalBarToRight(horizontalScrollBar);
            count++;
        }
        filterMenu.click();
    }

    public void clearField(FormFieldInterface field) {
        IButton clear = getElementFactory().getButton(By.xpath(String.format(CLEAR_FIELD_ICON, field.getLocator())), "Clear icon");
        clear.clickAndWait();
    }

    public void openColumnMenu(FormFieldInterface field) {
        int count = 0;
        IButton menu = getElementFactory().getButton(By.xpath(String.format(COLUMN_MENU, field.getFriendlyName())), "Menu");
        while (!menu.state().isDisplayed() && count < 20) {
            JavaScriptUtil.scrollHorizontalBarToRight(horizontalScrollBar);
            count++;
        }
        menu.click();
    }

    public void clickOnColumnTitle(FormFieldInterface field) {
        IButton title = getElementFactory().getButton(By.xpath(String.format(COLUMN_TITLE_TEMPLATE, field.getLocator())), "Column title");
        title.clickAndWait();
    }

    public boolean sortIconForColumnIsPresent(FormFieldInterface field, String sortType) {
        IButton sortIcon = getElementFactory().getButton(By.xpath(String.format(SORTED_BY_ICON, field.getLocator(), sortType)), "Sort icon");
        return sortIcon.state().isDisplayed();
    }

    public boolean readColumnAndCompareWithFilter(String type, List<String> valuesFromColumn, String filter) {
        TableFilter mode = TableFilter.getEnumValue(type);
        boolean isCorrect = false;
        for (String valueFromColumn : valuesFromColumn) {
            switch (mode) {
                case CONTAINS:
                    isCorrect = valueFromColumn.contains(filter);
                    break;
                case NOT_CONTAINS:
                    isCorrect = !valueFromColumn.contains(filter);
                    break;
                case EQUALS:
                    isCorrect = valueFromColumn.equals(filter);
                    break;
                case NOT_EQUAL:
                    isCorrect = !valueFromColumn.equals(filter);
                    break;
                case STARTS_WITH:
                    isCorrect = valueFromColumn.startsWith(filter);
                    break;
                case ENDS_WITH:
                    isCorrect = valueFromColumn.endsWith(filter);
                    break;
                default:
                    AqualityServices.getLogger().info("This filter does not exist");
                    break;
            }
        }
        return isCorrect;
    }

    public void setUpFilterInput(String value) {
        ITextBox filterMenu = getElementFactory().getTextBox(
                By.xpath(FILTER_INPUT), "Filter Input");
        filterMenu.clearAndType(value);
    }

    public void openFilterListItem() {
        IButton listItem = getElementFactory().getButton(
                By.cssSelector("span[class*='ag-icon-small-down']"), "List items filter");
        listItem.clickAndWait();
    }

    public List<String> getFilterListItem() {
        List<ILabel> items = getElementFactory().findElements(
                By.xpath("//div[contains(@class,'select-list-item')]//span"), ILabel.class);
        List<String> result = new ArrayList<>();
        items.forEach(item -> result.add(item.getText()));
        return result;
    }

    public List<String> getMenuOptionListItem() {
        List<ILabel> items = getElementFactory().findElements(
                By.xpath("//span[contains(@class,'menu-option-text')]"), ILabel.class);
        List<String> result = new ArrayList<>();
        items.forEach(item -> result.add(item.getText()));
        return result;
    }

    public boolean gridIsReady() {
        IButton grid = getElementFactory().getButton(By.cssSelector("div[class='ag-center-cols-container']"), "Grid rows");
        return !grid.getCssValue("height").equals("1px");
    }

    public void waitForGridReady() {
        AqualityServices.getConditionalWait().waitFor(this::gridIsReady);
    }

    public List<String> getStringListValuesFromColumn(FormFieldInterface field) {
        waitForGridReady();
        scrollToColumn(field);
        return getElementFactory().findElements(
                By.xpath(String.format(COLUMN_TEMPLATE, field.getLocator())), ElementType.LABEL)
                .stream().map(IElement::getText).collect(Collectors.toList());
    }

    protected Double getDoubleValueFromString(String text) {
        return Double.valueOf(RegExUtil.regexGetMatchGroup(text, NUMBER_REGEX, 0));
    }

    public List<String> getStringListValuesForLevelOneRowFromColumn(FormFieldInterface field) {
        waitForGridReady();
        scrollToColumn(field);
        List<String> resultList = new ArrayList<>();
        int lastRowIndex = Integer.parseInt(getElementFactory().getLabel(By.xpath(LAST_ROW),
                "Last level-1 row").getAttribute("row-index"));
        for (int i = 1; i <= lastRowIndex; i++) {
            try {
                AqualityServices.getConditionalWait().waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(ROW_LEVEL_ONE_СELL, i, field.getLocator()))), Duration.ofMillis(300));
                ILabel rowCell = getElementFactory().getLabel(By.xpath(String.format(ROW_LEVEL_ONE_СELL, i, field.getLocator())), "Row cell");
                    resultList.add(String.valueOf(getDoubleValueFromString(rowCell.getText())));
            } catch (Exception exception) {
                AqualityServices.getLogger().info("Row cell with index " + i);
            }
        }
        return resultList;
    }

    public boolean isValuesInColumnSortedBy(FormFieldInterface field, String sortBy) {
        List<String> results = getStringListValuesForLevelOneRowFromColumn(field);
        results.forEach(System.out::println);
        boolean isSorted = false;
        switch (sortBy) {
            case "asc" :
                List<String> ascSort = new ArrayList<>(results);
                Collections.sort(ascSort);
                isSorted = results.equals(ascSort);
                break;
            case "desc" :
                List<String> descSort = new ArrayList<>(results);
                descSort.sort(Collections.reverseOrder());
                isSorted = results.equals(descSort);
                break;
            default:
                break;
        }
        return isSorted;
    }

    public boolean tableIsEmpty() {
        List<ILabel> rowLabels = getElementFactory().findElements(By.xpath(ROW_TEMPLATE), ElementType.LINK);
        return rowLabels.isEmpty();
    }

    public boolean isColumnPresent(String column) {
        List<ILabel> columns = getElementFactory().findElements(By.xpath(String.format(COLUMN_PRESENT, column)), ElementType.LABEL);
        return !columns.isEmpty();
    }

    public String getColumnTooltip(FormFieldInterface field) {
        scrollToColumn(field);
        ILabel label = getElementFactory().getLabel(By.xpath(String.format(COLUMN_TITLE_TEMPLATE, field.getLocator())), field.getFriendlyName());
        label.getMouseActions().moveMouseToElement();
        ILabel tooltip = getElementFactory().getLabel(By.xpath(COLUMN_TOOLTIP_TEXT), "Column tooltip");
        return tooltip.getText();
    }

    public void setFieldValueTableFilter(String fieldName, Boolean value) {
        ICheckBox checkBox = getElementFactory().getCheckBox(
                By.xpath(String.format(FILTER_TABLE_CHECKBOX, fieldName)), fieldName);
        checkBox.getJsActions().scrollIntoView();

        boolean shouldBeChecked = value;
        boolean isChecked = AqualityServices.getBrowser().getDriver().findElement(
                By.xpath(String.format(FILTER_TABLE_CHECKBOX_TEMPLATE, fieldName))).isSelected();

        if (shouldBeChecked != isChecked) {
            checkBox.check();
        }
    }

    public void setFieldValueColumnCheckboxFilter(String fieldName, Boolean value) {
        ICheckBox checkBox = getElementFactory().getCheckBox(
                By.xpath(String.format(FILTER_COLUMN_CHECKBOX, fieldName)), fieldName);
        boolean shouldBeChecked = value;
        boolean isChecked = AqualityServices.getBrowser().getDriver().findElement(
                By.xpath(String.format(FILTER_COLUMN_CHECKBOX_TEMPLATE, fieldName))).isSelected();

        if (shouldBeChecked != isChecked) {
            checkBox.check();
        }
    }

    public boolean isCheckBoxOptionPresent(String fieldName) {
        ICheckBox checkBox = getElementFactory().getCheckBox(
                By.xpath(String.format(FILTER_COLUMN_CHECKBOX, fieldName)), fieldName);
        return checkBox.state().isDisplayed();
    }

    public void selectFilterType(FormFieldInterface type) {
        IButton selectType = getElementFactory().getButton(
                By.xpath(String.format(LIST_ITEM, type.getLocator())), "Select filter type");
        selectType.clickAndWait();
        AqualityServices.getBrowser().waitForPageToLoad();
    }

    public void selectMenuOption(FormFieldInterface type) {
        IButton selectType = getElementFactory().getButton(
                By.xpath(String.format(MENU_OPTION, type.getLocator())), "Select filter type");
        selectType.clickAndWait();
    }

    public void setFieldValueInputColumnFilter(String value) {
        input.clearAndType(value);
        customWait(4000);
    }

    public String getColumnTitle(FormFieldInterface field) {
        ILabel columnsTitleLabel = getElementFactory().getLabel(By.xpath(
                String.format(COLUMN_TITLE_TEMPLATE, field.getLocator())), field.getFriendlyName());
        return columnsTitleLabel.getText();
    }

    public void scrollToColumn(FormFieldInterface field) {
        ILabel label = getElementFactory().getLabel(By.xpath(
                String.format(COLUMN_TEMPLATE, field.getLocator())), field.getFriendlyName());
        int count = 0;
        while (!label.state().isDisplayed() && count < 10) {
            JavaScriptUtil.scrollHorizontalBarToRight(horizontalScrollBar);
            count++;
        }
    }
}
