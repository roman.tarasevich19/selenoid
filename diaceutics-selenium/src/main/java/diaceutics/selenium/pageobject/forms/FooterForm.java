package diaceutics.selenium.pageobject.forms;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import org.openqa.selenium.By;

public class FooterForm extends BaseToolForm {

   public FooterForm() {
        super(By.cssSelector("footer"), "Footer form");
    }

    private static final String FOOTER_LINK = "//li[.//a[text()='%s']]";
    private final IButton logo = getElementFactory().getButton(
            By.xpath("//div[@class='footerTop']//img"), "Footer logo");

    private final ILabel copyright = getElementFactory().getLabel(
            By.xpath("//span[text()=' © 2021 Diaceutics PLC or its affiliates. All rights reserved. ']"), "Copyright label");


   public void clickOnLink(String link){
       ILink footerLink = getElementFactory().getLink(
               By.xpath(String.format(FOOTER_LINK, link)), String.format("Click on %s footer link", link));
       footerLink.click();
   }

   public boolean isLogoDisplayed(){
       return logo.state().isExist();
   }

   public boolean isCopyRightDisplayed(){
       return copyright.state().isDisplayed();
   }

}
