package diaceutics.selenium.pageobject.forms;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.core.elements.ElementState;
import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.elements.TextDropDown;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import diaceutics.selenium.pageobject.forms.marketplace.InformForm;
import diaceutics.selenium.utilities.JavaScriptUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.Random;

public abstract class BaseMarketplaceForm extends BaseForm {

    public static final String CHECKBOX_TEMPLATE = "//label[@for='%s']";
    public static final String LINK_TEMPLATE = "//a[contains(text(),'%s')]";
    private static final String CHECKBOX_BORDER_TEMPLATE = "//div[.//input[@id='%s']]/label";
    private static final String MESSAGE_TEMPLATE = "//div[contains(@class,'alert')]//*[contains(text(),'%s')]";
    private static final String UPLOADED_FILE_LINK_TEMPLATE = "//span[contains(@class,'message-attachment-item')]//strong[.='%s']";
    private static final String OPTION_TEMPLATE = "//div[@class='tt-suggestion tt-selectable']";
    private static final String BUTTON_TEMPLATE = "//button[text()='%s']";

    private final ILabel alertError = getElementFactory().getLabel(By.xpath("//span[@class='error-link cursor-pointer']"), "Alert window");
    private final IButton nextErrorBtn = getElementFactory().getButton(By.xpath("//i[@class='triangle down']"), "Next error btn");
    private static final String MESSAGE_FOR_REQUIRED_FIELD_TEMPLATE =
            "//div[@class='form-group'][.//label[contains(text(),'%s')]]//label[text()='%s']";

    protected BaseMarketplaceForm(By locator, String name) {
        super(locator, name);
    }

    public InformForm getInformForm() {
        return new InformForm();
    }

    public boolean isMessageForFieldDisplayed(String message, String field) {
        ILabel messageLabel = getElementFactory().getLabel(By.xpath(String.format(MESSAGE_FOR_REQUIRED_FIELD_TEMPLATE, field, message)), field);
        return messageLabel.state().waitForDisplayed();
    }

    public String setFieldValue(FormFieldInterface field, String value) {
        switch (field.getFieldType()) {
            case TEXT:
                if (value == null) {
                    value = "";
                }
                ITextBox textBox = getElementFactory().getTextBox(By.id(field.getLocator()), field.getFriendlyName());
                textBox.clearAndType(value);

                break;

            case TEXT_DROPDOWN:
                TextDropDown textDropDown = new TextDropDown(
                        By.id(field.getLocator()), field.getFriendlyName(), ElementState.DISPLAYED, OPTION_TEMPLATE);

                textDropDown.selectByText(value);
                break;

            case COMBOBOX:
                IComboBox comboBox = getElementFactory().getComboBox(By.id(field.getLocator()), field.getFriendlyName());
                value = value.equals(FieldValues.RANDOM.toString()) ? getRandomValueFromCombobox(comboBox) : value;
                comboBox.selectByText(value);

                break;

            case CHECKBOX:
                ICheckBox checkBox = getElementFactory().getCheckBox(
                        By.xpath(String.format(BaseMarketplaceForm.CHECKBOX_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());

                boolean shouldBeChecked = Boolean.parseBoolean(value);
                if (shouldBeChecked) {
                    checkBox.getJsActions().click();
                }

                break;

            default:
                break;
        }

        return value;
    }

    public String getFieldValue(FormFieldInterface field) {
        String value = null;
        switch (field.getFieldType()) {
            case TEXT:
                ITextBox textBox = getElementFactory().getTextBox(By.id(field.getLocator()), field.getFriendlyName());
                value = textBox.getValue();
                break;

            case COMBOBOX:
                IComboBox comboBox = getElementFactory().getComboBox(By.id(field.getLocator()), field.getFriendlyName());
                value = comboBox.getSelectedText();
                break;

            default:
                break;
        }

        return value;
    }

    public boolean isRedBorderDisplayedForField(FormFieldInterface field) {
        boolean result = false;
        switch (field.getFieldType()) {
            case TEXT:
            case TEXT_DROPDOWN:
                AqualityServices.getConditionalWait().waitFor(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                        By.id(field.getLocator())));

                WebElement fieldBorderLabel = AqualityServices.getBrowser().getDriver().findElement(By.id(field.getLocator()));
                result = JavaScriptUtil.getBorderTopColor(fieldBorderLabel).equals(ElementColor.RED.getColorCode());
                break;

            case CHECKBOX:
                AqualityServices.getConditionalWait().waitFor(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                        By.xpath(String.format(CHECKBOX_BORDER_TEMPLATE, field.getLocator()))));

                WebElement checkboxBorderLabel = AqualityServices.getBrowser().getDriver().findElement(
                        By.xpath(String.format(CHECKBOX_BORDER_TEMPLATE, field.getLocator())));

                result = JavaScriptUtil.getCheckboxBorderColor(checkboxBorderLabel).contains(ElementColor.RED.getColorCode());
                break;
            default:
                break;
        }

        return result;
    }

    public void clickByLink(String linkName) {
        ILink link = getElementFactory().getLink(
                By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);

        link.clickAndWait();
    }

    public String getRandomValueFromCombobox(IComboBox comboBox) {
        List<String> options = comboBox.getTexts();
        int randomIndex = new Random().nextInt(options.size());
        return options.get(randomIndex);
    }

    public boolean isMessageDisplayed(String message) {
        ILabel alertLabel = getElementFactory().getLabel(By.xpath(String.format(MESSAGE_TEMPLATE, message)), message);
        return alertLabel.state().waitForDisplayed();
    }

    public void openNextError() {
        nextErrorBtn.click();
    }

    public boolean isAlertWindowContainsTextError(String error) {
        alertError.state().waitForDisplayed();
        return alertError.getAttribute("innerText").contains(error);
    }

    public boolean isFileUploaded(String fileName) {
        ILink imageLink = getElementFactory().getLink(
                By.xpath(String.format(UPLOADED_FILE_LINK_TEMPLATE, fileName)), fileName);

        return imageLink.state().waitForDisplayed();
    }

    @Override
    public void clickByButton(String buttonName) {
        IButton btn = getElementFactory().getButton(By.xpath(String.format(BUTTON_TEMPLATE, buttonName)), buttonName);
        btn.clickAndWait();
    }
}
