package diaceutics.selenium.pageobject.forms.marketplace;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class QuestionForm extends BaseMarketplaceForm {

    private static final String ERROR_MESSAGE_TEMPLATE = "//div[contains(text(),'%s')]";

    private final ITextBox messageTextBox = getElementFactory().getTextBox(By.id("message_body"), "Message");
    private final IButton sendBtn = getElementFactory().getButton(
            By.xpath("//div[contains(@class,'justify-content-end')]//button[contains(text(),'Send')]"), "Send");

    public QuestionForm() {
        super(By.xpath("//form[contains(@class,'profile-contact-form')]"), "Question form");
    }

    public void putMessage(String message) {
        messageTextBox.clearAndType(message);
    }

    public void clickSendButton() {
        sendBtn.getJsActions().click();
    }

    public boolean isErrorMessageDisplayed(String message) {
        ILabel alertLabel = getElementFactory().getLabel(
                By.xpath(String.format(ERROR_MESSAGE_TEMPLATE, message)), message);
        return alertLabel.state().waitForDisplayed();
    }
}
