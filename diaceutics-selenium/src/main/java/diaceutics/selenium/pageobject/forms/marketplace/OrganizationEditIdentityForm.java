package diaceutics.selenium.pageobject.forms.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class OrganizationEditIdentityForm extends BaseMarketplaceForm {

    public OrganizationEditIdentityForm() {
        super(By.name("organization_edit_identity"), "Organization Edit Identity");
    }
}
