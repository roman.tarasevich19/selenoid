package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.enums.grids.Grids;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;


public class AssaysGridForm extends BaseGridForm {

    private static final String ASSAY_TEMPLATE = "//tr[.//span[.='%s'] and .//span[.='%s'] and .//span[.='%s']]";
    private static final String ASSAY_NAME_TEMPLATE = "//tr[.//span[.='%s']]//td[count(//th[.='Assay name']/preceding-sibling::*)+1]//span";
    private static final String FILTER_TEMPLATE = "//app-lab-test-filters//div[contains(text(),'%s')]";
    private static final String FILTER_COUNT_TEMPLATE = FILTER_TEMPLATE + "/span";

    private final ILabel rowCountLabel = getElementFactory().getLabel(
            By.xpath("//h3[.='Assays']/parent::div/span"), "Row count");

    public AssaysGridForm() {
        super(By.xpath("//h3[.='Assays']"), "Assays Grid");
    }

    public boolean isAssayAdded(String name, String classifications, String method) {
        ILink assayLink = getElementFactory().getLink(
                By.xpath(getAssayLocator(name, classifications, method)), name);

        return assayLink.state().waitForDisplayed();
    }

    public void clickByAssay(String assayName) {
        ILink assayLink = getElementFactory().getLink(By.xpath(String.format(ASSAY_NAME_TEMPLATE, assayName)), assayName);

        assayLink.state().waitForDisplayed();
        assayLink.getJsActions().clickAndWait();
    }

    public String getNumberOfAssaysFromGridHead() {
        return RegExUtil.getNumbersFromString(rowCountLabel.getText());
    }

    public boolean isFilterSelected(String filter) {
        ILink filterLink = getElementFactory().getLink(By.xpath(String.format(FILTER_TEMPLATE, filter)), filter);
        String actualColorCode = filterLink.getCssValue("color");
        String expectedColorCode = ElementColor.GREY.getColorCode();
        return actualColorCode.equals(expectedColorCode);
    }

    public String getNumberOfAssaysFromFilter(String filter) {
        ILink filterLink = getElementFactory().getLink(By.xpath(String.format(FILTER_COUNT_TEMPLATE, filter)), filter);
        return RegExUtil.getNumbersFromString(filterLink.getText());
    }

    public void clickByFilter(String filter) {
        ILink filterLink = getElementFactory().getLink(By.xpath(String.format(FILTER_TEMPLATE, filter)), filter);
        filterLink.clickAndWait();
    }

    public String getValueFromColumnForAssay(String column, String name, String classifications, String method) {
        return getValueFromColumn(Grids.ASSAYS.getLocator(), column, getAssayLocator(name, classifications, method));
    }

    private String getAssayLocator(String name, String classifications, String method) {
        return String.format(ASSAY_TEMPLATE, name, classifications, method);
    }
}
