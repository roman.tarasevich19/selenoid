package diaceutics.selenium.pageobject.forms.labmapping;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.enums.chartlegends.LegendInterface;
import diaceutics.selenium.enums.charts.ChartsInterface;
import diaceutics.selenium.enums.webelementcolors.ChartColor;
import diaceutics.selenium.pageobject.forms.BaseChartsForm;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;


public class LabMappingChartsForm extends BaseChartsForm {

    public static final String PIE_BY_COLOR = "%s//*[name()='path' and contains(@class,'highcharts-point %s')]";
    public static final String LEGEND_TEXT_FROM_PIE_CHART_TEMPLATE = "%s//*[name()='g' and contains(@class,'highcharts-data-label-%s')]";
    public static final String PERCENT_REGEX = "(\\d+\\.\\d+)|(\\d+)";

    public LabMappingChartsForm() {
        super(By.id("chartsTab"), "Lab Mapping Charts");
    }

    public void clickOnPieByColor(ChartsInterface chart, ChartColor color) {
        IButton pieSector = getElementFactory().getButton(
                By.xpath(String.format(PIE_BY_COLOR, chart.getLocator(), color.getPieChartClassLocator())),
                String.format("Pie chart with color %s", color.getColor()));
        pieSector.click();
    }

    public String getNumberOfLabForLegendFromPieChart(ChartsInterface chart, LegendInterface legend) {
        return RegExUtil.getNumbersFromString(getTextForLegendFromPieChart(chart, legend));
    }

    public Double getPercentForLegendFromPieChart(ChartsInterface chart, LegendInterface legend) {
        return Double.valueOf(RegExUtil.regexGetMatchGroup(getTextForLegendFromPieChart(chart, legend), PERCENT_REGEX, 0));
    }

    private String getTextForLegendFromPieChart(ChartsInterface chart, LegendInterface legend) {
        ILabel pieSector = getElementFactory().getLabel(
                By.xpath(String.format(LEGEND_TEXT_FROM_PIE_CHART_TEMPLATE, chart.getLocator(), legend.getLocator())),
                String.format("Legend %s label", legend.getFriendlyName()));
        return pieSector.getText();
    }
}
