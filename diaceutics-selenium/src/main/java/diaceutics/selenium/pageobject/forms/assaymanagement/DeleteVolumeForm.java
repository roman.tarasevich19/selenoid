package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class DeleteVolumeForm extends BaseToolForm {

    private final IButton deleteVolumeBtn = getElementFactory().getButton(
            By.xpath("//button//div[contains(text(),'Delete volume')]"), "Delete volume");

    public DeleteVolumeForm() {
        super(By.xpath("//div[contains(@class,'deleteVolumeModal')]"), "Delete VolumeModal");
    }

    public void clickDeleteVolume() {
        deleteVolumeBtn.clickAndWait();
    }
}
