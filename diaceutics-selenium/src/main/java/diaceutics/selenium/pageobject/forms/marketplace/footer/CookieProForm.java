package diaceutics.selenium.pageobject.forms.marketplace.footer;

import aquality.selenium.elements.interfaces.IButton;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class CookieProForm extends BaseMarketplaceForm {

    private final IButton closeFormBtn = getElementFactory().getButton(
            By.cssSelector("button[class='ot-close-icon']"), "Close form");

    public CookieProForm() {
        super(By.id("ot-pc-title"), "Cookie Pro");
    }

    public void closeForm() {
        closeFormBtn.clickAndWait();
    }
}
