package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class LabTestBiomarkerForm extends BaseToolForm {

    private static final String BIOMARKER_LABEL_TEXT_VALUE = "//ui-select[@formcontrolname='biomarker']//span[contains(@class,'ng-value-label')]";
    private static final String VALID_BIOMARKER_ROW = "//app-biomarker-control-row";
    private static final String BIOMARKER_LABEL_TEMPLATE = "//app-biomarker-control-row[.//span[text()='%s']]";
    private static final String INVALID_BIOMARKER_ROW = "//app-invalid-biomarker-row";
    private static final String LAB_TEST_BUTTON_TEMPLATE = "//*[text()='%s']";
    private static final String REMOVE_BIOMARKER_BUTTON_TEMPLATE = BIOMARKER_LABEL_TEMPLATE +
            "//div[@title='remove biomarker']";

    private static final String REMOVE_VARIANT_BUTTON_TEMPLATE = BIOMARKER_LABEL_TEMPLATE +
            "//div[./span[text()='%s']]//span[.='×']";

    private static final String REMOVE_ALL_VARIANT_BUTTON_TEMPLATE = BIOMARKER_LABEL_TEMPLATE +
            "//span[@title='Clear all']";

    private static final String ALL_VARIANTS_LABEL_TEMPLATE =
            "//ui-select[@placeholder='-- select variants --']//span[contains(@class,'ng-value-label')]";

    private static final String VARIANTS_LABEL_TEMPLATE = BIOMARKER_LABEL_TEMPLATE +
            ALL_VARIANTS_LABEL_TEMPLATE;

    public LabTestBiomarkerForm() {
        super(By.xpath("//app-lab-test-biomarkers-form"), "Add Biomarker form");
    }

    public void clickOnLabTestFormButton(String buttonName) {
        IButton button = getElementFactory().getButton(By.xpath(String.format(LAB_TEST_BUTTON_TEMPLATE, buttonName)), "Button on Lab Test form");
        button.clickAndWait();
    }

    public int getInvalidBiomarkersRow() {
        List<ILabel> invalidBiomarkers = getElementFactory().findElements(By.xpath(INVALID_BIOMARKER_ROW), ElementType.LABEL);
        return invalidBiomarkers.size();
    }

    public String getValidBiomarkersRow() {
        List<ILabel> invalidBiomarkers = getElementFactory().findElements(By.xpath(VALID_BIOMARKER_ROW), ElementType.LABEL);
        return String.valueOf(invalidBiomarkers.size());
    }

    public List<String> getValidBiomarkersFromGrid() {
        labelSmallSpinner.state().waitForNotDisplayed();
        List<ILabel> biomarkers = getElementFactory().findElements(By.xpath(BIOMARKER_LABEL_TEXT_VALUE), ElementType.LABEL);
        return biomarkers.stream().map(ILabel::getText).sorted().collect(Collectors.toList());
    }

    public List<String> getAllVariantsFromAllRows() {
        List<ILabel> variantsLabel = getElementFactory().findElements(
                By.xpath(ALL_VARIANTS_LABEL_TEMPLATE), ElementType.LABEL);
        return variantsLabel.stream().map(ILabel::getText).collect(Collectors.toList());
    }

    public void clickRemoveBiomarker(String biomarker) {
        IButton deleteBiomarkerBtn = getElementFactory().getButton(
                By.xpath(String.format(REMOVE_BIOMARKER_BUTTON_TEMPLATE, biomarker)), "Delete biomarker button");

        deleteBiomarkerBtn.clickAndWait();
    }

    public boolean isBiomarkerDisplayed(String biomarker) {
        ILabel biomarkerLabel = getElementFactory().getLabel(
                By.xpath(String.format(BIOMARKER_LABEL_TEMPLATE, biomarker)), "Biomarker label");

        return biomarkerLabel.state().isDisplayed();
    }

    public List<String> getListVariantsForBiomarker(String biomarker) {
        List<ILabel> variantsLabel = getElementFactory().findElements(
                By.xpath(String.format(VARIANTS_LABEL_TEMPLATE, biomarker)), ElementType.LABEL);

        return variantsLabel.stream().map(ILabel::getText).collect(Collectors.toList());
    }

    public void removeVariantForBiomarker(String variant, String biomarker) {
        IButton removeVariant = getElementFactory().getButton(
                By.xpath(String.format(REMOVE_VARIANT_BUTTON_TEMPLATE, biomarker, variant)), "Remove variant button");

        removeVariant.clickAndWait();
    }

    public void removeAllVariantsForBiomarker(String biomarker) {
        IButton removeAllVariants = getElementFactory().getButton(
                By.xpath(String.format(REMOVE_ALL_VARIANT_BUTTON_TEMPLATE, biomarker)), "Remove all variants button");

        removeAllVariants.clickAndWait();
    }
}
