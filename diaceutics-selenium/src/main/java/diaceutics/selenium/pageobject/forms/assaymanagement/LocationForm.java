package diaceutics.selenium.pageobject.forms.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class LocationForm extends BaseToolForm {

    public LocationForm() {
        super(By.xpath("//h2[contains(text(),'Location')]"), "Location");
    }

}
