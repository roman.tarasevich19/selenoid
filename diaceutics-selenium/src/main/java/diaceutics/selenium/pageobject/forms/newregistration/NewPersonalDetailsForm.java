package diaceutics.selenium.pageobject.forms.newregistration;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.ICheckBox;
import aquality.selenium.elements.interfaces.IComboBox;
import aquality.selenium.elements.interfaces.IRadioButton;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import diaceutics.selenium.pageobject.forms.BaseForm;
import diaceutics.selenium.utilities.JavaScriptUtil;
import diaceutics.selenium.utilities.RegExUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NewPersonalDetailsForm extends BaseForm {

    private static final String TEXT = "//ui-input[.//label[contains(text(),'%s')]]//input";
    private static final String TEXT_AREA = "//ui-text-area[.//label[contains(text(),'%s')]]//textarea";
    private static final String RADIO_OPTION_TEMPLATE = "%s[.//span[contains(text(),'%s')]]";
    public static final String CHECKBOX_TEMPLATE = "//div[@class='privacyPolicy'][.//span[contains(text(),'%s')]]//span";
    private static final String CHECKBOX_BORDER_TEMPLATE = "//div[.//input[@id='%s']]/label";

    public NewPersonalDetailsForm() {
        super(By.id("user_registration_firstName"), "Personal Details");
    }

    public String setFieldValue(FormFieldInterface field, String value) {
        value = value == null ? "" : value;
        switch (field.getFieldType()) {
            case TEXT:
                value = value.contains(FieldValues.RANDOM.toString()) ?
                        RandomStringUtils.randomAlphabetic(Integer.parseInt(RegExUtil.getNumbersFromString(value))) :
                        value;

                ITextBox textInput = getElementFactory().getTextBox(By.xpath
                        (String.format(TEXT, field.getLocator())), field.getFriendlyName());
                textInput.clearAndType(value);

                break;

            case TEXT_AREA:
                ITextBox textArea = getElementFactory().getTextBox(By.xpath
                        (String.format(TEXT_AREA, field.getLocator())), field.getFriendlyName());
                textArea.clearAndType(value);

                break;

            case RADIO:
                IRadioButton radioOption = getElementFactory().getRadioButton(
                        By.xpath(String.format(RADIO_OPTION_TEMPLATE, field.getLocator(), value)),
                        field.getFriendlyName());

                radioOption.click();
                break;

            case CHECKBOX:
                ICheckBox checkBox = getElementFactory().getCheckBox(
                        By.xpath(String.format(CHECKBOX_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());

                boolean shouldBeChecked = Boolean.parseBoolean(value);
                if (shouldBeChecked) {
                    checkBox.getJsActions().click();
                }

                break;

            default:
                break;
        }

        return value;
    }

    public String getFieldValue(FormFieldInterface field) {
        String value = null;
        switch (field.getFieldType()) {
            case TEXT:
                ITextBox textBox = getElementFactory().getTextBox(By.xpath(String.format(TEXT, field.getLocator())), field.getFriendlyName());
                value = textBox.getValue();
                break;

            case COMBOBOX:
                IComboBox comboBox = getElementFactory().getComboBox(By.id(field.getLocator()), field.getFriendlyName());
                value = comboBox.getSelectedText();
                break;

            default:
                break;
        }

        return value;
    }

    public boolean isRedBorderDisplayedForField(FormFieldInterface field) {
        boolean result = false;
        switch (field.getFieldType()) {
            case TEXT:
                AqualityServices.getConditionalWait().waitFor(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                        By.xpath((String.format(TEXT, field.getLocator())))));

                WebElement fieldBorderLabel = AqualityServices.getBrowser().getDriver().findElement(By.xpath((String.format(TEXT, field.getLocator()))));
                result = JavaScriptUtil.getBorderTopColor(fieldBorderLabel).equals(ElementColor.RED_FOR_BORDERS.getColorCode());
                break;

            case CHECKBOX:
                AqualityServices.getConditionalWait().waitFor(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                        By.xpath(String.format(CHECKBOX_BORDER_TEMPLATE, field.getLocator()))));

                WebElement checkboxBorderLabel = AqualityServices.getBrowser().getDriver().findElement(
                        By.xpath(String.format(CHECKBOX_BORDER_TEMPLATE, field.getLocator())));

                result = JavaScriptUtil.getCheckboxBorderColor(checkboxBorderLabel).contains(ElementColor.RED.getColorCode());
                break;
            default:
                break;
        }

        return result;
    }


}
