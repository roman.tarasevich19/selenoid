package diaceutics.selenium.pageobject.forms.marketplace;

import aquality.selenium.elements.interfaces.IComboBox;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class CategoryForm extends BaseMarketplaceForm {

    private final IComboBox categoriesComboBox = getElementFactory().getComboBox(
            By.id("listing_categories_listingListingCategories"), "Categories");

    public CategoryForm() {
        super(By.xpath("//li//span[contains(text(),'Category')]"), "Categories of the collaboration");
    }

    public String getCategory() {
        return categoriesComboBox.getSelectedText().trim();
    }

}
