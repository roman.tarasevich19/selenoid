package diaceutics.selenium.pageobject.forms.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class ChangeAssayStatusForm extends BaseToolForm {

    public ChangeAssayStatusForm() {
        super(By.xpath("//ui-modal[contains(@class,'open')]//span[.='Change assay status']"),
                "Change assay status form");
    }
}
