package diaceutics.selenium.pageobject.forms.marketplace;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class UserEditIdentityForm extends BaseMarketplaceForm {

    public UserEditIdentityForm() {
        super(By.xpath("//h2[.='Identity']"), "Identity");
    }

    public IButton saveBtn = getElementFactory().getButton(By.xpath("//div[@class='text-center']//button[@type='submit']"),
            "Save btn on User Identify Form");
    public ILabel userAccountInfoUpdated = getElementFactory().getLabel(By.xpath("//h6[text()='User account information successfully updated']"),
            "Info was updated");

    public void save(){
        saveBtn.clickAndWait();
    }

    public boolean isUserInfoUpdated(){
       return userAccountInfoUpdated.state().isDisplayed();
    }

}
