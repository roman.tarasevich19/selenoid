package diaceutics.selenium.pageobject.forms.marketplace;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.core.elements.ElementState;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ICheckBox;
import aquality.selenium.elements.interfaces.IComboBox;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.elements.TextDropDown;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SearchForm extends BaseMarketplaceForm {

    private static final String OPTION_TEMPLATE = "//div[@class='pac-item']";
    private final IButton categoriesBtn = getElementFactory().getButton(By.id("categories"), "Categories button");
    private final IButton searchBtn = getElementFactory().getButton(
            By.xpath("//button[contains(text(),'Search')]"), "Search button");

    public SearchForm() {
        super(By.xpath("//div[contains(@class,'card header-search-dropdown')]"), "Search form");
    }

    public void clickSearchButton() {
        searchBtn.clickAndWait();
    }

    public String setFieldValue(FormFieldInterface field, String value) {
        switch (field.getFieldType()) {
            case TEXT:
                ITextBox textBox = getElementFactory().getTextBox(By.id(field.getLocator()), field.getFriendlyName());
                textBox.clearAndType(value);
                break;

            case TEXT_DROPDOWN:
                TextDropDown textDropDown = new TextDropDown(
                        By.id(field.getLocator()), field.getFriendlyName(), ElementState.DISPLAYED, OPTION_TEMPLATE);

                value = textDropDown.selectByText(value);
                break;

            case COMBOBOX:
                IComboBox comboBox = getElementFactory().getComboBox(By.id(field.getLocator()), field.getFriendlyName());
                value = value.equals(FieldValues.RANDOM.toString()) ? getRandomValueFromCombobox(comboBox) : value;
                comboBox.selectByText(value);
                break;

            case CHECKBOX:
                categoriesBtn.clickAndWait();
                ICheckBox checkBox = getElementFactory().getCheckBox(
                        By.xpath(String.format(CHECKBOX_TEMPLATE, field.getLocator())), field.getFriendlyName());
                if (!checkBox.state().isDisplayed()) {
                    categoriesBtn.clickAndWait();
                }
                boolean shouldBeChecked = Boolean.parseBoolean(value);
                if (shouldBeChecked) {
                    checkBox.clickAndWait();
                }
                categoriesBtn.clickAndWait();
                break;

            default:
                break;
        }

        return value;
    }

    public String getFieldValue(FormFieldInterface field) {
        String value = null;
        switch (field.getFieldType()) {
            case TEXT_DROPDOWN:
            case TEXT:
                ITextBox textBox = getElementFactory().getTextBox(By.id(field.getLocator()), field.getFriendlyName());
                value = textBox.getValue();
                break;

            case COMBOBOX:
                IComboBox comboBox = getElementFactory().getComboBox(By.id(field.getLocator()), field.getFriendlyName());
                value = comboBox.getSelectedText();
                break;

            case CHECKBOX:
                categoriesBtn.clickAndWait();
                WebElement checkBox = AqualityServices.getBrowser().getDriver().findElement(
                        By.id(field.getLocator()));
                value = String.valueOf(checkBox.isSelected());
                categoriesBtn.clickAndWait();
                break;

            default:
                break;
        }

        return value;
    }

    public void clearField(FormFieldInterface field) {
        switch (field.getFieldType()) {
            case TEXT_DROPDOWN:
            case TEXT:
                ITextBox textBox = getElementFactory().getTextBox(By.id(field.getLocator()), field.getFriendlyName());
                textBox.clearAndType("");
                break;

            default:
                break;
        }
    }
}
