package diaceutics.selenium.pageobject.forms;

import org.openqa.selenium.By;

public class EditSettingsForm extends BaseToolForm {

    public EditSettingsForm() {
        super(By.xpath("//div[contains(@class,'header')]//span[contains(text(),'Edit settings')]"), "Edit settings");
    }
}
