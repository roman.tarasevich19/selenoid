package diaceutics.selenium.pageobject.forms.physicianmapping;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.ICheckBox;
import aquality.selenium.elements.interfaces.ILink;
import diaceutics.selenium.enums.pagefields.physicianmapping.AGGridPhysicianSummaryFormFields;
import diaceutics.selenium.models.Physician;
import diaceutics.selenium.pageobject.forms.BaseAGGridForm;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AGGridPhysicianSummaryForm extends BaseAGGridForm {

    private static final String ALL_COLUMNS_BOX = "//div[contains(@class,'ag-column-select-header-checkbox')]";
    private static final String ALL_COLUMNS_BOX_INPUT = ALL_COLUMNS_BOX + "//input[@aria-label='Toggle Select All Columns']";

    private final ILink numberOfPhysiciansLink = getElementFactory().getLink(
            By.xpath("//span[@ref='lbRecordCount']"), "Number of Physicians");

    public AGGridPhysicianSummaryForm() {
        super(By.id("physicianMappingResultsContainer"), "Physician Mapping Results");
    }

    public void setValueToAllColumnsBox(Boolean value) {
        ICheckBox checkBox = getElementFactory().getCheckBox(
                By.xpath(ALL_COLUMNS_BOX), "Field all columns box");
        boolean shouldBeChecked = value;
        boolean isChecked = AqualityServices.getBrowser().getDriver().findElement(
                By.xpath(ALL_COLUMNS_BOX_INPUT)).isSelected();
        if (shouldBeChecked != isChecked) {
            checkBox.check();
        }
    }

    public List<Physician> getListPhysician() {
        List<Physician> physicianList = new ArrayList<>();

        getStringListValuesFromColumn(AGGridPhysicianSummaryFormFields.PHYSICIAN_NPI).forEach(physicianNPI -> {
            Physician physician = new Physician();
            physicianList.add(physician);
        });

        for (AGGridPhysicianSummaryFormFields field : AGGridPhysicianSummaryFormFields.values()) {
            List<String> columnListValues = getStringListValuesFromColumn(field);
            final int[] valueIndex = {0};
            physicianList.forEach(assay -> {
                assay.setReflectionFieldValue(field.getModelField(), columnListValues.get(valueIndex[0]));
                valueIndex[0]++;
            });
        }
        return physicianList;
    }


    public List<AGGridPhysicianSummaryFormFields> getColumnsToExclude(String field) {
        List<AGGridPhysicianSummaryFormFields> columnsToExclude = new ArrayList<>();
        List<AGGridPhysicianSummaryFormFields> allColumns = new ArrayList<>(Arrays.asList(AGGridPhysicianSummaryFormFields.values()));
        AGGridPhysicianSummaryFormFields column = AGGridPhysicianSummaryFormFields.getEnumValue(field);
        columnsToExclude.add(column);
        allColumns.removeAll(columnsToExclude);
        return allColumns;
    }

    public List<Physician> getFilteredListPhysician(String columnToExclude){
        List<Physician> physicianList = new ArrayList<>();
        List<AGGridPhysicianSummaryFormFields> allColumns = getColumnsToExclude(columnToExclude);
        getStringListValuesFromColumn(allColumns.get(0)).forEach(values -> {
            Physician physician = new Physician();
            physicianList.add(physician);
        });

        for (AGGridPhysicianSummaryFormFields field : allColumns) {
            List<String> columnListValues = getStringListValuesFromColumn(field);
            final int[] valueIndex = {0};
            physicianList.forEach(assay -> {
                assay.setReflectionFieldValue(field.getModelField(), columnListValues.get(valueIndex[0]));
                valueIndex[0]++;
            });
        }
        return physicianList;
    }

    public int getNumberOfPhysicians() {
        String numberOfPhysicians = numberOfPhysiciansLink.getText().replace(",","");
        return Integer.parseInt(numberOfPhysicians);
    }
}
