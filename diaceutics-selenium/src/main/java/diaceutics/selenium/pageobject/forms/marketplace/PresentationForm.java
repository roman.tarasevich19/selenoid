package diaceutics.selenium.pageobject.forms.marketplace;

import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

public class PresentationForm extends BaseMarketplaceForm {

    public PresentationForm() {
        super(By.xpath("//li//span[contains(text(),'Presentation')]"), "Presentation of the collaboration");
    }
}
