package diaceutics.selenium.pageobject.forms.assaymanagement;

import diaceutics.selenium.pageobject.forms.BaseToolForm;
import org.openqa.selenium.By;

public class DeleteAssayForm extends BaseToolForm {

    public DeleteAssayForm() {
        super(By.xpath("//div[@class='delete-assay-modal']"), "Delete assay");
    }

}
