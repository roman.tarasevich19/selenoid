package diaceutics.selenium.pageobject.forms;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.Attributes;
import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.elements.ComboboxJs;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import diaceutics.selenium.utilities.JavaScriptUtil;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;

public abstract class BaseToolForm extends BaseForm {

    public static final String COMBOBOX_JS_TEMPLATE = "%s//span[@class='ng-arrow-wrapper']";
    public static final String LINK_TEMPLATE = "//a[contains(text(),'%s')]";
    private static final String TEXT_TEMPLATE = "//input[..//label[contains(text(),'%s')]]";
    private static final String RADIO_BUTTON_TEMPLATE = "%s//label[contains(@class,'radioOptionContainer')][.//span[text()='%s']]";
    private static final String TEXT_AREA_TEMPLATE = "//ui-text-area[.//label[contains(text(),'%s')]]//textarea";
    private static final String CHECKBOX_TEMPLATE = "//ui-checkbox-group//label[contains(text(),'%s')]";
    private static final String CHECK_CHECKBOX_TEMPLATE = CHECKBOX_TEMPLATE + "/input";
    private static final String CHECK_COMBOBOX_TEMPLATE = "%s//input";
    private static final String SEARCH_FIELD_TEMPLATE = "//ui-search/input[contains(@placeholder,'%s')]";
    private static final String SELECTED_RADIO_BUTTON_TEMPLATE = "%s//label[contains(@class,'selected')]";
    private static final String CLEAR_ALL_BUTTON_TEMPLATE = "%s//span[@title='Clear all']";
    private static final String COMBOBOX_JS_BORDER_TEMPLATE = "%s//div[contains(@class,'ng-select-container')]";
    private static final String RADIO_BORDER_TEMPLATE = "%s//div[@class='options']";
    private static final String TEXT_BORDER_TEMPLATE = "//input[..//label[contains(text(),'%s')]]";

    private final IButton closeBtn = getElementFactory().getButton(By.id("icons/icon-x"), "Close");
    public final ILabel chartsLoadingSpinner = getElementFactory().getLabel(
            By.cssSelector("span[class='highcharts-loading-inner']"), "Loading charts label");

    public final ILabel loading = getElementFactory().getLabel(
            By.cssSelector("app-grid-loading-overlay"), "Loading label");

    private final ILink viewStatusHistoryLink = getElementFactory().getLink(
            By.xpath("//a[@class='statusHistoryButton']"), "Status History Link");

    private final ILabel statusLabel = getElementFactory().getLabel(
            By.xpath("//div[contains(@class,'buttonsToggleWrapper')]"), "Status label");

    protected BaseToolForm(By locator, String name) {
        super(locator, name);
    }

    public String setFieldValue(FormFieldInterface field, String value) {
        switch (field.getFieldType()) {
            case TEXT:
                ITextBox textBox = getElementFactory().getTextBox(
                        By.xpath(String.format(TEXT_TEMPLATE, field.getLocator())), field.getFriendlyName());

                textBox.clearAndType(value);
                break;

            case TEXT_AREA:
                ITextBox textBoxArea = getElementFactory().getTextBox(
                        By.xpath(String.format(TEXT_AREA_TEMPLATE, field.getLocator())), field.getFriendlyName());

                textBoxArea.clearAndType(value);
                break;

            case COMBOBOX_JS:
            case NOT_ACTIVE_COMBOBOX_JS:
                labelSmallSpinner.state().waitForNotDisplayed();
                ComboboxJs comboboxJs = getElementFactory().getCustomElement(
                        ComboboxJs.class, By.xpath(String.format(COMBOBOX_JS_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());

                value = value.equals(FieldValues.RANDOM.toString()) ? comboboxJs.getRandomValue() : value;

                comboboxJs.selectByText(value);
                break;

            case RADIO:
                IRadioButton radioButton = getElementFactory().getRadioButton(
                        By.xpath(String.format(RADIO_BUTTON_TEMPLATE, field.getLocator(), value)),
                        field.getFriendlyName());

                radioButton.click();
                break;

            case CHECKBOX:
                ICheckBox checkBox = getElementFactory().getCheckBox(
                        By.xpath(String.format(CHECKBOX_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());

                boolean shouldBeChecked = Boolean.parseBoolean(value);
                boolean isChecked = AqualityServices.getBrowser().getDriver().findElement(
                        By.xpath(String.format(CHECK_CHECKBOX_TEMPLATE, field.getLocator()))).isSelected();

                if (shouldBeChecked != isChecked) {
                    checkBox.check();
                }

                break;

            case NUMBER:
                ITextBox numberBox = getElementFactory().getTextBox(
                        By.xpath(field.getLocator()), field.getFriendlyName());

                numberBox.clearAndType(value);
                break;

            case BUTTON:
                IButton btn = getElementFactory().getButton(
                        By.xpath(String.format(field.getLocator(), value)), field.getFriendlyName());

                btn.clickAndWait();
                break;

            default:
                break;
        }

        return value;
    }

    public void sendKeys(FormFieldInterface field, Keys key) {
        ITextBox textBox = getElementFactory().getTextBox(
                By.xpath(String.format(TEXT_TEMPLATE, field.getLocator())), field.getFriendlyName());
        textBox.sendKeys(key);
    }

    public void clickByLink(String linkName) {
        ILink link = getElementFactory().getLink(By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);
        link.clickAndWait();
    }

    public boolean isFieldEnabled(FormFieldInterface field) {
        boolean value = false;
        switch (field.getFieldType()) {
            case TEXT:
                ITextBox textBox = getElementFactory().getTextBox(
                        By.xpath(String.format(TEXT_TEMPLATE, field.getLocator())), field.getFriendlyName());

                value = textBox.state().isEnabled();
                break;

            case COMBOBOX_JS:
                ComboboxJs comboboxJs = getElementFactory().getCustomElement(
                        ComboboxJs.class, By.xpath(String.format(CHECK_COMBOBOX_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());

                value = comboboxJs.state().isEnabled();
                break;

            case RADIO:
                IRadioButton radioButton = getElementFactory().getRadioButton(
                        By.xpath(field.getLocator()),
                        field.getFriendlyName());

                value = radioButton.state().isEnabled();
                break;

            case CHECKBOX:
                ICheckBox checkBox = getElementFactory().getCheckBox(
                        By.xpath(String.format(CHECK_CHECKBOX_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());

                value = checkBox.state().isClickable();
                break;

            default:
                break;
        }

        return value;
    }

    public String getFieldValue(FormFieldInterface field) {
        String value = null;
        switch (field.getFieldType()) {
            case TEXT:
                ILabel valueLabel = getElementFactory().getLabel(
                        By.xpath(String.format(TEXT_TEMPLATE, field.getLocator())), field.getFriendlyName());

                value = valueLabel.getAttribute(Attributes.VALUE.toString());
                break;

            case COMBOBOX_JS:
                ComboboxJs comboboxJs = getElementFactory().getCustomElement(
                        ComboboxJs.class, By.xpath(String.format(COMBOBOX_JS_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());
                value = comboboxJs.getSelectedText();
                break;

            case NOT_ACTIVE_COMBOBOX_JS:
                ILabel label = getElementFactory().getLabel(
                        By.xpath(field.getLocator()), field.getFriendlyName());

                value = label.getText();
                break;

            case RADIO:
                IRadioButton radioButton = getElementFactory().getRadioButton(
                        By.xpath(String.format(SELECTED_RADIO_BUTTON_TEMPLATE, field.getLocator())),
                        field.getFriendlyName());

                value = radioButton.getText();
                break;

            default:
                break;
        }

        return value;
    }

    public boolean isRedBorderDisplayedForField(FormFieldInterface field) {
        String fieldBorderLocator = "";
        switch (field.getFieldType()) {
            case COMBOBOX_JS:
                fieldBorderLocator = String.format(COMBOBOX_JS_BORDER_TEMPLATE, field.getLocator());
                break;

            case RADIO:
                fieldBorderLocator = String.format(RADIO_BORDER_TEMPLATE, field.getLocator());
                break;

            case TEXT:
                fieldBorderLocator = String.format(TEXT_BORDER_TEMPLATE, field.getLocator());
                break;

            case NUMBER:
                fieldBorderLocator = field.getLocator();
                break;
            default:
                break;
        }

        WebElement fieldBorderLabel = AqualityServices.getBrowser().getDriver().findElement(By.xpath(fieldBorderLocator));
        String borderColorCode = JavaScriptUtil.getBorderTopColor(fieldBorderLabel);
        return borderColorCode.equals(ElementColor.RED_FOR_BORDERS.getColorCode());
    }

    public void putTextInSearchField(String text, String searchFieldName) {
        ITextBox searchField = getElementFactory().getTextBox(
                By.xpath(String.format(SEARCH_FIELD_TEMPLATE, searchFieldName)), searchFieldName);
        searchField.clearAndType(text);
    }

    public List<String> getListOptionsFromField(FormFieldInterface field) {
        labelSmallSpinner.state().waitForNotDisplayed();
        ComboboxJs comboboxJs = getElementFactory().getCustomElement(
                ComboboxJs.class, By.xpath(String.format(COMBOBOX_JS_TEMPLATE, field.getLocator())),
                field.getFriendlyName());

        return comboboxJs.getStringListAllOptions();
    }

    public void clearField(FormFieldInterface field) {
        labelSmallSpinner.state().waitForNotDisplayed();
        IButton clearBtn = getElementFactory().getButton(
                By.xpath(String.format(CLEAR_ALL_BUTTON_TEMPLATE, field.getLocator())), field.getFriendlyName());

        clearBtn.clickAndWait();
    }

    public void clearTextField(FormFieldInterface field) {
        ITextBox textBox = getElementFactory().getTextBox(
                By.xpath(field.getLocator()), field.getFriendlyName());
        textBox.getElement().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        textBox.sendKeys(Keys.BACK_SPACE);
    }

    public void clickCloseButton() {
        closeBtn.clickAndWait();
    }

    public void clickViewStatusHistoryLink() {
        viewStatusHistoryLink.clickAndWait();
    }

    public String getStatus() {
        return RegExUtil.getLastWordFromString(statusLabel.getAttribute(Attributes.CLASS.toString()));
    }

    public boolean isLinkDisplayed(String linkName) {
        ILink link = getElementFactory().getLink(By.xpath(String.format(LINK_TEMPLATE, linkName)), linkName);
        return link.state().isDisplayed();
    }

    public void customWait(long time) {
        synchronized (AqualityServices.getBrowser().getDriver()) {
            try {
                AqualityServices.getBrowser().getDriver().wait(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
