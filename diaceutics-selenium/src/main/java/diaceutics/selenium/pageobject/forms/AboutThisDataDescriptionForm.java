package diaceutics.selenium.pageobject.forms;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.ILabel;
import org.openqa.selenium.By;
import java.util.List;

public class AboutThisDataDescriptionForm extends BaseModalForm {

    private static final String TEXT_CONTENT_ROW = "//div[@class='dataDescriptionModalContainer']//li";

    public AboutThisDataDescriptionForm(){
        super(By.xpath("//div[@class='modal']//span[text()='About this data subscription']"),
                "About this data description modal form");
    }

    public String getTextContent(){
        StringBuilder stringBuilder = new StringBuilder();
        List<ILabel> textContentRows = getElementFactory().findElements(By.xpath(TEXT_CONTENT_ROW), ElementType.LABEL);
        textContentRows.forEach(e -> stringBuilder.append(e.getText()));
        return stringBuilder.toString();
    }
}
