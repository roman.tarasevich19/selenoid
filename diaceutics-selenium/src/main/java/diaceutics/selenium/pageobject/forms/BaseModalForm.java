package diaceutics.selenium.pageobject.forms;

import aquality.selenium.elements.interfaces.ILabel;
import org.openqa.selenium.By;

public class BaseModalForm extends BaseToolForm {

    private final ILabel modalTitle = getElementFactory().getLabel(By.xpath("//div[@class='modal']//span"), "Modal form title");

    public BaseModalForm(By locator, String name) {
        super(locator, name);
    }

    public String getModalTitle(){
        return modalTitle.getText();
    }

}
