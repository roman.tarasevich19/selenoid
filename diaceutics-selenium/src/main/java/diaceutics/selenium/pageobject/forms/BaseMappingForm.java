package diaceutics.selenium.pageobject.forms;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.enums.grids.AGGridHeadingIcons;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

public abstract class BaseMappingForm extends BaseToolForm {

    public static final String HEADING_ICON_TEMPLATE = "//div[@class='banner'][.//ui-icon[@name='%s']]";
    public static final String HEADING_BANNER_TEXT_TEMPLATE = HEADING_ICON_TEMPLATE + "//span[@class='bannerCount']/span";

    private static final String TAB_LINK_TEMPLATE = "//ul[@class='buttons ng-star-inserted']//span[contains(text(),'%s')]";
    private static final String TEXT_FIELD_TEMPLATE =
            "//div[@class='criteriaItem'][.//span[contains(text(),'%s')]]//span[contains(@class,'criteriaItemValue')]";
    public final IButton returnBackToMainPageBtn = getElementFactory().getButton(
            By.xpath("//ui-post-header//ui-icon"), "Return back to Main page");
    private final ILabel projectDataSummary = getElementFactory().getLabel(
            By.xpath("//span[@class='project-name']"), "Data summary project name");

    protected BaseMappingForm(By locator, String name) {
        super(locator, name);
    }

    public EditSettingsForm getEditSettingsForm() {
        return new EditSettingsForm();
    }

    @Override
    public boolean isDisplayed() {
        try {
            labelLargeSpinner.state().waitForNotDisplayed();
        } catch (TimeoutException exception) {
            AqualityServices.getLogger().info(String.format("Loading Error: %s Page must be refreshed", exception.getMessage()));
            AqualityServices.getBrowser().refresh();
            labelLargeSpinner.state().waitForNotDisplayed();
            AqualityServices.getLogger().info("Grid has been loaded successfully!");
        }
        return super.isDisplayed();
    }

    public void clickTab(String tab) {
        ILink tabLink = getElementFactory().getLink(By.xpath(String.format(TAB_LINK_TEMPLATE, tab)), tab);
        tabLink.clickAndWait();
    }

    public void returnBackToMainPage() {
        returnBackToMainPageBtn.clickAndWait();
    }

    @Override
    public String getFieldValue(FormFieldInterface field) {
        ILabel valueLabel = getElementFactory().getLabel(
                By.xpath(String.format(TEXT_FIELD_TEMPLATE, field.getLocator())), field.getFriendlyName());

        return valueLabel.getText();
    }

    public Integer getNumberFromHeadingBanner(String panel){
        AGGridHeadingIcons icon = AGGridHeadingIcons.getEnumValue(panel);
        ILabel banner = getElementFactory().getLabel(By.xpath(String.format(HEADING_BANNER_TEXT_TEMPLATE, icon.getLocator())), "Heading banner");
        return Integer.valueOf(banner.getText());
    }

    public String getProjectFromDataSummaryPanel(){
        return projectDataSummary.getText();
    }

    public boolean isIconDisplayed(String iconName){
        AGGridHeadingIcons icon = AGGridHeadingIcons.getEnumValue(iconName);
        ILabel uiIcon = getElementFactory().getLabel(By.xpath(String.format(HEADING_ICON_TEMPLATE, icon.getLocator())), "Icon picture");
        return uiIcon.state().isDisplayed();
    }
}
