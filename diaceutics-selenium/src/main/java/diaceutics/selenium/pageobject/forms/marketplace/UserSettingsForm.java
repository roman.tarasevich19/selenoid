package diaceutics.selenium.pageobject.forms.marketplace;

import aquality.selenium.elements.TextBox;
import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import aquality.selenium.elements.interfaces.ITextBox;
import diaceutics.selenium.pageobject.forms.BaseMarketplaceForm;
import org.openqa.selenium.By;

import java.awt.*;

public class UserSettingsForm extends BaseMarketplaceForm {

    public UserSettingsForm() {
        super(By.xpath("//h2[contains(text(),'Change your password')]"), "Settings form");
    }

    private final IButton saveBtn = getElementFactory().getButton(By.xpath("//div[@class='text-center']//button[@type='submit']"),
            "Save");
    private final IButton deleteYourAccount = getElementFactory().getButton(By.xpath("//a[contains(text(),'Delete your account')]"), "Delete your account");
    private final ILabel deleteAccountWindow = getElementFactory().getLabel
            (By.xpath("//div[@class='modal-content']"), "Delete account window");

    private final ITextBox deleteAccountWindowPasswordInput = getElementFactory().getTextBox(
            By.xpath("//div[@class='modal-body']//input"), "Delete account window password input");
    private final IButton cancelBtn = getElementFactory().getButton(By.xpath("//div[@class='modal-dialog']//button[text()='Cancel']"), "Cancel");
    private final IButton okBtn = getElementFactory().getButton(By.xpath("//div[@class='modal-dialog']//button[text()='OK']"), "Ok");
    private final IButton closeFormIcon = getElementFactory().getButton(By.xpath("//button[@class='bootbox-close-button close']"), "Close delete acc form");


    public void deleteYourAccount(){
        deleteYourAccount.clickAndWait();
    }

    public boolean isDeleteAccountWindowOpened(){
        deleteAccountWindow.state().waitForDisplayed();
        return deleteAccountWindow.state().isDisplayed();
    }

    public boolean isDeleteAccountWindowClosed(){
        deleteAccountWindow.state().waitForNotDisplayed();
        return !deleteAccountWindow.state().isDisplayed();
    }

    public void cancel(){
        cancelBtn.state().waitForClickable();
        cancelBtn.click();
    }

    public void save(){
        saveBtn.clickAndWait();
    }

    public void ok(){
        okBtn.state().waitForClickable();
        okBtn.click();
    }

    public void closeForm(){
        closeFormIcon.state().waitForClickable();
        closeFormIcon.click();
    }

    public void fillInConfirmPasswordForDelete(String value){
        deleteAccountWindowPasswordInput.clearAndType(value);
    }

}
