package diaceutics.selenium.pageobject.forms.labmapping;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.*;
import diaceutics.selenium.elements.ComboboxJs;
import diaceutics.selenium.enums.pagefields.labmapping.AGGridLabSummaryFormAssayDetailsFields;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import diaceutics.selenium.enums.pagefields.labmapping.AGGridLabSummaryFormFields;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import diaceutics.selenium.models.Assay;
import diaceutics.selenium.models.Lab;
import diaceutics.selenium.pageobject.forms.BaseAGGridForm;
import diaceutics.selenium.utilities.JavaScriptUtil;
import diaceutics.selenium.utilities.RegExUtil;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


public class AGGridLabSummaryForm extends BaseAGGridForm {

    private static final String ROW_TOOLTIP = "//tooltip[contains(@class,'tooltip')]";
    private static final String ENTRY_TEMPLATE = "//div[@role='row']//*[text()='%s']";
    private static final String LAB_TEMPLATE = "//span[contains(@class,'ag-cell-wrapper')][.//app-lab-name-grouped-cell-renderer//span[.='%s']]";
    private static final String LAB_CONTRACTED_TEMPLATE = LAB_TEMPLATE + "//span[@class='ag-group-contracted']";
    private static final String EXPAND_LAB_ICON = "//div[contains(@class,'level-0')]//span[@class='ag-group-contracted']";
    private static final String LAB_EXPANDED_TEMPLATE = LAB_TEMPLATE + "//span[@class='ag-group-expanded']";
    private static final String LAB_LABEL_TEMPLATE = LAB_TEMPLATE + "//abbr[.='%s']";
    private static final String DMS_LABEL = "//abbr[@title='Disease market share']";
    private static final String LTMS_LABEL = "//abbr[@title='Lab test market share']";
    private static final String MARKET_SHARE_INDEX_TEMPLATE = "//div[@row-index='%s']%s";
    private static final String LAB_NAME_TEMPLATE = "//app-lab-name-grouped-cell-renderer//span";
    private static final String LAB_MS_LABEL_TEMPLATE =
            "//app-lab-name-grouped-cell-renderer[.//a[text()='%s']]//abbr[@title='%s']";

    private static final String TOGGLE_VIEW = "//div[contains(@class,'buttonToggle')][contains(text(),'%s')]";


    private final ILink nextLink = getElementFactory().getLink(By.xpath("//div[@ref='btNext']"), "Next");
    private final IButton collapseLabDetails = getElementFactory().getButton(By.xpath
                    ("//div[contains(@class,'column-group')][.//span[text()='Lab details']]//span[contains(@class,'icon-tree-open')]"),
            "Collapse Lab Details");

    private final ILabel labCountLabel = getElementFactory().getLabel(
            By.xpath("//div[contains(@class,'ag-paging-panel')]//span[@ref='lbRecordCount']"), "lab Count");

    public AGGridLabSummaryForm() {
        super(By.id("labMappingResultsContainer"), "lab Mapping Results");
    }

    @Override
    public String setFieldValue(FormFieldInterface field, String value) {
        labelSmallSpinner.state().waitForNotDisplayed();
        ComboboxJs comboboxJs = getElementFactory().getCustomElement(
                ComboboxJs.class, By.xpath(String.format(COMBOBOX_JS_TEMPLATE, field.getLocator())),
                field.getFriendlyName());

        comboboxJs.selectByText(value);
        return value;
    }

    public void refreshSearchIfTableIsEmpty() {
        if (tableIsEmpty()) {
            AqualityServices.getBrowser().refresh();
            waitForGridReady();
            loading.state().waitForNotDisplayed();
            labelLargeSpinner.state().waitForNotDisplayed();
        }
    }

    public void collapseLabDetails() {
        collapseLabDetails.clickAndWait();
    }

    public boolean isLabDisplayed(Lab lab) {
        ILink labLink = getElementFactory().getLink(By.xpath
                (String.format(LAB_TEMPLATE, lab.getName())), lab.getName());

        return labLink.state().isDisplayed();
    }

    public void clickByContractedLab(String labName) {
        ILink labLink = getElementFactory().getLink(By.xpath
                (String.format(LAB_CONTRACTED_TEMPLATE, labName)), labName);
        labLink.clickAndWait();
    }

    public void clickByName(String labName) {
        ILink labLink = getElementFactory().getLink(By.xpath
                (String.format(ENTRY_TEMPLATE, labName)), labName);
        labLink.clickAndWait();
    }

    public void switchView(String view) {
        ILink labLink = getElementFactory().getLink(By.xpath
                (String.format(TOGGLE_VIEW, view)), view);
        labLink.clickAndWait();
    }

    public void expandAllRows() {
        List<IButton> expandIcon = getElementFactory().findElements(By.xpath(EXPAND_LAB_ICON), ElementType.BUTTON);
        for (int i = 0; i < expandIcon.size(); i++) {
            expandIcon.stream().findFirst().get().getJsActions().clickAndWait();
        }
    }

    public boolean isViewOpenedOnAgGrid(String view) {
        ILink tab = getElementFactory().getLink(By.xpath
                (String.format(TOGGLE_VIEW, view)), view);
        return JavaScriptUtil.getBackgroundColor(tab.getElement()).equals(ElementColor.WHITE.getColorCode());
    }

    public boolean isSortWidgetDisplayed() {
        ILabel sortBy = getElementFactory().getLabel(By.xpath
                (AGGridLabSummaryFormFields.SORT_LAB_BY.getLocator()), "Sort by widget");
        ILabel order = getElementFactory().getLabel(By.xpath
                (AGGridLabSummaryFormFields.ORDER.getLocator()), "Order widget");
        return sortBy.state().isDisplayed() && order.state().isDisplayed();
    }

    public void clickByExpandedLab(String labName) {
        ILink labLink = getElementFactory().getLink(By.xpath
                (String.format(LAB_EXPANDED_TEMPLATE, labName)), labName);
        labLink.clickAndWait();
    }

    public String getColumnFieldValue(FormFieldInterface field) {
        scrollToColumn(field);
        ILabel label = getElementFactory().getLabel(By.xpath(
                String.format(COLUMN_TEMPLATE, field.getLocator())), field.getFriendlyName());
        return label.getText();
    }

    public boolean isLabelDisplayedForLab(String label, Lab lab) {
        List<IElement> labLabel = getElementFactory().findElements(By.xpath
                (String.format(LAB_LABEL_TEMPLATE, lab.getName(), label)), ElementType.LABEL);

        return !labLabel.isEmpty();
    }

    public boolean isDataSortedInDescendingOrderForDMS() {
        List<Double> listDMS = getAllListMarketShareLabels();
        List<Double> tmp = new ArrayList<>(listDMS);
        tmp.sort(Collections.reverseOrder());
        return listDMS.equals(tmp);
    }

    public String getTooltipLabName(String lab){
        ILabel labName = getElementFactory().getLabel(By.xpath(String.format(ENTRY_TEMPLATE, lab)), "lab name");
        labName.getMouseActions().moveMouseToElement();
        ILabel tooltip = getElementFactory().getLabel(By.xpath(ROW_TOOLTIP), "Tooltip for lab name");
        return tooltip.getText();
    }

    public String getTooltipLabel(String label) {
        ILabel firstLabel = null;
        switch (label) {
            case "LTMS":
                firstLabel = getElementFactory().getLabel(By.xpath(LTMS_LABEL), "LTMS label");
                break;
            case "DMS":
                firstLabel = getElementFactory().getLabel(By.xpath(DMS_LABEL), "DMS label");
                break;
            default:
                break;
        }
        Objects.requireNonNull(firstLabel).getMouseActions().moveMouseToElement();
        ILabel tooltip = getElementFactory().getLabel(By.xpath(ROW_TOOLTIP), "Tooltip for lab name");
        return tooltip.getText();
    }

    public boolean isDataSortedInAscendingOrderForDMS() {
        List<Double> listDMS = getAllListMarketShareLabels();
        List<Double> tmp = new ArrayList<>(listDMS);
        Collections.sort(tmp);
        return listDMS.equals(tmp);
    }

    public boolean isDataSortedInDescendingOrderForLTMS() {
        List<Double> listLTMS = getAllListSortedInDescendingOrderForLTMSLabel();
        List<Double> tmp = new ArrayList<>(listLTMS);
        tmp.sort(Collections.reverseOrder());
        return listLTMS.equals(tmp);
    }

    public boolean isDataSortedInAscendingOrderForLTMS() {
        List<Double> listLTMS = getAllListSortedInDescendingOrderForLTMSLabel();
        List<Double> tmp = new ArrayList<>(listLTMS);
        Collections.sort(tmp);
        return listLTMS.equals(tmp);
    }

    public List<Lab> getListLab() {
        List<String> stringListLabName = getStringListLabName();
        List<Lab> labList = new ArrayList<>();
        stringListLabName.forEach(labName -> {
            Lab lab = new Lab();
            lab.setName(labName);
            lab.setAssays(getListAssayFromLab(labName));
            labList.add(lab);
        });

        return labList;
    }

    public String getRandomLabName() {
        ILabel labNameLabel = getElementFactory().getLabel(By.xpath(LAB_NAME_TEMPLATE), "Lab name label");
        return labNameLabel.getText();
    }

    public String getMSValueForLab(String labName, String marker) {
        ILabel label = getElementFactory().getLabel(
                By.xpath(String.format(LAB_MS_LABEL_TEMPLATE, labName, marker)), marker + labName);

        return label.state().isDisplayed() ?
                RegExUtil.regexGetMatchGroup(label.getText(), "[^a-zA-Z]+", 0).trim() :
                "0.00%";
    }

    private List<Double> getAllListSortedInDescendingOrderForLTMSLabel() {
        List<Double> doubleList = new ArrayList<>();
        int labCount = Integer.parseInt(labCountLabel.getText().replace(",", ""));
        for (int i = 0; i < labCount; i++) {
            ILabel label = getElementFactory().getLabel(
                    By.xpath(String.format(MARKET_SHARE_INDEX_TEMPLATE, i, LTMS_LABEL)), "label");

            if (!nextLink.state().isEnabled()) {
                break;
            }

            if (!label.state().isDisplayed()) {
                nextLink.clickAndWait();
            }

            if (label.state().isDisplayed()) {
                doubleList.add(getDoubleValueFromString(label.getText()));
            }
        }

        return doubleList;
    }

    private List<Double> getAllListMarketShareLabels() {
        List<Double> doubleList = new ArrayList<>();
        switchToMarketShareLabel();
        do {
            doubleList.addAll(getValuesFromMarketShareLabels());
            nextLink.clickAndWait();
        } while (nextLink.state().isEnabled());

        doubleList.addAll(getValuesFromMarketShareLabels());

        return doubleList;
    }

    private void switchToMarketShareLabel() {
        ILabel msLabel = getElementFactory().getLabel(By.xpath(DMS_LABEL), "label");
        while (!msLabel.state().isDisplayed() && nextLink.state().isEnabled()) {
            nextLink.clickAndWait();
        }
    }

    private List<Double> getValuesFromMarketShareLabels() {
        List<IElement> labels = getElementFactory().findElements(By.xpath(DMS_LABEL), ElementType.LABEL);
        List<Double> doubleList = new ArrayList<>();
        labels.forEach(label -> doubleList.add(getDoubleValueFromString(label.getText())));

        return doubleList;
    }

    private List<String> getStringListLabName() {
        List<String> labNameList = new ArrayList<>();
        List<IElement> labels = getElementFactory().findElements(By.xpath(LAB_NAME_TEMPLATE), ElementType.LABEL);
        labels.forEach(label -> labNameList.add(label.getText()));
        return labNameList;
    }

    private List<Assay> getListAssayFromLab(String labName) {
        clickByContractedLab(labName);
        List<Assay> assayList = new ArrayList<>();

        getStringListValuesFromColumn(AGGridLabSummaryFormAssayDetailsFields.NAME).forEach(assayName -> {
            Assay assay = new Assay();
            assayList.add(assay);
        });

        for (AGGridLabSummaryFormAssayDetailsFields field : AGGridLabSummaryFormAssayDetailsFields.values()) {
            List<String> columnListValues = getStringListValuesFromColumn(field);
            final int[] valueIndex = {0};
            assayList.forEach(assay -> {
                assay.setReflectionFieldValue(field.getModelField(), columnListValues.get(valueIndex[0]));
                valueIndex[0]++;
            });
        }

        clickByExpandedLab(labName);

        for (int i = 0; i < 12; i++) {
            JavaScriptUtil.scrollHorizontalBarToLeft(horizontalScrollBar);
        }

        return assayList;
    }

}
