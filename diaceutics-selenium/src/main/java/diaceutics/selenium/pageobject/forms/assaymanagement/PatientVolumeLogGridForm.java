package diaceutics.selenium.pageobject.forms.assaymanagement;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import diaceutics.selenium.enums.grids.Grids;
import diaceutics.selenium.enums.grids.PatientVolumeLogGridColumns;
import diaceutics.selenium.models.Volume;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class PatientVolumeLogGridForm extends BaseGridForm {

    private static final String VOLUME_TEMPLATE =
            "//tr[.//span[.='%s'] and .//span[.='%s'] and .//span[.='%s'] and .//span[.='%s'] and .//span[contains(text(),'%s')]]";

    private static final String EDIT_VOLUME_BUTTON_TEMPLATE = "%s//td[.//span[.='view/edit']]";
    private static final String ARROW_VOLUME_BUTTON_TEMPLATE = "%s//div[contains(@class,'arrow')]";
    private static final String SQUARE_VOLUME_BUTTON_TEMPLATE = "%s//div[contains(@class,'square')]";
    private static final String BIOMARKER_VOLUME_LABEL_TEMPLATE =
            "//div[@class='biomarker'][./div[contains(text(),'%s')] and ./div[contains(text(),'%s')]]";

    private static final String LESS_THAN_FIFTY = "<50";

    public PatientVolumeLogGridForm() {
        super(By.xpath("//h3[.='Patient volume log']"), "Patient volume grid");
    }

    public void clickEditVolume(Volume volume) {
        IButton editVolumeBtn = getButtonForVolume(volume, EDIT_VOLUME_BUTTON_TEMPLATE, "Edit volume");
        editVolumeBtn.clickAndWait();
    }

    public void clickArrowButtonForVolume(Volume volume) {
        IButton editVolumeBtn = getButtonForVolume(volume, ARROW_VOLUME_BUTTON_TEMPLATE, "Arrow volume");
        editVolumeBtn.clickAndWait();
    }

    public void clickSquareButtonForVolume(Volume volume) {
        IButton editVolumeBtn = getButtonForVolume(volume, SQUARE_VOLUME_BUTTON_TEMPLATE, "Square volume");
        editVolumeBtn.clickAndWait();
    }

    public boolean isBiomarkerVolumeAddedInVolumeGrid(String biomarker, String biomarkerVolume) {
        ILabel biomarkerVolumeLabel = getElementFactory().getLabel(By.xpath(
                String.format(BIOMARKER_VOLUME_LABEL_TEMPLATE,
                        biomarker, biomarkerVolume)), biomarker);

        return biomarkerVolumeLabel.state().waitForDisplayed();
    }

    private IButton getButtonForVolume(Volume volume, String locatorTemplate, String buttonName) {
        return getElementFactory().getButton(By.xpath(
                String.format(locatorTemplate, getVolumeLocator(volume))), buttonName);
    }

    public boolean isVolumeDisplayed(Volume volume) {
        return getVolumeLabel(volume).state().waitForDisplayed();
    }

    public boolean isNotDisplayed(Volume volume) {
        return getVolumeLabel(volume).state().waitForNotDisplayed();
    }

    public String getValueFromColumnForVolume(String column, Volume volume) {
        return getValueFromColumn(Grids.PATIENT_VOLUME_LOG.getLocator(), column, getVolumeLocator(volume));
    }

    @Override
    public boolean isDataInColumnInGridSorted(String column, String gridLocator) {
        List<String> list = getValuesFromColumn(column, gridLocator);
        list = column.equals(PatientVolumeLogGridColumns.DISEASE_VOL.toString()) ?
                list.stream().filter(value -> !value.equals(LESS_THAN_FIFTY)).collect(Collectors.toList()) : list;

        return list.stream().sorted().collect(Collectors.toList()).equals(list);
    }

    private ILabel getVolumeLabel(Volume volume) {
        return getElementFactory().getLabel(By.xpath(getVolumeLocator(volume)), "Volume in Grid");
    }

    private String getVolumeLocator(Volume volume) {
        return String.format(
                VOLUME_TEMPLATE,
                volume.getTimePeriod(),
                volume.getDisease(),
                volume.getDiseaseVolume(),
                volume.getBiomarkerVolumes().size(),
                volume.getUpdated());
    }
}
