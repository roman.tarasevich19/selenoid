package diaceutics.selenium.configuration;

public class Configuration {

    public static final String START_URL = "/startUrl";
    public static final String API_URL = "/apiUrl";
    public static final String AUTH_0_API_URL = "/auth0ApiUrl";
    public static final String AUDIENCE_URL = "/audienceUrl";
    public static final String REGISTRATION_UI_URL = "/registrationUiUrl";
    public static final String REGISTRATION_API_URL = "/registrationApiUrl";
    public static final String BACK_OFFICE_URL = "/backOfficeUrl";

    private Configuration() {
    }

    public static String getUrl(String nameValue) {
        return Environment.getCurrentEnvironment().getValue(nameValue).toString();
    }
}
