package diaceutics.selenium.utilities;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class DateUtil {

    private static final Map<String, String> monthsShort = new HashMap<>();

    static {
        monthsShort.put("January", "Jan");
        monthsShort.put("February", "Feb");
        monthsShort.put("March", "Mar");
        monthsShort.put("April", "Apr");
        monthsShort.put("May", "May");
        monthsShort.put("June", "Jun");
        monthsShort.put("July", "Jul");
        monthsShort.put("August", "Aug");
        monthsShort.put("September", "Sept");
        monthsShort.put("October", "Oct");
        monthsShort.put("November", "Nov");
        monthsShort.put("December", "Dec");
    }

    private static final Map<Integer, String> monthsNumber = new HashMap<>();

    static {
        monthsNumber.put(1, "Jan");
        monthsNumber.put(2, "Feb");
        monthsNumber.put(3, "Mar");
        monthsNumber.put(4, "Apr");
        monthsNumber.put(5, "May");
        monthsNumber.put(6, "Jun");
        monthsNumber.put(7, "Jul");
        monthsNumber.put(8, "Aug");
        monthsNumber.put(9, "Sept");
        monthsNumber.put(10, "Oct");
        monthsNumber.put(11, "Nov");
        monthsNumber.put(12, "Dec");
    }

    public static String convertMonth(String month) {
        return monthsShort.get(month);
    }

    public static String convertNumberToMonth(Integer month) {
        return monthsNumber.get(month);
    }

    public static String getCurrentDate(String pattern) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    public static String convertDateFormat(String oldString, String oldPattern, String newPattern) {
        LocalDateTime datetime = LocalDateTime.parse(oldString, DateTimeFormatter.ofPattern(oldPattern));
        return datetime.format(DateTimeFormatter.ofPattern(newPattern));
    }
}
