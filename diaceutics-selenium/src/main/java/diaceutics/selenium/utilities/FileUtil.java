package diaceutics.selenium.utilities;

import aquality.selenium.browser.AqualityServices;
import lombok.experimental.UtilityClass;

import java.io.File;

@UtilityClass
public class FileUtil {

    public static boolean isExistDownloadedFile(String fileName) {
        File file = new File(getDownloadDirPath() + File.separator + fileName);
        return AqualityServices.getConditionalWait().waitFor(file::exists);
    }

    public static String getDownloadDirPath() {
        return AqualityServices.getConfiguration().getBrowserProfile().getDriverSettings().getDownloadDir();
    }
}
