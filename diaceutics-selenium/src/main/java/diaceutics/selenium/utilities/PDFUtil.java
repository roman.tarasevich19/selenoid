package diaceutics.selenium.utilities;

import aquality.selenium.browser.AqualityServices;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;

@UtilityClass
public class PDFUtil {

    @SneakyThrows
    public static String parsePDF(String fileName) {
        PDDocument pddDoc = PDDocument.load(new File(AqualityServices.
                getConfiguration().
                getBrowserProfile().
                getDriverSettings().
                getDownloadDir()
                + File.separator
                + fileName));

        PDFTextStripper reader = new PDFTextStripper();
        String pageText = reader.getText(pddDoc);
        pddDoc.close();

        return pageText;
    }

}
