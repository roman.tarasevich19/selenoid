package diaceutics.selenium.utilities;

import aquality.selenium.browser.AqualityServices;
import com.mailosaur.MailosaurClient;
import com.mailosaur.models.Message;
import com.mailosaur.models.SearchCriteria;
import diaceutics.selenium.enums.propertykeys.PropertyKeys;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UtilityClass
public class MailUtil {
    public static final String MAIL_ADDRESS_PATTERN = "%s.%s@mailosaur.io";
    private static final String APIKEY = System.getProperty(PropertyKeys.API_MAIL_KEY.toString());
    public static final String SERVER_ID = System.getProperty(PropertyKeys.MAIL_SERVER_ID.toString());

    public static String getLinkFromMailWithSubject(String mail, String subject) {
        Message message = getMailMessage(mail, subject);
        return message.text().links().get(0).href();
    }

    public static boolean isMailWithSubjectExist(String mail, String subject) {
        Message message = getMailMessage(mail, subject);
        return subject.equals(message.subject());
    }

    public static String createMailAddress() {
        return String.format(MAIL_ADDRESS_PATTERN, TimeUtil.getTimestamp(), SERVER_ID);
    }

    @SneakyThrows
    private static Message getMailMessage(String mailAddress, String subject) {
        MailosaurClient client = new MailosaurClient(APIKEY);
        SearchCriteria criteria = new SearchCriteria();
        criteria.withSentTo(mailAddress);
        criteria.withSubject(subject);
        return client.messages().get(SERVER_ID, criteria);
    }

    public String getUrlFromGmailMail(String email, String password) {
        String url = "";
        try {
            TimeUnit.SECONDS.sleep(5);
            Properties properties = new Properties();
            properties.put("mail.pop3.host", "pop.gmail.com");
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);
            Store store = emailSession.getStore("pop3s");
            store.connect("pop.gmail.com", email, password);
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);
            javax.mail.Message[] messages = emailFolder.getMessages();
            String result = "";
            if (messages[0].isMimeType("multipart/*")) {
                MimeMultipart mimeMultipart = (MimeMultipart) messages[0].getContent();
                result = getTextFromMimeMultipart(mimeMultipart);
            }
            Pattern pattern = Pattern.compile("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)");
            Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                url =  matcher.group();
            }
            AqualityServices.getLogger().info("URL: " + url);
            messages[0].setFlag(Flags.Flag.DELETED, true);
            emailFolder.close(true);
            store.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart)  throws MessagingException, IOException {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart){
                result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }
}
