package diaceutics.selenium.utilities;


import lombok.experimental.UtilityClass;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class CsvUtil {

    public static List<String> readStoredCsvFileByRows(String path) {
        try (BufferedReader reader = new BufferedReader(
                new FileReader(path))) {
            List<String> list = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
            return list;
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("File %s was not found or cannot be read", path), e);
        }
    }
}
