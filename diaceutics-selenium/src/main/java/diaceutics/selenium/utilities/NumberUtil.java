package diaceutics.selenium.utilities;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NumberUtil {

    public static int getRandomInt(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }
}
