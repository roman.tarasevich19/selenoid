package diaceutics.selenium.utilities;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtil {

    public static String removeExtraCharacters(String line) {
        return line.replace("0xC2", " ")
                .replace("&nbsp;", " ")
                .replace("\t", " ")
                .replace("\n", " ")
                .replace("[\\s]{2,}", " ")
                .trim();
    }
}
