package diaceutics.selenium.utilities;

import lombok.experimental.UtilityClass;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UtilityClass
public class RegExUtil {

    private static final String NUMBERS_REGEX = "\\d+";
    private static final String LAST_WORD_REGEX = "\\b(\\w+)$";

    public static String getNumbersFromString(String line) {
        return RegExUtil.regexGetMatchGroup(line, NUMBERS_REGEX, 0);
    }

    public static String getLastWordFromString(String line) {
        return RegExUtil.regexGetMatchGroup(line, LAST_WORD_REGEX, 0);
    }

    public static String regexGetMatchGroup(String text, String regex, int groupIndex) {
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(groupIndex);
        }
        throw new IllegalArgumentException(String.format("Pattern by %s was not found on %s", regex, text));
    }
}
