package diaceutics.selenium.utilities;

import aquality.selenium.browser.AqualityServices;
import diaceutics.selenium.enums.javascript.JavaScript;
import lombok.experimental.UtilityClass;
import org.openqa.selenium.WebElement;

@UtilityClass
public class JavaScriptUtil {

    public static boolean waitForAngular() {
        return Boolean.parseBoolean(AqualityServices
                .getBrowser().executeScript(JavaScript.WAIT_FOR_ANGULAR.getScript()).toString());
    }

    public static void makeElementStyleVisible(WebElement element) {
        AqualityServices.getBrowser().executeScript(JavaScript.MAKE_ELEMENT_STYLE_VISIBLE.getScript(), element);
    }

    public static void scrollHorizontalBarToRight(WebElement element) {
        AqualityServices.getBrowser().executeScript(JavaScript.SCROLL_HORIZONTAL_BAR_TO_RIGHT.getScript(), element);
    }

    public static void scrollHorizontalBarToLeft(WebElement element) {
        AqualityServices.getBrowser().executeScript(JavaScript.SCROLL_HORIZONTAL_BAR_TO_LEFT.getScript(), element);
    }

    public static String getBorderTopColor(WebElement element) {
        return AqualityServices.getBrowser().executeScript(JavaScript.GET_BORDER_TOP_COLOR.getScript(), element).toString();
    }

    public static String getCheckboxBorderColor(WebElement element) {
        return AqualityServices.getBrowser().executeScript(JavaScript.GET_CHECKBOX_BORDER_TOP_COLOR.getScript(), element).toString();
    }

    public static String getBackgroundColor(WebElement element) {
        return AqualityServices.getBrowser().executeScript(JavaScript.GET_BACKGROUND_COLOR.getScript(), element).toString();
    }
}
