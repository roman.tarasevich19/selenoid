package diaceutics.selenium.utilities;

import aquality.selenium.browser.AqualityServices;

import lombok.experimental.UtilityClass;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

@UtilityClass
public class ExelUtil {

    public static XSSFSheet getXSSFSheet(String fileName, int index) {
        return getWorkbookFromFile(fileName).getSheetAt(index);
    }

    public static String getStringValueFromCell(Cell cell) {
        String value = null;
        switch (cell.getCellType()) {
            case STRING:
                value = cell.getStringCellValue();
                break;
            case NUMERIC:
                value = String.valueOf((long) cell.getNumericCellValue());
                break;
            default:
                break;
        }

        return value;
    }

    public static XSSFWorkbook getWorkbookFromFile(String fileName) {
        try (FileInputStream file = new FileInputStream(
                AqualityServices.
                        getConfiguration().
                        getBrowserProfile().
                        getDriverSettings().
                        getDownloadDir()
                        + File.separator
                        + fileName)) {

            return new XSSFWorkbook(file);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("File %s was not found or cannot be read", fileName), e);
        }

    }

}
