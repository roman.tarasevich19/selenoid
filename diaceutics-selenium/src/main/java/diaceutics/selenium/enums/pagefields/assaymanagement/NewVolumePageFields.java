package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum NewVolumePageFields implements FormFieldInterface {
    YEAR("Year", "//div[./label[contains(text(),'Year')]]", "year", FieldType.COMBOBOX_JS),
    QUARTER("Quarter", "//ui-radio-group[@label='Quarter']", "quarter", FieldType.RADIO),
    DISEASE("Disease", "//div[./label[contains(text(),'Disease')]]", "disease", FieldType.COMBOBOX_JS),
    DISEASE_VOLUME("Disease volume", "Disease volume", "diseaseVolume", FieldType.TEXT),
    BIOMARKER_DETAILS("Biomarker details",
            "//ui-radio-group[@formcontrolname='isBiomarkerVolumes']",
            "biomarkerDetails",
            FieldType.RADIO);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    NewVolumePageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static NewVolumePageFields getEnumValue(String friendlyName) {
        NewVolumePageFields newVolumePageFields = null;
        for (NewVolumePageFields constant : NewVolumePageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                newVolumePageFields = constant;
                break;
            }
        }
        return newVolumePageFields;
    }
}
