package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum UserEditIdentityFormFields implements FormFieldInterface {

    EMAIL("Email", "user_email", "email", FieldType.TEXT),
    TITLE("Title", "user_title", "title", FieldType.COMBOBOX),
    FIRST_NAME("First name", "user_firstName", "firstName", FieldType.TEXT),
    LAST_NAME("Last name", "user_lastName", "lastName", FieldType.TEXT),
    POSITION_WITHIN_THE_ORGANIZATION(
            "Position within the organization",
            "user_profession",
            "positionWithinTheOrganization",
            FieldType.TEXT),

    WEBSITE("Website", "user_website", "website", FieldType.TEXT),
    PHONE("Phone", "user_phone", "phone", FieldType.TEXT),
    COUNTRY("Country", "user_billingAddress_country", "country", FieldType.COMBOBOX),
    CITY("City", "user_billingAddress_city", "city", FieldType.TEXT),
    ZIP("Zip", "user_billingAddress_zip", "zip", FieldType.TEXT),
    STATE("State", "user_billingAddress_state", "state", FieldType.TEXT),
    ADDRESS_STREET_NUMBER_AND_NAME("Address street number and name",
            "user_billingAddress_address",
            "addressStreetNumberAndName",
            FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    UserEditIdentityFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static UserEditIdentityFormFields getEnumValue(String friendlyName) {
        UserEditIdentityFormFields userEditIdentityFormFields = null;
        for (UserEditIdentityFormFields constant : UserEditIdentityFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                userEditIdentityFormFields = constant;
                break;
            }
        }
        return userEditIdentityFormFields;
    }
}
