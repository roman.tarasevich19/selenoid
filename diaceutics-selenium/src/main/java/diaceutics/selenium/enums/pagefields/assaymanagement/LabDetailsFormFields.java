package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LabDetailsFormFields implements FormFieldInterface {
    NAME("Name", "Name", "name", FieldType.TEXT),
    LAB_TYPE("Lab type", "//ui-radio-group[.//label[contains(text(),'Lab type')]]", "labType", FieldType.RADIO),
    OWNERSHIP("Ownership", "//ui-radio-group[.//label[contains(text(),'Ownership')]]", "ownership", FieldType.RADIO);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LabDetailsFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LabDetailsFormFields getEnumValue(String friendlyName) {
        LabDetailsFormFields labDetailsFormFields = null;
        for (LabDetailsFormFields constant : LabDetailsFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                labDetailsFormFields = constant;
                break;
            }
        }
        return labDetailsFormFields;
    }
}
