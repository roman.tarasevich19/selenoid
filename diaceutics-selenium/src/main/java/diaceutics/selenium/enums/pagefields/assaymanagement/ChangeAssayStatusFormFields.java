package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum ChangeAssayStatusFormFields implements FormFieldInterface {
    YEAR_TO("Year To",
            "//div[@class='inactiveModalBody']//ui-select[@placeholder='-- Year --']",
            "yearTo",
            FieldType.COMBOBOX_JS),

    MONTH_TO("Month To",
            "//div[@class='inactiveModalBody']//ui-select[@placeholder='-- Month --']",
            "monthTo",
            FieldType.COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    ChangeAssayStatusFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static ChangeAssayStatusFormFields getEnumValue(String friendlyName) {
        ChangeAssayStatusFormFields changeAssayStatusFormFields = null;
        for (ChangeAssayStatusFormFields constant : ChangeAssayStatusFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                changeAssayStatusFormFields = constant;
                break;
            }
        }
        return changeAssayStatusFormFields;
    }
}
