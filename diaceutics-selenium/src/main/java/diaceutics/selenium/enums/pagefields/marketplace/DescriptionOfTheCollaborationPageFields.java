package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum DescriptionOfTheCollaborationPageFields implements FormFieldInterface {
    TITLE("Title", "new_listing__step_one_translations_en_title", "title", FieldType.TEXT),
    DESCRIPTION("Description",
            "new_listing__step_one_translations_en_description",
            "description",
            FieldType.TEXT),

    ADDITIONAL_INFORMATION("Additional information",
            "new_listing__step_one_translations_en_rules",
            "additionalInformation",
            FieldType.TEXT),

    SELECT_A_CATEGORY("Select a category", "new_listing__step_one_categories", "category", FieldType.COMBOBOX),
    ENTER_A_YOUTUBE_URL("Enter a Youtube URL", "video-add-input", "youtubeUrl", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    DescriptionOfTheCollaborationPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static DescriptionOfTheCollaborationPageFields getEnumValue(String friendlyName) {
        DescriptionOfTheCollaborationPageFields descriptionOfTheCollaborationPageFields = null;
        for (DescriptionOfTheCollaborationPageFields constant : DescriptionOfTheCollaborationPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                descriptionOfTheCollaborationPageFields = constant;
                break;
            }
        }
        return descriptionOfTheCollaborationPageFields;
    }
}
