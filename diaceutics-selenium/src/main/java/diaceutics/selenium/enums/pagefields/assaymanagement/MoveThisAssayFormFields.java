package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum MoveThisAssayFormFields implements FormFieldInterface {

    SELECT_LAB("Select lab", "//div[./label[contains(text(),'Lab to move the assay to')]]",
            "selectLab", FieldType.COMBOBOX_JS),

    I_UNDERSTAND_THIS_ASSAY_WILL_BE_REMOVED("Checkbox agreement",
            "I understand this assay will be moved", "checkbox", FieldType.CHECKBOX);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    MoveThisAssayFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static MoveThisAssayFormFields getEnumValue(String friendlyName) {
        MoveThisAssayFormFields moveThisAssayFormField = null;
        for (MoveThisAssayFormFields constant : MoveThisAssayFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                moveThisAssayFormField = constant;
                break;
            }
        }
        return moveThisAssayFormField;
    }
}
