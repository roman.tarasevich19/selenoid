package diaceutics.selenium.enums.pagefields.newregistration;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum NewPersonalDetailsFormFields implements FormFieldInterface {

    FIRST_NAME("First name","First name","firstName",FieldType.TEXT),
    LAST_NAME("Last name","Last name","lastName",FieldType.TEXT),
    EMAIL("Work email","Work email","email",FieldType.TEXT),
    I_ACCEPT_THE_PRIVACY_POLICY("I read and accept the Privacy Policy",
            "I have read and accept",
            "userRegistrationRgpd", FieldType.CHECKBOX),
    ORGANIZATION_TYPE("Organization type", "//app-registration-type-option", "organizationType", FieldType.RADIO),
    YOUR_JOR_TITLE("Your job title",
            "Your job title",
            "positionWithinTheOrganization",
            FieldType.TEXT),
    YOUR_POSITION("Your position", "Your position", "positionWithinTheOrganization", FieldType.TEXT_AREA),
    ORGANIZATION("Organization", "Organization", "companyName", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    NewPersonalDetailsFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static NewPersonalDetailsFormFields getEnumValue(String friendlyName) {
        NewPersonalDetailsFormFields personalDetailsFormFields = null;
        for (NewPersonalDetailsFormFields constant : NewPersonalDetailsFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                personalDetailsFormFields = constant;
                break;
            }
        }
        return personalDetailsFormFields;
    }
}
