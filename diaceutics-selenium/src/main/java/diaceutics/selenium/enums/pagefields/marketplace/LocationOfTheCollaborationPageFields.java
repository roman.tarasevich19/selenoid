package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LocationOfTheCollaborationPageFields implements FormFieldInterface {
    COUNTRY("Country", "new_listing__step_two_location_country", "country", FieldType.COMBOBOX),
    CITY("City", "new_listing__step_two_location_city", "city", FieldType.TEXT),
    ZIP("Zip", "new_listing__step_two_location_zip", "zip", FieldType.TEXT),
    NUMBER("Number", "new_listing__step_two_location_street_number", "number", FieldType.TEXT),
    ROUTE("Route", "new_listing__step_two_location_route", "route", FieldType.TEXT),
    ADDITIONAL_LOCATION_INFORMATION("Additional location information",
            "new_listing__step_two_location_additional_info",
            "additionalLocationInformation",
            FieldType.TEXT),

    I_AGREE_TO_THE_TERMS_AND_CONDITIONS("I agree to the Terms & Conditions",
            "new_listing__step_two_tac",
            "iAcceptTheTermsAndConditions",
            FieldType.CHECKBOX);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LocationOfTheCollaborationPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LocationOfTheCollaborationPageFields getEnumValue(String friendlyName) {
        LocationOfTheCollaborationPageFields locationOfTheCollaborationPageFields = null;
        for (LocationOfTheCollaborationPageFields constant : LocationOfTheCollaborationPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                locationOfTheCollaborationPageFields = constant;
                break;
            }
        }
        return locationOfTheCollaborationPageFields;
    }
}
