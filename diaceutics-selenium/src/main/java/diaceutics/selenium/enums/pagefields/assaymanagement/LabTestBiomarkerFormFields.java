package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LabTestBiomarkerFormFields implements FormFieldInterface {
    BIOMARKER("Biomarker",
            "//ui-select[@placeholder='-- biomarker --']",
            "biomarkerName",
            FieldType.COMBOBOX_JS),

    VARIANTS("Variants",
            "//ui-select[@placeholder='-- select variants --']",
            "variants",
            FieldType.COMBOBOX_JS),
    SHOULD_BE("Should be",
            "//ui-select[@placeholder='-- select --']",
            "should be",
            FieldType.COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LabTestBiomarkerFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LabTestBiomarkerFormFields getEnumValue(String friendlyName) {
        LabTestBiomarkerFormFields labTestBiomarkerFormFields = null;
        for (LabTestBiomarkerFormFields constant : LabTestBiomarkerFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                labTestBiomarkerFormFields = constant;
                break;
            }
        }
        return labTestBiomarkerFormFields;
    }
}
