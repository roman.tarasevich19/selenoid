package diaceutics.selenium.enums.pagefields.physicianmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum PhysicianMappingPageFields implements FormFieldInterface {
    COUNTRY("Country", "Country", "country", FieldType.TEXT),
    DISEASE("Disease", "Disease", "diseases", FieldType.TEXT),
    BIOMARKER("Biomarker", "Biomarker", "biomarkerName", FieldType.TEXT),
    DATE_RANGE("Date Range", "Date Range", "dateRange", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    PhysicianMappingPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static PhysicianMappingPageFields getEnumValue(String friendlyName) {
        PhysicianMappingPageFields physicianMappingPageFields = null;
        for (PhysicianMappingPageFields constant : PhysicianMappingPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                physicianMappingPageFields = constant;
                break;
            }
        }
        return physicianMappingPageFields;
    }
}
