package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum UserSettingsFormFields implements FormFieldInterface {

    PASSWORD("Password", "user_plainPassword_first", "password", FieldType.TEXT),
    CONFIRM_PASSWORD("Confirm Password", "user_plainPassword_second", "confirmPassword", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    UserSettingsFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static UserSettingsFormFields getEnumValue(String friendlyName) {
        UserSettingsFormFields userEditIdentityFormFields = null;
        for (UserSettingsFormFields constant : UserSettingsFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                userEditIdentityFormFields = constant;
                break;
            }
        }
        return userEditIdentityFormFields;
    }
}
