package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum AssayManagementPageFields implements FormFieldInterface {

    COUNTRY("Country", "//ui-select[@placeholder='Country']", "country", FieldType.COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    AssayManagementPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static AssayManagementPageFields getEnumValue(String friendlyName) {
        AssayManagementPageFields assayManagementPageFields = null;
        for (AssayManagementPageFields constant : AssayManagementPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                assayManagementPageFields = constant;
                break;
            }
        }
        return assayManagementPageFields;
    }
}
