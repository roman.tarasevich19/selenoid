package diaceutics.selenium.enums.pagefields.labmapping;

import diaceutics.selenium.enums.formatvalue.FormatValue;
import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum AGGridLabSummaryFormAssayDetailsFields implements FormFieldInterface {
    NAME("Name", "name", "assayName", FieldType.TEXT, 0, FormatValue.STRING),
    DESCRIPTION("Description", "description", "assayDescription",
            FieldType.TEXT, 1, FormatValue.STRING),

    BIOMARKER("Biomarkers", "readableBiomarkers", "biomarkerName",
            FieldType.TEXT, 2, FormatValue.STRING),

    WHERE_IS_IT_PERFORMED("Where is it performed?", "readableSendout", "inHouseOrSendOut",
            FieldType.TEXT, 3, FormatValue.STRING),

    SEND_OUT_LAB("Send out lab", "readableSendOutLabName", "sendOutLab",
            FieldType.TEXT, 4, FormatValue.STRING),

    DETECTS("Detects germline/somatic alterations", "readableDetectGermlineSomatics", "detects",
            FieldType.TEXT, 5, FormatValue.STRING),

    SPECIMENS_TESTED("Specimen tested", "2", "specimensTested",
            FieldType.TEXT, 6, FormatValue.STRING),

    METHOD("Method", "readableMethod", "method", FieldType.TEXT, 7, FormatValue.STRING),

    METHOD_DESCRIPTION("Method description", "methodDescription", "methodDescription",
            FieldType.TEXT, 8, FormatValue.STRING),

    CLASSIFICATION("Classification", "readableClassificationWithoutRegulatory", "classification",
            FieldType.TEXT, 9, FormatValue.STRING),

    REGULATORY_STATUS("Regulatory status", "readableClassificationRegulatoryStatus", "classifications",
            FieldType.TEXT, 10, FormatValue.STRING),

    COMMERCIAL_ASSAYS("Commercial assays", "readableCommercialAssay", "commercialAssays",
            FieldType.TEXT, 11, FormatValue.STRING),

    TURN_AROUND_TIME("Turnaround time", "readableTat", "turnAroundTime",
            FieldType.TEXT, 12, FormatValue.TAT),

    ONTOLOGY("Ontology", "3", "ontology", FieldType.TEXT, 13, FormatValue.STRING),
    VARIANTS_INCLUDED("Variants included", "readableVariants", "variants",
            FieldType.TEXT, 14, FormatValue.STRING),

    SENSITIVITY("Sensitivity", "readableSensitivity", "sensitivity",
            FieldType.TEXT, 15, FormatValue.PERCENT_DOUBLE),

    SCORING_METHOD("Scoring method", "readableScoringMethodologies", "scoringMethod",
            FieldType.TEXT, 16, FormatValue.STRING),

    REPORT_FORMAT("Report format", "readableResultFormat", "reportFormat",
            FieldType.TEXT, 17, FormatValue.STRING),

    ASSAY_STATUS("Assay status", "status", "status",
            FieldType.TEXT, 18, FormatValue.STRING),

    ACTIVE_FROM("Active from", "statusFrom", "statusFrom",
            FieldType.TEXT, 19, FormatValue.STRING),

    INACTIVE_SINCE("Inactive since", "statusTo", "statusTo",
            FieldType.TEXT, 20, FormatValue.STRING);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;
    private final Integer index;
    private final FormatValue format;

    AGGridLabSummaryFormAssayDetailsFields(String friendlyName,
                                           String locator,
                                           String modelField,
                                           FieldType fieldType,
                                           Integer index,
                                           FormatValue format) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
        this.index = index;
        this.format = format;
    }

    public String formatValueAsField(String value) {
        return format.format(value);
    }

    public static AGGridLabSummaryFormAssayDetailsFields getEnumValue(String friendlyName) {
        AGGridLabSummaryFormAssayDetailsFields agGridLabSummaryFormAssayDetailsFields = null;
        for (AGGridLabSummaryFormAssayDetailsFields constant : AGGridLabSummaryFormAssayDetailsFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                agGridLabSummaryFormAssayDetailsFields = constant;
                break;
            }
        }
        return agGridLabSummaryFormAssayDetailsFields;
    }
}
