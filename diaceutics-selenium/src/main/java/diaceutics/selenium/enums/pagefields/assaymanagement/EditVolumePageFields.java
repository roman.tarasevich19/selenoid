package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum EditVolumePageFields implements FormFieldInterface {
    YEAR("Year", "//div[./label[contains(text(),'Year')]]//span[contains(@class,'ng-value-label')]",
            "year",
            FieldType.NOT_ACTIVE_COMBOBOX_JS),

    QUARTER("Quarter", "//ui-radio-group[@label='Quarter']", "quarter", FieldType.RADIO),
    DISEASE("Disease", "//div[./label[contains(text(),'Disease')]]",
            "disease",
            FieldType.COMBOBOX_JS),

    DISEASE_VOLUME("Disease volume", "Disease volume", "diseaseVolume", FieldType.TEXT),
    BIOMARKER_DETAILS("Biomarker details",
            "//ui-radio-group[@formcontrolname='isBiomarkerVolumes']",
            "biomarkerDetails",
            FieldType.RADIO);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    EditVolumePageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static EditVolumePageFields getEnumValue(String friendlyName) {
        EditVolumePageFields editVolumePageFields = null;
        for (EditVolumePageFields constant : EditVolumePageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                editVolumePageFields = constant;
                break;
            }
        }
        return editVolumePageFields;
    }
}
