package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum YourResponsePageFields implements FormFieldInterface {
    I_ACCEPT_MEMBERSHIP_TERMS_AND_PRIVACY_POLICY("I accept Membership Terms & Privacy Policy",
            "booking_new_tac",
             FieldType.CHECKBOX);

    private final String friendlyName;
    private final String locator;
    private final FieldType fieldType;

    YourResponsePageFields(String friendlyName, String locator,  FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.fieldType = fieldType;
    }

    public static YourResponsePageFields getEnumValue(String friendlyName) {
        YourResponsePageFields yourResponsePageFields = null;
        for (YourResponsePageFields constant : YourResponsePageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                yourResponsePageFields = constant;
                break;
            }
        }
        return yourResponsePageFields;
    }
}
