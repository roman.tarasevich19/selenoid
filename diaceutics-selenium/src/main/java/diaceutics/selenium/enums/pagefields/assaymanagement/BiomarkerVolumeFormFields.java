package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum BiomarkerVolumeFormFields implements FormFieldInterface {
    BIOMARKER("Biomarker",
            "//ui-select[@placeholder='-- select biomarker --']",
            "biomarkerName",
            FieldType.COMBOBOX_JS),

    VOLUME("Volume",
            "//ui-input[@formcontrolname='volume']/input",
            "volume",
            FieldType.NUMBER),

    NEW_BIOMARKER("New Biomarker",
            "//ui-select[@placeholder='-- select biomarker --'][not(.//span[@title='Clear all'])]",
            "biomarkerName",
            FieldType.COMBOBOX_JS),

    NEW_VOLUME("New Volume",
            "//ui-input[@formcontrolname='volume']/input[contains(@class,'ng-untouched')]",
            "volume",
            FieldType.NUMBER),

    COMMON_VOLUME("Common Volume",
            "//ui-input[@formcontrolname='commonVolume']/input",
            "commonVolume",
            FieldType.NUMBER),

    SAME_VOLUME_FOR_EACH_BIOMARKER("Same volume for each biomarker",
            "Same volume for each biomarker",
            "sameVolumeForEachBiomarker",
            FieldType.CHECKBOX);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    BiomarkerVolumeFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static BiomarkerVolumeFormFields getEnumValue(String friendlyName) {
        BiomarkerVolumeFormFields biomarkerVolumeFormFields = null;
        for (BiomarkerVolumeFormFields constant : BiomarkerVolumeFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                biomarkerVolumeFormFields = constant;
                break;
            }
        }
        return biomarkerVolumeFormFields;
    }
}
