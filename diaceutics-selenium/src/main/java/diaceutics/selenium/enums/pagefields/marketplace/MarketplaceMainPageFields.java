package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum MarketplaceMainPageFields implements FormFieldInterface {

    LOCATION("Location", "location", "location", FieldType.TEXT),
    TYPE("Type", "type", "type", FieldType.COMBOBOX),
    KEYWORDS("Keywords", "keywords", "keywords", FieldType.TEXT),
    SPONSORED("Sponsored", "//label[@for='categories_8']", "sponsored", FieldType.CHECKBOX),
    OFFERED("Offered", "//label[@for='categories_9']", "offered", FieldType.CHECKBOX),
    REQUESTED("Requested", "//label[@for='categories_10']", "requested", FieldType.CHECKBOX);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    MarketplaceMainPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static MarketplaceMainPageFields getEnumValue(String friendlyName) {
        MarketplaceMainPageFields marketplaceMainPageFields = null;
        for (MarketplaceMainPageFields constant : MarketplaceMainPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                marketplaceMainPageFields = constant;
                break;
            }
        }
        return marketplaceMainPageFields;
    }
}
