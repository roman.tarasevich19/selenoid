package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum CategoryFormFields implements FormFieldInterface {
    CATEGORY("Category", "listing_categories_listingListingCategories", "category", FieldType.COMBOBOX);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    CategoryFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static CategoryFormFields getEnumValue(String friendlyName) {
        CategoryFormFields categoryFormFields = null;
        for (CategoryFormFields constant : CategoryFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                categoryFormFields = constant;
                break;
            }
        }
        return categoryFormFields;
    }
}
