package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum PersonalDetailsFormFields implements FormFieldInterface {
    ORGANIZATION_TYPE("Organization type", "user_registration_type", "organizationType", FieldType.COMBOBOX),
    COMPANY_NAME("Company name",
            "user_registration_organizationName",
            "companyName",
            FieldType.TEXT_DROPDOWN),

    POSITION_WITHIN_THE_ORGANIZATION("Position within the organization",
            "user_registration_profession",
            "positionWithinTheOrganization",
            FieldType.TEXT),

    TITLE("Title", "user_registration_title", "title", FieldType.COMBOBOX),
    FIRST_NAME("First name", "user_registration_firstName", "firstName", FieldType.TEXT),
    LAST_NAME("Last name", "user_registration_lastName", "lastName", FieldType.TEXT),
    EMAIL("Email", "user_registration_email", "email", FieldType.TEXT),
    CREATE_A_PASSWORD("Create a password",
            "user_registration_plainPassword_first",
            "password",
            FieldType.TEXT),

    REPEAT_YOUR_PASSWORD("Repeat your password",
            "user_registration_plainPassword_second",
            "verification", FieldType.TEXT),

    I_ACCEPT_THE_TERMS_AND_CONDITIONS("I agree to the Membership Terms",
            "user_registration_tac",
            "userRegistrationTac", FieldType.CHECKBOX),

    I_ACCEPT_THE_PRIVACY_POLICY("I accept the Privacy Policy",
            "user_registration_rgpd",
            "userRegistrationRgpd", FieldType.CHECKBOX);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    PersonalDetailsFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static PersonalDetailsFormFields getEnumValue(String friendlyName) {
        PersonalDetailsFormFields personalDetailsFormFields = null;
        for (PersonalDetailsFormFields constant : PersonalDetailsFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                personalDetailsFormFields = constant;
                break;
            }
        }
        return personalDetailsFormFields;
    }
}
