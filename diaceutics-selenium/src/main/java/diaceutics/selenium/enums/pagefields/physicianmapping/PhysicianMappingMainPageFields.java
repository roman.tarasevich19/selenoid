package diaceutics.selenium.enums.pagefields.physicianmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum PhysicianMappingMainPageFields implements FormFieldInterface {
    PROJECT("Project", "//div[./span[contains(text(),'Project')]]", "project", FieldType.COMBOBOX_JS),
    DISEASE("Disease", "//div[./label[contains(text(),'Disease')]]", "diseases", FieldType.COMBOBOX_JS),
    BIOMARKER_ANALOGUE("Biomarker / Analogue",
            "//div[./label[contains(text(),'Biomarker / Analogue')]]",
            "biomarkerName",
            FieldType.COMBOBOX_JS),

    YEAR_FROM("Year From",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date from')]]//ui-select[@formcontrolname='fromYear']",
            "yearFrom",
            FieldType.COMBOBOX_JS),

    MONTH_FROM("Month From",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date from')]]//ui-select[@formcontrolname='fromMonth']",
            "monthFrom",
            FieldType.COMBOBOX_JS),

    YEAR_TO("Year To",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date to')]]//ui-select[@formcontrolname='toYear']",
            "yearTo",
            FieldType.COMBOBOX_JS),

    MONTH_TO("Month To",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date to')]]//ui-select[@formcontrolname='toMonth']",
            "monthTo",
            FieldType.COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    PhysicianMappingMainPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static PhysicianMappingMainPageFields getEnumValue(String friendlyName) {
        PhysicianMappingMainPageFields physicianMappingMainPageFields = null;
        for (PhysicianMappingMainPageFields constant : PhysicianMappingMainPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                physicianMappingMainPageFields = constant;
                break;
            }
        }
        return physicianMappingMainPageFields;
    }
}
