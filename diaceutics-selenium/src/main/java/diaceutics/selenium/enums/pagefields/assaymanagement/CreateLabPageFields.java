package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum CreateLabPageFields implements FormFieldInterface {
    NAME("Name", "Name ", "name", FieldType.TEXT),
    LAB_TYPE("Lab type", "//ui-radio-group[.//label[contains(text(),'Lab type')]]", "labType", FieldType.RADIO),
    OWNERSHIP("Ownership", "//ui-radio-group[.//label[contains(text(),'Ownership')]]", "ownership", FieldType.RADIO);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    CreateLabPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static CreateLabPageFields getEnumValue(String friendlyName) {
        CreateLabPageFields createLabPageFields = null;
        for (CreateLabPageFields constant : CreateLabPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                createLabPageFields = constant;
                break;
            }
        }
        return createLabPageFields;
    }
}
