package diaceutics.selenium.enums.pagefields;

public enum FieldType {
    TEXT,
    TEXT_H1,
    TEXT_H4,
    TEXT_GRID,
    TEXT_AREA,
    TEXT_DROPDOWN,
    COMBOBOX_JS,
    NOT_ACTIVE_COMBOBOX_JS,
    COMBOBOX,
    RADIO,
    NUMBER,
    CHECKBOX,
    BUTTON,
    ASSAY_STATUS,
    YEAR,
    MONTH,
}
