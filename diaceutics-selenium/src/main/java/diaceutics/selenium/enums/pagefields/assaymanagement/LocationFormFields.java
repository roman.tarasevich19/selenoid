package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LocationFormFields implements FormFieldInterface {
    ADDRESS_ONE("Address 1", "Address 1", "addressOne", FieldType.TEXT),
    ADDRESS_TWO("Address 2", "Address 2", "addressTwo", FieldType.TEXT),
    CITY_TOWN("City / Town", "City / Town", "cityTown", FieldType.TEXT),
    REGION("Region", "Region", "region", FieldType.TEXT),
    COUNTRY("Country", "//div[.//label[contains(text(),'Country')]]", "country", FieldType.COMBOBOX_JS),
    POSTAL_CODE("Postal code", "Postal code ", "postalCode", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LocationFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LocationFormFields getEnumValue(String friendlyName) {
        LocationFormFields locationFormFields = null;
        for (LocationFormFields constant : LocationFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                locationFormFields = constant;
                break;
            }
        }
        return locationFormFields;
    }
}
