package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LoginPageFields implements FormFieldInterface {
    EMAIL("Email", "email", "email", FieldType.TEXT),
    PASSWORD("Password", "password", "password", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LoginPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LoginPageFields getEnumValue(String friendlyName) {
        LoginPageFields loginPageFields = null;
        for (LoginPageFields constant : LoginPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                loginPageFields = constant;
                break;
            }
        }
        return loginPageFields;
    }
}
