package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum MyLocationFormFields implements FormFieldInterface {
    COUNTRY("Country", "listing_location_country", "country", FieldType.COMBOBOX),
    CITY("City", "listing_location_city", "city", FieldType.TEXT),
    ZIP("Zip", "listing_location_zip", "zip", FieldType.TEXT),
    NUMBER("Number", "listing_location_street_number", "number", FieldType.TEXT),
    ROUTE("Route", "listing_location_route", "route", FieldType.TEXT),
    ADDITIONAL_LOCATION_INFORMATION("Additional location information",
            "listing_location_additional_info",
            "additionalLocationInformation",
            FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    MyLocationFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static MyLocationFormFields getEnumValue(String friendlyName) {
        MyLocationFormFields myLocationFormFields = null;
        for (MyLocationFormFields constant : MyLocationFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                myLocationFormFields = constant;
                break;
            }
        }
        return myLocationFormFields;
    }
}
