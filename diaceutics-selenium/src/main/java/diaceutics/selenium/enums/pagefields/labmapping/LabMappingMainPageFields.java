package diaceutics.selenium.enums.pagefields.labmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LabMappingMainPageFields implements FormFieldInterface {
    PROJECT("Project", "//div[./span[contains(text(),'Select a project')]]", "project", FieldType.COMBOBOX_JS),
    COUNTRY("Country", "//div[./label[contains(text(),'Country')]]", "country", FieldType.COMBOBOX_JS),
    DISEASE("Disease", "//div[./label[contains(text(),'Disease')]]", "diseases", FieldType.COMBOBOX_JS),
    CRITERIA_TYPE("Criteria type",
            "//ui-radio-group[@formcontrolname='option']",
            "criteriaType",
            FieldType.RADIO),

    BIOMARKER_ANALOGUE("Biomarker/Analog",
            "//div[./label[contains(text(),'Biomarker/Analog')]]",
            "biomarkerName",
            FieldType.COMBOBOX_JS),

    METHOD("Method",
            "//div[./label[contains(text(),'Method')]]",
            "method",
            FieldType.COMBOBOX_JS),

    YEAR_FROM("Year From",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date from')]]//ui-select[@formcontrolname='fromYear']",
            "yearFrom",
            FieldType.COMBOBOX_JS),

    MONTH_FROM("Month From",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date from')]]//ui-select[@formcontrolname='fromMonth']",
            "monthFrom",
            FieldType.COMBOBOX_JS),

    YEAR_TO("Year To",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date to')]]//ui-select[@formcontrolname='toYear']",
            "yearTo",
            FieldType.COMBOBOX_JS),

    MONTH_TO("Month To",
            "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date to')]]//ui-select[@formcontrolname='toMonth']",
            "monthTo",
            FieldType.COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LabMappingMainPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LabMappingMainPageFields getEnumValue(String friendlyName) {
        LabMappingMainPageFields labMappingMainPageFields = null;
        for (LabMappingMainPageFields constant : LabMappingMainPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                labMappingMainPageFields = constant;
                break;
            }
        }
        return labMappingMainPageFields;
    }
}
