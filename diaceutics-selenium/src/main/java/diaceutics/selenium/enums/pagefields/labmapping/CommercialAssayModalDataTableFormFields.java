package diaceutics.selenium.enums.pagefields.labmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum CommercialAssayModalDataTableFormFields implements FormFieldInterface {

    COMMERCIAL_ASSAY("Commercial assay", "Commercial assay", "commercialAssay", FieldType.TEXT),
    DMS("DMS %", "DMS %", "DMS", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    CommercialAssayModalDataTableFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static CommercialAssayModalDataTableFormFields getEnumValue(String friendlyName) {
        CommercialAssayModalDataTableFormFields commercialAssayModalDataTableFormFields = null;
        for (CommercialAssayModalDataTableFormFields constant : CommercialAssayModalDataTableFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                commercialAssayModalDataTableFormFields = constant;
                break;
            }
        }
        return commercialAssayModalDataTableFormFields;
    }
}
