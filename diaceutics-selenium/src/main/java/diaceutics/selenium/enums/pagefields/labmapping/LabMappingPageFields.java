package diaceutics.selenium.enums.pagefields.labmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LabMappingPageFields implements FormFieldInterface {
    COUNTRY("Country", "Country", "country", FieldType.TEXT),
    DISEASE("Disease", "Disease", "diseases", FieldType.TEXT),
    BIOMARKER("Biomarker", "Biomarker", "biomarkerName", FieldType.TEXT),
    METHOD("Method", "Method", "method", FieldType.TEXT),
    DATE_RANGE("Date Range", "Date Range", "dateRange", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LabMappingPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LabMappingPageFields getEnumValue(String friendlyName) {
        LabMappingPageFields labMappingPageFields = null;
        for (LabMappingPageFields constant : LabMappingPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                labMappingPageFields = constant;
                break;
            }
        }
        return labMappingPageFields;
    }
}
