package diaceutics.selenium.enums.pagefields.testingdashboard;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum TestingDashboardPageFields implements FormFieldInterface {
    COUNTRY("Country", "Country", "country", FieldType.TEXT),
    DISEASE("Disease", "Disease", "diseases", FieldType.TEXT),
    BIOMARKER("Biomarker", "Biomarker", "biomarkerName", FieldType.TEXT),
    DATE_RANGE("Date Range", "Date Range", "dateRange", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    TestingDashboardPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static TestingDashboardPageFields getEnumValue(String friendlyName) {
        TestingDashboardPageFields dashboardPageFields = null;
        for (TestingDashboardPageFields constant : TestingDashboardPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                dashboardPageFields = constant;
                break;
            }
        }
        return dashboardPageFields;
    }
}
