package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum PresentationFormFields implements FormFieldInterface {
    TITLE("Title", "listing_translations_en_title", "title", FieldType.TEXT),
    DESCRIPTION("Description",
            "listing_translations_en_description",
            "description",
            FieldType.TEXT),

    ADDITIONAL_INFORMATION("Additional information",
            "listing_translations_en_rules",
            "additionalInformation",
            FieldType.TEXT),

    STATUS("Status",
            "listing_status_status",
            "status",
            FieldType.COMBOBOX);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    PresentationFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static PresentationFormFields getEnumValue(String friendlyName) {
        PresentationFormFields presentationFormFields = null;
        for (PresentationFormFields constant : PresentationFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                presentationFormFields = constant;
                break;
            }
        }
        return presentationFormFields;
    }
}
