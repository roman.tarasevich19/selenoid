package diaceutics.selenium.enums.pagefields.physicianmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum AGGridPhysicianSummaryFormFields implements FormFieldInterface {
    PHYSICIAN_NPI("Physician NPI", "npi", "physicianNPI", FieldType.TEXT, 0),
    ORGANIZATION_AFFILIATION("Organization affiliation",
            "hospitalAffiliation",
            "organizationAffiliation",
            FieldType.TEXT, 1),

    PHYSICIAN_FIRST_NAME("Physician first name",
            "physicianFirstName",
            "physicianFirstName",
            FieldType.TEXT, 2),

    PHYSICIAN_LAST_NAME("Physician last name",
            "physicianLastName",
            "physicianLastName",
            FieldType.TEXT, 3),

    PHYSICIAN_PRIMARY_SPECIALTY("Physician primary specialty",
            "readablePrimarySpecialtyWithGroup",
            "physicianPrimarySpecialty",
            FieldType.TEXT, 4),

    PHYSICIAN_ADDRESS_LINE_1("Physician address line 1",
            "physicianAddressLine1",
            "physicianAddressLine1",
            FieldType.TEXT, 5),

    PHYSICIAN_CITY("Physician city",
            "physicianCity",
            "physicianCity",
            FieldType.TEXT, 6),

    PHYSICIAN_STATE("Physician state",
            "physicianState",
            "physicianState",
            FieldType.TEXT, 7),

    PHYSICIAN_ZIP("Physician ZIP", "physicianZip", "physicianZip", FieldType.TEXT, 8),
    PHYSICIAN_PHONE_NUMBER("Physician phone number",
            "physicianPhoneNumber",
            "physicianPhoneNumber",
            FieldType.TEXT, 9),

    PHYSICIAN_QUINTILE("Physician quintile",
            "diseasePatientCountQuintile",
            "diseasePatientCountQuintile",
            FieldType.TEXT, 10),

    DISEASE_PATIENT_COUNT("Disease patient count",
            "diseasePatientCount",
            "diseasePatientCount",
            FieldType.TEXT, 11),

    DISEASE_TESTING_RATE("N of patients tested",
            "biomarkerPatientCount",
            "diseaseTestingRatePercentage",
            FieldType.TEXT, 12),

    BIOMARKER_TESTING_RATE("Biomarker testing rate",
            "readableBiomarkerTestingRatePercentage",
            "biomarkerTestingRatePercentage",
            FieldType.TEXT, 13),

    PREFERRED_LABORATORY_FOR_DISEASE_TESTING("Preferred laboratory for disease testing",
            "preferredLabName",
            "preferredLabName",
            FieldType.TEXT, 14),

    PREFERRED_LAB_CLASSIFICATION("Preferred laboratory classification",
            "preferredLabClassification",
            "preferredLabClassification",
            FieldType.TEXT, 15),

    PREFERRED_LABORATORY_ADDRESS_LINE_1("Preferred laboratory address line 1",
            "preferredLabAddressLine1",
            "preferredLabAddressLine1",
            FieldType.TEXT, 16),

    PREFERRED_LABORATORY_CITY("Preferred laboratory city",
            "preferredLabCity",
            "preferredLabCity",
            FieldType.TEXT, 17),

    PREFERRED_LABORATORY_STATE("Preferred laboratory state",
            "preferredLabState",
            "preferredLabState",
            FieldType.TEXT, 18),

    PREFERRED_LABORATORY_ZIP("Preferred laboratory ZIP",
            "preferredLabZip",
            "preferredLabZip",
            FieldType.TEXT, 19),

    READABLE_PREFERRED_LAB_OFFERS_BIOMARKER_TESTING("Preferred lab offer required biomarker testing?",
            "readablePreferredLabOffersBiomarkerTesting",
            "readablePreferredLabOffersBiomarkerTesting",
            FieldType.TEXT, 20);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;
    private int index;

    AGGridPhysicianSummaryFormFields(String friendlyName, String locator, String modelField, FieldType fieldType, int index) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
        this.index = index;
    }

    public void setIndex(int index){
        this.index = index;
    }

    public static AGGridPhysicianSummaryFormFields getEnumValue(String friendlyName) {
        AGGridPhysicianSummaryFormFields agGridLabSummaryFormFields = null;
        for (AGGridPhysicianSummaryFormFields constant : AGGridPhysicianSummaryFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                agGridLabSummaryFormFields = constant;
                break;
            }
        }
        return agGridLabSummaryFormFields;
    }
}
