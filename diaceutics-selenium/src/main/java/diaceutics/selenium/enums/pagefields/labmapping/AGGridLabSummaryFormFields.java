package diaceutics.selenium.enums.pagefields.labmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum AGGridLabSummaryFormFields implements FormFieldInterface {
    SORT_LAB_BY("Sort lab by", "//div[./label[contains(text(),'Sort by:')]]", FieldType.NOT_ACTIVE_COMBOBOX_JS),
    ORDER("Order", "//div[./ui-select[@formcontrolname='sortLabByOrder']]", FieldType.NOT_ACTIVE_COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final FieldType fieldType;

    AGGridLabSummaryFormFields(String friendlyName, String locator, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.fieldType = fieldType;
    }

    public static AGGridLabSummaryFormFields getEnumValue(String friendlyName) {
        AGGridLabSummaryFormFields agGridLabSummaryFormFields = null;
        for (AGGridLabSummaryFormFields constant : AGGridLabSummaryFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                agGridLabSummaryFormFields = constant;
                break;
            }
        }
        return agGridLabSummaryFormFields;
    }
}
