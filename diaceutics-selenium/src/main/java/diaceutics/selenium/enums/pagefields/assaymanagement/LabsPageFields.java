package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LabsPageFields implements FormFieldInterface {
    COUNTRY("Country", "//div[@class='country']", "country", FieldType.COMBOBOX_JS),
    PER_PAGE("Per-page", "//div[@class='pagination'][./span[text()='Per-page']]",
            "perPage",
            FieldType.COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LabsPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LabsPageFields getEnumValue(String friendlyName) {
        LabsPageFields labsPageFields = null;
        for (LabsPageFields constant : LabsPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                labsPageFields = constant;
                break;
            }
        }
        return labsPageFields;
    }
}
