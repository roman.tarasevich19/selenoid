package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.formatvalue.FormatValue;
import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum AssayDescriptionPageFields implements FormFieldInterface {
    ASSAY_NAME("Assay name",
            "//div[@class='titleArea']/h1",
            "assayName",
            FieldType.TEXT_H1,
            FormatValue.STRING),

    ASSAY_DESCRIPTION("Assay description",
            "Assay description",
            "assayDescription",
            FieldType.TEXT_H4,
            FormatValue.STRING),

    IN_HOUSE_OR_SEND_OUT("In-house or send-out",
            "In-house or send-out",
            "inHouseOrSendOut",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    SEND_OUT_LAB("Send out lab",
            "Send out lab",
            "sendOutLab",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    TESTING_PURPOSE("Testing purpose",
            "Testing purpose",
            "testingPurpose",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    DETECTS("Detects",
            "Detects",
            "detects",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    SPECIMENS_TESTED("Specimens tested",
            "Specimens tested",
            "specimensTested",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    METHOD_NAME("Method name",
            "Method name",
            "method",
            FieldType.TEXT_H4,
            FormatValue.STRING),

    METHOD_DESCRIPTION("Method description",
            "Method description",
            "methodDescription",
            FieldType.TEXT_H4,
            FormatValue.STRING),

    TURN_AROUND_TIME("Turnaround time",
            "Turnaround time",
            "turnAroundTime",
            FieldType.TEXT_GRID,
            FormatValue.TAT),

    ONTOLOGY("Ontology",
            "Ontology",
            "ontology",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    SENSITIVITY("Sensitivity",
            "Sensitivity",
            "sensitivity",
            FieldType.TEXT_GRID,
            FormatValue.PERCENT_DOUBLE),

    SCORING_METHOD("Scoring method",
            "Scoring method",
            "scoringMethod",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    REPORT_FORMAT("Report format",
            "Report format",
            "reportFormat",
            FieldType.TEXT_GRID,
            FormatValue.STRING),

    CLASSIFICATION("Classification",
            "Classification",
            "classifications",
            FieldType.TEXT_H4,
            FormatValue.STRING),

    COMMERCIAL_ASSAY("Commercial assay",
            "Commercial assay",
            "commercialAssays",
            FieldType.TEXT_H4,
            FormatValue.STRING),

    STATUS("Status",
            "//div[contains(@class,'statusWrapper')]/div[contains(@class,'active')]",
            "status",
            FieldType.ASSAY_STATUS,
            FormatValue.STRING),

    YEAR_TO("Year To",
            "//div[contains(@class,'statusWrapper')]/div[not (contains(@class,'active'))]",
            "yearTo",
            FieldType.YEAR,
            FormatValue.STRING),

    MONTH_TO("Month To",
            "//div[contains(@class,'statusWrapper')]/div[not (contains(@class,'active'))]",
            "monthTo",
            FieldType.MONTH,
            FormatValue.MONTH),

    YEAR_FROM("Year From",
            "//div[contains(@class,'statusWrapper')]/div[not (contains(@class,'active'))]",
            "yearFrom",
            FieldType.YEAR,
            FormatValue.STRING),

    MONTH_FROM("Month From",
            "//div[contains(@class,'statusWrapper')]/div[not (contains(@class,'active'))]",
            "monthFrom",
            FieldType.MONTH,
            FormatValue.MONTH);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;
    private final FormatValue format;

    AssayDescriptionPageFields(String friendlyName, String locator, String modelField, FieldType fieldType, FormatValue format) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
        this.format = format;
    }

    public String formatValueAsField(String value) {
        return format.format(value);
    }

    public static AssayDescriptionPageFields getEnumValue(String friendlyName) {
        AssayDescriptionPageFields assayDescriptionPageFields = null;
        for (AssayDescriptionPageFields constant : AssayDescriptionPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                assayDescriptionPageFields = constant;
                break;
            }
        }
        return assayDescriptionPageFields;
    }
}
