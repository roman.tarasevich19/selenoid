package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum AddAnAssayPageFields implements FormFieldInterface {
    ASSAY_NAME("Assay name", "Assay name", "assayName", FieldType.TEXT),
    ASSAY_DESCRIPTION("Assay description",
            "Assay description",
            "assayDescription",
            FieldType.TEXT_AREA),

    IN_HOUSE_OR_SEND_OUT("In-house or send-out?",
            "//ui-radio-group[.//label[contains(text(),'In-house or send-out?')]]",
            "inHouseOrSendOut",
            FieldType.RADIO),

    SEND_OUT_LAB("Send-out Lab",
            "//div[./label[contains(text(),'Send-out Lab')]]",
            "sendOutLab",
            FieldType.COMBOBOX_JS),

    TESTING_PURPOSE("Testing purpose",
            "//div[./label[contains(text(),'Testing purpose')]]",
            "testingPurpose",
            FieldType.COMBOBOX_JS),

    DETECTS("Detects",
            "//div[./label[contains(text(),'Detects')]]",
            "detects",
            FieldType.COMBOBOX_JS),

    SPECIMENS_TESTED("Specimens tested",
            "//div[./label[contains(text(),'Specimens tested')]]",
            "specimensTested",
            FieldType.COMBOBOX_JS),

    METHOD("Method",
            "//div[./label[contains(text(),'Method')]]",
            "method",
            FieldType.COMBOBOX_JS),

    METHOD_DESCRIPTION("Method description",
            "Method description",
            "methodDescription",
            FieldType.TEXT_AREA),

    TURN_AROUND_TIME("Turnaround time (TAT)",
            "//ui-input[./label[contains(text(),'Turnaround time (TAT)')]]//input",
            "turnAroundTime",
            FieldType.NUMBER),

    ONTOLOGY("Ontology",
            "//div[./label[contains(text(),'Ontology')]]",
            "ontology",
            FieldType.COMBOBOX_JS),

    SENSITIVITY("Sensitivity",
            "//ui-input[./label[contains(text(),'Sensitivity')]]//input",
            "sensitivity",
            FieldType.NUMBER),

    SCORING_METHOD("Scoring method",
            "//div[./label[contains(text(),'Scoring method')]]",
            "scoringMethod",
            FieldType.COMBOBOX_JS),

    REPORT_FORMAT("Report format",
            "//div[./label[contains(text(),'Report format')]]",
            "reportFormat",
            FieldType.COMBOBOX_JS),

    CLASSIFICATION("Classification",
            "//ui-radio-group[./label[contains(text(),'Classification')]]",
            "classification",
            FieldType.RADIO),

    FDA_510K_APPROVED_KIT("FDA 510K APPROVED KIT",
            "FDA 510K APPROVED KIT",
            "fda510KApprovedKit",
            FieldType.CHECKBOX),

    FDA_PMA_APPROVED_KIT("FDA PMA APPROVED KIT",
            "FDA PMA APPROVED KIT",
            "fdaPmaApprovedKit",
            FieldType.CHECKBOX),

    IVD_CE("IVD-CE", "IVD-CE", "ivdCe", FieldType.CHECKBOX),
    RUO_IUO("RUO/IUO", "RUO/IUO", "ruoIuo", FieldType.CHECKBOX),

    COMMERCIAL_ASSAYS("Commercial Assays",
            "//div[./label[contains(text(),'Commercial Assays')]]",
            "commercialAssays",
            FieldType.COMBOBOX_JS),

    STATUS("Status",
            "//div[contains(@class,'buttonsToggleWrapper')]/div[contains(text(),'%s')]",
            "status",
            FieldType.BUTTON);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    AddAnAssayPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static AddAnAssayPageFields getEnumValue(String friendlyName) {
        AddAnAssayPageFields addAnAssayPageFields = null;
        for (AddAnAssayPageFields constant : AddAnAssayPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                addAnAssayPageFields = constant;
                break;
            }
        }
        return addAnAssayPageFields;
    }
}
