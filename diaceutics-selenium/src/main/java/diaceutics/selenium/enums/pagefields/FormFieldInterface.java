package diaceutics.selenium.enums.pagefields;

public interface FormFieldInterface {
    String getFriendlyName();

    String getLocator();

    FieldType getFieldType();
}
