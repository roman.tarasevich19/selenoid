package diaceutics.selenium.enums.pagefields.assaymanagement;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum LabAddressPageFields implements FormFieldInterface {
    ADDRESS_ONE("Address 1", "Address 1", "addressOne", FieldType.TEXT),
    ADDRESS_TWO("Address 2", "Address 2", "addressTwo", FieldType.TEXT),
    CITY_TOWN("City / Town", "City / Town", "cityTown", FieldType.TEXT),
    REGION("Region", "Region", "region", FieldType.TEXT),
    COUNTRY("Country", "//div[./label[contains(text(),'Country')]]", "country", FieldType.COMBOBOX_JS),
    POSTAL_CODE("Postal code", "Postal code ", "postalCode", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    LabAddressPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static LabAddressPageFields getEnumValue(String friendlyName) {
        LabAddressPageFields labAddressPageFields = null;
        for (LabAddressPageFields constant : LabAddressPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                labAddressPageFields = constant;
                break;
            }
        }
        return labAddressPageFields;
    }
}
