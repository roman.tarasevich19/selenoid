package diaceutics.selenium.enums.pagefields.labmapping;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum AGGridLabSummaryFormLabDetailsFields implements FormFieldInterface {
    LAB_DISEASE_MARKET_SHARE_PERCENT("Disease market share percent",
            "lab.diseaseMarketSharePercent",
            "labDiseaseMarketSharePercent", FieldType.TEXT),

    LAB_TEST_MARKET_SHARE_PERCENT("Test market share percent",
            "lab.testMarketSharePercent",
            "assayDescription",
            FieldType.TEXT),

    LAB_NAME("Lab name", "lab.name", "name", FieldType.TEXT),
    ADDRESS("Address", "lab.readableAddress", "address", FieldType.TEXT),
    CITY("City", "lab.readableCity", "city", FieldType.TEXT),
    CLASSIFICATION("Classification", "lab.type", "labType", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    AGGridLabSummaryFormLabDetailsFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static AGGridLabSummaryFormLabDetailsFields getEnumValue(String friendlyName) {
        AGGridLabSummaryFormLabDetailsFields gridLabSummaryFormLabDetailsFields = null;
        for (AGGridLabSummaryFormLabDetailsFields constant : AGGridLabSummaryFormLabDetailsFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                gridLabSummaryFormLabDetailsFields = constant;
                break;
            }
        }
        return gridLabSummaryFormLabDetailsFields;
    }
}
