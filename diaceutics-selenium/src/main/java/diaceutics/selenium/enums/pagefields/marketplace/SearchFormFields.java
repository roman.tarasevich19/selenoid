package diaceutics.selenium.enums.pagefields.marketplace;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum SearchFormFields implements FormFieldInterface {
    LOCATION("Location", "location", "location", FieldType.TEXT_DROPDOWN),
    TYPE("Type", "type", "type", FieldType.COMBOBOX),
    SPONSORED("Sponsored", "categories_8", "sponsored", FieldType.CHECKBOX),
    TEST_REVIEW("Test Review", "categories_14", "testReview", FieldType.CHECKBOX),
    OFFERED("Offered", "categories_9", "offered", FieldType.CHECKBOX),
    REQUESTED("Requested", "categories_10", "requested", FieldType.CHECKBOX),
    TESTING_ICON_GROUP_ONE("Testing Icon Group one", "categories_12", "testingIconGroupOne", FieldType.CHECKBOX),
    TESTING_ICON_GROUP_TWO("Testing Icon Group two", "categories_13", "testingIconGroupTwo", FieldType.CHECKBOX),
    LABORATORY("Laboratory", "categories_2", "laboratory", FieldType.CHECKBOX),
    DIAGNOSTICS("Diagnostics", "categories_4", "diagnostics", FieldType.CHECKBOX),
    PHARMACEUTICAL("Pharmaceutical", "categories_3", "pharmaceutical", FieldType.CHECKBOX),
    SERVICE_PROVIDER("Service Provider", "categories_5", "serviceProvider", FieldType.CHECKBOX),
    DIACEUTICS("Diaceutics", "categories_11", "diaceutics", FieldType.CHECKBOX),
    OTHER("Other", "categories_6", "other", FieldType.CHECKBOX),
    KEYWORDS("Keywords", "keywords", "keywords", FieldType.TEXT);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    SearchFormFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static SearchFormFields getEnumValue(String friendlyName) {
        SearchFormFields searchFormFields = null;
        for (SearchFormFields constant : SearchFormFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                searchFormFields = constant;
                break;
            }
        }
        return searchFormFields;
    }
}
