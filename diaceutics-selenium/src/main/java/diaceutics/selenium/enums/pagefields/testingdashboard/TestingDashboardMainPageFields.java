package diaceutics.selenium.enums.pagefields.testingdashboard;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;
import lombok.Getter;

@Getter
public enum TestingDashboardMainPageFields implements FormFieldInterface {
    PROJECT("Project", "//div[./span[contains(text(),'Project')]]", "project", FieldType.COMBOBOX_JS),
    COUNTRY("Country", "//div[./label[contains(text(),'Country')]]", "country", FieldType.COMBOBOX_JS),
    DISEASE("Disease", "//div[./label[contains(text(),'Disease')]]", "diseases", FieldType.COMBOBOX_JS),
    BIOMARKER_ANALOGUE("Biomarker / Analogue",
            "//div[./label[contains(text(),'Biomarker / Analogue')]]",
            "biomarkerName",
            FieldType.COMBOBOX_JS),

    YEAR_FROM("Year From", "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date from')]]//ui-select[@formcontrolname='fromYear']",
            "yearFrom", FieldType.COMBOBOX_JS),
    MONTH_FROM("Month From", "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date from')]]//ui-select[@formcontrolname='fromMonth']",
            "monthFrom", FieldType.COMBOBOX_JS),
    YEAR_TO("Year To", "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date to')]]//ui-select[@formcontrolname='toYear']",
            "yearTo", FieldType.COMBOBOX_JS),
    MONTH_TO("Month To", "//div[@class='dateRangeWrapper'][.//span[contains(text(),'Date to')]]//ui-select[@formcontrolname='toMonth']",
            "monthTo", FieldType.COMBOBOX_JS);

    private final String friendlyName;
    private final String locator;
    private final String modelField;
    private final FieldType fieldType;

    TestingDashboardMainPageFields(String friendlyName, String locator, String modelField, FieldType fieldType) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.modelField = modelField;
        this.fieldType = fieldType;
    }

    public static TestingDashboardMainPageFields getEnumValue(String friendlyName) {
        TestingDashboardMainPageFields testingDashboardMainPageFields = null;
        for (TestingDashboardMainPageFields constant : TestingDashboardMainPageFields.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                testingDashboardMainPageFields = constant;
                break;
            }
        }
        return testingDashboardMainPageFields;
    }
}
