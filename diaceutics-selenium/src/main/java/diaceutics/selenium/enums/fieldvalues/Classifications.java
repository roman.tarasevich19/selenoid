package diaceutics.selenium.enums.fieldvalues;

import lombok.Getter;

@Getter
public enum Classifications {
    COMMERCIAL_ASSAY("Commercial assay", "classification"),
    LABORATORY_DEVELOPED_TEST("Laboratory Developed Test (LDT)", "classification"),
    IVD_CE("IVD-CE", "ivdCe"),
    RUO_IUO("RUO/IUO", "ruoIuo"),
    FDA_510K_APPROVED_KIT("FDA 510K APPROVED KIT", "fda510KApprovedKit"),
    FDA_PMA_APPROVED_KIT("FDA PMA APPROVED KIT", "fdaPmaApprovedKit");

    private final String name;
    private final String modelField;

    Classifications(String name, String modelField) {
        this.name = name;
        this.modelField = modelField;
    }
}
