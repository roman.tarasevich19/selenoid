package diaceutics.selenium.enums.fieldvalues;

public enum FieldValues {
    RANDOM("random"),
    UNSPECIFIED("Unspecified"),
    MAILOAUR("@mailosaur.io");

    private final String key;

    FieldValues(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }
}
