package diaceutics.selenium.enums.date;

public enum DateFormat {
    YYYY_MM_DD("yyyy-MM-dd"),
    YYYY_MM_DD_T_HH_MM_SS_SSS("yyyy-MM-dd'T'HH:mm:ss.SSS"),
    D_MMMM_YYYY_H_MM("d MMMM yyyy, h:mm");

    private final String format;

    DateFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return format;
    }
}
