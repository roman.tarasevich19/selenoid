package diaceutics.selenium.enums.charts;


public interface ChartsInterface {
    String getFriendlyName();

    String getLocator();
}
