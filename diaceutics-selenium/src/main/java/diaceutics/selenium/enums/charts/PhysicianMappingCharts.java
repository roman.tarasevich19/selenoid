package diaceutics.selenium.enums.charts;

public enum PhysicianMappingCharts implements ChartsInterface {
    NATIONAL_VIEW_BY_NUMBER_OF_DISEASE_PHYSICIANS(
            "National view by number of <Disease> physicians",
            "//app-map2"),

    NATIONAL_VIEW_BY_BIOMARKER_TESTING_RATE("National view by biomarker testing rate", "//app-map3"),
    NATIONAL_VIEW_BY_NUMBER_OF_BIOMARKER_PHYSICIANS(
            "National view by number of <Biomarker> physicians",
            "//app-map4"),

    TOP_5_SPECIALTIES_FOR_DISEASE_TREATING_PHYSICIANS(
            "Top 5 specialties for <Disease> treating physicians",
            "//app-graph1[.//highcharts-chart[@data-highcharts-chart='0']]"),

    TOP_5_SPECIALTIES_FOR_BIOMARKER_TREATING_PHYSICIANS(
            "Top 5 specialties for <Biomarker> treating physicians",
            "//app-graph1[.//highcharts-chart[@data-highcharts-chart='1']]"),

    LAB_CLASSIFICATION_PREFERENCE_BY_PHYSICIAN_SPECIALTY(
            "Lab classification preference by physician specialty",
            "//app-graph2"),

    PERCENTAGE_OF_PHYSICIANS_USING_LABS_THAT_OFFER_THE_REQUIRED_BIOMARKER_TEST(
            "Percentage of physicians using labs that offer the required biomarker test",
            "//app-graph3"),

    PHYSICIAN_SPECIALTIES("Physician specialties", "//app-graph4");

    private final String friendlyName;
    private final String locator;

    PhysicianMappingCharts(String friendlyName, String locator) {
        this.friendlyName = friendlyName;
        this.locator = locator;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getLocator() {
        return locator;
    }

    public static PhysicianMappingCharts getEnumValue(String friendlyName) {
        PhysicianMappingCharts physicianMappingCharts = null;
        for (PhysicianMappingCharts constant : PhysicianMappingCharts.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                physicianMappingCharts = constant;
                break;
            }
        }
        return physicianMappingCharts;
    }
}
