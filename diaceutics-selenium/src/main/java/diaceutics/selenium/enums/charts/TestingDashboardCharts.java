package diaceutics.selenium.enums.charts;

public enum TestingDashboardCharts implements ChartsInterface {
    COUNTRY_PIE_CHART("Country pie chart", "//app-testing-by-labs-graph", false, false),
    COUNTRY_MAP_CHART("Country map chart", "//app-testing-by-labs-map", false, false),
    CHART_1("Percentage of labs performing germline testing versus somatic testing for <Biomarker> for <Disease> in <Country>",
            "//app-germline-somatic-graph", true, false),
    CHART_2("Disease market share of labs performing germline versus somatic testing for <Biomarker> for <Disease> in <Country>",
            "//app-germline-somatic-dms-graph", true, false),
    CHART_3("Platforms available within labs testing for <Biomarker> for <Disease>",
            "//app-platform-graph", true, false),
    CHART_4("Disease market share represented by available platforms within labs testing for <Biomarker> for <Disease>",
            "//app-platform-dms-graph", true, false),
    CHART_5("Disease Market share represented by labs offering in-house versus send-out testing for <Biomarker> for <Disease>",
            "//app-in-house-send-out-graph", true, true),
    //TESTING LANDSCAPE
    CHART_6("Percentage of labs performing in-house <Biomarker> testing versus sending out for testing", "//app-graph1", true, true),
    CHART_7("Labs performing <Biomarker> testing by disease market share", "//app-graph1-dms", true, true),
    CHART_8("Percentage of labs offering <Biomarker> testing", "//app-graph12", true, true),
    CHART_9("<Disease> market share represented by labs offering/not offering <Biomarker> testing", "//app-graph12-dms", true, true),
    CHART_10("% <Disease> market share analyzed by lab classification", "//app-graph3", true, true),
    //METHODS
    CHART_11("In-house methods used for <Biomarker> testing", "//app-graph2", true, true),
    CHART_12("<Disease> market share represented by different in-house methods used for <Biomarker> testing", "//app-graph2-dms", true, true),
    CHART_13("Top 15 labs <Biomarker> capabilities by <Disease> market share", "//app-graph4", true, true),
    CHART_14("Number of different methods available for in-house <Biomarker> testing", "//app-graph6", true, true),
    CHART_15("<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing", "//app-graph6-dms", true, true),
    CHART_16("LDT versus commercial assays", "//app-graph7", true, true),
    CHART_17("Labs using LDTs versus commercial assays by DMS", "//app-graph7-dms", true, true),
    CHART_18("LDT versus commercial assays by method", "//app-graph8", true, true),
    CHART_19("LDT versus commercial assays by method_2", "//app-graph8-dms", true, true),
    CHART_20("Commercial assays used for in-house <Biomarker> testing", "//app-graph9", true, true),
    CHART_21("<Disease> market share represented by commercial assays used for in-house <Biomarker> testing", "//app-graph9-dms", true, true),

    //TURNAROUND TIME
    CHART_22("Frequency histogram of average TATs for in-house <Biomarker> testing split by method", "//app-graph10",true, true),
    CHART_23("Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Biomarker> market share", "//app-graph10-dms", true, true),
    CHART_24("Segmentation plot showing assay TaTs for in-house <Biomarker> testing", "//app-graph11", true, true),

    //QUALITY
    CHART_25("EQA providers utilized by labs", "//app-eqa-graph", true, true);


    private final String friendlyName;
    private final String locator;
    private final boolean isTitle;
    private final boolean isSubtitle;

    TestingDashboardCharts(String friendlyName, String locator, boolean isTitle, boolean isSubtitle) {
        this.friendlyName = friendlyName;
        this.locator = locator;
        this.isTitle = isTitle;
        this.isSubtitle = isSubtitle;
    }


    @Override
    public String getFriendlyName() {
        return friendlyName;
    }

    public boolean isTitle(){
        return isTitle;
    }

    public boolean isSubtitle(){
        return isSubtitle;
    }

    @Override
    public String getLocator() {
        return locator;
    }

    public static TestingDashboardCharts getEnumValue(String friendlyName) {
        TestingDashboardCharts testingDashboardCharts = null;
        for (TestingDashboardCharts constant : TestingDashboardCharts.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                testingDashboardCharts = constant;
                break;
            }
        }
        return testingDashboardCharts;
    }
}
