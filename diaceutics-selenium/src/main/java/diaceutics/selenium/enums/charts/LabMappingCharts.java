package diaceutics.selenium.enums.charts;

public enum LabMappingCharts implements ChartsInterface {
    PERCENTAGE_OF_LABS_PERFORMING_BIOMARKER_TESTING(
            "Percentage of labs performing in-house <Biomarker> testing versus sending out for testing", "//app-graph1"),

    LABS_PERFORMING_BIOMARKER_TESTING_BY_DISEASE_MARKET_SHARE(
            "Labs performing <Biomarker> testing by disease market share", "//app-graph1-dms"),

    PERCENTAGE_OF_LABS_OFFERING_BIOMARKER_TESTING(
            "Percentage of labs offering <Biomarker> testing", "//app-graph12"),

    DISEASE_MARKET_SHARE_REPRESENTED_BY_LABS(
            "<Disease> market share represented by labs offering/not offering <Biomarker> testing", "//app-graph12-dms"),

    PERCENTAGE_DISEASE_MARKET_SHARE_ANALYZED_BY_LAB(
            "% <Disease> market share analyzed by lab classification", "//app-graph3"),

    //METHODS
    IN_HOUSE_METHODS_USED_FOR(
            "In-house methods used for <Biomarker> testing", "//app-graph2"),

    DISEASE_MARKET_SHARE_REPRESENTED_BY_DIFFERENT_IN_HOUSE(
            "<Disease> market share represented by different in-house methods used for <Biomarker> testing",
            "//app-graph2-dms"),

    TOP_15_LABS_BIOMARKER(
            "Top 15 labs <Biomarker> capabilities by <Disease> market share",
            "//app-graph4"),

    NUMBER_OF_DIFFERENT_METHODS_AVAILABLE(
            "Number of different methods available for in-house <Biomarker> testing", "//app-graph6"),

    DISEASE_MARKET_SHARE_REPRESENTED_BY_LABS_USING_ONE(
            "<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing", "//app-graph6-dms"),

    LDT_VERSUS_COMMERCIAL_ASSAYS(
            "LDT versus commercial assays", "//app-graph7"),

    LABS_USING_LDTS_VERUS_COMMERCIAL_ASSAYS(
            "Labs using LDTs versus commercial assays by DMS", "//app-graph7-dms"),

    LDT_VERSUS_COMMERCIAL_ASSAYS_BY_METHOD(
            "LDT versus commercial assays by method", "//app-graph8"),

    LDT_VERSUS_COMMERCIAL_ASSAYS_BY_METHOD_2(
            "LDT versus commercial assays by method_2", "//app-graph8-dms"),

    COMMERCIAL_ASSAYS_USED_FOR_IN_HOUSE(
            "Commercial assays used for in-house <Biomarker> testing", "//app-graph9"),

    DISEASE_MARKET_SHARE_REPRESENTED_BY_COMMERCIAL_ASSAYS(
            "<Disease> market share represented by commercial assays used for in-house <Biomarker> testing",
            "//app-graph9-dms"),

    //TURNAROUND TIME
    FREQUENCY_HISTOGRAM_OF_AVERAGE_TATS_FOR_IN_HOUSE(
            "Frequency histogram of average TATs for in-house <Biomarker> testing split by method", "//app-graph10"),

    FREQUENCY_HISTOGRAM_OF_AVERAGE_TATS_FOR_IN_HOUSE_SPLIT(
            "Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Disease> market share", "//app-graph10-dms"),

    SEGMENTATION_PLOT_SHOWING_ASSAY_TATS(
            "Segmentation plot showing assay TaTs for in-house <Biomarker> testing", "//app-graph11");


    private final String friendlyName;
    private final String locator;

    LabMappingCharts(String friendlyName, String locator) {
        this.friendlyName = friendlyName;
        this.locator = locator;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getLocator() {
        return locator;
    }

    public static LabMappingCharts getEnumValue(String friendlyName) {
        LabMappingCharts labMappingCharts = null;
        for (LabMappingCharts constant : LabMappingCharts.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                labMappingCharts = constant;
                break;
            }
        }
        return labMappingCharts;
    }
}
