package diaceutics.selenium.enums.charts;


public enum UsStates implements ChartsInterface {
    COLORADO("Colorado", "CO"),
    FLORIDA("Florida", "FL"),
    MINNESOTA("Minnesota", "MN"),
    NEW_JERSEY("New Jersey", "NJ"),
    VIRGINIA("Virginia", "VA"),
    MONTANA("Montana", "MT"),
    OHIO("Ohio", "OH"),
    MARYLAND("Maryland", "MD"),
    TENNESSEE("Tennessee", "TN"),
    KENTUCKY("Kentucky", "KY"),
    CONNECTICUT("Connecticut", "CT"),
    DISTRICT_COLUMBIA("District of Columbia", "DC"),
    NORTH_CAROLINA("North Carolina", "NC"),
    PENNSYLVANIA("Pennsylvania", "PA"),
    TEXAS("Texas", "TX"),
    OKLAHOMA("Oklahoma", "OK"),
    MISSOURI("Missouri", "MO"),
    NEW_YORK("New York", "NY"),
    CALIFORNIA("California", "CA"),
    INDIANA("Indiana", "IN"),
    KANSAS("Kansas", "KS"),
    NEVADA("Nevada", "NV"),
    SOUTH_CAROLINA("South Carolina", "SC"),
    ARIZONA("Arizona", "AZ"),
    MASSACHUSETTS("Massachusetts", "MA"),
    ALABAMA("Alabama", "AL"),
    GEORGIA("Georgia", "GA"),
    OREGON("Oregon", "OR"),
    WISCONSIN("Wisconsin", "WI"),
    MICHIGAN("Michigan", "MI"),
    ILLINOIS("Illinois", "IL"),
    NEW_MEXICO("New Mexico", "NM"),
    LOUISIANA("Louisiana", "LA"),
    ARKANSAS("Arkansas", "AR"),
    MISSISSIPPI("Mississippi", "MS"),
    IOWA("Iowa", "IA"),
    NEW_HAMPSHIRE("New Hampshire", "NH"),
    WASHINGTON("Washington", "WA"),
    UTAH("Utah", "UT"),
    IDAHO("Idaho", "ID"),
    SOUTH_DAKOTA("South Dakota", "SD"),
    HAWAII("Hawaii", "HI"),
    MAINE("Maine", "ME"),
    WEST_VIRGINIA("West Virginia", "WV"),
    NEBRASKA("Nebraska", "NE"),
    NORTH_DAKOTA("North Dakota", "ND"),
    DELAWARE("Delaware", "DE"),
    VERMONT("Vermont", "VT"),
    WYOMING("Wyoming", "WY"),
    RHODE_ISLAND("Rhode Island", "RI"),
    ALASKA("Alaska", "AK");


    private final String friendlyName;
    private final String state;
    private final String modelField;

    UsStates(String friendlyName, String locator) {
        this.friendlyName = friendlyName;
        this.state = locator;
        this.modelField = "title";
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    @Override
    public String getLocator() {
        return state;
    }

    public String getModelField(){
        return modelField;
    }

    public static UsStates getEnumValue(String friendlyName) {
        UsStates usStates = null;
        for (UsStates constant : UsStates.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                usStates = constant;
                break;
            }
        }
        return usStates;
    }
}
