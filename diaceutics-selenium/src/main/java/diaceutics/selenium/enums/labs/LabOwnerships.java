package diaceutics.selenium.enums.labs;

public enum LabOwnerships {
    UNSPECIFIED("Unspecified"),
    PUBLIC("Public"),
    PRIVATE("Private"),
    MILITARY("Military"),
    NOT_APPLICABLE("Not Applicable");

    private final String key;

    LabOwnerships(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }
}
