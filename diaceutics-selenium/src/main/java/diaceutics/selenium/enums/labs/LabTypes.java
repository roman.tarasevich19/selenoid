package diaceutics.selenium.enums.labs;

public enum LabTypes {
    UNSPECIFIED("Unspecified"),
    ACADEMIC_LAB("Academic Lab"),
    COMMERCIAL_LAB("Commercial Lab"),
    HOSPITAL_LAB("Hospital Lab");

    private final String key;

    LabTypes(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }
}
