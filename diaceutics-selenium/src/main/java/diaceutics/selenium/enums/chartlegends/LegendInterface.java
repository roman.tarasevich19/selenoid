package diaceutics.selenium.enums.chartlegends;


public interface LegendInterface {
    String getFriendlyName();

    String getLocator();
}
