package diaceutics.selenium.enums.chartlegends;


public enum LabMappingPieChartLegends implements LegendInterface {
    COMMERCIAL_ASSAY("Commercial assay", "color-1"),
    BOTH("Both", "color-2"),
    UNSPECIFIED("Unspecified", "color-3");

    private final String friendlyName;
    private final String locator;

    LabMappingPieChartLegends(String friendlyName, String locator) {
        this.friendlyName = friendlyName;
        this.locator = locator;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getLocator() {
        return locator;
    }

    public static LabMappingPieChartLegends getEnumValue(String friendlyName) {
        LabMappingPieChartLegends legends = null;
        for (LabMappingPieChartLegends constant : LabMappingPieChartLegends.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                legends = constant;
                break;
            }
        }
        return legends;
    }
}
