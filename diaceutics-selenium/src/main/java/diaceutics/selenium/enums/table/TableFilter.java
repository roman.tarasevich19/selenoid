package diaceutics.selenium.enums.table;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;

public enum TableFilter implements FormFieldInterface {
    CONTAINS("Contains", "Contains"),
    NOT_CONTAINS("Not contains", "Not contains"),
    EQUALS("Equals", "Equals"),
    NOT_EQUAL("Not equal", "Not equal"),
    STARTS_WITH("Starts with", "Starts with"),
    ENDS_WITH("Ends with", "Ends with");

    private final String friendlyName;
    private final String locator;

    TableFilter(String friendlyName, String locator) {
        this.friendlyName = friendlyName;
        this.locator = locator;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getLocator() {
        return locator;
    }

    public FieldType getFieldType() {
        return FieldType.COMBOBOX;
    }

    public static TableFilter getEnumValue(String friendlyName) {
        TableFilter tableFilter = null;
        for (TableFilter constant : TableFilter.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                tableFilter = constant;
                break;
            }
        }
        return tableFilter;
    }


}
