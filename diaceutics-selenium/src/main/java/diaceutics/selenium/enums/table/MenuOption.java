package diaceutics.selenium.enums.table;

import diaceutics.selenium.enums.pagefields.FieldType;
import diaceutics.selenium.enums.pagefields.FormFieldInterface;

public enum MenuOption implements FormFieldInterface {
    AUTOSIZE_THIS_COLUMN("Autosize This Column", "Autosize This Column"),
    AUTOSIZE_ALL_COLUMNS("Autosize All Columns", "Autosize All Columns"),
    RESET_COLUMNS("Reset Columns", "Reset Columns"),
    EXPAND_ALL("Expand All", "Expand All"),
    COLLAPSE_ALL("COLLAPSE ALL", "SCOLLAPSE ALL");

    private final String friendlyName;
    private final String locator;

    MenuOption(String friendlyName, String locator) {
        this.friendlyName = friendlyName;
        this.locator = locator;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getLocator() {
        return locator;
    }

    public FieldType getFieldType() {
        return FieldType.COMBOBOX;
    }

    public static MenuOption getEnumValue(String friendlyName) {
        MenuOption tableFilter = null;
        for (MenuOption constant : MenuOption.values()) {
            if (constant.getFriendlyName().equals(friendlyName)) {
                tableFilter = constant;
                break;
            }
        }
        return tableFilter;
    }


}
