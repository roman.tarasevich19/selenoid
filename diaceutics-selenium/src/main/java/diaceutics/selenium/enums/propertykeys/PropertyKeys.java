package diaceutics.selenium.enums.propertykeys;

public enum PropertyKeys {
    API_MAIL_KEY("apiMailKey"),
    MAIL_SERVER_ID("mailServerId");

    private final String key;

    PropertyKeys(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }
}
