package diaceutics.selenium.enums.urls;

public enum Urls {
    //HEADER
    HOME("Home URL", "/en/dashboard/"),
    PROJECTS("Projects URL", "https://projects.dxrx.io/"),
    MARKETPLACE("Marketplace URL", "/en/listing/search_result?type=collaboration"),
    MY_ORGANISATION("My organization URL", "/en/organization/edit-presentation"),
    MY_PROFILE("My profile URL", "/en/dashboard/user/edit-identity"),
    SETTINGS("Settings URL", "/en/dashboard/user/settings"),
    HELP("Help URL", "https://help.dxrx.io/en/"),
    LOGIN("Login URL", "https://login.dxrx.io/u/login"),
    MY_COLLABORATIONS("My collaborations URL", "/en/dashboard/listing"),
    INTERCOM("Intercom URL", "https://www.intercom.com/"),

    //FOOTER
    TERMS("Terms URL", "https://www.diaceutics.com/dxrx-terms"),
    PRIVACY_STATEMENT("Privacy statement URL", "https://www.diaceutics.com/dxrx-privacy-statement"),
    COOKIE_POLICY("Cookie Policy URL", "https://www.diaceutics.com/dxrx-cookies-policy"),
    CONTACT_US("Contact Us URL", "https://www.diaceutics.com/contact-us"),

    //REGISTER PAGE
    PRIVACY_POLICY("Privacy Policy URL", "https://www.diaceutics.com/dxrx-privacy-statement"),
    MEMBERSHIP_TERMS("Membership Terms URL", "https://www.diaceutics.com/dxrx-membership-terms"),

    //TOOLS
    ASSAYS("Assays URL", "/en/assays/lab/70577"),
    ASSAY_MANAGEMENT("Assay Management URL", "/en/assays/"),
    LAB_MAPPING("Lab Mapping URL", "/en/mappings/lab/search"),
    PHYSICIAN_MAPPING("Physician Mapping URL", "/en/mappings/physicians/search"),
    TESTING_DASHBOARD("Testing Dashboard URL", "/en/tests/search"),

    //PERMISSIONS
    PERMISS_ASSAYS("assays", "/en/assays/"),
    PERMISS_ASSAYS_SEARCH("assay search", "/en/assays/search?countryCode=BLR&keyword=77777/"),
    PERMISS_LAB_72444("lab", "/en/assays/lab/%s"),
    PERMISS_EDIT_LAB_72444("edit lab", "/en/assays/lab/edit/%s"),
    PERMISS_EDIT_LAB_73506("edit not assigned lab", "/en/assays/lab/edit/%s"),
    PERMISS_LAB_73506("not assigned lab", "/en/assays/lab/%s"),
    PERMISS_TESTS_CREATE_72444("tests create for lab", "/en/assays/test/create/%s"),
    PERMISS_TESTS_CREATE_73506("tests create for not assigned lab", "/en/assays/test/create/%s"),
    PERMISS_TESTS_EDIT_72444("tests edit for lab", "/en/assays/test/edit/%s/4912"),
    PERMISS_TESTS_EDIT_73506("tests edit for not assigned lab", "/en/assays/test/edit/%s/5331"),
    PERMISS_TESTS_EDIT_72444_ASSAYS("tests edit lab assays", "/en/assays/test/edit/%s/assayId/");

    private final String friendlyName;
    private final String url;

    Urls(String friendlyName, String url) {
        this.friendlyName = friendlyName;
        this.url = url;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getUrl() {
        return url;
    }

    public static Urls getEnumValue(String color) {
        Urls urls = null;
        for (Urls constant : Urls.values()) {
            if (constant.getFriendlyName().equals(color)) {
                urls = constant;
                break;
            }
        }
        return urls;
    }
}
