package diaceutics.selenium.enums.formatvalue;


import diaceutics.selenium.utilities.DateUtil;

public enum FormatValue {
    STRING(""),
    TAT("%s days"),
    MONTH(""),
    DMS("%s%%"),
    PERCENT_DOUBLE("%s.00%%");

    private final String format;

    FormatValue(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return format;
    }

    public String format(String value) {
        switch (this) {
            case DMS:
            case TAT:
            case PERCENT_DOUBLE:
                return String.format(format, value);
            case MONTH:
                return DateUtil.convertMonth(value);
            default:
                return value;
        }
    }
}
