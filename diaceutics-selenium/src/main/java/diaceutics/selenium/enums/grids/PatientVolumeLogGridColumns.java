package diaceutics.selenium.enums.grids;

public enum PatientVolumeLogGridColumns {
    TIME_PERIOD("Time period"),
    DISEASE("Disease"),
    DISEASE_VOL("Disease vol."),
    BIOMARKERS("Biomarkers"),
    BIOMARKER_VOL("Biomarker vol."),
    UPDATED("Updated");

    private final String key;

    PatientVolumeLogGridColumns(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }
}
