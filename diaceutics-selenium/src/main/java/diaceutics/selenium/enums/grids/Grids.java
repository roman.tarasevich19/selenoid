package diaceutics.selenium.enums.grids;

import lombok.Getter;

@Getter
public enum Grids {
    ASSAYS("Assays", "//div[contains(@class,'dataTable')][.//h3[.='Assays']]//ui-table//table[2]"),
    PATIENT_VOLUME_LOG("Patient volume log",
            "//div[contains(@class,'dataTable')][.//h3[.='Patient volume log']]//ui-table//table[2]"),

    LOG_OF_ASSAY_STATUS_CHANGES("Log of assay status changes",
            "//div[@class='statusHistoryModal']//table[@multitemplatedatarows]"),

    DRILL_DOWN_OF_COMMERCIAL_ASSAY_SEGMENT("Drill down of Commercial Assay segment",
            "//ui-modal[contains(@class,'open')]//ui-table//table[2]");

    private final String name;
    private final String locator;

    Grids(String name, String locator) {
        this.name = name;
        this.locator = locator;
    }
}
