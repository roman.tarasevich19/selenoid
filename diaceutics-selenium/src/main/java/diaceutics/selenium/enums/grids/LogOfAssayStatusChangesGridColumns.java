package diaceutics.selenium.enums.grids;

import lombok.Getter;

@Getter
public enum LogOfAssayStatusChangesGridColumns {
    UPDATED("Updated", "updated"),
    STATUS("Status", "status"),
    ACTIVE("Active", "active"),
    INACTIVE("Inactive", "inactive");

    private final String columnName;
    private final String modelField;

    LogOfAssayStatusChangesGridColumns(String columnName, String modelField) {
        this.columnName = columnName;
        this.modelField = modelField;
    }
}
