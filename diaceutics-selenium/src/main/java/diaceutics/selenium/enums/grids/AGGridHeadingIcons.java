package diaceutics.selenium.enums.grids;

import lombok.Getter;

@Getter
public enum AGGridHeadingIcons {
    LABS("labs", "labs"),
    LABS_ASSAYS_FOR_BIOMARKER("labs assays for biomarker", "d-n-a"),
    PATIENT_TESTS("patient tests", "patient"),
    PHYSICIANS("physicians", "doctor");

    private final String iconName;
    private final String locator;

    AGGridHeadingIcons(String iconName, String locator) {
        this.iconName = iconName;
        this.locator = locator;
    }
    
    public static AGGridHeadingIcons getEnumValue(String friendlyName) {
        AGGridHeadingIcons labMappingHeadingIconFields = null;
        for (AGGridHeadingIcons constant : AGGridHeadingIcons.values()) {
            if (constant.getIconName().equals(friendlyName)) {
                labMappingHeadingIconFields = constant;
                break;
            }
        }
        return labMappingHeadingIconFields;
    }
}
