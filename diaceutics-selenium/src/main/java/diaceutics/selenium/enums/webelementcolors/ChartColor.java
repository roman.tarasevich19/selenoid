package diaceutics.selenium.enums.webelementcolors;

import lombok.Getter;

@Getter
public enum ChartColor {
    PINK("pink", "rgb(229, 44, 152)", "highcharts-color-1");

    private final String color;
    private final String colorCode;
    private final String pieChartClassLocator;

    ChartColor(String color, String colorCode, String pieChartClassLocator) {
        this.color = color;
        this.colorCode = colorCode;
        this.pieChartClassLocator = pieChartClassLocator;
    }

    public static ChartColor getEnumValue(String color) {
        ChartColor chartColor = null;
        for (ChartColor constant : ChartColor.values()) {
            if (constant.getColor().equals(color)) {
                chartColor = constant;
                break;
            }
        }
        return chartColor;
    }
}
