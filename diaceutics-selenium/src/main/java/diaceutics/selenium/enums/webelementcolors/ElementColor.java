package diaceutics.selenium.enums.webelementcolors;

import lombok.Getter;

@Getter
public enum ElementColor {
    PURPLE("purple", "rgba(223, 169, 255, 1)"),
    GREY("grey", "rgba(67, 67, 67, 1)"),
    RED("red", "rgb(220, 53, 69)"),
    RED_FOR_BORDERS("redBorder", "rgb(242, 68, 65)"),
    VIOLET("violet", "rgba(170, 89, 217, 1)"),
    WHITE("white", "rgb(246, 246, 246)"),
    RED_AUTH_ZERO("red_auth0", "rgb(255, 0, 0)"),
    ORANGE_AUTH_ZERO("orange_auth0", "rgba(221, 75, 57, 1)"),
    GREEN_AUTH_ZERO("green_auth0", "rgba(126, 211, 33, 1)"),
    WHITE_AUTH_ZERO("white_auth0", "rgba(255, 255, 255, 1)");

    private final String color;
    private final String colorCode;

    ElementColor(String color, String colorCode) {
        this.color = color;
        this.colorCode = colorCode;
    }

    public static ElementColor getEnumValue(String color) {
        ElementColor elementColor = null;
        for (ElementColor constant : ElementColor.values()) {
            if (constant.getColor().equals(color)) {
                elementColor = constant;
                break;
            }
        }
        return elementColor;
    }
}
