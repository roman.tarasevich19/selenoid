package diaceutics.restassured.utilities;

import diaceutics.restassured.project.requests.token.Token;
import diaceutics.selenium.configuration.Configuration;
import diaceutics.selenium.models.User;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.experimental.UtilityClass;

@UtilityClass
public class RequestSpecHelper {
    private static RequestSpecification spec;

    public static RequestSpecification getRequestSpec(User user) {
        if (spec == null)
            spec = new RequestSpecBuilder()
                    .log(LogDetail.URI)
                    .setBaseUri(Configuration.getUrl(Configuration.API_URL))
                    .setBasePath(RestAssured.basePath)
                    .setContentType(ContentType.JSON)
                    .build()
                    .auth()
                    .oauth2(new Token().getToken(user));
        return spec;
    }

    public static RequestSpecification getDefaultRequestSpec() {
        return RestAssured.given().spec(new RequestSpecBuilder().setBasePath(RestAssured.basePath).build());
    }

}
