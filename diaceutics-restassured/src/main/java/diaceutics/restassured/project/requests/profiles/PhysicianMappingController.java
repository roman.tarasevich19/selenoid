package diaceutics.restassured.project.requests.profiles;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.endpoints.profiles.PhysicianMappingControllerEndPoints;
import diaceutics.restassured.project.constants.paths.ProfilesPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PhysicianMappingController {

    public PhysicianMappingController() {
        RestAssured.requestSpecification.given().basePath(ProfilesPath.BASE_PATH);
    }

    public Response returnPreferredLabs(Object object) {
        return RestRequestsHelper.postRequest(
                PhysicianMappingControllerEndPoints.RETURNS_PREFERRED_LABS, object);
    }
}
