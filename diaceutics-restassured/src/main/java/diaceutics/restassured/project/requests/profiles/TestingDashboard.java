package diaceutics.restassured.project.requests.profiles;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.endpoints.profiles.TestingDashboardEndPoints;
import diaceutics.restassured.project.constants.paths.ProfilesPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestingDashboard {

    public TestingDashboard() {
        RestAssured.requestSpecification.given().basePath(ProfilesPath.BASE_PATH);
    }

    public Response returnsDataForLabTestingCharts(Object object) {
       return RestRequestsHelper.postRequest(
               TestingDashboardEndPoints.RETURNS_DATA_FOR_LAB_TESTING_CHARTS, object);}

    public Response returnsDataForVersusCharts(Object object) {
        return RestRequestsHelper.postRequest(
                TestingDashboardEndPoints.RETURNS_DATA_FOR_VERSUS_CHARTS, object);}

    public Response returnsDataForClaimsCharts(Object object) {
        return RestRequestsHelper.postRequest(
                TestingDashboardEndPoints.RETURNS_DATA_FOR_CLAIMS_CHARTS, object);}

    public Response returnsDataForPlatformCharts(Object object) {
        return RestRequestsHelper.postRequest(
                TestingDashboardEndPoints.RETURNS_DATA_FOR_PLATFORM_CHARTS, object);}
}
