package diaceutics.restassured.project.requests.profiles;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.endpoints.profiles.MetadataEndPoints;
import diaceutics.restassured.project.constants.paths.ProfilesPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.Map;

public class Metadata {

    public Metadata() {
        RestAssured.requestSpecification.given().basePath(ProfilesPath.BASE_PATH);
    }

    public Response returnsPageableListOfAllBiomarkers(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_BIOMARKERS, params);
    }

    public Response returnsPageableListOfAllBiomarkers() {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_BIOMARKERS);
    }

    public Response returnsPageableListOfAllClassifications(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_CLASSIFICATIONS, params);
    }

    public Response returnsPageableListOfAllCountriesForAGivenProfileType(Map<String, String> params) {
        return RestRequestsHelper.getRequest(
                MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_COUNTRIES_FOR_A_GIVEN_PROFILE_TYPE, params);
    }

    public Response returnsPageableListOfAllDiseases(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_DISEASES, params);
    }

    public Response returnsPageableListOfAllMethods(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_METHODS, params);
    }

    public Response returnsPageableListOfAllOntologies(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_ONTOLOGIES, params);
    }

    public Response returnsPageableListOfPlatformManufacturers(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_PLATFORM_MANUFACTURERS, params);
    }

    public Response returnsPageableListOfResultFormats(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_RESULT_FORMATS, params);
    }

    public Response returnsPageableListOfAllDetectGermlineSomatics(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_DETECT_GERMLINE_SOMATICS, params);
    }

    public Response returnsPageableListOfAllScoringMethodologies(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_SCORING_METHODOLOGIES, params);
    }

    public Response returnsPageableListOfAllSpecimenRequirements(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_SPECIMEN_REQUIREMENTS, params);
    }

    public Response returnsPageableListOfAllTestingPurposes(Map<String, String> params) {
        return RestRequestsHelper.getRequest(MetadataEndPoints.RETURNS_PAGEABLE_LIST_OF_ALL_TESTING_PURPOSES, params);
    }

    public Response returnsListOfAllBiomarkersForTheSelectedCommercialAssay(int commercialAssayId) {
        return RestRequestsHelper.getRequest(
                String.format(MetadataEndPoints.RETURNS_LIST_OF_ALL_BIOMARKERS_FOR_THE_SELECTED_COMMERCIAL_ASSAY,
                        commercialAssayId));
    }

    public Response returnsListOfAllBiomarkersForTheSelectedCommercialAssaySecondRequest(int commercialAssayId) {
        return RestRequestsHelper.getRequest(
                String.format(MetadataEndPoints.RETURNS_LIST_OF_ALL_BIOMARKERS_FOR_THE_SELECTED_COMMERCIAL_ASSAY_SECOND_REQUEST,
                        commercialAssayId));
    }
}
