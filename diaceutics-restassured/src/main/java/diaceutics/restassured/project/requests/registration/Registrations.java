package diaceutics.restassured.project.requests.registration;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.endpoints.profiles.RegistrationEndPoints;
import diaceutics.restassured.project.constants.paths.RegistrationPath;
import diaceutics.restassured.project.models.ApiUser;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Registrations {

    public Registrations() {
        RestAssured.requestSpecification.given().basePath(RegistrationPath.BASE_PATH);
    }

    public Response createUser(ApiUser user) {
        return RestRequestsHelper.postRequest(RegistrationEndPoints.CREATE_USER, user);
    }

}
