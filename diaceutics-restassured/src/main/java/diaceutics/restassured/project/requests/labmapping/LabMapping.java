package diaceutics.restassured.project.requests.labmapping;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.endpoints.labmapping.LabMappingEndPoints;
import diaceutics.restassured.project.constants.endpoints.labmapping.SubscriptionsEndPoints;
import diaceutics.restassured.project.constants.paths.LabMappingPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class LabMapping {

    public LabMapping() {
        RestAssured.requestSpecification.given().basePath(LabMappingPath.BASE_PATH);
    }

    public Response returnsCollectionSubscriptionsForTheAuthenticatedPrinciple() {
        return RestRequestsHelper.getRequest(
                SubscriptionsEndPoints.RETURNS_A_COLLECTION_OF_SUBSCRIPTIONS_FOR_THE_AUTHENTICATED_PRINCIPLE);
    }

    public Response generatesLabMapping(Object filter) {
        return RestRequestsHelper.postRequest(LabMappingEndPoints.GENERATES_A_LAB_MAPPING, filter);
    }

    public Response generatesTestingDashboardMapping(Object filter) {
        return RestRequestsHelper.postRequest(LabMappingEndPoints.GENERATES_A_TESTING_DASHBOARD_MAPPING, filter);
    }
}
