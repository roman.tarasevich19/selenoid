package diaceutics.restassured.project.requests.profiles;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.paths.ProfilesPath;
import diaceutics.restassured.project.constants.endpoints.profiles.VolumesEndPoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.Map;

public class Volumes {

    public Volumes() {
        RestAssured.requestSpecification.given().basePath(ProfilesPath.BASE_PATH);
    }

    public Response createsNewLabVolume(Integer labId, String jsonBody) {
        return RestRequestsHelper.postRequest(String.format(VolumesEndPoints.CREATES_NEW_LAB_VOLUME, labId), jsonBody);
    }

    public Response createsNewLabVolume(Integer labId, String jsonBody, String token) {
        return RestRequestsHelper.postRequest(String.format(VolumesEndPoints.CREATES_NEW_LAB_VOLUME, labId), jsonBody, token);
    }

    public Response returnsCollectionOfLabVolumes(Integer labId) {
        return RestRequestsHelper.getRequest(String.format(VolumesEndPoints.RETURNS_COLLECTION_OF_LAB_VOLUMES, labId));
    }

    public Response returnsCollectionOfLabVolumes(Integer labId, String token) {
        return RestRequestsHelper.getRequest(String.format(VolumesEndPoints.RETURNS_COLLECTION_OF_LAB_VOLUMES, labId), token);
    }

    public Response updatesAnExistingVolume(Integer labId, Object volume) {
        return RestRequestsHelper.putRequest(
                String.format(VolumesEndPoints.UPDATES_AN_EXISTING_VOLUME, labId), volume);
    }

    public Response updatesAnExistingVolume(Integer labId, Object volume, String token) {
        return RestRequestsHelper.putRequest(
                String.format(VolumesEndPoints.UPDATES_AN_EXISTING_VOLUME, labId), volume, token);
    }

    public Response deleteVolumeForLab(Integer labId, Object deleteVolume) {
        return RestRequestsHelper.deleteRequest(String.format(VolumesEndPoints.DELETE_VOLUME_FOR_LAB, labId), deleteVolume);
    }

    public Response deleteVolumeForLab(Integer labId, Object deleteVolume, String token) {
        return RestRequestsHelper.deleteRequest(
                String.format(VolumesEndPoints.DELETE_VOLUME_FOR_LAB, labId), deleteVolume, token);
    }

    public Response returnsLabVolumesInternalDto(Object volumesFilter) {
        return RestRequestsHelper.postRequest(VolumesEndPoints.POST_FOR_VOLUMES, volumesFilter);
    }

    public Response getReturnVolumeForLab(Integer labId, Map<String, String> params) {
        return RestRequestsHelper.getRequest(String.format(VolumesEndPoints.RETURNS_LAB_VOLUME, labId), params);
    }

    public Response getReturnVolumeForLab(Integer labId, Map<String, String> params, String token) {
        return RestRequestsHelper.getRequest(String.format(VolumesEndPoints.RETURNS_LAB_VOLUME, labId), params, token);
    }
}
