package diaceutics.restassured.project.requests.token;

import diaceutics.restassured.utilities.RequestSpecHelper;
import diaceutics.restassured.project.enums.TokenFields;
import diaceutics.selenium.configuration.Configuration;
import diaceutics.selenium.models.User;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


public class Token {

    private static final String TOKEN_END_POINT = "/oauth/token";

    public String getToken(User user) {
        Response response = RequestSpecHelper.getDefaultRequestSpec()
                .baseUri(Configuration.getUrl(Configuration.AUTH_0_API_URL))
                .contentType(ContentType.URLENC.withCharset("UTF-8"))
                .formParam(TokenFields.GRANT_TYPE.getModelField(), TokenFields.PASSWORD.getModelField())
                .formParam(TokenFields.USER_NAME.getModelField(), user.getEmail())
                .formParam(TokenFields.PASSWORD.getModelField(), user.getPassword())
                .formParam(TokenFields.CLIENT_ID.getModelField(), System.getProperty(TokenFields.CLIENT_ID.getModelField()))
                .formParam(TokenFields.CLIENT_SECRET.getModelField(), System.getProperty(TokenFields.CLIENT_SECRET.getModelField()))
                .formParam(TokenFields.AUDIENCE.getModelField(), Configuration.getUrl(Configuration.AUDIENCE_URL))
                .post(TOKEN_END_POINT).then().extract().response();

        return response.body().path(TokenFields.ACCESS_TOKEN.getModelField());
    }
}
