package diaceutics.restassured.project.requests.physicianmapping;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.endpoints.physicianmapping.PhysicianMappingEndPoints;
import diaceutics.restassured.project.constants.endpoints.physicianmapping.SubscriptionsEndPoints;
import diaceutics.restassured.project.constants.paths.PhysicianMappingPath;
import diaceutics.restassured.project.models.PhysicianMappingRequest;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PhysicianMapping {

    public PhysicianMapping() {
        RestAssured.requestSpecification.given().basePath(PhysicianMappingPath.BASE_PATH);
    }

    public Response returnsCollectionSubscriptionsForTheAuthenticatedPrinciple() {
        return RestRequestsHelper.getRequest(
                SubscriptionsEndPoints.RETURNS_A_COLLECTION_OF_SUBSCRIPTIONS_FOR_THE_AUTHENTICATED_PRINCIPLE);
    }

    public Response generatesPhysicianMapping(PhysicianMappingRequest filter) {
        return RestRequestsHelper.postRequest(PhysicianMappingEndPoints.GENERATES_A_PHYSICIAN_MAPPING, filter);}
}
