package diaceutics.restassured.project.requests.profiles;

import diaceutics.restassured.utilities.RestRequestsHelper;
import diaceutics.restassured.project.constants.endpoints.profiles.LabsEndPoints;
import diaceutics.restassured.project.constants.paths.ProfilesPath;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.restassured.project.models.Address;
import diaceutics.restassured.project.models.MovingAssayToLab;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.Map;

public class Labs {

    public Labs() {
        RestAssured.requestSpecification.given().basePath(ProfilesPath.BASE_PATH);
    }

    public Response createLab(LabDto labDto) {
        return RestRequestsHelper.postRequest(LabsEndPoints.CREATE_LAB, labDto);
    }

    public Response createLab(LabDto labDto, String token) {
        return RestRequestsHelper.postRequest(LabsEndPoints.CREATE_LAB, labDto, token);
    }

    public Response returnsCollectionOfLabs(Map<String, String> params) {
        return RestRequestsHelper.getRequest(LabsEndPoints.RETURNS_COLLECTION_OF_LABS, params);
    }

    public Response returnsCollectionOfLabs(Map<String, String> params, String token) {
        return RestRequestsHelper.getRequest(LabsEndPoints.RETURNS_COLLECTION_OF_LABS, params, token);
    }

    public Response returnsLabWithGivenID(Integer labId) {
        return RestRequestsHelper.getRequest(String.format(LabsEndPoints.RETURNS_LAB_WITH_GIVEN_ID, labId));
    }

    public Response returnsLabWithGivenID(Integer labId, String token) {
        return RestRequestsHelper.getRequest(String.format(LabsEndPoints.RETURNS_LAB_WITH_GIVEN_ID, labId), token);
    }

    public Response updatesAnExistingLab(Integer labId, LabDto labDto) {
        return RestRequestsHelper.putRequest(String.format(LabsEndPoints.UPDATES_AN_EXISTING_LAB, labId), labDto);
    }

    public Response updatesAnExistingLab(Integer labId, LabDto labDto, String token) {
        return RestRequestsHelper.putRequest(String.format(LabsEndPoints.UPDATES_AN_EXISTING_LAB, labId), labDto, token);
    }

    public Response updatesAnExistingAddress(Integer labId, Integer addressId, Address address) {
        return RestRequestsHelper.putRequest(
                String.format(LabsEndPoints.UPDATES_AN_EXISTING_ADDRESS, labId, addressId), address);
    }

    public Response updatesAnExistingAddress(Integer labId, Integer addressId, Address address, String token) {
        return RestRequestsHelper.putRequest(
                String.format(LabsEndPoints.UPDATES_AN_EXISTING_ADDRESS, labId, addressId), address, token);
    }

    public Response createsNewLabTestForLab(Integer labId, String jsonBody) {
        return RestRequestsHelper.postRequest(String.format(LabsEndPoints.CREATES_NEW_LAB_TEST_FOR_LAB, labId), jsonBody);
    }

    public Response createsNewLabTestForLab(Integer labId, String jsonBody, String token) {
        return RestRequestsHelper.postRequest(String.format(LabsEndPoints.CREATES_NEW_LAB_TEST_FOR_LAB, labId), jsonBody, token);
    }

    public Response returnsLabTestWithGivenLabIDAndLabTestIDs(Integer labId, Integer labTestId) {
        return RestRequestsHelper.getRequest(
                String.format(LabsEndPoints.RETURNS_LAB_TEST_WITH_GIVEN_LAB_ID_AND_LAB_TEST_I_DS, labId, labTestId));
    }

    public Response returnsLabTestWithGivenLabIDAndLabTestIDs(Integer labId, Integer labTestId, String token) {
        return RestRequestsHelper.getRequest(
                String.format(LabsEndPoints.RETURNS_LAB_TEST_WITH_GIVEN_LAB_ID_AND_LAB_TEST_I_DS, labId, labTestId), token);
    }

    public Response updatesLabTest(Integer labId, Integer labTestId, String jsonBody) {
        return RestRequestsHelper.putRequest(String.format(LabsEndPoints.UPDATES_LAB_TEST, labId, labTestId), jsonBody);
    }

    public Response updatesLabTest(Integer labId, Integer labTestId, String jsonBody, String token) {
        return RestRequestsHelper.putRequest(String.format(LabsEndPoints.UPDATES_LAB_TEST, labId, labTestId), jsonBody, token);
    }

    public Response deleteLabTest(Integer labId, Integer labTestId) {
        return RestRequestsHelper.deleteRequest(String.format(LabsEndPoints.DELETE_LAB_TEST, labId, labTestId));
    }

    public Response deleteLabTest(Integer labId, Integer labTestId, String token) {
        return RestRequestsHelper.deleteRequest(String.format(LabsEndPoints.DELETE_LAB_TEST, labId, labTestId), token);
    }

    public Response returnsCollectionOfLabTestsForLab(Integer labId) {
        return RestRequestsHelper.getRequest(String.format(LabsEndPoints.RETURNS_COLLECTION_OF_LAB_TESTS_FOR_LAB, labId));
    }

    public Response returnsCollectionOfLabTestsForLab(Integer labId, String token) {
        return RestRequestsHelper.getRequest(String.format(LabsEndPoints.RETURNS_COLLECTION_OF_LAB_TESTS_FOR_LAB, labId), token);
    }

    public Response movingAssayToLab(Integer labId, MovingAssayToLab movingAssayToLab) {
        return RestRequestsHelper.postRequest(String.format(LabsEndPoints.MOVING_ASSAY_TO_LAB, labId), movingAssayToLab);
    }

    public Response movingAssayToLab(Integer labId, MovingAssayToLab movingAssayToLab, String token) {
        return RestRequestsHelper.postRequest(String.format(LabsEndPoints.MOVING_ASSAY_TO_LAB, labId), movingAssayToLab, token);
    }

    public Response returnsCollectionOfLabNamesAndTheirIDs() {
        return RestRequestsHelper.getRequest(LabsEndPoints.RETURNS_COLLECTION_OF_LAB_NAMES_AND_THEIR_I_DS);
    }
}
