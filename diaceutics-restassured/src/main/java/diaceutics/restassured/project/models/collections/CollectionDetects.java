package diaceutics.restassured.project.models.collections;


import diaceutics.restassured.project.models.DetectGermlineSomatic;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionDetects extends Collection {
    private Pageable pageable;
    private List<DetectGermlineSomatic> content;
}
