package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.Pageable;
import diaceutics.restassured.project.models.SpecimenRequirement;
import lombok.Data;

import java.util.List;

@Data
public class CollectionSpecimenRequirements extends Collection {
    private Pageable pageable;
    private List<SpecimenRequirement> content;
}
