package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.Objects;

@Data
public class LabDto {
    private Integer id;
    private String name;
    private String countryName;
    private String countryCode;
    private String url;
    private String type;
    private String ownershipType;
    private ArrayList<Object> npis;
    private ArrayList<Address> addresses;
    private ArrayList<Object> platforms;
    private ArrayList<LabTestDto> labTests;
    private double diseaseMarketSharePercent;
    private double testMarketSharePercent;
    private double totalDiseaseVolume;
    private double methodMarketSharePercent;
    private Object diaceuticsId;

    public LabTestDto getAssayByNameAndId(String name, int id) {
        return labTests.stream()
                .filter(labTest -> labTest.getName().equals(name) && labTest.getId() == id)
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LabDto)) return false;
        LabDto labDto = (LabDto) o;
        return Objects.equals(getName(), labDto.getName()) &&
                Objects.equals(getCountryCode(), labDto.getCountryCode()) &&
                Objects.equals(getUrl(), labDto.getUrl()) &&
                Objects.equals(getType(), labDto.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCountryCode(), getUrl(), getType());
    }
}
