package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.SortTwo;
import lombok.Data;

@Data
public class Collection {
    private int totalPages;
    private int totalElements;
    private boolean last;
    private boolean first;
    private SortTwo sort;
    private int numberOfElements;
    private int size;
    private int number;
    private boolean empty;
}
