package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
public class DiseaseVolumeDto {
    private int id;
    private int year;
    private int originalDiseasePk;
    private String timePeriod;
    private int timePeriodValue;
    private Date asOf;
    private double diseaseVolume;
    private Disease disease;
    private List<BiomarkerVolume> biomarkerVolumes;
    private String claimQuarterGroupId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DiseaseVolumeDto)) return false;
        DiseaseVolumeDto diseaseVolumeDto = (DiseaseVolumeDto) o;
        return getYear() == diseaseVolumeDto.getYear()
                && getTimePeriodValue() == diseaseVolumeDto.getTimePeriodValue()
                && Double.compare(diseaseVolumeDto.getDiseaseVolume(), getDiseaseVolume()) == 0
                && Objects.equals(getTimePeriod(), diseaseVolumeDto.getTimePeriod())
                && Objects.equals(getDisease(), diseaseVolumeDto.getDisease())
                && Objects.equals(getBiomarkerVolumes(), diseaseVolumeDto.getBiomarkerVolumes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getYear(), getTimePeriod(), getTimePeriodValue(),
                getDiseaseVolume(), getDisease(), getBiomarkerVolumes());
    }
}
