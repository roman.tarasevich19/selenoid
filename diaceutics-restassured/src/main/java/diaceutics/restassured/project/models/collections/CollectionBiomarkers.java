package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.BiomarkerDto;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionBiomarkers extends Collection {
    private Pageable pageable;
    private List<BiomarkerDto> content;
}
