package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class PhysicianMappingRequest {
    private int projectId;
    private int biomarkerId;
    private int diseaseId;
    private int fromYear;
    private int fromMonth;
    private int toYear;
    private int toMonth;
}
