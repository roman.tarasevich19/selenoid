package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Country {
    private String name;
    private String code;
    private int profileCount;
    private String profileType;
}
