package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class TestingDashboardVolumeDto {
    private int id;
    private int labId;
    private String labName;
    private int biomarkerId;
    private List<Integer> biomarkerIds;
    private double normalisedMetricValue;
    private int claimDateId;
    private List<Integer> detection;
    private List<String> sendOut;
    private List<String> platforms;
}
