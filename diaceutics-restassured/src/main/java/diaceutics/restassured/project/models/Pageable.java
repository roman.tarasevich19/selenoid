package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Pageable {
    private Sort sort;
    private int pageNumber;
    private int pageSize;
    private int offset;
    private boolean paged;
    private boolean unpaged;
}
