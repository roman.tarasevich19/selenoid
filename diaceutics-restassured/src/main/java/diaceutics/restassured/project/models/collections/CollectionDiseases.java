package diaceutics.restassured.project.models.collections;


import diaceutics.restassured.project.models.Disease;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionDiseases extends Collection {
    private Pageable pageable;
    private List<Disease> content;
}
