package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Disease {
    private int pk;
    private String id;
    private String name;
}
