package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.Classification;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionClassifications extends Collection {
    private Pageable pageable;
    private List<Classification> content;
}
