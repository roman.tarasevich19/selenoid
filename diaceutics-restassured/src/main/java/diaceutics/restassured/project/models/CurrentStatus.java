package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class CurrentStatus {
    private String status;
    private int yearFrom;
    private int monthFrom;
    private int yearTo;
    private int monthTo;
    private Object logDate;
}
