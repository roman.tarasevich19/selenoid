package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class VariantTwo {
    private int id;
    private String name;
    private BiomarkerDto biomarker;
    private List<Variant> variants;
}
