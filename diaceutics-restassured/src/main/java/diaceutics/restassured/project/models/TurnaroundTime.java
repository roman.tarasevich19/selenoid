package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.Date;
import java.util.Objects;

@Data
public class TurnaroundTime {
    private Date asOf;
    private int days;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TurnaroundTime)) return false;
        TurnaroundTime that = (TurnaroundTime) o;
        return getDays() == that.getDays();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDays());
    }
}
