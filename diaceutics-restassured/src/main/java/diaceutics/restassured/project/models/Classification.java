package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Classification {
    private int id;
    private String name;
    private String type;
}
