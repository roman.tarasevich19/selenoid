package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class Manufacturer {
    private int id;
    private String name;
    private List<Equipment> equipment;
}
