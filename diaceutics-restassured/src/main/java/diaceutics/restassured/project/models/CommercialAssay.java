package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class CommercialAssay {
    private int id;
    private String name;
}
