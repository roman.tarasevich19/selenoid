package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class SubscriptionDto {
    private int projectId;
    private String projectName;
    private String projectDescription;
    private int fromYear;
    private int fromMonth;
    private int toYear;
    private int toMonth;
    private List<BiomarkerDto> biomarkers;
    private List<Disease> diseases;
    private List<Country> countries;
    private List<Method> methods;
}
