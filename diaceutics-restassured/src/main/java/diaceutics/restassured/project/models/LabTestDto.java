package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class LabTestDto {
    private int id;
    private int labId;
    private String name;
    private String description;
    private TurnaroundTime turnaroundTime;
    private Method method;
    private String methodDescription;
    private String sampleReportUrl;
    private ResultFormat resultFormat;
    private TestingPurpose testingPurpose;
    private CommercialAssay commercialAssay;
    private String sendOut;
    private int sendOutLabId;
    private String sendOutLabName;
    private int accuracyPercent;
    private int sensitivityPercent;
    private int precisionPercent;
    private boolean includedInPanel;
    private String panelName;
    private String isBatched;
    private boolean variantsIncluded;
    private List<Disease> diseases;
    private List<BiomarkerDto> biomarkers;
    private List<SpecimenRequirement> specimenRequirements;
    private List<Ontology> ontologies;
    private List<ScoringMethodology> scoringMethodologies;
    private List<VariantTwo> variants;
    private List<DetectGermlineSomatic> detectGermlineSomatics;
    private List<Classification> classifications;
    private List<LabTestStatusDto> statuses;
    private CurrentStatus currentStatus;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LabTestDto)) return false;
        LabTestDto labTestDto = (LabTestDto) o;
        return getLabId() == labTestDto.getLabId() &&
                getSendOutLabId() == labTestDto.getSendOutLabId() &&
                getAccuracyPercent() == labTestDto.getAccuracyPercent() &&
                getSensitivityPercent() == labTestDto.getSensitivityPercent() &&
                getPrecisionPercent() == labTestDto.getPrecisionPercent() &&
                isIncludedInPanel() == labTestDto.isIncludedInPanel() &&
                isVariantsIncluded() == labTestDto.isVariantsIncluded() &&
                Objects.equals(getName(), labTestDto.getName()) &&
                Objects.equals(getDescription(), labTestDto.getDescription()) &&
                Objects.equals(getTurnaroundTime(), labTestDto.getTurnaroundTime()) &&
                Objects.equals(getMethod(), labTestDto.getMethod()) &&
                Objects.equals(getMethodDescription(), labTestDto.getMethodDescription()) &&
                Objects.equals(getSampleReportUrl(), labTestDto.getSampleReportUrl()) &&
                Objects.equals(getResultFormat(), labTestDto.getResultFormat()) &&
                Objects.equals(getTestingPurpose(), labTestDto.getTestingPurpose()) &&
                Objects.equals(getCommercialAssay(), labTestDto.getCommercialAssay()) &&
                Objects.equals(getSendOut(), labTestDto.getSendOut()) &&
                Objects.equals(getSendOutLabName(), labTestDto.getSendOutLabName()) &&
                Objects.equals(getPanelName(), labTestDto.getPanelName()) &&
                Objects.equals(getIsBatched(), labTestDto.getIsBatched()) &&
                Objects.equals(getBiomarkers(), labTestDto.getBiomarkers()) &&
                Objects.equals(getSpecimenRequirements(), labTestDto.getSpecimenRequirements()) &&
                Objects.equals(getOntologies(), labTestDto.getOntologies()) &&
                Objects.equals(getScoringMethodologies(), labTestDto.getScoringMethodologies()) &&
                Objects.equals(getVariants(), labTestDto.getVariants()) &&
                Objects.equals(getDetectGermlineSomatics(), labTestDto.getDetectGermlineSomatics()) &&
                Objects.equals(getClassifications(), labTestDto.getClassifications()) &&
                Objects.equals(getCurrentStatus(), labTestDto.getCurrentStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLabId(), getName(), getDescription(), getTurnaroundTime(),
                getMethod(), getMethodDescription(), getSampleReportUrl(), getResultFormat(),
                getTestingPurpose(), getCommercialAssay(), getSendOut(), getSendOutLabId(),
                getSendOutLabName(), getAccuracyPercent(), getSensitivityPercent(),
                getPrecisionPercent(), isIncludedInPanel(), getPanelName(),
                getIsBatched(), isVariantsIncluded(), getBiomarkers(),
                getSpecimenRequirements(), getOntologies(), getScoringMethodologies(),
                getVariants(), getDetectGermlineSomatics(), getClassifications(),
                getCurrentStatus());
    }
}
