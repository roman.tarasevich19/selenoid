package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class PhysicianMappingControllerRequest {
    private List<Integer> labIds;
    private int biomarkerId;
}
