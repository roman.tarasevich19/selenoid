package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class PreferredLab {
    private int labId;
    private boolean offersBiomarkerTesting;
    private String name;
    private String type;
    private String addressLine1;
    private String city;
    private String region;
    private String postalCode;
}
