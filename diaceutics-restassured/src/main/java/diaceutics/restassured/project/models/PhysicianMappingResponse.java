package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class PhysicianMappingResponse {
    private List<Physician> physicians;

    public int getNumberOfPatientTests() {
        return (int) physicians.stream()
                .mapToDouble(Physician::getDiseasePatientCount).sum();
    }
}
