package diaceutics.restassured.project.models.collections;


import diaceutics.restassured.project.models.Ontology;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionOntologies extends Collection {
    private Pageable pageable;
    private List<Ontology> content;
}
