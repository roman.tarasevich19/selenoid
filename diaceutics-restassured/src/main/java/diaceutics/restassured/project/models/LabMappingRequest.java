package diaceutics.restassured.project.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class LabMappingRequest {
    private int projectId;
    private String countryCode;
    private String criteriaType;
    private int criteriaId;
    private List<Integer> diseaseIds;
    private int fromYear;
    private int fromMonth;
    private int toYear;
    private int toMonth;
}
