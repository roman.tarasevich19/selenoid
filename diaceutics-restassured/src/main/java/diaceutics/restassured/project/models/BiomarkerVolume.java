package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.Date;
import java.util.Objects;

@Data
public class BiomarkerVolume {
    private double volume;
    private BiomarkerDto biomarker;
    private Date asOf;
    private String claimQuarterGroupId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BiomarkerVolume)) return false;
        BiomarkerVolume that = (BiomarkerVolume) o;
        return Double.compare(that.getVolume(), getVolume()) == 0 && Objects.equals(getBiomarker(), that.getBiomarker());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVolume(), getBiomarker());
    }
}
