package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class VolumesFilter {
    private String countryCode;
    private List<Integer> biomarkerIds;
    private List<Integer> diseaseIds;
    private int fromYear;
    private int fromMonth;
    private int toYear;
    private int toMonth;
}
