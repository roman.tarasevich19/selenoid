package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Variant {
    private int id;
    private String name;
}
