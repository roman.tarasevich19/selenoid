package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Sort {
    private boolean sorted;
    private boolean unsorted;
    private boolean empty;
}
