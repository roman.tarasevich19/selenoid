package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;
@Data
public class DiseaseVolume {
    private Object biomarker;
    private Disease disease;
    private List<DiseaseVolumeDto> volumes;
}
