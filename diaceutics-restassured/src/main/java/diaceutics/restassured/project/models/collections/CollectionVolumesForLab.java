package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.DiseaseVolumeDto;
import lombok.Data;

import java.util.List;

@Data
public class CollectionVolumesForLab {
    private Integer labId;
    private List<DiseaseVolumeDto> volumes;
}
