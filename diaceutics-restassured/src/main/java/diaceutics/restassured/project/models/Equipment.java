package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Equipment {
    private int id;
    private String name;
    private Object method;
}
