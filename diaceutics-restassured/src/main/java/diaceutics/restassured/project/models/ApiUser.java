package diaceutics.restassured.project.models;

import diaceutics.selenium.models.BaseModel;
import lombok.Data;

@Data
public class ApiUser extends BaseModel {
    private String email;
    private String firstName;
    private String lastName;
    private String userType;
    private String jobTitle;
    private String organisation;
    private String comment;
}
