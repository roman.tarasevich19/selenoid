package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class LabVolumesInternalDto {
    private LabDto lab;
    private int labId;
    private List<Object> biomarkerVolumes;
    private List<DiseaseVolume> diseaseVolumes;
}
