package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class DetectGermlineSomatic {
    private String id;
    private String name;
}
