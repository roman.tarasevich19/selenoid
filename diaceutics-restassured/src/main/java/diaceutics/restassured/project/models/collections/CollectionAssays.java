package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.LabTestDto;
import lombok.Data;

import java.util.List;

@Data
public class CollectionAssays extends Collection {
    private String pageable;
    private List<LabTestDto> content;
}
