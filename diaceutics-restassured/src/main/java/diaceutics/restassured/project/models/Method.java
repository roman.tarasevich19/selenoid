package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class Method {
    private int id;
    private String name;
    private List<CommercialAssay> commercialAssays;
}
