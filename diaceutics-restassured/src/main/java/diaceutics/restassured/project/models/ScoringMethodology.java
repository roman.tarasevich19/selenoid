package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class ScoringMethodology {
    private String id;
    private String name;
}
