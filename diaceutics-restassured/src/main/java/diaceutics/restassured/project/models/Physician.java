package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Physician {
    private int npi;
    private String hospitalAffiliation;
    private String physicianFirstName;
    private String physicianLastName;
    private String physicianPrimarySpecialty;
    private String physicianPrimarySpecialtyGroup;
    private String physicianAddressLine1;
    private String physicianCity;
    private String physicianState;
    private String physicianZip;
    private String physicianPhoneNumber;
    private String diseasePatientCountQuintile;
    private int diseasePatientCount;
    private double diseaseTestingRatePercentage;
    private int biomarkerPatientCount;
    private double biomarkerTestingRatePercentage;
    private String preferredLabName;
    private String preferredLabClassification;
    private String preferredLabAddressLine1;
    private String preferredLabCity;
    private String preferredLabState;
    private String preferredLabZip;
    private boolean preferredLabOffersBiomarkerTesting;
}
