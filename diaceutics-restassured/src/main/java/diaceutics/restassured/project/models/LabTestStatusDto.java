package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class LabTestStatusDto {
    private String status;
    private int yearFrom;
    private int monthFrom;
    private int yearTo;
    private int monthTo;
    private String logDate;
}
