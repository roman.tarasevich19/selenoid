package diaceutics.restassured.project.models.collections;


import diaceutics.restassured.project.models.Method;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionMethods extends Collection {
    private Pageable pageable;
    private List<Method> content;
}
