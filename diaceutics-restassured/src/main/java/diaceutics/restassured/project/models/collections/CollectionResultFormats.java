package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.Pageable;
import diaceutics.restassured.project.models.ResultFormat;
import lombok.Data;

import java.util.List;

@Data
public class CollectionResultFormats extends Collection {
    private Pageable pageable;
    private List<ResultFormat> content;
}
