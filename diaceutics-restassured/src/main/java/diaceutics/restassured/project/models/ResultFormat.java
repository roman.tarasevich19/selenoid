package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class ResultFormat {
    private String id;
    private String name;
}
