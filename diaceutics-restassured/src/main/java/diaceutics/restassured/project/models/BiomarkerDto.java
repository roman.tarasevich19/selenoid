package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class BiomarkerDto {
    private Integer id;
    private String name;
    private Object originalName;
    private List<Variant> variants;
}
