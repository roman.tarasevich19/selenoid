package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class SpecimenRequirement {
    private String id;
    private String name;
}
