package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class LabsSearchResult {
    private List<LabDto> content;
    private Pageable pageable;
    private int totalPages;
    private int totalElements;
    private boolean last;
    private boolean first;
    private SortTwo sort;
    private int numberOfElements;
    private int size;
    private int number;
    private boolean empty;
}
