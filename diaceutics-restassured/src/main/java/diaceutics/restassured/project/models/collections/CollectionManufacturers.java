package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.Manufacturer;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionManufacturers extends Collection {
    private Pageable pageable;
    private List<Manufacturer> content;
}
