package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.Country;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionCountries extends Collection {
    private Pageable pageable;
    private List<Country> content;
}
