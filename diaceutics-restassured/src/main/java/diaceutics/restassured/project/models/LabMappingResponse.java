package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class LabMappingResponse {
    private List<LabDto> labs;

    public LabDto getLabByNameAndId(String name, int id) {
        return labs.stream()
                .filter(lab -> lab.getName().equals(name) && lab.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public int getLabsWithAssaysForBiomarker() {
        return (int) labs.stream()
                .filter(lab -> !lab.getLabTests().isEmpty()).count();
    }

    public int getNumberOfPatientTests() {
        return (int) labs.stream()
               .mapToDouble(LabDto::getTotalDiseaseVolume).sum();
    }
}
