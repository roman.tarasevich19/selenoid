package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class SortTwo {
    private boolean sorted;
    private boolean unsorted;
    private boolean empty;
}
