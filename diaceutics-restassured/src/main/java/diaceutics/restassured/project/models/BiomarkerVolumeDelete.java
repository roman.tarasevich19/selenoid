package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class BiomarkerVolumeDelete {
    private String claimQuarterGroupId;
}
