package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.Pageable;
import diaceutics.restassured.project.models.TestingPurpose;
import lombok.Data;

import java.util.List;

@Data
public class CollectionTestingPurposes extends Collection {
    private Pageable pageable;
    private List<TestingPurpose> content;
}
