package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class MovingAssayToLab {
    private int labId;
    private List<Integer> assayIds;

    public MovingAssayToLab(Integer labId, Integer... assayId) {
        this.labId = labId;
        this.assayIds = Arrays.asList(assayId);
    }
}
