package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.LabDto;
import diaceutics.restassured.project.models.Pageable;
import lombok.Data;

import java.util.List;

@Data
public class CollectionLabs extends Collection {
    private Pageable pageable;
    private List<LabDto> content;
}
