package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class TestingPurpose {
    private String id;
    private String name;
}
