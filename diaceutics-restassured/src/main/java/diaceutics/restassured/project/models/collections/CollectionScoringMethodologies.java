package diaceutics.restassured.project.models.collections;

import diaceutics.restassured.project.models.Pageable;
import diaceutics.restassured.project.models.ScoringMethodology;
import lombok.Data;

import java.util.List;

@Data
public class CollectionScoringMethodologies extends Collection {
    private Pageable pageable;
    private List<ScoringMethodology> content;
}
