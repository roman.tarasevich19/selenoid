package diaceutics.restassured.project.models;

import lombok.Data;

@Data
public class Ontology {
    private String id;
    private String name;
}
