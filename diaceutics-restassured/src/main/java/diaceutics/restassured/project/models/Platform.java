package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.Objects;

@Data
public class Platform {
    private int id;
    private Manufacturer manufacturer;
    private Equipment equipment;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Platform)) return false;
        Platform platform = (Platform) o;
        return Objects.equals(getManufacturer(), platform.getManufacturer()) &&
                Objects.equals(getEquipment(), platform.getEquipment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getManufacturer(), getEquipment());
    }

}
