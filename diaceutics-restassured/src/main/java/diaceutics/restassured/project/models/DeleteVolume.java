package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.List;

@Data
public class DeleteVolume {
    private List<BiomarkerVolumeDelete> biomarkerVolumes;
    private String claimQuarterGroupId;

    public DeleteVolume(List<BiomarkerVolumeDelete> biomarkerVolumes, String claimQuarterGroupId) {
        this.biomarkerVolumes = biomarkerVolumes;
        this.claimQuarterGroupId = claimQuarterGroupId;
    }
}
