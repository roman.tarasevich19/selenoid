package diaceutics.restassured.project.models;

import lombok.Data;

import java.util.Objects;

@Data
public class Address {
    private int id;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String region;
    private String countryName;
    private String countryCode;
    private String postalCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(getName(), address.getName()) &&
                Objects.equals(getAddressLine1(), address.getAddressLine1()) &&
                Objects.equals(getAddressLine2(), address.getAddressLine2()) &&
                Objects.equals(getCity(), address.getCity()) &&
                Objects.equals(getRegion(), address.getRegion()) &&
                Objects.equals(getCountryCode(), address.getCountryCode()) &&
                Objects.equals(getPostalCode(), address.getPostalCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAddressLine1(),
                getAddressLine2(), getCity(), getRegion(),
                getCountryCode(), getPostalCode());
    }
}
