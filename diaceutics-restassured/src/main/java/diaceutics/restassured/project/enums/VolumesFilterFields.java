package diaceutics.restassured.project.enums;

public enum VolumesFilterFields {
    COUNTRY_CODE("countryCode"),
    BIOMARKER_IDS("biomarkerIds"),
    DISEASE_IDS("diseaseIds"),
    FROM_YEAR("fromYear"),
    FROM_MONTH("fromMonth"),
    TO_YEAR("toYear"),
    TO_MONTH("toMonth");

    private final String modelField;

    VolumesFilterFields(String modelField) {
        this.modelField = modelField;
    }

    public String getModelField() {
        return modelField;
    }

}
