package diaceutics.restassured.project.enums;

public enum PhysicianMappingRequestFields {
    PROJECT_ID("projectId"),
    BIOMARKER_ID("biomarkerId"),
    DISEASE_ID("diseaseId"),
    FROM_YEAR("fromYear"),
    FROM_MONTH("fromMonth"),
    TO_YEAR("toYear"),
    TO_MONTH("toMonth");

    private final String modelField;

    PhysicianMappingRequestFields(String modelField) {
        this.modelField = modelField;
    }

    public String getModelField() {
        return modelField;
    }

}
