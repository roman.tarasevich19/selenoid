package diaceutics.restassured.project.enums;

public enum LabMappingRequestFields {
    PROJECT_ID("projectId"),
    COUNTRY_CODE("countryCode"),
    CRITERIA_TYPE("criteriaType"),
    CRITERIA_ID("criteriaId"),
    DISEASE_IDS("diseaseIds"),
    FROM_YEAR("fromYear"),
    FROM_MONTH("fromMonth"),
    TO_YEAR("toYear"),
    TO_MONTH("toMonth");

    private final String modelField;

    LabMappingRequestFields(String modelField) {
        this.modelField = modelField;
    }

    public String getModelField() {
        return modelField;
    }

}
