package diaceutics.restassured.project.enums;

public enum ApiLabFields {
    LAB_ID("id"),
    NAME("name"),
    COUNTRY_CODE("countryCode"),
    URL("url"),
    TYPE("type");

    private final String modelField;

    ApiLabFields(String modelField) {
        this.modelField = modelField;
    }

    public String getModelField() {
        return modelField;
    }

}
