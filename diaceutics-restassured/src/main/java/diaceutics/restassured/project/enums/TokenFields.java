package diaceutics.restassured.project.enums;

public enum TokenFields {
    GRANT_TYPE("grant_type"),
    USER_NAME("username"),
    PASSWORD("password"),
    CLIENT_ID("client_id"),
    CLIENT_SECRET("client_secret"),
    AUDIENCE("audience"),
    ACCESS_TOKEN("access_token");

    private final String modelField;

    TokenFields(String modelField) {
        this.modelField = modelField;
    }

    public String getModelField() {
        return modelField;
    }

}
