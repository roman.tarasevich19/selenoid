package diaceutics.restassured.project.enums;

public enum AddressFields {
    ADDRESS_ID("id"),
    NAME("name"),
    ADDRESS_LINE_ONE("addressLine1"),
    ADDRESS_LINE_TWO("addressLine2"),
    CITY("city"),
    REGION("region"),
    COUNTRY_CODE("countryCode"),
    POSTAL_CODE("postalCode");

    private final String modelField;

    AddressFields(String modelField) {
        this.modelField = modelField;
    }

    public String getModelField() {
        return modelField;
    }

}
