package diaceutics.restassured.project.enums;

public enum ApiVolumeFields {
    CLAIM_QUARTER_GROUP_ID("claimQuarterGroupId"),
    YEAR("year"),
    TIME_PERIOD_VALUE("timePeriodValue"),
    VOLUME("volume"),
    BIOMARKER("biomarker"),
    DISEASE("disease");

    private final String modelField;

    ApiVolumeFields(String modelField) {
        this.modelField = modelField;
    }

    public String getModelField() {
        return modelField;
    }

}
