package diaceutics.restassured.project.constants.paths;

public class RegistrationPath {

    private RegistrationPath() {
    }

    public static final String BASE_PATH = "/v1/";
}
