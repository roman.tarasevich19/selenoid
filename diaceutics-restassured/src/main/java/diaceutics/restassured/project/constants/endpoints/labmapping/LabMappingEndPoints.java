package diaceutics.restassured.project.constants.endpoints.labmapping;

public class LabMappingEndPoints {

    private LabMappingEndPoints() {
    }

    public static final String GENERATES_A_LAB_MAPPING = "mapping";
    public static final String GENERATES_A_TESTING_DASHBOARD_MAPPING = "mapping-testing-dashboard";
}
