package diaceutics.restassured.project.constants.endpoints.physicianmapping;

public class SubscriptionsEndPoints {

    private SubscriptionsEndPoints() {
    }

    public static final String RETURNS_A_COLLECTION_OF_SUBSCRIPTIONS_FOR_THE_AUTHENTICATED_PRINCIPLE = "subscriptions";
}
