package diaceutics.restassured.project.constants.endpoints.profiles;

public class MetadataEndPoints {

    private MetadataEndPoints() {
    }

    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_BIOMARKERS = "biomarkers";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_CLASSIFICATIONS = "classifications";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_COUNTRIES_FOR_A_GIVEN_PROFILE_TYPE = "countries";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_DISEASES = "diseases";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_METHODS = "methods";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_ONTOLOGIES = "ontologies";
    public static final String RETURNS_PAGEABLE_LIST_OF_PLATFORM_MANUFACTURERS = "platforms/manufacturers";
    public static final String RETURNS_PAGEABLE_LIST_OF_RESULT_FORMATS = "result_formats";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_DETECT_GERMLINE_SOMATICS = "detect_germline_somatics";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_SCORING_METHODOLOGIES = "scoring-methodologies";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_SPECIMEN_REQUIREMENTS = "specimen_requirements";
    public static final String RETURNS_PAGEABLE_LIST_OF_ALL_TESTING_PURPOSES = "testing_purposes";
    public static final String RETURNS_LIST_OF_ALL_BIOMARKERS_FOR_THE_SELECTED_COMMERCIAL_ASSAY = "biomarkers/%s";
    public static final String RETURNS_LIST_OF_ALL_BIOMARKERS_FOR_THE_SELECTED_COMMERCIAL_ASSAY_SECOND_REQUEST =
            "commercial-assays/%s/biomarkers";
}
