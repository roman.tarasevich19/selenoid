package diaceutics.restassured.project.constants.endpoints.profiles;

public class VolumesEndPoints {

    private VolumesEndPoints() {
    }

    public static final String CREATES_NEW_LAB_VOLUME = "/labs/%s/volumes/diseases";
    public static final String RETURNS_LAB_VOLUME = CREATES_NEW_LAB_VOLUME;
    public static final String RETURNS_COLLECTION_OF_LAB_VOLUMES = "/labs/%s/volumes";
    public static final String UPDATES_AN_EXISTING_VOLUME = CREATES_NEW_LAB_VOLUME;
    public static final String DELETE_VOLUME_FOR_LAB = CREATES_NEW_LAB_VOLUME;
    public static final String POST_FOR_VOLUMES = "labs/volumes";
}
