package diaceutics.restassured.project.constants.endpoints.profiles;

public class LabsEndPoints {

    private LabsEndPoints() {
    }

    public static final String CREATE_LAB = "labs";
    public static final String RETURNS_COLLECTION_OF_LABS = CREATE_LAB;
    public static final String RETURNS_LAB_WITH_GIVEN_ID = "labs/%s";
    public static final String UPDATES_AN_EXISTING_LAB = RETURNS_LAB_WITH_GIVEN_ID;
    public static final String UPDATES_AN_EXISTING_ADDRESS = "labs/%s/addresses/%s";
    public static final String CREATES_NEW_LAB_TEST_FOR_LAB = "labs/%s/tests";
    public static final String RETURNS_LAB_TEST_WITH_GIVEN_LAB_ID_AND_LAB_TEST_I_DS = "labs/%s/tests/%s";
    public static final String UPDATES_LAB_TEST = RETURNS_LAB_TEST_WITH_GIVEN_LAB_ID_AND_LAB_TEST_I_DS;
    public static final String DELETE_LAB_TEST = RETURNS_LAB_TEST_WITH_GIVEN_LAB_ID_AND_LAB_TEST_I_DS;
    public static final String RETURNS_COLLECTION_OF_LAB_TESTS_FOR_LAB = CREATES_NEW_LAB_TEST_FOR_LAB;
    public static final String RETURNS_COLLECTION_OF_LAB_NAMES_AND_THEIR_I_DS = "labnames-sendout";
    public static final String MOVING_ASSAY_TO_LAB = "labs/%s/tests/move";
}

