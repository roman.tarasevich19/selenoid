package diaceutics.restassured.project.constants.endpoints.profiles;

public class TestingDashboardEndPoints {

    private TestingDashboardEndPoints() {
    }

    public static final String RETURNS_DATA_FOR_LAB_TESTING_CHARTS = "/dashboards/testing/volumes";
    public static final String RETURNS_DATA_FOR_VERSUS_CHARTS = RETURNS_DATA_FOR_LAB_TESTING_CHARTS + "/versus";
    public static final String RETURNS_DATA_FOR_CLAIMS_CHARTS = RETURNS_DATA_FOR_LAB_TESTING_CHARTS + "/claims";
    public static final String RETURNS_DATA_FOR_PLATFORM_CHARTS = RETURNS_DATA_FOR_LAB_TESTING_CHARTS + "/platform";
}
