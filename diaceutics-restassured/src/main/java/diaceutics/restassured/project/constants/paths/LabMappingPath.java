package diaceutics.restassured.project.constants.paths;

public class LabMappingPath {

    private LabMappingPath() {
    }

    public static final String BASE_PATH = "/lab-mapping/v1/";
}
