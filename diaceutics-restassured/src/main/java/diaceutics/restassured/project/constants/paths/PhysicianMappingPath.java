package diaceutics.restassured.project.constants.paths;

public class PhysicianMappingPath {

    private PhysicianMappingPath() {
    }

    public static final String BASE_PATH = "/physician-mapping/v1/";
}
