package diaceutics.restassured.project.constants.endpoints.profiles;

public class RegistrationEndPoints {

    private RegistrationEndPoints() {
    }

    public static final String CREATE_USER = "registration";
}
