package diaceutics.restassured.project.constants.paths;

public class ProfilesPath {

    private ProfilesPath() {
    }

    public static final String BASE_PATH = "/profiles/v1/";
}
