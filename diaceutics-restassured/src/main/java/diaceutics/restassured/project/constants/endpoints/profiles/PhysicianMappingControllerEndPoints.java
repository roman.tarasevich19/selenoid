package diaceutics.restassured.project.constants.endpoints.profiles;

public class PhysicianMappingControllerEndPoints {

    private PhysicianMappingControllerEndPoints() {
    }

    public static final String RETURNS_PREFERRED_LABS = "mappings/physician/preferred_labs";
}
