package diaceutics.restassured.project.constants.endpoints.physicianmapping;

public class PhysicianMappingEndPoints {

    private PhysicianMappingEndPoints() {
    }

    public static final String GENERATES_A_PHYSICIAN_MAPPING = "mapping";
}
