package diaceutics.cucumber.hooks;

import diaceutics.cucumber.enums.users.ApiPermissionsUsers;
import diaceutics.cucumber.enums.users.UiPermissionsUsers;
import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.User;
import io.cucumber.java.Before;

import javax.inject.Inject;

public class DataReaderHooksPermissions {

    private final ScenarioContext scenarioContext;

    @Inject
    public DataReaderHooksPermissions(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
    }

    @Before(value = "@ApiPermissions", order = 0)
    public void getUserInfo() {

        for (ApiPermissionsUsers permissionsUsers : ApiPermissionsUsers.values()) {
            User user = DataReader.getAPIPermissionsUserInfoForByUserName(permissionsUsers.getUserName());
            user.setEmail(System.getProperty(permissionsUsers.getEmailProperty()));
            user.setPassword(System.getProperty(permissionsUsers.getPasswordProperty()));
            scenarioContext.add(permissionsUsers.getScenarioContextName(), user);
        }
    }

    @Before(value = "@UiPermissions", order = 0)
    public void getUserInfoForUiPermissions() {
        for (UiPermissionsUsers permissionsUsers : UiPermissionsUsers.values()) {
            User user = DataReader.getUIPermissionsUserInfoForByUserName(permissionsUsers.getUserName());
            user.setEmail(System.getProperty(permissionsUsers.getEmailProperty()));
            user.setPassword(System.getProperty(permissionsUsers.getPasswordProperty()));
            scenarioContext.add(permissionsUsers.getScenarioContextName(), user);
        }
    }
}
