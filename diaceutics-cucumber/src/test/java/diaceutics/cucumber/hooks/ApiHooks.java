package diaceutics.cucumber.hooks;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.utilities.RequestSpecHelper;
import diaceutics.selenium.configuration.Configuration;
import diaceutics.selenium.models.User;
import io.cucumber.java.Before;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;

import javax.inject.Inject;

public class ApiHooks {

    private final ScenarioContext scenarioContext;

    @Inject
    public ApiHooks(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
    }

    @Before(value = "@api or @uiAndApi", order = 2)
    public void before() {
        User user = scenarioContext.get("ADMIN");
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = RequestSpecHelper.getRequestSpec(user);
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Before(value = "@ApiNewRegistration", order = 3)
    public void beforeRegistration() {
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = RequestSpecHelper
                .getDefaultRequestSpec()
                .baseUri(Configuration.getUrl(Configuration.REGISTRATION_API_URL))
                .contentType(ContentType.JSON);
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
