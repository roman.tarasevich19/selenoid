package diaceutics.cucumber.hooks;

import aquality.selenium.browser.AqualityServices;
import diaceutics.selenium.configuration.Configuration;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class BrowserHooks {

    @Before(value = "@ui", order = 0)
    public void goToStartUrl() {
        AqualityServices.getBrowser().goTo(Configuration.getUrl(Configuration.START_URL));
    }

    @Before(value = "@NewUserRegistration", order = 0)
    public void goToRegistrationUrl() {
        AqualityServices.getBrowser().goTo(Configuration.getUrl(Configuration.REGISTRATION_UI_URL));
    }

    @After(value = "@ui or @NewUserRegistration", order = 0)
    public void closeBrowser() {
        if (AqualityServices.isBrowserStarted()) {
            AqualityServices.getBrowser().quit();
        }
    }
}
