package diaceutics.cucumber.hooks;

import diaceutics.cucumber.utilities.jirautil.JiraUtil;
import diaceutics.selenium.configuration.Environment;
import diaceutics.selenium.utilities.IScreenshotProvider;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.Status;
import org.apache.commons.io.FileUtils;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

public class ScreenshotHooks {

    private final IScreenshotProvider screenshotProvider;
    private File screenFile = new File(new File("").getAbsolutePath() + "/target/allure-results");

    @Inject
    public ScreenshotHooks(IScreenshotProvider screenshotProvider) {
        this.screenshotProvider = screenshotProvider;
    }

    @After(value = "@ui", order = 1)
    public void takeScreenshot(Scenario scenario) {
        if (scenario.getStatus() != Status.PASSED) {
            scenario.attach(screenshotProvider.takeScreenshot(), "image/png", "screenshot");
            byte[] screen = screenshotProvider.takeScreenshot();
            String screenPathToSave = screenFile.getPath() + File.separator + scenario.getName()
                    .replaceAll(" ", "_").replaceAll(JiraUtil.ONLY_LETTERS_DIGITS, "_")
                    + "_" + Environment.getEnvironmentName() + ".jpg";
            try {
                FileUtils.writeByteArrayToFile(new File(screenPathToSave), screen);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
