package diaceutics.cucumber.hooks;

import diaceutics.cucumber.enums.users.ApiPermissionsUsers;
import diaceutics.cucumber.enums.users.Users;
import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.User;
import io.cucumber.java.Before;

import javax.inject.Inject;

public class DataReaderHooks {

    private final ScenarioContext scenarioContext;

    @Inject
    public DataReaderHooks(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
    }

    @Before(order = 0)
    public void getUserInfo() {

        for (Users users : Users.values()) {
            User user = DataReader.getUserInfoByUserName(users.getUserName());
            user.setEmail(System.getProperty(users.getEmailProperty()));
            user.setPassword(System.getProperty(users.getPasswordProperty()));
            scenarioContext.add(users.getScenarioContextName(), user);
        }

        User userForEditPersonalData = DataReader.getUserInfoByUserName("userForEditPersonalData");
        scenarioContext.add("user.edit.data", userForEditPersonalData);

        User changePasswordUser = DataReader.getUserInfoByUserName("userChangePassword");
        scenarioContext.add("changePasswordUser", changePasswordUser);
    }
}
