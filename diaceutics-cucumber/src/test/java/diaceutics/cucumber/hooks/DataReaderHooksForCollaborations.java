package diaceutics.cucumber.hooks;

import diaceutics.cucumber.enums.collaborations.Collaborations;
import diaceutics.cucumber.enums.users.CollaborationUsers;
import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.models.User;
import io.cucumber.java.Before;

import javax.inject.Inject;

public class DataReaderHooksForCollaborations {

    private final ScenarioContext scenarioContext;

    @Inject
    public DataReaderHooksForCollaborations(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
    }

    @Before(value = "@Collaboration", order = 0)
    public void getUserAndCollaborationInfo() {

        for (CollaborationUsers users : CollaborationUsers.values()) {
            User user = new User();
            user.setEmail(System.getProperty(users.getEmailProperty()));
            user.setPassword(System.getProperty(users.getPasswordProperty()));
            scenarioContext.add(users.getScenarioContextName(), user);
        }

        for (Collaborations collaboration : Collaborations.values()) {
            Collaboration testCollaboration =
                    DataReader.getCollaborationInfoByCollaborationName(collaboration.getCollaborationName());

            scenarioContext.add(collaboration.getScenarioContextName(), testCollaboration);
        }
    }
}
