package diaceutics.cucumber.hooks;

import aquality.selenium.browser.AqualityServices;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class LogHooks {
    @Before(order = 0)
    public void initScenario(Scenario scenario) {
        AqualityServices.getLogger().info("<============================================>");
        AqualityServices.getLogger().info("Start scenario: " + scenario.getName());
    }

    @After(order = 0)
    public void logResults(Scenario scenario) {
        AqualityServices.getLogger().info("End scenario: " + scenario.getName());
        AqualityServices.getLogger().info("Scenario status: " + scenario.getStatus());
        AqualityServices.getLogger().info("<============================================>");
    }
}
