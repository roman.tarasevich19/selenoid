package diaceutics.cucumber.hooks;

import diaceutics.selenium.utilities.FileUtil;
import io.cucumber.java.After;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class FileHooks {

    @SneakyThrows
    @After(value = "not @NotClearDownload", order = 1)
    public void cleanDirectory() {
        File file = new File(FileUtil.getDownloadDirPath());
        if (file.exists()) {
            FileUtils.cleanDirectory(file);
        }
    }
}
