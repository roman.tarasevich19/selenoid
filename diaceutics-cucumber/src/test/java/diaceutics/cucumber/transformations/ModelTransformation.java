package diaceutics.cucumber.transformations;

import diaceutics.restassured.project.models.LabTestStatusDto;
import diaceutics.selenium.enums.date.DateFormat;
import diaceutics.selenium.models.StatusModel;
import diaceutics.selenium.utilities.DateUtil;

public class ModelTransformation {

    public static StatusModel transformToUiStatus(LabTestStatusDto labTestStatusDto) {
        String updated = labTestStatusDto.getLogDate() == null ? "--" :
                DateUtil.convertDateFormat(labTestStatusDto.getLogDate(),
                        DateFormat.YYYY_MM_DD_T_HH_MM_SS_SSS.toString(),
                        DateFormat.D_MMMM_YYYY_H_MM.toString());

        String active = labTestStatusDto.getYearFrom() == 0 && labTestStatusDto.getMonthFrom() == 0 ? "--" :
                String.join(" ", DateUtil.convertNumberToMonth(labTestStatusDto.getMonthFrom()),
                        String.valueOf(labTestStatusDto.getYearFrom()));

        String inactive = labTestStatusDto.getYearTo() == 0 && labTestStatusDto.getMonthTo() == 0 ? "--" :
                String.join(" ", DateUtil.convertNumberToMonth(labTestStatusDto.getMonthTo()),
                        String.valueOf(labTestStatusDto.getYearTo()));

        StatusModel status = new StatusModel();
        status.setStatus(labTestStatusDto.getStatus());
        status.setUpdated(updated);
        status.setActive(active);
        status.setInactive(inactive);
        return status;
    }
}
