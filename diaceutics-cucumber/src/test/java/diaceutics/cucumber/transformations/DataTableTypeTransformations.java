package diaceutics.cucumber.transformations;

import diaceutics.restassured.project.enums.*;
import diaceutics.restassured.project.models.*;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.utilities.MailUtil;
import diaceutics.selenium.utilities.RegExUtil;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.DataTableType;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataTableTypeTransformations {

    @DataTableType
    public LabDto getApiLab(Map<String, String> entry) {
        LabDto labDto = new LabDto();
        labDto.setName(entry.get(ApiLabFields.NAME.getModelField()) + TimeUtil.getTimestamp());
        labDto.setCountryCode(entry.get(ApiLabFields.COUNTRY_CODE.getModelField()));
        labDto.setType(entry.get(ApiLabFields.TYPE.getModelField()));
        return labDto;
    }

    @DataTableType
    public Address getAddress(Map<String, String> entry) {
        Address address = new Address();
        address.setAddressLine1(entry.get(AddressFields.ADDRESS_LINE_ONE.getModelField()));
        address.setAddressLine2(entry.get(AddressFields.ADDRESS_LINE_TWO.getModelField()));
        address.setCity(entry.get(AddressFields.CITY.getModelField()));
        address.setRegion(entry.get(AddressFields.REGION.getModelField()));
        address.setCountryCode(entry.get(AddressFields.COUNTRY_CODE.getModelField()));
        address.setPostalCode(entry.get(AddressFields.POSTAL_CODE.getModelField()));
        return address;
    }

    @DataTableType
    public LabMappingRequest getLabMappingRequest(Map<String, String> entry) {
        return LabMappingRequest.builder()
                .projectId(Integer.parseInt(entry.get(LabMappingRequestFields.PROJECT_ID.getModelField())))
                .criteriaId(Integer.parseInt(entry.get(LabMappingRequestFields.CRITERIA_ID.getModelField())))
                .fromYear(Integer.parseInt(entry.get(LabMappingRequestFields.FROM_YEAR.getModelField())))
                .fromMonth(Integer.parseInt(entry.get(LabMappingRequestFields.FROM_MONTH.getModelField())))
                .toYear(Integer.parseInt(entry.get(LabMappingRequestFields.TO_YEAR.getModelField())))
                .toMonth(Integer.parseInt(entry.get(LabMappingRequestFields.TO_MONTH.getModelField())))
                .countryCode(entry.get(LabMappingRequestFields.COUNTRY_CODE.getModelField()))
                .criteriaType(entry.get(LabMappingRequestFields.CRITERIA_TYPE.getModelField()))
                .diseaseIds(Arrays.stream(entry.get(LabMappingRequestFields.DISEASE_IDS.getModelField())
                        .split(",")).map(Integer::parseInt).collect(Collectors.toList()))
                .build();
    }

    @DataTableType
    public PhysicianMappingRequest getPhysicianMappingRequest(Map<String, String> entry) {
        PhysicianMappingRequest physicianMappingRequest = new PhysicianMappingRequest();
        physicianMappingRequest.setProjectId(Integer.parseInt(entry.get(PhysicianMappingRequestFields.PROJECT_ID.getModelField())));
        physicianMappingRequest.setBiomarkerId(Integer.parseInt(entry.get(PhysicianMappingRequestFields.BIOMARKER_ID.getModelField())));
        physicianMappingRequest.setDiseaseId(Integer.parseInt(entry.get(PhysicianMappingRequestFields.DISEASE_ID.getModelField())));
        physicianMappingRequest.setFromYear(Integer.parseInt(entry.get(PhysicianMappingRequestFields.FROM_YEAR.getModelField())));
        physicianMappingRequest.setFromMonth(Integer.parseInt(entry.get(PhysicianMappingRequestFields.FROM_MONTH.getModelField())));
        physicianMappingRequest.setToYear(Integer.parseInt(entry.get(PhysicianMappingRequestFields.TO_YEAR.getModelField())));
        physicianMappingRequest.setToMonth(Integer.parseInt(entry.get(PhysicianMappingRequestFields.TO_MONTH.getModelField())));
        return physicianMappingRequest;
    }

    @DataTableType
    public VolumesFilter getVolumesFilter(Map<String, String> entry) {
        VolumesFilter volumesFilter = new VolumesFilter();
        volumesFilter.setCountryCode(entry.get(VolumesFilterFields.COUNTRY_CODE.getModelField()));
        List<Integer> biomarkerId = new ArrayList<>();
        biomarkerId.add(Integer.parseInt(entry.get(VolumesFilterFields.BIOMARKER_IDS.getModelField())));
        volumesFilter.setBiomarkerIds(biomarkerId);
        List<Integer> diseaseIds = new ArrayList<>();
        diseaseIds.add(Integer.parseInt(entry.get(VolumesFilterFields.DISEASE_IDS.getModelField())));
        volumesFilter.setDiseaseIds(diseaseIds);
        volumesFilter.setFromYear(Integer.parseInt(entry.get(VolumesFilterFields.FROM_YEAR.getModelField())));
        volumesFilter.setFromMonth(Integer.parseInt(entry.get(VolumesFilterFields.FROM_MONTH.getModelField())));
        volumesFilter.setToYear(Integer.parseInt(entry.get(VolumesFilterFields.TO_YEAR.getModelField())));
        volumesFilter.setToMonth(Integer.parseInt(entry.get(VolumesFilterFields.TO_MONTH.getModelField())));
        return volumesFilter;
    }

    @DataTableType
    public ApiUser getApiUser(Map<String, String> entry) {
        ApiUser user = new ApiUser();
        entry.forEach((field, value) -> {
            value = value.contains(FieldValues.RANDOM.toString()) ? RandomStringUtils.randomAlphabetic(Integer.parseInt(RegExUtil.getNumbersFromString(value))) : value;
            value = value.equals(FieldValues.MAILOAUR.toString()) ? MailUtil.createMailAddress() : value;
            user.setReflectionFieldValue(field, value);
        });
        return user;
    }
}
