package diaceutics.cucumber.runners;

import diaceutics.cucumber.objectfactory.CustomObjectFactory;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        features = {"src/test/java/diaceutics/cucumber/features"},
        glue = {
                "diaceutics.cucumber.hooks",
                "diaceutics.cucumber.transformations",
                "diaceutics.cucumber.stepdefinitions"
        },
        plugin = {"io.qameta.allure.cucumber5jvm.AllureCucumber5Jvm", "json:target/cucumber-reports/cucumber.json"},
        strict = true,
        objectFactory = CustomObjectFactory.class
)
public class TestRunner extends CustomRunner {

    @Override
    @DataProvider(parallel = true)
    public Object[][] parallelScenarios() {
        return super.parallelScenarios();
    }

    @Override
    @DataProvider(parallel = false)
    public Object[][] serialScenarios() {
        return super.serialScenarios();
    }

}
