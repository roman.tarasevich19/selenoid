package diaceutics.cucumber.enums.collaborations;

import lombok.Getter;

@Getter
public enum Collaborations {
    TEST_COLLABORATION("Test collaboration", "Test collaboration"),
    TEST_COLLABORATION_TWO("Test collaboration two", "Test collaboration two"),
    ADMIN_COLLABORATION("Admin collaboration", "admin collaboration");

    private final String collaborationName;
    private final String scenarioContextName;

    Collaborations(String collaborationName, String scenarioContextName) {
        this.collaborationName = collaborationName;
        this.scenarioContextName = scenarioContextName;
    }
}
