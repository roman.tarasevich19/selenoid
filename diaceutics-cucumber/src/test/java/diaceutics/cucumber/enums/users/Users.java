package diaceutics.cucumber.enums.users;

import lombok.Getter;

@Getter
public enum Users {
    BACKOFFICE("backOffice", "BACKOFFICE", "backoffice.login", "backoffice.password"),
    ADMIN("admin", "ADMIN", "admin.login", "admin.password");

    private final String scenarioContextName;
    private final String emailProperty;
    private final String passwordProperty;
    private final String userName;

    Users(String userName, String scenarioContextName, String emailProperty, String passwordProperty) {
        this.userName = userName;
        this.scenarioContextName = scenarioContextName;
        this.emailProperty = emailProperty;
        this.passwordProperty = passwordProperty;
    }
}
