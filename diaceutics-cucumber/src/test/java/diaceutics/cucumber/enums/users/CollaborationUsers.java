package diaceutics.cucumber.enums.users;

import lombok.Getter;

@Getter
public enum CollaborationUsers {
    USER_FOR_TESTING_COLLABORATION("User for testing collaboration",
            "User for testing collaboration",
            "collaboration.user.login",
            "collaboration.user.password"),

    USER_WITHOUT_COLLABORATION("User without collaboration",
            "User without collaboration",
            "without.collaboration.user.login",
            "collaboration.user.password");

    private final String scenarioContextName;
    private final String emailProperty;
    private final String passwordProperty;
    private final String userName;

    CollaborationUsers(String userName, String scenarioContextName, String emailProperty, String passwordProperty) {
        this.userName = userName;
        this.scenarioContextName = scenarioContextName;
        this.emailProperty = emailProperty;
        this.passwordProperty = passwordProperty;
    }
}
