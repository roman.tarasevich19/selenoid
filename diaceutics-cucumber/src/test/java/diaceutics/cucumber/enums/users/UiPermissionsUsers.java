package diaceutics.cucumber.enums.users;

import lombok.Getter;

@Getter
public enum UiPermissionsUsers {
    AM_LABS_READ("labs.read", "labs.read", "labs.read", "uiPassword"),
    AM_LABS_READ_OWN("labs.read.own", "labs.read.own", "labs.read.own", "uiPassword"),
    AM_LABS_UPDATE("labs.update", "labs.update", "labs.update", "uiPassword"),
    AM_LABS_UPDATE_OWN("labs.update.own", "labs.update.own", "labs.update.own", "uiPassword"),
    AM_ADDRESSES_UPDATE("addresses.update", "addresses.update", "addresses.update", "uiPassword"),
    AM_ADDRESSES_UPDATE_OWN("addresses.update.own", "addresses.update.own", "addresses.update.own", "uiPassword"),
    AM_ASSAYS_CREATE("assays.create", "assays.create", "assays.create", "uiPassword"),
    AM_ASSAYS_CREATE_OWN("assays.create.own", "assays.create.own", "assays.create.own", "uiPassword"),
    AM_ASSAYS_UPDATE("assays.update", "assays.update", "assays.update", "uiPassword"),
    AM_ASSAYS_UPDATE_OWN("assays.update.own", "assays.update.own", "assays.update.own", "uiPassword"),
    AM_ASSAYS_READ("assays.read", "assays.read", "assays.read", "uiPassword"),
    AM_ASSAYS_READ_OWN("assays.read.own", "assays.read.own", "assays.read.own", "uiPassword"),
    AM_ASSAYS_DELETE("assays.delete", "assays.delete", "assays.delete", "uiPassword"),
    AM_VOLUME_READ("volumes.read", "volumes.read", "volumes.read", "uiPassword"),
    AM_VOLUME_READ_OWN("volumes.read.own", "volumes.read.own", "volumes.read.own", "uiPassword"),
    AM_VOLUME_CREATE("volumes.create", "volumes.create","volumes.create","uiPassword"),
    AM_VOLUME_CREATE_OWN("volumes.create.own", "volumes.create.own", "volumes.create.own", "uiPassword"),
    AM_VOLUME_UPDATE("volumes.update", "volumes.update", "volumes.update", "uiPassword"),
    AM_VOLUME_UPDATE_OWN("volumes.update.own", "volumes.update.own", "volumes.update.own", "uiPassword"),
    AM_VOLUME_DELETE("volumes.delete", "volumes.delete", "volumes.delete", "uiPassword"),
    LABS_LINK_TO_ASSAY("labs.link.to.assay", "labs.link.to.assay","labs.link.to.assay", "uiPassword"),
    WITHOUT_ASSAY_CREATE_UPDATE("without.assay.create.update", "without.assay.create.update","without.assay.create.update", "uiPassword"),
    USER_WITHOUT_PERMISSIONS("user without permissions", "user without permissions", "without.permissions", "permission.password");

    private final String scenarioContextName;
    private final String userName;
    private final String passwordProperty;
    private final String emailProperty;

    UiPermissionsUsers(String userName, String scenarioContextName, String emailProperty, String passwordProperty) {
        this.userName = userName;
        this.scenarioContextName = scenarioContextName;
        this.emailProperty = emailProperty;
        this.passwordProperty = passwordProperty;
    }
}
