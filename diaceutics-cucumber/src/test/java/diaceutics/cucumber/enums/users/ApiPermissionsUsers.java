package diaceutics.cucumber.enums.users;

import lombok.Getter;

@Getter
public enum ApiPermissionsUsers {
    AM_LABS_CREATE("am.labs.create", "am.labs.create", "am.labs.create", "permission.password"),
    AM_LABS_UPDATE("am.labs.update", "am.labs.update", "am.labs.update", "permission.password"),
    AM_LABS_UPDATE_OWN("am.labs.update.own", "am.labs.update.own", "am.labs.update.own", "permission.password"),
    AM_LABS_READ("am.labs.read", "am.labs.read", "am.labs.read", "permission.password"),
    AM_LABS_READ_OWN("am.labs.read.own", "am.labs.read.own", "am.labs.read.own", "permission.password"),
    AM_ADDRESSES_UPDATE("am.addresses.update", "am.addresses.update", "am.addresses.update", "permission.password"),
    AM_ADDRESSES_UPDATE_OWN("am.addresses.update.own", "am.addresses.update.own", "am.addresses.update.own", "permission.password"),
    AM_ASSAYS_CREATE("am.assays.create", "am.assays.create", "am.assays.create", "permission.password"),
    AM_ASSAYS_CREATE_OWN("am.assays.create.own", "am.assays.create.own", "am.assays.create.own", "permission.password"),
    AM_ASSAYS_UPDATE("am.assays.update", "am.assays.update", "am.assays.update", "permission.password"),
    AM_ASSAYS_UPDATE_OWN("am.assays.update.own", "am.assays.update.own", "am.assays.update.own", "permission.password"),
    AM_ASSAYS_READ("am.assays.read", "am.assays.read", "am.assays.read", "permission.password"),
    AM_ASSAYS_READ_OWN("am.assays.read.own", "am.assays.read.own", "am.assays.read.own", "permission.password"),
    AM_ASSAYS_DELETE("am.assays.delete", "am.assays.delete", "am.assays.delete", "permission.password"),
    AM_VOLUME_READ("am.volume.read", "am.volume.read", "am.volume.read", "permission.password"),
    AM_VOLUME_READ_OWN("am.volume.read.own", "am.volume.read.own", "am.volume.read.own", "permission.password"),
    AM_VOLUME_CREATE("am.volume.create", "am.volume.create", "am.volume.create", "permission.password"),
    AM_VOLUME_CREATE_OWN("am.volume.create.own", "am.volume.create.own", "am.volume.create.own", "permission.password"),
    AM_VOLUME_UPDATE("am.volume.update", "am.volume.update", "am.volume.update", "permission.password"),
    AM_VOLUME_UPDATE_OWN("am.volume.update.own", "am.volume.update.own", "am.volume.update.own", "permission.password"),
    AM_VOLUME_DELETE("am.volume.delete", "am.volume.delete", "am.volume.delete", "permission.password"),
    AM_ASSAYS_MOVE("am.assays.move", "am.assays.move", "am.assays.move", "permission.password");

    private final String scenarioContextName;
    private final String emailProperty;
    private final String passwordProperty;
    private final String userName;

    ApiPermissionsUsers(String userName, String scenarioContextName, String emailProperty, String passwordProperty) {
        this.userName = userName;
        this.scenarioContextName = scenarioContextName;
        this.emailProperty = emailProperty;
        this.passwordProperty = passwordProperty;
    }
}
