package diaceutics.cucumber.utilities.apputils;

import diaceutics.selenium.utilities.CsvUtil;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class BiomarkerUtil {

    public List<String> getValidBiomarkersFromFile(String fileName, List<String> markersFromApp){
        return CsvUtil.readStoredCsvFileByRows(fileName).stream()
                .sorted()
                .distinct()
                .filter(markersFromApp::contains)
                .collect(Collectors.toList());
    }

    public List<String> getInvalidBiomarkersFromFile(String fileName, List<String> markersFromApp){
        return CsvUtil.readStoredCsvFileByRows(fileName).stream()
                .sorted()
                .distinct()
                .filter(invalidBiomarker -> !markersFromApp.contains(invalidBiomarker))
                .filter(biomarker -> !biomarker.contains("Biomarkers"))
                .collect(Collectors.toList());
    }
}
