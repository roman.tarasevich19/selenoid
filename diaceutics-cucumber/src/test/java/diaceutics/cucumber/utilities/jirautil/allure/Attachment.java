package diaceutics.cucumber.utilities.jirautil.allure;

import lombok.Data;

@Data
public class Attachment {
    private String name;
    private String source;
    private String type;
}

