package diaceutics.cucumber.utilities.jirautil.allure;

import lombok.Data;

@Data
public class StatusDetails {
    private boolean known;
    private boolean muted;
    private boolean flaky;
    private String message;
    private String trace;
}
