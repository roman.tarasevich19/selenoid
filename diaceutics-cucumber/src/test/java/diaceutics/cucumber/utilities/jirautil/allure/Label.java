package diaceutics.cucumber.utilities.jirautil.allure;

import lombok.Data;

import java.util.List;

@Data
public class Label {
    private String name;
    private String status;
    private String value;
    private StatusDetails statusDetails;
    private String stage;
    private List<Step> steps;
    private List<Object> attachments;
    private List<Object> parameters;
    private long start;
    private long stop;
    private String uuid;
    private String historyId;
    private String fullName;
    private List<Label> labels;
    private List<Object> links;
}
