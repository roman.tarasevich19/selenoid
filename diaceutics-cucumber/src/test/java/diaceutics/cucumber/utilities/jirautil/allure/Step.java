package diaceutics.cucumber.utilities.jirautil.allure;

import lombok.Data;

import java.util.List;

@Data
public class Step {
    private String name;
    private String status;
    private StatusDetails statusDetails;
    private String stage;
    private List<Object> steps;
    private List<Attachment> attachments;
    private List<Object> parameters;
    private Object start;
    private Object stop;
}
