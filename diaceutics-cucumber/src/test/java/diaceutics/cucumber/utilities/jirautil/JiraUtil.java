package diaceutics.cucumber.utilities.jirautil;

import aquality.selenium.browser.AqualityServices;
import com.atlassian.jira.rest.client.api.*;
import com.atlassian.jira.rest.client.api.domain.*;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.auth.BasicHttpAuthenticationHandler;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import diaceutics.restassured.utilities.JSONConvertor;
import diaceutics.selenium.configuration.Environment;
import diaceutics.cucumber.utilities.jirautil.allure.Allure;
import io.atlassian.util.concurrent.Promise;
import java.io.File;
import java.net.URI;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class JiraUtil {

    private static JiraRestClient restClient;
    private static final String ISSUE_NOT_FOUND = "Issue not found";
    private static final Long TASK_ID = 10101L;
    private static final String PROJECT_KEY = "QAD";
    private static final String LAB_MAPPING = "LabMapping";
    private static final String PHYSICIAN_MAPPING = "PhysicianMapping";
    private static final String TESTING_DASHBOARD = "TestingDashboard";
    private static final String ASSAY_MANAGEMENT = "AssayManagement";
    private static final String MARKETPLACE = "Marketplace";
    private static final String PERMISSIONS_UI = "PermissionUI";
    private static final String API = "API";
    private static final String ALLURE_STATUS_PASSED = "passed";
    private static final String ALLURE_DIRECTORY = "/target/allure-results";
    private static final File allureFolder = new File(new File("").getAbsolutePath() + ALLURE_DIRECTORY);
    public static final String ONLY_LETTERS_DIGITS = "[^a-zA-Z0-9]";
    private static final String JIRA_URL = "https://diaceutics.atlassian.net";

    public static List<File> getAllAllureJsonFiles(){
        return Arrays.stream(Objects.requireNonNull(allureFolder.listFiles())).filter(file -> file.getPath().contains("result.json")).collect(Collectors.toList());
    }

    public static List<File> getAllScreenshotFiles(){
        return Arrays.stream(Objects.requireNonNull(allureFolder.listFiles())).filter(file -> file.getPath().contains("jpg")).collect(Collectors.toList());
    }

    public static void updateTestStatus() {
        String env = "_" + Environment.getEnvironmentName();
        getAllAllureJsonFiles().forEach(allureJson -> {
            String trace = "";
            File screenToAttach;
            Allure allure = JSONConvertor.convertJSONFileToObject(allureJson, Allure.class);
            String jiraSummary = allure.getName().replaceAll(ONLY_LETTERS_DIGITS, "_").replaceAll(" ", "") + env;
            JiraUtil.updateIssueStatus(jiraSummary, allure.getStatus(), getComponent(jiraSummary));
            if (!allure.getStatus().equals(ALLURE_STATUS_PASSED)) {
                trace = allure.getStatusDetails().getTrace();
                screenToAttach = getAllScreenshotFiles().stream().filter(screen -> screen.getPath().contains(jiraSummary)).findFirst().get();
                JiraUtil.addScreenshot(jiraSummary, PROJECT_KEY, screenToAttach);
            }
            JiraUtil.addComment(jiraSummary, PROJECT_KEY, allure.getStatus(), trace);
        });
    }

    public static String getComponent(String scenarioFullName) {
        String label = null;
        if(scenarioFullName.contains("DIATD")){
            label = TESTING_DASHBOARD;
        }else if(scenarioFullName.contains("DIAPM")){
            label = PHYSICIAN_MAPPING;
        }else if(scenarioFullName.contains("DIAFE")){
            label = ASSAY_MANAGEMENT;
        }else if(scenarioFullName.contains("DIAM")){
            label = MARKETPLACE;
        }else if(scenarioFullName.contains("DIALM")){
            label = LAB_MAPPING;
        }else if(scenarioFullName.contains("DIAPFE")){
            label = PERMISSIONS_UI;
        } else if(scenarioFullName.contains("DIABE")){
            label = API;
        }
        return label;
    }

    public static JiraRestClient getRestClient(){
        URI jiraServerUri = URI.create(JIRA_URL);
        String jiraUser = System.getProperty("jiraUser");
        String jiraPassword = System.getProperty("jiraPassword");
        AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        AuthenticationHandler auth = new BasicHttpAuthenticationHandler(jiraUser, jiraPassword);
        restClient = factory.create(jiraServerUri, auth);
        return restClient;
    }

    public static void createNewIssue(String summary, String status, String component) {
        restClient = getRestClient();
        String[] labels = {status};
        IssueRestClient issueClient = restClient.getIssueClient();
        IssueInputBuilder iib = new IssueInputBuilder();
        iib.setProjectKey(PROJECT_KEY);
        iib.setSummary(summary);
        iib.setDescription(setUpdatedDateTime());
        iib.setComponentsNames(Collections.singleton(component));
        iib.setIssueTypeId(TASK_ID);
        iib.setFieldInput(new FieldInput("labels", Arrays.asList(labels)));
        IssueInput issue = iib.build();
        String issueKey = issueClient.createIssue(issue).claim().getKey();
        AqualityServices.getLogger().info("New issue was created: " + summary);
    }

    public static void addComment(String summary, String projectKey, String commentStatus, String trace) {
        restClient = getRestClient();
        String commentTime = setTestRunOnlyBy(commentStatus);
        String comment = commentTime + "\n" + "```" + trace;
        Issue issue = getIssue(summary, projectKey);
        restClient.getIssueClient()
                .addComment(Objects.requireNonNull(issue).getCommentsUri(), Comment.valueOf(comment));
    }

    public static void addScreenshot(String summary, String projectKey, File file){
        restClient = getRestClient();
        Issue issue = getIssue(summary, projectKey);
        restClient.getIssueClient()
                .addAttachments(Objects.requireNonNull(issue).getAttachmentsUri(), file).claim();
    }

    public static void updateIssueStatus(String summary,String newStatus, String component) {
        String[] labels = {newStatus};
        String issueKey = getIssueBySummaryAndGetIssueKey(summary, PROJECT_KEY);
        if (issueKey.equals(ISSUE_NOT_FOUND)) {
            createNewIssue(summary, newStatus, component);
        } else {
            AqualityServices.getLogger().info("Ticket with summary : " + summary + " will be updated!");
            IssueInput input = null;
            input = new IssueInputBuilder()
                    .setFieldInput(new FieldInput("labels", Arrays.asList(labels)))
                    .setComponentsNames(Collections.singleton(component))
                    .setDescription(setUpdatedDateTime())
                    .build();
            restClient.getIssueClient()
                    .updateIssue(issueKey, input)
                    .claim();
        }
    }

    private static String setUpdatedDateTime(){
        String date_format = "dd/MM/yyyy hh:mm a";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(date_format);
        Instant now = Instant.now(); /* UTC time */
        ZonedDateTime ukTime = now.atZone(ZoneId.of("Europe/London"));
        ZonedDateTime plTime = now.atZone(ZoneId.of("Europe/Warsaw"));
        ZonedDateTime byTime = now.atZone(ZoneId.of("Europe/Minsk"));
        return "Last update: " + "\n" +
                format.format(ukTime) + " - UK time" + "\n" +
                format.format(plTime) + " - PL time" + "\n" +
                format.format(byTime) + " - BY time" + "\n";
    }

    private static String setTestRunOnlyBy(String status){
        String date_format = "dd/MM/yyyy hh:mm a";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(date_format);
        Instant now = Instant.now(); /* UTC time */
        ZonedDateTime byTime = now.atZone(ZoneId.of("Europe/Minsk"));
        return String.format("%s", status) + "\n" +
                "Test finish time: " + "\n" +
                format.format(byTime) + " - BY time" + "\n";
    }

    private static String getIssueType(String summary, String projectKey){
        String key = getIssueBySummaryAndGetIssueKey(summary, projectKey);
        restClient = getRestClient();
        System.out.println(restClient.getIssueClient()
                .getIssue(key)
                .claim().getFields());
        return restClient.getIssueClient()
                .getIssue(key)
                .claim().getIssueType().getName();
    }

    private static void deleteIssue(String summary, boolean deleteSubtasks, String projectKey) {
        String issueKey = getIssueBySummaryAndGetIssueKey(summary, projectKey);
        restClient.getIssueClient()
                .deleteIssue(issueKey, deleteSubtasks)
                .claim();
    }

    public static void deleteAllIssuesByComponent(String summary, String projectKey) {
        List<String> issueKeys = getIssuesKeyByPartSummary(summary, projectKey);
        Objects.requireNonNull(issueKeys).forEach(issue -> {
            restClient.getIssueClient().deleteIssue(issue, true).claim();
            AqualityServices.getLogger().info("Issue: " + issue + " was deleted");
        });
    }

    public static void deleteIssueByKey(String issueKey, boolean deleteSubtasks) {
        restClient = getRestClient();
        restClient.getIssueClient()
                .deleteIssue(issueKey, deleteSubtasks)
                .claim();
        AqualityServices.getLogger().info("Issue: " + issueKey + " was deleted");
    }

    private static String getIssueBySummaryAndGetIssueKey(String summary, String projectKey) {
        restClient = getRestClient();
        String key = null;
        String jql = String.format("project = \"" + projectKey + "\" AND summary ~ " + "\\" + "\"%s" + "\\" + "\"", summary);
        AqualityServices.getLogger().info("JQL search: " + jql);
        SearchRestClient searchRestClient = restClient.getSearchClient();
        int maxPerQuery = 5;
        int startIndex = 0;
        Promise<SearchResult> searchResult = searchRestClient.searchJql(jql, maxPerQuery, startIndex, null);
        SearchResult results = searchResult.claim();
        if(results.getTotal() == 0){
            try {
                return ISSUE_NOT_FOUND;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            key = results.getIssues().iterator().next().getKey();
            AqualityServices.getLogger().info("Issue was found: " + key);
        }
        return key;
    }

    private static List<String> getIssuesKeyByPartSummary(String summary, String projectKey) {
        restClient = getRestClient();
        List<String> keys = new ArrayList<>();
        String jql = String.format("project = \"" + projectKey + "\" AND summary ~ \"%s\"", summary);
        AqualityServices.getLogger().info("JQL search: " + jql);
        SearchRestClient searchRestClient = restClient.getSearchClient();
        int maxPerQuery = 500;
        int startIndex = 0;
        Promise<SearchResult> searchResult = searchRestClient.searchJql(jql, maxPerQuery, startIndex, null);
        SearchResult results = searchResult.claim();
        if(results.getTotal() == 0){
            try {
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            AqualityServices.getLogger().info("TOTAL:" + results.getTotal());
            results.getIssues().iterator().forEachRemaining(issue -> {
                keys.add(issue.getKey());
                AqualityServices.getLogger().info("KEY: " + issue.getKey() + " was found");
            });
        }
        return keys;
    }

    private static Issue getIssue(String summary, String projectKey) {
        String key = null;
        Issue issue = null;
        String jql = String.format("project = \"" + projectKey + "\" AND summary ~ " + "\\" + "\"%s" + "\\" + "\"", summary);
        AqualityServices.getLogger().info("Search issue for new comment: " + jql);
        SearchRestClient searchRestClient = restClient.getSearchClient();
        int maxPerQuery = 5;
        int startIndex = 0;
        Promise<SearchResult> searchResult = searchRestClient.searchJql(jql, maxPerQuery, startIndex, null);
        SearchResult results = searchResult.claim();
        if(results.getTotal() == 0){
            try {
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            AqualityServices.getLogger().info("ISSUE WAS FOUND");
            issue = results.getIssues().iterator().next();
        }
        return issue;
    }
}