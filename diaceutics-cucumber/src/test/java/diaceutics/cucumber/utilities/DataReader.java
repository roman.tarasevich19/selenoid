package diaceutics.cucumber.utilities;

import com.fasterxml.jackson.core.type.TypeReference;
import diaceutics.selenium.configuration.Environment;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.models.User;
import diaceutics.selenium.utilities.ResourceUtil;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@UtilityClass
public class DataReader {
    private final String FILE_RESOURCE_PATH = "testdata/%s/";
    private final String COMMON_FILES_PATH = "common";
    private final String USERS_FILE = "users.json";
    private final String USERS_FILE_UI_PERMISSIONS = "usersUiPermissions.json";
    private final String USERS_FILE_API_PERMISSIONS = "usersAPIPermissions.json";
    private final String COLLABORATIONS_FILE = "collaborations.json";

    public User getUserInfoByUserName(String userName) {
        return getUserInfoByNameFromFile(userName, USERS_FILE);
    }

    public User getUIPermissionsUserInfoForByUserName(String userName) {
        return getUserInfoByNameFromFile(userName, USERS_FILE_UI_PERMISSIONS);
    }

    public User getAPIPermissionsUserInfoForByUserName(String userName) {
        return getUserInfoByNameFromFile(userName, USERS_FILE_API_PERMISSIONS);
    }

    public Collaboration getCollaborationInfoByCollaborationName(String collaborationName) {
        String path = ResourceUtil.getResourcePath(getResourcePath() + COLLABORATIONS_FILE);
        String fileContent = FileProcessor.getFileContent(path);
        Map<String, Collaboration> collaborations = JsonObjectMapper.mapToObject(
                fileContent,
                new TypeReference<Map<String, Collaboration>>() {
                }
        );
        return collaborations.get(collaborationName);
    }

    public File readJSONFile(String fileName) {
        String path = ResourceUtil.getResourcePath(getResourcePath() + fileName);
        return new File(path);
    }

    @SneakyThrows
    public String generateStringFromFile(String fileName) {
        String path = ResourceUtil.getResourcePath(getResourcePath() + fileName);
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public String getCommonFilesPath(String fileName) {
        return ResourceUtil.getResourcePath(String.format(FILE_RESOURCE_PATH, COMMON_FILES_PATH) + fileName);
    }

    private String getResourcePath() {
        return String.format(FILE_RESOURCE_PATH, Environment.getEnvironmentName());
    }

    private User getUserInfoByNameFromFile(String name, String fileName) {
        String path = ResourceUtil.getResourcePath(getResourcePath() + fileName);
        String fileContent = FileProcessor.getFileContent(path);
        Map<String, User> users = JsonObjectMapper.mapToObject(
                fileContent,
                new TypeReference<Map<String, User>>() {
                }
        );
        return users.get(name);
    }
}
