package diaceutics.cucumber.stepdefinitions.ui;

import aquality.selenium.browser.AqualityServices;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.configuration.Configuration;
import diaceutics.selenium.enums.urls.Urls;
import diaceutics.selenium.models.User;
import diaceutics.selenium.utilities.FileUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class BrowserSteps {

    private final ScenarioContext scenarioContext;

    @Inject
    public BrowserSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
    }

    @When("I click back on browser")
    public void iClickBackOnBrowser() {
        AqualityServices.getBrowser().goBack();
    }

    @And("I select last tab")
    public void iSelectLastWindow() {
        for (String tab : AqualityServices.getBrowser().getDriver().getWindowHandles()) {
            AqualityServices.getBrowser().getDriver().switchTo().window(tab);
        }
    }

    @And("I close tab number {int}")
    public void iCloseTabByIndex(Integer index) {
        List<String> tabs = new ArrayList<>(AqualityServices.getBrowser().getDriver().getWindowHandles());
        AqualityServices.getBrowser().getDriver().switchTo().window(tabs.get(index));
        AqualityServices.getBrowser().getDriver().close();
    }

    @Then("File {string} is downloaded")
    public void fileIsDownloaded(String fileName) {
        Assert.assertTrue(FileUtil.isExistDownloadedFile(fileName), String.format("File %s should be downloaded", fileName));
    }

    @When("I open first tab")
    public void iOpenFirstWindow() {
        List<String> tabs = new ArrayList<>(AqualityServices.getBrowser().getDriver().getWindowHandles());
        AqualityServices.getBrowser().getDriver().switchTo().window(tabs.get(0));
    }

    @And("I refresh page")
    public void iRefreshPage() {
        AqualityServices.getBrowser().refresh();
    }

    @When("I open DXRX app")
    public void openDXRXapp(){
        AqualityServices.getBrowser().goTo(Configuration.getUrl(Configuration.START_URL));
    }

    @When("I open BackOffice app")
    public void openBackOfficeApp(){
        AqualityServices.getBrowser().goTo(Configuration.getUrl(Configuration.BACK_OFFICE_URL));
    }

    @Then("Browser opened link {string}")
    public void browserOpenedLink(String linkName) {
        String actualLink = AqualityServices.getBrowser().getDriver().getCurrentUrl();
        String expectedLink = Configuration.getUrl(Configuration.START_URL) + Urls.getEnumValue(linkName).getUrl();
        SoftAssert.getInstance().assertEquals(actualLink, expectedLink,
                String.format("Browser should be opened with link - %s", expectedLink));
    }

    @Then("Link should be {string}")
    public void linkShouldBe(String linkName) {
        String expectedLink = Urls.getEnumValue(linkName).getUrl();
        String actualLink = AqualityServices.getBrowser().getDriver().getCurrentUrl();
        SoftAssert.getInstance().assertEquals(actualLink, expectedLink,
                String.format("Browser should be opened with link - %s", expectedLink));
    }

    @Then("Link should contains {string}")
    public void linkShouldContains(String linkName) {
        String expectedLink = Urls.getEnumValue(linkName).getUrl();
        String actualLink = AqualityServices.getBrowser().getDriver().getCurrentUrl();
        SoftAssert.getInstance().assertTrue(actualLink.contains(expectedLink),
                String.format("Browser should be opened with link - %s", expectedLink));
    }

    @Then("I open {string} url with environment in browser")
    public void iOpenLink(String linkName) {
        AqualityServices.getBrowser().getDriver()
                .navigate()
                .to(Configuration.getUrl(Configuration.START_URL) + Urls.getEnumValue(linkName).getUrl());
    }

    @Then("I open {string} url with environment in browser with user {string} lab")
    public void iOpenLinkWithUserLabId(String linkName, String userName) {
        User user = scenarioContext.get(userName);
        AqualityServices.getBrowser().getDriver()
                .navigate()
                .to(Configuration.getUrl(Configuration.START_URL)
                        + String.format(Urls.getEnumValue(linkName).getUrl(),user.getLabId()));
    }

    @Then("I open {string} url with environment in browser with user {string} not assigned lab")
    public void iOpenLinkWithUserNotAssignedLabId(String linkName, String userName) {
        User user = scenarioContext.get(userName);
        AqualityServices.getBrowser().getDriver()
                .navigate()
                .to(Configuration.getUrl(Configuration.START_URL)
                        + String.format(Urls.getEnumValue(linkName).getUrl(),user.getNotAssignedLabId()));
    }

    @When("I open {string} url in browser")
    public void iOpenUrlInBrowser(String url) {
        AqualityServices.getBrowser().goTo(url);
    }
}
