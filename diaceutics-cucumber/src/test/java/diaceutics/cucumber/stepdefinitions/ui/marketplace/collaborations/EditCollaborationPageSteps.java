package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.marketplace.CategoryFormFields;
import diaceutics.selenium.enums.pagefields.marketplace.MyLocationFormFields;
import diaceutics.selenium.enums.pagefields.marketplace.PresentationFormFields;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.EditCollaborationPage;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;


public class EditCollaborationPageSteps {

    private final EditCollaborationPage editCollaborationPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public EditCollaborationPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        editCollaborationPage = new EditCollaborationPage();
    }

    @Then("Edit collaboration page is opened")
    public void editCollaborationPageIsOpened() {
        Assert.assertTrue(editCollaborationPage.isDisplayed(),
                "Edit collaboration page should be opened");
    }

    @And("Collaboration {string} with following fields is displayed on Presentation form on Edit collaboration page")
    public void collaborationWithFollowingFieldsIsDisplayedOnEditCollaborationPage(String key, List<String> fields) {
        Collaboration collaboration = scenarioContext.get(key);
        fields.forEach(field -> {
            String actualValue = editCollaborationPage.getPresentationForm()
                    .getFieldValue(PresentationFormFields.getEnumValue(field));
            String expectedValue = collaboration
                    .getReflectionFieldValue(PresentationFormFields.getEnumValue(field).getModelField());
            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for %s is not correct on Edit collaboration page", actualValue, field));
        });
    }

    @When("I click {string} link on Edit collaboration page")
    public void iClickLinkOnEditCollaborationPage(String linkName) {
        editCollaborationPage.clickLink(linkName);
    }

    @Then("Categories form on Edit collaboration page is opened")
    public void categoriesFormOnEditCollaborationPageIsOpened() {
        Assert.assertTrue(editCollaborationPage.getCategoryForm().isDisplayed(),
                "Categories form on Edit collaboration page should be opened");
    }

    @When("I click group {string} on Edit collaboration page")
    public void iClickGroupCategoryOnEditCollaborationPage(String linkName) {
        editCollaborationPage.clickGroupLink(linkName);
    }

    @And("Collaboration {string} with following fields is displayed on My location form on Edit collaboration page:")
    public void collaborationWithFollowingFieldsIsDisplayedOnMyLocationFormOnEditCollaborationPage(String key, List<String> fields) {
        Collaboration collaboration = scenarioContext.get(key);
        fields.forEach(field -> {
            String actualValue = editCollaborationPage.getMyLocationForm().getFieldValue(
                    MyLocationFormFields.getEnumValue(field));

            String expectedValue = collaboration.getReflectionFieldValue(
                    MyLocationFormFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for %s is not correct on My location form on Edit collaboration page",
                            actualValue, field));
        });
    }

    @Then("My location form on Edit collaboration page is opened")
    public void myLocationFormOnEditCollaborationPageIsOpened() {
        Assert.assertTrue(editCollaborationPage.getMyLocationForm().isDisplayed(),
                "My location form on Edit collaboration page should be opened");
    }

    @When("I fill following fields on Presentation form on Edit collaboration page and save as {string}:")
    public void iFillFollowingFieldsOnPresentationFormOnEditCollaborationPageAndSaveAsCollaboration(
            String key, Map<String, String> data) {
        Collaboration collaboration = scenarioContext.get(key);
        data.forEach((field, value) -> {
            value = field.equals(PresentationFormFields.TITLE.getFriendlyName()) ?
                    value + TimeUtil.getTimestamp() : value;

            String selectedValue = editCollaborationPage.getPresentationForm()
                    .setFieldValue(PresentationFormFields.getEnumValue(field), value);

            collaboration.setReflectionFieldValue(PresentationFormFields.getEnumValue(field).getModelField(), selectedValue);
        });
    }

    @And("I click Save changes button on Edit collaboration page")
    public void iClickSaveChangesButtonOnEditCollaborationPage() {
        editCollaborationPage.clickSaveChangesButton();
    }

    @When("I set {string} value for {string} field on Category form on Edit collaboration page and save as {string}")
    public void iSetRandomValueForCategoryFieldOnCategoryFormOnEditCollaborationPage(
            String value, String field, String key) {
        Collaboration collaboration = scenarioContext.get(key);
        String selectedValue = editCollaborationPage.getPresentationForm()
                .setFieldValue(CategoryFormFields.getEnumValue(field), value);

        collaboration.setReflectionFieldValue(CategoryFormFields.getEnumValue(field).getModelField(), selectedValue);
    }

    @Then("Collaboration {string} with {string} field is displayed on Category form on Edit collaboration page")
    public void collaborationWithFieldIsDisplayedOnCategoryFormOnEditCollaborationPage(String key, String field) {
        Collaboration collaboration = scenarioContext.get(key);
        String actualValue = editCollaborationPage.getCategoryForm().getFieldValue(CategoryFormFields.getEnumValue(field)).trim();
        String expectedValue = collaboration.getReflectionFieldValue(CategoryFormFields.getEnumValue(field).getModelField()).trim();
        SoftAssert.getInstance().assertEquals(actualValue, expectedValue,
                String.format("Value %s for category field is not correct on Edit collaboration page", actualValue));
    }

    @When("I fill following fields on My location form on Edit collaboration page and save as {string}:")
    public void iFillFollowingFieldsOnMyLocationFormOnEditCollaborationPageAndSaveAsCollaboration(String key, Map<String, String> data) {
        Collaboration collaboration = scenarioContext.get(key);
        data.forEach((field, value) -> {
            String selectedValue = editCollaborationPage.getMyLocationForm()
                    .setFieldValue(MyLocationFormFields.getEnumValue(field), value);

            collaboration.setReflectionFieldValue(MyLocationFormFields.getEnumValue(field).getModelField(), selectedValue);
        });
    }

    @Then("Inform Form with message {string} is displayed on Edit collaboration page")
    public void informFormWithMessageIsDisplayedOnEditCollaborationPage(String message) {
        Assert.assertTrue(editCollaborationPage.getInformForm().isMessageDisplayed(message),
                String.format("Message %s should be displayed on Edit collaboration page", message));
    }

    @And("Status {string} is displayed on Edit collaboration page")
    public void statusPublishedIsDisplayedOnEditCollaborationPage(String expectedStatus) {
        String actualStatus = editCollaborationPage.getFieldValue(PresentationFormFields.STATUS);
        Assert.assertEquals(actualStatus, expectedStatus,
                String.format("Status should be %s on Edit collaboration page", expectedStatus));
    }

    @Then("Presentation form on Edit collaboration page is opened")
    public void presentationFormOnEditCollaborationPageIsOpened() {
        Assert.assertTrue(editCollaborationPage.getPresentationForm().isDisplayed(),
                "Presentation form on Edit collaboration page should be opened");
    }
}
