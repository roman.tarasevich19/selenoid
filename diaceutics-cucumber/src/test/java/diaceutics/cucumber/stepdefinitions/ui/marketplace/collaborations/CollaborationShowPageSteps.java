package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.CollaborationShowPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import javax.inject.Inject;

public class CollaborationShowPageSteps {

    private final CollaborationShowPage collaborationShowPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public CollaborationShowPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        collaborationShowPage = new CollaborationShowPage();
    }

    @Then("Collaboration Show page is opened")
    public void collaborationShowPageIsOpened() {
        Assert.assertTrue(collaborationShowPage.isDisplayed(),
                "Collaborations Show page should be opened");
    }

    @When("I click Ask a question button on Collaborations Show page")
    public void iClickAskAQuestionButtonOnCollaborationsShowPage() {
        collaborationShowPage.clickByAskAQuestionButton();
    }

    @Then("Question form is opened on Collaborations Show page")
    public void questionFormIsOpenedOnCollaborationsShowPage() {
        Assert.assertTrue(collaborationShowPage.getQuestionForm().isDisplayed(),
                "Question form on Collaboration Show page should be opened");
    }

    @When("I put random message in Question form on Collaborations Show page and save as {string}")
    public void iPutMessageSaveAsMessageInQuestionFormOnCollaborationsShowPage(String key) {
        String message = RandomStringUtils.randomAlphabetic(5);
        collaborationShowPage.getQuestionForm().putMessage(message);
        scenarioContext.add(key, message);
    }

    @Then("Inform Form with following message is displayed on Collaborations Show page:")
    public void informFormWithFollowingMessageIsDisplayedOnCollaborationsShowPage(String expectedMessage) {
        String actualMessage = collaborationShowPage.getInformForm().getSuccessMessage();
        Assert.assertEquals(actualMessage, expectedMessage,
                String.format("Message %s should be displayed on Edit collaboration page", expectedMessage));
    }

    @When("I add a file {string} on Question form on Collaborations Show page")
    public void iAddAFileOnQuestionFormOnCollaborationsShowPage(String fileName) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        collaborationShowPage.getQuestionForm().addFile(filePath);
    }

    @Then("File {string} is added on Question form on Collaborations Show page")
    public void fileIsAddedOnQuestionFormOnCollaborationsShowPage(String fileName) {
        Assert.assertTrue(collaborationShowPage.getQuestionForm().isFileUploaded(fileName),
                String.format("File %s should be added on Question form on Collaborations Show page", fileName));
    }

    @When("I click Submit a response button on Collaborations Show page")
    public void iClickSubmitAResponseButtonOnCollaborationsShowPage() {
        collaborationShowPage.clickBySubmitAResponseButton();
    }

    @And("I click Send on Question form on Collaborations Show page")
    public void iClickSendOnQuestionFormOnCollaborationsShowPage() {
        collaborationShowPage.getQuestionForm().clickSendButton();
    }

    @Then("Error message {string} is displayed on Question form on Collaborations Show page")
    public void errorMessageIsDisplayedOnQuestionFormOnCollaborationsShowPage(String message) {
        Assert.assertTrue(collaborationShowPage.getQuestionForm().isErrorMessageDisplayed(message),
                String.format("Error message %s should be displayed on Question form on Collaborations Show page", message));
    }
}
