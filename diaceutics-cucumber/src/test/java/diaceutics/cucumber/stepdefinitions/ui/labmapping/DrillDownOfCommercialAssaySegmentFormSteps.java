package diaceutics.cucumber.stepdefinitions.ui.labmapping;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.grids.Grids;
import diaceutics.selenium.pageobject.forms.labmapping.DrillDownOfCommercialAssaySegmentGridForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import javax.inject.Inject;

public class DrillDownOfCommercialAssaySegmentFormSteps {

    private final DrillDownOfCommercialAssaySegmentGridForm drillDownOfCommercialAssaySegmentGridForm;
    private final ScenarioContext scenarioContext;

    @Inject
    public DrillDownOfCommercialAssaySegmentFormSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        drillDownOfCommercialAssaySegmentGridForm = new DrillDownOfCommercialAssaySegmentGridForm();
    }

    @Then("Drill down of Commercial Assay segment form is opened")
    public void labMappingIsOpened() {
        Assert.assertTrue(drillDownOfCommercialAssaySegmentGridForm.isDisplayed(),
                "Drill down of Commercial Assay segment form should be opened");
    }

    @And("I get value for {string} row in {string} column and compare with {string} on Drill down of Commercial Assay segment form")
    public void iGetValueInColumnAndCompareWithValueOnDrillDownOfCommercialAssaySegmentForm(String row, String column, String key) {
        String actualValue = drillDownOfCommercialAssaySegmentGridForm
                .getValueFromColumnForRow(Grids.DRILL_DOWN_OF_COMMERCIAL_ASSAY_SEGMENT.getLocator(), column, row);

        String expectedValue = scenarioContext.get(key);
        Assert.assertEquals(actualValue, expectedValue,
                String.format("Value for %s row in column %s should be %s on Drill down of Commercial Assay segment form",
                        row, column, expectedValue));
    }
}
