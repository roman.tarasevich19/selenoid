package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.restassured.project.models.LabTestDto;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.selenium.enums.grids.Grids;
import diaceutics.selenium.enums.pagefields.assaymanagement.*;
import diaceutics.selenium.models.*;
import diaceutics.selenium.pageobject.pages.assaymanagement.LabProfilePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;

public class LabProfilePageSteps {

    private final LabProfilePage labProfilePage;
    private final ScenarioContext scenarioContext;

    @Inject
    public LabProfilePageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        labProfilePage = new LabProfilePage();
    }

    @Given("Lab Profile page is opened")
    public void labProfilesPageIsOpened() {
        Assert.assertTrue(labProfilePage.isDisplayed(), "LabProfile page should be opened");
    }

    @When("I sort data by alphabet in {string} column on Assays Grid")
    public void iSortDataByAlphabetInColumnOnAssaysGrid(String column) {
        labProfilePage.getAssaysGridForm().clickSortColumnInGrid(column, Grids.ASSAYS.getLocator());
    }

    @When("I sort data by alphabet in {string} column on Patient volume log Grid")
    public void iSortDataByAlphabetInColumnOnPatientVolumeLogGrid(String column) {
        labProfilePage.getAssaysGridForm().clickSortColumnInGrid(column, Grids.PATIENT_VOLUME_LOG.getLocator());
    }

    @Then("Data in {string} column on Assays Grid sorted according to alphabet")
    public void dataInColumnOnAssaysGridSortedAccordingToAlphabet(String column) {
        SoftAssert.getInstance().assertTrue(
                labProfilePage.getAssaysGridForm().isDataInColumnInGridSorted(column, Grids.ASSAYS.getLocator()),
                String.format("Data in Grid %s in %s column should be sorted according to alphabet",
                        Grids.ASSAYS.getName(),
                        column));
    }

    @Then("Data in {string} column on Patient volume log Grid sorted according to alphabet")
    public void dataInColumnOnPatientVolumeLogGridSortedAccordingToAlphabet(String column) {
        SoftAssert.getInstance().assertTrue(
                labProfilePage.getPatientVolumeLogGridForm().isDataInColumnInGridSorted(column, Grids.PATIENT_VOLUME_LOG.getLocator()),
                String.format("Data in Grid %s in %s column should be sorted according to alphabet",
                        Grids.ASSAYS.getName(),
                        column));
    }

    @When("I count the number of assays in the Assays grid and save as {string}")
    public void iCountTheNumberOfAssaysInThePlatformsGridAndSaveAsNumberOfPlatforms(String key) {
        String numberOfPlatformsFromGrid = labProfilePage.getAssaysGridForm().getNumberOfRowsInGrid(Grids.ASSAYS.getLocator());
        scenarioContext.add(key, numberOfPlatformsFromGrid);
    }

    @Then("{string} must be the same as a number stated in the Assays Grid title")
    public void numberOfAssaysInPlatformGridMustBeTheSameAsANumberStatedInThePlatformsGridTitle(String key) {
        String numberOfAssaysFromGrid = scenarioContext.get(key);
        Assert.assertEquals(numberOfAssaysFromGrid,
                labProfilePage.getAssaysGridForm().getNumberOfAssaysFromGridHead(),
                "The number of rows in Assays grid must be the same as a number assay in the Assays grid title");
    }

    @When("I click Edit Details on Lab Profile Page")
    public void iClickOnEditDetailsOnLabProfilePage() {
        labProfilePage.clickEditDetails();
    }

    @And("Lab ID assigned to user {string} is correct")
    public void getLabIdForUser(String key) {
        User user = scenarioContext.get(key);
        String expectedId = user.getLabId();
        String actualId = labProfilePage.getIdFromUrl();
        Assert.assertEquals(actualId, expectedId, String.format("Lab id for user: %s is not correct", user.getEmail()));
    }

    @And("Lab {string} with following fields is displayed on Lab Profile page")
    public void labLabIsDisplayedOnLabProfilePage(String key, List<String> fields) {
        Lab expectedLab = scenarioContext.get(key);
        Lab actualLab = labProfilePage.getLabFromPage();
        Address actualAddress = labProfilePage.getLabLocationFromPage();
        fields.forEach(field -> {
            String actualValue;
            String expectedValue;
            if (field.equals(LabAddressPageFields.COUNTRY.getFriendlyName())
                    || field.equals(LabAddressPageFields.REGION.getFriendlyName())) {
                Address expectedAddress = expectedLab.getAddresses().get(0);
                actualValue = actualAddress.getReflectionFieldValue(LabAddressPageFields.getEnumValue(field).getModelField());
                expectedValue = expectedAddress.getReflectionFieldValue(LabAddressPageFields.getEnumValue(field).getModelField());
            } else {
                actualValue = actualLab.getReflectionFieldValue(CreateLabPageFields.getEnumValue(field).getModelField());
                expectedValue = expectedLab.getReflectionFieldValue(CreateLabPageFields.getEnumValue(field).getModelField());
            }
            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for %s is not correct on Lab Profile page", actualValue, expectedLab.getName()));
        });
    }

    @Then("{string} message is displayed on Lab Profile page")
    public void newLocationAddedMessageIsDisplayedOnLabProfilePage(String message) {
        Assert.assertTrue(labProfilePage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Lab Profile page", message));
    }

    @When("I click on {string} on Lab Profile Page")
    public void iClickOnAddButtonOnLabProfilePage(String buttonName) {
        labProfilePage.clickAdd(buttonName);
    }


    @And("Volume {string} is displayed in Patient volume log grid on Lab Profile page")
    public void volumeVolumeIsDisplayedInVolumesGridOnLabProfilePage(String key) {
        Volume volume = scenarioContext.get(key);
        Assert.assertTrue(labProfilePage.getPatientVolumeLogGridForm().isVolumeDisplayed(volume),
                String.format("Volume with values %s, %s, %s should be added in Volumes grid",
                        volume.getTimePeriod(),
                        volume.getDisease(),
                        volume.getDiseaseVolume()));
    }

    @When("I click on Edit button for the {string} volume on Lab Profile Page")
    public void iClickOnEditButtonForTheVolumeVolumeOnLabProfilePage(String key) {
        Volume volume = scenarioContext.get(key);
        labProfilePage.getPatientVolumeLogGridForm().clickEditVolume(volume);
    }

    @And("Assay {string} is displayed in Assays grid on Lab Profile page")
    public void assayAssayIsAddedToAssaysGridOnLabProfilePage(String key) {
        String assayName;
        String classifications;
        String method;
        if (scenarioContext.get(key).getClass().isInstance(new Assay())) {
            Assay assay = scenarioContext.get(key);
            assayName = assay.getAssayName();
            classifications = assay.getClassifications();
            method = assay.getMethod();
        } else {
            LabTestDto assay = scenarioContext.get(key);
            assayName = assay.getName();
            classifications = assay.getClassifications().get(0).getName();
            method = assay.getMethod().getName();
        }

        Assert.assertTrue(labProfilePage.getAssaysGridForm().isAssayAdded(assayName, classifications, method),
                String.format("Assay %s should be added in Assay grid", assayName));
    }

    @When("I click on Assay {string} in Assays grid on Lab Profile Page")
    public void iClickOnAssayAssayInAssaysGridOnLabProfilePage(String key) {
        String assayName;
        if (scenarioContext.get(key).getClass().isInstance(new Assay())) {
            Assay assay = scenarioContext.get(key);
            assayName = assay.getAssayName();
        } else {
            LabTestDto assay = scenarioContext.get(key);
            assayName = assay.getName();
        }

        labProfilePage.getAssaysGridForm().clickByAssay(assayName);
    }

    @When("I put a Assay {string} on search field {string} and press Search icon on Lab Profile page")
    public void iPutAAssayAssayOnSearchFieldEnterKeywordsAndPressSearchIconOnLabProfilePage(String key, String searchFieldName) {
        LabTestDto assay = scenarioContext.get(key);
        labProfilePage.putTextInSearchField(assay.getName(), searchFieldName);
        labProfilePage.clickSearch();
    }

    @And("Value {string} for Volume {string} is displayed in {string} column in Patient volume log Grid on Lab Profile page")
    public void valueIsDisplayedInVolumeColumnOnVolumesGridOnLabProfilePageForVolumeVolume(
            String expectedValue, String key, String column) {
        Volume volume = scenarioContext.get(key);
        String actualValue = labProfilePage.getPatientVolumeLogGridForm()
                .getValueFromColumnForVolume(column, volume);

        SoftAssert.getInstance().assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s in %s column on Patient volume log Grid on Lab Profile page is not correct",
                        actualValue, column));
    }

    @And("Value from Volume {string} is displayed in {string} column in Patient volume log Grid on Lab Profile page")
    public void valueFromVolumeIsDisplayedInVolumeColumnInVolumesGridOnLabProfilePage(
            String key, String column) {
        Volume volume = scenarioContext.get(key);
        String expectedValue = volume.getDiseaseVolume();
        String actualValue = labProfilePage.getPatientVolumeLogGridForm()
                .getValueFromColumnForVolume(column, volume);

        SoftAssert.getInstance().assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s in %s column on Patient volume log Grid on Lab Profile page is not correct",
                        actualValue, column));
    }

    @And("Assay {string} is not displayed in Assays grid on Lab Profile page")
    public void assayAssayIsNotDisplayedInAssaysGridOnLabProfilePage(String key) {
        String assayName;
        String classifications;
        String method;
        if (scenarioContext.get(key).getClass().isInstance(new Assay())) {
            Assay assay = scenarioContext.get(key);
            assayName = assay.getAssayName();
            classifications = assay.getClassifications();
            method = assay.getMethod();
        } else {
            LabTestDto assay = scenarioContext.get(key);
            assayName = assay.getName();
            classifications = assay.getClassifications().get(0).getName();
            method = assay.getMethod().getName();
        }
        Assert.assertFalse(labProfilePage.getAssaysGridForm().isAssayAdded(assayName, classifications, method),
                String.format("Assay %s should be not displayed in Assay grid", assayName));
    }

    @And("Volume {string} is not displayed in Volumes grid on Lab Profile page")
    public void volumeIsDeletedOnVolumesGridOnLabProfilePage(String key) {
        Volume volume = scenarioContext.get(key);
        Assert.assertTrue(labProfilePage.getPatientVolumeLogGridForm().isNotDisplayed(volume),
                String.format("Volume with values %s, %s should be deleted in Volumes grid",
                        volume.getTimePeriod(),
                        volume.getDisease()));
    }

    @Then("Message {string} is displayed in Assays grid on Lab Profile page")
    public void messageIsDisplayedInAssaysGridOnLabProfilePage(String message) {
        Assert.assertTrue(labProfilePage.getAssaysGridForm().isMessageDisplayedInGrid(message, Grids.ASSAYS.getName()),
                String.format("Message %s should be displayed in Assays grid on Lab Profile page", message));
    }

    @Then("Message {string} is displayed in Patient volume log grid on Lab Profile page")
    public void messageIsDisplayedInPatientVolumeLogGridOnLabProfilePage(String message) {
        Assert.assertTrue(labProfilePage.getPatientVolumeLogGridForm()
                        .isMessageDisplayedInGrid(message, Grids.PATIENT_VOLUME_LOG.getName()),
                String.format("Message %s should be displayed in Patient volume log grid on Lab Profile page", message));
    }

    @When("I click back button on Lab Profile page")
    public void iClickBackButtonOnLabProfilePage() {
        labProfilePage.clickBack();
    }

    @When("I put a Volume {string} on search field {string} and press Search icon on Lab Profile page")
    public void iPutAVolumeOnSearchFieldTypeToFilterByDiseaseAndPressSearchIconOnLabProfilePage(
            String key, String searchFieldName) {
        Volume volume = scenarioContext.get(key);
        labProfilePage.putTextInSearchField(volume.getDisease(), searchFieldName);
        labProfilePage.clickSearch(searchFieldName);
    }

    @And("Hide version Lab profile page is opened")
    public void hideVersionOfPageIsPresent() {
        Assert.assertTrue(labProfilePage.hideVersionOfPageIsDisplayed(), "Hide version of page should be displayed");
    }

    @And("Assays block is present")
    public void assaysBlockIsPresent() {
        Assert.assertTrue(labProfilePage.assayBlockIsPresent(), "Assay block should be present");
    }

    @And("Assays block is not present")
    public void assaysBlockIsNotPresent() {
        Assert.assertFalse(labProfilePage.assayBlockIsPresent(), "Assay block should be not present");
    }

    @And("Volumes block is present")
    public void volumeBlockIsPresent() {
        Assert.assertTrue(labProfilePage.volumeBlockIsPresent(), "Volume block should be present");
    }

    @And("Volumes block is not present")
    public void volumeBlockIsNotPresent() {
        Assert.assertFalse(labProfilePage.volumeBlockIsPresent(), "Volume block should be not present");
    }

    @Then("Edit Details is not displayed on Lab Profile Page")
    public void editDetailsIsNotPresent() {
        Assert.assertFalse(labProfilePage.editDetailsIsPresent(), "Edit details should be not displayed");
    }

    @Then("Edit Details is displayed on Lab Profile Page")
    public void editDetailsIsPresent() {
        Assert.assertTrue(labProfilePage.editDetailsIsPresent(), "Edit details should be displayed");
    }

    @Then("Add assay is not displayed on Lab Profile Page")
    public void addAssayIsNotPresent() {
        Assert.assertFalse(labProfilePage.addAssayIsPresent(), "Add assay should be not displayed");
    }

    @Then("Add assay is displayed on Lab Profile Page")
    public void addAssayIsPresent() {
        Assert.assertTrue(labProfilePage.addAssayIsPresent(), "Add assay should be displayed");
    }

    @Then("Add volume is not displayed on Lab Profile Page")
    public void addVolumeIsNotPresent() {
        Assert.assertFalse(labProfilePage.addVolumeIsPresent(), "Add volume should be not displayed");
    }

    @Then("Add volume is displayed on Lab Profile Page")
    public void addVolumeIsPresent() {
        Assert.assertTrue(labProfilePage.addVolumeIsPresent(), "Add volume should be displayed");
    }

    @Then("Access is denied")
    public void isAccessIsDenied() {
        Assert.assertTrue(labProfilePage.isAccessIsDenied(), "Access should be denied");
    }

    @When("I click on Arrow button for the {string} volume in Patient volume log grid on Lab Profile page")
    public void iClickOnArrowButtonForTheVolumeVolumeOnLabProfilePage(String key) {
        Volume volume = scenarioContext.get(key);
        labProfilePage.getPatientVolumeLogGridForm().clickArrowButtonForVolume(volume);
    }

    @Then("Biomarkers from {string} are displayed in Patient volume log grid on Lab Profile page")
    public void biomarkersFromVolumeAreDisplayedInVolumesGridOnLabProfilePage(String key) {
        Volume volume = scenarioContext.get(key);
        List<BiomarkerVolume> biomarkerVolumes = volume.getBiomarkerVolumes();
        biomarkerVolumes.forEach(biomarkerVolume ->
                Assert.assertTrue(labProfilePage.getPatientVolumeLogGridForm()
                                .isBiomarkerVolumeAddedInVolumeGrid(biomarkerVolume.getBiomarkerName(), biomarkerVolume.getVolume()),
                        String.format("Biomarker %s should be displayed in Patient volume log grid",
                                biomarkerVolume.getBiomarkerName())));
    }

    @When("I click on Square button for the {string} volume in Patient volume log grid on Lab Profile page")
    public void iClickOnSquareButtonForTheVolumeVolumeInPatientVolumeLogGridOnLabProfilePage(String key) {
        Volume volume = scenarioContext.get(key);
        labProfilePage.getPatientVolumeLogGridForm().clickSquareButtonForVolume(volume);
    }

    @Then("Message {string} {string} {string} is displayed on Lab Profile page")
    public void messageAssayAssayHasBeenMovedToAnotherLabIsDisplayedOnLabProfilePage(String assayKey, String message, String labKey) {
        LabTestDto assay = scenarioContext.get(assayKey);
        LabDto lab = scenarioContext.get(labKey);
        String expectedMessage = String.format("'%s' %s '%s'", assay.getName(), message, lab.getName());
        Assert.assertTrue(labProfilePage.isAlertMessageDisplayed(expectedMessage),
                String.format("Message %s should be displayed on Lab Profile page", expectedMessage));
    }

    @Then("Filter {string} is selected in Assays grid on Lab Profile page")
    public void filterIsSelectedInAssaysGridOnLabProfilePage(String filter) {
        Assert.assertTrue(labProfilePage.getAssaysGridForm().isFilterSelected(filter),
                String.format("Filter %s should be selected in Assays grid on Lab Profile page", filter));
    }

    @Then("{string} must be the same as a number for {string} filter in Assays grid on Lab Profile page")
    public void numberOfAssaysMustBeTheSameAsANumberForFilterInAssaysGridOnLabProfilePage(String key, String filter) {
        String numberOfAssaysFromGrid = scenarioContext.get(key);
        Assert.assertEquals(numberOfAssaysFromGrid,
                labProfilePage.getAssaysGridForm().getNumberOfAssaysFromFilter(filter),
                String.format("The number of rows in Assays grid must be the same as a number assay in the filter %s", filter));
    }

    @When("I click {string} filter in Assays grid on Lab Profile page")
    public void iClickActiveFilterInAssaysGridOnLabProfilePage(String filter) {
        labProfilePage.getAssaysGridForm().clickByFilter(filter);
    }

    @And("Value {string} for Assay {string} is displayed in {string} column in Assays Grid on Lab Profile page")
    public void valueForAssayIsDisplayedInColumnInAssaysGridOnLabProfilePage(
            String expectedValue, String key, String column) {
        String assayName;
        String classifications;
        String method;
        if (scenarioContext.get(key).getClass().isInstance(new Assay())) {
            Assay assay = scenarioContext.get(key);
            assayName = assay.getAssayName();
            classifications = assay.getClassifications();
            method = assay.getMethod();
        } else {
            LabTestDto assay = scenarioContext.get(key);
            assayName = assay.getName();
            classifications = assay.getClassifications().get(0).getName();
            method = assay.getMethod().getName();
        }
        String actualValue = labProfilePage.getAssaysGridForm()
                .getValueFromColumnForAssay(column, assayName, classifications, method);

        SoftAssert.getInstance().assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s in %s column on Assays Grid on Lab Profile page is not correct",
                        actualValue, column));
    }

    @And("Value {string} for Biomarker from {string} is displayed in Biomarker vol. column in Patient volume log Grid on Lab Profile page")
    public void valueForBiomarkerFromVolumeIsDisplayedInColumnInPatientVolumeLogGridOnLabProfilePage(
            String expectedValue, String key) {
        Volume volume = scenarioContext.get(key);
        BiomarkerVolume biomarkerVolume = volume.getBiomarkerVolumes().get(0);

        Assert.assertTrue(labProfilePage.getPatientVolumeLogGridForm()
                        .isBiomarkerVolumeAddedInVolumeGrid(biomarkerVolume.getBiomarkerName(), expectedValue),
                String.format("Value <50 for Biomarker %s should be displayed in Patient volume log grid",
                        biomarkerVolume.getBiomarkerName()));
    }
}
