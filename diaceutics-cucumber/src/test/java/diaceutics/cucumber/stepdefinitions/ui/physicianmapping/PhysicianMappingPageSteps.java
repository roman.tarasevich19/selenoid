package diaceutics.cucumber.stepdefinitions.ui.physicianmapping;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.charts.PhysicianMappingCharts;
import diaceutics.selenium.enums.pagefields.EditSettingsFormFields;
import diaceutics.selenium.enums.pagefields.physicianmapping.AGGridPhysicianSummaryFormFields;
import diaceutics.selenium.enums.pagefields.physicianmapping.PhysicianMappingPageFields;
import diaceutics.selenium.enums.table.TableFilter;
import diaceutics.selenium.models.Physician;
import diaceutics.selenium.models.PhysicianExelTable;
import diaceutics.selenium.models.SearchFilter;
import diaceutics.selenium.pageobject.pages.physicianmapping.PhysicianMappingPage;
import diaceutics.selenium.utilities.PDFUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class PhysicianMappingPageSteps {

    private final PhysicianMappingPage physicianMappingPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public PhysicianMappingPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        physicianMappingPage = new PhysicianMappingPage();
    }

    @Then("Physician Mapping page is opened")
    public void physicianMappingIsOpened() {
        Assert.assertTrue(physicianMappingPage.isDisplayed(), "Physician Mapping page should be opened");
    }

    @And("Link {string} is not present on Physician Mapping page")
    public void linkIsNotPresentOnPhysicianMappingPage(String link) {
        Assert.assertFalse(physicianMappingPage.isLinkDisplayed(link),
                String.format("Link %s should be not present on Physician Mapping page", link));
    }

    @When("I click Export data on Physician Mapping page")
    public void iClickExportDataOnPhysicianMappingPage() {
        physicianMappingPage.clickExportData();
    }

    @When("I open menu for column {string}")
    public void openFilterMenuForColumn(String column){
        AGGridPhysicianSummaryFormFields field = AGGridPhysicianSummaryFormFields.getEnumValue(column);
        physicianMappingPage.getAGGridPhysicianSummaryForm().openColumnMenu(field);
    }

    @When("I set up {string} to the filter on Physician Mapping page")
    public void setUpFilterInputValue(String value){
        physicianMappingPage.getAGGridPhysicianSummaryForm().setUpFilterInput(value);
    }

    @And("I check Filter menu types to compare are equals the following list:")
    public void checkFilterItemList(List<String> listItems){
        physicianMappingPage.getAGGridPhysicianSummaryForm().openFilterListItem();
        SoftAssert.getInstance().assertEquals(
                physicianMappingPage.getAGGridPhysicianSummaryForm().getFilterListItem(), listItems, "List with filter types does not correct");
    }

    @When("I select filter type {string}")
    public void selectType(String type){
        TableFilter filter = TableFilter.getEnumValue(type);
        physicianMappingPage.getAGGridPhysicianSummaryForm().openFilterListItem();
        physicianMappingPage.getAGGridPhysicianSummaryForm().selectFilterType(filter);
    }

    @When("I set value {string} for filter box {string}")
    public void setValueForCheckboxFilter(String bool, String box){
        physicianMappingPage.getAGGridPhysicianSummaryForm().setFieldValueColumnCheckboxFilter(box, Boolean.parseBoolean(bool));
        scenarioContext.add(box, box);
    }

    @When("I set value {string} for all columns box")
    public void setValueForCheckboxForAllColumns(String bool){
        physicianMappingPage.getAGGridPhysicianSummaryForm().setValueToAllColumnsBox(Boolean.parseBoolean(bool));
    }

    @And("I fill value {string} in filter body input")
    public void setUpFilter(String value) {
        physicianMappingPage.getAGGridPhysicianSummaryForm().setFieldValueInputColumnFilter(value);
    }

    @Then("Physician table is empty")
    public void isTableEmpty() {
       Assert.assertTrue(physicianMappingPage.getAGGridPhysicianSummaryForm().tableIsEmpty(), "Table should be empty");
    }

    @Then("I read data with filter {string} for column {string}")
    public void readDataForFilterColumn(String filter, String column) {
        List<String> valuesFromColumn = physicianMappingPage.getAGGridPhysicianSummaryForm().getStringListValuesFromColumn(AGGridPhysicianSummaryFormFields.getEnumValue(column));
        String value = scenarioContext.get(filter);
        valuesFromColumn.forEach(valueFromColumn -> SoftAssert.getInstance().assertTrue(
                valueFromColumn.equals(value), String.format("All values from column %s should equals filter value %s", column, value)));
    }

    @Then("I read data from column {string} and check values with filter {string} and type {string}")
    public void readDataFromColumnAndCheckWithFilter(String column, String filter, String type){
        List<String> valuesFromColumn = physicianMappingPage.getAGGridPhysicianSummaryForm().getStringListValuesFromColumn(AGGridPhysicianSummaryFormFields.getEnumValue(column));
        SoftAssert.getInstance().assertTrue(
                physicianMappingPage.getAGGridPhysicianSummaryForm().readColumnAndCompareWithFilter(type, valuesFromColumn, filter),
                String.format("Values from column %s not correct in %s type", column, type));
    }

    @When("I set value {string} for column {string}")
    public void uncheckColumn(String bool, String column) {
        physicianMappingPage.getAGGridPhysicianSummaryForm().setFieldValueTableFilter(column, Boolean.parseBoolean(bool));
    }

    @Then("Column {string} is not present")
    public void isColumnPresent(String column){
        Assert.assertFalse(physicianMappingPage.getAGGridPhysicianSummaryForm().isColumnPresent(column),
                String.format("Column %s should be not present", column));
    }

    @Then("I read data in column {string} on Physician Mapping and values equals {string} on Physician Mapping page")
    public void readDataForFilterAssayColumns(String column, String filter) {
        List<String> valuesFromColumn = physicianMappingPage.getAGGridPhysicianSummaryForm().
                getStringListValuesFromColumn(AGGridPhysicianSummaryFormFields.getEnumValue(column));
        valuesFromColumn.forEach(valueFromColumn -> SoftAssert.getInstance().assertTrue(
                valueFromColumn.equals(filter), String.format("Value from column %s should equals filter value - %s", column, filter)));
    }

    @When("I read file {string} with Physicians with filter column {string} and save data as {string}")
    public void iReadFileExportXlsxWithFilterColumnsAndSaveDataAsPhysiciansFromFile(String fileName, String column, String key) {
        PhysicianExelTable physicianExelTable = new PhysicianExelTable(fileName);
        List<Physician> physicianListFromFile = physicianExelTable.getAllListPhysicianWithoutColumn(column);
        scenarioContext.add(key, physicianListFromFile);
    }

    @When("I read data from AG-Grid Physician summary on Physician Mapping page and compare with {string} without column {string}")
    public void iReadDataFromAGGridLabSummaryOnLabMappingPageAndSaveAsLabss(String key, String  column) {
        List<Physician> twentyFirstPhysiciansFromUI = physicianMappingPage.getAGGridPhysicianSummaryForm().getFilteredListPhysician(column);
        List<Physician> physiciansFromFile = scenarioContext.get(key);
        List<Physician> twentyFirstPhysiciansFromFile = physiciansFromFile.subList(0, 20);
        Assert.assertEquals(twentyFirstPhysiciansFromUI, twentyFirstPhysiciansFromFile,
                "The data from AG-Grid Physician summary on Physician Mapping page and from file should be the same");
    }

    @When("I read data from AG-Grid Physician summary on Physician Mapping page and compare with {string}")
    public void iReadDataFromAGGridLabSummaryOnLabMappingPageAndSaveAsLabs(String key) {
        List<Physician> twentyFirstPhysiciansFromUI = physicianMappingPage.getAGGridPhysicianSummaryForm().getListPhysician();
        List<Physician> physiciansFromFile = scenarioContext.get(key);
        List<Physician> twentyFirstPhysiciansFromFile = physiciansFromFile.subList(0, 20);
        Assert.assertEquals(twentyFirstPhysiciansFromUI, twentyFirstPhysiciansFromFile,
                "The data from AG-Grid Physician summary on Physician Mapping page and from file should be the same");
    }

    @When("I read file {string} with Physicians and save data as {string}")
    public void iReadFileExportXlsxAndSaveDataAsPhysiciansFromFile(String fileName, String key) {
        PhysicianExelTable physicianExelTable = new PhysicianExelTable(fileName);
        List<Physician> physicianListFromFile = physicianExelTable.getAllListPhysician();
        scenarioContext.add(key, physicianListFromFile);
    }

    @And("I compare number of physicians from {string} and from AG-Grid Physician summary on Physician Mapping page")
    public void iCompareNumberOfPhysiciansFromPhysiciansFromFileAndFromAGGridPhysicianSummary(String key) {
        List<Physician> physicianListFromFile = scenarioContext.get(key);
        int numberOfPhysiciansFromFile = physicianListFromFile.size();
        int numberOfPhysiciansFromUI = physicianMappingPage.getAGGridPhysicianSummaryForm().getNumberOfPhysicians();
        Assert.assertEquals(numberOfPhysiciansFromUI, numberOfPhysiciansFromFile,
                "The number of Physicians from AG-Grid Physician summary and from file should be the same");
    }

    @And("I get number of physicians from AG-Grid on Physician Mapping page and save to {string}")
    public void iGetNumberOfPhysiciansFromAGGridPhysicianMappingPage(String key) {
        int numberOfPhysiciansFromUI = physicianMappingPage.getAGGridPhysicianSummaryForm().getNumberOfPhysicians();
        scenarioContext.add(key, numberOfPhysiciansFromUI);
    }

    @Then("Values for physicians from {string} for following fields should be not empty:")
    public void valueFromPhysiciansFromFileForFollowingFieldsShouldBeNotEmpty(String key, List<String> fields) {
        List<Physician> physicianListFromFile = scenarioContext.get(key);
        physicianListFromFile.forEach(physician -> fields.forEach(field -> {
            String value = physician.getReflectionFieldValue(
                    AGGridPhysicianSummaryFormFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertNotEquals(value, "",
                    String.format("Physician with NPI - %s should be with not empty value for %s field",
                            physician.getPhysicianNPI(), field));
        }));

    }

    @When("I click {string} tab on Physician Mapping page")
    public void iClickChartsTabOnPhysicianMappingPage(String tab) {
        physicianMappingPage.clickTab(tab);
    }

    @Then("Charts form on Physician Mapping page is opened")
    public void chartsFormOnPhysicianMappingPageIsOpened() {
        Assert.assertTrue(physicianMappingPage.getPhysicianMappingChartsForm().isDisplayed(),
                "Charts form on Physician Mapping Results should be opened");
    }

    @Then("Project name from Data summary panel is {string} on Physician Mapping page")
    public void getProjectFromDataSummaryPanelOnPhysicianMappingPage(String project) {
        Assert.assertEquals(physicianMappingPage.getProjectFromDataSummaryPanel(), project,
                "Project on Data summary panel is not correct on Physician Mapping page");
    }

    @And("I click {string} link on Physician Mapping page")
    public void iClickLinkOnPhysicianMappingMainPage(String link) {
        physicianMappingPage.clickByLink(link);
    }

    @And("Active filters is {string}")
    public void getActiveFiltersFromChartForm(String expectedActiveFilters){
        Assert.assertEquals(physicianMappingPage.getPhysicianMappingChartsForm().getActiveLabels(),
                expectedActiveFilters, "Active filters is not correct");
    }

    @When("I click Download all charts on Charts form on Physician Mapping page")
    public void iClickDownloadAllChartsOnChartsFormOnPhysicianMappingPage() {
        physicianMappingPage.getPhysicianMappingChartsForm().clickDownloadAllCharts();
    }

    @And("I compare charts from file {string} and from Charts form on Physician Mapping page")
    public void iCompareChartsFromFileChartPdfAndFromChartsFormOnPhysicianMappingPage(String fileName) {
        String textFromPdfFile = PDFUtil.parsePDF(fileName);
        List<String> titlesFromPage = physicianMappingPage.getPhysicianMappingChartsForm().getStringListAllHighChartsTitle();
        titlesFromPage.forEach(title ->
                Assert.assertTrue(textFromPdfFile.contains(title),
                        String.format("Chart with title - %s should be in file", title)));
    }

    @Then("{string} must be the same as on {string} chart on Physician Mapping page")
    public void numberMustBeTheSameAsOnChartOnPhysicianMappingPage(
            String key, String chartName) {
        Integer fromGrid = scenarioContext.get(key);
        Integer numberOfLabsFromChart = physicianMappingPage.getPhysicianMappingChartsForm()
                .getTotalCountOfLabsFromChartSubTitle(PhysicianMappingCharts.getEnumValue(chartName));
        Assert.assertEquals(fromGrid, numberOfLabsFromChart,
                String.format("Number %s from Grid should be the same on chart %s", fromGrid, chartName));
    }

    @Then("Number from subtitle from chart {string} should be equal {string} on Physician Mapping page")
    public void numberMustBeEqualOnPhysicianMappingPage(String chartName, String expectedNumber) {
        int number = physicianMappingPage.getPhysicianMappingChartsForm()
                .getTotalCountOfLabsFromChartSubTitle(PhysicianMappingCharts.getEnumValue(chartName));
        Assert.assertEquals(String.valueOf(number), expectedNumber,
                String.format("Number %d from chart should be the same as %s", number, expectedNumber));
    }

    @Then("I get number from subtitle from chart {string} on Physician Mapping page and save to {string}")
    public void numberFromChartOnPhysicianMappingPage(String chartName, String key) {
        scenarioContext.add(key,physicianMappingPage.getPhysicianMappingChartsForm()
                .getTotalCountOfLabsFromChartSubTitle(PhysicianMappingCharts.getEnumValue(chartName)));
    }

    @Then("Int sum of {string} and {string} should be equal {string} on Physician Mapping page")
    public void checkSumOfTwoValues(String firstValueKey, String secondValueKey, String keySum){
        int sum = scenarioContext.get(keySum);
        int firstValue = scenarioContext.get(firstValueKey);
        int secondValue = scenarioContext.get(secondValueKey);
        Assert.assertEquals(sum, firstValue + secondValue, String.format("%d plus %d should be equal %d", firstValue, secondValue, sum));
    }

    @When("I click Download chart {string} on Charts form on Physician Mapping page and save charts title as {string}")
    public void iClickDownloadChartChartNameOnChartsFormOnPhysicianMappingPage(String chartName, String key) {
        String chartTitle = physicianMappingPage.getPhysicianMappingChartsForm()
                .getHighchartsTitle(PhysicianMappingCharts.getEnumValue(chartName));

        scenarioContext.add(key, chartTitle);
        physicianMappingPage.getPhysicianMappingChartsForm()
                .clickDownloadAChart(PhysicianMappingCharts.getEnumValue(chartName));
    }

    @And("File {string} contains a chart with title {string}")
    public void fileContainsAChartWithTitle(String fileName, String key) {
        String chartTitleFromPage = scenarioContext.get(key);
        String textFromPdfFile = PDFUtil.parsePDF(fileName);
        Assert.assertTrue(textFromPdfFile.contains(chartTitleFromPage),
                String.format("Chart with title - %s should be in file", chartTitleFromPage));
    }

    @And("Search filter {string} with following fields is displayed on Physician Mapping page")
    public void searchFilterSearchFilterWithFollowingFieldsIsDisplayedOnPhysicianMappingPage(String key, List<String> fields) {
        SearchFilter filter = scenarioContext.get(key);
        filter.setDateRange();
        fields.forEach(field -> {
            String actualValue = physicianMappingPage.getFieldValue(PhysicianMappingPageFields.getEnumValue(field));
            String expectedValue = filter.getReflectionFieldValue(PhysicianMappingPageFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value - %s for field - %s is not correct on Testing Dashboard page",
                            actualValue, field));
        });

    }

    @When("I click {string} on Physician Mapping page")
    public void iClickEditSearchOnPhysicianMappingPage(String buttonName) {
        physicianMappingPage.clickByButton(buttonName);
    }

    @Then("Edit settings form is opened on Physician Mapping page")
    public void editSettingsFormIsOpenedOnPhysicianMappingPage() {
        Assert.assertTrue(physicianMappingPage.getEditSettingsForm().isDisplayed(),
                "Edit settings form on Physician Mapping page should be opened");
    }

    @When("I fill following fields on Edit settings form on Physician Mapping page and save {string}:")
    public void iFillFollowingFieldsOnEditSettingsFormOnPhysicianMappingPageAndSaveSearchFilter(
            String key, Map<String, String> data) {
        SearchFilter filter = new SearchFilter();
        data.forEach((field, value) -> {
            String selectedValue = physicianMappingPage.getEditSettingsForm().setFieldValue(
                    EditSettingsFormFields.getEnumValue(field), value);

            filter.setReflectionFieldValue(
                    EditSettingsFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, filter);
    }

    @And("I click {string} on Edit settings form on Physician Mapping")
    public void iClickApplyOnEditSettingsFormOnPhysicianMapping(String buttonName) {
        physicianMappingPage.getEditSettingsForm().clickByButton(buttonName);
    }

    @When("I click Main page link on Physician Mapping page")
    public void iClickMainPageLinkOnPhysicianMappingPage() {
        physicianMappingPage.clickMainPage();
    }

    @And("Icon {string} is displayed on heading panel on Physician Mapping page")
    public void iconIsDisplayedOnPhysicianMappingPage(String icon){
        Assert.assertTrue(physicianMappingPage.isIconDisplayed(icon), String.format("Icon %s should be displayed", icon));
    }

    @Then("I get number from {string} heading banner on Physician Mapping page and save to {string}")
    public void getTNumberFromHeadingBannerOnPhysicianMappingPage(String banner,String key) {
        Integer number = physicianMappingPage.getNumberFromHeadingBanner(banner);
        scenarioContext.add(key, number);
    }

    @Then("I get number of heading banner {string} and compare with saved value {string} on Physician Mapping page")
    public void getNumberOfHeadingBannerAndCompareWithSavedValueOnPhysicianMappingPage(String fromUI, String key){
        Integer numberFromUI = scenarioContext.get(fromUI);
        Integer savedNumber = scenarioContext.get(key);
        Assert.assertEquals(numberFromUI, savedNumber, String.format("Number should be equal from UI %S and from service %s", numberFromUI, savedNumber));
    }
}
