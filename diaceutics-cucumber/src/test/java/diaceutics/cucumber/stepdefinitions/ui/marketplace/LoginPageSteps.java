package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.pagefields.assaymanagement.AddAnAssayPageFields;
import diaceutics.selenium.enums.pagefields.marketplace.LoginPageFields;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.marketplace.LoginPage;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Map;

public class LoginPageSteps {

    private final LoginPage loginPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public LoginPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        loginPage = new LoginPage();
    }

    @Given("Login page is opened")
    public void loginPageIsOpened() {
        Assert.assertTrue(loginPage.isDisplayed(), "login page should be opened");
    }

    @When("I login as {string} user")
    public void iLoginAsUser(String key) {
        User user = scenarioContext.get(key);
        loginPage.logInAs(user);
    }

    @When("I click login button on Login page")
    public void iClickLoginButtonOnLoginPage() {
        loginPage.clickLoginButton();
    }

    @Then("Message {string} is displayed on Login page")
    public void messageWrongEmailOrPasswordIsDisplayedOnLoginPage(String message) {
        Assert.assertTrue(loginPage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Login page", message));

    }

    @Then("Message {string} for {string} field is displayed on Login page")
    public void messageThisFieldIsRequiredForEmailFieldIsDisplayedOnLoginPage(String message, String field) {
        Assert.assertTrue(loginPage.isMessageForFieldDisplayed(message, field),
                String.format("Message %s for %s field should be displayed on Login page", message, field));
    }

    @When("I fill following fields on Login page:")
    public void iFillFollowingFieldsOnLoginPage(Map<String, String> data) {
        data.forEach((field, value) -> loginPage.setFieldValue(LoginPageFields.getEnumValue(field), value));
    }

    @When("I click by {string} link on Login page")
    public void iClickBySignUpLinkOnLoginPage(String linkName) {
        loginPage.clickByLink(linkName);
    }
}
