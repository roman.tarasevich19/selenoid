package diaceutics.cucumber.stepdefinitions.ui.newRegistration;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.enums.pagefields.newregistration.NewPersonalDetailsFormFields;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.newregistration.NewRegistrationPage;
import diaceutics.selenium.utilities.MailUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class NewRegistrationPageSteps {

    private final NewRegistrationPage registrationPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public NewRegistrationPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        registrationPage = new NewRegistrationPage();
    }

    @Given("New registration page is opened")
    public void registerPageIsOpened() {
        Assert.assertTrue(registrationPage.isDisplayed(), "Registration page should be opened");
    }

    @When("I fill following fields for first step on Personal Details form on new Registration page and save as {string}:")
    public void iSetFollowingValuesForFirstStepRegisterFormAndSaveAsAccount(String key, Map<String, String> data) {
        User user = new User();
        data.forEach((field, value) -> {
            value = value.equals(FieldValues.MAILOAUR.toString()) ? MailUtil.createMailAddress() : value;
            String selectedValue = registrationPage.getNewPersonalDetailsForm()
                    .setFieldValue(NewPersonalDetailsFormFields.getEnumValue(field), value);

            user.setReflectionFieldValue(NewPersonalDetailsFormFields.getEnumValue(field).getModelField(), selectedValue);
        });
        scenarioContext.add(key, user);
    }

    @When("I fill following fields for second step on Personal Details form on new Registration page and save as {string}:")
    public void iSetFollowingValuesForSecondStepRegisterFormAndSaveAsAccount(String key, Map<String, String> data) {
        User user = scenarioContext.get(key);
        data.forEach((field, value) -> {
            value = registrationPage.getNewPersonalDetailsForm()
                    .setFieldValue(NewPersonalDetailsFormFields.getEnumValue(field), value);

            user.setReflectionFieldValue(NewPersonalDetailsFormFields.getEnumValue(field).getModelField(), value);
        });
        scenarioContext.add(key, user);
    }

    @When("I fill following fields to create new user on Personal Details form on new Registration page using data from {string}")
    public void iSetFollowingValuesFromUserForFirstStepRegisterFormAndSaveAsAccount(String key, List<String> data) {
        User user = scenarioContext.get(key);
        data.forEach((field) -> {
            String selectedValue = user.getReflectionFieldValue(NewPersonalDetailsFormFields.getEnumValue(field).getModelField());
            registrationPage.getNewPersonalDetailsForm()
                    .setFieldValue(NewPersonalDetailsFormFields.getEnumValue(field), selectedValue);
        });
    }

    @And("I click {string} on new Registration page")
    public void iClickOnButtonOnNewRegistrationPage(String button) {
        registrationPage.getNewPersonalDetailsForm().clickByButton(button);
    }

    @When("I click {string} link on new Registration page")
    public void iClickLinkOnRegistrationPage(String linkName) {
        registrationPage.clickByLink(linkName);
    }

    @Then("Result message {string} is present on new Registration page")
    public void registrationPassedSuccessfully(String message) {
        Assert.assertTrue(registrationPage.isLabelDisplayed(message),
                String.format("Message %s should be present", message));
    }

    @When("I opened New registration page")
    public void iOpenedNewRegistrationPage() {
        registrationPage.openNewRegistrationPage();
    }

    @And("Form with error message {string} is displayed on new Registration page")
    public void formWithErrorMessageIsDisplayedOnRegistrationPage(String message) {
        Assert.assertTrue(registrationPage.getNewPersonalDetailsForm().isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on Registration page", message));
    }

    @Then("Error container is displayed for {string} field on new Registration page")
    public void errorContainerIsDisplayedForFieldOnRegistrationPage(String field) {
        SoftAssert.getInstance().assertTrue(registrationPage.getNewPersonalDetailsForm()
                        .isRedBorderDisplayedForField(NewPersonalDetailsFormFields.getEnumValue(field)),
                String.format("Error container for field - %s should be displayed", field));
    }
}
