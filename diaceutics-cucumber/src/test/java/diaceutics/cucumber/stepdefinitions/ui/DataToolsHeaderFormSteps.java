package diaceutics.cucumber.stepdefinitions.ui;

import diaceutics.selenium.pageobject.forms.DataToolsHeaderForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class DataToolsHeaderFormSteps {

    private final DataToolsHeaderForm dataToolsHeaderForm;

    public DataToolsHeaderFormSteps() {
        dataToolsHeaderForm = new DataToolsHeaderForm();
    }

    @When("I click {string} on Data Tools Header form")
    public void iClickOnDataToolsHeaderFormOnAssayManagementPage(String linkName) {
        dataToolsHeaderForm.clickBy(linkName);
    }

    @When("Tool {string} is not available on Masted Header Form")
     public void isToolLabelNotPresent(String tool){
        Assert.assertFalse(dataToolsHeaderForm.isLinkAvailable(tool), String.format("Tool: %s should be not present on Master Header Form", tool));
    }

    @And("{string} link on Data Tools Header is highlighted in {string} color")
    public void dataToolsLinkOnDataToolsHeaderIsHighlightedInPurpleColor(String linkName, String color) {
        Assert.assertTrue(dataToolsHeaderForm.isLinkHighlightedInColor(linkName, color),
                String.format("%s link on Data Tools Header should be highlighted in %s color", linkName, color));
    }
}
