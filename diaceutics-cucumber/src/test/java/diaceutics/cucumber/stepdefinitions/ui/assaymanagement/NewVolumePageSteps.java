package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.assaymanagement.BiomarkerVolumeFormFields;
import diaceutics.selenium.enums.pagefields.assaymanagement.NewVolumePageFields;
import diaceutics.selenium.models.Assay;
import diaceutics.selenium.models.BiomarkerVolume;
import diaceutics.selenium.models.Volume;
import diaceutics.selenium.pageobject.pages.assaymanagement.NewVolumePage;
import diaceutics.selenium.utilities.NumberUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;


public class NewVolumePageSteps {

    private final NewVolumePage newVolumePage;
    private final ScenarioContext scenarioContext;

    @Inject
    public NewVolumePageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        newVolumePage = new NewVolumePage();
    }

    @Then("New volume page is opened")
    public void newVolumePageIsOpened() {
        Assert.assertTrue(newVolumePage.isDisplayed(), "New volume page should be opened");
    }

    @When("I fill following fields on New volume page and save as {string}:")
    public void iFillFollowingFieldsOnNewVolumePageAndSaveAsVolume(String key, Map<String, String> data) {
        Volume volume = new Volume();
        data.forEach((field, value) -> {
            String selectedValue = newVolumePage.setFieldValue(NewVolumePageFields.getEnumValue(field), value);
            selectedValue = field.equals(NewVolumePageFields.DISEASE_VOLUME.getFriendlyName())
                    && Integer.parseInt(selectedValue) < 50 ? "<50" : selectedValue;

            volume.setReflectionFieldValue(NewVolumePageFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, volume);
    }

    @And("I click {string} on New volume page")
    public void iClickSaveAndFinishOnNewVolumePage(String buttonName) {
        newVolumePage.clickByButton(buttonName);
    }

    @And("I fill following fields on Biomarker volume form on New volume page and add biomarker volume to {string}:")
    public void iFillFollowingFieldsOnBiomarkerVolumeFormOnNewVolumePageAndAddBiomarkerVolumeToVolume(
            String key, Map<String, String> data) {
        Volume volume = scenarioContext.get(key);
        BiomarkerVolume biomarkerVolume = new BiomarkerVolume();
        data.forEach((field, value) -> {
            String selectedValue = newVolumePage.getBiomarkerVolumeForm()
                    .setFieldValue(BiomarkerVolumeFormFields.getEnumValue(field), value);
            biomarkerVolume.setReflectionFieldValue(BiomarkerVolumeFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        volume.addBiomarkerVolumes(biomarkerVolume);
    }

    @When("I fill following fields on New volume page using data from {string}:")
    public void iFillFollowingFieldsOnNewVolumePageUsingDataFromVolume(String key, List<String> fields) {
        Volume volume = scenarioContext.get(key);
        fields.forEach(field -> newVolumePage.setFieldValue(NewVolumePageFields.getEnumValue(field),
                volume.getReflectionFieldValue(NewVolumePageFields.getEnumValue(field).getModelField())));
    }

    @Then("Message {string} for {string} is displayed on New volume page")
    public void messageForVolumeIsDisplayedOnNewVolumePage(String messageTemplate, String key) {
        Volume volume = scenarioContext.get(key);
        String expectedMessage = String.format(messageTemplate, volume.getYear(), volume.getQuarter());
        Assert.assertTrue(newVolumePage.isAlertMessageDisplayed(expectedMessage),
                String.format("Message %s should be displayed on Log test volume form", expectedMessage));
    }

    @And("I set random value from {int} to {int} for field {string} on New volume page and save for {string}")
    public void iSetRandomValueFromToForFieldDiseaseVolumeOnNewVolumePageAndSaveForVolume(
            int min, int max, String field, String key) {
        Volume volume = scenarioContext.get(key);
        int randomValue = NumberUtil.getRandomInt(min, max);
        String selectedValue = newVolumePage.setFieldValue(NewVolumePageFields.getEnumValue(field), String.valueOf(randomValue));
        selectedValue = Integer.parseInt(selectedValue) < 50 ? "<50" : selectedValue;
        volume.setReflectionFieldValue(NewVolumePageFields.getEnumValue(field).getModelField(), selectedValue);
    }

    @And("Message {string} is displayed on required fields on New volume page")
    public void messageIsDisplayedOnRequiredFieldsOnNewVolumePage(String message) {
        Assert.assertTrue(newVolumePage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on New volume page", message));
    }

    @And("I click {string} button on Biomarker volume form on New volume page")
    public void iClickAddAnotherButtonOnBiomarkerVolumeFormOnNewVolumePage(String buttonName) {
        newVolumePage.getBiomarkerVolumeForm().clickByButton(buttonName);
    }

    @And("I set value {string} for {string} field on Biomarker volume form on New volume page and save to {string}")
    public void iSetValueForFieldOnBiomarkerVolumeFormOnNewVolumePageAndSaveToVolume(
            String value, String field, String key) {
        Volume volume = scenarioContext.get(key);
        newVolumePage.getBiomarkerVolumeForm().setFieldValue(BiomarkerVolumeFormFields.getEnumValue(field), value);
        List<BiomarkerVolume> biomarkerVolumes = volume.getBiomarkerVolumes();
        biomarkerVolumes.forEach(biomarkerVolume -> biomarkerVolume.setVolume(value));
    }

    @And("Red border is displayed for following fields on New volume page:")
    public void redBorderIsDisplayedForFollowingFieldsOnNewVolumePage(List<String> fields) {
        fields.forEach(field -> SoftAssert.getInstance().assertTrue(
                newVolumePage.isRedBorderDisplayedForField(NewVolumePageFields.getEnumValue(field)),
                String.format("Red border should be displayed for %s field on New volume page", field)));
    }

    @And("Red border is displayed for following fields on Biomarker volume form on New volume page:")
    public void redBorderIsDisplayedForFollowingFieldsOnBiomarkerVolumeFormOnNewVolumePage(List<String> fields) {
        fields.forEach(field -> SoftAssert.getInstance().assertTrue(
                newVolumePage.getBiomarkerVolumeForm()
                        .isRedBorderDisplayedForField(BiomarkerVolumeFormFields.getEnumValue(field)),
                String.format("Red border should be displayed for %s field on Biomarker volume form on New volume page",
                        field)));
    }

    @When("I set {string} for {string} field on Biomarker volume form on New volume page")
    public void iSetForFieldOnBiomarkerVolumeFormOnNewVolumePage(String value, String field) {
        newVolumePage.getBiomarkerVolumeForm().setFieldValue(BiomarkerVolumeFormFields.getEnumValue(field), value);
    }

    @And("I add Biomarker the same as in {string} on Biomarker volume form on New volume page for {string}")
    public void iAddBiomarkerTheSameAsInAssayOnBiomarkerVolumeFormOnNewVolumePageForVolume(String assayKey, String volumeKey) {
        Assay assay = scenarioContext.get(assayKey);
        Volume volume = scenarioContext.get(volumeKey);
        BiomarkerVolume biomarkerVolume = volume.getBiomarkerVolumes().get(0);
        String selectedValue = newVolumePage.getBiomarkerVolumeForm()
                .setFieldValue(BiomarkerVolumeFormFields.BIOMARKER, assay.getBiomarkers().get(0).getBiomarkerName());
        biomarkerVolume.setReflectionFieldValue(BiomarkerVolumeFormFields.BIOMARKER.getModelField(), selectedValue);
    }

    @And("I set random value from {int} to {int} for field {string} on Biomarker volume form on New volume page and save for {string}")
    public void iSetRandomValueFromToForFieldOnBiomarkerVolumeFormOnNewVolumePageAndSaveForVolume(
            int min, int max, String field, String key) {
        Volume volume = scenarioContext.get(key);
        BiomarkerVolume biomarkerVolume = volume.getBiomarkerVolumes().get(0);
        int randomValue = NumberUtil.getRandomInt(min, max);
        String selectedValue = newVolumePage.setFieldValue(BiomarkerVolumeFormFields.getEnumValue(field), String.valueOf(randomValue));
        selectedValue = Integer.parseInt(selectedValue) < 50 ? "<50" : selectedValue;
        biomarkerVolume.setReflectionFieldValue(BiomarkerVolumeFormFields.getEnumValue(field).getModelField(), selectedValue);
    }
}
