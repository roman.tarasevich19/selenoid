package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import aquality.selenium.browser.AqualityServices;
import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.restassured.project.models.BiomarkerDto;
import diaceutics.restassured.project.models.collections.CollectionBiomarkers;
import diaceutics.selenium.enums.pagefields.assaymanagement.AddAnAssayPageFields;
import diaceutics.selenium.enums.pagefields.assaymanagement.LabTestBiomarkerFormFields;
import diaceutics.selenium.models.Assay;
import diaceutics.selenium.models.Biomarker;
import diaceutics.selenium.pageobject.pages.assaymanagement.AddAnAssayPage;
import diaceutics.cucumber.utilities.apputils.BiomarkerUtil;
import diaceutics.selenium.utilities.CsvUtil;
import diaceutics.selenium.utilities.FileUtil;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class AddAnAssayPageSteps {

    private final AddAnAssayPage addAnAssayPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public AddAnAssayPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        addAnAssayPage = new AddAnAssayPage();
    }

    @Given("Add an Assay page is opened")
    public void addAnAssayPageIsOpened() {
        Assert.assertTrue(addAnAssayPage.isDisplayed(), "Add an Assay page should be opened");
    }

    @When("I fill following fields on Add an Assay page and save as {string}:")
    public void fillFollowingFieldsOnAddAnAssayPageAndSave(String key, Map<String, String> data) {
        Assay assay = new Assay();
        data.forEach((field, value) -> {
            value = field.equals(AddAnAssayPageFields.ASSAY_NAME.getFriendlyName()) ? value + TimeUtil.getTimestamp() : value;
            String selectedValue = addAnAssayPage.setFieldValue(AddAnAssayPageFields.getEnumValue(field), value);
            assay.setReflectionFieldValue(AddAnAssayPageFields.getEnumValue(field).getModelField(), selectedValue);
        });

        assay.setClassifications(assay.addClassifications());
        scenarioContext.add(key, assay);
    }

    @And("I click {string} on Add an Assay page")
    public void iClickOnButtonAddAnAssayPage(String buttonName) {
        addAnAssayPage.clickByButton(buttonName);
    }

    @And("I click {string} on Discard form on Add an Assay page")
    public void iClickOnButtonDiscardFormAddAnAssayPage(String buttonName) {
        addAnAssayPage.getDiscardForm().clickByButton(buttonName);
    }

    @And("Compare with {string} and number of rows of invalid biomarkers are the same in {string} and displayed on Lab Test Biomarker form")
    public void numberOfRowsOfInvalidBiomarkers(String key, String fileName) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        CollectionBiomarkers allValidBiomarkers = scenarioContext.get(key);
        Assert.assertEquals(addAnAssayPage.getLabTestBiomarkerForm().getInvalidBiomarkersRow(),
                BiomarkerUtil.getInvalidBiomarkersFromFile(filePath,
                        allValidBiomarkers.getContent().stream()
                                .map(BiomarkerDto::getName)
                                .collect(Collectors.toList())).size(),
                "Number of invalid biomarkers is not correct");
    }

    @And("Compare with {string} and number of rows of invalid biomarkers are not the same in {string} and displayed on Lab Test Biomarker form")
    public void numberOfRowsOfInvalidBiomarkersAreNotTheSame(String key, String fileName) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        CollectionBiomarkers allValidBiomarkers = scenarioContext.get(key);
        Assert.assertNotEquals(addAnAssayPage.getLabTestBiomarkerForm().getInvalidBiomarkersRow(),
                BiomarkerUtil.getInvalidBiomarkersFromFile(filePath,
                        allValidBiomarkers.getContent().stream()
                                .map(BiomarkerDto::getName)
                                .collect(Collectors.toList())).size(),
                "Number of invalid biomarkers is not correct");
    }

    @And("{string} rows of valid biomarkers are displayed on Lab Test Biomarker form")
    public void numberOfRowsOfValidBiomarkers(String numberOfRows) {
        Assert.assertEquals(addAnAssayPage.getLabTestBiomarkerForm().getValidBiomarkersRow(), numberOfRows,
                "Number of valid biomarkers should be: " + numberOfRows);
    }

    @Then("Lab Test Biomarker form is opened on Add an Assay page")
    public void labTestBiomarkerFormIsOpened() {
        Assert.assertTrue(addAnAssayPage.getLabTestBiomarkerForm().isDisplayed(),
                "Lab Test Biomarker form should be opened on Add an Assay page");
    }

    @When("I set {string} on search field {string} and press Search icon on Lab Test Biomarker form")
    public void searchOnLabTestFormAddAnAssayPage(String search, String searchFieldName) {
        addAnAssayPage.putTextInSearchField(search, searchFieldName);
        addAnAssayPage.clickSearch();
    }

    @Then("Move this assay form is opened on Add an Assay page")
    public void moveThisAssayFormIsOpened() {
        Assert.assertTrue(addAnAssayPage.getLabTestBiomarkerForm().isDisplayed(),
                "Add Biomarker form should be opened on Add an Assay page");
    }

    @When("Upload biomarkers form is opened")
    public void uploadBiomarkersFormIsOpened() {
        Assert.assertTrue(addAnAssayPage.getUploadBiomarkerForm().isDisplayed(), "Upload Biomarkers form is opened");
    }

    @When("Upload biomarkers form is closed")
    public void uploadBiomarkersFormIsClosed() {
        Assert.assertFalse(addAnAssayPage.getUploadBiomarkerForm().isDisplayed(), "Upload Biomarkers form is closed");
    }

    @Then("Message {string} is displayed on Add an Assay page")
    public void messageSomeItemsBelowNeedYourAttentionIsDisplayedOnAddAnAssayPage(String message) {
        Assert.assertTrue(addAnAssayPage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Add an Assay page", message));
    }

    @And("Message {string} is displayed on required fields on Add an Assay page")
    public void messageOnRequiredFieldsOnAddAnAssayPage(String message) {
        Assert.assertTrue(addAnAssayPage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Add an Assay page", message));
    }

    @Then("Field {string} should be disabled on Add an Assay page")
    public void fieldSendOutLabShouldBeDisabledOnAddAnAssayPage(String field) {
        Assert.assertFalse(addAnAssayPage.isFieldEnabled(AddAnAssayPageFields.getEnumValue(field)),
                String.format("Field %s should be disabled on Add an Assay page", field));
    }

    @Then("Field {string} should be enabled on Add an Assay page")
    public void fieldPanelNameShouldBeEnabledOnAddAnAssayPage(String field) {
        Assert.assertTrue(addAnAssayPage.isFieldEnabled(AddAnAssayPageFields.getEnumValue(field)),
                String.format("Field %s should be enabled on Add an Assay page", field));
    }

    @When("I click on {string} biomarkers on Lab Test Biomarker form")
    public void clickOnButtonOnLabTestBiomarkerForm(String buttonName) {
        addAnAssayPage.getLabTestBiomarkerForm().clickOnLabTestFormButton(buttonName);
    }

    @And("Downloaded CSV {string} contains header {string} on Lab Test Biomarker form")
    public void downloadedCsvForBiomarkersContainsText(String fileName, String text) {
        String path = FileUtil.getDownloadDirPath() + File.separator + fileName;
        List<String> textFromCsvFile = CsvUtil.readStoredCsvFileByRows(path);
        AqualityServices.getLogger().info("FROM file:" + textFromCsvFile.toString());
        Assert.assertTrue(textFromCsvFile.toString().contains(text),
                String.format("%s should be in file", text));
    }

    @Then("Compare with {string} and valid biomarkers from imported file {string} are displayed in grid in Lab Test Biomarker form")
    public void allBiomarkersAreDisplayed(String key, String fileName) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        CollectionBiomarkers allValidBiomarkers = scenarioContext.get(key);
        Assert.assertEquals(addAnAssayPage.getLabTestBiomarkerForm().getValidBiomarkersFromGrid(),
                BiomarkerUtil.getValidBiomarkersFromFile(filePath, allValidBiomarkers.getContent().stream()
                        .map(BiomarkerDto::getName)
                        .collect(Collectors.toList())),
                String.format("All Biomarkers from imported file %s should be present in grid", fileName));
    }

    @Then("Each biomarker contains {string} after filtering on grid in Lab Test Biomarker form")
    public void eachBiomarkerAreDisplayedByFilter(String filter) {
        Assert.assertTrue(addAnAssayPage.getLabTestBiomarkerForm().getValidBiomarkersFromGrid().stream().allMatch(biomarker -> biomarker.contains(filter)),
                String.format("All Biomarkers searching by %s should be present in grid", filter));
    }

    @Then("The following values should be available for {string} field on Add an Assay page:")
    public void theFollowingValuesShouldBeAvailableForCommercialAssaysFieldOnAddAnAssayPage(String field, List<String> values) {
        List<String> options = addAnAssayPage.getListOptionsFromField(AddAnAssayPageFields.getEnumValue(field));
        values.forEach(value -> SoftAssert.getInstance().assertTrue(options.contains(value),
                String.format("Value %s should be available for %s field on Add an Assay page", value, field)));
    }

    @When("I fill following fields in Lab Test Biomarker form on Add an Assay page and save as {string}:")
    public void iFillFollowingFieldsOnLabTestBiomarkerFormOnAddAnAssayPageAndSaveAsBiomarkerOne(String key, Map<String, String> data) {
        Biomarker biomarker = new Biomarker();
        data.forEach((field, value) -> {
            String selectedValue = addAnAssayPage.getLabTestBiomarkerForm().setFieldValue(LabTestBiomarkerFormFields.getEnumValue(field), value);
            biomarker.setReflectionFieldValue(LabTestBiomarkerFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, biomarker);
    }

    @And("Commercial assay Biomarkers are added to this assay {string}")
    public void commercialAssayBiomarkersFromAssayIsDisplayedOnEditAssayPage(String key) {
        Assay assay = scenarioContext.get(key);
        Assert.assertTrue(addAnAssayPage.isCommercialAssayBiomarkerAdded(assay),
                String.format("Commercial assay Biomarkers %s should be added to this assay %s",
                        assay.getCommercialAssays(), assay.getAssayName()));
    }

    @And("Red border is displayed for following fields on Add an Assay page:")
    public void redBorderIsDisplayedForFollowingFieldsOnAddAnAssayPage(List<String> fields) {
        fields.forEach(field -> SoftAssert.getInstance().assertTrue(
                addAnAssayPage.isRedBorderDisplayedForField(AddAnAssayPageFields.getEnumValue(field)),
                String.format("Red border should be displayed for %s field on Add an Assay page",
                        field)));
    }

    @And("Red border is displayed for {string} field on Lab Test Biomarker form on Add an Assay page")
    public void redBorderIsDisplayedForBiomarkerFieldOnLabTestBiomarkerFormOnAddAnAssayPage(String field) {
        SoftAssert.getInstance().assertTrue(
                addAnAssayPage.getLabTestBiomarkerForm().isRedBorderDisplayedForField(LabTestBiomarkerFormFields.getEnumValue(field)),
                String.format("Red border should be displayed for %s field on Lab Test Biomarker form on Add an Assay page",
                        field));
    }

    @When("I fill following fields in Lab Test Biomarker form on Add an Assay page and add to {string}:")
    public void iFillFollowingFieldsInLabTestBiomarkerFormOnAddAnAssayPageAndAddToAssay(String key, Map<String, String> data) {
        Assay assay = scenarioContext.get(key);
        Biomarker biomarker = new Biomarker();
        data.forEach((field, value) -> {
            String selectedValue = addAnAssayPage.getLabTestBiomarkerForm().setFieldValue(LabTestBiomarkerFormFields.getEnumValue(field), value);
            biomarker.setReflectionFieldValue(LabTestBiomarkerFormFields.getEnumValue(field).getModelField(), selectedValue);
        });
        assay.addBiomarker(biomarker);
    }

    @When("I set {string} value for Biomarker field in Lab Test Biomarker form on Add Assay page and save as {string}")
    public void iSetRandomValueForBiomarkerFieldInLabTestBiomarkerFormOnAddAssayPageAndSaveAsBiomarker(
            String value, String key) {
        Biomarker biomarker = new Biomarker();
        String selectedValue = addAnAssayPage.getLabTestBiomarkerForm().setFieldValue(LabTestBiomarkerFormFields.BIOMARKER, value);
        biomarker.setBiomarkerName(selectedValue);
        scenarioContext.add(key, biomarker);
    }

    @And("I fill following values for Variants field in Lab Test Biomarker form on Add Assay page and add to {string}")
    public void iFillFollowingValuesForVariantsFieldInLabTestBiomarkerFormOnAddAssayPageAndAddToBiomarker(
            String key, List<String> values) {
        Biomarker biomarker = scenarioContext.get(key);
        values.forEach(value -> {
            addAnAssayPage.getLabTestBiomarkerForm().setFieldValue(LabTestBiomarkerFormFields.VARIANTS, value);
            biomarker.addVariant(value);
        });
    }

    @Then("Variants from {string} are displayed on Lab Test Biomarker form on Add an Assay page")
    public void variantsFromBiomarkerAreDisplayedOnLabTestBiomarkerFormOnAddAnAssayPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        List<String> expectedVariants = biomarker.getListOfVariants();
        List<String> actualVariants = addAnAssayPage.getLabTestBiomarkerForm().getListVariantsForBiomarker(biomarker.getBiomarkerName());
        Assert.assertTrue(actualVariants.containsAll(expectedVariants),
                String.format("Variants from Biomarker %s should be displayed on Lab Test Biomarker form on Add an Assay page",
                        biomarker.getBiomarkerName()));
    }

    @Then("Each variant contains {string} after filtering on Lab Test Biomarker form on Add an Assay page")
    public void allVariantsAreContainsSearchTextOnLabTestBiomarkerFormOnAddAnAssayPage(String searchText) {
        Assert.assertTrue(addAnAssayPage.getLabTestBiomarkerForm().getAllVariantsFromAllRows().stream().allMatch(variant -> variant.contains(searchText)),
                String.format("All variants searching by %s should be present in grid", searchText));
    }

    @And("I set status {string} for Assay {string} on Add an Assay page")
    public void iSetStatusInactiveForAssayAssayOnAddAnAssayPage(String status, String key) {
        Assay assay = scenarioContext.get(key);
        addAnAssayPage.setFieldValue(AddAnAssayPageFields.STATUS, status);
        assay.setReflectionFieldValue(AddAnAssayPageFields.STATUS.getModelField(), status);
    }

    @And("Status {string} is displayed on Add an Assay page")
    public void statusActiveIsDisplayedOnAddAnAssayPage(String expectedStatus) {
        String actualStatus = addAnAssayPage.getStatus();
        Assert.assertEquals(actualStatus, expectedStatus,
                String.format("Status %s should be displayed on Add an Assay page", expectedStatus));
    }
}
