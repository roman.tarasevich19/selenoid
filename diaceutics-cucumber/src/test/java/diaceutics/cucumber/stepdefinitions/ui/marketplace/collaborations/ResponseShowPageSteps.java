package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.ResponseShowPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class ResponseShowPageSteps {

    private final ResponseShowPage responseShowPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public ResponseShowPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        responseShowPage = new ResponseShowPage();
    }

    @Then("Response Show page is opened")
    public void responseShowPageIsOpened() {
        Assert.assertTrue(responseShowPage.isDisplayed(), "Response Show page should be opened");
    }

    @Then("Inform Form with following message is displayed on Response Show page:")
    public void informFormWithFollowingMessageIsDisplayedOnResponseShowPage(String message) {
        Assert.assertTrue(responseShowPage.getInformForm().isMessageDisplayed(message),
                String.format("Inform Form with message %s is displayed on Response Show page", message));
    }

    @And("Location for collaboration {string} is displayed on Response Show page")
    public void locationForCollaborationIsDisplayedOnResponseShowPage(String key) {
        Collaboration expectedCollaboration = scenarioContext.get(key);
        String actualCountry = responseShowPage.getCountry();
        Assert.assertEquals(actualCountry, expectedCollaboration.getCountry(),
                String.format("Location for response should be %s on Response Show page", expectedCollaboration.getCountry()));
    }

    @And("Contact details for user {string} is displayed on Response Show page")
    public void contactDetailsForUserIsDisplayedOnResponseShowPage(String key) {
        User user = scenarioContext.get(key);
        String actualEmail = responseShowPage.getUserEmail();
        Assert.assertEquals(actualEmail, user.getEmail(),
                String.format("Contact details for user should be %s on Response Show page", user.getEmail()));
    }

    @When("I click {string} button on Response Show page")
    public void iClickShortlistButtonOnResponseShowPage(String buttonName) {
        responseShowPage.clickBy(buttonName);
    }
}
