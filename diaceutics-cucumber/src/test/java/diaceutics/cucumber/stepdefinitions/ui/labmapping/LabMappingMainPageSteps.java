package diaceutics.cucumber.stepdefinitions.ui.labmapping;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.pagefields.labmapping.LabMappingMainPageFields;
import diaceutics.selenium.models.SearchFilter;
import diaceutics.selenium.models.Volume;
import diaceutics.selenium.pageobject.pages.labmapping.LabMappingMainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class LabMappingMainPageSteps {

    private final LabMappingMainPage labMappingMainPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public LabMappingMainPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        labMappingMainPage = new LabMappingMainPage();
    }

    @Then("Lab Mapping Main page is opened")
    public void labMappingMainIsOpened() {
        Assert.assertTrue(labMappingMainPage.isDisplayed(), "Lab Mapping Main page should be opened");
    }

    @And("Lab Mapping Main page subheader equals {string}")
    public void isServiceLabelCorrect(String serviceExplainer) {
        Assert.assertEquals(labMappingMainPage.getTextFromSeviceExplainer(), serviceExplainer.trim(),
                "Lab Mapping service label is not correct");
    }

    @When("I fill following fields on Lab Mapping Main page:")
    public void iFillFollowingFieldsOnLabMappingMainPage(Map<String, String> data) {
        data.forEach((field, value) -> labMappingMainPage.setFieldValue(LabMappingMainPageFields.getEnumValue(field), value));
    }

    @And("I click {string} on Lab Mapping Main page")
    public void iClickStartOnLabMappingMainPage(String buttonName) {
        labMappingMainPage.clickByButton(buttonName);
    }

    @And("I click {string} without wait on Lab Mapping Main page")
    public void iClickStartWithoutWaitOnLabMappingMainPage(String buttonName) {
        labMappingMainPage.clickByButtonWithoutWait(buttonName);
    }

    @And("I click {string} link on Lab Mapping Main page")
    public void iClickLinkOnLabMappingMainPage(String link) {
        labMappingMainPage.clickByLink(link);
    }

    @And("Modal form title is {string} on Lab Mapping Main page")
    public void modalFormTitleIs(String title) {
        Assert.assertEquals(labMappingMainPage.getAboutThisDataDescriptionForm().getModalTitle(), title,
                "Title of modal form is not correct on Lab Mapping main page");
    }

    @And("Link {string} is not present on Lab Mapping Main page")
    public void linkIsNotPresentOnLabMapping(String link) {
        Assert.assertFalse(labMappingMainPage.isLinkDisplayed(link),
                String.format("Link %s should be not present on Lab Mapping main page", link));
    }

    @And("I get {string} text and {string} and compare on Lab Mapping Main page")
    public void iCompareSavedTextOnLabMappingPage(String key, String anotherKey) {
        String expectedText = scenarioContext.get(key);
        String actualText = scenarioContext.get(anotherKey);
        Assert.assertEquals(actualText, expectedText, "Text is not the same");
    }

    @And("I get text content from modal form and save to {string} on Lab Mapping page")
    public void getTextContentFromModalFormOnLabMappingPage(String key){
        scenarioContext.add(key, labMappingMainPage.getAboutThisDataDescriptionForm().getTextContent());
    }

    @When("I fill following fields on Lab Mapping Main page using data from {string}:")
    public void iFillFollowingFieldsOnLabMappingMainPageUsingDataFromLab(String key, List<String> fields) {
        Volume volume = scenarioContext.get(key);
        String disease = volume.getDisease();
        String biomarker = volume.getBiomarkerVolumes().get(0).getBiomarkerName();
        labMappingMainPage.setFieldValue(LabMappingMainPageFields.getEnumValue(fields.get(0)), disease);
        labMappingMainPage.setFieldValue(LabMappingMainPageFields.getEnumValue(fields.get(1)), biomarker);
    }

    @Then("Message {string} is displayed on Lab Mapping Main page")
    public void messageSomeItemsBelowNeedYourAttentionIsDisplayedOnLabMappingMainPage(String message) {
        Assert.assertTrue(labMappingMainPage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Lab Mapping Main page", message));

    }

    @And("Message {string} is displayed on required fields on Lab Mapping Main page")
    public void messageIsDisplayedOnRequiredFieldsOnLabMappingMainPage(String message) {
        Assert.assertTrue(labMappingMainPage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Lab Mapping Main page", message));
    }

    @When("I fill following fields on Lab Mapping Main page and save {string}:")
    public void iFillFollowingFieldsOnLabMappingMainPageAndSaveSearchFilter(String key, Map<String, String> data) {
        SearchFilter filter = new SearchFilter();
        data.forEach((field, value) -> {
            String selectedValue = labMappingMainPage.setFieldValue(
                    LabMappingMainPageFields.getEnumValue(field), value);

            filter.setReflectionFieldValue(
                    LabMappingMainPageFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, filter);
    }
}
