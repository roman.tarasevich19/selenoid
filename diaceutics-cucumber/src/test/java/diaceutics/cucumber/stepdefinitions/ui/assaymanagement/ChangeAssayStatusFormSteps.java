package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.assaymanagement.ChangeAssayStatusFormFields;
import diaceutics.selenium.models.Assay;
import diaceutics.selenium.pageobject.forms.assaymanagement.ChangeAssayStatusForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class ChangeAssayStatusFormSteps {

    private final ChangeAssayStatusForm changeAssayStatusForm;
    private final ScenarioContext scenarioContext;

    @Inject
    public ChangeAssayStatusFormSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        changeAssayStatusForm = new ChangeAssayStatusForm();
    }

    @Then("Change assay status form is opened")
    public void changeAssayStatusFormIsOpened() {
        Assert.assertTrue(changeAssayStatusForm.isDisplayed(), "Change Assay Status form should be opened");
    }

    @When("I fill following fields on Change assay status form and save as {string}:")
    public void iFillFollowingFieldsOnChangeAssayStatusFormAndSaveAsAssay(
            String key, Map<String, String> data) {
        Assay assay = scenarioContext.get(key);
        data.forEach((field, value) -> {
            String selectedValue = changeAssayStatusForm.setFieldValue(ChangeAssayStatusFormFields.getEnumValue(field), value);
            assay.setReflectionFieldValue(ChangeAssayStatusFormFields.getEnumValue(field).getModelField(), selectedValue);
        });
    }

    @Then("Change assay status form is closed")
    public void changeAssayStatusFormIsClosedOn() {
        Assert.assertFalse(changeAssayStatusForm.isDisplayed(),
                "Change assay status form should be closed");
    }

    @When("I close Change assay status form")
    public void iCloseChangeAssayStatusForm() {
        changeAssayStatusForm.clickCloseButton();
    }

    @And("I click {string} on Change assay status form")
    public void iClickChangeStatusOnChangeAssayStatusForm(String buttonName) {
        changeAssayStatusForm.clickByButton(buttonName);
    }

    @Then("Message {string} is displayed for required fields on Change assay status form")
    public void messageMonthIsRequiredIsDisplayedForRequiredFieldsOnChangeAssayStatusForm(String message) {
        Assert.assertTrue(changeAssayStatusForm.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed for required fields on Change assay status form", message));
    }

    @And("Red border is displayed for following fields on Change assay status form:")
    public void redBorderIsDisplayedForFollowingFieldsOnChangeAssayStatusForm(List<String> fields) {
        fields.forEach(field -> SoftAssert.getInstance().assertTrue(
                changeAssayStatusForm.isRedBorderDisplayedForField(ChangeAssayStatusFormFields.getEnumValue(field)),
                String.format("Red border should be displayed for %s field on Change assay status form", field)));
    }
}
