package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.assaymanagement.EditAssayPageFields;
import diaceutics.selenium.enums.pagefields.assaymanagement.LabTestBiomarkerFormFields;
import diaceutics.selenium.models.Assay;
import diaceutics.selenium.models.Biomarker;
import diaceutics.selenium.pageobject.pages.assaymanagement.EditAssayPage;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class EditAssayPageSteps {

    private final EditAssayPage editAssayPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public EditAssayPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        editAssayPage = new EditAssayPage();
    }

    @Given("Edit Assay page is opened")
    public void editAssayPageIsOpened() {
        Assert.assertTrue(editAssayPage.isDisplayed(), "Edit Assay page should be opened");
    }

    @When("I fill following fields on Edit Assay page and save as {string}:")
    public void fillLabAddressFormOnAddLocationPage(String key, Map<String, String> data) {
        Assay assay = scenarioContext.get(key);
        data.forEach((field, value) -> {
            value = field.equals(EditAssayPageFields.ASSAY_NAME.getFriendlyName()) ? value + TimeUtil.getTimestamp() : value;
            String selectedValue = editAssayPage.setFieldValue(EditAssayPageFields.getEnumValue(field), value);
            assay.setReflectionFieldValue(EditAssayPageFields.getEnumValue(field).getModelField(), selectedValue);
        });

        assay.setClassifications(assay.addClassifications());
    }

    @And("I click {string} on Edit Assay page")
    public void iClickOnButtonAssayPage(String buttonName) {
        editAssayPage.clickByButton(buttonName);
    }

    @Then("Message {string} is displayed on required fields on Edit Assay page")
    public void messagePleaseEnterAnAssayNameIsDisplayedOnRequiredFieldsOnEditAssayPage(String message) {
        Assert.assertTrue(editAssayPage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Edit Assay page", message));
    }

    @When("I click Delete on Edit Assay page")
    public void iClickDeleteOnEditAssayPage() {
        editAssayPage.clickDelete();
    }

    @Then("Delete assay form on Edit Assay page is opened")
    public void deleteAssayFormOnEditAssayPageIsOpened() {
        Assert.assertTrue(editAssayPage.getDeleteAssayForm().isDisplayed(),
                "Delete assay form on Edit Assay should be opened");
    }

    @When("I click {string} on Delete assay form on Edit Assay page")
    public void iClickDeleteAssayOnDeleteAssayFormOnEditAssayPage(String buttonName) {
        editAssayPage.getDeleteAssayForm().clickByButton(buttonName);
    }

    @And("Delete button is displayed on Edit Assay page")
    public void deleteButtonIsDisplayedOnEditAssayPage() {
        Assert.assertTrue(editAssayPage.isDisplayedDeleteButton(),
                "Delete button should be displayed on Edit Assay page");
    }

    @When("I clear following fields on Edit Assay page")
    public void iClearFollowingFieldsOnEditAssayPage(List<String> fields) {
        fields.forEach(field -> editAssayPage.clearField(EditAssayPageFields.getEnumValue(field)));
    }

    @When("I clear following text fields on Edit Assay page")
    public void iClearFollowingTextFieldsOnEditAssayPage(List<String> fields) {
        fields.forEach(field -> editAssayPage.clearTextField(EditAssayPageFields.getEnumValue(field)));
    }

    @When("I fill following fields in Lab Test Biomarker form on Edit Assay page and save as {string}:")
    public void iFillFollowingFieldsInLabTestBiomarkerFormOnEditAssayPageAndSaveAsBiomarker(
            String key, Map<String, String> data) {
        Biomarker biomarker = new Biomarker();
        data.forEach((field, value) -> {
            String selectedValue = editAssayPage.getLabTestBiomarkerForm().setFieldValue(LabTestBiomarkerFormFields.getEnumValue(field), value);
            biomarker.setReflectionFieldValue(LabTestBiomarkerFormFields.getEnumValue(field).getModelField(), selectedValue);
            scenarioContext.add(key, biomarker);
        });
    }

    @When("I clear {string} field on Lab Test Biomarker form on Edit Assay page")
    public void iClearFieldOnLabTestBiomarkerFormOnEditAssayPage(String field) {
        editAssayPage.clearField(LabTestBiomarkerFormFields.getEnumValue(field));
    }

    @Then("Lab Test Biomarker form is opened on Edit Assay page")
    public void labTestBiomarkerFormIsOpenedOnEditAssayPage() {
        Assert.assertTrue(editAssayPage.getLabTestBiomarkerForm().isDisplayed(),
                "Lab Test Biomarker form should be opened on Edit Assay page");
    }

    @Then("Message {string} is displayed on Edit Assay page")
    public void messageIsDisplayedOnEditAssayPage(String message) {
        Assert.assertTrue(editAssayPage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Edit Assay page", message));
    }

    @When("I click remove Biomarker {string} on Lab Test Biomarker form on Edit Assay page")
    public void iClickRemoveBiomarkerOnLabTestBiomarkerFormOnEditAssayPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        editAssayPage.getLabTestBiomarkerForm().clickRemoveBiomarker(biomarker.getBiomarkerName());
    }

    @Then("Field {string} should be disabled on Edit Assay page")
    public void fieldShouldBeDisabledOnEditAssayPage(String field) {
        Assert.assertFalse(editAssayPage.isFieldEnabled(EditAssayPageFields.getEnumValue(field)),
                String.format("Field %s should be disabled on Edit Assay page", field));
    }

    @Then("Biomarker {string} is not displayed on Lab Test Biomarker form on Edit Assay page")
    public void biomarkerIsNotDisplayedOnLabTestBiomarkerFormOnEditAssayPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        Assert.assertFalse(editAssayPage.getLabTestBiomarkerForm().isBiomarkerDisplayed(biomarker.getBiomarkerName()),
                String.format("Biomarker %s shouldn't displayed on Lab Test Biomarker form on Edit Assay page",
                        biomarker.getBiomarkerName()));
    }

    @Then("Biomarker {string} is displayed on Lab Test Biomarker form on Edit Assay page")
    public void biomarkerIsDisplayedOnLabTestBiomarkerFormOnEditAssayPage(String biomarker) {
        Assert.assertTrue(editAssayPage.getLabTestBiomarkerForm().isBiomarkerDisplayed(biomarker),
                String.format("Biomarker %s shouldn't displayed on Lab Test Biomarker form on Edit Assay page",
                        biomarker));
    }

    @When("I remove following variants for {string} in Lab Test Biomarker form on Edit Assay page:")
    public void iRemoveFollowingVariantsForBiomarkerInLabTestBiomarkerFormOnEditAssayPage(String key, List<String> variants) {
        Biomarker biomarker = scenarioContext.get(key);
        variants.forEach(variant -> {
            editAssayPage.getLabTestBiomarkerForm().removeVariantForBiomarker(variant, biomarker.getBiomarkerName());
            biomarker.removeVariant(variant);
        });
    }

    @Then("Variants from {string} are displayed on Lab Test Biomarker form on Edit Assay page")
    public void variantsFromBiomarkerAreDisplayedOnLabTestBiomarkerFormOnEditAssayPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        List<String> expectedVariants = biomarker.getListOfVariants();
        List<String> actualVariants = editAssayPage.getLabTestBiomarkerForm().getListVariantsForBiomarker(biomarker.getBiomarkerName());
        Assert.assertTrue(actualVariants.containsAll(expectedVariants),
                String.format("Variants from Biomarker %s should be displayed on Lab Test Biomarker form on Edit Assay page",
                        biomarker.getBiomarkerName()));
    }

    @When("I clear field Variants for {string} in Lab Test Biomarker form on Edit Assay page")
    public void iClearFieldVariantsForBiomarkerInLabTestBiomarkerFormOnEditAssayPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        editAssayPage.getLabTestBiomarkerForm().removeAllVariantsForBiomarker(biomarker.getBiomarkerName());
        biomarker.removeAllVariants();
    }

    @When("I remove all Variants for {string} in Lab Test Biomarker form on Edit Assay page")
    public void iRemoveAllVariantsForBiomarkerInLabTestBiomarkerFormOnEditAssayPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        editAssayPage.getLabTestBiomarkerForm().removeAllVariantsForBiomarker(biomarker.getBiomarkerName());
        biomarker.removeAllVariants();
    }

    @When("I set status {string} for Assay {string} on Edit Assay page")
    public void iSetStatusForAssayOnEditAssayPage(String status, String key) {
        Assay assay = scenarioContext.get(key);
        editAssayPage.setFieldValue(EditAssayPageFields.STATUS, status);
        assay.setReflectionFieldValue(EditAssayPageFields.STATUS.getModelField(), status);
    }

    @Then("Reactivate this assay form is opened on Edit Assay page")
    public void reactivateThisAssayFormIsOpenedOnEditAssayPage() {
        Assert.assertTrue(editAssayPage.getReactivateThisAssayForm().isDisplayed(),
                "Reactivate this assay form should be opened on Edit Assay page");
    }

    @When("I click {string} on Reactivate this assay form on Edit Assay page")
    public void iClickYesSetToActiveOnReactivateThisAssayFormOnEditAssayPage(String buttonName) {
        editAssayPage.getReactivateThisAssayForm().clickByButton(buttonName);
    }

    @When("I click View status history link on Edit Assay page")
    public void iClickViewStatusHistoryLinkOnEditAssayPage() {
        editAssayPage.clickViewStatusHistoryLink();
    }

    @Then("Reactivate this assay form is closed on Edit Assay page")
    public void reactivateThisAssayFormIsClosedOnEditAssayPage() {
        Assert.assertFalse(editAssayPage.getReactivateThisAssayForm().isDisplayed(),
                "Reactivate this assay form should be closed on Edit Assay page");
    }

    @And("Status {string} is displayed on Edit Assay page")
    public void statusIsDisplayedOnEditAssayPage(String expectedStatus) {
        String actualStatus = editAssayPage.getStatus();
        Assert.assertEquals(actualStatus, expectedStatus,
                String.format("Status %s should be displayed onEdit Assay page", expectedStatus));
    }

    @And("Red border is displayed for following fields on Edit Assay page:")
    public void redBorderIsDisplayedForFollowingFieldsOnEditAssayPage(List<String> fields) {
        fields.forEach(field -> SoftAssert.getInstance().assertTrue(
                editAssayPage.isRedBorderDisplayedForField(EditAssayPageFields.getEnumValue(field)),
                String.format("Red border should be displayed for %s field oon Edit Assay page", field)));
    }
}
