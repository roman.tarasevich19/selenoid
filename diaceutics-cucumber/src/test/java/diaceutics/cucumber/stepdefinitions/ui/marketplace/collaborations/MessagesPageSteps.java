package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.MessagesPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class MessagesPageSteps {

    private final MessagesPage messagesPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public MessagesPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        messagesPage = new MessagesPage();
    }

    @Then("Messages page is opened")
    public void messagesPageIsOpened() {
        Assert.assertTrue(messagesPage.isDisplayed(), "Messages page should be opened");
    }

    @And("Message {string} from {string} user is displayed on Messages page")
    public void messageFromUserIsDisplayedOnMessagesPage(String messageKey, String userKey) {
        String message = scenarioContext.get(messageKey);
        User user = scenarioContext.get(userKey);
        Assert.assertTrue(messagesPage.isMessageDisplayedFromUser(message, user),
                String.format("Message %s should be displayed on Messages page", message));
    }

    @And("Message {string} from {string} user was sent {string}")
    public void messageMessageFromADMINUserWasSent(String messageKey, String userKey, String expectedDate) {
        String message = scenarioContext.get(messageKey);
        User user = scenarioContext.get(userKey);
        String actualDate = messagesPage.getDateForMessageFromUser(message, user);
        Assert.assertEquals(actualDate, expectedDate,
                String.format("Message %s should be send %s", message, expectedDate));
    }

    @When("I click message button for {string} from {string} user on Messages page")
    public void iClickMessageButtonForMessageFromUserOnMessagesPage(String messageKey, String userKey) {
        String message = scenarioContext.get(messageKey);
        User user = scenarioContext.get(userKey);
        messagesPage.clickMessageButton(message, user);
    }
}
