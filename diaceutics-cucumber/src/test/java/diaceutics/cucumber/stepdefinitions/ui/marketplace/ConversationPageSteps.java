package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.marketplace.ConversationPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import javax.inject.Inject;

public class ConversationPageSteps {

    private final ConversationPage conversationPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public ConversationPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        conversationPage = new ConversationPage();
    }

    @Then("Conversation page is opened")
    public void conversationPageIsOpened() {
        Assert.assertTrue(conversationPage.isDisplayed(), "Conversation page should be opened");
    }

    @And("File {string} is attached to {string} on Conversation page")
    public void fileIsAttachedToMessageOnConversationPage(String expectedFileName, String messageKey) {
        String message = scenarioContext.get(messageKey);
        String actualFileName = conversationPage.getAttachmentFileNameForMessage(message);
        Assert.assertEquals(actualFileName, expectedFileName,
                String.format("File %s should be attached to %s on Conversation page", expectedFileName, message));
    }

    @And("Message {string} was sent from a {string} user on Conversation page")
    public void messageWasSentFromAUserOnConversationPage(String messageKey, String userKey) {
        String message = scenarioContext.get(messageKey);
        User user = scenarioContext.get(userKey);
        Assert.assertTrue(conversationPage.isMessageDisplayedFromUser(message, user),
                String.format("Message %s from user %s should be displayed on Messages page", message, user.getFirstName()));
    }
}
