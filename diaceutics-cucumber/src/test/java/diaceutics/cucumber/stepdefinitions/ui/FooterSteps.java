package diaceutics.cucumber.stepdefinitions.ui;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.pageobject.forms.FooterForm;
import diaceutics.selenium.pageobject.pages.marketplace.ContactUsPage;
import diaceutics.selenium.pageobject.pages.marketplace.CookiesPolicyPage;
import diaceutics.selenium.pageobject.pages.marketplace.PrivacyStatementPage;
import diaceutics.selenium.pageobject.pages.marketplace.TermsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class FooterSteps {

    private final FooterForm footerForm;
    private final ScenarioContext scenarioContext;
    private final TermsPage termsPage;
    private final PrivacyStatementPage privacyStatementPage;
    private final CookiesPolicyPage cookiesPolicyPage;
    private final ContactUsPage contactUsPage;

    @Inject
    public FooterSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        footerForm = new FooterForm();
        termsPage = new TermsPage();
        privacyStatementPage = new PrivacyStatementPage();
        cookiesPolicyPage = new CookiesPolicyPage();
        contactUsPage = new ContactUsPage();
    }

    @Then("Terms page is opened")
    public void isTermsPageDisplayed(){
        Assert.assertTrue(termsPage.isDisplayed(), "Terms page should be opened");
    }

    @Then("Privacy Statement page is opened")
    public void isPrivacyStatementPageDisplayed(){
        Assert.assertTrue(privacyStatementPage.isDisplayed(), "Privacy Statement page should be opened");
    }

    @Then("Cookies Policy page is opened")
    public void isCookiesPolicyPageDisplayed(){
        Assert.assertTrue(cookiesPolicyPage.isDisplayed(), "Cookies Policy page should be opened");
    }

    @Then("Contact Us page is opened")
    public void isContactUsPageDisplayed(){
        Assert.assertTrue(contactUsPage.isDisplayed(), "Contact Us page should be opened");
    }

    @Then("Footer logo is displayed")
    public void footerLogoIsDisplayed(){
        Assert.assertFalse(footerForm.isLogoDisplayed(), "Logo should displayed");
    }

    @Then("Copyright label is correct")
    public void copyRightLabelIsCorrect(){
        Assert.assertTrue(footerForm.isCopyRightDisplayed(), "Copyright label is not displayed");
    }

    @And("Url {string} is correct")
    public void isUrlCorrect(String url){
        Assert.assertEquals(url, footerForm.getBrowserUrl(), "Url is not correct");
    }

    @When("I click on {string} link")
    public void typeOnLink(String link){
        footerForm.clickOnLink(link);
    }
}
