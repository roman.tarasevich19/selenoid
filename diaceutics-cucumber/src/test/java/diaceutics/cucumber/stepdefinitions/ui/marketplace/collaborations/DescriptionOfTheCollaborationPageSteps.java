package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.marketplace.DescriptionOfTheCollaborationPageFields;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.DescriptionOfTheCollaborationPage;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Map;

public class DescriptionOfTheCollaborationPageSteps {

    private final DescriptionOfTheCollaborationPage descriptionOfTheCollaborationPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public DescriptionOfTheCollaborationPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        descriptionOfTheCollaborationPage = new DescriptionOfTheCollaborationPage();
    }

    @Then("Description collaboration page is opened")
    public void descriptionCollaborationPageIsOpened() {
        Assert.assertTrue(descriptionOfTheCollaborationPage.isDisplayed(), "Description collaboration page should be opened");
    }

    @When("I fill following fields on Description of the collaboration page and save as {string}:")
    public void iFillFollowingFieldsOnDescriptionCollaborationPageAndSaveAsCollaboration(String key, Map<String, String> data) {
        Collaboration collaboration = new Collaboration();
        data.forEach((field, value) -> {
            value = field.equals(DescriptionOfTheCollaborationPageFields.TITLE.getFriendlyName()) ? value + TimeUtil.getTimestamp() : value;
            String selectedValue = descriptionOfTheCollaborationPage.
                    setFieldValue(DescriptionOfTheCollaborationPageFields.getEnumValue(field), value);

            collaboration.setReflectionFieldValue(
                    DescriptionOfTheCollaborationPageFields.getEnumValue(field).getModelField(), selectedValue);
        });
        scenarioContext.add(key, collaboration);
    }

    @When("I click {string} on Description collaboration page")
    public void iClickButtonOnDescriptionCollaborationPage(String buttonName) {
        descriptionOfTheCollaborationPage.clickByButton(buttonName);
    }

    @And("I upload file {string} on Description collaboration page")
    public void iUploadFileOnDescriptionCollaborationPage(String fileName) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        descriptionOfTheCollaborationPage.uploadFile(filePath);
    }

    @Then("File {string} is uploaded on Description collaboration page")
    public void fileIsUploadedOnDescriptionCollaborationPage(String fileName) {
        Assert.assertTrue(descriptionOfTheCollaborationPage.isFileUploaded(fileName),
                String.format("File %s should be added on Description collaboration page", fileName));
    }

    @And("I click by Videos on Description collaboration page")
    public void iClickByVideosOnDescriptionCollaborationPage() {
        descriptionOfTheCollaborationPage.clickByVideos();
    }

    @Then("Error container is displayed for {string} field on Description collaboration page")
    public void errorContainerIsDisplayedForFieldOnDescriptionCollaborationPage(String field) {
        SoftAssert.getInstance().assertTrue(descriptionOfTheCollaborationPage.isRedBorderDisplayedForField(
                DescriptionOfTheCollaborationPageFields.getEnumValue(field)),
                String.format("Error container for field - %s should be displayed", field));
    }

    @And("I click Add video on Description collaboration page")
    public void iClickAddVideoOnDescriptionCollaborationPage() {
        descriptionOfTheCollaborationPage.clickAddVideo();
    }

    @And("Form with error message {string} is displayed on Description collaboration page")
    public void formWithErrorMessagesDisplayedOnDescriptionCollaborationPage(String message) {
        Assert.assertTrue(descriptionOfTheCollaborationPage.getInformForm().isMessageDisplayed(message),
                String.format("Message %s should be displayed on Description collaboration page", message));
    }
}
