package diaceutics.cucumber.stepdefinitions.ui.newRegistration;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.newregistration.ResetPasswordPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import javax.inject.Inject;

public class ResetPasswordPageSteps {

    private final ResetPasswordPage resetPasswordPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public ResetPasswordPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        resetPasswordPage = new ResetPasswordPage();

    }

    @Given("Reset password page is opened")
    public void registerPageIsOpened() {
        Assert.assertTrue(resetPasswordPage.isDisplayed(), "Reset password page should be opened");
    }

    @When("I click on Request reset on Reset password page")
    public void iClickOnRequestResetOnResetPasswordPage() {
        resetPasswordPage.requestReset();
    }

    @When("I click on Back to Login on Reset password page")
    public void iClickOnBackToLoginOnResetPasswordPage() {
        resetPasswordPage.backToLogin();
    }

    @When("I fill new value {string} for account email Reset password page")
    public void iFillNewValueForAccountEmailOnResetPasswordPage(String value) {
        resetPasswordPage.setAccountEmailValue(value);
    }

    @When("I fill new value from user {string} for account email Reset password page")
    public void iFillNewValueFromUserForAccountEmailOnResetPasswordPage(String userName) {
        User user = scenarioContext.get(userName);
        resetPasswordPage.setAccountEmailValue(user.getEmail());
    }

    @Then("Message {string} for {string} field is displayed on Reset password page")
    public void messageThisFieldIsRequiredForEmailFieldIsDisplayedOnResetPasswordPage(String message, String field) {
        Assert.assertTrue(resetPasswordPage.isMessageForFieldDisplayed(message, field),
                String.format("Message %s for %s field should be displayed on Reset password page", message, field));
    }
}
