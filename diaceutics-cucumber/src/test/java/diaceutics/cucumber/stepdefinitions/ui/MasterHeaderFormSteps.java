package diaceutics.cucumber.stepdefinitions.ui;

import diaceutics.selenium.pageobject.forms.MasterHeaderForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class MasterHeaderFormSteps {

    private final MasterHeaderForm masterHeaderForm;

    public MasterHeaderFormSteps() {
        masterHeaderForm = new MasterHeaderForm();
    }

    @When("I click {string} on Master Header form")
    public void iClickHomeOnMasterHeader(String linkName) {
        masterHeaderForm.clickByLink(linkName);
    }

    @When("I click {string} from dropdown menu on Master Header form")
    public void iClickHomeOnMasterHeaderFromDropdown(String linkName) {
        masterHeaderForm.clickByLinkFromDropdown(linkName);
    }

    @Then("Data tools context menu is displayed on Master Header form")
    public void dataToolsContextMenuIsDisplayedOnMasterHeaderOnPhysicianMappingMainPage() {
        Assert.assertTrue(masterHeaderForm.isContextMenuDisplayed(),
                "Data tools context menu should be displayed on Master Header");
    }

    @And("{string} link on Master Header form is highlighted in {string} color")
    public void linkIsHighlightedInColor(String linkName, String color) {
        Assert.assertTrue(masterHeaderForm.isLinkHighlightedInColor(linkName, color),
                String.format("%s link on Master Header form should be highlighted in %s color", linkName, color));
    }

    @When("I click {string} on user menu on Master Header form")
    public void iClickLogoutOnUserMenuOnMasterHeaderForm(String menu) {
        masterHeaderForm.openUserMenu(menu);
    }

    @When("I click DXRX logo on the top left on Master Header form")
    public void iClickDXRXLogoOnTheTopLeftOnMasterHeaderForm() {
        masterHeaderForm.clickDXRXLogo();
    }

    @Then("User should be logged in")
    public void userTestUserShouldBeLoggedIn() {
        Assert.assertTrue(masterHeaderForm.isUserLogin(), "User should be logged in");
    }

    @Then("I click {string} on user menu on Master header")
    public void iClickLogoutOnUserMenuOnMasterHeader(String menu) {
        masterHeaderForm.openUserMenu(menu);
    }

    @When("I click {string} on Master header")
    public void iClickLoginOnMasterHeader(String linkName) {
        masterHeaderForm.clickByLinkOnTheRightTopCorner(linkName);
    }

    @When("I open Search form on on Master Header form")
    public void iOpenSearchFormOnMarketplaceMainPage() {
        masterHeaderForm.openSearchForm();
    }
}
