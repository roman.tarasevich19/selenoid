package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.pagefields.marketplace.YourResponsePageFields;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.YourResponsePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import javax.inject.Inject;

public class YourResponsePageSteps {

    private final YourResponsePage yourResponsePage;
    private final ScenarioContext scenarioContext;

    @Inject
    public YourResponsePageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        yourResponsePage = new YourResponsePage();
    }

    @Then("Your Response page is opened")
    public void yourResponsePageIsOpened() {
        Assert.assertTrue(yourResponsePage.isDisplayed(), "Your Response page should be opened");
    }

    @When("I put random message save as {string} on Your Response page")
    public void iPutRandomMessageSaveAsMessageOnYourResponsePage(String key) {
        String message = RandomStringUtils.randomAlphabetic(10);
        yourResponsePage.putMessage(message);
        scenarioContext.add(key, message);
    }

    @And("I click Continue button on Your Response page")
    public void iClickContinueButtonOnYourResponsePage() {
        yourResponsePage.clickContinueButton();
    }

    @When("I add a file {string} on Question form on Your Response page")
    public void iAddAFileOnQuestionFormOnYourResponsePage(String fileName) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        yourResponsePage.addFile(filePath);
    }

    @Then("File {string} is added on Your Response page")
    public void fileIsAddedOnYourResponsePage(String fileName) {
        Assert.assertTrue(yourResponsePage.isFileUploaded(fileName),
                String.format("File %s should be added on Your Response page", fileName));
    }

    @Then("Inform Form with message {string} is displayed on Your Response page")
    public void informFormWithMessageIsDisplayedOnYourResponsePage(String message) {
        Assert.assertTrue(yourResponsePage.getInformForm().isMessageDisplayed(message),
                String.format("Message %s should be displayed on Your Response page", message));
    }

    @And("Error container is displayed for {string} field on Your Response page")
    public void errorContainerIsDisplayedForFieldOnYourResponsePage(String field) {
        Assert.assertTrue(yourResponsePage.isRedBorderDisplayedForField(YourResponsePageFields.getEnumValue(field)),
                String.format("Error container for field - %s should be displayed on Your Response page", field));
    }

    @When("I set {string} value for {string} field on Your Response page")
    public void iSetValueForFieldOnYourResponsePage(String value, String field) {
        yourResponsePage.setFieldValue(YourResponsePageFields.getEnumValue(field), value);
    }
}
