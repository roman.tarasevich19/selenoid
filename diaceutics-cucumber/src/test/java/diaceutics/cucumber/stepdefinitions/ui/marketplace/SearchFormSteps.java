package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.marketplace.SearchFormFields;
import diaceutics.selenium.models.MarketplaceSearchFilter;
import diaceutics.selenium.pageobject.forms.marketplace.SearchForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class SearchFormSteps {

    private final SearchForm searchForm;
    private final ScenarioContext scenarioContext;

    @Inject
    public SearchFormSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        searchForm = new SearchForm();
    }

    @Then("Search form is opened")
    public void searchFormIsOpened() {
        Assert.assertTrue(searchForm.isDisplayed(), "Search form should be opened");
    }

    @When("I set following fields on Search form and save as {string}:")
    public void iSetFollowingFieldsOnSearchFormAndSaveAsFilter(String key, Map<String, String> data) {
        MarketplaceSearchFilter filter = new MarketplaceSearchFilter();
        data.forEach((field, value) -> {
            String selectedValue = searchForm.setFieldValue(SearchFormFields.getEnumValue(field), value);
            filter.setReflectionFieldValue(SearchFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, filter);
    }

    @And("I click search button on Search form")
    public void iClickSearchButtonOnSearchForm() {
        searchForm.clickSearchButton();
    }

    @And("Search filter {string} for following fields is displayed on Search form:")
    public void searchFilterFilterForFollowingFieldsIsDisplayedOnSearchForm(String key, List<String> fields) {
        MarketplaceSearchFilter filter = scenarioContext.get(key);
        fields.forEach(field -> {
            String actualValue = searchForm.getFieldValue(SearchFormFields.getEnumValue(field));
            String expectedValue = filter.getReflectionFieldValue(SearchFormFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value - %s for field - %s is not correct on Search form",
                            actualValue, field));
        });
    }

    @And("Search filter has default values for following fields on Search form:")
    public void searchFilterHasDefaultValuesForFollowingFieldsOnSearchForm(List<String> fields) {
        MarketplaceSearchFilter filter = new MarketplaceSearchFilter();
        fields.forEach(field -> {
            String actualValue = searchForm.getFieldValue(SearchFormFields.getEnumValue(field));
            String expectedValue = filter.getReflectionFieldValue(SearchFormFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value - %s for field - %s is not correct on Search form",
                            actualValue, field));
        });
    }

    @When("I clear {string} field on Search form")
    public void iClearLocationFieldOnSearchForm(String field) {
        searchForm.clearField(SearchFormFields.getEnumValue(field));
    }
}
