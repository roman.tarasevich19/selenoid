package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.MyCollaborationsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class MyCollaborationsPageSteps {

    private final MyCollaborationsPage myCollaborationsPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public MyCollaborationsPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        myCollaborationsPage = new MyCollaborationsPage();
    }

    @Then("My Collaborations page is opened")
    public void myCollaborationsPageIsOpened() {
        Assert.assertTrue(myCollaborationsPage.isDisplayed(), "My Collaborations should be opened");
    }

    @Then("I click {string} Collaboration {string} on My Collaborations page and save copy as a {string}")
    public void iClickCollaborationCollaborationOnMyCollaborationsPageAndSaveCopy(
            String buttonName, String collaborationKey, String copyCollaborationKey) {
        Collaboration collaboration = scenarioContext.get(collaborationKey);
        myCollaborationsPage.clickByButtonForCollaboration(buttonName, collaboration.getTitle());
        scenarioContext.add(copyCollaborationKey, new Collaboration(collaboration));
    }

    @Then("I click {string} Collaboration {string} on My Collaborations page")
    public void iClickCollaborationCollaborationOnMyCollaborationsPage(String buttonName, String key) {
        Collaboration collaboration = scenarioContext.get(key);
        myCollaborationsPage.clickByButtonForCollaboration(buttonName, collaboration.getTitle());
    }

    @And("Number of {int} collaborations as a {string} are displayed on My Collaborations page")
    public void numberOfCollaborationsAsACollaborationAreDisplayedOnMyCollaborationsPage(
            int expectedNumberOfCollaborations, String key) {
        Collaboration collaboration = scenarioContext.get(key);
        int actualNumberOfCollaborations = myCollaborationsPage.getNumberOfCollaborationsWithTitle(collaboration.getTitle());
        Assert.assertEquals(actualNumberOfCollaborations, expectedNumberOfCollaborations,
                String.format("Number of %s collaborations with title %s should be displayed on My Collaborations page",
                        expectedNumberOfCollaborations, collaboration.getTitle()));
    }

    @When("I set {string} status and click Apply button on My Collaborations page")
    public void iSetStatusAndClickApplyButtonOnMyCollaborationsPage(String status) {
        myCollaborationsPage.setStatus(status);
        myCollaborationsPage.clickApplyButton();
    }

    @And("Collaboration {string} is displayed on My Collaborations page")
    public void collaborationIsDisplayedOnMyCollaborationsPage(String key) {
        Collaboration collaboration = scenarioContext.get(key);
        Assert.assertTrue(myCollaborationsPage.isCollaborationDisplayed(collaboration.getTitle()),
                String.format("Collaboration with title %s should be displayed on My Collaborations page", collaboration.getTitle()));
    }

    @When("I select random collaboration from My Collaborations page and save as {string}")
    public void iSelectRandomCollaborationAndSaveFromMyCollaborationsPage(String key) {
        String collaborationTitle = myCollaborationsPage.getRandomCollaborationTitle();
        Collaboration collaboration = new Collaboration();
        collaboration.setTitle(collaborationTitle);
        scenarioContext.add(key, collaboration);
    }
}
