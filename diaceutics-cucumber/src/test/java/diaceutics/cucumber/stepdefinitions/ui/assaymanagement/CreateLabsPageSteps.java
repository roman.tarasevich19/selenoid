package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.pagefields.assaymanagement.CreateLabPageFields;
import diaceutics.selenium.pageobject.pages.assaymanagement.CreateLabPage;
import diaceutics.selenium.models.Lab;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Map;

public class CreateLabsPageSteps {

    private final CreateLabPage createLabPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public CreateLabsPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        createLabPage = new CreateLabPage();
    }

    @Then("Create a Lab page is opened")
    public void createLabsPageIsOpened() {
        Assert.assertTrue(createLabPage.isDisplayed(), "Create a Lab page should be opened");
    }

    @When("I fill following fields on Create a Lab page and save as {string}:")
    public void fillCreateLabPage(String key, Map<String, String> data) {
        Lab lab = new Lab();
        data.forEach((field, value) -> {
            value = field.equals(CreateLabPageFields.NAME.getFriendlyName()) ? value + TimeUtil.getTimestamp() : value;
            String selectedValue = createLabPage.setFieldValue(CreateLabPageFields.getEnumValue(field), value);
            lab.setReflectionFieldValue(CreateLabPageFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, lab);
    }

    @And("I click {string} on Create a Lab page")
    public void iClickNextOnCreateALabPage(String buttonName) {
        createLabPage.clickByButton(buttonName);
    }

    @Then("Message {string} is displayed on Create a Lab page")
    public void someItemsBelowNeedYourAttentionMessageAppearsOnCreateALabPage(String message) {
        Assert.assertTrue(createLabPage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Create a Lab page", message));
    }

    @And("Message {string} is displayed on required fields on Create a Lab page")
    public void messagePleaseInputACountryDisplayedOnRequiredFieldsOnCreateALabPage(String message) {
        Assert.assertTrue(createLabPage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Create a Lab page", message));
    }
}
