package diaceutics.cucumber.stepdefinitions.ui.testingdashboard;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.pagefields.testingdashboard.TestingDashboardMainPageFields;
import diaceutics.selenium.models.SearchFilter;
import diaceutics.selenium.pageobject.pages.testingdashboard.TestingDashboardMainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Map;

public class TestingDashboardMainPageSteps {

    private final TestingDashboardMainPage testingDashboardMainPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public TestingDashboardMainPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        testingDashboardMainPage = new TestingDashboardMainPage();
    }

    @Then("Testing Dashboard Main page is opened")
    public void testingDashboardMainIsOpened() {
        Assert.assertTrue(testingDashboardMainPage.isDisplayed(), "Testing Dashboard Main page should be opened");
    }

    @When("I fill following fields on Testing Dashboard Main page:")
    public void iFillFollowingFieldsOnTestingDashboardMainPage(Map<String, String> data) {
        data.forEach((field, value) -> testingDashboardMainPage.setFieldValue(TestingDashboardMainPageFields.getEnumValue(field), value));
    }

    @And("I click {string} on Testing Dashboard Main page")
    public void iClickStartOnTestingDashboardMainPage(String buttonName) {
        testingDashboardMainPage.clickByButton(buttonName);
    }

    @Then("Message {string} is displayed on Testing Dashboard Main page")
    public void messageSomeItemsBelowNeedYourAttentionIsDisplayedOnTestingDashboardMainPage(String message) {
        Assert.assertTrue(testingDashboardMainPage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Testing Dashboard Main page", message));

    }

    @And("Message {string} is displayed on required fields on Testing Dashboard Main page")
    public void messageIsDisplayedOnRequiredFieldsOnTestingDashboardMainPage(String message) {
        Assert.assertTrue(testingDashboardMainPage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Testing Dashboard Main page", message));
    }

    @When("I fill following fields on Testing Dashboard Main page and save {string}:")
    public void iFillFollowingFieldsOnTestingDashboardMainPageAndSaveSearchFilter(String key, Map<String, String> data) {
        SearchFilter filter = new SearchFilter();
        data.forEach((field, value) -> {
            String selectedValue = testingDashboardMainPage.setFieldValue(
                    TestingDashboardMainPageFields.getEnumValue(field), value);

            filter.setReflectionFieldValue(
                    TestingDashboardMainPageFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, filter);
    }
}
