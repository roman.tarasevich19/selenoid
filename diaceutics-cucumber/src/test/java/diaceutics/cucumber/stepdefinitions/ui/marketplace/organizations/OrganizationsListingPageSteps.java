package diaceutics.cucumber.stepdefinitions.ui.marketplace.organizations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.MarketplaceSearchFilter;
import diaceutics.selenium.models.Organization;
import diaceutics.selenium.pageobject.pages.marketplace.organizations.OrganizationsListingPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;

public class OrganizationsListingPageSteps {

    private final OrganizationsListingPage organizationsListingPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public OrganizationsListingPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        organizationsListingPage = new OrganizationsListingPage();
    }

    @Then("Organizations Listing page is opened")
    public void organizationsPageIsOpened() {
        Assert.assertTrue(organizationsListingPage.isDisplayed(), "Organizations Listing page should be opened");
    }

    @And("Organizations are displayed on Organizations Listing page")
    public void organizationsAreDisplayedOnOrganizationsListingPage() {
        Assert.assertTrue(organizationsListingPage.areOrganizationsDisplayed(),
                "Organizations should be displayed on Organizations Listing page");
    }

    @And("Organizations by location from {string} are displayed on Organizations Listing page")
    public void organizationsByLocationFilterAreDisplayedOnOrganizationsListingPage(String key) {
        MarketplaceSearchFilter filter = scenarioContext.get(key);
        List<Organization> organizations = organizationsListingPage.getListOrganization();
        Assert.assertTrue(organizations.stream().allMatch(organization ->
                        organization.getLocation().contains(filter.getLocation().toUpperCase())),
                String.format("Organizations by location %s are displayed on Organizations Listing page",
                        filter.getLocation()));
    }

    @When("I open random collaboration on Organizations Listing page")
    public void iOpenRandomCollaborationOnOrganizationsListingPage() {
        organizationsListingPage.openRandomOrganization();
    }
}
