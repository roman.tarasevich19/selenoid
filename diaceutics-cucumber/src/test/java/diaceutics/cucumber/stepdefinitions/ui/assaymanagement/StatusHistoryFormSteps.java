package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.transformations.ModelTransformation;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.restassured.project.models.LabTestDto;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.restassured.project.models.LabTestStatusDto;
import diaceutics.selenium.enums.grids.Grids;
import diaceutics.selenium.models.StatusModel;
import diaceutics.selenium.pageobject.forms.assaymanagement.StatusHistoryForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class StatusHistoryFormSteps {

    private final StatusHistoryForm statusHistoryForm;
    private final ScenarioContext scenarioContext;

    @Inject
    public StatusHistoryFormSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        statusHistoryForm = new StatusHistoryForm();
    }

    @Then("Status History form is opened")
    public void statusHistoryFormIsOpened() {
        Assert.assertTrue(statusHistoryForm.isDisplayed(), "Status History form should be opened");
    }

    @And("Name for Assay {string} is displayed on Status History form")
    public void nameForAssayAssayIsDisplayedOnStatusHistoryForm(String key) {
        LabTestDto assay = scenarioContext.get(key);
        String expectedAssayName = assay.getName();
        String actualAssayName = statusHistoryForm.getAssayName();
        Assert.assertEquals(actualAssayName, expectedAssayName,
                String.format("Name - %s for Assay should be displayed on Status History form", expectedAssayName));
    }

    @And("Name for Lab {string} is displayed on Status History form")
    public void nameForLabLabIsDisplayedOnStatusHistoryForm(String key) {
        LabDto lab = scenarioContext.get(key);
        String expectedLabName = lab.getName();
        String actualLabName = statusHistoryForm.getLabName();
        Assert.assertEquals(actualLabName, expectedLabName,
                String.format("Name - %s for Lab should be displayed on Status History form", expectedLabName));
    }

    @Then("I get statuses from Log of assay status grid and compare with statuses for {string} on Status History form")
    public void iGetStatusesFromLogOfAssayStatusGridAndCompareWithStatusesForAssayOnStatusHistoryForm(String key) {
        LabTestDto assay = scenarioContext.get(key);
        List<LabTestStatusDto> labTestStatusDtos = assay.getStatuses();
        List<StatusModel> actualStatuses = statusHistoryForm.getLogAssayStatusChangesGridForm().getStatuses().stream()
                .sorted().collect(Collectors.toList());

        List<StatusModel> expectedStatuses = labTestStatusDtos.stream().map(ModelTransformation::transformToUiStatus)
                .sorted().collect(Collectors.toList());
        Assert.assertEquals(actualStatuses, expectedStatuses,
                "Statuses from Log of assay status grid should be the same as statuses from Api");
    }

    @When("I close Status History form")
    public void iCloseStatusHistoryForm() {
        statusHistoryForm.clickCloseButton();
    }

    @When("I sort data by alphabet in {string} column on Log of assay status changes grid")
    public void iSortDataByAlphabetInColumnOnLogOfAssayStatusChangesGrid(String column) {
        statusHistoryForm.getLogAssayStatusChangesGridForm().clickSortColumnInGrid(column, Grids.LOG_OF_ASSAY_STATUS_CHANGES.getLocator());
    }

    @Then("Data in {string} column on Log of assay status changes grid sorted according to alphabet")
    public void dataInColumnOnLogOfAssayStatusChangesGridSortedAccordingToAlphabet(String column) {
        SoftAssert.getInstance().assertTrue(
                statusHistoryForm.getLogAssayStatusChangesGridForm().isDataInColumnInGridSorted(
                        column, Grids.LOG_OF_ASSAY_STATUS_CHANGES.getLocator()),
                String.format("Data in Grid %s in %s column should be sorted according to alphabet",
                        Grids.LOG_OF_ASSAY_STATUS_CHANGES.getName(), column));
    }

    @Then("Status History form is closed")
    public void statusHistoryFormIsClosed() {
        Assert.assertFalse(statusHistoryForm.isDisplayed(), "Status History form should be closed");
    }
}
