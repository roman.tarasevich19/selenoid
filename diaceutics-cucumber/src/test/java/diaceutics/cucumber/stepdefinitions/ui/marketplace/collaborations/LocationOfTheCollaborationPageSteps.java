package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.pagefields.marketplace.LocationOfTheCollaborationPageFields;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.LocationOfTheCollaborationPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Map;


public class LocationOfTheCollaborationPageSteps {

    private final LocationOfTheCollaborationPage locationOfTheCollaborationPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public LocationOfTheCollaborationPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        locationOfTheCollaborationPage = new LocationOfTheCollaborationPage();
    }

    @Then("Location of the collaboration page is opened")
    public void descriptionCollaborationPageIsOpened() {
        Assert.assertTrue(locationOfTheCollaborationPage.isDisplayed(),
                "Location of the collaboration page should be opened");
    }

    @When("I fill following fields on Location of the collaboration page and save as {string}:")
    public void iFillFollowingFieldsOnLocationOfTheCollaborationPageAndSaveAsCollaboration(String key, Map<String, String> data) {
        Collaboration collaboration = scenarioContext.get(key);
        data.forEach((field, value) -> {
            String selectedValue = locationOfTheCollaborationPage.setFieldValue(LocationOfTheCollaborationPageFields.getEnumValue(field), value);
            collaboration.setReflectionFieldValue(LocationOfTheCollaborationPageFields.getEnumValue(field).getModelField(), selectedValue);
        });
    }

    @And("I click {string} on Location of the collaboration page")
    public void iClickPublishACollaborationOnLocationOfTheCollaborationPage(String buttonName) {
        locationOfTheCollaborationPage.clickByButton(buttonName);
    }
}
