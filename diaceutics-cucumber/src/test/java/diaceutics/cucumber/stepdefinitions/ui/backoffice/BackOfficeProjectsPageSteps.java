package diaceutics.cucumber.stepdefinitions.ui.backoffice;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.backoffice.pageobject.pages.BackOfficeProjectsPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import javax.inject.Inject;

public class BackOfficeProjectsPageSteps {

    private final BackOfficeProjectsPage backOfficeProjectsPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public BackOfficeProjectsPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        backOfficeProjectsPage = new BackOfficeProjectsPage();
    }

    @Then("BackOffice projects page is opened")
    public void backOfficeMainPageIsOpened(){
        Assert.assertTrue(backOfficeProjectsPage.isDisplayed(), "Back office projects page is not opened");
    }

    @When("I click {string} link on BackOffice projects page")
    public void iClickLinkOnBackOfficeMainPage(String link){
        backOfficeProjectsPage.clickByLink(link);
    }
}
