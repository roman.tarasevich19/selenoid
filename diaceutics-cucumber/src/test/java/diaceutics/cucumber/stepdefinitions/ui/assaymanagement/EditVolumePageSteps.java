package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.assaymanagement.*;
import diaceutics.selenium.models.BiomarkerVolume;
import diaceutics.selenium.models.Volume;
import diaceutics.selenium.pageobject.pages.assaymanagement.EditVolumePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class EditVolumePageSteps {

    private final EditVolumePage editVolumePage;
    private final ScenarioContext scenarioContext;

    @Inject
    public EditVolumePageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        editVolumePage = new EditVolumePage();
    }

    @Then("Edit volume page is opened")
    public void EditVolumePageIsOpened() {
        Assert.assertTrue(editVolumePage.isDisplayed(), "Edit volume page should be opened");
    }

    @When("I click Delete volume on Edit volume page")
    public void iClickDeleteVolumeOnEditVolumePage() {
        editVolumePage.clickDeleteVolume();
    }

    @Then("Delete volume form on Edit volume page is opened")
    public void deleteVolumeFormOnEditVolumePageIsOpened() {
        Assert.assertTrue(editVolumePage.getDeleteVolumeForm().isDisplayed(),
                "Delete volume form on Edit volume should be opened");
    }

    @When("I click Delete volume on Delete volume form on Edit volume")
    public void iClickDeleteVolumeOnDeleteVolumeFormOnEditVolume() {
        editVolumePage.getDeleteVolumeForm().clickDeleteVolume();
    }

    @When("I fill following fields on Edit volume page and save as {string}:")
    public void iFillFollowingFieldsOnEditVolumePageAndSaveAsVolume(String key, Map<String, String> data) {
        Volume volume = scenarioContext.get(key);
        data.forEach((field, value) -> {
            String selectedValue = editVolumePage.setFieldValue(EditVolumePageFields.getEnumValue(field), value);
            selectedValue = field.equals(EditVolumePageFields.DISEASE_VOLUME.getFriendlyName())
                    && Integer.parseInt(selectedValue) < 50 ? "<50" : selectedValue;

            volume.setReflectionFieldValue(EditVolumePageFields.getEnumValue(field).getModelField(), selectedValue);
        });
    }

    @And("I fill following fields on Biomarker volume form on Edit volume page and edit biomarker volume for {string}:")
    public void iFillFollowingFieldsOnBiomarkerVolumeFormOnEditVolumePageAndEditBiomarkerVolumeToVolume(
            String key, Map<String, String> data) {
        Volume volume = scenarioContext.get(key);
        BiomarkerVolume biomarkerVolume = volume.getBiomarkerVolumes().get(0);
        data.forEach((field, value) -> {
            String selectedValue = editVolumePage.getBiomarkerVolumeForm()
                    .setFieldValue(BiomarkerVolumeFormFields.getEnumValue(field), value);
            biomarkerVolume.setReflectionFieldValue(BiomarkerVolumeFormFields.getEnumValue(field).getModelField(), selectedValue);
        });
    }

    @And("I click {string} on Edit volume page")
    public void iClickSaveAndFinishOnEditVolumePage(String buttonName) {
        editVolumePage.clickByButton(buttonName);
    }

    @And("Biomarker volumes on Biomarker volume form on Edit volume page should be the same as Biomarker volumes from {string}")
    public void biomarkerVolumesOnBiomarkerVolumeFormOnEditVolumePageShouldBeTheSameAsBiomarkerVolumesFromVolume(String key) {
        Volume volume = scenarioContext.get(key);
        List<BiomarkerVolume> expectedBiomarkerVolumes = volume.getBiomarkerVolumes();
        List<BiomarkerVolume> actualBiomarkerVolumes = editVolumePage.getBiomarkerVolumeForm().getBiomarkerVolumes();
        Collections.sort(expectedBiomarkerVolumes);
        Assert.assertEquals(actualBiomarkerVolumes, expectedBiomarkerVolumes,
                "Biomarker volumes on Biomarker volume form should be the same as Biomarker volumes from volume");
    }

    @When("I fill following fields on Edit volume page using data from {string}:")
    public void iFillFollowingFieldsOnEditVolumePageUsingDataFromVolume(String key, List<String> fields) {
        Volume volume = scenarioContext.get(key);
        fields.forEach(field -> editVolumePage.setFieldValue(EditVolumePageFields.getEnumValue(field),
                volume.getReflectionFieldValue(EditVolumePageFields.getEnumValue(field).getModelField())));
    }

    @Then("Message {string} for {string} is displayed on Edit volume page")
    public void messageForVolumeOneIsDisplayedOnEditVolumePage(String messageTemplate, String key) {
        Volume volume = scenarioContext.get(key);
        String expectedMessage = String.format(messageTemplate, volume.getYear(), volume.getQuarter());
        Assert.assertTrue(editVolumePage.isAlertMessageDisplayed(expectedMessage),
                String.format("Message %s should be displayed on Log test volume form", expectedMessage));
    }

    @When("I click view log on Edit volume page")
    public void iClickViewLogOnEditVolumePage() {
        editVolumePage.clickViewLog();
    }

    @And("Volume {string} with following fields is displayed on Edit volume page:")
    public void volumeVolumeOneWithFollowingFieldsIsDisplayedOnEditVolumePage(String key, List<String> fields) {
        Volume expectedVolume = scenarioContext.get(key);
        fields.forEach(field -> {
            String actualValue = editVolumePage.getFieldValue(EditVolumePageFields.getEnumValue(field));
            String expectedValue = expectedVolume.getReflectionFieldValue(EditVolumePageFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for field - %s is not correct on Edit volume page",
                            actualValue,
                            field));
        });
    }

    @And("Red border is displayed for following fields on Edit volume page:")
    public void redBorderIsDisplayedForFollowingFieldsOnEditVolumePage(List<String> fields) {
        fields.forEach(field -> SoftAssert.getInstance().assertTrue(
                editVolumePage.isRedBorderDisplayedForField(EditVolumePageFields.getEnumValue(field)),
                String.format("Red border should be displayed for %s field on Edit volume page", field)));
    }

    @When("I clear {string} field on Edit volume page")
    public void iClearDiseaseFieldOnEditVolumePage(String field) {
        editVolumePage.clearField(EditVolumePageFields.getEnumValue(field));
    }
}
