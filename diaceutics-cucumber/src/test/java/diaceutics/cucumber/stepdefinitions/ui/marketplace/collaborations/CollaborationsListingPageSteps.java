package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.CollaborationsListingPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class CollaborationsListingPageSteps {

    private final CollaborationsListingPage collaborationsListingPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public CollaborationsListingPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        collaborationsListingPage = new CollaborationsListingPage();
    }

    @Then("Collaborations Listing page is opened")
    public void collaborationsPageIsOpened() {
        Assert.assertTrue(collaborationsListingPage.isDisplayed(), "Collaborations Listing page should be opened");
    }

    @When("I open Collaboration {string} on Collaborations Listing page")
    public void iOpenCollaborationTestCollaborationOnCollaborationsListingPage(String key) {
        Collaboration collaboration = scenarioContext.get(key);
        collaborationsListingPage.openCollaboration(collaboration.getTitle());
    }

    @And("Collaborations are displayed on Collaborations Listing page")
    public void collaborationsAreDisplayedOnCollaborationsListingPage() {
        Assert.assertTrue(collaborationsListingPage.areCollaborationsDisplayed(),
                "Collaborations should be displayed on Collaborations Listing page");
    }

    @And("Collaboration {string} is displayed on Collaborations Listing page")
    public void collaborationIsDisplayedOnCollaborationsListingPage(String key) {
        Collaboration collaboration = scenarioContext.get(key);
        Assert.assertTrue(collaborationsListingPage.isCollaborationDisplayed(collaboration.getTitle()),
                String.format("Collaboration with title %s should be displayed on Collaborations Listing page",
                        collaboration.getTitle()));
    }

    @When("I open random collaboration on Collaborations Listing page")
    public void iOpenRandomCollaborationOnCollaborationsListingPage() {
        collaborationsListingPage.openRandomCollaboration();
    }
}
