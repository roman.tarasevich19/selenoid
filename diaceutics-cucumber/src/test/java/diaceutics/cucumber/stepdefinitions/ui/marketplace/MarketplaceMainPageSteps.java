package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import diaceutics.selenium.pageobject.pages.marketplace.MarketplaceMainPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;


public class MarketplaceMainPageSteps {

    private final MarketplaceMainPage marketplaceMainPage;

    public MarketplaceMainPageSteps() {
        marketplaceMainPage = new MarketplaceMainPage();
    }

    @Given("Marketplace Main page is opened")
    public void marketplaceMainPageIsOpened() {
        Assert.assertTrue(marketplaceMainPage.isDisplayed(), "Marketplace Main page should be opened");
    }

    @When("I click {string} on Marketplace Main page")
    public void iClickLoginOnMarketplaceMainPage(String buttonName) {
        marketplaceMainPage.clickByLinkOnTheMiddleOfThePage(buttonName);
    }

    @Then("Close form")
    public void closeForm() {
        marketplaceMainPage.getCookieProForm().closeForm();
    }

    @Then("Cookie Pro form on Marketplace Main page is opened")
    public void cookieProFormOnMarketplaceMainPageIsOpened() {
        Assert.assertTrue(marketplaceMainPage.getCookieProForm().isDisplayed(),
                "Cookie Pro form on Marketplace Main page should be opened");
    }
}
