package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.assaymanagement.LabAddressPageFields;
import diaceutics.selenium.enums.pagefields.assaymanagement.LabDetailsFormFields;
import diaceutics.selenium.enums.pagefields.assaymanagement.LocationFormFields;
import diaceutics.selenium.models.Lab;
import diaceutics.selenium.models.Address;
import diaceutics.selenium.pageobject.pages.assaymanagement.EditProfilePage;
import diaceutics.selenium.utilities.TimeUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class EditProfilePageSteps {

    private final EditProfilePage editProfilePage;
    private final ScenarioContext scenarioContext;

    @Inject
    public EditProfilePageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        editProfilePage = new EditProfilePage();
    }

    @Given("Edit Profile page is opened")
    public void editProfileLabDetailsPageIsOpened() {
        Assert.assertTrue(editProfilePage.isDisplayed(), "Edit Profile page should be opened");
    }

    @When("I fill following fields on Lab Details form on Edit Profile page and save as {string}:")
    public void iChangeLabDetailsFormUsingFollowingDataAndSavaAsLab(String key, Map<String, String> data) {
        Lab lab = new Lab();
        data.forEach((field, value) -> {
            value = field.equals(LabDetailsFormFields.NAME.getFriendlyName()) ? value + TimeUtil.getTimestamp() : value;
            String selectedValue = editProfilePage.getLabDetailsForm().setFieldValue(LabDetailsFormFields.getEnumValue(field), value);
            lab.setReflectionFieldValue(LabDetailsFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, lab);
    }

    @And("I click Return to profile on Edit Profile page")
    public void iClickReturnToProfileOnEditProfilePage() {
        editProfilePage.clickReturnToProfile();
    }

    @Then("Message {string} is displayed on Edit Profile page")
    public void labUpdatedMessageIsDisplayedOnEditProfilePage(String message) {
        Assert.assertTrue(editProfilePage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Edit Profile Lab Details page", message));
    }

    @And("Message {string} is displayed on required fields on Edit Profile page")
    public void messagePleaseInputACountryDisplayedOnRequiredFieldsOnEditProfilePage(String message) {
        Assert.assertTrue(editProfilePage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Edit Profile Lab Details page", message));
    }

    @When("I clear {string} field on Edit Profile Lab Details page")
    public void iClearNameFieldOnLabProfilePage(String field) {
        editProfilePage.setFieldValue(LabDetailsFormFields.getEnumValue(field), "1");
        editProfilePage.sendKeys(LabDetailsFormFields.getEnumValue(field), Keys.BACK_SPACE);
    }

    @Then("I click Add new location on Edit Profile page")
    public void iClickAddNewLocationOnEditProfilePage() {
        editProfilePage.clickAddNewLocation();
    }

    @And("I click {string} on Edit Profile page")
    public void iClickLocationOnEditProfilePage(String linkName) {
        editProfilePage.clickLink(linkName);
    }

    @Then("Location form on Edit Profile page is opened")
    public void locationFormOnEditProfilePageIsOpened() {
        Assert.assertTrue(editProfilePage.getLocationForm().isDisplayed(),
                "Location form on Edit Profile page should be opened");
    }

    @When("I fill following fields on Location form on Edit Profile page and save as {string}:")
    public void iFillFollowingFieldsOnLocationFormOnEditProfilePageAndSaveAsLab(String key, Map<String, String> data) {
        Lab lab = new Lab();
        Address address = new Address();
        data.forEach((field, value) -> {
            String selectedValue = editProfilePage.getLocationForm().setFieldValue(LocationFormFields.getEnumValue(field), value);
            address.setReflectionFieldValue(LocationFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        lab.addLocation(address);
        scenarioContext.add(key, lab);
    }

    @And("I click button {string} on Edit Profile page")
    public void iClickButtonSaveOnEditProfilePage(String buttonName) {
        editProfilePage.clickByButton(buttonName);
    }

    @And("Location from {string} with following fields is displayed on Edit Profile page:")
    public void locationFromLabWithFollowingFieldsIsDisplayedOnEditProfilePage(String key, List<String> fields) {
        Lab expectedLab = scenarioContext.get(key);
        Address expectedAddress = expectedLab.getAddresses().get(0);
        fields.forEach(field -> {
            String actualValue = editProfilePage.getFieldValue(LabAddressPageFields.getEnumValue(field));
            String expectedValue = expectedAddress.getReflectionFieldValue(LabAddressPageFields.getEnumValue(field).getModelField());
            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for field - %s is not correct on Edit Profile page", actualValue, field));
        });
    }

    @And("Value {string} for field {string} is selected on Lab Details form on Edit Profile page")
    public void valueOwnershipForOwnershipIsSelectedOnLabDetailsFormOnEditProfilePage(String expectedValue, String field) {
        String actualValue = editProfilePage.getLabDetailsForm().getFieldValue(LabDetailsFormFields.getEnumValue(field));
        SoftAssert.getInstance().assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s for field - %s is not correct on Edit Profile page", actualValue, field));
    }
}
