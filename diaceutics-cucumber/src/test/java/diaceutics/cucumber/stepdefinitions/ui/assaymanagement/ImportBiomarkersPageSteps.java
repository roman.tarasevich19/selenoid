package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.pageobject.pages.assaymanagement.AddAnAssayPage;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;

import javax.inject.Inject;

public class ImportBiomarkersPageSteps {

    private final AddAnAssayPage addAnAssayPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public ImportBiomarkersPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        addAnAssayPage = new AddAnAssayPage();
    }

    @When("I click {string} on Upload biomarkers form")
    public void cancel(String buttonName){
        addAnAssayPage.getUploadBiomarkerForm().clickByButton(buttonName);
    }

    @When("Close Upload biomarkers form")
    public void close(){
        addAnAssayPage.getUploadBiomarkerForm().clickCloseButton();
    }

    @When("I add a file {string} on Import Biomarkers form")
    public void iAddAFileOnImportBiomarkersForm(String fileName) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        addAnAssayPage.getUploadBiomarkerForm().addFile(filePath);
    }

    @When("Message {string} is present on Upload biomarkers form")
    public void isMessageOnUploadBiomarkersFormPresent(String message) {
        Assert.assertTrue(addAnAssayPage.getUploadBiomarkerForm().isAlertMessageDisplayed(message),
                String.format("%s message should be present", message));
    }

    @When("Concat value from {string} and check that message {string} is present on Upload biomarkers form")
    public void isMessageOnUploadBiomarkersFormPresent(String key, String message) {
        String value = scenarioContext.get(key);
        String messageOnUI = value + StringUtils.SPACE + message;
        Assert.assertTrue(addAnAssayPage.getUploadBiomarkerForm().isAlertMessageDisplayed(messageOnUI),
                String.format("%s message should be present", messageOnUI));
    }

    @When("Concat value from {string} and check that message {string} is not present on Upload biomarkers form")
    public void isMessageOnUploadBiomarkersFormNotPresent(String key, String message) {
        String value = scenarioContext.get(key);
        String messageOnUI = value + StringUtils.SPACE + message;
        Assert.assertFalse(addAnAssayPage.getUploadBiomarkerForm().isAlertMessageDisplayed(messageOnUI),
                String.format("%s message should be not present", messageOnUI));
    }
}
