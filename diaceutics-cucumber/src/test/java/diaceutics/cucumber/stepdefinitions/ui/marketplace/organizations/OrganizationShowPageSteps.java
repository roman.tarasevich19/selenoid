package diaceutics.cucumber.stepdefinitions.ui.marketplace.organizations;

import diaceutics.selenium.pageobject.pages.marketplace.organizations.OrganizationShowPage;
import io.cucumber.java.en.Then;
import org.testng.Assert;

public class OrganizationShowPageSteps {

    private final OrganizationShowPage collaborationShowPage;

    public OrganizationShowPageSteps() {
        collaborationShowPage = new OrganizationShowPage();
    }

    @Then("Organization Show page is opened")
    public void organizationShowPageIsOpened() {
        Assert.assertTrue(collaborationShowPage.isDisplayed(),
                "Organization Show page should be opened");
    }
}
