package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.webelementcolors.ElementColor;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.newregistration.ChangeNewPasswordPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import javax.inject.Inject;

public class ChangeNewPasswordPageSteps {

    private final ChangeNewPasswordPage changeNewPasswordPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public ChangeNewPasswordPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        changeNewPasswordPage = new ChangeNewPasswordPage();
    }

    @Then("Change new password page is opened")
    public void changeNewPasswordPageIsOpened() {
        Assert.assertTrue(changeNewPasswordPage.isDisplayed(), "Change new password page should be opened");
    }

    @When("I set password {string} on new password page")
    public void iSetPasswordOnNewPasswordPage(String value) {
        changeNewPasswordPage.setPassword(value);
    }

    @When("I click submit on new password page")
    public void iCLickSubmitOnNewPasswordPage() {
        changeNewPasswordPage.submit();
    }

    @And("I set confirm password {string} on new password page")
    public void iSetConfirmPasswordOnNewPasswordPage(String value) {
        changeNewPasswordPage.setConfirmPassword(value);
    }

    @Then("Red border for password field is present on new password page")
    public void redBorderForPasswordFieldIsPresentOnNewPasswordPage() {
        Assert.assertTrue(changeNewPasswordPage.isRedBorderForPasswordField(), "Red border for password field should be present");
    }

    @Then("Red border for confirm password field is present on new password page")
    public void redBorderForConfirmPasswordFieldIsPresentOnNewPasswordPage() {
        Assert.assertTrue(changeNewPasswordPage.isRedBorderForConfirmPasswordField(), "Red border for confirm password field should be present");
    }

    @Then("Validation rules message {string} has color {string} on new password page")
    public void validationRulesMessageIsPresentOnNewPasswordPage(String message, String color) {
        String expectedColor = ElementColor.getEnumValue(color).getColorCode();
        Assert.assertEquals(changeNewPasswordPage.getMessageTextColor(message), expectedColor, String.format("Message %s should be in %s color", message, color));
    }

    @Then("Message {string} is present on new password page")
    public void messageIsPresentOnNewPasswordPage(String message) {
        Assert.assertTrue(changeNewPasswordPage.isMessageOnPage(message), "Error message for password should be present");
    }

    @Then("I save {string} for user {string} on new password page")
    public void iSaveNewDataForUser(String data, String key) {
        User user = scenarioContext.get(key);
        user.setPassword(data);
    }
}
