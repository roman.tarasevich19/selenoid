package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import aquality.selenium.browser.AqualityServices;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.pagefields.marketplace.UserEditIdentityFormFields;
import diaceutics.selenium.enums.pagefields.marketplace.UserSettingsFormFields;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.marketplace.MyProfilePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class MyProfilePageSteps {

    private final MyProfilePage myProfilePage;
    private final ScenarioContext scenarioContext;

    @Inject
    public MyProfilePageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        myProfilePage = new MyProfilePage();
    }

    @Then("MyProfile page is opened")
    public void myProfilePageIsOpened() {
        Assert.assertTrue(myProfilePage.isDisplayed(), "MyProfile page should be opened");
    }

    @And("Data for user {string} is displayed on the following fields on User Edit Identity Form on MyProfile page:")
    public void dataForUserAreDisplayedOnTheFollowingFieldsOnUserEditIdentityForm(String key, List<String> fields) {
        User user = scenarioContext.get(key);
        fields.forEach(field -> {
            String actualValue = myProfilePage.getUserEditIdentityForm().getFieldValue(UserEditIdentityFormFields.getEnumValue(field));
            String expectedValue = user.getReflectionFieldValue(UserEditIdentityFormFields.getEnumValue(field).getModelField());
            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value for field %s is not correct on Identity form on MyProfile page", field));
        });
    }

    @When("Set up {string} user for the following fields:")
    public void changeDataOnTheFollowingFieldsOnUserEditIdentityForm(String key, List<String> fields) {
        User user = scenarioContext.get(key);
        fields.forEach(field -> {
            String value = user.getReflectionFieldValue(UserEditIdentityFormFields.getEnumValue(field).getModelField());
            AqualityServices.getLogger().info("VALUE for field: " + field + " is: " + value);
            myProfilePage.getUserEditIdentityForm().setFieldValue(UserEditIdentityFormFields.getEnumValue(field), value);
        });
    }

    @When("Set up empty value for the following fields on User Edit form:")
    public void changeDataOnTheFollowingFieldsOnUserEditIdentityForm(List<String> fields) {
        fields.forEach(field -> myProfilePage.getUserEditIdentityForm()
                .setFieldValue(UserEditIdentityFormFields.getEnumValue(field), null));
    }

    @When("Next error")
    public void nextError() {
        myProfilePage.getUserSettingsForm().openNextError();
    }

    @Then("Save changes on Edit Identify Form on MyProfilePage")
    public void save() {
        myProfilePage.getUserEditIdentityForm().save();
    }

    @Then("Error container is displayed for following fields on User Identity Form on MyProfile page:")
    public void errorContainerIsDisplayedForFollowingFields(List<String> fields) {
        fields.forEach((field) -> SoftAssert.getInstance().assertTrue(myProfilePage.getUserEditIdentityForm()
                        .isRedBorderDisplayedForField(UserEditIdentityFormFields.getEnumValue(field)),
                String.format("Error container for field - %s should be displayed", field)));
    }

    @And("Form with error message {string} is displayed on User Identity Form on MyProfile page")
    public void formWithErrorMessageIsDisplayedOnProfilePage(String message) {
        Assert.assertTrue(myProfilePage.getUserEditIdentityForm().isMessageDisplayed(message),
                String.format("Message %s should be displayed on My Profile page", message));
    }

    @Then("User information was updated successfully")
    public void userInfoWasUpdated() {
        Assert.assertTrue(myProfilePage.getUserEditIdentityForm().isUserInfoUpdated(), "User info should be updated successfully");
    }

    @When("I open Settings form on My Profile page")
    public void openSettingsForm() {
        myProfilePage.openSettings();
    }

    @When("I click on Delete your account")
    public void deleteAccount() {
        myProfilePage.getUserSettingsForm().deleteYourAccount();
    }

    @Then("Delete account window is opened")
    public void deleteAccountWindowIsOpened() {
        Assert.assertTrue(myProfilePage.getUserSettingsForm().isDeleteAccountWindowOpened(), "Delete your account window should be opened");
    }

    @Then("Delete account window is closed")
    public void deleteAccountWindowIsClosed() {
        Assert.assertTrue(myProfilePage.getUserSettingsForm().isDeleteAccountWindowClosed(), "Delete your account window should be closed");
    }

    @Then("Settings form is opened")
    public void settingsFormIsOpened() {
        Assert.assertTrue(myProfilePage.getUserSettingsForm().isDisplayed(), "Settings form should be opened");
    }

    @And("I click on Cancel on Delete Account window")
    public void cancelOnDeleteAccountWindow() {
        myProfilePage.getUserSettingsForm().cancel();
    }

    @And("I click on OK on Delete Account window")
    public void okOnDeleteAccountWindow() {
        myProfilePage.getUserSettingsForm().ok();
    }

    @And("I click on Close form on Delete Account window")
    public void closeFormOnDeleteAccountWindow() {
        myProfilePage.getUserSettingsForm().closeForm();
    }

    @And("I set up Password {string} on Delete Account window")
    public void setUpPasswordOnDeleteAccountWindow(String value) {
        myProfilePage.getUserSettingsForm().fillInConfirmPasswordForDelete(value);
    }

    @When("Set up password values on User Settings Form:")
    public void setUpPasswordsOnUserSettingsForm(Map<String, String> fields) {
        fields.forEach((field, value) -> myProfilePage.getUserSettingsForm()
                .setFieldValue(UserSettingsFormFields.getEnumValue(field), value));
    }

    @And("Form with error message {string} is displayed on User Settings form on MyProfile page")
    public void formWithErrorMessageIsDisplayedOnPage(String message) {
        Assert.assertTrue(myProfilePage.getUserSettingsForm().isMessageDisplayed(message),
                String.format("Message %s should be displayed on settings form My Profile page", message));
    }

    @And("Alert should contains {string} on User Settings form")
    public void alertShouldContainsTextError(String message) {
        Assert.assertTrue(myProfilePage.getUserSettingsForm().isAlertWindowContainsTextError(message),
                String.format("Message %s should be in Alert Window on settings form My Profile page", message));
    }

    @Then("Save changes on User Settings Form on MyProfilePage")
    public void saveOnSettingsForm() {
        myProfilePage.getUserSettingsForm().save();
    }

    @Then("Error container is displayed for following fields on User Settings Form on MyProfile page:")
    public void errorContainerIsdisplayedForFollowingFields(List<String> fields) {
        fields.forEach((field) -> SoftAssert.getInstance().assertTrue(myProfilePage.getUserSettingsForm()
                        .isRedBorderDisplayedForField(UserSettingsFormFields.getEnumValue(field)),
                String.format("Error container for field - %s should be displayed", field)));
    }

}
