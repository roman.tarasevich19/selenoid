package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.selenium.enums.pagefields.assaymanagement.AssayDescriptionPageFields;
import diaceutics.selenium.enums.pagefields.assaymanagement.MoveThisAssayFormFields;
import diaceutics.selenium.models.Assay;
import diaceutics.selenium.models.Biomarker;
import diaceutics.selenium.pageobject.pages.assaymanagement.AssayDescriptionPage;
import diaceutics.selenium.utilities.StringUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class AssayDescriptionPageSteps {

    private final AssayDescriptionPage assayDescriptionPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public AssayDescriptionPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        assayDescriptionPage = new AssayDescriptionPage();
    }

    @Given("Assay description page is opened")
    public void assayDetailsPageIsOpened() {
        Assert.assertTrue(assayDescriptionPage.isDisplayed(), "Assay description page should be opened");
    }

    @When("I click Move this assay on Assay description page")
    public void iClickMoveThisAssayOnAssayDetailsPage() {
        assayDescriptionPage.clickMoveThisAssay();
    }

    @Then("Move this assay form is opened on Assay description page")
    public void moveThisAssayIsPresentOnAssayDetailsPage() {
        Assert.assertTrue(assayDescriptionPage.getMoveThisAssayForm().isDisplayed(), "Move this assay form should be opened");
    }

    @When("I select new lab {string} to move assay")
    public void selectNewLabForMoveAssay(String newLab) {
        LabDto lab = scenarioContext.get(newLab);
        String labName = lab.getName();
        assayDescriptionPage.getMoveThisAssayForm().setFieldValue(MoveThisAssayFormFields
                .getEnumValue(MoveThisAssayFormFields.SELECT_LAB.getFriendlyName()), labName);
    }

    @When("I approve move assay")
    public void iApproveMoveAssay() {
        assayDescriptionPage.getMoveThisAssayForm().setFieldValue(MoveThisAssayFormFields
                .getEnumValue(MoveThisAssayFormFields.I_UNDERSTAND_THIS_ASSAY_WILL_BE_REMOVED.getFriendlyName()), "true");
    }

    @When("I click {string} button on Move this assay form")
    public void clickButtonOnMoveThisAssayForm(String buttonName) {
        assayDescriptionPage.getMoveThisAssayForm().clickByButton(buttonName);
    }

    @And("Message {string} is displayed on required fields on Move assay form")
    public void messagePleaseInputACountryDisplayedOnRequiredFieldsOnMoveAssayForm(String message) {
        Assert.assertTrue(assayDescriptionPage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Move assay form", message));
    }

    @When("I click Edit Details on Assay description page")
    public void iClickEditDetailsOnAssayDetailsPage() {
        assayDescriptionPage.clickEditDetails();
    }

    @And("Biomarker {string} is displayed on Assay description page")
    public void biomarkerBiomarkerOneIsDisplayedOnAssayDescriptionPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        Assert.assertTrue(assayDescriptionPage.getBiomarkersAndVariantsGridForm().isBiomarkerDisplayed(biomarker),
                String.format("Biomarker %s should be displayed on Assay description page",
                        biomarker.getBiomarkerName()));
    }

    @And("The following biomarkers are displayed on Assay description page:")
    public void biomarkersAreDisplayedOnAssayDescriptionPage(List<String> expectedBiomarkers) {
        Assert.assertEquals(assayDescriptionPage.getBiomarkersAndVariantsGridForm().getAllBiomarkersFromGrid(), expectedBiomarkers,
                "Biomarkers should be displayed on Assay description page");
    }

    @And("Biomarkers are sorted by name ASC on Assay description page")
    public void biomarkersAreSortedOnAssayDescriptionPage() {
        Assert.assertTrue(assayDescriptionPage.getBiomarkersAndVariantsGridForm().areBiomarkersSorted(),
                "Biomarkers should be sorted ASC on Assay description page");
    }

    @And("Biomarker {string} is not displayed on Assay description page")
    public void biomarkerBiomarkerOneIsNotDisplayedOnAssayDescriptionPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        Assert.assertFalse(assayDescriptionPage.getBiomarkersAndVariantsGridForm().isBiomarkerDisplayed(biomarker),
                String.format("Biomarker with values %s, %s should not present on Assay description page",
                        biomarker.getBiomarkerName(), biomarker.getVariants()));
    }

    @And("Assay {string} with following fields is displayed on Assay description page:")
    public void assayWithFollowingFieldsIsDisplayedOnAssayDescriptionPage(String key, List<String> fields) {
        Assay expectedAssay = scenarioContext.get(key);
        fields.forEach(field -> {
            AssayDescriptionPageFields assayDescriptionPageField = AssayDescriptionPageFields.getEnumValue(field);
            String actualValue = assayDescriptionPage.getFieldValue(assayDescriptionPageField);
            String expectedValue = assayDescriptionPageField.formatValueAsField(
                    expectedAssay.getReflectionFieldValue(assayDescriptionPageField.getModelField()));

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for field - %s is not correct on Assay description page",
                            actualValue,
                            field));
        });
    }

    @And("field {string} has value {string} on Assay description page")
    public void fieldHasValueOnAssayDescriptionPage(String field, String expectedValue) {
        String actualValue = assayDescriptionPage.getFieldValue(AssayDescriptionPageFields.getEnumValue(field));
        SoftAssert.getInstance().assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s for field - %s is not correct on Assay description page",
                        actualValue,
                        field));
    }

    @And("Value {string} for following fields is displayed on Assay description page:")
    public void valueUnspecifiedForFollowingFieldsIsDisplayedOnAssayDescriptionPage(String expectedValue, List<String> fields) {
        fields.forEach(field -> {
            AssayDescriptionPageFields assayDescriptionPageField = AssayDescriptionPageFields.getEnumValue(field);
            String actualValue = assayDescriptionPage.getFieldValue(assayDescriptionPageField);
            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for field - %s is not correct on Assay description page",
                            actualValue,
                            field));
        });
    }

    @And("Variants for {string} are displayed in Biomarkers and variants grid on Assay description page")
    public void variantsForBiomarkerAreDisplayedInBiomarkersAndVariantsGridOnAssayDescriptionPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        List<String> expectedVariants = biomarker.getListOfVariants();
        List<String> actualVariants = assayDescriptionPage.getBiomarkersAndVariantsGridForm()
                .getVariantsForBiomarker(biomarker.getBiomarkerName());

        Assert.assertTrue(expectedVariants.containsAll(actualVariants),
                String.format("Variants from Biomarker %s should be displayed in Biomarkers and variants grid on Assay description page",
                        biomarker.getBiomarkerName()));
    }

    @And("Variants for {string} sorted according to alphabet in Biomarkers and variants grid on Assay description page")
    public void variantsForBiomarkerSortedAccordingToAlphabetInBiomarkersAndVariantsGridOnAssayDescriptionPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        Assert.assertTrue(assayDescriptionPage.getBiomarkersAndVariantsGridForm()
                        .isVariantsSortedForBiomarker(biomarker.getBiomarkerName()),
                String.format("Variants for %s biomarker should be sorted in Biomarkers and variants grid on Assay description page",
                        biomarker.getBiomarkerName()));
    }

    @And("Variants column for {string} is empty in Biomarkers and variants grid on Assay description page")
    public void variantsColumnForBiomarkerIsEmptyInBiomarkersAndVariantsGridOnAssayDescriptionPage(String key) {
        Biomarker biomarker = scenarioContext.get(key);
        Assert.assertTrue(assayDescriptionPage.getBiomarkersAndVariantsGridForm()
                        .isVariantsColumnEmptyForBiomarker(biomarker.getBiomarkerName()),
                String.format("Variants for %s biomarker should be sorted in Biomarkers and variants grid on Assay description page",
                        biomarker.getBiomarkerName()));
    }

    @And("Field Select lab should be contains {string} on Move this assay form on Assay description page")
    public void fieldSelectLabShouldBeContainsLabsOnMoveThisAssayFormOnAssayDescriptionPage(String key) {
        List<String> expectedValues = scenarioContext.get(key);
        List<String> actualValues = assayDescriptionPage.getMoveThisAssayForm()
                .getListOptionsFromField(MoveThisAssayFormFields.SELECT_LAB).stream()
                .map(StringUtil::removeExtraCharacters)
                .sorted().collect(Collectors.toList());

        Assert.assertEquals(actualValues, expectedValues, "Field Select lab should be contains all labs");
    }

    @And("Value {string} for {string} field is displayed on Assay description page")
    public void valueForFieldIsDisplayedOnAssayDescriptionPage(String expectedValue, String field) {
        AssayDescriptionPageFields assayDescriptionPageField = AssayDescriptionPageFields.getEnumValue(field);
        String actualValue = assayDescriptionPage.getFieldValue(assayDescriptionPageField);

        SoftAssert.getInstance().assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s for field - %s is not correct on Assay description page",
                        actualValue,
                        field));
    }

    @When("I click View status history link on Assay description page")
    public void iClickViewStatusHistoryLinkOnAssayDescriptionPage() {
        assayDescriptionPage.clickViewStatusHistoryLink();
    }
}
