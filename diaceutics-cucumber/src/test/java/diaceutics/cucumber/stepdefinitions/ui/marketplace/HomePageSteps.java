package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import diaceutics.selenium.pageobject.pages.marketplace.HomePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class HomePageSteps {

    private final HomePage homePage;

    public HomePageSteps() {
        homePage = new HomePage();
    }

    @Given("Home page is opened")
    public void homePageIsOpened() {
        Assert.assertTrue(homePage.isDisplayed(), "Home page should be opened");
    }

    @When("I open {string} tools")
    public void iOpenAssayManagementTools(String tool) {
        homePage.openTool(tool);
    }

    @Then("Tool {string} is not available")
    public void toolIsNotAvailable(String tool) {
        Assert.assertFalse(homePage.isToolPresent(tool), String.format(" Tool: %S should be not available", tool));
    }

    @When("I click Start a collaboration on Home page")
    public void iClickStartACollaborationOnHomePage() {
        homePage.clickStartCollaboration();
    }

    @When("I click View all public collaborations on Home page")
    public void iClickViewAllPublicCollaborationsOnHomePage() {
        homePage.clickViewAllPublicCollaborations();
    }

    @And("Following message is displayed on Home page:")
    public void followingMessageIsDisplayedOnHomePage(String message) {
        Assert.assertTrue(homePage.isAdminMessageDisplayed(message),
                String.format("Message %s should be displayed on Home page", message));
    }
}
