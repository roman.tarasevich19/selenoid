package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.selenium.enums.pagefields.assaymanagement.AssayManagementPageFields;
import diaceutics.selenium.pageobject.pages.assaymanagement.AssayManagementPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;

public class AssayManagementPageSteps {

    private final AssayManagementPage assayManagementPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public AssayManagementPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        assayManagementPage = new AssayManagementPage();
    }

    @Given("Assay Management page is opened")
    public void assayManagementPageIsOpened() {
        Assert.assertTrue(assayManagementPage.isDisplayed(), "Assay Management page should be opened");
    }

    @When("I click {string} on Assay Management page")
    public void openCreateLabPage(String buttonName) {
        assayManagementPage.clickByButton(buttonName);
    }

    @When("I choose a Country {string} and press Search icon on Assay Management page")
    public void iChooseACountryAndPressSearchIcon(String country) {
        assayManagementPage.setFieldValue(AssayManagementPageFields.COUNTRY, country);
        assayManagementPage.clickSearch();
    }

    @When("I put a Lab {string} on search field {string} and press Search icon on Assay Management page")
    public void iPutALabLabNameAnSearchTextBoxAndPressSearchIcon(String key, String searchFieldName) {
        LabDto lab = scenarioContext.get(key);
        String labName = lab.getName();
        assayManagementPage.putTextInSearchField(labName, searchFieldName);
        assayManagementPage.clickSearch();
    }

    @When("I put a {string} on search field {string} and press Search icon on Assay Management page")
    public void iPutValueAnSearchTextBoxAndPressSearchIcon(String value, String searchFieldName) {
        assayManagementPage.putTextInSearchField(value, searchFieldName);
        assayManagementPage.clickSearch();
    }

    @When("I click random country on Assay Management page and save {string} and {string}")
    public void iClickCountryOnAssayManagementPageAndSaveLabCount(String countryNameKey, String numberOfLabsKey) {
        String countryName = assayManagementPage.getRandomCountry();
        String labCount = assayManagementPage.getLabsCountForCountry(countryName);
        scenarioContext.add(countryNameKey, countryName);
        scenarioContext.add(numberOfLabsKey, labCount);
        assayManagementPage.clickByCountryLink(countryName);
    }

    @When("I get list countries from dropdown on Assay Management page and save as {string}")
    public void iGetListCountriesFromDropdownOnAssayManagementPageAndSaveAsCountries(String key) {
        List<String> countries = assayManagementPage.getListCountriesFromDropDown();
        scenarioContext.add(key, countries);
    }

    @Then("List {string} should be the same as a list countries on Assay Management")
    public void listCountriesShouldBeTheSameAsAListCountriesOnAssayManagement(String key) {
        List<String> countriesFromDropDown = scenarioContext.get(key);
        List<String> countriesFromPage = assayManagementPage.getStringListCountries();
        Assert.assertEquals(countriesFromDropDown, countriesFromPage,
                "List countries from dropdown should be the same as list countries from page");
    }

    @When("I choose a Country {string} on Assay Management page")
    public void iChooseACountryPeruOnAssayManagementPage(String country) {
        assayManagementPage.setFieldValue(AssayManagementPageFields.COUNTRY, country);
    }

    @When("I put name from {string} without the hyphen on search field {string} and press Search on Assay Management page")
    public void iPutNameFromLabWithoutTheHyphenOnSearchFieldAndPressSearchOnAssayManagementPage(String key, String searchFieldName) {
        LabDto lab = scenarioContext.get(key);
        String labNameWithoutHyphen = lab.getName().replace("-", " ");
        assayManagementPage.putTextInSearchField(labNameWithoutHyphen, searchFieldName);
        assayManagementPage.clickSearch();
    }
}
