package diaceutics.cucumber.stepdefinitions.ui.marketplace;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.fieldvalues.FieldValues;
import diaceutics.selenium.enums.pagefields.marketplace.PersonalDetailsFormFields;
import diaceutics.selenium.models.User;
import diaceutics.selenium.pageobject.pages.marketplace.RegistrationPage;
import diaceutics.selenium.utilities.MailUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;


public class RegistrationPageSteps {

    private final RegistrationPage registrationPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public RegistrationPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        registrationPage = new RegistrationPage();
    }

    @Given("Registration page is opened")
    public void registerPageIsOpened() {
        Assert.assertTrue(registrationPage.isDisplayed(), "Registration page should be opened");
    }

    @When("I fill following fields on Personal Details form on Registration page and save as {string}:")
    public void iSetFollowingValuesForRegisterFormAndSaveAsAccount(String key, Map<String, String> data) {
        User user = new User();
        data.forEach((field, value) -> {
            value = value.equals(FieldValues.MAILOAUR.toString()) ? MailUtil.createMailAddress() : value;
            String selectedValue = registrationPage.getPersonalDetailsForm()
                    .setFieldValue(PersonalDetailsFormFields.getEnumValue(field), value);

            user.setReflectionFieldValue(PersonalDetailsFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, user);
    }

    @And("I click Register on Personal Details form on Registration page")
    public void iClickRegisterOnRegisterPage() {
        registrationPage.getPersonalDetailsForm().clickRegister();
    }

    @Then("Error container is displayed for following fields on Registration page:")
    public void errorContainerIsDisplayedForFollowingFields(List<String> fields) {
        fields.forEach((field) -> SoftAssert.getInstance().assertTrue(registrationPage.getPersonalDetailsForm()
                        .isRedBorderDisplayedForField(PersonalDetailsFormFields.getEnumValue(field)),
                String.format("Error container for field - %s should be displayed", field)));
    }

    @Then("Error container is not displayed for following fields on Registration page:")
    public void errorContainerIsNotDisplayedForFollowingFields(List<String> fields) {
        fields.forEach((field) -> SoftAssert.getInstance().assertFalse(registrationPage.getPersonalDetailsForm()
                        .isRedBorderDisplayedForField(PersonalDetailsFormFields.getEnumValue(field)),
                String.format("Error container for field - %s is displayed", field)));
    }

    @And("Following message is displayed on Personal Details form on Registration page:")
    public void followingMessageIsDisplayedOnPersonalDetailsFormOnRegistrationPage(List<String> messages) {
        String message = String.join(" ", messages);
        Assert.assertTrue(registrationPage.getPersonalDetailsForm().isMessageDisplayed(message),
                String.format("Message %s should be displayed on Personal Details form on Registration page", message));
    }

    @When("I click {string} link on Registration page")
    public void iClickLinkOnRegistrationPage(String linkName) {
        registrationPage.clickByLink(linkName);
    }

    @And("Form with error message {string} is displayed on Registration page")
    public void formWithErrorMessageIsDisplayedOnRegistrationPage(String message) {
        Assert.assertTrue(registrationPage.getInformForm().isMessageDisplayed(message),
                String.format("Message %s should be displayed on Registration page", message));
    }

    @Then("Error container is displayed for {string} field on Registration page")
    public void errorContainerIsDisplayedForFieldOnRegistrationPage(String field) {
        SoftAssert.getInstance().assertTrue(registrationPage.getPersonalDetailsForm()
                        .isRedBorderDisplayedForField(PersonalDetailsFormFields.getEnumValue(field)),
                String.format("Error container for field - %s should be displayed", field));
    }
}
