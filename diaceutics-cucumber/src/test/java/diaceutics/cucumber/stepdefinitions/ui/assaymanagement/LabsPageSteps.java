package diaceutics.cucumber.stepdefinitions.ui.assaymanagement;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.selenium.enums.pagefields.assaymanagement.LabsPageFields;
import diaceutics.selenium.pageobject.pages.assaymanagement.LabsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;

public class LabsPageSteps {

    private final LabsPage labsPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public LabsPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        labsPage = new LabsPage();
    }

    @Given("Labs page is opened")
    public void labsPageIsOpened() {
        Assert.assertTrue(labsPage.isDisplayed(), "Labs page should be opened");
    }

    @And("Search is finished")
    public void searchIsFinished() {
        labsPage.waitForLabsDisplayed();
    }

    @Then("I select {string} lab on Labs page")
    public void iSelectCountryOnFiltersLabsPage(String key) {
        LabDto lab = scenarioContext.get(key);
        labsPage.clickByLabLink(lab.getName());
    }

    @Then("All of the following labs for the specific country are displayed on Labs page:")
    public void allOfTheLabsForTheCountryCountryNameAreDisplayed(List<String> keys) {
        keys.forEach((key) -> {
            LabDto lab = scenarioContext.get(key);
            String labName = lab.getName();
            SoftAssert.getInstance().assertTrue(labsPage.isLabDisplayedIndFilterResults(labName),
                    String.format("Lab %s should be displayed in filter results", labName));
        });
    }

    @And("Lab {string} is displayed in filter results on Labs page")
    public void labTestLabIsDisplayedInFilterResults(String key) {
        LabDto lab = scenarioContext.get(key);
        String labName = lab.getName();
        Assert.assertTrue(labsPage.isLabDisplayedIndFilterResults(labName),
                String.format("Lab %s should be displayed in filter results", labName));
    }

    @And("I get result list and each entry contains search filter {string} on Labs page")
    public void labTestLabIsDisplayed(String searchText) {
        labsPage.getTextFromEachEntry().forEach(textInBlock ->
                Assert.assertTrue(StringUtils.containsIgnoreCase(textInBlock, searchText),
                        String.format("Text %s should contains search query %s", textInBlock, searchText)));
    }

    @When("I click random lab on Labs page")
    public void iClickRandomLabOnLabsPage() {
        String labName = labsPage.getRandomLabName();
        labsPage.clickByLabLink(labName);
    }

    @And("{string} should be the same as a labs count on Labs page")
    public void numberOfLabsShouldBeTheSameAsALabsCountOnLabsPage(String key) {
        String expectedLabsCount = scenarioContext.get(key);
        String actualLabsCount = labsPage.getLabsCount();
        Assert.assertEquals(actualLabsCount, expectedLabsCount,
                String.format("Labs count should be - %s on Labs page", expectedLabsCount));
    }

    @And("{string} should be the same as a country name on Labs page")
    public void countryNameShouldBeTheSameAsACountryNameOnLabsPage(String key) {
        String expectedCountryName = scenarioContext.get(key);
        String actualCountryName = labsPage.getCountryName();
        Assert.assertEquals(actualCountryName, expectedCountryName,
                String.format("Labs count should be - %s on Labs page", expectedCountryName));
    }

    @When("I set {string} value for Per-page on Labs page")
    public void iSetValueForPerPageOnLabsPage(String value) {
        labsPage.setFieldValue(LabsPageFields.PER_PAGE, value);
    }

    @Then("Labs from {string} to {string} should be displayed on Labs page")
    public void labsFromToShouldBeDisplayedOnLabsPage(String expectedMinRange, String expectedMaxRange) {
        String actualMinRange = labsPage.getMinRange();
        String actualMaxRange = labsPage.getMaxRange();
        Assert.assertEquals(actualMaxRange, expectedMaxRange,
                String.format("Max lab range should be - %s on Labs page", expectedMaxRange));

        Assert.assertEquals(actualMinRange, expectedMinRange,
                String.format("Min lab range should be - %s on Labs page", expectedMinRange));
    }

    @When("I click Next on Labs page")
    public void iClickNextOnLabsPage() {
        labsPage.clickNext();
    }

    @When("I click First on Labs page")
    public void iClickFirstOnLabsPage() {
        labsPage.clickFirst();
    }

    @When("I click Next button {int} times on Labs page")
    public void iClickNextButtonTimesOnLabsPage(int numberOfTimes) {
        labsPage.clickNext(numberOfTimes);
    }

    @And("Labs page is opened on page {int}")
    public void labsPageIsOpenedOnPage(int expectedNumberOfPage) {
        int actualNumberOfPage = labsPage.getCurrentNumberOfPage();
        Assert.assertEquals(actualNumberOfPage, expectedNumberOfPage,
                String.format("Labs page should be opened on page - %s", expectedNumberOfPage));
    }

    @When("I choose a Country {string} and press Search icon on Labs page")
    public void iChooseACountryAndPressSearchIconOnLabsPage(String country) {
        labsPage.setFieldValue(LabsPageFields.COUNTRY, country);
        labsPage.clickSearch();
    }

    @When("I put a {string} on search field {string} and press Search icon on Labs page")
    public void iPutOnSearchFieldEnterKeywordsAndPressSearchIconOnLabsPage(String value, String searchFieldName) {
        labsPage.putTextInSearchField(value, searchFieldName);
        labsPage.clickSearch();
    }
}
