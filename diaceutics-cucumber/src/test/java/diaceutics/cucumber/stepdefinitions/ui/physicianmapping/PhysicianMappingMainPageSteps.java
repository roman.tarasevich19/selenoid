package diaceutics.cucumber.stepdefinitions.ui.physicianmapping;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.enums.pagefields.physicianmapping.PhysicianMappingMainPageFields;
import diaceutics.selenium.models.SearchFilter;
import diaceutics.selenium.pageobject.pages.physicianmapping.PhysicianMappingMainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Map;

public class PhysicianMappingMainPageSteps {

    private final PhysicianMappingMainPage physicianMappingMainPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public PhysicianMappingMainPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        physicianMappingMainPage = new PhysicianMappingMainPage();
    }

    @Then("Physician Mapping Main page is opened")
    public void physicianMappingMainIsOpened() {
        Assert.assertTrue(physicianMappingMainPage.isDisplayed(), "Physician Mapping Main page should be opened");
    }

    @When("I fill following fields on Physician Mapping Main page:")
    public void iFillFollowingFieldsOnPhysicianMappingMainPage(Map<String, String> data) {
        data.forEach((field, value) -> physicianMappingMainPage.setFieldValue(PhysicianMappingMainPageFields.getEnumValue(field), value));
    }

    @And("Link {string} is not present on Physician Mapping Main page")
    public void linkIsNotPresentOnPhysicianMapping(String link) {
        Assert.assertFalse(physicianMappingMainPage.isLinkDisplayed(link),
                String.format("Link %s should be not present on Physician Mapping main page", link));
    }

    @And("I click {string} on Physician Mapping Main page")
    public void iClickStartOnPhysicianMappingMainPage(String buttonName) {
        physicianMappingMainPage.clickByButton(buttonName);
    }

    @Then("Message {string} is displayed on Physician Mapping Main page")
    public void messageSomeItemsBelowNeedYourAttentionIsDisplayedOnPhysicianMappingMainPage(String message) {
        Assert.assertTrue(physicianMappingMainPage.isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Physician Mapping Main page", message));
    }

    @And("Message {string} is displayed on required fields on Physician Mapping Main page")
    public void messageIsDisplayedOnRequiredFieldsOnPhysicianMappingMainPage(String message) {
        Assert.assertTrue(physicianMappingMainPage.isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Physician Mapping Main page", message));
    }

    @When("I fill following fields on Physician Mapping Main page and save {string}:")
    public void iFillFollowingFieldsOnPhysicianMappingMainPageAndSaveSearchFilter(String key, Map<String, String> data) {
        SearchFilter filter = new SearchFilter();
        data.forEach((field, value) -> {
            String selectedValue = physicianMappingMainPage.setFieldValue(
                    PhysicianMappingMainPageFields.getEnumValue(field), value);

            filter.setReflectionFieldValue(
                    PhysicianMappingMainPageFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, filter);
    }

    @And("I click {string} link on Physician Mapping Main page")
    public void iClickLinkOnPhysicianMappingMainPage(String link) {
        physicianMappingMainPage.clickByLink(link);
    }

    @And("Modal form title is {string} on Physician Mapping Main page")
    public void modalFormTitleIs(String title) {
        Assert.assertEquals(physicianMappingMainPage.getAboutThisDataDescriptionForm().getModalTitle(), title,
                "Title of modal form is not correct on Physician Mapping Main page");
    }

    @And("I get {string} text and {string} and compare on Physician Mapping Main page")
    public void iCompareSavedTextOnPhysicianMappingPage(String key, String anotherKey) {
        String expectedText = scenarioContext.get(key);
        String actualText = scenarioContext.get(anotherKey);
        Assert.assertEquals(actualText, expectedText, "Text is not the same on Physician Mapping Main page");
    }

    @And("I get text content from modal form and save to {string} on Physician Mapping page")
    public void getTextContentFromModalFormOnPhysicianMappingPage(String key){
        scenarioContext.add(key, physicianMappingMainPage.getAboutThisDataDescriptionForm().getTextContent());
    }

    @And("I click {string} without wait on Physician Mapping Main page")
    public void iClickStartWithoutWaitOnPhysicianMappingMainPage(String buttonName) {
        physicianMappingMainPage.clickByButtonWithoutWait(buttonName);
    }

}
