package diaceutics.cucumber.stepdefinitions.ui;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.pageobject.forms.ChatForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import javax.inject.Inject;

public class ChatSteps {

    private final ChatForm chatForm;
    private final ScenarioContext scenarioContext;

    @Inject
    public ChatSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        chatForm = new ChatForm();
    }

    @When("I open chat form")
    public void openChatFormFromLabMappingPage() {
        chatForm.clickOnChatForm();
    }

    @Then("Chat form is opened")
    public void chatFormOnLabMappingMainIsOpened() {
        Assert.assertTrue(chatForm.isDisplayed(), "Chat form on Lab Mapping Main page should be opened");
    }

    @And("I switch to Chat form frame")
    public void switchToFrame(){
        chatForm.switchToChatForm(1);
    }

    @And("Start another conversation section is present")
    public void startAnotherConversationSectionIsPresent(){
        Assert.assertTrue(chatForm.isStartAnotherConversationPresent());
    }

    @And("Continue the conversation section is present")
    public void continueTheConversationSectionIsPresent(){
        Assert.assertTrue(chatForm.isContinueTheConversationPresent());
    }

    @And("Find your answers section is present")
    public void findYourAnswersSectionIsPresent(){
        Assert.assertTrue(chatForm.isFindYourAnswerPresent());
    }

    @And("I return back on Chat Form")
    public void returnBack(){
        chatForm.returnBack();
    }

    @When("I click See all your conversation")
    public void seeAllYourConversations(){
        chatForm.openSeeYourAllConversations();
    }

    @Then("Your conversations section is opened")
    public void yourConversationsSectionIsOpened(){
        Assert.assertTrue(chatForm.isYourConversationsSectionOpened(), "Your conversation section should be opened");
    }

    @When("I click Send us a message to open new chat")
    public void sendUsMessage(){
        chatForm.sendUsAMessage();
    }

    @When("I set up a new message {string}")
    public void setUpNewMessage(String message){
        chatForm.setUpMessage(message);
    }

    @Then("Send message button is not present")
    public void sendBtnIsNotPresent(){
        Assert.assertFalse(chatForm.isSendMessagePresent(), "Send btn should be not present with empty message");
    }

    @Then("Send message button is present")
    public void sendBtnIsPresent(){
        Assert.assertTrue(chatForm.isSendMessagePresent(), "Send btn should be present");
    }

    @And("I send a new message")
    public void sendNewMessage(){
        chatForm.sendMessage();
    }

    @Then("Message {string} sent")
    public void messageSent(String message){
        Assert.assertTrue(chatForm.isMessageSent(message));
    }

    @And("Answer for message is present")
    public void answerIsPresent(){
        Assert.assertTrue(chatForm.isAnswerPresent(), "Answer for message should be present");
    }

    @When("I open GIF menu and select first gif to sent")
    public void loadGIF(){
        chatForm.selectGIF();
    }

    @Then("GIF sent successfully")
    public void gifSent(){
        Assert.assertTrue(chatForm.isGifSent());
    }

    @When("I continue last conversation")
    public void continueLastConversation(){
        chatForm.continueLastConversation();
    }

    @Then("User can see all his previous conversations")
    public void userCanSeeAllHisPreviousChats(){
        Assert.assertTrue(chatForm.isUserCanSeeAllHisPreviousConversations());
    }

    @And("I click we run on Intercom link")
    public void runOnIntercom(){
        chatForm.runOnIntercom();
    }

    @When("I open Emoji menu and select first emoji to sent")
    public void loadEmoji(){
        chatForm.selectEmoji();
    }

    @Then("Emoji sent successfully")
    public void emojiSent(){
        Assert.assertTrue(chatForm.isEmojiSent(), "Emoji should be sent");
    }

    @When("I click on Upload document and upload {string}")
    public void upload(String file){
        chatForm.uploadFileOnChat(file);
    }

    @Then("Document {string} uploaded")
    public void documentUploaded(String doc){
        Assert.assertTrue(chatForm.isDocumentUploaded(doc), "Document should be uploaded");
    }

    @Then("I click on Find your answers search")
    public void clickOnFindYourAnswers(){
        chatForm.findYourAnswersSearch();
    }

    @When("I set up find your answers search with {string}")
    public void findYourAnswersSearch(String search){
        chatForm.findYourAnswer(search);
        chatForm.findYourAnswersSearch();
    }

    @Then("Search for find your answer section is present")
    public void answerIsForFindYourAnswersPresent(){
        Assert.assertTrue(chatForm.isAnswerForFindYourAnswerPresent());
    }

    @And("Search error is present")
    public void searchError(){
        Assert.assertTrue(chatForm.isSearchErrorAppears(), "Search error should be present");
    }
}
