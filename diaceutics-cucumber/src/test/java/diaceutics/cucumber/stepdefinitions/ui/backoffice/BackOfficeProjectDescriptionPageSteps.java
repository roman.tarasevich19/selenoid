package diaceutics.cucumber.stepdefinitions.ui.backoffice;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.backoffice.pageobject.pages.BackOfficeProjectDescriptionPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import javax.inject.Inject;

public class BackOfficeProjectDescriptionPageSteps {

    private final BackOfficeProjectDescriptionPage backOfficeProjectDescriptionPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public BackOfficeProjectDescriptionPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        backOfficeProjectDescriptionPage = new BackOfficeProjectDescriptionPage();
    }

    @Then("BackOffice project description page is opened")
    public void backOfficeMainPageIsOpened(){
        Assert.assertTrue(backOfficeProjectDescriptionPage.isDisplayed(), "Back office project description page is not opened");
    }

    @When("I click {string} link on BackOffice project description page")
    public void iClickLinkOnBackOfficeMainPage(String link){
        backOfficeProjectDescriptionPage.clickByLink(link);
    }

    @When("I get project description on BackOffice project description page and save to {string}")
    public void getProjectDescriptionOnBackOfficeProjectDescriptionPageAndSaveTo(String key){
        scenarioContext.add(key, backOfficeProjectDescriptionPage.getTextContentFromDescriptionArea());
    }

    @When("I update project description last row using {string} on BackOffice project description page")
    public void updateProjectDescriptionLastRowOnBackOfficeProjectDescriptionPageAndSaveTo(String text){
        backOfficeProjectDescriptionPage.updateTextContentInLastRow(text);
    }

    @When("I click on {string} control bar button BackOffice project description page")
    public void iClickOnControlBarButtonOnBackOfficeDescriptionPage(String button){
        backOfficeProjectDescriptionPage.clickOnControlBarButton(button);
    }
}
