package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.Collaboration;
import diaceutics.selenium.pageobject.pages.marketplace.collaborations.ResponsesPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class ResponsesPageSteps {

    private final ResponsesPage responsesPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public ResponsesPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        responsesPage = new ResponsesPage();
    }

    @Then("Responses page is opened")
    public void responsesPageIsOpened() {
        Assert.assertTrue(responsesPage.isDisplayed(), "Responses page should be opened");
    }

    @When("I remove all responses on Responses page")
    public void iRemoveAllResponsesOnResponsesPage() {
        responsesPage.removeAllResponses();
    }

    @And("Response from collaboration {string} is displayed on Responses page")
    public void responseFromCollaborationIsDisplayedOnResponsesPage(String key) {
        Collaboration collaboration = scenarioContext.get(key);
        Assert.assertTrue(responsesPage.isResponseFromCollaborationDisplayed(collaboration.getTitle()),
                String.format("Response from collaboration %s is displayed on Responses page", collaboration.getTitle()));
    }

    @When("I click show button for response from collaboration {string} on Responses page")
    public void iClickShowButtonForCollaborationOnResponsesPage(String key) {
        Collaboration collaboration = scenarioContext.get(key);
        responsesPage.clickShowButtonForCollaboration(collaboration.getTitle());
    }

    @And("Response from collaboration {string} has status {string} on Responses page")
    public void responseFromCollaborationTestCollaborationHasStatusOnResponsesPage(String key, String expectedStatus) {
        Collaboration collaboration = scenarioContext.get(key);
        String actualStatus = responsesPage.getStatusForResponseFromCollaboration(collaboration.getTitle());
        Assert.assertEquals(actualStatus, expectedStatus,
                String.format("Response from collaboration %s should has status %s on Responses page",
                        collaboration.getTitle(), expectedStatus));
    }
}
