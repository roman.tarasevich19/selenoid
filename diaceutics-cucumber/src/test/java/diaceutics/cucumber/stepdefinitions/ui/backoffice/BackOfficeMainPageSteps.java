package diaceutics.cucumber.stepdefinitions.ui.backoffice;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.backoffice.pageobject.pages.BackOfficeMainPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class BackOfficeMainPageSteps {

    private final BackOfficeMainPage backOfficeMainPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public BackOfficeMainPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        backOfficeMainPage = new BackOfficeMainPage();
    }

    @Then("BackOffice main page is opened")
    public void backOfficeMainPageIsOpened(){
        Assert.assertTrue(backOfficeMainPage.isDisplayed(), "Back office main page is not opened");
    }

    @When("I click {string} link on BackOffice main page")
    public void iClickLinkOnBackOfficeMainPage(String link){
        backOfficeMainPage.clickByLink(link);
    }
}
