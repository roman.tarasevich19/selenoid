package diaceutics.cucumber.stepdefinitions.ui.labmapping;

import aquality.selenium.browser.AqualityServices;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.chartlegends.LabMappingPieChartLegends;
import diaceutics.selenium.enums.charts.LabMappingCharts;
import diaceutics.selenium.enums.grids.AGGridHeadingIcons;
import diaceutics.selenium.enums.formatvalue.FormatValue;
import diaceutics.selenium.enums.pagefields.labmapping.*;
import diaceutics.selenium.enums.table.MenuOption;
import diaceutics.selenium.enums.table.TableFilter;
import diaceutics.selenium.enums.webelementcolors.ChartColor;
import diaceutics.selenium.models.*;
import diaceutics.selenium.pageobject.pages.labmapping.LabMappingPage;
import diaceutics.selenium.utilities.PDFUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class LabMappingPageSteps {

    private final LabMappingPage labMappingPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public LabMappingPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        labMappingPage = new LabMappingPage();
    }

    @Then("Lab Mapping page is opened")
    public void labMappingIsOpened() {
        Assert.assertTrue(labMappingPage.isDisplayed(), "Lab Mapping page should be opened");
    }

    @And("Lab {string} is displayed in AG-Grid Lab summary on Lab Mapping page")
    public void labLabIsDisplayedInAgGridOnLabMappingPage(String key) {
        Lab lab = scenarioContext.get(key);
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isLabDisplayed(lab),
                String.format("Lab %s should be displayed in Ag Grid on Lab Mapping page", lab.getName()));
    }

    @When("I click by {string} Lab in AG-Grid Lab summary on Lab Mapping page")
    public void iClickByLabInAgGridOnLabMappingPage(String key) {
        Lab lab = scenarioContext.get(key);
        labMappingPage.getAGGridLabSummaryForm().clickByContractedLab(lab.getName());
    }

    @When("I click on entry {string} in AG-Grid Lab summary on Lab Mapping page")
    public void iClickOnEntryInAgGridOnLabMappingPage(String name) {
        labMappingPage.getAGGridLabSummaryForm().clickByName(name);
    }

    @When("I switch view to {string} on Lab Mapping page")
    public void iSwitchViewOnLabMappingPage(String name) {
        labMappingPage.getAGGridLabSummaryForm().switchView(name);
    }

    @When("View {string} is opened on Lab Mapping page")
    public void currentStateViewOfAgGridOnLabMapping(String name) {
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isViewOpenedOnAgGrid(name), String.format("View %s should be opened", name));
    }

    @When("Sort widget is present on Lab Mapping page")
    public void sortWidgetIsPresentOnLabMappingPage() {
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isSortWidgetDisplayed(), "Sort widget should be present");
    }

    @When("Sort widget is not present on Lab Mapping page")
    public void sortWidgetIsNotPresentOnLabMappingPage() {
        Assert.assertFalse(labMappingPage.getAGGridLabSummaryForm().isSortWidgetDisplayed(), "Sort widget should be not present");
    }

    @And("I return back to LM main page")
    public void returnBack() {
        labMappingPage.returnBackToMainPage();
    }

    @Then("Assay {string} with following fields is displayed in AG-Grid Lab summary on Lab Mapping page:")
    public void assayWithFollowingFieldsIsDisplayedOnLabMappingPage(String key, List<String> fields) {
        Assay expectedAssay = scenarioContext.get(key);
        Biomarker expectedBiomarker = expectedAssay.getBiomarkers().get(0);
        expectedAssay.setStatuses();
        fields.forEach(field -> {
            AGGridLabSummaryFormAssayDetailsFields agGridLabSummaryFormAssayDetailsFields =
                    AGGridLabSummaryFormAssayDetailsFields.getEnumValue(field);

            String expectedValue = field.equals(AGGridLabSummaryFormAssayDetailsFields.BIOMARKER.getFriendlyName()) ||
                    field.equals(AGGridLabSummaryFormAssayDetailsFields.VARIANTS_INCLUDED.getFriendlyName()) ?
                    expectedBiomarker.getReflectionFieldValue(agGridLabSummaryFormAssayDetailsFields.getModelField()) :
                    agGridLabSummaryFormAssayDetailsFields.formatValueAsField(
                            expectedAssay.getReflectionFieldValue(agGridLabSummaryFormAssayDetailsFields.getModelField()));

            String actualValue = labMappingPage.getAGGridLabSummaryForm().getColumnFieldValue(agGridLabSummaryFormAssayDetailsFields);

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value %s for %s is not correct on Lab Mapping Results page",
                            actualValue, field));
        });
    }

    @And("Label {string} is displayed for Lab {string} in AG-Grid Lab summary on Lab Mapping page")
    public void labelIsDisplayedForLabLabInAGGridLabSummaryOnLabMappingPage(String label, String key) {
        Lab lab = scenarioContext.get(key);
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isLabelDisplayedForLab(label, lab),
                String.format("Label %s for %s should be displayed in AG-Grid Lab summary on Lab Mapping page",
                        label, lab.getName()));
    }

    @When("I fill following fields on Lab Mapping page:")
    public void iFillFollowingFieldsOnLabMappingPage(Map<String, String> data) {
        data.forEach((field, value) ->
                labMappingPage.getAGGridLabSummaryForm().setFieldValue(AGGridLabSummaryFormFields.getEnumValue(field), value));
    }

    @Then("Data in AG-Grid Lab summary on Lab Mapping page is sorted in descending order for DMS sorting")
    public void dataInAGGridLabSummaryOnLabMappingPageIsSortedInDescendingOrderForDMSSorting() {
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isDataSortedInDescendingOrderForDMS(),
                "Data in AG-Grid Lab summary should be sorted in descending order for both for DMS sorting");
    }

    @And("Value {string} is displayed in {string} field on Lab Mapping page")
    public void valueDescendingIsDisplayedInOrderFieldOnLabMappingPage(String expectedValue, String field) {
        String actualValue = labMappingPage.getAGGridLabSummaryForm()
                .getFieldValue(AGGridLabSummaryFormFields.getEnumValue(field)).replaceAll("\n", "");

        Assert.assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s for %s is not correct on Lab Mapping page",
                        actualValue, field));
    }

    @And("Data in AG-Grid Lab summary on Lab Mapping page is sorted in ascending order for DMS sorting")
    public void dataInAGGridLabSummaryOnLabMappingPageIsSortedInAscendingOrderForDMSSorting() {
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isDataSortedInAscendingOrderForDMS(),
                "Data in AG-Grid Lab summary should be sorted in ascending order for both for DMS sorting");
    }

    @Then("Data in AG-Grid Lab summary on Lab Mapping Results page is sorted in descending order for LTMS sorting")
    public void dataInAGGridLabSummaryOnLabMappingPageIsSortedInDescendingOrderForLTMSSorting() {
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isDataSortedInDescendingOrderForLTMS(),
                "Data in AG-Grid Lab summary should be sorted in descending order for both for LTMS sorting");
    }

    @Then("Data in AG-Grid Lab summary on Lab Mapping page is sorted in ascending order for LTMS sorting")
    public void dataInAGGridLabSummaryOnLabMappingPageIsSortedInAscendingOrderForLTMSSorting() {
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isDataSortedInAscendingOrderForLTMS(),
                "Data in AG-Grid Lab summary should be sorted in ascending order for both for LTMS sorting");
    }

    @And("I clear {string} field on AG-Grid on Lab Mapping page")
    public void iClearFieldOnAgGridOnLabMappingPage(String field) {
        labMappingPage.getAGGridLabSummaryForm().clearField(AGGridLabSummaryFormFields.getEnumValue(field));
    }

    @When("I click Export data on Lab Mapping page")
    public void iClickExportDataOnLabMappingPage() {
        labMappingPage.clickExportData();
    }

    @When("I read data from AG-Grid Lab summary on Lab Mapping page and compare with {string}")
    public void iReadDataFromAGGridLabSummaryOnLabMappingPageAndSaveAsLabs(String key) {
        List<Lab> labListFromUI = labMappingPage.getAGGridLabSummaryForm().getListLab();
        List<Lab> labListFromFile = scenarioContext.get(key);
        Assert.assertEquals(labListFromUI, labListFromFile.subList(0, 20),
                "The data from AG-Grid Lab summary on Lab Mapping page and from file should be the same");
    }

    @When("I read file {string} with Labs and save data as {string}")
    public void iReadFileWithLabsAndSaveDataAsLabsFromFile(String fileName, String key) {
        LabExelTable labExelTable = new LabExelTable(fileName);
        List<Lab> labs = labExelTable.getListLab();
        scenarioContext.add(key, labs);
    }

    @When("I click {string} tab on Lab Mapping page")
    public void iClickChartsTabOnOnLabMappingPage(String tab) {
        labMappingPage.clickTab(tab);
    }

    @When("I click on Lab Mapping label on Lab Mapping page")
    public void iClickOnLabelOnLabMappingPage() {
        labMappingPage.clickOnLMLabel();
    }

    @Then("Charts form on Lab Mapping page is opened")
    public void chartsFormOnLabMappingPageIsOpened() {
        Assert.assertTrue(labMappingPage.getLabMappingChartsForm().isDisplayed(),
                "Charts form on Lab Mapping should be opened");
    }

    @And("Search filter {string} with following fields is displayed on Lab Mapping page")
    public void searchFilterSearchFilterWithFollowingFieldsIsDisplayedOnLabMappingPage(String key, List<String> fields) {
        SearchFilter filter = scenarioContext.get(key);
        filter.setDateRange();
        fields.forEach(field -> {
            String actualValue = labMappingPage.getFieldValue(LabMappingPageFields.getEnumValue(field));
            String expectedValue = filter.getReflectionFieldValue(LabMappingPageFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value - %s for field - %s is not correct on Lab Mapping page",
                            actualValue, field));
        });
    }

    @When("I click {string} on Lab Mapping page")
    public void iClickEditSettingsOnLabMappingPage(String buttonName) {
        labMappingPage.clickByButton(buttonName);
    }

    @When("I click {string} link on Lab Mapping page")
    public void iClickLinkOnLabMappingPage(String buttonName) {
        labMappingPage.clickByLink(buttonName);
    }

    @Then("Edit settings form is opened on Lab Mapping page")
    public void editSettingsFormIsOpenedOnLabMappingPage() {
        Assert.assertTrue(labMappingPage.getEditSettingsForm().isDisplayed(),
                "Edit settings form on Lab Mapping page should be opened");
    }

    @When("I fill following fields on Edit settings form on Lab Mapping page and save {string}:")
    public void iFillFollowingFieldsOnEditSettingsFormOnLabMappingPageAndSaveSearchFilter(
            String key, Map<String, String> data) {
        SearchFilter filter = new SearchFilter();
        data.forEach((field, value) -> {
            String selectedValue = labMappingPage.getEditSettingsForm().setFieldValue(
                    LabMappingEditSettingsFormFields.getEnumValue(field), value);

            filter.setReflectionFieldValue(LabMappingEditSettingsFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, filter);
    }

    @And("I click {string} on Edit settings form on Lab Mapping")
    public void iClickApplyOnEditSettingsFormOnLabMapping(String buttonName) {
        labMappingPage.getEditSettingsForm().clickByButton(buttonName);
    }

    @When("I click Download chart {string} on Charts form on Lab Mapping page")
    public void iClickDownloadChartChartOnChartsFormOnLabMappingPage(String chartName) {
        labMappingPage.getLabMappingChartsForm()
                .clickDownloadAChart(LabMappingCharts.getEnumValue(chartName));
    }

    @When("I click Download all charts on Charts form on Lab Mapping page")
    public void iClickDownloadAllChartsOnChartsFormOnLabMappingPage() {
        labMappingPage.getLabMappingChartsForm().clickDownloadAllCharts();
    }

    @When("I save title to {string} from chart {string} on Lab Mapping page")
    public void iSaveTitleFromChartLabMappingPage(String key, String chartName) {
        String chartTitle = labMappingPage.getLabMappingChartsForm()
                .getHighchartsTitle(LabMappingCharts.getEnumValue(chartName));
        scenarioContext.add(key, chartTitle);
    }

    @When("I save titles to {string} from all charts on Lab Mapping page")
    public void iSaveTitlesFromAllChartLabMappingPage(String key) {
        List<String> titles = new ArrayList<>();
        for (LabMappingCharts chart : LabMappingCharts.values()) {
            String title = labMappingPage.getLabMappingChartsForm()
                    .getHighchartsTitle(LabMappingCharts.getEnumValue(chart.getFriendlyName()));
            titles.add(title);
        }
        scenarioContext.add(key, titles);
    }

    @When("I save subtitles to {string} from all charts on Lab Mapping page")
    public void iSaveSubTitlesFromAllChartLabMappingPage(String key) {
        List<String> subTitles = new ArrayList<>();
        for (LabMappingCharts chart : LabMappingCharts.values()) {
            String title = labMappingPage.getLabMappingChartsForm()
                    .getChartSubTitle(LabMappingCharts.getEnumValue(chart.getFriendlyName()));
            subTitles.add(title);
        }
        scenarioContext.add(key, subTitles);
    }

    @When("I save subtitle to {string} from chart {string} on Lab Mapping page")
    public void iSaveSubTitleFromChart(String key, String chartName) {
        String chartSubTitle = labMappingPage.getLabMappingChartsForm()
                .getChartSubTitle(LabMappingCharts.getEnumValue(chartName));
        scenarioContext.add(key, chartSubTitle);
    }

    @And("File {string} contains a chart on Lab Mapping with text {string}")
    public void fileContainsAChartWithTitle(String fileName, String key) {
        String chartTitleFromPage = scenarioContext.get(key);
        String textFromPdfFile = PDFUtil.parsePDF(fileName).replaceAll("[\\n\\r?]+", " ");
        Assert.assertTrue(textFromPdfFile.contains(chartTitleFromPage),
                String.format("Chart with text - %s should be in file", chartTitleFromPage));
    }

    @And("File {string} contains all charts values from {string} on Lab Mapping page")
    public void fileContainsAChartsWithListTitlesOnLabMapping(String fileName, String key) {
        List<String> chartTitlesFromPage = scenarioContext.get(key);
        String textFromPdfFile = PDFUtil.parsePDF(fileName).replaceAll("[\\n\\r?]+", " ")
                .replaceAll("[\\s]{2,}", " ");

        AqualityServices.getLogger().info("Get from PDF: " + textFromPdfFile);
        chartTitlesFromPage.forEach(chart -> Assert.assertTrue(textFromPdfFile.contains(chart), String.format("Chart with text - %s should be in file", chart)));
    }

    @When("I check {string} field in AG-Grid Lab summary on Lab Mapping page")
    public void iCheckFieldInAGGridLabSummaryOnLabMappingPage(String fieldName) {
        labMappingPage.getAGGridLabSummaryForm().setFieldValueTableFilter(fieldName, Boolean.TRUE);
    }

    @When("I get tooltip from Lab Details column {string} and compare with {string} on Lab Mapping page")
    public void iGetTooltipFromLabDetailsColumnAndCompareWithExpectedOnLabMappingPage(String fieldName, String tooltip) {
        AGGridLabSummaryFormLabDetailsFields field = AGGridLabSummaryFormLabDetailsFields.getEnumValue(fieldName);
        Assert.assertEquals(labMappingPage.getAGGridLabSummaryForm().getColumnTooltip(field), tooltip, "Tooltip is not correct");
    }

    @When("I get tooltip from Assay Details column {string} and compare with {string} on Lab Mapping page")
    public void iGetTooltipFromAssayDetailsColumnAndCompareWithExpectedOnLabMappingPage(String fieldName, String tooltip) {
        AGGridLabSummaryFormAssayDetailsFields field = AGGridLabSummaryFormAssayDetailsFields.getEnumValue(fieldName);
        Assert.assertEquals(labMappingPage.getAGGridLabSummaryForm().getColumnTooltip(field), tooltip, "Tooltip is not correct");
    }

    @Then("Column {string} is not present on Lab Mapping")
    public void isColumnPresent(String column) {
        Assert.assertFalse(labMappingPage.getAGGridLabSummaryForm().isColumnPresent(column),
                String.format("Column %s should be not present", column));
    }


    @When("I uncheck column {string} on Lab Mapping")
    public void uncheckColumn(String column) {
        labMappingPage.getAGGridLabSummaryForm().setFieldValueTableFilter(column, Boolean.FALSE);
    }

    @When("I get tooltip for Lab name {string} on Lab Mapping and compare with {string}")
    public void iGetTooltipForLabName(String lab, String expectedTooltip) {
        Assert.assertEquals(labMappingPage.getAGGridLabSummaryForm().getTooltipLabName(lab), expectedTooltip,
                "Tooltip for Lab name is not present");
    }

    @When("All three spinners are present on Lab Mapping page:")
    public void spinnerWithTextIsPresentOnLabMappingPage(List<String> spinnerText) {
        List<Boolean> expectedList = Arrays.asList(true, true, true);
        Assert.assertEquals(labMappingPage.getAGGridLabSummaryForm().getSpinnerText(spinnerText), expectedList,
                String.format("Spinner %s for Lab name is not present", spinnerText));
    }

    @And("I get tooltip for {string} label on Lab Mapping and compare with {string}")
    public void iGetTooltipForLabel(String label, String expectedTooltip) {
        Assert.assertEquals(labMappingPage.getAGGridLabSummaryForm().getTooltipLabel(label), expectedTooltip,
                String.format("Tooltip for %s is not present", label));
    }

    @When("I collapse Lab Details on Lab Mapping")
    public void collapseLabDetails() {
        labMappingPage.getAGGridLabSummaryForm().collapseLabDetails();
    }

    @When("I open Filter Menu for Lab Details column {string} on Lab Mapping")
    public void openFilterMenuForColumn(String column) {
        AGGridLabSummaryFormLabDetailsFields field = AGGridLabSummaryFormLabDetailsFields.getEnumValue(column);
        labMappingPage.getAGGridLabSummaryForm().openFilterColumn(field);
    }

    @When("I open Filter Menu for Assay Details column {string} on Lab Mapping")
    public void openFilterMenuForAssayColumn(String column) {
        AGGridLabSummaryFormAssayDetailsFields field = AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column);
        labMappingPage.getAGGridLabSummaryForm().openFilterColumn(field);
    }

    @Then("I check that options for Assay Details are present in opened column filter")
    public void checkOptions(List<String> expectedOptions) {
        expectedOptions.forEach(option -> Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isCheckBoxOptionPresent(option),
                String.format("Option %s should be present in column", option)));
    }

    @Then("Checkbox for column {string} is disabled on Lab Mapping page")
    public void checkThatCheckboxIsDisabledForLabMappingPage(String column) {
        Assert.assertFalse(labMappingPage.getAGGridLabSummaryForm().isCheckBoxOptionPresent(column),
                String.format("Checkbox for %s should be disabled", column));
    }

    @When("I open Menu for Assay Details column {string} on Lab Mapping")
    public void openMenuForAssayDetailsColumn(String column) {
        AGGridLabSummaryFormAssayDetailsFields field = AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column);
        labMappingPage.getAGGridLabSummaryForm().openColumnMenu(field);
    }

    @When("I click on Assay Details column {string} title on Lab Mapping")
    public void iClickOnAssayDetailsColumnTitle(String column) {
        AGGridLabSummaryFormAssayDetailsFields field = AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column);
        labMappingPage.getAGGridLabSummaryForm().clickOnColumnTitle(field);
    }

    @When("I click on Lab Details column {string} title on Lab Mapping")
    public void iClickOnLabDetailsColumnTitle(String column) {
        AGGridLabSummaryFormLabDetailsFields field = AGGridLabSummaryFormLabDetailsFields.getEnumValue(column);
        labMappingPage.getAGGridLabSummaryForm().clickOnColumnTitle(field);
    }

    @When("Sorted by {string} icon is present for Lab Details column {string}")
    public void sortedIconIsPresentForLabDetailsOnLabMappingPage(String sortType, String column) {
        AGGridLabSummaryFormLabDetailsFields field = AGGridLabSummaryFormLabDetailsFields.getEnumValue(column);
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().sortIconForColumnIsPresent(field, sortType),
                String.format("Sort %s for column %s should be present", sortType, column));
    }

    @When("Sorted by {string} icon is present for Assay Details column {string}")
    public void sortedIconIsPresentForAssayDetailsOnLabMappingPage(String sortType, String column) {
        AGGridLabSummaryFormAssayDetailsFields field = AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column);
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().sortIconForColumnIsPresent(field, sortType),
                String.format("Sort %s for column %s should be present", sortType, column));
    }

    @Then("Lab details column {string} values sorted by {string}")
    public void labDetailsColumnIsSortedOnLabMappingPage(String column, String sortType) {
        AGGridLabSummaryFormLabDetailsFields field = AGGridLabSummaryFormLabDetailsFields.getEnumValue(column);
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isValuesInColumnSortedBy(field, sortType),
                String.format("Sort %s for column %s should be present", sortType, column));
    }

    @Then("Assay details column {string} values sorted by {string}")
    public void assayDetailsColumnIsSortedOnLabMappingPage(String column, String sortType) {
        AGGridLabSummaryFormAssayDetailsFields field = AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column);
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().isValuesInColumnSortedBy(field, sortType),
                String.format("Sort %s for column %s should be present", sortType, column));
    }

    @When("I open Menu for Lab Details column {string} on Lab Mapping")
    public void openMenuForLabDetailsColumn(String column) {
        AGGridLabSummaryFormLabDetailsFields field = AGGridLabSummaryFormLabDetailsFields.getEnumValue(column);
        labMappingPage.getAGGridLabSummaryForm().openColumnMenu(field);
    }

    @And("I check Filter menu types on Lab Mapping to compare are equals the following list:")
    public void checkFilterItemList(List<String> listItems) {
        labMappingPage.getAGGridLabSummaryForm().openFilterListItem();
        SoftAssert.getInstance().assertEquals(
                labMappingPage.getAGGridLabSummaryForm().getFilterListItem(), listItems, "List with filter types does not correct");
    }

    @And("I check menu options on Lab Mapping to compare are equals the following list:")
    public void checkMenuOptionList(List<String> menuOptions) {
        SoftAssert.getInstance().assertEquals(
                labMappingPage.getAGGridLabSummaryForm().getMenuOptionListItem(), menuOptions, "List with menu options does not correct");
    }

    @When("I select filter type {string} on Lab Mapping")
    public void selectType(String type) {
        TableFilter filter = TableFilter.getEnumValue(type);
        labMappingPage.getAGGridLabSummaryForm().openFilterListItem();
        labMappingPage.getAGGridLabSummaryForm().selectFilterType(filter);
    }

    @When("I select menu option {string} on Lab Mapping")
    public void selectMenuOption(String type) {
        MenuOption option = MenuOption.getEnumValue(type);
        labMappingPage.getAGGridLabSummaryForm().selectMenuOption(option);
    }

    @When("I expand all rows in AG-Grid on Lab Mapping")
    public void expandAllRowsInAgGridOnLabMappingPage() {
        labMappingPage.getAGGridLabSummaryForm().expandAllRows();
    }

    @When("I set value {string} for filter box {string} on Lab Mapping")
    public void setValueForCheckboxFilter(String bool, String box) {
        labMappingPage.getAGGridLabSummaryForm().setFieldValueColumnCheckboxFilter(box, Boolean.parseBoolean(bool));
        scenarioContext.add(box, box);
    }

    @And("I fill value {string} in filter body input on Lab Mapping")
    public void setUpFilter(String value) {
        labMappingPage.getAGGridLabSummaryForm().setFieldValueInputColumnFilter(value);
    }

    @Then("Refresh page if grid is empty")
    public void refreshSearch() {
        labMappingPage.getAGGridLabSummaryForm().refreshSearchIfTableIsEmpty();
    }

    @Then("Lab mapping table is empty")
    public void isTableEmpty() {
        Assert.assertTrue(labMappingPage.getAGGridLabSummaryForm().tableIsEmpty(), "Table should be empty");
    }

    @Then("I read data with filter {string} for Lab details column {string} on Lab Mapping")
    public void readDataForFilterLabColumn(String filter, String column) {
        List<String> valuesFromColumn = labMappingPage.getAGGridLabSummaryForm().getStringListValuesFromColumn(AGGridLabSummaryFormLabDetailsFields.getEnumValue(column));
        String value = scenarioContext.get(filter);
        valuesFromColumn.forEach(valueFromColumn -> SoftAssert.getInstance().assertTrue(
                valueFromColumn.equals(value), String.format("Value from column %s should equals filter value %s", column, value)));
    }

    @Then("I read data with filter {string} for Assay details column {string} on Lab Mapping")
    public void readDataForFilterAssayColumn(String filter, String column) {
        List<String> valuesFromColumn = labMappingPage.getAGGridLabSummaryForm().getStringListValuesFromColumn(AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column));
        String value = scenarioContext.get(filter);
        valuesFromColumn.forEach(valueFromColumn -> SoftAssert.getInstance().assertTrue(
                valueFromColumn.contains(value), String.format("Value from column %s should equals filter value - %s", column, value)));
    }

    @Then("I read data in Assay details column {string} on Lab Mapping and values equals {string}")
    public void readDataForFilterAssayColumns(String column, String filter) {
        List<String> valuesFromColumn = labMappingPage.getAGGridLabSummaryForm().getStringListValuesFromColumn(AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column));
        valuesFromColumn.forEach(valueFromColumn -> SoftAssert.getInstance().assertTrue(
                valueFromColumn.equals(filter), String.format("Value from column %s should equals filter value - %s", column, filter)));
    }

    @Then("I read data from Lab details column {string} and check values with filter {string} and type {string}")
    public void readDataFromLabColumnAndCheckWithFilter(String column, String filter, String type) {
        List<String> valuesFromColumn = labMappingPage.getAGGridLabSummaryForm().getStringListValuesFromColumn(AGGridLabSummaryFormLabDetailsFields.getEnumValue(column));
        SoftAssert.getInstance().assertTrue(
                labMappingPage.getAGGridLabSummaryForm().readColumnAndCompareWithFilter(type, valuesFromColumn, filter),
                String.format("Values from column %s not correct in %s type", column, type));
    }

    @Then("I read data from Assay details column {string} and check values with filter {string} and type {string}")
    public void readDataFromAssayColumnAndCheckWithFilter(String column, String filter, String type) {
        List<String> valuesFromColumn = labMappingPage.getAGGridLabSummaryForm().getStringListValuesFromColumn(AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column));
        AqualityServices.getLogger().info(String.format("Values from column %s: ", column) + valuesFromColumn);
        SoftAssert.getInstance().assertTrue(
                labMappingPage.getAGGridLabSummaryForm().readColumnAndCompareWithFilter(type, valuesFromColumn, filter),
                String.format("Values from column %s not correct in %s type", column, type));
    }

    @Then("columns with following titles should be displayed in AG-Grid Lab summary on Lab Mapping page:")
    public void columnsWithFollowingTitlesShouldBeDisplayedInAGGridLabSummaryOnLabMappingPage(List<String> columns) {
        columns.forEach(column -> {
            String actualTitle = labMappingPage.getAGGridLabSummaryForm()
                    .getColumnTitle(AGGridLabSummaryFormLabDetailsFields.getEnumValue(column));

            SoftAssert.getInstance().assertEquals(actualTitle, column,
                    String.format("Columns with title - %s should be displayed in AG-Grid Lab summary",
                            column));
        });
    }

    @And("I click random Lab from AG-Grid Lab summary and save lab name as {string}")
    public void iClickRandomLabFromAGGridLabSummaryAndSaveLabNameAsLab(String key) {
        String labName = labMappingPage.getAGGridLabSummaryForm().getRandomLabName();
        scenarioContext.add(key, labName);
        labMappingPage.getAGGridLabSummaryForm().clickByContractedLab(labName);
    }

    @Then("Values for {string} column should be the same as {string} label for Lab {string}")
    public void valuesForColumnShouldBeTheSameAsDMSLabelForLabLab(String columnName, String marker, String key) {
        List<String> values = labMappingPage.getAGGridLabSummaryForm()
                .getStringListValuesFromColumn(AGGridLabSummaryFormLabDetailsFields.getEnumValue(columnName));

        String labName = scenarioContext.get(key);
        String labMsValue = labMappingPage.getAGGridLabSummaryForm().getMSValueForLab(labName, marker);
        SoftAssert.getInstance().assertTrue(values.stream().allMatch(dmsValue -> dmsValue.equals(labMsValue)),
                String.format("Values for %s column should be the same as %s label for Lab %s",
                        columnName, marker, labName));
    }

    @Then("Value for field {string} should be empty in AG-Grid Lab summary on Lab Mapping page")
    public void valueForFieldVariantsIncludedForAssayShouldBeEmptyInAGGridLabSummaryOnLabMappingPage(String field) {
        String actualValue = labMappingPage.getAGGridLabSummaryForm()
                .getColumnFieldValue(AGGridLabSummaryFormAssayDetailsFields.getEnumValue(field));

        Assert.assertTrue(actualValue.isEmpty(),
                "Value for field {string} should be empty in AG-Grid Lab summary on Lab Mapping page");
    }

    @And("Values {string} for Lab {string} should be {string}")
    public void valuesDiseaseMarketShareForLabLabShouldBe(String marker, String key, String expectedValue) {
        Lab lab = scenarioContext.get(key);
        String actualValue = labMappingPage.getAGGridLabSummaryForm().getMSValueForLab(lab.getName(), marker);
        Assert.assertEquals(actualValue, expectedValue,
                String.format("Values %s for Lab %s should be %s", marker, lab.getName(), expectedValue));
    }

    @And("Column {string} has value {string} in AG-Grid Lab summary on Lab Mapping page")
    public void columnHasValueInAGGridLabSummaryOnLabMappingPage(String column, String expectedValue) {
        String actualValue = labMappingPage.getAGGridLabSummaryForm()
                .getColumnFieldValue(AGGridLabSummaryFormAssayDetailsFields.getEnumValue(column));

        SoftAssert.getInstance().assertEquals(
                actualValue,
                expectedValue,
                String.format("Value %s for %s is not correct on Lab Mapping Results page",
                        actualValue, column));
    }

    @Then("I get number from {string} heading banner on Lab Mapping page and save to {string}")
    public void getTNumberFromHeadingBannerOnLabMappingPage(String banner, String key) {
        Integer number = labMappingPage.getNumberFromHeadingBanner(banner);
        scenarioContext.add(key, number);
    }

    @Then("Project name from Data summary panel is {string} on Lab Mapping page")
    public void getProjectFromDataSummaryPanelOnLabMappingPage(String project) {
        Assert.assertEquals(labMappingPage.getProjectFromDataSummaryPanel(), project,
                "Project on Data summary panel is not correct on Lab Mapping page");
    }


    @And("Link {string} is not present on Lab Mapping page")
    public void linkIsNotPresentOnLabMappingPage(String link) {
        Assert.assertFalse(labMappingPage.isLinkDisplayed(link),
                String.format("Link %s should be not present on Lab Mapping page", link));
    }

    @Then("Saved in {string} total number of labs that have assays that detect biomarker are the same as in {string} chart")
    public void totalNumberOfLabsAreTheSame(String label, String subtitle) {
        String fromLabel = scenarioContext.get(label);
        String fromSubtitle = scenarioContext.get(subtitle);
        Assert.assertEquals(fromLabel, fromSubtitle);
    }

    @Then("I get number of heading banner {string} and compare with saved value {string} on Lab Mapping page")
    public void getNumberOfHeadingBannerAndCompareWithSavedValueOnLabMappingPage(String fromUI, String key) {
        Integer numberFromUI = scenarioContext.get(fromUI);
        Integer savedNumber = scenarioContext.get(key);
        Assert.assertEquals(numberFromUI, savedNumber, String.format("Number should be equal from UI %S and from service %s", numberFromUI, savedNumber));
    }

    @Then("{string} must be the same as a total number of labs on {string} chart on Charts form on Lab Mapping page")
    public void numberOfLabsMustBeTheSameAsATotalNumberOfLabsOnChartOnChartsFormOnLabMappingPage(
            String key, String chartName) {
        Integer numberOfLabs = scenarioContext.get(key);
        Integer numberOfLabsFromChart = labMappingPage.getLabMappingChartsForm()
                .getTotalCountOfLabsFromChartSubTitle(LabMappingCharts.getEnumValue(chartName));

        Assert.assertEquals(numberOfLabs, numberOfLabsFromChart,
                String.format("Number of labs that have assays that detect biomarker not equal number of labs in %s chart",
                        chartName));
    }

    @And("{string} was increment in one after new lab was created on Lab Mapping page")
    public void numberOfLabsWasIncrementInOneAfterNewLabWasCreatedOnLabMappingPage(String key) {
        Integer beforeNumberOfLabs = scenarioContext.get(key);
        Integer actualNumberOfLabs = labMappingPage.getNumberFromHeadingBanner(AGGridHeadingIcons.LABS.getIconName());
        Integer expectedNumberOfLabs = ++beforeNumberOfLabs;
        Assert.assertEquals(actualNumberOfLabs, expectedNumberOfLabs,
                "Number of labs should be increment in one after new lab was created on Lab Mapping page");
    }

    @And("Icon {string} is displayed on heading panel on Lab Mapping page")
    public void iconIsDisplayedOnLabMappingPage(String icon) {
        Assert.assertTrue(labMappingPage.isIconDisplayed(icon), String.format("Icon %s should be displayed", icon));
    }

    @When("I click by {string} pie sector in chart {string} on Lab Mapping page")
    public void iClickByPieSectorInChartOnLabMappingPage(String color, String chart) {
        labMappingPage.getLabMappingChartsForm().clickOnPieByColor(
                LabMappingCharts.getEnumValue(chart),
                ChartColor.getEnumValue(color));
    }

    @When("I get number of labs for {string} legend from Pie chart {string} save as {string}")
    public void iGetNumberOfLabsForLegendFromPieChartSaveAsLabs(String legend, String chart, String key) {
        String numberOfLabs = labMappingPage.getLabMappingChartsForm().getNumberOfLabForLegendFromPieChart(
                LabMappingCharts.getEnumValue(chart),
                LabMappingPieChartLegends.getEnumValue(legend));

        scenarioContext.add(key, numberOfLabs);
    }

    @When("I get percents for following legend from Pie chart {string} save as {string}:")
    public void iGetPercentsForFollowingLegendFromSaveAs(String chart, String key, List<String> legends) {
        String sumPercent = FormatValue.DMS.format(String.valueOf(
                legends.stream().mapToDouble(legend ->
                        labMappingPage.getLabMappingChartsForm()
                                .getPercentForLegendFromPieChart(
                                        LabMappingCharts.getEnumValue(chart),
                                        LabMappingPieChartLegends.getEnumValue(legend)))
                        .sum()));

        scenarioContext.add(key, sumPercent);
    }

    @When("I clear following fields on Edit settings form on Lab Mapping page")
    public void iClearFollowingFieldsOnEditSettingsFormOnLabMappingPage(List<String> fields) {
        fields.forEach(field -> labMappingPage.getEditSettingsForm().clearField(LabMappingEditSettingsFormFields.getEnumValue(field)));
    }

    @Then("Message {string} is displayed on Edit settings form on Lab Mapping")
    public void messageIsDisplayedOnEditSettingsFormOnLabMapping(String message) {
        Assert.assertTrue(labMappingPage.getEditSettingsForm().isAlertMessageDisplayed(message),
                String.format("Message %s should be displayed on Edit settings form on Lab Mapping", message));
    }

    @And("Message {string} is displayed on required fields on Edit settings form on Lab Mapping")
    public void messageIsDisplayedOnRequiredFieldsOnEditSettingsFormOnLabMapping(String message) {
        Assert.assertTrue(labMappingPage.getEditSettingsForm().isMessageDisplayedOnRequiredFields(message),
                String.format("Message %s should be displayed on required fields on Edit settings form on Lab Mapping",
                        message));
    }
}
