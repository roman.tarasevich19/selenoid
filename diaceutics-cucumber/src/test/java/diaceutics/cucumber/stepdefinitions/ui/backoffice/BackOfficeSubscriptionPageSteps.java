package diaceutics.cucumber.stepdefinitions.ui.backoffice;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.backoffice.pageobject.pages.BackOfficeSubscriptionPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import javax.inject.Inject;

public class BackOfficeSubscriptionPageSteps {

    private final BackOfficeSubscriptionPage backOfficeSubscriptionPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public BackOfficeSubscriptionPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        backOfficeSubscriptionPage = new BackOfficeSubscriptionPage();
    }

    @Then("BackOffice subscriptions page is opened")
    public void backOfficeMainPageIsOpened(){
        Assert.assertTrue(backOfficeSubscriptionPage.isDisplayed(), "Back office subscriptions page is not opened");
    }

    @When("I click {string} link on BackOffice Subscription page")
    public void iClickLinkOnBackOfficeMainPage(String link){
        backOfficeSubscriptionPage.clickByLink(link);
    }
}
