package diaceutics.cucumber.stepdefinitions.ui.marketplace.collaborations;

import diaceutics.selenium.pageobject.forms.marketplace.CollaborationHeaderForm;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class CollaborationHeaderFormSteps {

    private final CollaborationHeaderForm collaborationHeaderForm;

    public CollaborationHeaderFormSteps() {
        collaborationHeaderForm = new CollaborationHeaderForm();
    }

    @When("I click {string} link on Collaboration Header form")
    public void iClickHomeOnCollaborationHeader(String linkName) {
        collaborationHeaderForm.clickByLink(linkName);
    }

    @And("{string} link is displayed on Collaboration Header form")
    public void linkIsDisplayedOnCollaborationHeaderForm(String linkName) {
        Assert.assertTrue(collaborationHeaderForm.isLinkDisplayed(linkName));
    }
}
