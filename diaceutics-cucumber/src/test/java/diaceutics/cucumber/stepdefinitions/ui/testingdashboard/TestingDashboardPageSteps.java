package diaceutics.cucumber.stepdefinitions.ui.testingdashboard;

import aquality.selenium.browser.AqualityServices;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.selenium.enums.charts.TestingDashboardCharts;
import diaceutics.selenium.enums.pagefields.EditSettingsFormFields;
import diaceutics.selenium.enums.pagefields.testingdashboard.TestingDashboardPageFields;
import diaceutics.selenium.models.SearchFilter;
import diaceutics.selenium.models.TestingDashboardExcelTable;
import diaceutics.selenium.models.UsState;
import diaceutics.selenium.pageobject.pages.testingdashboard.TestingDashboardPage;
import diaceutics.selenium.utilities.PDFUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class TestingDashboardPageSteps {

    private final TestingDashboardPage testingDashboardPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public TestingDashboardPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        testingDashboardPage = new TestingDashboardPage();
    }

    @Then("Testing Dashboard page is opened")
    public void testingDashboardIsOpened() {
        Assert.assertTrue(testingDashboardPage.isDisplayed(),
                "Testing Dashboard page should be opened");
    }

    @And("I return back to TD main page")
    public void returnBack(){
        testingDashboardPage.returnBackToMainPage();
    }

    @And("Search filter {string} with following fields is displayed on Testing Dashboard page")
    public void searchFilterWithFollowingFieldsIsDisplayedOnTestingDashboardPage(String key, List<String> fields) {
        SearchFilter filter = scenarioContext.get(key);
        filter.setDateRange();
        fields.forEach(field -> {
            String actualValue = testingDashboardPage.getFieldValue(TestingDashboardPageFields.getEnumValue(field));
            String expectedValue = filter.getReflectionFieldValue(TestingDashboardPageFields.getEnumValue(field).getModelField());

            SoftAssert.getInstance().assertEquals(
                    actualValue,
                    expectedValue,
                    String.format("Value - %s for field - %s is not correct on Testing Dashboard page",
                            actualValue, field));
        });
    }

    @When("I click {string} on Testing Dashboard page")
    public void iClickEditSearchOnTestingDashboardPage(String buttonName) {
        testingDashboardPage.clickByButton(buttonName);
    }

    @Then("Edit settings form is opened on Testing Dashboard page")
    public void editSettingsFormIsOpenedOnTestingDashboardPage() {
        Assert.assertTrue(testingDashboardPage.getEditSettingsForm().isDisplayed(),
                "Edit settings form on Testing Dashboard page should be opened");
    }

    @When("I fill following fields on Edit settings form on Testing Dashboard page and save {string}:")
    public void iFillFollowingFieldsOnEditSettingsFormOnTestingDashboardPageAndSaveSearchFilter(
            String key, Map<String, String> data) {
        SearchFilter filter = new SearchFilter();
        data.forEach((field, value) -> {
            String selectedValue = testingDashboardPage.getEditSettingsForm().setFieldValue(
                    EditSettingsFormFields.getEnumValue(field), value);

            filter.setReflectionFieldValue(
                    EditSettingsFormFields.getEnumValue(field).getModelField(), selectedValue);
        });

        scenarioContext.add(key, filter);
    }

    @And("I click {string} on Edit settings form on Testing Dashboard")
    public void iClickApplyOnEditSettingsFormOnTestingDashboard(String buttonName) {
        testingDashboardPage.getEditSettingsForm().clickByButton(buttonName);
    }

    @When("I click Download all charts on Charts form on Testing Dashboard page")
    public void iClickDownloadAllChartsOnChartsFormOnLabMappingPage() {
        testingDashboardPage.getTestingDashboardChartsForm().clickDownloadAllCharts();
    }

    @When("I click Download chart {string} on Charts form on Testing Dashboard page")
    public void iClickDownloadChartChartNameOnChartsFormOnTestingDashboardPage(String chartName) {
        testingDashboardPage.getTestingDashboardChartsForm()
                .clickDownloadAChart(TestingDashboardCharts.getEnumValue(chartName));
    }

    @When("I save title to {string} from chart {string} on Testing Dashboard page")
    public void iSaveTitleFromChart(String key, String chartName) {
        String chartTitle;
        if (chartName.equals(TestingDashboardCharts.COUNTRY_MAP_CHART.getFriendlyName())){
            chartTitle = testingDashboardPage.getTestingDashboardChartsForm()
                    .getHighchartsTitle(TestingDashboardCharts.COUNTRY_PIE_CHART);
        } else {
            chartTitle = testingDashboardPage.getTestingDashboardChartsForm()
                    .getHighchartsTitle(TestingDashboardCharts.getEnumValue(chartName));
        }
        scenarioContext.add(key, chartTitle);
    }

    @When("I save subtitle to {string} from chart {string} on Testing Dashboard page")
    public void iSaveSubTitleFromChart(String key, String chartName) {
        String chartSubTitle = testingDashboardPage.getTestingDashboardChartsForm()
                .getChartSubTitle(TestingDashboardCharts.getEnumValue(chartName));
        scenarioContext.add(key, chartSubTitle);
    }

    @When("I save titles to {string} from all charts on Testing Dashboard page")
    public void iSaveTitlesFromAllChartTestingDashboardPage(String key) {
        List<String> titles = new ArrayList<>();
        for (TestingDashboardCharts chart : TestingDashboardCharts.values()) {
            if (chart.isTitle()) {
                String title = testingDashboardPage.getTestingDashboardChartsForm()
                        .getHighchartsTitle(TestingDashboardCharts.getEnumValue(chart.getFriendlyName()));
                titles.add(title);
            }
        }
        scenarioContext.add(key, titles);
    }

    @When("I save subtitles to {string} from all charts on Testing Dashboard page")
    public void iSaveSubTitlesFromAllChartTestingDashboardPage(String key) {
        List<String> subTitles = new ArrayList<>();
        for (TestingDashboardCharts chart : TestingDashboardCharts.values()) {
            if(chart.isSubtitle()){
                String subtitle = testingDashboardPage.getTestingDashboardChartsForm()
                        .getChartSubTitle(TestingDashboardCharts.getEnumValue(chart.getFriendlyName()));
                subTitles.add(subtitle);
            }
        }
        scenarioContext.add(key, subTitles);
    }

    @And("File {string} contains all charts values from {string} on Testing Dashboard page")
    public void fileContainsAChartsWithListTitlesOnTestingDashboard(String fileName, String key) {
        List<String> chartTitlesFromPage = scenarioContext.get(key);
        String textFromPdfFile = PDFUtil.parsePDF(fileName).replaceAll("[\\n\\r?]+", " ").replaceAll("[\\s]{2,}", " ");
        AqualityServices.getLogger().info("Get from PDF: " + textFromPdfFile);
        chartTitlesFromPage.forEach(chart -> Assert.assertTrue(textFromPdfFile.contains(chart), String.format("Chart with text - %s should be in file", chart)));
    }

    @When("I click Export data chart {string} on Charts form on Testing Dashboard page")
    public void iClickExportDataChartNameOnChartsFormOnTestingDashboardPage(String chartName) {
        testingDashboardPage.getTestingDashboardChartsForm()
                .clickExportDataChart(TestingDashboardCharts.getEnumValue(chartName));
    }

    @When("I read tooltips from Map chart and save as {string}")
    public void getToolTipsFromUI(String key) {
        List<UsState> toolTipsFromUI = testingDashboardPage.getTestingDashboardChartsForm().getUsToolTipsFromUI();
        scenarioContext.add(key, toolTipsFromUI);
    }

    @Then("I get tooltips from UI from saved file {string} and compare with Excel file {string}")
    public void compareTooltipsUIAndExcel(String key, String excel){
        List<UsState> savedToolTipsFromUI = scenarioContext.get(key);
        TestingDashboardExcelTable testingDashboardExcelTable = new TestingDashboardExcelTable(excel);
        List<UsState> toolTipsFromExcel = testingDashboardExcelTable.getAllListStates();
        savedToolTipsFromUI.sort(Comparator.comparing(UsState::getTitle).reversed());
        toolTipsFromExcel.sort(Comparator.comparing(UsState::getTitle).reversed());
        for(int i = 0; i < 51; i++){
            Assert.assertEquals(savedToolTipsFromUI.get(i), toolTipsFromExcel.get(i),
                    "Data does not equal! On UI: " + savedToolTipsFromUI.get(i) + " and in excel: " + toolTipsFromExcel.get(i));
        }
    }

    @And("File {string} contains a chart on Testing Dashboard with text {string} excluding digits")
    public void fileContainsAChartWithTitleExcludingDigits(String fileName, String key) {
        String chartTitleFromPage = scenarioContext.get(key);
        String textFromPdfFile = PDFUtil.parsePDF(fileName).replaceAll("[\\n\\r?]+", " ").replaceAll("[0-9]", "");
        Assert.assertTrue(textFromPdfFile.contains(chartTitleFromPage),
                String.format("Chart with text - %s should be in file", chartTitleFromPage));
    }

    @And("File {string} contains a chart on Testing Dashboard with text {string}")
    public void fileContainsAChartWithTitle(String fileName, String key) {
        String chartTitleFromPage = scenarioContext.get(key);
        String textFromPdfFile = PDFUtil.parsePDF(fileName).replaceAll("[\\n\\r?]+", " ");
        Assert.assertTrue(textFromPdfFile.contains(chartTitleFromPage),
                String.format("Chart with text - %s should be in file", chartTitleFromPage));
    }

    @And("{string} should be the same as a number of labs on Testing Dashboard page")
    public void numberOfLabsMustBeTheSameAsANumberOfLabsOnTestingDashboardPage(String key) {
        Integer expectedNumberOfLabs = scenarioContext.get(key);
        Integer actualNumberOfLabs = testingDashboardPage.getNumberOfLabs();
        Assert.assertEquals(actualNumberOfLabs, expectedNumberOfLabs,
                String.format("Number of labs should be %s on Testing Dashboard page", expectedNumberOfLabs));
    }
}
