package diaceutics.cucumber.stepdefinitions;

import aquality.selenium.browser.AqualityServices;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.selenium.models.User;
import diaceutics.selenium.utilities.MailUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import javax.inject.Inject;
import javax.mail.MessagingException;

public class MailSteps {

    private final ScenarioContext scenarioContext;

    @Inject
    public MailSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
    }

    @Then("I get a mail for the {string} with a subject {string}")
    public void iGetAMailForTheUserWithASubjectVerifyYourEmail(String key, String subject) {
        User user = scenarioContext.get(key);
        Assert.assertTrue(MailUtil.isMailWithSubjectExist(user.getEmail(), subject),
                String.format("Mail with subject %s should be exist", subject));
    }

    @And("I open Verify Link from {string} mail with subject {string} and confirm registration")
    public void iOpenVerifyLinkFromUserMail(String key, String subject) {
        User user = scenarioContext.get(key);
        String url = MailUtil.getLinkFromMailWithSubject(user.getEmail(), subject);
        AqualityServices.getBrowser().goTo(url);
    }

    @And("I open link from gmail mail {string} for setting new password")
    public void readGmailMail(String key){
        User user = scenarioContext.get(key);
        String url = MailUtil.getUrlFromGmailMail(user.getEmail(), user.getPassword());
        AqualityServices.getBrowser().goTo(url);
    }
}
