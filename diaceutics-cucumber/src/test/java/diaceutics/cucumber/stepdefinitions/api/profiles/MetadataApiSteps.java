package diaceutics.cucumber.stepdefinitions.api.profiles;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.cucumber.utilities.SoftAssert;
import diaceutics.restassured.utilities.JSONConvertor;
import diaceutics.restassured.project.models.*;
import diaceutics.restassured.project.models.collections.*;
import diaceutics.restassured.project.requests.profiles.Metadata;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;


public class MetadataApiSteps {

    private final Metadata metadata;
    private final ScenarioContext scenarioContext;

    @Inject
    public MetadataApiSteps(ScenarioContext apiStepData) {
        this.scenarioContext = apiStepData;
        metadata = new Metadata();
    }

    @When("I execute request Returns pageable list of all biomarkers with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllBiomarkers(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllBiomarkers(params);
    }

    @When("I execute request Returns pageable list of all classifications with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllClassifications(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllClassifications(params);
    }

    @When("I execute request Returns pageable list of all countries with profile counts for Laboratories with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllCountriesForAGivenProfileType(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllCountriesForAGivenProfileType(params);
    }

    @When("I execute request Returns pageable list of all DetectGermlineSomatic with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllDetects(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllDetectGermlineSomatics(params);
    }

    @And("response includes the countries matches the profile type {string}")
    public void responseIncludesTheLabsMatchesTheProfileType(String profileType) {
        CollectionCountries actualCollection = scenarioContext.response.body().as(CollectionCountries.class);
        List<Country> countries = actualCollection.getContent();
        Assert.assertTrue(countries.stream().allMatch(
                country -> country.getProfileType().equals(profileType)),
                String.format("Response should be includes countries matches the profile type %s", profileType));
    }

    @When("I execute request Returns pageable list of all diseases for a Lab Test with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllDiseasesForALabTestWithParameters(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllDiseases(params);
    }

    @When("I execute request Returns pageable list of all Methods with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllMethodsWithParameters(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllMethods(params);
    }

    @When("I execute request Returns pageable list of all Ontologies with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllOntologiesWithParameters(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllOntologies(params);
    }

    @When("I execute request Returns pageable list of all platform manufacturers and equipment with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllPlatformManufacturersAndEquipmentWithParameters(
            Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfPlatformManufacturers(params);
    }

    @When("I execute request Returns pageable list of all Result Formats with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllResultFormatsWithParameters(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfResultFormats(params);
    }

    @And("response includes the collection biomarkers from file {string}")
    public void responseIncludesTheCollectionBiomarkersFromFileJson(String fileName) {
        CollectionBiomarkers actualCollection = scenarioContext.response.body().as(CollectionBiomarkers.class);
        CollectionBiomarkers expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionBiomarkers.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all Biomarkers from file %s", fileName));
    }

    @And("response includes the classifications from file {string}")
    public void responseIncludesTheClassificationsFromFileJson(String fileName) {
        CollectionClassifications actualCollection = scenarioContext.response.body().as(CollectionClassifications.class);
        CollectionClassifications expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionClassifications.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all classifications from file %s", fileName));
    }

    @And("response includes the diseases from file {string}")
    public void responseIncludesTheDiseasesFromFileJson(String fileName) {
        CollectionDiseases actualCollection = scenarioContext.response.body().as(CollectionDiseases.class);
        CollectionDiseases expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionDiseases.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all diseases from file %s", fileName));
    }

    @And("response includes the detects from file {string}")
    public void responseIncludesTheDetectsFromFileJson(String fileName) {
        CollectionDetects actualCollection = scenarioContext.response.body().as(CollectionDetects.class);
        CollectionDetects expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionDetects.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all detects from file %s", fileName));
    }

    @And("response includes the methods from file {string}")
    public void responseIncludesTheMethodsFromFileJson(String fileName) {
        CollectionMethods actualCollection = scenarioContext.response.body().as(CollectionMethods.class);
        CollectionMethods expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionMethods.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all methods from file %s", fileName));
    }

    @And("response includes the ontologies from file {string}")
    public void responseIncludesTheOntologiesFromFileJson(String fileName) {
        CollectionOntologies actualCollection = scenarioContext.response.body().as(CollectionOntologies.class);
        CollectionOntologies expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionOntologies.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all ontologies from file %s", fileName));
    }

    @And("response includes the all platform manufacturers and equipment from file {string}")
    public void responseIncludesTheAllPlatformManufacturersAndEquipmentFromFileJson(String fileName) {
        CollectionManufacturers actualCollection = scenarioContext.response.body().as(CollectionManufacturers.class);
        CollectionManufacturers expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionManufacturers.class);

        List<Manufacturer> actualManufacturer = actualCollection.getContent();
        List<Manufacturer> expectedManufacturer = expectedCollection.getContent();
        IntStream.range(0, actualManufacturer.size()).forEach(i ->
                SoftAssert.getInstance().assertEquals(actualManufacturer.get(i), expectedManufacturer.get(i),
                        String.format("platform manufacturers with id -%s should be the same", i)));

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all platform manufacturers and equipment from file %s",
                        fileName));
    }

    @And("response includes the all Result Formats from file {string}")
    public void responseIncludesTheAllResultFormatsFromFilePlatformsJson(String fileName) {
        CollectionResultFormats actualCollection = scenarioContext.response.body().as(CollectionResultFormats.class);
        CollectionResultFormats expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionResultFormats.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all Result Formats from file %s", fileName));
    }

    @When("I execute request Returns pageable list of all Scoring Methodologies with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllScoringMethodologiesWithParameters(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllScoringMethodologies(params);
    }

    @And("response includes the all Scoring Methodologies from file {string}")
    public void responseIncludesTheAllScoringMethodologiesFromFile(String fileName) {
        CollectionScoringMethodologies actualCollection = scenarioContext.response.body().as(CollectionScoringMethodologies.class);
        CollectionScoringMethodologies expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionScoringMethodologies.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all Scoring Methodologies from file %s", fileName));
    }

    @When("I execute request Returns pageable list of all Specimen Requirements with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllSpecimenRequirementsWithParameters(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllSpecimenRequirements(params);
    }

    @And("response includes the all Specimen Requirements from file {string}")
    public void responseIncludesTheAllSpecimenRequirementsFromFile(String fileName) {
        CollectionSpecimenRequirements actualCollection = scenarioContext.response.body().as(CollectionSpecimenRequirements.class);
        CollectionSpecimenRequirements expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionSpecimenRequirements.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all Specimen Requirements from file %s", fileName));
    }

    @When("I execute request Returns pageable list of all Testing Purposes with parameters:")
    public void iExecuteRequestReturnsPageableListOfAllTestingPurposesWithParameters(Map<String, String> params) {
        scenarioContext.response = metadata.returnsPageableListOfAllTestingPurposes(params);
    }

    @And("response includes the all Testing Purposes from file {string}")
    public void responseIncludesTheAllTestingPurposesFromFileTestingPurposesJson(String fileName) {
        CollectionTestingPurposes actualCollection = scenarioContext.response.body().as(CollectionTestingPurposes.class);
        CollectionTestingPurposes expectedCollection = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), CollectionTestingPurposes.class);

        Assert.assertEquals(actualCollection, expectedCollection,
                String.format("Response should be includes list of all Testing Purposes from file %s", fileName));
    }

    @When("I execute request Returns list of all Biomarkers for the selected Commercial Assay with Id {int}:")
    public void iExecuteRequestReturnsListOfAllBiomarkersForTheSelectedCommercialAssayWithId(int commercialAssayId) {
        scenarioContext.response = metadata.returnsListOfAllBiomarkersForTheSelectedCommercialAssay(commercialAssayId);
    }

    @And("response includes the biomarkers from file {string}")
    public void responseIncludesTheBiomarkersFromFile(String fileName) {
        BiomarkerDto[] actualBiomarkerDtos = scenarioContext.response.body().as(BiomarkerDto[].class);
        BiomarkerDto[] expectedBiomarkerDtos = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), BiomarkerDto[].class);

        Assert.assertEquals(actualBiomarkerDtos, expectedBiomarkerDtos,
                String.format("Response should be includes list of all Biomarkers from file %s", fileName));
    }

    @When("I execute second request Returns list of all Biomarkers for the selected Commercial Assay with Id {int}:")
    public void iExecuteSecondRequestReturnsListOfAllBiomarkersForTheSelectedCommercialAssayWithId(int commercialAssayId) {
        scenarioContext.response = metadata.returnsListOfAllBiomarkersForTheSelectedCommercialAssaySecondRequest(commercialAssayId);
    }
}
