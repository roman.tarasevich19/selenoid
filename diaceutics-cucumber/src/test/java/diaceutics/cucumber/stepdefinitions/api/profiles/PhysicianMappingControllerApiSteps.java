package diaceutics.cucumber.stepdefinitions.api.profiles;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.restassured.project.models.PhysicianMappingControllerRequest;
import diaceutics.restassured.project.models.PreferredLab;
import diaceutics.restassured.project.requests.profiles.PhysicianMappingController;
import io.cucumber.java.en.And;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PhysicianMappingControllerApiSteps {

    private final PhysicianMappingController physicianMappingController;
    private final ScenarioContext scenarioContext;

    @Inject
    public PhysicianMappingControllerApiSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        physicianMappingController = new PhysicianMappingController();
    }

    @And("I execute the POST request physician-mapping-controller using id from following lab:")
    public void iExecuteThePOSTRequestPhysicianMappingControllerUsingIdFromLabLab(List<String> keys) {
        PhysicianMappingControllerRequest request = new PhysicianMappingControllerRequest();
        List<Integer> labIds = new ArrayList<>();
        keys.forEach(key -> {
            LabDto labDto = scenarioContext.get(key);
            labIds.add(labDto.getId());
        });
        request.setLabIds(labIds);
        scenarioContext.response = physicianMappingController.returnPreferredLabs(request);
    }

    @And("response includes the following labs:")
    public void responseIncludesTheFollowingLabs(List<String> keys) {
        PreferredLab[] collectionPreferredLabs = scenarioContext.response.body().as(PreferredLab[].class);
        List<Integer> labIds = new ArrayList<>();
        Arrays.asList(collectionPreferredLabs).forEach(lab -> labIds.add(lab.getLabId()));

        keys.forEach(key -> {
            LabDto labDto = scenarioContext.get(key);
            Integer labId = labDto.getId();
            Assert.assertTrue(labIds.contains(labId),
                    String.format("Response should be includes lab with id - %s", labId));
        });
    }
}
