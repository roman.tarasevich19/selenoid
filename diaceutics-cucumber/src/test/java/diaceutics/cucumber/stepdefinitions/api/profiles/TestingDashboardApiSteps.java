package diaceutics.cucumber.stepdefinitions.api.profiles;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.project.models.LabDto;
import diaceutics.restassured.project.models.TestingDashboardVolumeDto;
import diaceutics.restassured.project.models.VolumesFilter;
import diaceutics.restassured.project.requests.profiles.TestingDashboard;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.Arrays;

public class TestingDashboardApiSteps {

    private final TestingDashboard testingDashboard;
    private final ScenarioContext scenarioContext;

    @Inject
    public TestingDashboardApiSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        testingDashboard = new TestingDashboard();
    }

    @When("I execute POST request Returns data for lab testing charts using following data:")
    public void iExecutePOSTRequestReturnsDataForLabTestingChartsUsingFollowingData(@Transpose VolumesFilter request) {
        scenarioContext.response = testingDashboard.returnsDataForLabTestingCharts(request);
    }

    @And("response includes the list of TestingDashboardVolumeDto objects")
    public void responseIncludesTheTestingDashboardVolumeDtoObjects() {
        TestingDashboardVolumeDto[] testingDashboardVolumes =
                scenarioContext.response.body().as(TestingDashboardVolumeDto[].class);

        Assert.assertTrue(testingDashboardVolumes.length > 0,
                "Response should be includes list of TestingDashboardVolumeDto objects");
    }

    @When("I execute POST request Returns data for versus charts using following data:")
    public void iExecutePOSTRequestReturnsDataForVersusChartsUsingFollowingData(@Transpose VolumesFilter request) {
        scenarioContext.response = testingDashboard.returnsDataForVersusCharts(request);
    }

    @And("response includes data with the Lab {string}")
    public void responseIncludesDataWithTheLabLab(String key) {
        LabDto lab = scenarioContext.get(key);
        TestingDashboardVolumeDto[] testingDashboardVolumes =
                scenarioContext.response.body().as(TestingDashboardVolumeDto[].class);

        Assert.assertTrue(Arrays.stream(testingDashboardVolumes).anyMatch(
                testingDashboardVolume -> testingDashboardVolume.getLabId() == lab.getId()),
                String.format("Response should be includes data with the Lab with id - %s", lab.getId()));
    }

    @And("response includes the list of TestingDashboardVolumeDto objects with biomarkerID {int}")
    public void responseIncludesTheListOfTestingDashboardVolumeDtoObjectsWithBiomarkerID(int biomarkerID) {
        TestingDashboardVolumeDto[] testingDashboardVolumes =
                scenarioContext.response.body().as(TestingDashboardVolumeDto[].class);
        Assert.assertTrue(Arrays.stream(testingDashboardVolumes).allMatch(
                testingDashboardVolume -> testingDashboardVolume.getBiomarkerId() == biomarkerID),
                String.format(
                        "Response should be includes the list of TestingDashboardVolumeDto objects with biomarkerID %s",
                        biomarkerID));
    }

    @When("I execute POST request Returns data for claims charts using following data and save {string}:")
    public void iExecutePOSTRequestReturnsDataForClaimsChartsUsingFollowingData(
            String key, @Transpose VolumesFilter request) {
        scenarioContext.response = testingDashboard.returnsDataForClaimsCharts(request);
        scenarioContext.add(key, request);
    }

    @And("And response includes data matches the {string}")
    public void andResponseIncludesDataMatchesTheFilter(String key) {
        VolumesFilter request = scenarioContext.get(key);
        Integer[] responseData = scenarioContext.response.body().as(Integer[].class);
        String minDate = String.format("%s0%s", request.getFromYear(), request.getFromMonth());
        String maxDate = String.format("%s0%s", request.getToYear(), request.getToMonth());
        Assert.assertTrue(Arrays.stream(responseData).allMatch(
                date -> date >= Integer.parseInt(minDate) && date <= Integer.parseInt(maxDate)),
                String.format("Response should be includes data from %s year %s month to %s year %s month",
                        request.getFromYear(), request.getFromMonth(), request.getToYear(), request.getToMonth()));
    }

    @When("I execute POST request Returns data for platform charts using following data:")
    public void iExecutePOSTRequestReturnsDataForPlatformChartsUsingFollowingData(@Transpose VolumesFilter request) {
        scenarioContext.response = testingDashboard.returnsDataForPlatformCharts(request);
    }
}
