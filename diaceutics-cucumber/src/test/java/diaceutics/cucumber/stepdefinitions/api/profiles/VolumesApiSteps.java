package diaceutics.cucumber.stepdefinitions.api.profiles;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.utilities.JSONConvertor;
import diaceutics.restassured.project.models.*;
import diaceutics.restassured.project.models.collections.CollectionVolumesForLab;
import diaceutics.restassured.project.requests.profiles.Volumes;
import diaceutics.restassured.project.requests.token.Token;
import diaceutics.selenium.models.User;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

import java.util.*;
import java.util.stream.Collectors;

public class VolumesApiSteps {

    private final Volumes volumes;
    private final ScenarioContext scenarioContext;

    @Inject
    public VolumesApiSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        volumes = new Volumes();
    }

    @When("I execute the GET request Returns collection volumes for Lab {string}")
    public void iExecuteTheGETReturnsCollectionVolumesForLabLab(String key) {
        LabDto labDto = scenarioContext.get(key);
        Integer labId = labDto.getId();
        scenarioContext.response = volumes.returnsCollectionOfLabVolumes(labId);
    }

    @When("I execute the PUT request Updates an volume {string} for Lab {string} using data from file {string}")
    public void iExecuteThePUTUpdatesAnVolumeVolumeForLabLabUsingFollowingData(
            String volumeKey, String labKey, String fileName) {
        DiseaseVolumeDto diseaseVolumeDto = scenarioContext.get(volumeKey);
        LabDto labDto = scenarioContext.get(labKey);
        String claimQuarterGroupId = diseaseVolumeDto.getClaimQuarterGroupId();
        int originalDiseasePk = diseaseVolumeDto.getDisease().getPk();
        Integer labId = labDto.getId();
        String jsonBody = DataReader.generateStringFromFile(fileName);
        DiseaseVolumeDto updatedVolume = JSONConvertor.convertJSONStringToObject(jsonBody, DiseaseVolumeDto.class);
        updatedVolume.setClaimQuarterGroupId(claimQuarterGroupId);
        updatedVolume.setOriginalDiseasePk(originalDiseasePk);
        scenarioContext.response = volumes.updatesAnExistingVolume(labId, updatedVolume);
    }

    @And("response includes for the Lab {string} following volumes:")
    public void responseIncludesForTheLabLabFollowingVolumes(String labKey, List<String> volumeKeys) {
        LabDto labDto = scenarioContext.get(labKey);
        Integer expectedLabId = labDto.getId();
        CollectionVolumesForLab collectionVolumesForLab = scenarioContext.response.body().as(CollectionVolumesForLab.class);
        List<DiseaseVolumeDto> actualDiseaseVolumeDtos = collectionVolumesForLab.getVolumes();
        volumeKeys.forEach(volumeKey -> {
            DiseaseVolumeDto expectedDiseaseVolumeDto = scenarioContext.get(volumeKey);
            Assert.assertTrue(actualDiseaseVolumeDtos.contains(expectedDiseaseVolumeDto),
                    String.format("Response should be includes Volume with year %s and time period %s",
                            expectedDiseaseVolumeDto.getYear(), expectedDiseaseVolumeDto.getTimePeriodValue()));
        });

        Assert.assertEquals(collectionVolumesForLab.getLabId(),
                expectedLabId,
                String.format("Response should be includes labId %s", expectedLabId));
    }

    @When("I execute the POST request Creates a new lab volume for Lab {string} using data from file {string}")
    public void iExecuteThePOSTRequestCreatesANewLabVolumeForLabLabUsingDataFromFile(String key, String fileName) {
        LabDto labDto = scenarioContext.get(key);
        Integer labId = labDto.getId();
        String jsonBody = DataReader.generateStringFromFile(fileName);
        scenarioContext.response = volumes.createsNewLabVolume(labId, jsonBody);
    }

    @And("response includes the volume from file {string}")
    public void responseIncludesTheVolumeFrom(String fileName) {
        DiseaseVolumeDto actualDiseaseVolumeDto = scenarioContext.response.body().as(DiseaseVolumeDto.class);
        DiseaseVolumeDto expectedDiseaseVolumeDto = JSONConvertor.convertJSONFileToObject(DataReader.readJSONFile(fileName), DiseaseVolumeDto.class);
        Assert.assertEquals(actualDiseaseVolumeDto, expectedDiseaseVolumeDto,
                String.format("Response should be includes Volume with year %s and time period %s",
                        expectedDiseaseVolumeDto.getYear(), expectedDiseaseVolumeDto.getTimePeriodValue()));
    }

    @And("I save from response volume as {string}")
    public void iSaveFromResponseVolumeAsVolume(String key) {
        DiseaseVolumeDto diseaseVolumeDto = scenarioContext.response.body().as(DiseaseVolumeDto.class);
        scenarioContext.add(key, diseaseVolumeDto);
    }

    @When("I execute the request DELETE Volume {string} for Lab {string}")
    public void iExecuteTheDELETEVolumeForLabLab(String volumeKey, String labKey) {
        DiseaseVolumeDto diseaseVolumeDto = scenarioContext.get(volumeKey);
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        String claimQuarterGroupId = diseaseVolumeDto.getClaimQuarterGroupId();
        List<String> claimQuarterGroupIds = diseaseVolumeDto.getBiomarkerVolumes().stream()
                .map(BiomarkerVolume::getClaimQuarterGroupId).collect(Collectors.toList());

        List<BiomarkerVolumeDelete> biomarkerVolumes = new ArrayList<>();
        claimQuarterGroupIds.forEach(quarterGroupId -> {
            BiomarkerVolumeDelete biomarkerVolumeDelete = new BiomarkerVolumeDelete();
            biomarkerVolumeDelete.setClaimQuarterGroupId(quarterGroupId);
            biomarkerVolumes.add(biomarkerVolumeDelete);
        });

        DeleteVolume deleteVolume = new DeleteVolume(biomarkerVolumes, claimQuarterGroupId);
        scenarioContext.response = volumes.deleteVolumeForLab(labId, deleteVolume);
    }

    @And("response not includes Volume {string}")
    public void responseNotIncludesVolume(String key) {
        CollectionVolumesForLab collectionVolumesForLab = scenarioContext.response.body().as(CollectionVolumesForLab.class);
        List<DiseaseVolumeDto> diseaseVolumeDtos = collectionVolumesForLab.getVolumes();
        DiseaseVolumeDto diseaseVolumeDto = scenarioContext.get(key);
        Assert.assertFalse(diseaseVolumeDtos.contains(diseaseVolumeDto),
                String.format("Response includes Volume with year %s and time period %s",
                        diseaseVolumeDto.getYear(), diseaseVolumeDto.getTimePeriodValue()));
    }

    @When("I execute POST Returns LabVolumesInternalDto objects using following data:")
    public void iExecutePOSTReturnsLabVolumesInternalDtoObjectsUsingFollowingData(@Transpose VolumesFilter volumesFilter) {
        scenarioContext.response = volumes.returnsLabVolumesInternalDto(volumesFilter);
    }

    @And("response includes the list of LabVolumesInternalDto objects")
    public void responseIncludesTheListOfLabVolumesInternalDtoObjects() {
        LabVolumesInternalDto[] labVolumesInternal =
                scenarioContext.response.body().as(LabVolumesInternalDto[].class);

        Assert.assertTrue(labVolumesInternal.length > 0,
                "Response should be includes list of LabVolumesInternalDto objects");
    }

    @When("I execute the POST request Creates volume for {string} for user {string} using data from file {string}")
    public void iExecuteThePOSTRequestCreatesVolumeForLabForUserUsingDataFromFile(
            String labKey, String userKey, String fileName) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        String jsonBody = DataReader.generateStringFromFile(fileName);
        scenarioContext.response = volumes.createsNewLabVolume(labId, jsonBody, token);
    }

    @When("I execute the GET Returns collection volumes for Lab {string} for user {string}")
    public void iExecuteTheGETReturnsCollectionVolumesForLabLabForUser(String labKey, String userKey) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        scenarioContext.response = volumes.returnsCollectionOfLabVolumes(labId, token);
    }

    @When("I execute the PUT request Updates volume {string} for {string} for user {string} using data from file {string}")
    public void iExecuteThePUTRequestUpdatesVolumeForLabForUserUsingDataFromFile(
            String volumeKey, String labKey, String userKey, String fileName) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        DiseaseVolumeDto diseaseVolumeDto = scenarioContext.get(volumeKey);
        LabDto labDto = scenarioContext.get(labKey);
        String claimQuarterGroupId = diseaseVolumeDto.getClaimQuarterGroupId();
        int originalDiseasePk = diseaseVolumeDto.getDisease().getPk();
        Integer labId = labDto.getId();
        String jsonBody = DataReader.generateStringFromFile(fileName);
        DiseaseVolumeDto updatedVolume = JSONConvertor.convertJSONStringToObject(jsonBody, DiseaseVolumeDto.class);
        updatedVolume.setClaimQuarterGroupId(claimQuarterGroupId);
        updatedVolume.setOriginalDiseasePk(originalDiseasePk);
        scenarioContext.response = volumes.updatesAnExistingVolume(labId, updatedVolume, token);
    }

    @When("I execute the request DELETE volume {string} for {string} for user {string}")
    public void iExecuteTheRequestDELETEVolumeForLabForUser(String volumeKey, String labKey, String userKey) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        DiseaseVolumeDto diseaseVolumeDto = scenarioContext.get(volumeKey);
        LabDto labDto = scenarioContext.get(labKey);
        String claimQuarterGroupId = diseaseVolumeDto.getClaimQuarterGroupId();
        Integer labId = labDto.getId();
        List<String> claimQuarterGroupIds = diseaseVolumeDto.getBiomarkerVolumes().stream()
                .map(BiomarkerVolume::getClaimQuarterGroupId).collect(Collectors.toList());

        List<BiomarkerVolumeDelete> biomarkerVolumes = new ArrayList<>();
        claimQuarterGroupIds.forEach(quarterGroupId -> {
            BiomarkerVolumeDelete biomarkerVolumeDelete = new BiomarkerVolumeDelete();
            biomarkerVolumeDelete.setClaimQuarterGroupId(quarterGroupId);
            biomarkerVolumes.add(biomarkerVolumeDelete);
        });

        DeleteVolume deleteVolume = new DeleteVolume(biomarkerVolumes, claimQuarterGroupId);
        scenarioContext.response = volumes.deleteVolumeForLab(labId, deleteVolume, token);
    }

    @When("I execute the GET request Return volume for Lab {string} by parameters:")
    public void iExecuteTheGETReturnVolumeForLabLabByParameters(String labKey, Map<String, String> params) {
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        scenarioContext.response = volumes.getReturnVolumeForLab(labId, params);
    }

    @And("response includes Volume {string}")
    public void responseIncludesVolumeVolume(String volumeKey) {
        DiseaseVolumeDto expectedVolume = scenarioContext.get(volumeKey);
        DiseaseVolumeDto actualVolume = scenarioContext.response.body().as(DiseaseVolumeDto.class);
        Assert.assertEquals(actualVolume, expectedVolume,
                String.format("Response should be includes Volume with year %s and time period %s",
                        expectedVolume.getYear(), expectedVolume.getTimePeriodValue()));
    }

    @When("I execute the GET request Return volume for Lab {string} for user {string} by parameters:")
    public void iExecuteTheGETRequestReturnVolumeForLabForByParameters(String labKey, String userKey, Map<String, String> params) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        scenarioContext.response = volumes.getReturnVolumeForLab(labId, params, token);
    }
}
