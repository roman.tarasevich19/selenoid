package diaceutics.cucumber.stepdefinitions.api.physicianmapping;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.utilities.JSONConvertor;
import diaceutics.restassured.project.models.*;
import diaceutics.restassured.project.requests.physicianmapping.PhysicianMapping;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;

public class PhysicianMappingApiSteps {

    private final PhysicianMapping physicianMapping;
    private final ScenarioContext scenarioContext;

    @Inject
    public PhysicianMappingApiSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        physicianMapping = new PhysicianMapping();
    }

    @When("I execute GET request Returns a collection of subscriptions for the authenticated principle")
    public void iExecuteRequestReturnsACollectionOfSubscriptionsForTheAuthenticatedPrinciple() {
        scenarioContext.response = physicianMapping.returnsCollectionSubscriptionsForTheAuthenticatedPrinciple();
    }

    @And("response includes the projects from file {string}")
    public void responseIncludesTheProjectsForUser(String fileName) {
        SubscriptionDto[] actualSubscriptionDtos = scenarioContext.response.body().as(SubscriptionDto[].class);
        SubscriptionDto[] expectedSubscriptionDtos = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), SubscriptionDto[].class);

        Assert.assertEquals(actualSubscriptionDtos, expectedSubscriptionDtos,
                String.format("Response should be includes list of all Projects from file %s", fileName));
    }

    @When("I execute POST request Generates a physician mapping using following data:")
    public void iExecutePOSTRequestGeneratesAPhysicianMappingUsingFollowingData(@Transpose PhysicianMappingRequest filter) {
        scenarioContext.response = physicianMapping.generatesPhysicianMapping(filter);
    }

    @And("response includes the list of Physician")
    public void responseIncludesTheListOfPhysician() {
        PhysicianMappingResponse physicianMappingResponse = scenarioContext.response.body().as(PhysicianMappingResponse.class);
        List<Physician> physicians = physicianMappingResponse.getPhysicians();
        Assert.assertTrue(physicians.size() > 0, "Response should be includes list of Physician");
    }

    @And("save from response number of physicians as {string}")
    public void saveFromResponseNumberOfLabsAs(String key) {
        PhysicianMappingResponse physicianMappingResponse = scenarioContext.response.body().as(PhysicianMappingResponse.class);
        List<Physician> physicians = physicianMappingResponse.getPhysicians();
        scenarioContext.add(key, physicians.size());
    }

    @And("save from response number of patient tests physician mapping as {string}")
    public void saveFromResponseNumberOfPatientTestsPhysicianMappingAs(String key) {
        PhysicianMappingResponse physicianMappingResponse = scenarioContext.response.body().as(PhysicianMappingResponse.class);
        int patientTests = physicianMappingResponse.getNumberOfPatientTests();
        scenarioContext.add(key, patientTests);
    }
}
