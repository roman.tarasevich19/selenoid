package diaceutics.cucumber.stepdefinitions.api.profiles;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.utilities.JSONConvertor;
import diaceutics.restassured.project.enums.ApiLabFields;
import diaceutics.restassured.project.models.*;
import diaceutics.restassured.project.models.collections.CollectionAssays;
import diaceutics.restassured.project.models.collections.CollectionLabs;
import diaceutics.restassured.project.requests.profiles.Labs;
import diaceutics.restassured.project.requests.token.Token;
import diaceutics.selenium.models.User;
import diaceutics.selenium.utilities.StringUtil;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LabsApiSteps {

    private final Labs labs;
    private final ScenarioContext scenarioContext;

    @Inject
    public LabsApiSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        labs = new Labs();
    }

    @Then("I execute the POST request Creates a new lab {string} using following data:")
    public void iCreateLadUsingFollowingData(String key, @Transpose LabDto labDto) {
        scenarioContext.add(key, labDto);
        scenarioContext.response = labs.createLab(labDto);
    }

    @Then("the status code is {int}")
    public void theStatusCodeIs(int statusCode) {
        scenarioContext.response.then().statusCode(statusCode);
    }

    @When("I execute the GET request Returns collection of labs using following data and save criteria as {string}:")
    public void iExecuteTheGETRequestReturnsCollectionOfLabsUsingFollowingData(
            String key, Map<String, String> params) {
        scenarioContext.response = labs.returnsCollectionOfLabs(params);
        scenarioContext.add(key, params);
    }

    @And("response includes the labs matches the criteria {string}")
    public void theSearchResultMatchesTheCriteriaCriteria(String key) {
        Map<String, String> params = scenarioContext.get(key);
        LabsSearchResult result = scenarioContext.response.body().as(LabsSearchResult.class);
        List<LabDto> labs = result.getContent();

        Assert.assertTrue(labs.stream().allMatch(
                apiLab -> apiLab.getType().equals(params.get(ApiLabFields.TYPE.getModelField()))),
                String.format("Response should be includes labs with type %s", params.get(ApiLabFields.TYPE.getModelField())));

        Assert.assertTrue(labs.stream().allMatch(
                apiLab -> apiLab.getCountryCode().equals(params.get(ApiLabFields.COUNTRY_CODE.getModelField()))),
                String.format("Response should be includes labs with country code %s",
                        params.get(ApiLabFields.COUNTRY_CODE.getModelField())));

        Assert.assertTrue(labs.stream().allMatch(
                apiLab -> apiLab.getName().contains(params.get("keyword"))),
                String.format("Response should be includes labs contains name %s",
                        params.get(ApiLabFields.NAME.getModelField())));
    }

    @When("I execute the GET request Returns lab using ID from {string}")
    public void iExecuteTheGETRequestReturnsLabUsingLabId(String key) {
        LabDto labDto = scenarioContext.get(key);
        scenarioContext.response = labs.returnsLabWithGivenID(labDto.getId());
    }

    @And("response includes the Lab {string}")
    public void responseIncludesTheLabLab(String key) {
        LabDto expectedLab = scenarioContext.get(key);
        LabDto actualLab = scenarioContext.response.body().as(LabDto.class);
        Assert.assertEquals(actualLab, expectedLab,
                String.format("Response should be includes lab by name %s", expectedLab.getName()));
    }

    @And("I execute the PUT request Updates an existing lab {string} using following data:")
    public void iExecuteThePUTRequestUpdatesAnExistingLabUsingFollowingDataAndSaveLabAsLab(
            String key, @Transpose LabDto newLab) {
        LabDto labDto = scenarioContext.get(key);
        Integer labId = labDto.getId();
        scenarioContext.response = labs.updatesAnExistingLab(labId, newLab);
        newLab.setId(labId);
        scenarioContext.add(key, newLab);
    }

    @And("response includes the address {string}")
    public void responseIncludesTheAddressAddress(String key) {
        Address expectedAddress = scenarioContext.get(key);
        Address actualAddress = scenarioContext.response.body().as(Address.class);
        Assert.assertEquals(actualAddress, expectedAddress,
                String.format("Response should be includes address by id %s", expectedAddress.getId()));
    }

    @When("I execute the PUT request Updates an existing address for Lab {string} using following data and save as {string}:")
    public void iExecuteThePUTRequestUpdatesAnExistingAddressForLabLabUsingFollowingDataAndSaveAsAddress(
            String labKey, String addressKey, @Transpose Address updatedAddress) {
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        Address address = labDto.getAddresses().get(0);
        Integer addressId = address.getId();
        scenarioContext.response = labs.updatesAnExistingAddress(labId, addressId, updatedAddress);
        scenarioContext.add(addressKey, updatedAddress);
    }

    @And("I execute the POST request Creates a new lab test for Lab {string} using data from file {string}")
    public void iExecuteThePOSTRequestCreatesANewLabTestForLabLabUsingDataFromFile(String key, String fileName) {
        String jsonBody = DataReader.generateStringFromFile(fileName);
        LabDto labDto = scenarioContext.get(key);
        Integer labId = labDto.getId();
        scenarioContext.response = labs.createsNewLabTestForLab(labId, jsonBody);
    }

    @And("response includes the lab test for Lab {string} from file {string}")
    public void responseIncludesTheLabTestForLabLabFromFileTestAssayJson(String key, String fileName) {
        LabTestDto expectedLabTestDto = JSONConvertor.convertJSONFileToObject(DataReader.readJSONFile(fileName), LabTestDto.class);
        LabTestDto actualLabTestDto = scenarioContext.response.body().as(LabTestDto.class);
        LabDto labDto = scenarioContext.get(key);
        Integer labId = labDto.getId();
        expectedLabTestDto.setLabId(labId);
        Assert.assertEquals(actualLabTestDto, expectedLabTestDto,
                String.format("Response should be includes lab test by name %s", expectedLabTestDto.getName()));
    }

    @And("I save from response lab test as {string}")
    public void iSaveFromResponseLabTestAsAssay(String key) {
        LabTestDto labTestDto = scenarioContext.response.body().as(LabTestDto.class);
        scenarioContext.add(key, labTestDto);
    }

    @When("I execute the GET request Returns a lab test using IDs from {string}")
    public void iExecuteTheGETRequestReturnsALabTestUsingIDsFromAssay(String key) {
        LabTestDto labTestDto = scenarioContext.get(key);
        Integer labId = labTestDto.getLabId();
        Integer labTestId = labTestDto.getId();
        scenarioContext.response = labs.returnsLabTestWithGivenLabIDAndLabTestIDs(labId, labTestId);
    }

    @And("response includes the lab test {string}")
    public void responseIncludesTheLabTestAssay(String key) {
        LabTestDto expectedLabTestDto = scenarioContext.get(key);
        LabTestDto actualLabTestDto = scenarioContext.response.body().as(LabTestDto.class);
        Assert.assertEquals(actualLabTestDto, expectedLabTestDto,
                String.format("Response should be includes lab test by name %s", expectedLabTestDto.getName()));
    }

    @When("I execute the PUT request Updates an lab test {string} using data from file {string}")
    public void iExecuteThePUTRequestUpdatesAnLabTestAssayUsingDataFromFile(String key, String fileName) {
        LabTestDto labTestDto = scenarioContext.get(key);
        Integer labId = labTestDto.getLabId();
        Integer labTestId = labTestDto.getId();
        String jsonBody = DataReader.generateStringFromFile(fileName);
        scenarioContext.response = labs.updatesLabTest(labId, labTestId, jsonBody);
    }

    @When("I execute the DELETE request Marks a lab test using IDs from {string}")
    public void iExecuteTheDELETERequestMarksALabTestUsingIDsFromAssay(String key) {
        LabTestDto labTestDto = scenarioContext.get(key);
        Integer labId = labTestDto.getLabId();
        Integer labTestId = labTestDto.getId();
        scenarioContext.response = labs.deleteLabTest(labId, labTestId);
    }

    @When("I execute the GET request Returns collection of lab tests for Lab {string}")
    public void iExecuteTheGETRequestReturnsCollectionOfLabTestsForLabLab(String key) {
        LabDto labDto = scenarioContext.get(key);
        Integer labId = labDto.getId();
        scenarioContext.response = labs.returnsCollectionOfLabTestsForLab(labId);
    }

    @And("response includes the following lab test:")
    public void responseIncludesTheFollowingLabTest(List<String> keys) {
        CollectionAssays actualCollection = scenarioContext.response.body().as(CollectionAssays.class);
        List<LabTestDto> actualLabTestDtos = actualCollection.getContent();
        keys.forEach(key -> {
            LabTestDto labTestDto = scenarioContext.get(key);
            Assert.assertTrue(actualLabTestDtos.contains(labTestDto),
                    String.format("Response should be includes Assay by name - %s", labTestDto.getName()));
        });
    }

    @When("I execute the GET request Returns Collection of Lab Names and their IDs with parameters:")
    public void iExecuteTheGETRequestReturnsCollectionOfLabNamesAndTheirIDsWithParameters() {
        scenarioContext.response = labs.returnsCollectionOfLabNamesAndTheirIDs();
    }

    @And("I save Lab as {string} from response")
    public void iSaveLabAsLabFromResponse(String key) {
        LabDto labDto = scenarioContext.response.body().as(LabDto.class);
        scenarioContext.add(key, labDto);
    }

    @And("response includes the labs")
    public void responseIncludesTheLabs() {
        CollectionLabs labs = scenarioContext.response.body().as(CollectionLabs.class);
        Assert.assertTrue(labs.getContent().size() > 0, "Response should be includes labs");
    }

    @And("response not includes lab test {string}")
    public void responseNotIncludesLabTestAssay(String key) {
        CollectionAssays actualCollection = scenarioContext.response.body().as(CollectionAssays.class);
        List<LabTestDto> actualLabTestDtos = actualCollection.getContent();
        LabTestDto labTestDto = scenarioContext.get(key);
        Assert.assertFalse(actualLabTestDtos.contains(labTestDto),
                String.format("Response includes Assay by name - %s", labTestDto.getName()));
    }

    @Given("I execute the POST request Create a lab for user {string} using following data:")
    public void iExecuteThePOSTRequestCreateALabForUserUsingFollowingData(
            String userKey, @Transpose LabDto labDto) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        scenarioContext.response = labs.createLab(labDto, token);
    }

    @When("I execute the PUT request Update lab {string} for user {string} using following data:")
    public void iExecuteThePUTRequestUpdateLabForUserUsingFollowingData(
            String labKey, String userKey, @Transpose LabDto newLab) {
        LabDto labDto = scenarioContext.get(labKey);
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        Integer labId = labDto.getId();
        scenarioContext.response = labs.updatesAnExistingLab(labId, newLab, token);
    }

    @When("I execute the GET request Returns collection of labs for user {string} using following data:")
    public void iExecuteTheGETRequestReturnsCollectionOfLabsForUserUsingFollowingData(
            String userKey, Map<String, String> params) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        scenarioContext.response = labs.returnsCollectionOfLabs(params, token);
    }

    @And("I execute the GET request Returns lab for user {string} using ID from {string}")
    public void iExecuteTheGETRequestReturnsLabForUserUsingIDFromLab(String userKey, String labKey) {
        LabDto labDto = scenarioContext.get(labKey);
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        scenarioContext.response = labs.returnsLabWithGivenID(labDto.getId(), token);
    }

    @And("I execute the PUT request Update address for {string} using for user {string} following data:")
    public void iExecuteThePUTRequestUpdateAddressForLabUsingOrUserFollowingData(
            String labKey, String userKey, @Transpose Address updatedAddress) {
        LabDto labDto = scenarioContext.get(labKey);
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        Integer labId = labDto.getId();
        Address address = labDto.getAddresses().get(0);
        Integer addressId = address.getId();
        scenarioContext.response = labs.updatesAnExistingAddress(labId, addressId, updatedAddress, token);
    }

    @When("I execute the POST request Create lab test for {string} for user {string} using data from file {string}")
    public void iExecuteThePOSTRequestCreateLabTestForLabForUserUsingDataFromFile(
            String labKey, String userKey, String fileName) {
        String jsonBody = DataReader.generateStringFromFile(fileName);
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        scenarioContext.response = labs.createsNewLabTestForLab(labId, jsonBody, token);
    }

    @When("I execute the PUT request Update lab test {string} for user {string} using data from file {string}")
    public void iExecuteThePUTRequestUpdateAnLabTestForUserUsingDataFromFile(
            String assayKey, String userKey, String fileName) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabTestDto labTestDto = scenarioContext.get(assayKey);
        Integer labId = labTestDto.getLabId();
        Integer labTestId = labTestDto.getId();
        String jsonBody = DataReader.generateStringFromFile(fileName);
        scenarioContext.response = labs.updatesLabTest(labId, labTestId, jsonBody, token);
    }

    @When("I execute the GET request Returns a lab test for user {string} using IDs from {string}")
    public void iExecuteTheGETRequestReturnsALabTestForUserUsingIDsFromLabTest(String userKey, String assayKey) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabTestDto labTestDto = scenarioContext.get(assayKey);
        Integer labId = labTestDto.getLabId();
        Integer labTestId = labTestDto.getId();
        scenarioContext.response = labs.returnsLabTestWithGivenLabIDAndLabTestIDs(labId, labTestId, token);
    }

    @When("I execute the GET request Returns collection of lab tests for user {string} for {string}")
    public void iExecuteTheGETRequestReturnsCollectionOfLabTestsForUserForLab(String userKey, String labKey) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        scenarioContext.response = labs.returnsCollectionOfLabTestsForLab(labId, token);
    }

    @When("I execute the request DELETE lab test {string} for user {string}")
    public void iExecuteTheRequestDELETELabTestLabTestForUser(String assayKey, String userKey) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabTestDto labTestDto = scenarioContext.get(assayKey);
        Integer labId = labTestDto.getLabId();
        Integer labTestId = labTestDto.getId();
        scenarioContext.response = labs.deleteLabTest(labId, labTestId, token);
    }

    @When("I get own lab Id for user {string} and save as {string}")
    public void iGetOwnLabForUserAmLabsUpdateOwnAndSaveAsLab(String userKey, String labKey) {
        User user = scenarioContext.get(userKey);
        Integer labId = Integer.valueOf(user.getLabId());
        LabDto labDto = new LabDto();
        labDto.setId(labId);
        scenarioContext.add(labKey, labDto);
    }

    @When("I execute the POST request moving assays {string} to lab {string}")
    public void iExecuteThePOSTRequestMovingAssaysToLab(String assayKey, String labKey) {
        LabTestDto labTestDto = scenarioContext.get(assayKey);
        Integer assayId = labTestDto.getId();
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        MovingAssayToLab movingAssayToLab = new MovingAssayToLab(labId, assayId);
        scenarioContext.response = labs.movingAssayToLab(labId, movingAssayToLab);
        labTestDto.setLabId(labId);
    }

    @When("I execute the POST request moving assays {string} to lab {string} for user {string}")
    public void iExecuteThePOSTRequestMovingAssayAnotherLabForUser(String assayKey, String labKey, String userKey) {
        User user = scenarioContext.get(userKey);
        String token = new Token().getToken(user);
        LabTestDto labTestDto = scenarioContext.get(assayKey);
        Integer assayId = labTestDto.getId();
        LabDto labDto = scenarioContext.get(labKey);
        Integer labId = labDto.getId();
        MovingAssayToLab movingAssayToLab = new MovingAssayToLab(labId, assayId);
        scenarioContext.response = labs.movingAssayToLab(labId, movingAssayToLab, token);
    }

    @When("I execute the GET request Returns Collection of Lab Names and their IDs")
    public void iExecuteTheGETRequestReturnsCollectionOfLabNamesAndTheirIDs() {
        scenarioContext.response = labs.returnsCollectionOfLabNamesAndTheirIDs();
    }

    @And("I save from response list of lab names as {string}")
    public void iSaveFromResponseListOfLabNamesAsLabs(String key) {
        CollectionLabs labs = scenarioContext.response.body().as(CollectionLabs.class);
        List<String> labNames = labs.getContent().stream()
                .map(lab -> StringUtil.removeExtraCharacters(lab.getName()))
                .sorted().collect(Collectors.toList());

        scenarioContext.add(key, labNames);
    }
}
