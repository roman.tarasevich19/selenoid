package diaceutics.cucumber.stepdefinitions.api.registration;

import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.project.requests.registration.Registrations;
import diaceutics.restassured.project.models.ApiUser;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.When;
import javax.inject.Inject;

public class RegistrationApiSteps {

    private final Registrations registrations;
    private final ScenarioContext scenarioContext;

    @Inject
    public RegistrationApiSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        registrations = new Registrations();
    }

    @When("I register new user and save to {string} using following data:")
    public void iCreateOrganizationUserUsingRandomValues(String key, @Transpose ApiUser apiUser) {
        scenarioContext.add(key, apiUser);
        scenarioContext.response = registrations.createUser(apiUser);
    }

    @When("I get saved user values from {string} and register new user using these values")
    public void iRegisterNewUserUsingValuesFromExistingUser(String key) {
        ApiUser apiUser = scenarioContext.get(key);
        scenarioContext.response = registrations.createUser(apiUser);
    }
}
