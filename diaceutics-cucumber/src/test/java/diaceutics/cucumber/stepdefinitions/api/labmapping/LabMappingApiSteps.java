package diaceutics.cucumber.stepdefinitions.api.labmapping;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.utilities.JSONConvertor;
import diaceutics.restassured.project.models.*;
import diaceutics.restassured.project.requests.labmapping.LabMapping;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;
import java.util.List;

public class LabMappingApiSteps {

    private final LabMapping labMapping;
    private final ScenarioContext scenarioContext;

    @Inject
    public LabMappingApiSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        labMapping = new LabMapping();
    }

    @When("I execute request Returns a collection of subscriptions for the authenticated principle")
    public void iExecuteRequestReturnsACollectionOfSubscriptionsForTheAuthenticatedPrinciple() {
        scenarioContext.response = labMapping.returnsCollectionSubscriptionsForTheAuthenticatedPrinciple();
    }

    @And("response includes list of all the projects from file {string}")
    public void responseIncludesListOfAllTheProjectsFromFile(String fileName) {
        SubscriptionDto[] actualSubscriptionDtos = scenarioContext.response.body().as(SubscriptionDto[].class);
        SubscriptionDto[] expectedSubscriptionDtos = JSONConvertor.convertJSONFileToObject(
                DataReader.readJSONFile(fileName), SubscriptionDto[].class);

        Assert.assertEquals(actualSubscriptionDtos, expectedSubscriptionDtos,
                String.format("Response should be includes list of all Projects from file %s", fileName));
    }

    @When("I execute POST request Generates a lab mapping using following data:")
    public void iExecutePOSTRequestGeneratesALabMappingUsingFollowingDataAndSaveSearchFilter(
            @Transpose LabMappingRequest request) {
        scenarioContext.response = labMapping.generatesLabMapping(request);
    }

    @And("response includes the Lab {string} with Assay {string}")
    public void responseIncludesInLabMappingSearchLabLab(String labKey, String assayKey) {
        LabDto expectedLab = scenarioContext.get(labKey);
        LabTestDto expectedLabTestDto = scenarioContext.get(assayKey);
        LabMappingResponse labMappingResponse = scenarioContext.response.body().as(LabMappingResponse.class);
        List<LabDto> labs = labMappingResponse.getLabs();
        Assert.assertTrue(labs.stream().anyMatch(lab -> lab.getId().equals(expectedLab.getId())),
                String.format("Response should be includes the lab with id - %s", expectedLab.getId()));

        LabDto actualLab = labMappingResponse.getLabByNameAndId(expectedLab.getName(), expectedLab.getId());
        LabTestDto actualLabTestDto = actualLab.getAssayByNameAndId(expectedLabTestDto.getName(), expectedLabTestDto.getId());
        Assert.assertEquals(actualLabTestDto.getId(), expectedLabTestDto.getId(),
                String.format("Response should be includes the Assay with id - %s", expectedLabTestDto.getId()));
    }

    @And("save from response number of labs as {string}")
    public void saveFromResponseNumberOfLabsAs(String key) {
        LabMappingResponse labMappingResponse = scenarioContext.response.body().as(LabMappingResponse.class);
        List<LabDto> labs = labMappingResponse.getLabs();
        scenarioContext.add(key, labs.size());
    }

    @And("save from response number of labs with assays for biomarker as {string}")
    public void saveFromResponseNumberOfLabsWithAssaysForBiomarker(String key) {
        LabMappingResponse labMappingResponse = scenarioContext.response.body().as(LabMappingResponse.class);
        scenarioContext.add(key, labMappingResponse.getLabsWithAssaysForBiomarker());
    }

    @And("save from response number of patient tests as {string}")
    public void saveFromResponseNumberOfPatientTests(String key) {
        LabMappingResponse labMappingResponse = scenarioContext.response.body().as(LabMappingResponse.class);
        scenarioContext.add(key, labMappingResponse.getNumberOfPatientTests());
    }

    @When("I execute POST request Generates a testing dashboard mapping using following data:")
    public void iExecutePOSTRequestGeneratesATestingDashboardMappingUsingFollowingData(
            @Transpose LabMappingRequest request) {
        scenarioContext.response = labMapping.generatesTestingDashboardMapping(request);
    }

    @And("response includes the Labs")
    public void responseIncludesTheLabs() {
        LabMappingResponse labMappingResponse = scenarioContext.response.body().as(LabMappingResponse.class);
        List<LabDto> labs = labMappingResponse.getLabs();
        Assert.assertTrue(labs.size() > 0, "Response should be includes the las");
    }
}
