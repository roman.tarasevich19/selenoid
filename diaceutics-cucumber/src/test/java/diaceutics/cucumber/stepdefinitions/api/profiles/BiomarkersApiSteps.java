package diaceutics.cucumber.stepdefinitions.api.profiles;

import diaceutics.cucumber.utilities.DataReader;
import diaceutics.cucumber.utilities.ScenarioContext;
import diaceutics.restassured.project.models.BiomarkerDto;
import diaceutics.restassured.project.models.collections.CollectionBiomarkers;
import diaceutics.restassured.project.requests.profiles.Metadata;
import diaceutics.cucumber.utilities.apputils.BiomarkerUtil;
import io.cucumber.java.en.And;

import javax.inject.Inject;
import java.util.stream.Collectors;

public class BiomarkersApiSteps {

    private final Metadata metadata;
    private final ScenarioContext scenarioContext;

    @Inject
    public BiomarkersApiSteps(ScenarioContext apiStepData) {
        this.scenarioContext = apiStepData;
        metadata = new Metadata();
    }

    @And("I get number of invalid biomarkers from file {string} and save to {string}")
    public void getNumberOfInvalidBiomarkersFromCsvFile(String fileName, String key) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        CollectionBiomarkers allValidBiomarkers = metadata.returnsPageableListOfAllBiomarkers().body().as(CollectionBiomarkers.class);
        String numberOfInvalidBiomarkers =
                String.valueOf(BiomarkerUtil.getInvalidBiomarkersFromFile(
                        filePath, allValidBiomarkers.getContent().stream()
                                .map(BiomarkerDto::getName)
                                .collect(Collectors.toList())).size());
        scenarioContext.add(key, numberOfInvalidBiomarkers);
    }

    @And("I get number of valid biomarkers from file {string} and save to {string}")
    public void getNumberOfValidBiomarkersFromCsvFile(String fileName, String key) {
        String filePath = DataReader.getCommonFilesPath(fileName);
        CollectionBiomarkers allValidBiomarkers = metadata.returnsPageableListOfAllBiomarkers().body().as(CollectionBiomarkers.class);
        String numberOfValidBiomarkers =
                String.valueOf(BiomarkerUtil.getValidBiomarkersFromFile(
                        filePath, allValidBiomarkers.getContent().stream()
                                .map(BiomarkerDto::getName)
                                .collect(Collectors.toList())).size());
        scenarioContext.add(key, numberOfValidBiomarkers);
    }

    @And("I get all biomarkers from application and save to {string}")
    public void getAllBiomarkersFromApp(String key){
        CollectionBiomarkers allBiomarkers = metadata.returnsPageableListOfAllBiomarkers().body().as(CollectionBiomarkers.class);
        scenarioContext.add(key, allBiomarkers);
    }
}
