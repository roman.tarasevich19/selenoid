Feature: Physician Mapping export data

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Physician Mapping' tools
    And I select last tab
    Then Physician Mapping Main page is opened
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project     |
      | Disease              | Acute Myeloid Leukaemia |
      | Biomarker / Analogue | ABCB1                   |
      | Year From            | 2017                    |
      | Month From           | January                 |
      | Year To              | 2017                    |
      | Month To             | February                |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened

  @ui @PhysicianMapping @PhysicianMappingExportData @SingleThread
  Scenario: DIAPM:0013 Validation export data
    When I click Export data on Physician Mapping page
      Then File 'export.xlsx' is downloaded
    When I read file 'export.xlsx' with Physicians and save data as 'Physicians from file'
      Then I read data from AG-Grid Physician summary on Physician Mapping page and compare with 'Physicians from file'
      And I compare number of physicians from 'Physicians from file' and from AG-Grid Physician summary on Physician Mapping page

  @ui @PhysicianMapping @PhysicianMappingExportData @SingleThread
  Scenario: DIAPM:0014 Validation of mandatory fields on physician mapping table
    When I click Export data on Physician Mapping page
      Then File 'export.xlsx' is downloaded
    When I read file 'export.xlsx' with Physicians and save data as 'Physicians from file'
      Then Values for physicians from 'Physicians from file' for following fields should be not empty:
        | Physician NPI          |
        | Physician quintile     |
        | Disease patient count  |
        | Biomarker testing rate |

  @ui @PhysicianMapping @PhysicianMappingExportData
  Scenario Outline: <test count> Validation of filtering column <column> in table
    When I set value 'false' for column '<column>'
      Then Column '<column>' is not present

    Examples:
      | column                                          | test count |
      | Physician NPI                                   | DIAPM:0015 |
      | Organization affiliation                        | DIAPM:0016 |
      | Physician first name                            | DIAPM:0017 |
      | Physician last name                             | DIAPM:0018 |
      | Physician primary specialty                     | DIAPM:0019 |
      | Physician address line 1                        | DIAPM:0020 |
      | Physician city                                  | DIAPM:0021 |
      | Physician state                                 | DIAPM:0022 |
      | Physician ZIP                                   | DIAPM:0023 |
      | Physician phone number                          | DIAPM:0024 |
      | Physician quintile                              | DIAPM:0025 |
      | Disease patient count                           | DIAPM:0026 |
      | N of patients tested                            | DIAPM:0027 |
      | Biomarker testing rate                          | DIAPM:0028 |
      | Preferred laboratory for disease testing        | DIAPM:0029 |
      | Preferred laboratory classification             | DIAPM:0030 |
      | Preferred laboratory address line 1             | DIAPM:0031 |
      | Preferred laboratory city                       | DIAPM:0032 |
      | Preferred laboratory state                      | DIAPM:0033 |
      | Preferred laboratory ZIP                        | DIAPM:0034 |
      | Preferred lab offer required biomarker testing? | DIAPM:0035 |

  @ui @PhysicianMapping @PhysicianMappingExportData @SingleThread
  Scenario Outline: <test count> Validation of Export data without column <column> in table
    When I set value 'false' for column '<column>'
      Then Column '<column>' is not present
    When I click Export data on Physician Mapping page
      Then File 'export.xlsx' is downloaded
    When I read file 'export.xlsx' with Physicians with filter column '<column>' and save data as 'Physicians from file'
      Then I read data from AG-Grid Physician summary on Physician Mapping page and compare with 'Physicians from file' without column '<column>'

    Examples:
      | column                                          | test count |
      | Physician NPI                                   | DIAPM:0036 |
      | Organization affiliation                        | DIAPM:0037 |
      | Physician first name                            | DIAPM:0038 |
      | Physician last name                             | DIAPM:0039 |
      | Physician primary specialty                     | DIAPM:0040 |
      | Physician address line 1                        | DIAPM:0041 |
      | Physician city                                  | DIAPM:0042 |
      | Physician state                                 | DIAPM:0043 |
      | Physician ZIP                                   | DIAPM:0044 |
      | Physician phone number                          | DIAPM:0045 |
      | Physician quintile                              | DIAPM:0046 |
      | Disease patient count                           | DIAPM:0047 |
      | N of patients tested                            | DIAPM:0048 |
      | Biomarker testing rate                          | DIAPM:0049 |
      | Preferred laboratory for disease testing        | DIAPM:0050 |
      | Preferred laboratory classification             | DIAPM:0051 |
      | Preferred laboratory address line 1             | DIAPM:0052 |
      | Preferred laboratory city                       | DIAPM:0053 |
      | Preferred laboratory state                      | DIAPM:0054 |
      | Preferred laboratory ZIP                        | DIAPM:0055 |
      | Preferred lab offer required biomarker testing? | DIAPM:0056 |
