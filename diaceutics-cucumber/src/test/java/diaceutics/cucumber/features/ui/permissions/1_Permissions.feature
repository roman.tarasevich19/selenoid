Feature: Permissions

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0001 Permissions for AM:labs read user
    When I login as 'labs.read' user
      And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'labs.read' lab
      Then Hide version Lab profile page is opened
      And Edit Details is not displayed on Lab Profile Page
      And Add assay is not displayed on Lab Profile Page
      And Add volume is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'labs.read' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'labs.read' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'labs.read' not assigned lab
      Then Hide version Lab profile page is opened
      And Edit Details is not displayed on Lab Profile Page
      And Add assay is not displayed on Lab Profile Page
      And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'labs.read' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'labs.read' not assigned lab
    Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'labs.read' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'labs.read' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'labs.read' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0002 Permissions for AM:labs read-own user
    When I login as 'labs.read.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
      Then Access is denied
    When I open 'lab' url with environment in browser with user 'labs.read.own' lab
     Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'labs.read.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'labs.read.own' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'labs.read.own' not assigned lab
      Then Hide version Lab profile page is opened
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'labs.read.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'labs.read.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'labs.read.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'labs.read.own' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'labs.read.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0003 Permissions for AM:labs update user
    When I login as 'labs.update' user
      And User should be logged in
    When I open 'assays' url with environment in browser
    Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'labs.update' lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'labs.update' lab
      Then Edit Profile page is opened
    When I open 'edit not assigned lab' url with environment in browser with user 'labs.update' not assigned lab
      Then Edit Profile page is opened
    When I open 'not assigned lab' url with environment in browser with user 'labs.update' not assigned lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'labs.update' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'labs.update' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'labs.update' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'labs.update' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'labs.update' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0004 Permissions for AM:labs update-own user
    When I login as 'labs.update.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
      Then Access is denied
    When I open 'lab' url with environment in browser with user 'labs.update.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'labs.update.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'labs.update.own' not assigned lab
      Then Edit Profile page is opened
    When I open 'not assigned lab' url with environment in browser with user 'labs.update.own' not assigned lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'labs.update.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'labs.update.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'labs.update.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'labs.update.own' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'labs.update.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0005 Permissions for AM:addresses update
    When I login as 'addresses.update' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'addresses.update' lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'addresses.update' lab
      Then Edit Profile page is opened
    When I open 'edit not assigned lab' url with environment in browser with user 'addresses.update' not assigned lab
      Then Edit Profile page is opened
    When I open 'not assigned lab' url with environment in browser with user 'addresses.update' not assigned lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'addresses.update' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'addresses.update' not assigned lab
     Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'addresses.update' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'addresses.update' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'addresses.update' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0006 Permissions for AM:addresses update own
    When I login as 'addresses.update.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
     Then Access is denied
    When I open 'lab' url with environment in browser with user 'addresses.update.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'addresses.update.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'addresses.update.own' not assigned lab
      Then Edit Profile page is opened
    When I open 'not assigned lab' url with environment in browser with user 'addresses.update.own' not assigned lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'addresses.update.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'addresses.update.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'addresses.update.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'addresses.update.own' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'addresses.update.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0007 Permissions for AM:assays read
    When I login as 'assays.read' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
    Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'assays.read' lab
      Then Hide version Lab profile page is opened
    And Assays block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'assays.read' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'assays.read' not assigned lab
     Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'assays.read' not assigned lab
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'assays.read' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'assays.read' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'assays.read' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'assays.read' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'assays.read' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0008 Permissions for AM:assays read own
    When I login as 'assays.read.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
     Then Access is denied
    When I open 'assay search' url with environment in browser
      Then Access is denied
    When I open 'lab' url with environment in browser with user 'assays.read.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'assays.read.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'assays.read.own' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'assays.read.own' not assigned lab
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'assays.read.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'assays.read.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'assays.read.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'assays.read.own' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'assays.read.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0009 Permissions for AM:assays create
    When I login as 'assays.create' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
     Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'assays.create' lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    And Add assay is displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'assays.create' lab
      Then Edit Profile page is opened
    When I open 'edit not assigned lab' url with environment in browser with user 'assays.create' not assigned lab
      Then Edit Profile page is opened
    When I open 'not assigned lab' url with environment in browser with user 'assays.create' not assigned lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'assays.create' lab
      Then Add an Assay page is opened
    When I open 'tests create for not assigned lab' url with environment in browser with user 'assays.create' not assigned lab
      Then Add an Assay page is opened
    When I open 'tests edit for lab' url with environment in browser with user 'assays.create' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'assays.create' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'assays.create' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0010 Permissions for AM:assays create own
    When I login as 'assays.create.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
     Then Access is denied
    When I open 'lab' url with environment in browser with user 'assays.create.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'assays.create.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'assays.create.own' not assigned lab
      Then Edit Profile page is opened
    When I open 'not assigned lab' url with environment in browser with user 'assays.create.own' not assigned lab
      Then Hide version Lab profile page is opened
    And Edit Details is displayed on Lab Profile Page
    And Add assay is displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'assays.create.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'assays.create.own' not assigned lab
      Then Add an Assay page is opened
    When I open 'tests edit for lab' url with environment in browser with user 'assays.create.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'assays.create.own' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'assays.create.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0011 Permissions for AM:assays update
    When I login as 'assays.update' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'assays.update' lab
      Then Hide version Lab profile page is opened
    And Assays block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'assays.update' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'assays.update' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'assays.update' not assigned lab
      Then Hide version Lab profile page is opened
    And Assays block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'assays.update' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'assays.update' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'assays.update' lab
      Then Edit Assay page is opened
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'assays.update' not assigned lab
     Then Edit Assay page is opened
    When I open 'tests edit lab assays' url with environment in browser with user 'assays.update' lab
      Then Edit Assay page is opened

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0012 Permissions for AM:assays update own
    When I login as 'assays.update.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
      Then Access is denied
    When I open 'lab' url with environment in browser with user 'assays.update.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'assays.update.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'assays.update.own' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'assays.update.own' not assigned lab
      Then Hide version Lab profile page is opened
    And Assays block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'assays.update.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'assays.update.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'assays.update.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'assays.update.own' not assigned lab
      Then Edit Assay page is opened
    When I open 'tests edit lab assays' url with environment in browser with user 'assays.update.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0013 Permissions for AM:volumes read
    When I login as 'volumes.read' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'volumes.read' lab
    Then Hide version Lab profile page is opened
    And Volumes block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'volumes.read' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'volumes.read' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'volumes.read' not assigned lab
      Then Hide version Lab profile page is opened
    And Volumes block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'volumes.read' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'volumes.read' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'volumes.read' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'volumes.read' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'volumes.read' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0014 Permissions for AM:volumes read own
    When I login as 'volumes.read.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
      Then Access is denied
    When I open 'lab' url with environment in browser with user 'volumes.read.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'volumes.read.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'volumes.read.own' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'volumes.read.own' not assigned lab
    Then Hide version Lab profile page is opened
    And Volumes block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'volumes.read.own' lab
     Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'volumes.read.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'volumes.read.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'volumes.read.own' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'volumes.read.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0015 Permissions for AM:volumes update
    When I login as 'volumes.update' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'volumes.update' lab
      Then Hide version Lab profile page is opened
    And Volumes block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'volumes.update' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'volumes.update' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'volumes.update' not assigned lab
      Then Hide version Lab profile page is opened
    And Volumes block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'volumes.update' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'volumes.update' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'volumes.update' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'volumes.update' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'volumes.update' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0016 Permissions for AM:volumes update own
    When I login as 'volumes.update.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
      Then Access is denied
    When I open 'lab' url with environment in browser with user 'volumes.update.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'volumes.update.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'volumes.update.own' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'volumes.update.own' not assigned lab
      Then Hide version Lab profile page is opened
    And Volumes block is present
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Add volume is not displayed on Lab Profile Page
    When I open 'tests create for lab' url with environment in browser with user 'volumes.update.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'volumes.update.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'volumes.update.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'volumes.update.own' not assigned lab
     Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'volumes.update.own' lab
     Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0017 Permissions for AM:volumes create
    When I login as 'volumes.create' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'volumes.create' lab
      Then Hide version Lab profile page is opened
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Assays block is not present
    And Volumes block is present
    And Add volume is displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'volumes.create' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'volumes.create' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'volumes.create' not assigned lab
      Then Hide version Lab profile page is opened
    And Volumes block is present
    And Add volume is displayed on Lab Profile Page
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Assays block is not present
    When I open 'tests create for lab' url with environment in browser with user 'volumes.create' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'volumes.create' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'volumes.create' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'volumes.create' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'volumes.create' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0018 Permissions for AM:volumes create own
    When I login as 'volumes.create.own' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Access is denied
    When I open 'assay search' url with environment in browser
      Then Access is denied
    When I open 'lab' url with environment in browser with user 'volumes.create.own' lab
      Then Access is denied
    When I open 'edit lab' url with environment in browser with user 'volumes.create.own' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'volumes.create.own' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'volumes.create.own' not assigned lab
     Then Hide version Lab profile page is opened
    And Volumes block is present
    And Add volume is displayed on Lab Profile Page
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Assays block is not present
    When I open 'tests create for lab' url with environment in browser with user 'volumes.create.own' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'volumes.create.own' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'volumes.create.own' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'volumes.create.own' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'volumes.create.own' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0019 Permissions for AM:assays delete
    When I login as 'assays.delete' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
      Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'assays.delete' lab
      Then Hide version Lab profile page is opened
    And Volumes block is not present
    And Assays block is present
    And Add volume is not displayed on Lab Profile Page
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'assays.delete' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'assays.delete' not assigned lab
    Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'assays.delete' not assigned lab
      Then Hide version Lab profile page is opened
    And Volumes block is not present
    And Add volume is not displayed on Lab Profile Page
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Assays block is present
    When I open 'tests create for lab' url with environment in browser with user 'assays.delete' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'assays.delete' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'assays.delete' lab
      Then Edit Assay page is opened
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'assays.delete' not assigned lab
      Then Edit Assay page is opened
    When I open 'tests edit lab assays' url with environment in browser with user 'assays.delete' lab
      Then Edit Assay page is opened

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0020 Permissions for AM:volumes delete
    When I login as 'volumes.delete' user
    And User should be logged in
    When I open 'assays' url with environment in browser
      Then Assay Management page is opened
    When I open 'assay search' url with environment in browser
    Then SearchResults page is opened
    When I open 'lab' url with environment in browser with user 'volumes.delete' lab
      Then Hide version Lab profile page is opened
    And Volumes block is present
    And Assays block is not present
    And Add volume is not displayed on Lab Profile Page
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    When I open 'edit lab' url with environment in browser with user 'volumes.delete' lab
      Then Access is denied
    When I open 'edit not assigned lab' url with environment in browser with user 'volumes.delete' not assigned lab
      Then Access is denied
    When I open 'not assigned lab' url with environment in browser with user 'volumes.delete' not assigned lab
      Then Hide version Lab profile page is opened
    And Volumes block is present
    And Add volume is not displayed on Lab Profile Page
    And Edit Details is not displayed on Lab Profile Page
    And Add assay is not displayed on Lab Profile Page
    And Assays block is not present
    When I open 'tests create for lab' url with environment in browser with user 'volumes.delete' lab
      Then Access is denied
    When I open 'tests create for not assigned lab' url with environment in browser with user 'volumes.delete' not assigned lab
      Then Access is denied
    When I open 'tests edit for lab' url with environment in browser with user 'volumes.delete' lab
      Then Access is denied
    When I open 'tests edit for not assigned lab' url with environment in browser with user 'volumes.delete' not assigned lab
      Then Access is denied
    When I open 'tests edit lab assays' url with environment in browser with user 'volumes.delete' lab
      Then Access is denied

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0021 Permissions for labs:link to assay management
    When I login as 'labs.link.to.assay' user
    And User should be logged in
    And I click 'Data tools' on Master Header form
      Then Data tools context menu is displayed on Master Header form
    When I click 'Lab Mapping' from dropdown menu on Master Header form
    And I select last tab
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | March               |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click on entry 'QUEST DIAGNOSTICS' in AG-Grid Lab summary on Lab Mapping page
      Then Lab Mapping page is opened
    When I switch view to 'Assays' on Lab Mapping page
    And I click on entry 'NORTON HOSPITALS INC' in AG-Grid Lab summary on Lab Mapping page
      Then Lab Mapping page is opened

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0022 Required fields validation for user without permissions assays create and assays update assay creation
    When I login as 'without.assay.create.update' user
      Then Home page is opened
    When I open 'Assays' tools
      And I select last tab
      Then Lab Profile page is opened
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
      And Red border is displayed for following fields on Add an Assay page:
        | Assay name            |
        | Detects               |
        | Method                |
        | Ontology              |
        | Turnaround time (TAT) |
      And Message 'Please enter an assay name' is displayed on required fields on Add an Assay page
      And Message 'Please provide at least one value' is displayed on required fields on Add an Assay page
      And Message 'Method is required' is displayed on required fields on Add an Assay page
      And Message 'Classification is required' is displayed on required fields on Add an Assay page
      And Message 'Turnaround time is required' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Classification | Commercial assay |
      Then Message 'At least one commercial assay regulatory status is required' is displayed on required fields on Add an Assay page

  @ui @UiPermissions @SingleThread
  Scenario: DIAPFE:0023 Required fields validation for user without permissions assays create and assays update edit assay
    When I login as 'without.assay.create.update' user
      Then Home page is opened
    When I open 'Assays' tools
    And I select last tab
      Then Lab Profile page is opened
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | In-house or send-out? | In-house |
    And I click 'Proceed' on Edit Assay page
      Then Message 'Some items below need your attention.' is displayed on Edit Assay page
      And Red border is displayed for following fields on Edit Assay page:
      | Detects               |
      | Method                |
      | Ontology              |
      | Turnaround time (TAT) |
      And Message 'Please provide at least one value' is displayed on required fields on Edit Assay page
      And Message 'Method is required' is displayed on required fields on Edit Assay page
      And Message 'Classification is required' is displayed on required fields on Edit Assay page
      And Message 'Turnaround time is required' is displayed on required fields on Edit Assay page
    When I fill following fields on Edit Assay page and save as 'assay':
      | Classification | Commercial assay |
      Then Message 'At least one commercial assay regulatory status is required' is displayed on required fields on Edit Assay page
    When I click Delete on Edit Assay page
      Then Delete assay form on Edit Assay page is opened
    When I click 'Delete assay' on Delete assay form on Edit Assay page
      Then Lab Profile page is opened
      And 'The assay was successfully deleted' message is displayed on Lab Profile page
