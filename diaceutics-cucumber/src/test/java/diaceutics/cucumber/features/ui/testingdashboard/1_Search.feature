Feature: Testing Dashboard search

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Testing Dashboard' tools
    And I select last tab
      Then Testing Dashboard Main page is opened

  @ui @TestingDashboard @TestingDashboardSearch
  Scenario: DIATD:0001 Search
    When I fill following fields on Testing Dashboard Main page:
      | Project              | Full Access Project |
      | Country              | random              |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2018                |
      | Month From           | random              |
      | Year To              | 2019                |
      | Month To             | random              |
    And I click 'Start' on Testing Dashboard Main page
      Then Testing Dashboard page is opened

  @ui @TestingDashboard @TestingDashboardSearch
  Scenario: DIATD:0002 Required fields validation
    When I click 'Start' on Lab Mapping Main page
      Then Message 'Some items below need your attention.' is displayed on Testing Dashboard Main page
      And Message 'Please select a project' is displayed on required fields on Testing Dashboard Main page
    When I fill following fields on Testing Dashboard Main page:
      | Project | Full Access Project |
      Then Message 'Please input a country' is displayed on required fields on Testing Dashboard Main page
      And Message 'Disease is required' is displayed on required fields on Testing Dashboard Main page
      And Message 'A biomarker is required' is displayed on required fields on Testing Dashboard Main page
      And Message 'You specified an invalid date range' is displayed on required fields on Testing Dashboard Main page
    When I fill following fields on Testing Dashboard Main page:
      | Country              | random  |
      | Disease              | random  |
      | Biomarker / Analogue | random  |
      | Year From            | 2018    |
      | Month From           | April   |
      | Year To              | 2018    |
      | Month To             | January |
    And I click 'Start' on Testing Dashboard Main page
      Then Message 'Some items below need your attention.' is displayed on Testing Dashboard Main page
      And Message 'You specified an invalid date range' is displayed on required fields on Testing Dashboard Main page

  @ui @TestingDashboard @TestingDashboardSearch
  Scenario: DIATD:0003 Search filter editing
    When I fill following fields on Testing Dashboard Main page and save 'search filter':
      | Project              | Full Access Project |
      | Country              | random              |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2018                |
      | Month From           | random              |
      | Year To              | 2019                |
      | Month To             | random              |
    And I click 'Start' on Testing Dashboard Main page
      Then Testing Dashboard page is opened
      And Search filter 'search filter' with following fields is displayed on Testing Dashboard page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |
    When I click 'Edit' on Testing Dashboard page
      Then Edit settings form is opened on Testing Dashboard page
    When I fill following fields on Edit settings form on Testing Dashboard page and save 'search filter':
      | Country              | random |
      | Disease              | random |
      | Biomarker / Analogue | random |
      | Year From            | 2018   |
      | Month From           | random |
      | Year To              | 2019   |
      | Month To             | random |
    And I click 'Apply' on Edit settings form on Testing Dashboard
      Then Testing Dashboard page is opened
      And Search filter 'search filter' with following fields is displayed on Testing Dashboard page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |

  @ui @uiAndApi @TestingDashboard @TestingDashboardSearch
  Scenario: DIATD:0004 Check number of labs
    When I execute POST request Generates a lab mapping using following data:
      | projectId    | 1         |
      | countryCode  | USA       |
      | criteriaType | BIOMARKER |
      | criteriaId   | 1         |
      | diseaseIds   | 1         |
      | fromYear     | 2017      |
      | fromMonth    | 2         |
      | toYear       | 2020      |
      | toMonth      | 2         |
      Then the status code is 200
      And save from response number of labs as 'numberOfLabs'
    When I fill following fields on Testing Dashboard Main page:
      | Project              | Full Access Project        |
      | Country              | United States              |
      | Disease              | Non small cell lung cancer |
      | Biomarker / Analogue | ALK                        |
      | Year From            | 2017                       |
      | Month From           | February                   |
      | Year To              | 2020                       |
      | Month To             | February                   |
    And I click 'Start' on Testing Dashboard Main page
      Then Testing Dashboard page is opened
      And 'numberOfLabs' should be the same as a number of labs on Testing Dashboard page

  @ui @TestingDashboard @TestingDashboardSearch
  Scenario: DIATD:0067 Canceled search filter editing
    When I fill following fields on Testing Dashboard Main page and save 'search filter':
      | Project              | Full Access Project |
      | Country              | random              |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2018                |
      | Month From           | random              |
      | Year To              | 2019                |
      | Month To             | random              |
    And I click 'Start' on Testing Dashboard Main page
      Then Testing Dashboard page is opened
      And Search filter 'search filter' with following fields is displayed on Testing Dashboard page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |
    When I click 'Edit' on Testing Dashboard page
      Then Edit settings form is opened on Testing Dashboard page
    When I fill following fields on Edit settings form on Testing Dashboard page and save 'new search filter':
      | Country              | random |
      | Disease              | random |
      | Biomarker / Analogue | random |
      | Year From            | 2019   |
      | Month From           | random |
      | Year To              | 2021   |
      | Month To             | random |
    And I click 'Cancel' on Edit settings form on Testing Dashboard
      Then Testing Dashboard page is opened
      And Search filter 'search filter' with following fields is displayed on Testing Dashboard page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |
