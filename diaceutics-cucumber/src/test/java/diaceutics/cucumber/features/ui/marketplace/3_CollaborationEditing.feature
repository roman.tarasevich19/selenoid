Feature: Collaboration editing

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in

  @ui @Marketplace @CollaborationEditing @SingleThread
  Scenario: DIAM:0021 Possibility to edit a Collaboration
    When I click Start a collaboration on Home page
      Then Description collaboration page is opened
    When I fill following fields on Description of the collaboration page and save as 'collaboration':
      | Title                  | Test title   |
      | Description            | Description  |
      | Additional information | Requirements |
      | Select a category      | random       |
    When I click 'Next step' on Description collaboration page
      Then Location of the collaboration page is opened
    When I fill following fields on Location of the collaboration page and save as 'collaboration':
      | Country                         | Belarus     |
      | City                            | Minsk       |
      | Zip                             | 1200        |
      | Number                          | 78          |
      | Route                           | Test route  |
      | Additional location information | Information |
    And I click 'Submit for publishing' on Location of the collaboration page
      Then Edit collaboration page is opened
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Edit' Collaboration 'collaboration' on My Collaborations page
      Then Edit collaboration page is opened
    When I fill following fields on Presentation form on Edit collaboration page and save as 'collaboration':
      | Title                  | Test title        |
      | Description            | Edit description  |
      | Additional information | Edit requirements |
    And I click Save changes button on Edit collaboration page
      Then Inform Form with message 'Update successful' is displayed on Edit collaboration page
      And Collaboration 'collaboration' with following fields is displayed on Presentation form on Edit collaboration page
        | Title                  |
        | Description            |
        | Additional information |
    When I click 'Category' link on Edit collaboration page
      Then Categories form on Edit collaboration page is opened
    When I set 'random' value for 'Category' field on Category form on Edit collaboration page and save as 'collaboration'
    And I click Save changes button on Edit collaboration page
      Then Collaboration 'collaboration' with 'Category' field is displayed on Category form on Edit collaboration page
    When I click group 'Location' on Edit collaboration page
    And I click 'Location' link on Edit collaboration page
      Then My location form on Edit collaboration page is opened
    When I fill following fields on My location form on Edit collaboration page and save as 'collaboration':
      | Country                         | United Kingdom   |
      | City                            | London           |
      | Zip                             | 1111             |
      | Number                          | 39               |
      | Route                           | Edit route       |
      | Additional location information | Edit Information |
    And I click Save changes button on Edit collaboration page
      Then Inform Form with message 'Update successful' is displayed on Edit collaboration page
      And Collaboration 'collaboration' with following fields is displayed on My location form on Edit collaboration page:
        | Country                         |
        | City                            |
        | Zip                             |
        | Number                          |
        | Route                           |
        | Additional location information |

  @ui @Marketplace @CollaborationEditing @SingleThread
  Scenario: DIAM:0022 Possibility to set Hidden status for Published collaboration
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I set 'Published' status and click Apply button on My Collaborations page
      Then My Collaborations page is opened
    When I select random collaboration from My Collaborations page and save as 'random collaboration'
    And I click 'Edit' Collaboration 'random collaboration' on My Collaborations page
      Then Edit collaboration page is opened
    When I fill following fields on Presentation form on Edit collaboration page and save as 'random collaboration':
      | Status | Hidden |
    And I click Save changes button on Edit collaboration page
      Then Inform Form with message 'Update successful' is displayed on Edit collaboration page
      And Status 'Hidden' is displayed on Edit collaboration page
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I set 'Hidden' status and click Apply button on My Collaborations page
      Then My Collaborations page is opened
    When I click 'Edit' Collaboration 'random collaboration' on My Collaborations page
      Then Edit collaboration page is opened
      And Status 'Hidden' is displayed on Edit collaboration page
    When I fill following fields on Presentation form on Edit collaboration page and save as 'random collaboration':
      | Status | Published |
    And I click Save changes button on Edit collaboration page
      Then Inform Form with message 'Update successful' is displayed on Edit collaboration page
      And Status 'Published' is displayed on Edit collaboration page
