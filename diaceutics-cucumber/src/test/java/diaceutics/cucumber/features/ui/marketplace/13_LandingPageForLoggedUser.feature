Feature: Landing page for logged user

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in

  @ui @Marketplace @LandingPageForLoggedUser
  Scenario: DIAM:0065 Validation search filter
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location    | Afghanistan   |
      | Type        | Collaboration |
      | Sponsored   | true          |
      | Test Review | true          |
      | Offered     | true          |
      | Keywords    | test          |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I click DXRX logo on the top left on Master Header form
      Then Marketplace Main page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location               | Afghanistan   |
      | Type                   | Collaboration |
      | Requested              | true          |
      | Testing Icon Group one | true          |
      | Testing Icon Group two | true          |
      | Keywords               | new test      |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
      And Search filter 'filter' for following fields is displayed on Search form:
        | Location               |
        | Type                   |
        | Sponsored              |
        | Test Review            |
        | Offered                |
        | Requested              |
        | Testing Icon Group one |
        | Testing Icon Group two |
        | Keywords               |
    When I click 'My organization' on Master Header form
      Then Organization page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location       | Afghanistan  |
      | Type           | Organization |
      | Laboratory     | true         |
      | Diagnostics    | true         |
      | Pharmaceutical | true         |
      | Keywords       | test         |
    And I click search button on Search form
      Then Organizations Listing page is opened
    When I click DXRX logo on the top left on Master Header form
      Then Marketplace Main page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location         | Minsk        |
      | Type             | Organization |
      | Service Provider | true         |
      | Diaceutics       | true         |
      | Other            | true         |
      | Keywords         | new test     |
    And I click search button on Search form
    Then Organizations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
      And Search filter 'filter' for following fields is displayed on Search form:
        | Location         |
        | Type             |
        | Laboratory       |
        | Diagnostics      |
        | Pharmaceutical   |
        | Service Provider |
        | Diaceutics       |
        | Other            |
        | Keywords         |

  @ui @Marketplace @LandingPageForLoggedUser
  Scenario: DIAM:0066 Validation search filter with refresh page
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location               | Afghanistan   |
      | Type                   | Collaboration |
      | Sponsored              | true          |
      | Test Review            | true          |
      | Requested              | true          |
      | Testing Icon Group one | true          |
      | Testing Icon Group two | true          |
      | Offered                | true          |
      | Keywords               | test          |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I refresh page
    And I open Search form on on Master Header form
      Then Search form is opened
      And Search filter has default values for following fields on Search form:
        | Location               |
        | Type                   |
        | Sponsored              |
        | Test Review            |
        | Offered                |
        | Requested              |
        | Testing Icon Group one |
        | Testing Icon Group two |
        | Keywords               |
    When I set following fields on Search form and save as 'filter':
      | Location         | Afghanistan  |
      | Type             | Organization |
      | Laboratory       | true         |
      | Diagnostics      | true         |
      | Pharmaceutical   | true         |
      | Service Provider | true         |
      | Diaceutics       | true         |
      | Other            | true         |
      | Keywords         | test         |
    And I click search button on Search form
      Then Organizations Listing page is opened
    When I refresh page
    And I open Search form on on Master Header form
      Then Search form is opened
      And Search filter has default values for following fields on Search form:
        | Location               |
        | Type                   |
        | Sponsored              |
        | Test Review            |
        | Offered                |
        | Requested              |
        | Testing Icon Group one |
        | Testing Icon Group two |
        | Keywords               |

  @ui @Marketplace @LandingPageForLoggedUser
  Scenario: DIAM:0067 Validation search filter with logout
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location               | Afghanistan   |
      | Type                   | Collaboration |
      | Sponsored              | true          |
      | Test Review            | true          |
      | Requested              | true          |
      | Testing Icon Group one | true          |
      | Testing Icon Group two | true          |
      | Offered                | true          |
      | Keywords               | test          |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
      And Search filter has default values for following fields on Search form:
        | Location               |
        | Type                   |
        | Sponsored              |
        | Test Review            |
        | Offered                |
        | Requested              |
        | Testing Icon Group one |
        | Testing Icon Group two |
        | Keywords               |
    When I set following fields on Search form and save as 'filter':
      | Location         | Afghanistan  |
      | Type             | Organization |
      | Laboratory       | true         |
      | Diagnostics      | true         |
      | Pharmaceutical   | true         |
      | Service Provider | true         |
      | Diaceutics       | true         |
      | Other            | true         |
      | Keywords         | test         |
    And I click search button on Search form
      Then Organizations Listing page is opened
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    And Search filter has default values for following fields on Search form:
      | Location               |
      | Type                   |
      | Sponsored              |
      | Test Review            |
      | Offered                |
      | Requested              |
      | Testing Icon Group one |
      | Testing Icon Group two |
      | Keywords               |

  @ui @Marketplace @LandingPageForLoggedUser
  Scenario: DIAM:0068 Validation search filter with empty location
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Type        | Collaboration |
      | Sponsored   | true          |
      | Test Review | true          |
      | Offered     | true          |
      | Keywords    | test          |
    And I click search button on Search form
      Then Collaborations Listing page is opened
      And Collaborations are displayed on Collaborations Listing page
    When I click 'My organization' on Master Header form
      Then Organization page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Type           | Organization |
      | Laboratory     | true         |
      | Diagnostics    | true         |
      | Pharmaceutical | true         |
      | Keywords       | test         |
    And I click search button on Search form
      Then Organizations Listing page is opened
      And Organizations are displayed on Organizations Listing page

  @ui @Marketplace @Collaboration @LandingPageForLoggedUser
  Scenario: DIAM:0069 Validation search filter with change location
    When I click View all public collaborations on Home page
      Then Collaborations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location | Afghanistan   |
      | Type     | Collaboration |
    And I click search button on Search form
      Then Collaborations Listing page is opened
      And Collaboration 'Test collaboration' is displayed on Collaborations Listing page
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location | Albania       |
      | Type     | Collaboration |
    And I click search button on Search form
      Then Collaborations Listing page is opened
      And Collaboration 'Test collaboration two' is displayed on Collaborations Listing page
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location | United States |
      | Type     | Organization  |
    And I click search button on Search form
      Then Organizations Listing page is opened
      And Organizations by location from 'filter' are displayed on Organizations Listing page
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location | United Kingdom |
      | Type     | Organization   |
    And I click search button on Search form
      Then Organizations Listing page is opened
      And Organizations by location from 'filter' are displayed on Organizations Listing page
