Feature: Physician Mapping filter table columns

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Physician Mapping' tools
      And I select last tab
      Then Physician Mapping Main page is opened
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | Breast Cancer       |
      | Biomarker / Analogue | ALK                 |
      | Year From            | 2017                |
      | Month From           | January             |
      | Year To              | 2017                |
      | Month To             | February            |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened

  @ui @PhysicianMapping @PhysicianMappingFiltering
  Scenario: DIAPM:0057 Check filtering types
    When I open menu for column 'Physician NPI'
      Then I check Filter menu types to compare are equals the following list:
      | Contains     |
      | Not contains |
      | Equals       |
      | Not equal    |
      | Starts with  |
      | Ends with    |

  @ui @PhysicianMapping @PhysicianMappingFiltering
  Scenario Outline: <test count> Filtering Physician NPI column <mode> mode
    When I open menu for column 'Physician NPI'
    And I select filter type '<mode>'
    And I fill value '<value>' in filter body input
      Then I read data from column 'Physician NPI' and check values with filter '<value>' and type '<mode>'

    Examples:
      | mode         | value      | test count |
      | Contains     | 11912      | DIAPM:0058 |
      | Not contains | 9888       | DIAPM:0059 |
      | Equals       | 1841225240 | DIAPM:0060 |
      | Not equal    | 1841225240 | DIAPM:0061 |
      | Starts with  | 18412      | DIAPM:0062 |
      | Ends with    | 240        | DIAPM:0063 |

  @ui @PhysicianMapping @PhysicianMappingFiltering
  Scenario Outline: <test count> Filtering Physician first name column <mode> mode
    When I open menu for column 'Physician first name'
    And I select filter type '<mode>'
    And I fill value '<value>' in filter body input
      Then I read data from column 'Physician first name' and check values with filter '<value>' and type '<mode>'

    Examples:
      | mode         | value | test count |
      | Contains     | STUA  | DIAPM:0064 |
      | Not contains | STUA  | DIAPM:0065 |
      | Equals       | JOHN  | DIAPM:0066 |
      | Not equal    | JOHN  | DIAPM:0067 |
      | Starts with  | MI    | DIAPM:0068 |
      | Ends with    | HN    | DIAPM:0069 |

  @ui @PhysicianMapping @PhysicianMappingFiltering
  Scenario Outline: <test count> Filtering Physician last name column <mode> mode
    When I open menu for column 'Physician last name'
    And I select filter type '<mode>'
    And I fill value '<value>' in filter body input
      Then I read data from column 'Physician last name' and check values with filter '<value>' and type '<mode>'

    Examples:
      | mode         | value   | test count |
      | Contains     | SON     | DIAPM:0070 |
      | Not contains | SON     | DIAPM:0071 |
      | Equals       | JOHNSON | DIAPM:0072 |
      | Not equal    | JOHNSON | DIAPM:0073 |
      | Starts with  | PE      | DIAPM:0074 |
      | Ends with    | KE      | DIAPM:0075 |

  @ui @PhysicianMapping @PhysicianMappingFiltering
  Scenario Outline: <test count> Filtering <column> in table
    When I open menu for column '<column>'
    And I set value 'false' for filter box '(Select All)'
      Then Physician table is empty
    When I set value 'true' for filter box '<filter>'
      Then I read data with filter '<filter>' for column '<column>'

    Examples:
      | column                                          | filter                            | test count |
      | Physician primary specialty                     | Acupuncturist                     | DIAPM:0076 |
      | Physician address line 1                        | # 25070                           | DIAPM:0077 |
      | Physician city                                  | ABBEVILLE                         | DIAPM:0078 |
      | Physician state                                 | AE                                | DIAPM:0079 |
      | Physician ZIP                                   | 006012337                         | DIAPM:0080 |
      | Physician phone number                          | (151) 826-2512                    | DIAPM:0081 |
      | Physician quintile                              | Q1                                | DIAPM:0082 |
      | Disease patient count                           | 1                                 | DIAPM:0083 |
      | N of patients tested                            | 10                                | DIAPM:0084 |
      | Biomarker testing rate                          | 0.00%                             | DIAPM:0085 |
      | Organization affiliation                        | ABBEVILLE AREA MEDICAL CENTER     | DIAPM:0086 |
      | Preferred laboratory for disease testing        | 100 PLAZA CLINICAL LAB INC        | DIAPM:0087 |
      | Preferred laboratory classification             | ACADEMIC                          | DIAPM:0088 |
      | Preferred laboratory address line 1             | # 43 CALLE IGNACIO MORALES ACOSTA | DIAPM:0089 |
      | Preferred laboratory city                       | ABERDEEN                          | DIAPM:0090 |
      | Preferred laboratory state                      | AK                                | DIAPM:0091 |
      | Preferred laboratory ZIP                        | 00767-3110                        | DIAPM:0092 |
      | Preferred lab offer required biomarker testing? | No                                | DIAPM:0093 |


  @ui @PhysicianMapping @PhysicianMappingFiltering
  Scenario: DIAPM:0102 Check filters on Chart form
    When I click 'Charts' tab on Physician Mapping page
      And Charts form on Physician Mapping page is opened
      Then Active filters is '0 active filters:'
    When I click 'Data table' tab on Physician Mapping page
      Then Physician Mapping page is opened
    When I open menu for column 'Physician city'
    And I set value 'false' for filter box '(Select All)'
      Then Physician table is empty
    When I set value 'true' for filter box 'ABERDEEN'
      Then I read data with filter 'ABERDEEN' for column 'Physician city'
    When I click 'Charts' tab on Physician Mapping page
    And Charts form on Physician Mapping page is opened
      Then Active filters is '1 active filters:'
    When I click 'Data table' tab on Physician Mapping page
      Then Physician Mapping page is opened
    When I open menu for column 'Physician state'
    And I set value 'false' for filter box '(Select All)'
      Then Physician table is empty
    When I set value 'true' for filter box 'SD'
      Then I read data with filter 'SD' for column 'Physician state'
    When I click 'Charts' tab on Physician Mapping page
    And Charts form on Physician Mapping page is opened
      Then Active filters is '2 active filters:'

  @ui @uiAndApi @PhysicianMapping
  Scenario: DIAPM:0103 Ag Grid heading icons and values and after edit search
    When I execute POST request Generates a physician mapping using following data:
      | projectId    | 1         |
      | biomarkerId  | 1         |
      | diseaseId    | 2         |
      | fromYear     | 2017      |
      | fromMonth    | 1         |
      | toYear       | 2017      |
      | toMonth      | 2         |
    Then the status code is 200
      And save from response number of physicians as 'numberOfPhysicians'
      And save from response number of patient tests physician mapping as 'numberOfPatientTests'
      And Icon 'physicians' is displayed on heading panel on Physician Mapping page
      And Icon 'patient tests' is displayed on heading panel on Physician Mapping page
    When I get number from 'physicians' heading banner on Physician Mapping page and save to 'numberOfPhysiciansUI'
    And I get number from 'patient tests' heading banner on Physician Mapping page and save to 'numberOfPatientTestsUI'
      Then I get number of heading banner 'numberOfPhysiciansUI' and compare with saved value 'numberOfPhysicians' on Physician Mapping page
      And I get number of heading banner 'numberOfPatientTestsUI' and compare with saved value 'numberOfPatientTests' on Physician Mapping page
    When I click 'Edit' on Physician Mapping page
      Then Edit settings form is opened on Physician Mapping page
    When I fill following fields on Edit settings form on Physician Mapping page and save 'search filter':
      | Disease              | Breast Cancer |
      | Biomarker / Analogue | ALK           |
      | Year From            | 2018          |
      | Month From           | January       |
      | Year To              | 2018          |
      | Month To             | January       |
    And I click 'Apply' on Edit settings form on Physician Mapping
      Then Physician Mapping page is opened
    When I execute POST request Generates a physician mapping using following data:
      | projectId    | 1         |
      | biomarkerId  | 1         |
      | diseaseId    | 2         |
      | fromYear     | 2018      |
      | fromMonth    | 1         |
      | toYear       | 2018      |
      | toMonth      | 1         |
    Then the status code is 200
      And save from response number of physicians as 'numberOfPhysiciansEdit'
      And save from response number of patient tests physician mapping as 'numberOfPatientTestsEdit'
    When I get number from 'physicians' heading banner on Physician Mapping page and save to 'numberOfPhysiciansUIedit'
    And I get number from 'patient tests' heading banner on Physician Mapping page and save to 'numberOfPatientTestsUIedit'
      Then I get number of heading banner 'numberOfPhysiciansUIedit' and compare with saved value 'numberOfPhysiciansEdit' on Physician Mapping page
      And I get number of heading banner 'numberOfPatientTestsUIedit' and compare with saved value 'numberOfPatientTestsEdit' on Physician Mapping page

  @ui @uiAndApi @PhysicianMapping
  Scenario: DIAPM:0104 Ag Grid heading icons and values and after edit search
    When I click 'Edit' on Physician Mapping page
      Then Edit settings form is opened on Physician Mapping page
    When I fill following fields on Edit settings form on Physician Mapping page and save 'search filter':
      | Disease              | Urothelial Cancer |
      | Biomarker / Analogue | HEBP1             |
      | Year From            | 2018              |
      | Month From           | January           |
      | Year To              | 2018              |
      | Month To             | January           |
    And I click 'Apply' on Edit settings form on Physician Mapping
      Then Physician Mapping page is opened
    When I execute POST request Generates a physician mapping using following data:
      | projectId   | 1    |
      | biomarkerId | 2671 |
      | diseaseId   | 31   |
      | fromYear    | 2018 |
      | fromMonth   | 1    |
      | toYear      | 2018 |
      | toMonth     | 1    |
    Then the status code is 200
      And save from response number of physicians as 'numberOfPhysiciansEdit'
      And save from response number of patient tests physician mapping as 'numberOfPatientTestsEdit'
    When I get number from 'physicians' heading banner on Physician Mapping page and save to 'numberOfPhysiciansUIedit'
    And I get number from 'patient tests' heading banner on Physician Mapping page and save to 'numberOfPatientTestsUIedit'
      Then I get number of heading banner 'numberOfPhysiciansUIedit' and compare with saved value 'numberOfPhysiciansEdit' on Physician Mapping page
      And I get number of heading banner 'numberOfPatientTestsUIedit' and compare with saved value 'numberOfPatientTestsEdit' on Physician Mapping page