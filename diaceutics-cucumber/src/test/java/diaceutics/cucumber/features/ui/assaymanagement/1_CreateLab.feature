Feature: Create a Lab

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened

  @ui @AssayManagement @CreateALab
  Scenario Outline: <test count> Possibility to successfully create an <Lab type>
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name     | TestLab    |
      | Lab type | <Lab type> |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And 'Lab created.' message is displayed on Lab Profile page
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
        | Country  |
        | Region   |

    Examples:
      | Lab type       | test count |
      | Unspecified    | DIAFE:0001 |
      | Academic Lab   | DIAFE:0002 |
      | Commercial Lab | DIAFE:0003 |
      | Hospital Lab   | DIAFE:0004 |

  @ui @AssayManagement @CreateALab
  Scenario: DIAFE:0005 Create a Lab: Required fields validation
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I click 'Next' on Create a Lab page
      Then Message 'Some items below need your attention.' is displayed on Create a Lab page
      And Message 'Please input a name for the lab' is displayed on required fields on Create a Lab page
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name     | TestLab     |
      | Lab type | Unspecified |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I click 'Finish' on Lab Address page
      Then Message 'Some items below need your attention.' is displayed on Lab Address page
      And Message 'Please input a country' is displayed on required fields on Lab Address page
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And 'Lab created.' message is displayed on Lab Profile page
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
        | Country  |
        | Region   |

  @ui @AssayManagement @CreateALab
  Scenario Outline: <test count> Possibility to successfully create Unspecified Lab with Ownership <Ownership>
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name      | TestLab     |
      | Lab type  | Unspecified |
      | Ownership | <Ownership> |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And 'Lab created.' message is displayed on Lab Profile page
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
        | Country  |
        | Region   |
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened
      And Value '<Ownership>' for field 'Ownership' is selected on Lab Details form on Edit Profile page

    Examples:
      | Ownership      | test count |
      | Public         | DIAFE:0006 |
      | Private        | DIAFE:0007 |
      | Military       | DIAFE:0008 |
      | Not Applicable | DIAFE:0009 |

  @ui @AssayManagement @CreateALab
  Scenario Outline: <test count> Possibility to successfully create Academic Lab with Ownership <Ownership>
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name      | TestLab      |
      | Lab type  | Academic Lab |
      | Ownership | <Ownership>  |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And 'Lab created.' message is displayed on Lab Profile page
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
        | Country  |
        | Region   |
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened
      And Value '<Ownership>' for field 'Ownership' is selected on Lab Details form on Edit Profile page

    Examples:
      | Ownership      | test count |
      | Public         | DIAFE:0010 |
      | Private        | DIAFE:0011 |
      | Military       | DIAFE:0012 |
      | Not Applicable | DIAFE:0013 |

  @ui @AssayManagement @CreateALab
  Scenario Outline: <test count> Possibility to successfully create Commercial Lab with Ownership <Ownership>
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name      | TestLab        |
      | Lab type  | Commercial Lab |
      | Ownership | <Ownership>    |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And 'Lab created.' message is displayed on Lab Profile page
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
        | Country  |
        | Region   |
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened
      And Value '<Ownership>' for field 'Ownership' is selected on Lab Details form on Edit Profile page

    Examples:
      | Ownership      | test count |
      | Public         | DIAFE:0014 |
      | Private        | DIAFE:0015 |
      | Military       | DIAFE:0016 |
      | Not Applicable | DIAFE:0017 |

  @ui @AssayManagement @CreateALab
  Scenario Outline: <test count> Possibility to successfully create Hospital Lab with Ownership <Ownership>
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name      | TestLab      |
      | Lab type  | Hospital Lab |
      | Ownership | <Ownership>  |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And 'Lab created.' message is displayed on Lab Profile page
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
        | Country  |
        | Region   |
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened
      And Value '<Ownership>' for field 'Ownership' is selected on Lab Details form on Edit Profile page

    Examples:
      | Ownership      | test count |
      | Public         | DIAFE:0018 |
      | Private        | DIAFE:0019 |
      | Military       | DIAFE:0020 |
      | Not Applicable | DIAFE:0021 |

  @ui @AssayManagement @CreateALab
  Scenario: DIAFE:0067 Validation message for lab without assay and volume
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name     | TestLab     |
      | Lab type | Unspecified |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And Message 'Your search returned no results' is displayed in Assays grid on Lab Profile page
      And Message 'Please adjust your search settings and try again.' is displayed in Assays grid on Lab Profile page
      And Message 'Your search returned no results' is displayed in Patient volume log grid on Lab Profile page
      And Message 'Please adjust your search settings and try again.' is displayed in Patient volume log grid on Lab Profile page