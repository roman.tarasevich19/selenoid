Feature: User registration

  @NotActual
  Scenario Outline: <test count> User with Organization type <Organization type> registration
    Given Marketplace Main page is opened
    When I click 'Register' on Marketplace Main page
      Then Registration page is opened
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Organization type                | <Organization type> |
      | Company name                     | <Company name>      |
      | Position within the organization | Test position       |
      | Title                            | random              |
      | First name                       | Test first name     |
      | Last name                        | Test last name      |
      | Email                            | @mailosaur.io       |
      | Create a password                | Testpassword1       |
      | Repeat your password             | Testpassword1       |
      | I agree to the Membership Terms  | true                |
      | I accept the Privacy Policy      | true                |
    And I click Register on Personal Details form on Registration page
      Then Marketplace Main page is opened
      And I get a mail for the 'user' with a subject 'Verify your email'
      And I get a mail for the 'user' with a subject 'Your account on DXRX is under moderation'
    When I open Verify Link from 'user' mail with subject 'Verify your email' and confirm registration
      Then Registration Confirmed page is opened
      And Message 'Your email address was successfully verified.' is displayed on Registration Confirmed page

    Examples:
      | Organization type | Company name           | test count |
      | Diaceutics        | Diaceutics             | DIAM:0001  |
      | Laboratory        | Asd                    | DIAM:0002  |
      | Service Provider  | Histocyte Laboratories | DIAM:0003  |
      | Other             | Thursday test          | DIAM:0004  |
      | Diagnostics       | Jonathan Test Lab      | DIAM:0005  |

  @NotActual
  Scenario: DIAM:0006 Links validation on Registration page
    Given Marketplace Main page is opened
    When I click 'Register' on Marketplace Main page
      Then New registration page is opened
    When I click 'Membership Terms' link on Registration page
    And I select last tab
      Then Link should be 'Membership Terms URL'
    When I open first tab
    And I click 'Privacy Policy' link on Registration page
    And I select last tab
      Then Link should be 'Privacy Policy URL'
    When I open first tab
    And I click 'Login' link on Registration page
      Then Login page is opened

  @NotActual
  Scenario: DIAM:0007 Validation for mandatory fields
    Given Marketplace Main page is opened
    When I click 'Register' on Marketplace Main page
      Then Registration page is opened
    When I click Register on Personal Details form on Registration page
      Then Registration page is opened
      And Error container is displayed for 'Company name' field on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Organization type | Laboratory |
      | Company name      | Asd        |
    And I click Register on Personal Details form on Registration page
      Then Error container is displayed for 'Position within the organization' field on Registration page
      And Form with error message 'Some information is missing' is displayed on Registration page
      And Form with error message 'Enter your position' is displayed on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Position within the organization | Test position |
    And I click Register on Personal Details form on Registration page
      Then Error container is displayed for 'First name' field on Registration page
      And Form with error message 'Please enter your first name' is displayed on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | First name | Test first name |
    And I click Register on Personal Details form on Registration page
      Then Error container is displayed for 'Last name' field on Registration page
      And Form with error message 'Please enter your last name' is displayed on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Last name | Test last name |
    And I click Register on Personal Details form on Registration page
      Then Error container is displayed for 'Email' field on Registration page
      And Form with error message 'Please enter an email' is displayed on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Email | @mailosaur.io |
    And I click Register on Personal Details form on Registration page
      Then Error container is displayed for 'Create a password' field on Registration page
      And Form with error message 'Enter a new password' is displayed on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Create a password | Testpassword1 |
    And I click Register on Personal Details form on Registration page
      Then Error container is displayed for 'Create a password' field on Registration page
      And Form with error message 'The password fields must match.' is displayed on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Create a password    | Testpassword1 |
      | Repeat your password | Testpassword1 |
    And I click Register on Personal Details form on Registration page
      Then Registration page is opened
      And Form with error message 'Please accept the Terms and conditions in order to sign-up' is displayed on Registration page
      And Error container is displayed for 'I agree to the Membership Terms' field on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Create a password               | Testpassword1 |
      | Repeat your password            | Testpassword1 |
      | I agree to the Membership Terms | true          |
    And I click Register on Personal Details form on Registration page
      Then Registration page is opened
      And Form with error message 'Please accept our Privacy Policy to sign-up' is displayed on Registration page
      And Error container is displayed for 'I accept the Privacy Policy' field on Registration page
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Create a password           | Testpassword1 |
      | Repeat your password        | Testpassword1 |
      | I accept the Privacy Policy | true          |
      And Error container is not displayed for following fields on Registration page:
        | Company name                     |
        | Position within the organization |
        | First name                       |
        | Last name                        |
        | Email                            |
        | Create a password                |
        | Repeat your password             |
        | I agree to the Membership Terms  |
        | I accept the Privacy Policy      |

  @ui @Marketplace @UserRegistration
  Scenario: DIAM:0008 Checking the links (login, register) on the Marketplace header
    Given Marketplace Main page is opened
    When I click 'Login' on Master header
      Then Login page is opened
    When I click back on browser
      Then Marketplace Main page is opened
    When I click 'Register' on Master header
      Then New registration page is opened

  @NotActual
  Scenario Outline: DIAM:0009 Validation for Password field
    Given Marketplace Main page is opened
    When I click 'Register' on Marketplace Main page
      Then Registration page is opened
    When I fill following fields on Personal Details form on Registration page and save as 'user':
      | Organization type                | Laboratory      |
      | Company name                     | Asd             |
      | Position within the organization | Test position   |
      | Title                            | random          |
      | First name                       | Test first name |
      | Last name                        | Test last name  |
      | Email                            | @mailosaur.io   |
      | Create a password                | <password>      |
      | Repeat your password             | <password>      |
      | I agree to the Membership Terms  | true            |
      | I accept the Privacy Policy      | true            |
    And I click Register on Personal Details form on Registration page
      Then Following message is displayed on Personal Details form on Registration page:
      | Must contain letters and at least one number, |
      | capital letter and special character          |

    Examples:
      | password |
      | 123      |
      | password |
      | 12345678 |

  @ui @Marketplace @UserRegistration @SingleThread
  Scenario: DIAM:0078 Validation registration links redirect
    Given Marketplace Main page is opened
    When I click 'Register' on Master header
      Then New registration page is opened
    When I click back on browser
      Then Marketplace Main page is opened
    When I click 'Collaborations' on Master Header form
      Then Collaborations Listing page is opened
    When I click 'Register' on Master header
      Then New registration page is opened
    When I open DXRX app
      Then Marketplace Main page is opened
    When I click 'Organizations' on Master Header form
      Then Organizations Listing page is opened
    When I click 'Register' on Master header
      Then New registration page is opened
    When I open DXRX app
      Then Marketplace Main page is opened
    When I click 'Collaborations' on Master Header form
      Then Collaborations Listing page is opened
    When I open random collaboration on Collaborations Listing page
    And I select last tab
      Then Collaboration Show page is opened
    When I click 'Register' on Master header
      Then New registration page is opened
    When I open DXRX app
      Then Marketplace Main page is opened
    When I click 'Organizations' on Master Header form
      Then Organizations Listing page is opened
    When I open random collaboration on Organizations Listing page
    And I select last tab
      Then Organization Show page is opened
    When I click 'Register' on Master header
      Then New registration page is opened
