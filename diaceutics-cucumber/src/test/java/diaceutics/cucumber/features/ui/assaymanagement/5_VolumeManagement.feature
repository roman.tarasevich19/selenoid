Feature: Volume Management

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I put a Lab 'lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'lab' is displayed in filter results on Labs page
    When I select 'lab' lab on Labs page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0045 Possibility to add a volume without biomarker volumes to the existing lab
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                      |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Disease volume    | 100                         |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0046 Possibility to add a volume with biomarker volumes to the existing lab
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 100                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Biomarker | random |
      | Volume    | 50     |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on Arrow button for the 'volume' volume in Patient volume log grid on Lab Profile page
      Then Biomarkers from 'volume' are displayed in Patient volume log grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0047 Volume duplication impossibility
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                      |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Disease volume    | 100                         |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page using data from 'volume':
      | Year              |
      | Quarter           |
      | Disease           |
      | Disease volume    |
      | Biomarker details |
    And I click 'Save and finish' on New volume page
      Then Message 'A log for this disease already exists for %s %s' for 'volume' is displayed on New volume page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0048 Possibility to edit volumes without biomarker volumes
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                      |
      | Quarter           | Q1                          |
      | Disease           | random                      |
      | Disease volume    | 10                          |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on Edit button for the 'volume' volume on Lab Profile Page
      Then Edit volume page is opened
    When I fill following fields on Edit volume page and save as 'volume':
      | Disease        | random |
      | Disease volume | 10     |
    And I click 'Save and finish' on Edit volume page
      Then Lab Profile page is opened
      And 'Volume successfully updated.' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0049 Possibility to edit volumes with biomarker volumes
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                |
      | Quarter           | Q1                    |
      | Disease           | random                |
      | Disease volume    | 10                    |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Biomarker | random |
      | Volume    | 60     |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on Edit button for the 'volume' volume on Lab Profile Page
      Then Edit volume page is opened
    When I fill following fields on Edit volume page and save as 'volume':
      | Disease        | random |
      | Disease volume | 100    |
    And I fill following fields on Biomarker volume form on Edit volume page and edit biomarker volume for 'volume':
      | Biomarker | random |
      | Volume    | 70     |
    And I click 'Save and finish' on Edit volume page
      Then Lab Profile page is opened
      And 'Volume successfully updated.' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on Arrow button for the 'volume' volume in Patient volume log grid on Lab Profile page
      Then Biomarkers from 'volume' are displayed in Patient volume log grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0050 Possibility to sort volumes
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volumeOne':
      | Year              | random                      |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Disease volume    | 10                          |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volumeOne' is displayed in Patient volume log grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volumeTwo':
      | Year              | random                |
      | Quarter           | Q1                    |
      | Disease           | random                |
      | Disease volume    | 200                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volumeTwo':
      | Biomarker | random |
      | Volume    | 20     |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volumeTwo' is displayed in Patient volume log grid on Lab Profile page
    When I sort data by alphabet in 'Time period' column on Patient volume log Grid
      Then Data in 'Time period' column on Patient volume log Grid sorted according to alphabet
    When I sort data by alphabet in 'Disease' column on Patient volume log Grid
      Then Data in 'Disease' column on Patient volume log Grid sorted according to alphabet
    When I sort data by alphabet in 'Disease vol.' column on Patient volume log Grid
      Then Data in 'Disease vol.' column on Patient volume log Grid sorted according to alphabet
    When I sort data by alphabet in 'Biomarkers' column on Patient volume log Grid
      Then Data in 'Biomarkers' column on Patient volume log Grid sorted according to alphabet
    When I sort data by alphabet in 'Updated' column on Patient volume log Grid
      Then Data in 'Updated' column on Patient volume log Grid sorted according to alphabet

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0051 Data validation in Disease volume field with values less fifty
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                      |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Biomarker details | No biomarker volumes to log |
    And I set random value from 1 to 49 for field 'Disease volume' on New volume page and save for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
      And Value '<50' for Volume 'volume' is displayed in 'Disease vol.' column in Patient volume log Grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0052 Data validation in Disease volume field with values more fifty
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                      |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Biomarker details | No biomarker volumes to log |
    And I set random value from 50 to 100 for field 'Disease volume' on New volume page and save for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
      And Value from Volume 'volume' is displayed in 'Disease vol.' column in Patient volume log Grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0053 Possibility to delete a Volume
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                      |
      | Quarter           | Q4                          |
      | Disease           | random                      |
      | Disease volume    | 100                         |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on Edit button for the 'volume' volume on Lab Profile Page
      Then Edit volume page is opened
    When I click Delete volume on Edit volume page
      Then Delete volume form on Edit volume page is opened
    When I click Delete volume on Delete volume form on Edit volume
      Then Lab Profile page is opened
      And 'Volume successfully deleted' message is displayed on Lab Profile page
      And Volume 'volume' is not displayed in Volumes grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0076 Possibility to add a volume with Disease volume equal to zero
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                      |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Disease volume    | 0                           |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0077 Required fields validation
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I click 'Save and finish' on New volume page
      Then Message 'Year is required' is displayed on required fields on New volume page
      And Message 'Quarter is required' is displayed on required fields on New volume page
      And Message 'Disease is required' is displayed on required fields on New volume page
      And Message 'Disease volume is required' is displayed on required fields on New volume page
      And Red border is displayed for following fields on New volume page:
        | Year           |
        | Quarter        |
        | Disease        |
        | Disease volume |
      And Red border is displayed for following fields on Biomarker volume form on New volume page:
        | Biomarker |
        | Volume    |
    When I set 'true' for 'Same volume for each biomarker' field on Biomarker volume form on New volume page
    And I click 'Save and finish' on New volume page
      Then Red border is displayed for following fields on Biomarker volume form on New volume page:
        | Common Volume |

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0078 Filter a volume by Disease
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volumeOne':
      | Year              | random                      |
      | Quarter           | Q2                          |
      | Disease           | Bladder Cancer              |
      | Disease volume    | 100                         |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volumeOne' is displayed in Patient volume log grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volumeTwo':
      | Year              | random                      |
      | Quarter           | Q3                          |
      | Disease           | Alzheimers Disease          |
      | Disease volume    | 50                          |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volumeTwo' is displayed in Patient volume log grid on Lab Profile page
    When I put a Volume 'volumeOne' on search field 'Type to filter by disease' and press Search icon on Lab Profile page
      Then Volume 'volumeOne' is displayed in Patient volume log grid on Lab Profile page
      And Volume 'volumeTwo' is not displayed in Volumes grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0065 Possibility to add same volume for each biomarker
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 100                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Biomarker | random |
      | Volume    | 20     |
    And I click '+ Add another' button on Biomarker volume form on New volume page
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | New Biomarker | random |
      | New Volume    | 30     |
    And I click '+ Add another' button on Biomarker volume form on New volume page
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | New Biomarker                  | random |
      | New Volume                     | 40     |
      | Same volume for each biomarker | true   |
    And I set value '60' for 'Common Volume' field on Biomarker volume form on New volume page and save to 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on Square button for the 'volume' volume in Patient volume log grid on Lab Profile page
      Then Biomarkers from 'volume' are displayed in Patient volume log grid on Lab Profile page
    When I click on Edit button for the 'volume' volume on Lab Profile Page
      Then Edit volume page is opened
      And Biomarker volumes on Biomarker volume form on Edit volume page should be the same as Biomarker volumes from 'volume'

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0066 Possibility to edit and save volume with existent volume data
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volumeOne':
      | Year              | 2020                        |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Disease volume    | 100                         |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volumeOne' is displayed in Patient volume log grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volumeTwo':
      | Year              | 2020                        |
      | Quarter           | Q2                          |
      | Disease           | random                      |
      | Disease volume    | 200                         |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volumeTwo' is displayed in Patient volume log grid on Lab Profile page
    When I click on Edit button for the 'volumeTwo' volume on Lab Profile Page
      Then Edit volume page is opened
    When I clear 'Disease' field on Edit volume page
    And I fill following fields on Edit volume page using data from 'volumeOne':
      | Disease |
    And I click 'Save and finish' on Edit volume page
      Then Message 'A log for this disease already exists for %s %s' for 'volumeOne' is displayed on Edit volume page
      And Red border is displayed for following fields on Edit volume page:
        | Disease |
    When I click view log on Edit volume page
      Then Edit volume page is opened
      And Volume 'volumeOne' with following fields is displayed on Edit volume page:
        | Year              |
        | Quarter           |
        | Disease           |
        | Disease volume    |
        | Biomarker details |

  @ui @uiAndApi @AssayManagement @VolumeManagement
  Scenario: DIAFE:0120 Data validation in Biomarker volume field with values less fifty
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | random                |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 10                    |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Biomarker | random |
    And I set random value from 0 to 49 for field 'Volume' on Biomarker volume form on New volume page and save for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click on Arrow button for the 'volume' volume in Patient volume log grid on Lab Profile page
      Then Value '<50' for Biomarker from 'volume' is displayed in Biomarker vol. column in Patient volume log Grid on Lab Profile page
