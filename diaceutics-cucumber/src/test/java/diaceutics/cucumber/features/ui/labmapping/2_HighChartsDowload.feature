Feature: Lab Mapping HighCharts Downloading

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
    And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Country          | United States       |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Year From        | 2018                |
      | Month From       | January             |
      | Year To          | 2019                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click 'Charts' tab on Lab Mapping page
      Then Charts form on Lab Mapping page is opened

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0003 Download Percentage of labs performing in-house Biomarker testing versus
    When I click Download chart 'Percentage of labs performing in-house <Biomarker> testing versus sending out for testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Percentage of labs performing in-house <Biomarker> testing versus sending out for testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Percentage of labs performing in-house <Biomarker> testing versus sending out for testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0004 Download Labs performing Biomarker testing by disease market share
    When I click Download chart 'Labs performing <Biomarker> testing by disease market share' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Labs performing <Biomarker> testing by disease market share' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Labs performing <Biomarker> testing by disease market share' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0005 Download Percentage of labs offering Biomarker testing
    When I click Download chart 'Percentage of labs offering <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Percentage of labs offering <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Percentage of labs offering <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0006 Download Disease market share represented by labs offering/not offering
    When I click Download chart '<Disease> market share represented by labs offering/not offering <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart '<Disease> market share represented by labs offering/not offering <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart '<Disease> market share represented by labs offering/not offering <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0007 Download Disease market share analyzed by lab classification
    When I click Download chart '% <Disease> market share analyzed by lab classification' on Charts form on Lab Mapping page
    And I save title to 'title' from chart '% <Disease> market share analyzed by lab classification' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart '% <Disease> market share analyzed by lab classification' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0008 Download In-house methods used for Biomarker testing
    When I click Download chart 'In-house methods used for <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'In-house methods used for <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'In-house methods used for <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0009 Download Disease market share represented by different in-house methods
    When I click Download chart '<Disease> market share represented by different in-house methods used for <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart '<Disease> market share represented by different in-house methods used for <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart '<Disease> market share represented by different in-house methods used for <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0010 Download Top labs Biomarker capabilities by Disease market share
    When I click Download chart 'Top 15 labs <Biomarker> capabilities by <Disease> market share' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Top 15 labs <Biomarker> capabilities by <Disease> market share' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Top 15 labs <Biomarker> capabilities by <Disease> market share' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0011 Download Number of different methods available for in-house Biomarker testing
    When I click Download chart 'Number of different methods available for in-house <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Number of different methods available for in-house <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Number of different methods available for in-house <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0012 Download Disease market share represented by labs using
    When I click Download chart '<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart '<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart '<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0013 Download LDT versus commercial assays
    When I click Download chart 'LDT versus commercial assays' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'LDT versus commercial assays' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'LDT versus commercial assays' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0014 Download Labs using LDTs versus commercial assays by DMS
    When I click Download chart 'Labs using LDTs versus commercial assays by DMS' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Labs using LDTs versus commercial assays by DMS' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Labs using LDTs versus commercial assays by DMS' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0015 Download LDT versus commercial assays by method
    When I click Download chart 'LDT versus commercial assays by method' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'LDT versus commercial assays by method' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'LDT versus commercial assays by method' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0016 Download LDT versus commercial assays by method second
    When I click Download chart 'LDT versus commercial assays by method_2' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'LDT versus commercial assays by method_2' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'LDT versus commercial assays by method_2' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0017 Download Commercial assays used for in-house Biomarker testing
    When I click Download chart 'Commercial assays used for in-house <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Commercial assays used for in-house <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Commercial assays used for in-house <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0018 Download Disease market share represented by commercial assays
    When I click Download chart '<Disease> market share represented by commercial assays used for in-house <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart '<Disease> market share represented by commercial assays used for in-house <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart '<Disease> market share represented by commercial assays used for in-house <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0019 Download Frequency histogram of average TATs for in-house Biomarker
    When I click Download chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0125 Download Frequency histogram of average TATs for in-house Biomarker split
    When I click Download chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Disease> market share' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Disease> market share' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Disease> market share' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0126 Download Segmentation plot showing assay TaTs for in-house Biomarker
    When I click Download chart 'Segmentation plot showing assay TaTs for in-house <Biomarker> testing' on Charts form on Lab Mapping page
    And I save title to 'title' from chart 'Segmentation plot showing assay TaTs for in-house <Biomarker> testing' on Lab Mapping page
    And I save subtitle to 'subtitle' from chart 'Segmentation plot showing assay TaTs for in-house <Biomarker> testing' on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'title'
      And File 'chart.pdf' contains a chart on Lab Mapping with text 'subtitle'

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0128 Download All Charts
    When I click Download all charts on Charts form on Lab Mapping page
    And I save titles to 'titles' from all charts on Lab Mapping page
    And I save subtitles to 'subtitles' from all charts on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains all charts values from 'titles' on Lab Mapping page
      And File 'chart.pdf' contains all charts values from 'subtitles' on Lab Mapping page

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0129 Edit search and Download all charts
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Disease          | Bladder Cancer      |
      | Biomarker/Analog | EPCAM(DEL/DUP ONLY) |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Charts form on Lab Mapping page is opened
    When I click Download all charts on Charts form on Lab Mapping page
    And I save titles to 'titles' from all charts on Lab Mapping page
    And I save subtitles to 'subtitles' from all charts on Lab Mapping page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains all charts values from 'titles' on Lab Mapping page
      And File 'chart.pdf' contains all charts values from 'subtitles' on Lab Mapping page