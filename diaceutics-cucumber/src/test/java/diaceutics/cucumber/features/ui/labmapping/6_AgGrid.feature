Feature: Lab Mapping AG-Grid

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
    And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened
      And Lab Mapping Main page subheader equals 'Analyze and understand lab testing behaviors'
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project        |
      | Disease          | Breast Cancer              |
      | Biomarker/Analog | ALK                        |
      | Country          | United States              |
      | Year From        | 2017                       |
      | Month From       | January                    |
      | Year To          | 2017                       |
      | Month To         | March                      |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Refresh page if grid is empty

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario: DIALM:0030 Validation for lab details columns titles
    When I check 'Lab details' field in AG-Grid Lab summary on Lab Mapping page
    Then columns with following titles should be displayed in AG-Grid Lab summary on Lab Mapping page:
      | Disease market share percent |
      | Test market share percent    |
      | Lab name                     |
      | Address                      |
      | City                         |
      | Classification               |

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario Outline: <test count> Validation of filtering Lab details column <column> in table
    When I check 'Lab details' field in AG-Grid Lab summary on Lab Mapping page
    And I uncheck column '<column>' on Lab Mapping
      Then Column '<column>' is not present on Lab Mapping

    Examples:
      | column                       | test count |
      | Disease market share percent | DIALM:0031 |
      | Test market share percent    | DIALM:0032 |
      | Lab name                     | DIALM:0033 |
      | Address                      | DIALM:0034 |
      | City                         | DIALM:0035 |
      | Classification               | DIALM:0036 |

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario: DIALM:0037 Checkbox for Name column is disabled
    Then Checkbox for column 'Assay name' is disabled on Lab Mapping page

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario Outline: <test count> Validation of filtering column Assay Details <column> in table
    When I uncheck column '<column>' on Lab Mapping
      Then Column '<column>' is not present on Lab Mapping

    Examples:
      | column                               | test count |
      | Description                          | DIALM:0038 |
      | Biomarkers                           | DIALM:0039 |
      | Where is it performed?               | DIALM:0040 |
      | Send out lab                         | DIALM:0041 |
      | Detects germline/somatic alterations | DIALM:0042 |
      | Specimen tested                      | DIALM:0043 |
      | Method                               | DIALM:0044 |
      | Method description                   | DIALM:0045 |
      | Commercial assays                    | DIALM:0046 |
      | Classification                       | DIALM:0047 |
      | Turnaround time                      | DIALM:0048 |
      | Ontology                             | DIALM:0049 |
      | Variants included                    | DIALM:0050 |
      | Sensitivity                          | DIALM:0051 |
      | Report format                        | DIALM:0052 |

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario Outline: <test count> Check sorting of Lab details <column>
    When I check 'Lab details' field in AG-Grid Lab summary on Lab Mapping page
    And I uncheck column 'Assay details' on Lab Mapping
    When I click on Lab Details column '<column>' title on Lab Mapping
      Then I expand all rows in AG-Grid on Lab Mapping
      And Sorted by 'asc' icon is present for Lab Details column '<column>'
      And Lab details column '<column>' values sorted by 'asc'
    When I click on Lab Details column '<column>' title on Lab Mapping
      Then I expand all rows in AG-Grid on Lab Mapping
      And Sorted by 'desc' icon is present for Lab Details column '<column>'
      And Lab details column '<column>' values sorted by 'desc'

    Examples:
      | column                       | test count |
      | Disease market share percent | DIALM:0053 |
      | Test market share percent    | DIALM:0054 |
      | Lab name                     | DIALM:0055 |
      | Address                      | DIALM:0056 |
      | City                         | DIALM:0057 |
      | Classification               | DIALM:0058 |

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario Outline: <test count> Check filtering Menu Options Assay Details with <column> in table
    When I uncheck column 'Assay details' on Lab Mapping
      Then I collapse Lab Details on Lab Mapping
      And I check '<column>' field in AG-Grid Lab summary on Lab Mapping page
    When I click on Assay Details column '<column>' title on Lab Mapping
      Then I expand all rows in AG-Grid on Lab Mapping
      And Sorted by 'asc' icon is present for Assay Details column '<column>'
      And Assay details column '<column>' values sorted by 'asc'
    When I click on Assay Details column '<column>' title on Lab Mapping
      Then I expand all rows in AG-Grid on Lab Mapping
      And Sorted by 'desc' icon is present for Assay Details column '<column>'
      And Assay details column '<column>' values sorted by 'desc'

    Examples:
      | column                               | test count |
      | Name                                 | DIALM:0059 |
      | Description                          | DIALM:0060 |
      | Biomarkers                           | DIALM:0061 |
      | Where is it performed?               | DIALM:0062 |
      | Send out lab                         | DIALM:0063 |
      | Detects germline/somatic alterations | DIALM:0064 |
      | Specimen tested                      | DIALM:0065 |
      | Method                               | DIALM:0066 |
      | Method description                   | DIALM:0067 |
      | Commercial assays                    | DIALM:0068 |
      | Classification                       | DIALM:0069 |
      | Turnaround time                      | DIALM:0070 |
      | Ontology                             | DIALM:0071 |
      | Variants included                    | DIALM:0072 |
      | Sensitivity                          | DIALM:0073 |
      | Report format                        | DIALM:0074 |

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario Outline: <test count> Filtering Address column <mode> mode
    When I uncheck column 'Assay details' on Lab Mapping
    And I check 'Lab details' field in AG-Grid Lab summary on Lab Mapping page
    And I click 'Data' tab on Lab Mapping page
      Then I open Menu for Lab Details column 'Address' on Lab Mapping
      And I select filter type '<mode>' on Lab Mapping
      And I fill value '<value>' in filter body input on Lab Mapping
      And I click 'Data' tab on Lab Mapping page
    When I expand all rows in AG-Grid on Lab Mapping
      Then I read data from Lab details column 'Address' and check values with filter '<value>' and type '<mode>'

    Examples:
      | mode         | value        | test count |
      | Contains     | COLU         | DIALM:0075 |
      | Not contains | COLUMBIA     | DIALM:0076 |
      | Equals       | 1447 YORK CT | DIALM:0077 |
      | Not equal    | 1447 YORK CT | DIALM:0078 |
      | Starts with  | 320          | DIALM:0079 |
      | Ends with    | RK           | DIALM:0080 |

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario: DIALM:0081 Filtering Lab Name
    When I check 'Lab details' field in AG-Grid Lab summary on Lab Mapping page
      Then I uncheck column 'Assay details' on Lab Mapping
    When I open Menu for Lab Details column 'Lab name' on Lab Mapping
    And I set value 'false' for filter box '(Select All)'
      Then Lab mapping table is empty
    When I set value 'true' for filter box '100 PLAZA CLINICAL LAB INC'
      Then I click 'Data' tab on Lab Mapping page
    When I expand all rows in AG-Grid on Lab Mapping
      Then I read data with filter '100 PLAZA CLINICAL LAB INC' for Lab details column 'Lab name' on Lab Mapping

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario Outline: <test count> Filtering City column <mode> mode
    When I uncheck column 'Assay details' on Lab Mapping
      Then I check 'Lab details' field in AG-Grid Lab summary on Lab Mapping page
    When I open Menu for Lab Details column 'City' on Lab Mapping
      Then I select filter type '<mode>' on Lab Mapping
      And I fill value '<value>' in filter body input on Lab Mapping
      And I click 'Data' tab on Lab Mapping page
    When I expand all rows in AG-Grid on Lab Mapping
      Then I read data from Lab details column 'City' and check values with filter '<value>' and type '<mode>'

    Examples:
      | mode         | value       | test count |
      | Contains     | NEW         | DIALM:0082 |
      | Not contains | NEW         | DIALM:0083 |
      | Equals       | ALISO VIEJO | DIALM:0084 |
      | Not equal    | NEW YORK    | DIALM:0085 |
      | Starts with  | PHO         | DIALM:0086 |
      | Ends with    | RK          | DIALM:0087 |

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario Outline: <test count> Check filtering Menu Options Assay Details with <column> in table
    When I uncheck column 'Assay details' on Lab Mapping
      Then I collapse Lab Details on Lab Mapping
      And I check '<column>' field in AG-Grid Lab summary on Lab Mapping page
    When I open Menu for Assay Details column '<column>' on Lab Mapping
    And I set value 'false' for filter box '(Select All)' on Lab Mapping
      Then Lab mapping table is empty
    When I set value 'true' for filter box '<filter>' on Lab Mapping
      Then I click 'Data' tab on Lab Mapping page
    When I expand all rows in AG-Grid on Lab Mapping
      Then I read data with filter '<filter>' for Assay details column '<column>' on Lab Mapping

    Examples:
      | column                               | filter               | test count |
      | Assay status                         | ACTIVE               | DIALM:0088 |
      | Where is it performed?               | In-house             | DIALM:0089 |
      | Detects germline/somatic alterations | Germline             | DIALM:0090 |
      | Specimen tested                      | Blood                | DIALM:0091 |
      | Method                               | Digital Droplet PCR  | DIALM:0092 |
      | Commercial assays                    | Unspecified          | DIALM:0093 |
      | Classification                       | Commercial Assay     | DIALM:0094 |
      | Turnaround time                      | 1 days               | DIALM:0095 |
      | Ontology                             | DELETION             | DIALM:0096 |
      | Variants included                    | Entire gene coverage | DIALM:0097 |
      | Sensitivity                          | Unspecified          | DIALM:0098 |
      | Report format                        | Unspecified          | DIALM:0099 |

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario Outline: <test count> Filtering Biomarkers column <mode> mode
    When I open Menu for Assay Details column 'Biomarkers' on Lab Mapping
      Then I select filter type '<mode>' on Lab Mapping
      And I fill value '<value>' in filter body input on Lab Mapping
      And I click 'Data' tab on Lab Mapping page
    When I expand all rows in AG-Grid on Lab Mapping
      Then I read data from Assay details column 'Biomarkers' and check values with filter '<value>' and type '<mode>'

    Examples:
      | mode         | value | test count |
      | Contains     | ALK   | DIALM:0100 |
      | Not contains | BRAF  | DIALM:0101 |
      | Equals       | ALK   | DIALM:0102 |
      | Not equal    | BRAF  | DIALM:0103 |
      | Starts with  | AL    | DIALM:0104 |
      | Ends with    | LK    | DIALM:0105 |

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario: DIALM:0106 Check options for Classification column
    When I uncheck column 'Assay details' on Lab Mapping
      Then I collapse Lab Details on Lab Mapping
      And I check 'Classification' field in AG-Grid Lab summary on Lab Mapping page
    When I open Menu for Assay Details column 'Classification' on Lab Mapping
      Then I check that options for Assay Details are present in opened column filter
      | Commercial Assay         |
      | Lab developed test (LDT) |
      | Unspecified              |

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario Outline: <test count> Filtering Regulatory status column <mode> mode
    When I uncheck column 'Assay details' on Lab Mapping
    And I check 'Regulatory status' field in AG-Grid Lab summary on Lab Mapping page
      Then I open Menu for Assay Details column 'Regulatory status' on Lab Mapping
      And I select filter type '<mode>' on Lab Mapping
      And I fill value '<value>' in filter body input on Lab Mapping
      And I click 'Data' tab on Lab Mapping page
    When I expand all rows in AG-Grid on Lab Mapping
      Then I read data from Assay details column 'Regulatory status' and check values with filter '<value>' and type '<mode>'

    Examples:
      | mode     | value                 | test count |
      | Equals   | FDA 510K APPROVED KIT | DIALM:0112 |
      | Contains | FDA PMA APPROVED KIT  | DIALM:0113 |
      | Equals   | IVD-CE                | DIALM:0114 |
      | Equals   | RUO/IUO               | DIALM:0115 |
      | Equals   | Unspecified           | DIALM:0116 |

  @ui @LabMapping @LabMappingFilteringAssayDetails
  Scenario: DIALM:0117 Show Not applicable in Send out Lab where performed In-house
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Country          | United States |
      | Disease          | Breast Cancer |
      | Biomarker/Analog | ALK           |
      | Year From        | 2018          |
      | Month From       | March         |
      | Year To          | 2018          |
      | Month To         | March         |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Lab Mapping page is opened
    When I uncheck column 'Assay details' on Lab Mapping
      Then I check 'Send out lab' field in AG-Grid Lab summary on Lab Mapping page
      And I check 'Where is it performed?' field in AG-Grid Lab summary on Lab Mapping page
    When I open Menu for Assay Details column 'Where is it performed?' on Lab Mapping
    And I set value 'false' for filter box '(Select All)' on Lab Mapping
      Then Lab mapping table is empty
    When I set value 'true' for filter box 'In-house' on Lab Mapping
    And I click 'Data' tab on Lab Mapping page
    And I expand all rows in AG-Grid on Lab Mapping
      Then I read data in Assay details column 'Send out lab' on Lab Mapping and values equals 'Not applicable'

  @ui @LabMapping
  Scenario: DIALM:0131 Click on Lab and Assay name and open new tab
    When I click on entry 'QUEST DIAGNOSTICS' in AG-Grid Lab summary on Lab Mapping page
    And I select last tab
      Then Lab Profile page is opened
    When I close tab number 2
    And I select last tab
      Then I switch view to 'Assays' on Lab Mapping page
    When I click on entry 'EAST TENNESSEE COMMUNITY OPEN MRI' in AG-Grid Lab summary on Lab Mapping page
      Then Lab Profile page is opened

  @ui @uiAndApi @LabMapping
  Scenario: DIALM:0132 Ag Grid heading icons and values and after edit search
    When I execute POST request Generates a lab mapping using following data:
      | projectId    | 1         |
      | countryCode  | USA       |
      | criteriaType | BIOMARKER |
      | criteriaId   | 1         |
      | diseaseIds   | 2         |
      | fromYear     | 2017      |
      | fromMonth    | 1         |
      | toYear       | 2017      |
      | toMonth      | 3         |
      Then the status code is 200
      And save from response number of labs as 'numberOfLabs'
      And save from response number of labs with assays for biomarker as 'numberOfLabsAssaysForBiomarker'
      And save from response number of patient tests as 'numberOfPatientTests'
      And Icon 'labs' is displayed on heading panel on Lab Mapping page
      And Icon 'labs assays for biomarker' is displayed on heading panel on Lab Mapping page
      And Icon 'patient tests' is displayed on heading panel on Lab Mapping page
    When I get number from 'labs' heading banner on Lab Mapping page and save to 'numberOfLabsOnUI'
    And I get number from 'labs assays for biomarker' heading banner on Lab Mapping page and save to 'numberOfLabsAssaysForBiomarkerUI'
    And I get number from 'patient tests' heading banner on Lab Mapping page and save to 'numberOfPatientTestsUI'
      Then I get number of heading banner 'numberOfLabsOnUI' and compare with saved value 'numberOfLabs' on Lab Mapping page
      And I get number of heading banner 'numberOfLabsAssaysForBiomarkerUI' and compare with saved value 'numberOfLabsAssaysForBiomarker' on Lab Mapping page
      And I get number of heading banner 'numberOfPatientTestsUI' and compare with saved value 'numberOfPatientTests' on Lab Mapping page
    When I click 'Edit' on Lab Mapping page
    Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Country          | United States |
      | Disease          | Breast Cancer |
      | Biomarker/Analog | ALK           |
      | Year From        | 2018          |
      | Month From       | March         |
      | Year To          | 2018          |
      | Month To         | March         |
    And I click 'Apply' on Edit settings form on Lab Mapping
    Then Lab Mapping page is opened
    When I execute POST request Generates a lab mapping using following data:
      | projectId    | 1         |
      | countryCode  | USA       |
      | criteriaType | BIOMARKER |
      | criteriaId   | 1         |
      | diseaseIds   | 2         |
      | fromYear     | 2018      |
      | fromMonth    | 3         |
      | toYear       | 2018      |
      | toMonth      | 3         |
    Then the status code is 200
      And save from response number of labs as 'numberOfLabsEdit'
      And save from response number of labs with assays for biomarker as 'numberOfLabsAssaysForBiomarkerEdit'
      And save from response number of patient tests as 'numberOfPatientTestsEdit'
    When I get number from 'labs' heading banner on Lab Mapping page and save to 'numberOfLabsOnUIedit'
    And I get number from 'labs assays for biomarker' heading banner on Lab Mapping page and save to 'numberOfLabsAssaysForBiomarkerUIedit'
    And I get number from 'patient tests' heading banner on Lab Mapping page and save to 'numberOfPatientTestsUIedit'
      Then I get number of heading banner 'numberOfLabsOnUIedit' and compare with saved value 'numberOfLabsEdit' on Lab Mapping page
      And I get number of heading banner 'numberOfLabsAssaysForBiomarkerUIedit' and compare with saved value 'numberOfLabsAssaysForBiomarkerEdit' on Lab Mapping page
      And I get number of heading banner 'numberOfPatientTestsUIedit' and compare with saved value 'numberOfPatientTestsEdit' on Lab Mapping page

  @ui @uiAndApi @LabMapping
  Scenario: DIALM:0133 Ag Grid heading icons and values with zero values
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Country          | United States  |
      | Disease          | Thyroid Cancer |
      | Biomarker/Analog | BRIP           |
      | Year From        | 2017           |
      | Month From       | January        |
      | Year To          | 2017           |
      | Month To         | January        |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Lab Mapping page is opened
    When I execute POST request Generates a lab mapping using following data:
      | projectId    | 1         |
      | countryCode  | USA       |
      | criteriaType | BIOMARKER |
      | criteriaId   | 175       |
      | diseaseIds   | 21        |
      | fromYear     | 2017      |
      | fromMonth    | 1         |
      | toYear       | 2017      |
      | toMonth      | 1         |
      Then the status code is 200
      And save from response number of labs as 'numberOfLabs'
      And save from response number of labs with assays for biomarker as 'numberOfLabsAssaysForBiomarker'
      And save from response number of patient tests as 'numberOfPatientTests'
    When I get number from 'labs' heading banner on Lab Mapping page and save to 'numberOfLabsOnUI'
    And I get number from 'labs assays for biomarker' heading banner on Lab Mapping page and save to 'numberOfLabsAssaysForBiomarkerUI'
    And I get number from 'patient tests' heading banner on Lab Mapping page and save to 'numberOfPatientTestsUI'
      Then I get number of heading banner 'numberOfLabsOnUI' and compare with saved value 'numberOfLabs' on Lab Mapping page
      And I get number of heading banner 'numberOfLabsAssaysForBiomarkerUI' and compare with saved value 'numberOfLabsAssaysForBiomarker' on Lab Mapping page
      And I get number of heading banner 'numberOfPatientTestsUI' and compare with saved value 'numberOfPatientTests' on Lab Mapping page

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario Outline: <test count> Check tooltip for Assay details columns
    When I uncheck column 'Assay details' on Lab Mapping
    And I check '<column>' field in AG-Grid Lab summary on Lab Mapping page
      Then I get tooltip from Assay Details column '<column>' and compare with '<tooltip>' on Lab Mapping page
    Examples:
      | column                               | tooltip                   | test count |
      | Detects germline/somatic alterations | Alteration type detected  | DIALM:0136 |
      | Specimen tested                      | Types accepted by lab     | DIALM:0137 |
      | Method                               | Technology used for test  | DIALM:0138 |
      | Sensitivity                          | Test performance info     | DIALM:0139 |
      | Scoring method                       | Interpretation of results | DIALM:0140 |

  @ui @LabMapping @LabMappingFilteringLabDetails
  Scenario: DIALM:0141 Check tooltip for Lab name and LTMS DMS
    When I get tooltip for Lab name 'QUEST DIAGNOSTICS' on Lab Mapping and compare with 'View lab profile'
      Then I click 'Data' tab on Lab Mapping page
    When I get tooltip for 'LTMS' label on Lab Mapping and compare with 'Lab test market share'
      Then I click 'Data' tab on Lab Mapping page
      And I get tooltip for 'DMS' label on Lab Mapping and compare with 'Disease market share'
    When I switch view to 'Assays' on Lab Mapping page
      Then I get tooltip for Lab name 'EAST TENNESSEE COMMUNITY OPEN MRI' on Lab Mapping and compare with 'View lab profile'