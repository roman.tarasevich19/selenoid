Feature: Physician Mapping HighCharts

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Physician Mapping' tools
    And I select last tab
      Then Physician Mapping Main page is opened

  @ui @PhysicianMapping @PhysicianMappingHighCharts @SingleThread
  Scenario: DIAPM:0004 Validation for All HighCharts downloading
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2017                |
      | Month From           | random              |
      | Year To              | 2018                |
      | Month To             | random              |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I click 'Charts' tab on Physician Mapping page
      Then Charts form on Physician Mapping page is opened
    When I click Download all charts on Charts form on Physician Mapping page
      Then File 'chart.pdf' is downloaded
      And I compare charts from file 'chart.pdf' and from Charts form on Physician Mapping page

  @ui @PhysicianMapping @PhysicianMappingHighCharts @SingleThread
  Scenario Outline: <test count> Validation for HighChart downloading
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2017                |
      | Month From           | random              |
      | Year To              | 2018                |
      | Month To             | random              |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I click 'Charts' tab on Physician Mapping page
      Then Charts form on Physician Mapping page is opened
    When I click Download chart '<chart>' on Charts form on Physician Mapping page and save charts title as 'title'
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart with title 'title'

    Examples:
      | chart                                                                      | test count |
      | National view by number of <Disease> physicians                            | DIAPM:0005 |
      | National view by biomarker testing rate                                    | DIAPM:0006 |
      | National view by number of <Biomarker> physicians                          | DIAPM:0007 |
      | Top 5 specialties for <Disease> treating physicians                        | DIAPM:0008 |
      | Top 5 specialties for <Biomarker> treating physicians                      | DIAPM:0009 |
      | Lab classification preference by physician specialty                       | DIAPM:0010 |
      | Percentage of physicians using labs that offer the required biomarker test | DIAPM:0011 |
      | Physician specialties                                                      | DIAPM:0012 |

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario Outline: <test count> Validation for Preferred Lab <type>
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | Breast Cancer       |
      | Biomarker / Analogue | CD20                |
      | Year From            | 2017                |
      | Month From           | January             |
      | Year To              | 2017                |
      | Month To             | January             |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I set value 'false' for all columns box
      Then I set value 'true' for column 'Preferred laboratory for disease testing'
      And I set value 'true' for column 'Preferred lab offer required biomarker testing?'
    When I open menu for column 'Preferred laboratory for disease testing'
    And I set value 'false' for filter box '(Select All)'
      Then Physician table is empty
    When I set up '<lab>' to the filter on Physician Mapping page
    And I set value 'true' for filter box '<lab>'
      Then I get number of physicians from AG-Grid on Physician Mapping page and save to 'numberPhysicianGrid'
      And I read data in column 'Preferred lab offer required biomarker testing?' on Physician Mapping and values equals '<isRequired>' on Physician Mapping page
    When I click 'Charts' tab on Physician Mapping page
      Then Charts form on Physician Mapping page is opened
      And 'numberPhysicianGrid' must be the same as on 'Percentage of physicians using labs that offer the required biomarker test' chart on Physician Mapping page

    Examples:
      | test count | type              | lab                         | isRequired |
      | DIAPM:0110 | with biomarker    | AMERICAN PATHOLOGY PARTNERS | Yes        |
      | DIAPM:0111 | without biomarker | UNIVERSITY OF PITTSBURGH    | No         |

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0112 Validation for Preferred Lab Blank
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | Breast Cancer       |
      | Biomarker / Analogue | CD20                |
      | Year From            | 2017                |
      | Month From           | January             |
      | Year To              | 2017                |
      | Month To             | January             |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I set value 'false' for all columns box
      Then I set value 'true' for column 'Preferred laboratory for disease testing'
      And I set value 'true' for column 'Preferred lab offer required biomarker testing?'
    When I open menu for column 'Preferred lab offer required biomarker testing?'
    And I set value 'false' for filter box '(Select All)'
      Then Physician table is empty
    When I set value 'true' for filter box '(Blanks)'
      Then I get number of physicians from AG-Grid on Physician Mapping page and save to 'numberPhysicianGrid'
      And I read data in column 'Preferred lab offer required biomarker testing?' on Physician Mapping and values equals '' on Physician Mapping page
    When I click 'Charts' tab on Physician Mapping page
      Then Charts form on Physician Mapping page is opened
      And Number from subtitle from chart 'Percentage of physicians using labs that offer the required biomarker test' should be equal '0' on Physician Mapping page

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0113 Validation for Preferred Lab Yes and No
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | Breast Cancer       |
      | Biomarker / Analogue | CD20                |
      | Year From            | 2017                |
      | Month From           | January             |
      | Year To              | 2017                |
      | Month To             | January             |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I set value 'false' for all columns box
      Then I set value 'true' for column 'Preferred laboratory for disease testing'
      And I set value 'true' for column 'Preferred lab offer required biomarker testing?'
    When I open menu for column 'Preferred lab offer required biomarker testing?'
    And I set value 'false' for filter box '(Select All)'
      Then Physician table is empty
    When I set value 'true' for filter box 'Yes'
    And I set value 'true' for filter box 'No'
      Then I get number of physicians from AG-Grid on Physician Mapping page and save to 'numberPhysicianGrid'
    When I click 'Charts' tab on Physician Mapping page
      Then Charts form on Physician Mapping page is opened
      And 'numberPhysicianGrid' must be the same as on 'Percentage of physicians using labs that offer the required biomarker test' chart on Physician Mapping page

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0114 Validation for Preferred Lab Yes and No Edit
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | Breast Cancer       |
      | Biomarker / Analogue | CD20                |
      | Year From            | 2017                |
      | Month From           | January             |
      | Year To              | 2017                |
      | Month To             | January             |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I click 'Edit' on Physician Mapping page
      Then Edit settings form is opened on Physician Mapping page
    When I fill following fields on Edit settings form on Physician Mapping page and save 'search filter':
      | Disease              | Urothelial Cancer |
      | Biomarker / Analogue | HEBP1             |
      | Year From            | 2018              |
      | Month From           | January           |
      | Year To              | 2018              |
      | Month To             | January           |
    And I click 'Apply' on Edit settings form on Physician Mapping
    And I click 'Data' tab on Physician Mapping page
      Then Physician Mapping page is opened
    When I get number of physicians from AG-Grid on Physician Mapping page and save to 'numberPhysicianGrid'
      Then I click 'Charts' tab on Physician Mapping page
      And Charts form on Physician Mapping page is opened
      And Number from subtitle from chart 'Percentage of physicians using labs that offer the required biomarker test' should be equal '' on Physician Mapping page

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0115 Validation for Preferred Lab calculate physicians
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | Breast Cancer       |
      | Biomarker / Analogue | CD20                |
      | Year From            | 2017                |
      | Month From           | January             |
      | Year To              | 2017                |
      | Month To             | January             |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I get number of physicians from AG-Grid on Physician Mapping page and save to 'numberPhysicianGrid'
      Then I click 'Charts' tab on Physician Mapping page
      And Charts form on Physician Mapping page is opened
      And I get number from subtitle from chart 'Percentage of physicians using labs that offer the required biomarker test' on Physician Mapping page and save to 'fromChart'
    When I click 'Data' tab on Physician Mapping page
      Then Physician Mapping page is opened
    When I set value 'false' for all columns box
      Then I set value 'true' for column 'Preferred laboratory for disease testing'
      And I set value 'true' for column 'Preferred lab offer required biomarker testing?'
    When I open menu for column 'Preferred lab offer required biomarker testing?'
    And I set value 'false' for filter box '(Select All)'
      Then Physician table is empty
    When I set value 'true' for filter box '(Blanks)'
      Then I get number of physicians from AG-Grid on Physician Mapping page and save to 'numberPhysicianGridBlanks'
      And Int sum of 'numberPhysicianGridBlanks' and 'fromChart' should be equal 'numberPhysicianGrid' on Physician Mapping page