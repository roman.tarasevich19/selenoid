Feature: Ask question for collaboration

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I click View all public collaborations on Home page
      Then Collaborations Listing page is opened
    When I open Search form on on Master Header form
    And I set following fields on Search form and save as 'filter':
      | Location | Afghanistan |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I open Collaboration 'Test collaboration' on Collaborations Listing page
    And I select last tab
      Then Collaboration Show page is opened
    When I click Ask a question button on Collaborations Show page
      Then Question form is opened on Collaborations Show page

  @ui @Marketplace @Collaboration @AskQuestionForOrganisation @SingleThread
  Scenario: DIAM:0026 Possibility to Ask question for collaboration with valid data input
    When I put random message in Question form on Collaborations Show page and save as 'message'
    And I click Send on Question form on Collaborations Show page
      Then Inform Form with following message is displayed on Collaborations Show page:
        | Your message has been sent! You can find all your messages and discussions directly in your Dashboard. |
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'User for testing collaboration' user
      Then Home page is opened
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Messages' link on Collaboration Header form
      Then Messages page is opened
      And Message 'message' from 'ADMIN' user is displayed on Messages page
      And Message 'message' from 'ADMIN' user was sent 'Today'

  @ui @Marketplace @Collaboration @AskQuestionForOrganisation @SingleThread
  Scenario Outline: <test count> Possibility to Ask question for collaboration with <file type> file adding
    When I add a file '<file name>' on Question form on Collaborations Show page
      Then File '<file name>' is added on Question form on Collaborations Show page
    When I put random message in Question form on Collaborations Show page and save as 'message'
    And I click Send on Question form on Collaborations Show page
      Then Inform Form with following message is displayed on Collaborations Show page:
        | Your message has been sent! You can find all your messages and discussions directly in your Dashboard. |
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'User for testing collaboration' user
      Then Home page is opened
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Messages' link on Collaboration Header form
      Then Messages page is opened
      And Message 'message' from 'ADMIN' user is displayed on Messages page
      And Message 'message' from 'ADMIN' user was sent 'Today'
    When I click message button for 'message' from 'ADMIN' user on Messages page
      Then Conversation page is opened
      And File '<file name>' is attached to 'message' on Conversation page
      And Message 'message' was sent from a 'ADMIN' user on Conversation page

    Examples:
      | file name     | test count | file type |
      | TestDOCX.docx | DIAM:0027  | docx      |
      | TestGIF.gif   | DIAM:0028  | gif       |
      | TestJPG.jpg   | DIAM:0029  | jpg       |
      | TestPDF.pdf   | DIAM:0030  | pdf       |
      | TestPNG.png   | DIAM:0031  | png       |
      | TestPPTX.pptx | DIAM:0032  | pptx      |
      | TestRTF.rtf   | DIAM:0033  | rtf       |
      | TestXLS.xls   | DIAM:0034  | xls       |

  @ui @Marketplace @Collaboration @AskQuestionForOrganisation
  Scenario: DIAM:0035 Ask question for collaboration with invalid data input
    When I click Send on Question form on Collaborations Show page
      Then Error message 'Message body cannot be empty' is displayed on Question form on Collaborations Show page
    When I add a file 'TestDOCX.docx' on Question form on Collaborations Show page
      Then File 'TestDOCX.docx' is added on Question form on Collaborations Show page
    When I click Send on Question form on Collaborations Show page
      Then Error message 'Message body cannot be empty' is displayed on Question form on Collaborations Show page
