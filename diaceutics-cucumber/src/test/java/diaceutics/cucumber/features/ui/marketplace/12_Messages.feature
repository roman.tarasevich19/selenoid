Feature: Messages

  @ui @Marketplace @Collaboration @Messages
  Scenario: DIAM:0064 Validation that Messages are available for all logged users
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
      And 'Messages' link is displayed on Collaboration Header form
    When I click 'Messages' link on Collaboration Header form
      Then Messages page is opened
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'User without collaboration' user
      Then Home page is opened
    When I click 'Home' on Master Header form
      Then Home page is opened
      And 'Messages' link is displayed on Collaboration Header form
    When I click 'Messages' link on Collaboration Header form
      Then Messages page is opened
