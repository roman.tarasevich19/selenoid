Feature: Physician Mapping search

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Physician Mapping' tools
    And I select last tab
      Then Physician Mapping Main page is opened

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0001 Required fields validation
    When I click 'Start' on Physician Mapping Main page
      Then Message 'Some items below need your attention.' is displayed on Physician Mapping Main page
      And Message 'Please select a project' is displayed on required fields on Physician Mapping Main page
    When I fill following fields on Physician Mapping Main page:
      | Project | Full Access Project |
      Then Message 'Disease is required' is displayed on required fields on Physician Mapping Main page
      And Message 'A biomarker is required' is displayed on required fields on Physician Mapping Main page
      And Message 'You specified an invalid date range' is displayed on required fields on Physician Mapping Main page
    When I fill following fields on Physician Mapping Main page:
      | Disease              | random  |
      | Biomarker / Analogue | random  |
      | Year From            | 2018    |
      | Month From           | April   |
      | Year To              | 2018    |
      | Month To             | January |
    And I click 'Start' on Physician Mapping Main page
      Then Message 'Some items below need your attention.' is displayed on Physician Mapping Main page
      And Message 'You specified an invalid date range' is displayed on required fields on Physician Mapping Main page

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0002 Search filter editing
    When I fill following fields on Physician Mapping Main page and save 'search filter':
      | Project              | Full Access Project |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2018                |
      | Month From           | random              |
      | Year To              | 2019                |
      | Month To             | random              |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Physician Mapping page
        | Disease    |
        | Biomarker  |
        | Date Range |
    When I click 'Edit' on Physician Mapping page
      Then Edit settings form is opened on Physician Mapping page
    When I fill following fields on Edit settings form on Physician Mapping page and save 'search filter':
      | Disease              | random |
      | Biomarker / Analogue | random |
      | Year From            | 2018   |
      | Month From           | random |
      | Year To              | 2019   |
      | Month To             | random |
    And I click 'Apply' on Edit settings form on Physician Mapping
      Then Physician Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Physician Mapping page
        | Disease    |
        | Biomarker  |
        | Date Range |

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0003 Possibility to return to main page
    When I fill following fields on Physician Mapping Main page:
      | Project              | Full Access Project |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2017                |
      | Month From           | random              |
      | Year To              | 2018                |
      | Month To             | random              |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
    When I click Main page link on Physician Mapping page
      Then Physician Mapping Main page is opened

  @ui @PhysicianMapping @PhysicianMappingHighCharts
  Scenario: DIAPM:0105 Canceled filter editing
    When I fill following fields on Physician Mapping Main page and save 'search filter':
      | Project              | Full Access Project |
      | Disease              | random              |
      | Biomarker / Analogue | random              |
      | Year From            | 2018                |
      | Month From           | random              |
      | Year To              | 2019                |
      | Month To             | random              |
    And I click 'Start' on Physician Mapping Main page
      Then Physician Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Physician Mapping page
        | Disease    |
        | Biomarker  |
        | Date Range |
    When I click 'Edit' on Physician Mapping page
      Then Edit settings form is opened on Physician Mapping page
    When I fill following fields on Edit settings form on Physician Mapping page and save 'new search filter':
      | Disease              | random |
      | Biomarker / Analogue | random |
      | Year From            | 2019   |
      | Month From           | random |
      | Year To              | 2021   |
      | Month To             | random |
    And I click 'Cancel' on Edit settings form on Physician Mapping
      Then Physician Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Physician Mapping page
        | Disease    |
        | Biomarker  |
        | Date Range |
