Feature: Lab Mapping DMS, BMS Ordering

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0025 Filtering by DMS descending order
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Fallopian Cancer    |
      | Biomarker/Analog | BRCA                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When Value 'Descending' is displayed in 'Order' field on Lab Mapping page
      Then Data in AG-Grid Lab summary on Lab Mapping page is sorted in descending order for DMS sorting

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0026 Filtering by DMS ascending order
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Fallopian Cancer    |
      | Biomarker/Analog | BRCA                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I fill following fields on Lab Mapping page:
      | Sort lab by | Disease market share (DMS) |
      | Order       | Ascending                  |
      Then Data in AG-Grid Lab summary on Lab Mapping page is sorted in ascending order for DMS sorting

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0027 Filtering by LTMS descending order
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Fallopian Cancer    |
      | Biomarker/Analog | BRCA                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I fill following fields on Lab Mapping page:
      | Sort lab by | Lab test market share (LTMS) |
      | Order       | Descending                   |
      Then Data in AG-Grid Lab summary on Lab Mapping Results page is sorted in descending order for LTMS sorting

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0028 Filtering by LTMS ascending order
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Fallopian Cancer    |
      | Biomarker/Analog | BRCA                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I fill following fields on Lab Mapping page:
      | Sort lab by | Lab test market share (LTMS) |
      | Order       | Ascending                    |
      Then Data in AG-Grid Lab summary on Lab Mapping page is sorted in ascending order for LTMS sorting

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0029 Checking 2dp rounding for lab details columns
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | BRCA                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I check 'Lab details' field in AG-Grid Lab summary on Lab Mapping page
    And I click random Lab from AG-Grid Lab summary and save lab name as 'lab'
      Then Values for 'Disease market share percent' column should be the same as 'Disease market share' label for Lab 'lab'
      And Values for 'Test market share percent' column should be the same as 'Lab test market share' label for Lab 'lab'

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0118 Filtering by LTMS ascending order for non US search
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project     |
      | Disease          | Acute Myeloid Leukaemia |
      | Biomarker/Analog | ALK                     |
      | Country          | Belarus                 |
      | Year From        | 2017                    |
      | Month From       | January                 |
      | Year To          | 2021                    |
      | Month To         | January                 |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I fill following fields on Lab Mapping page:
      | Sort lab by | Lab test market share (LTMS) |
      | Order       | Ascending                    |
      Then Data in AG-Grid Lab summary on Lab Mapping page is sorted in ascending order for LTMS sorting

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0119 Filtering by LTMS Descending order for non US search
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project     |
      | Disease          | Acute Myeloid Leukaemia |
      | Biomarker/Analog | ALK                     |
      | Country          | Belarus                 |
      | Year From        | 2017                    |
      | Month From       | January                 |
      | Year To          | 2021                    |
      | Month To         | January                 |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I fill following fields on Lab Mapping page:
      | Sort lab by | Lab test market share (LTMS) |
      | Order       | Descending                   |
      Then Data in AG-Grid Lab summary on Lab Mapping Results page is sorted in descending order for LTMS sorting

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0134 Labs and Assays view group widget and after edit search
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Fallopian Cancer    |
      | Biomarker/Analog | ARAF                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And View 'LABS' is opened on Lab Mapping page
      And Sort widget is present on Lab Mapping page
      And Value 'Descending' is displayed in 'Order' field on Lab Mapping page
      And Value 'Sort by:Disease market share (DMS)×' is displayed in 'Sort lab by' field on Lab Mapping page
      And Data in AG-Grid Lab summary on Lab Mapping page is sorted in descending order for DMS sorting
    When I switch view to 'Assays' on Lab Mapping page
      Then View 'Assays' is opened on Lab Mapping page
      And Sort widget is not present on Lab Mapping page
    When I switch view to 'LABS' on Lab Mapping page
      Then View 'LABS' is opened on Lab Mapping page
      And Sort widget is present on Lab Mapping page
    When I fill following fields on Lab Mapping page:
      | Sort lab by | Lab test market share (LTMS) |
      | Order       | Ascending                    |
      Then Data in AG-Grid Lab summary on Lab Mapping page is sorted in ascending order for LTMS sorting
    When I switch view to 'Assays' on Lab Mapping page
      Then View 'Assays' is opened on Lab Mapping page
    When I switch view to 'LABS' on Lab Mapping page
      Then View 'LABS' is opened on Lab Mapping page
      And Value 'Ascending' is displayed in 'Order' field on Lab Mapping page
      And Value 'Sort by:Lab test market share (LTMS)×' is displayed in 'Sort lab by' field on Lab Mapping page
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Country          | United States |
      | Disease          | Breast Cancer |
      | Biomarker/Analog | ARAF          |
      | Year From        | 2018          |
      | Month From       | March         |
      | Year To          | 2018          |
      | Month To         | March         |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Lab Mapping page is opened
      And View 'LABS' is opened on Lab Mapping page
      And Sort widget is present on Lab Mapping page
      And Value 'Ascending' is displayed in 'Order' field on Lab Mapping page
      And Value 'Sort by:Disease market share (DMS)×' is displayed in 'Sort lab by' field on Lab Mapping page
    When I return back to LM main page
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Country          | United States       |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Year From        | 2019                |
      | Month From       | March               |
      | Year To          | 2019                |
      | Month To         | March               |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    And View 'LABS' is opened on Lab Mapping page
    And Sort widget is present on Lab Mapping page
    And Value 'Descending' is displayed in 'Order' field on Lab Mapping page
    And Value 'Sort by:Disease market share (DMS)×' is displayed in 'Sort lab by' field on Lab Mapping page

  @ui @LabMapping @LabMappingOrdering
  Scenario: DIALM:0135 Labs and Assays view group widget clear field
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Fallopian Cancer    |
      | Biomarker/Analog | ARAF                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I clear 'Sort lab by' field on AG-Grid on Lab Mapping page
      Then Value 'Sort by:-- Select a lab sort --' is displayed in 'Sort lab by' field on Lab Mapping page
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Country          | United States |
      | Disease          | Breast Cancer |
      | Biomarker/Analog | ARAF          |
      | Year From        | 2018          |
      | Month From       | March         |
      | Year To          | 2018          |
      | Month To         | March         |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Lab Mapping page is opened
      And Value 'Descending' is displayed in 'Order' field on Lab Mapping page
      And Value 'Sort by:-- Select a lab sort --' is displayed in 'Sort lab by' field on Lab Mapping page
    When I return back to LM main page
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Country          | United States       |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Year From        | 2019                |
      | Month From       | March               |
      | Year To          | 2019                |
      | Month To         | March               |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Value 'Descending' is displayed in 'Order' field on Lab Mapping page
      And Value 'Sort by:Disease market share (DMS)×' is displayed in 'Sort lab by' field on Lab Mapping page