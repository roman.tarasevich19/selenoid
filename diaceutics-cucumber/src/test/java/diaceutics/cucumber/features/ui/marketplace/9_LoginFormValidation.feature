Feature: Login Form validation

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened

  @ui @Marketplace @LoginFormValidation
  Scenario: DIAM:0051 Validation of the required fields
    When I click login button on Login page
      Then Message 'This field is required.' for 'Email' field is displayed on Login page
      And Message 'This field is required.' for 'Password' field is displayed on Login page
    When I fill following fields on Login page:
      | Email | nonexistent email |
      Then Message 'Please enter a valid email address.' for 'Email' field is displayed on Login page
    When I fill following fields on Login page:
      | Email    | test@gmail.com       |
      | Password | nonexistent password |
    And I click login button on Login page
      Then Message 'Your email and/or password are not recognised' is displayed on Login page

  @ui @Marketplace @LoginFormValidation
  Scenario: DIAM:0053 Validation for links on login page
    When I click by 'Sign up' link on Login page
      Then New registration page is opened
    When I click back on browser
      Then Login page is opened
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened

  @ui @Marketplace @LoginFormValidation
  Scenario: DIAM:0071 Validation for forget password page invalid email
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I click on Request reset on Reset password page
      Then Message 'This field is required.' for 'Account email' field is displayed on Reset password page
    When I fill new value 'invalid' for account email Reset password page
    And I click on Request reset on Reset password page
      Then Message 'Please enter a valid email address.' for 'Account email' field is displayed on Reset password page
    When I fill new value 'invalid@' for account email Reset password page
    And I click on Request reset on Reset password page
      Then Message 'Please enter a valid email address.' for 'Account email' field is displayed on Reset password page
    When I fill new value 'invalid.com' for account email Reset password page
    And I click on Request reset on Reset password page
      Then Message 'Please enter a valid email address.' for 'Account email' field is displayed on Reset password page
    When I fill new value 'invalid@gmail' for account email Reset password page
    And I click on Request reset on Reset password page
      Then Message 'Please enter a valid email address.' for 'Account email' field is displayed on Reset password page

  @ui @Marketplace @LoginFormValidation
  Scenario: DIAM:0072 Validation for forget password page back
    When I click by 'Forgot password?' link on Login page
    Then Reset password page is opened
    When I fill new value from user 'changePasswordUser' for account email Reset password page
    And I click on Back to Login on Reset password page
    Then Login page is opened

  @ui @Marketplace @LoginFormValidation
  Scenario: DIAM:0073 Validation for forget password page support link
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I fill new value 'valid@gmail.com' for account email Reset password page
    And I click on Request reset on Reset password page
    And I click 'Contact Support' link on new Registration page
    And I select last tab
      Then Link should contains 'Help URL'

  @ui @Marketplace @LoginFormValidation @SingleThread
  Scenario: DIAM:0074 Validation for forget password page login link
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I fill new value 'valid@gmail.com' for account email Reset password page
    And I click on Request reset on Reset password page
    And I click 'Go to login' link on new Registration page
      Then Login page is opened
    
  @ui @Marketplace @LoginFormValidation @SingleThread
  Scenario: DIAM:0075 Validation for new password page
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I fill new value from user 'changePasswordUser' for account email Reset password page
    And I click on Request reset on Reset password page
      Then I click 'Go to login' link on new Registration page
      And Login page is opened
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I open link from gmail mail 'changePasswordUser' for setting new password
      Then Change new password page is opened

  @ui @Marketplace @LoginFormValidation @SingleThread
  Scenario: DIAM:0076 Validation for new password invalid values
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I fill new value from user 'changePasswordUser' for account email Reset password page
    And I click on Request reset on Reset password page
      Then I click 'Go to login' link on new Registration page
      And Login page is opened
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I open link from gmail mail 'changePasswordUser' for setting new password
      Then Change new password page is opened
    When I click submit on new password page
      Then Red border for password field is present on new password page
    When I set password '!' on new password page
      Then Validation rules message 'At least 8 characters in length' has color 'orange_auth0' on new password page
      And Validation rules message 'Lower case letters (a-z)' has color 'white_auth0' on new password page
      And Validation rules message 'Upper case letters (A-Z)' has color 'white_auth0' on new password page
      And Validation rules message 'Numbers (i.e. 0-9)' has color 'white_auth0' on new password page
    When I set password '!q' on new password page
      Then Validation rules message 'At least 8 characters in length' has color 'orange_auth0' on new password page
      And Validation rules message 'Lower case letters (a-z)' has color 'green_auth0' on new password page
      And Validation rules message 'Upper case letters (A-Z)' has color 'white_auth0' on new password page
      And Validation rules message 'Numbers (i.e. 0-9)' has color 'white_auth0' on new password page
    When I set password '!qfreqfdfd' on new password page
      Then Validation rules message 'Upper case letters (A-Z)' has color 'white_auth0' on new password page
      And Validation rules message 'Numbers (i.e. 0-9)' has color 'white_auth0' on new password page
      And Validation rules message 'At least 8 characters in length' has color 'green_auth0' on new password page
      And Validation rules message 'Lower case letters (a-z)' has color 'green_auth0' on new password page
    When I set password '!qfreqfdfdT' on new password page
      Then Validation rules message 'Lower case letters (a-z)' has color 'green_auth0' on new password page
      And Validation rules message 'Numbers (i.e. 0-9)' has color 'white_auth0' on new password page
      And Validation rules message 'Upper case letters (A-Z)' has color 'green_auth0' on new password page
      And Validation rules message 'At least 8 characters in length' has color 'green_auth0' on new password page
    When I set password '!qfreqfdfd5' on new password page
      And Validation rules message 'Lower case letters (a-z)' has color 'green_auth0' on new password page
      And Validation rules message 'Upper case letters (A-Z)' has color 'white_auth0' on new password page
      And Validation rules message 'Numbers (i.e. 0-9)' has color 'green_auth0' on new password page
    Then Validation rules message 'At least 8 characters in length' has color 'green_auth0' on new password page
    When I set password 'Ngftr42!g' on new password page
    And I click submit on new password page
      Then Red border for confirm password field is present on new password page
    When I set confirm password 'Ng' on new password page
    And I click submit on new password page
      Then Message 'Please ensure the password and the confirmation are the same' is present on new password page
      And Red border for confirm password field is present on new password page
    When I set confirm password 'NgGHdf!43' on new password page
    And I click submit on new password page
      Then Message 'Please ensure the password and the confirmation are the same' is present on new password page
      And Red border for confirm password field is present on new password page

  @ui @Marketplace @LoginFormValidation @SingleThread
  Scenario: DIAM:0077 Validation for change password
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I fill new value from user 'changePasswordUser' for account email Reset password page
    And I click on Request reset on Reset password page
      Then I click 'Go to login' link on new Registration page
      And Login page is opened
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I open link from gmail mail 'changePasswordUser' for setting new password
      Then Change new password page is opened
    When I set password 'NewPassword@1' on new password page
    And I set confirm password 'NewPassword@1' on new password page
    And I click submit on new password page
      Then Message 'Your new password has been set. Please login.' is present on new password page
    And I save 'NewPassword@1' for user 'changePasswordUser' on new password page
    When I open DXRX app
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'changePasswordUser' user
      Then Home page is opened
      And User should be logged in
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I fill new value from user 'changePasswordUser' for account email Reset password page
    And I click on Request reset on Reset password page
      Then I click 'Go to login' link on new Registration page
      And Login page is opened
      And I save 'Qwaszx@1' for user 'changePasswordUser' on new password page
    When I click by 'Forgot password?' link on Login page
      Then Reset password page is opened
    When I open link from gmail mail 'changePasswordUser' for setting new password
      Then Change new password page is opened
    When I set password 'Qwaszx@1' on new password page
    And I set confirm password 'Qwaszx@1' on new password page
    And I click submit on new password page
      Then Message 'Your new password has been set. Please login.' is present on new password page
    When I open DXRX app
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'changePasswordUser' user
      Then Home page is opened
      And User should be logged in