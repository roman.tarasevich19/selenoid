Feature: Profile-Settings Delete account

  @ui @Marketplace @ProfileSettings @SingleThread
   Scenario: DIAM:0047 Delete account with incorrect password input
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'changePasswordUser' user
      And User should be logged in
    When I click 'My profile' on user menu on Master header
      Then MyProfile page is opened
    And I open Settings form on My Profile page
      Then Settings form is opened
    When I click on Delete your account
    And Delete account window is opened
    And I set up Password 'gfgf' on Delete Account window
    And I click on Cancel on Delete Account window
      Then Delete account window is closed
    When I click on Delete your account
    And Delete account window is opened
    And I set up Password 'gfgf' on Delete Account window
    And I click on OK on Delete Account window
      Then Delete account window is closed
     # And ERROR IS PRESENT todo

  @ui @Marketplace @ProfileSettings @SingleThread
  Scenario: DIAM:0048 Change password with invalid data Empty
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
    Then Login page is opened
    When I login as 'changePasswordUser' user
    And User should be logged in
    When I click 'My profile' on user menu on Master header
    Then MyProfile page is opened
    And I open Settings form on My Profile page
    Then Settings form is opened
    And Save changes on User Settings Form on MyProfilePage
    And Alert should contains 'Enter a new password' on User Settings form
    Then Error container is displayed for following fields on User Settings Form on MyProfile page:
      | Password         |
      | Confirm Password |
    
  @ui @Marketplace @ProfileSettings @SingleThread
  Scenario: DIAM:0049 Change password with invalid data No confirm
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
    Then Login page is opened
    When I login as 'changePasswordUser' user
    And User should be logged in
    When I click 'My profile' on user menu on Master header
    Then MyProfile page is opened
    And I open Settings form on My Profile page
    Then Settings form is opened
    When Set up password values on User Settings Form:
      | Password | gfgfg |
    And Save changes on User Settings Form on MyProfilePage
    And Alert should contains 'cocorico_user.password.mismatch' on User Settings form

  @ui @Marketplace @ProfileSettings @SingleThread
  Scenario: DIAM:0050 New and repeated passwords do not match
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
    Then Login page is opened
    When I login as 'changePasswordUser' user
    And User should be logged in
    When I click 'My profile' on user menu on Master header
    Then MyProfile page is opened
    And I open Settings form on My Profile page
    Then Settings form is opened
    When Set up password values on User Settings Form:
      | Password         | gfgfg      |
      | Confirm Password | gfgffdfere |
    And Save changes on User Settings Form on MyProfilePage
    And Alert should contains 'cocorico_user.password.mismatch' on User Settings form
