Feature: Edit a Lab

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      Then the status code is 200
    When I put a Lab 'lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'lab' is displayed in filter results on Labs page
    When I select 'lab' lab on Labs page
      Then Lab Profile page is opened
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened

  @ui @uiAndApi @AssayManagement @EditALab
  Scenario: DIAFE:0027 Required fields validation
    When I clear 'Name' field on Edit Profile Lab Details page
    And I click button 'Save' on Edit Profile page
      Then Message 'Some items below need your attention.' is displayed on Edit Profile page
      And Message 'Please input a name for the lab' is displayed on required fields on Edit Profile page

  @ui @uiAndApi @AssayManagement @EditALab
  Scenario: DIAFE:0028 Possibility to edit a lab and validate an edited data
    When I fill following fields on Lab Details form on Edit Profile page and save as 'lab':
      | Name     | TestLab      |
      | Lab type | Academic Lab |
    And I click button 'Save' on Edit Profile page
      Then Message 'Lab updated!' is displayed on Edit Profile page
    When I click Return to profile on Edit Profile page
      Then Lab Profile page is opened
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened
    When I fill following fields on Lab Details form on Edit Profile page and save as 'lab':
      | Name     | TestLab        |
      | Lab type | Commercial Lab |
    And I click button 'Save' on Edit Profile page
      Then Message 'Lab updated!' is displayed on Edit Profile page
    When I click Return to profile on Edit Profile page
      Then Lab Profile page is opened
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened
    When I fill following fields on Lab Details form on Edit Profile page and save as 'lab':
      | Name     | TestLab      |
      | Lab type | Hospital Lab |
    And I click button 'Save' on Edit Profile page
      Then Message 'Lab updated!' is displayed on Edit Profile page
    When I click Return to profile on Edit Profile page
      Then Lab Profile page is opened
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |

  @ui @uiAndApi @AssayManagement @EditALab
  Scenario: DIAFE:0029 Possibility to edit a lab Location and validate an edited data
    When I click 'Location' on Edit Profile page
      Then Location form on Edit Profile page is opened
    When I fill following fields on Location form on Edit Profile page and save as 'lab':
      | Address 1   | New Address 1   |
      | Address 2   | New Address 2   |
      | City / Town | New City        |
      | Region      | ke              |
      | Country     | Austria         |
      | Postal code | New Postal code |
    And I click button 'Save' on Edit Profile page
      Then Message 'Location updated!' is displayed on Edit Profile page
    When I click Return to profile on Edit Profile page
      Then Lab Profile page is opened
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Country |
        | Region  |
    When I click Edit Details on Lab Profile Page
      Then Edit Profile page is opened
    When I click 'Location' on Edit Profile page
      Then Location form on Edit Profile page is opened
      And Location from 'lab' with following fields is displayed on Edit Profile page:
        | Address 1   |
        | Address 2   |
        | City / Town |
        | Region      |
        | Country     |
        | Postal code |

  @ui @uiAndApi @AssayManagement @EditALab
  Scenario: DIAFE:0030 Possibility to edit Ownership field for lab and validate an edited data
    When I fill following fields on Lab Details form on Edit Profile page and save as 'lab':
      | Ownership | Public |
    And I click button 'Save' on Edit Profile page
      Then Message 'Lab updated!' is displayed on Edit Profile page
      And Value 'Public' for field 'Ownership' is selected on Lab Details form on Edit Profile page
    When I fill following fields on Lab Details form on Edit Profile page and save as 'lab':
      | Ownership | Private |
    And I click button 'Save' on Edit Profile page
      Then Message 'Lab updated!' is displayed on Edit Profile page
      And Value 'Private' for field 'Ownership' is selected on Lab Details form on Edit Profile page
    When I fill following fields on Lab Details form on Edit Profile page and save as 'lab':
      | Ownership | Military |
    And I click button 'Save' on Edit Profile page
      Then Message 'Lab updated!' is displayed on Edit Profile page
      And Value 'Military' for field 'Ownership' is selected on Lab Details form on Edit Profile page
    When I fill following fields on Lab Details form on Edit Profile page and save as 'lab':
      | Ownership | Not Applicable |
    And I click button 'Save' on Edit Profile page
      Then Message 'Lab updated!' is displayed on Edit Profile page
      And Value 'Not Applicable' for field 'Ownership' is selected on Lab Details form on Edit Profile page
