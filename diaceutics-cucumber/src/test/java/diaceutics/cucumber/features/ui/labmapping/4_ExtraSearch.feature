Feature: Lab Mapping Extra Search

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name     | TestLab     |
      | Lab type | Unspecified |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1   | Test Address 1   |
      | Address 2   | Test Address 2   |
      | City / Town | Test City        |
      | Region      | br               |
      | Country     | Paraguay         |
      | Postal code | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And Lab 'lab' with following fields is displayed on Lab Profile page
        | Name     |
        | Lab type |
        | Country  |
        | Region   |

  @ui @LabMapping @LabMappingExtraSearch
  Scenario: DIALM:0021 non-US labs ag-grid filtering verification
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay       |
      | Assay description     | Test description |
      | In-house or send-out? | In-house         |
      | Detects               | random           |
      | Specimens tested      | random           |
      | Method                | random           |
      | Method description    | Test description |
      | Turnaround time (TAT) | 5                |
      | Ontology              | random           |
      | Sensitivity           | 55               |
      | Scoring method        | random           |
      | Report format         | random           |
      | Classification        | Commercial assay |
      | FDA 510K APPROVED KIT | true             |
      | FDA PMA APPROVED KIT  | false            |
      | IVD-CE                | false            |
      | RUO/IUO               | false            |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and add to 'assay':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | 2018                  |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 120                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Volume | 20 |
    And I add Biomarker the same as in 'assay' on Biomarker volume form on New volume page for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click 'Lab Mapping' on Data Tools Header form
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project    | Full Access Project |
      | Country    | Paraguay            |
      | Year From  | 2017                |
      | Month From | January             |
      | Year To    | 2020                |
      | Month To   | July                |
    And I fill following fields on Lab Mapping Main page using data from 'volume':
      | Disease          |
      | Biomarker/Analog |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Lab 'lab' is displayed in AG-Grid Lab summary on Lab Mapping page
    When I click by 'lab' Lab in AG-Grid Lab summary on Lab Mapping page
      Then Assay 'assay' with following fields is displayed in AG-Grid Lab summary on Lab Mapping page:
        | Name                                 |
        | Description                          |
        | Biomarkers                           |
        | Where is it performed?               |
        | Send out lab                         |
        | Detects germline/somatic alterations |
        | Specimen tested                      |
        | Method                               |
        | Method description                   |
        | Commercial assays                    |
        | Classification                       |
        | Regulatory status                    |
        | Turnaround time                      |
        | Ontology                             |
        | Variants included                    |
        | Sensitivity                          |
        | Scoring method                       |
        | Report format                        |
        | Assay status                         |
        | Active from                          |
        | Inactive since                       |

  @ui @LabMapping @LabMappingExtraSearch
  Scenario: DIALM:0022 Possibility to display labs which have volumes but no assays
      When I click on 'Add volume' on Lab Profile Page
        Then New volume page is opened
      When I fill following fields on New volume page and save as 'volume':
        | Year              | 2018                  |
        | Quarter           | Q2                    |
        | Disease           | random                |
        | Disease volume    | 120                   |
        | Biomarker details | Log biomarker volumes |
      And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
        | Biomarker | random |
        | Volume    | 20     |
      And I click 'Save and finish' on New volume page
        Then Lab Profile page is opened
        And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
      When I click 'Lab Mapping' on Data Tools Header form
        Then Lab Mapping Main page is opened
      When I fill following fields on Lab Mapping Main page:
        | Project    | Full Access Project |
        | Country    | Paraguay            |
        | Year From  | 2017                |
        | Month From | January             |
        | Year To    | 2020                |
        | Month To   | July                |
      And I fill following fields on Lab Mapping Main page using data from 'volume':
        | Disease          |
        | Biomarker/Analog |
      And I click 'Start' on Lab Mapping Main page
        Then Lab Mapping page is opened
        And Lab 'lab' is displayed in AG-Grid Lab summary on Lab Mapping page
        And Label 'No Assays recorded' is displayed for Lab 'lab' in AG-Grid Lab summary on Lab Mapping page

  @ui @LabMapping @LabMappingExtraSearch
  Scenario: DIALM:0023 Possibility to display biomarker without variants
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name             | Test Assay              |
      | Assay description      | Test Assay description  |
      | Detects                | random                  |
      | Specimens tested       | random                  |
      | Method                 | random                  |
      | Method description     | Test Method description |
      | Turnaround time (TAT)  | 5                       |
      | Ontology               | random                  |
      | Sensitivity            | 44                      |
      | Scoring method         | random                  |
      | Report format          | random                  |
      | Classification         | Commercial assay        |
      | FDA 510K APPROVED KIT  | true                    |
      | FDA PMA APPROVED KIT   | false                   |
      | IVD-CE                 | false                   |
      | RUO/IUO                | false                   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and add to 'assay':
      | Biomarker | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | 2018                  |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 120                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Volume | 20 |
    And I add Biomarker the same as in 'assay' on Biomarker volume form on New volume page for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click 'Lab Mapping' on Data Tools Header form
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project    | Full Access Project |
      | Country    | Paraguay            |
      | Year From  | 2017                |
      | Month From | January             |
      | Year To    | 2020                |
      | Month To   | July                |
    And I fill following fields on Lab Mapping Main page using data from 'volume':
      | Disease          |
      | Biomarker/Analog |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Lab 'lab' is displayed in AG-Grid Lab summary on Lab Mapping page
    When I click by 'lab' Lab in AG-Grid Lab summary on Lab Mapping page
      Then Value for field 'Variants included' should be empty in AG-Grid Lab summary on Lab Mapping page

  @ui @LabMapping @LabMappingExtraSearch
  Scenario: DIALM:0024 Validation value Disease market share for labs which have volume with Disease volume equals to zero
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | 2018                  |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 0                     |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Biomarker | random |
      | Volume    | 20     |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click 'Lab Mapping' on Data Tools Header form
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project    | Full Access Project |
      | Country    | Paraguay            |
      | Year From  | 2017                |
      | Month From | January             |
      | Year To    | 2020                |
      | Month To   | July                |
    And I fill following fields on Lab Mapping Main page using data from 'volume':
      | Disease          |
      | Biomarker/Analog |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Lab 'lab' is displayed in AG-Grid Lab summary on Lab Mapping page
      And Values 'Disease market share' for Lab 'lab' should be '0.00%'

  @ui @LabMapping @LabMappingExtraSearch
  Scenario: DIALM:0124 Possibility to display an assay with Send out lab in ag-grid
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
      | Send-out Lab          | random     |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and add to 'assay':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | 2018                  |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 100                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Volume | 20 |
    And I add Biomarker the same as in 'assay' on Biomarker volume form on New volume page for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click 'Lab Mapping' on Data Tools Header form
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project    | Full Access Project |
      | Country    | Paraguay            |
      | Year From  | 2017                |
      | Month From | January             |
      | Year To    | 2020                |
      | Month To   | July                |
    And I fill following fields on Lab Mapping Main page using data from 'volume':
      | Disease          |
      | Biomarker/Analog |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Lab 'lab' is displayed in AG-Grid Lab summary on Lab Mapping page
    When I click by 'lab' Lab in AG-Grid Lab summary on Lab Mapping page
      Then Assay 'assay' with following fields is displayed in AG-Grid Lab summary on Lab Mapping page:
        | Name                   |
        | Biomarkers             |
        | Where is it performed? |
        | Send out lab           |

  @ui @LabMapping @LabMappingExtraSearch
  Scenario: DIALM:0127 Possibility to display an assay without specified Send out lab in ag-grid
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and add to 'assay':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
    Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | 2018                  |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 100                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Volume | 20 |
    And I add Biomarker the same as in 'assay' on Biomarker volume form on New volume page for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click 'Lab Mapping' on Data Tools Header form
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project    | Full Access Project |
      | Country    | Paraguay            |
      | Year From  | 2017                |
      | Month From | January             |
      | Year To    | 2020                |
      | Month To   | July                |
    And I fill following fields on Lab Mapping Main page using data from 'volume':
      | Disease          |
      | Biomarker/Analog |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Lab 'lab' is displayed in AG-Grid Lab summary on Lab Mapping page
    When I click by 'lab' Lab in AG-Grid Lab summary on Lab Mapping page
      Then Assay 'assay' with following fields is displayed in AG-Grid Lab summary on Lab Mapping page:
      | Name                   |
      | Biomarkers             |
      | Where is it performed? |
      And Column 'Send out lab' has value 'Not disclosed' in AG-Grid Lab summary on Lab Mapping page

  @ui @LabMapping @LabMappingExtraSearch
  Scenario: DIALM:0130 Possibility to display an assay with Inactive status in ag-grid
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To  | 2020   |
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Add an Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and add to 'assay':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | 2018                  |
      | Quarter           | Q2                    |
      | Disease           | random                |
      | Disease volume    | 120                   |
      | Biomarker details | Log biomarker volumes |
    And I fill following fields on Biomarker volume form on New volume page and add biomarker volume to 'volume':
      | Volume | 20 |
    And I add Biomarker the same as in 'assay' on Biomarker volume form on New volume page for 'volume'
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click 'Lab Mapping' on Data Tools Header form
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project    | Full Access Project |
      | Country    | Paraguay            |
      | Year From  | 2017                |
      | Month From | January             |
      | Year To    | 2020                |
      | Month To   | July                |
    And I fill following fields on Lab Mapping Main page using data from 'volume':
      | Disease          |
      | Biomarker/Analog |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Lab 'lab' is displayed in AG-Grid Lab summary on Lab Mapping page
    When I click by 'lab' Lab in AG-Grid Lab summary on Lab Mapping page
      Then Assay 'assay' with following fields is displayed in AG-Grid Lab summary on Lab Mapping page:
        | Name           |
        | Assay status   |
        | Active from    |
        | Inactive since |
