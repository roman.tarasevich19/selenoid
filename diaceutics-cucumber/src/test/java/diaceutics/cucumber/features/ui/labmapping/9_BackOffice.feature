Feature: Lab Mapping Back office

  @ui @LabMapping @LabMappingBackOffice @SingleThread
  Scenario: DIALM:0147 Validation data description info box
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Thyroid Cancer      |
      | Biomarker/Analog | BRIP                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
    And I click 'Data summary' link on Lab Mapping Main page
      Then Modal form title is 'About this data subscription' on Lab Mapping Main page
      And I get text content from modal form and save to 'subscriptionMainPage' on Lab Mapping page
      And I click 'Done' on Lab Mapping Main page
    When I click 'Start' without wait on Lab Mapping Main page
      Then Project name from Data summary panel is 'Full Access Project' on Lab Mapping page
    When I click 'Data summary' link on Lab Mapping page
      Then I get text content from modal form and save to 'subscriptionPage' on Lab Mapping page
    When I open BackOffice app
      Then BackOffice main page is opened
      And I click 'Log In' link on BackOffice main page
      And I click 'Logout' link on BackOffice main page
      And I click 'Log In' link on BackOffice main page
    When I login as 'BACKOFFICE' user
      Then BackOffice main page is opened
    When I click 'Manage Projects & Subscriptions' link on BackOffice main page
      Then BackOffice subscriptions page is opened
    When I click 'Projects' link on BackOffice Subscription page
      Then BackOffice projects page is opened
    When I click 'Full Access Project' link on BackOffice projects page
      Then BackOffice project description page is opened
      And I get project description on BackOffice project description page and save to 'backOfficeDescription'
      And I get 'subscriptionMainPage' text and 'backOfficeDescription' and compare on Lab Mapping Main page
      And I get 'subscriptionPage' text and 'backOfficeDescription' and compare on Lab Mapping Main page

  @ui @LabMapping @LabMappingBackOffice @SingleThread
  Scenario: DIALM:0148 Validation data description info box after edit
    When I open BackOffice app
      Then BackOffice main page is opened
      And I click 'Log In' link on BackOffice main page
    When I login as 'BACKOFFICE' user
      Then BackOffice main page is opened
    When I click 'Manage Projects & Subscriptions' link on BackOffice main page
      Then BackOffice subscriptions page is opened
    When I click 'Projects' link on BackOffice Subscription page
      Then BackOffice projects page is opened
    When I click 'Full Access Project' link on BackOffice projects page
      Then BackOffice project description page is opened
    When I update project description last row using ' text' on BackOffice project description page
    And I click on 'Save' control bar button BackOffice project description page
      Then BackOffice projects page is opened
    When I click 'Full Access Project' link on BackOffice projects page
      Then BackOffice project description page is opened
      And I get project description on BackOffice project description page and save to 'backOfficeDescription'
    When I open DXRX app
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then User should be logged in
    When I click 'Logout' on user menu on Master Header form
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Thyroid Cancer      |
      | Biomarker/Analog | BRIP                |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | January             |
      Then I click 'Data summary' link on Lab Mapping Main page
      And I get text content from modal form and save to 'subscriptionMainPage' on Lab Mapping page
      And I click 'Done' on Lab Mapping Main page
    When I click 'Start' without wait on Lab Mapping Main page
      Then Project name from Data summary panel is 'Full Access Project' on Lab Mapping page
    When I click 'Data summary' link on Lab Mapping page
      Then I get text content from modal form and save to 'subscriptionPage' on Lab Mapping page
      And I get 'backOfficeDescription' text and 'subscriptionMainPage' and compare on Lab Mapping Main page
      And I get 'backOfficeDescription' text and 'subscriptionPage' and compare on Lab Mapping Main page

  @ui @LabMapping @LabMappingBackOffice @SingleThread
  Scenario: DIALM:0149 Validation data description info box make default
    When I open BackOffice app
      Then BackOffice main page is opened
      And I click 'Log In' link on BackOffice main page
      And I login as 'BACKOFFICE' user
      And BackOffice main page is opened
    When I click 'Manage Projects & Subscriptions' link on BackOffice main page
      Then BackOffice subscriptions page is opened
    When I click 'Projects' link on BackOffice Subscription page
      Then BackOffice projects page is opened
    When I click 'Full Access Project' link on BackOffice projects page
      Then BackOffice project description page is opened
    When I update project description last row using 'CPT codes include: xxxx, xxxx, xxx' on BackOffice project description page
    And I click on 'Save' control bar button BackOffice project description page
      Then BackOffice projects page is opened

  @ui @LabMapping @LabMappingBackOffice @SingleThread
  Scenario: DIALM:0150 Validation data empty description
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | limited-access-test        |
      | Disease          | Non small cell lung cancer |
      | Biomarker/Analog | ALK                        |
      | Country          | United States              |
      | Year From        | 2019                       |
      | Month From       | February                   |
      | Year To          | 2019                       |
      | Month To         | February                   |
      Then Link 'Data summary' is not present on Lab Mapping Main page
    When I click 'Start' without wait on Lab Mapping Main page
      Then Project name from Data summary panel is 'limited-access-test' on Lab Mapping page
      And Link 'Data summary' is not present on Lab Mapping page