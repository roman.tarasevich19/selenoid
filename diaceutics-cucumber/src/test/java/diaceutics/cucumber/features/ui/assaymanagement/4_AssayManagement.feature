Feature: Assay Management

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I put a Lab 'lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'lab' is displayed in filter results on Labs page
    When I select 'lab' lab on Labs page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0031 Possibility to add an assay
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay       |
      | Assay description     | Test description |
      | In-house or send-out? | In-house         |
      | Testing purpose       | random           |
      | Detects               | random           |
      | Specimens tested      | random           |
      | Method                | random           |
      | Method description    | Test description |
      | Turnaround time (TAT) | 5                |
      | Ontology              | random           |
      | Sensitivity           | 55               |
      | Scoring method        | random           |
      | Report format         | random           |
      | Classification        | Commercial assay |
      | FDA 510K APPROVED KIT | true             |
      | FDA PMA APPROVED KIT  | false            |
      | IVD-CE                | false            |
      | RUO/IUO               | false            |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name           |
        | Assay description    |
        | In-house or send-out |
        | Testing purpose      |
        | Detects              |
        | Specimens tested     |
        | Method name          |
        | Method description   |
        | Turnaround time      |
        | Ontology             |
        | Sensitivity          |
        | Scoring method       |
        | Report format        |
        | Classification       |
        | Commercial assay     |
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0032 Possibility to edit assays
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay       |
      | Assay description     | Test description |
      | In-house or send-out? | In-house         |
      | Testing purpose       | random           |
      | Detects               | random           |
      | Specimens tested      | random           |
      | Method                | random           |
      | Method description    | Test description |
      | Turnaround time (TAT) | 5                |
      | Ontology              | random           |
      | Sensitivity           | 55               |
      | Scoring method        | random           |
      | Report format         | random           |
      | Classification        | Commercial assay |
      | FDA 510K APPROVED KIT | true             |
      | FDA PMA APPROVED KIT  | false            |
      | IVD-CE                | false            |
      | RUO/IUO               | false            |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I clear following fields on Edit Assay page
      | Detects          |
      | Specimens tested |
      | Ontology         |
      | Scoring method   |
    And I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay       |
      | Assay description     | Test description |
      | In-house or send-out? | In-house         |
      | Testing purpose       | random           |
      | Detects               | random           |
      | Specimens tested      | random           |
      | Method                | random           |
      | Method description    | Test description |
      | Turnaround time (TAT) | 5                |
      | Ontology              | random           |
      | Sensitivity           | 55               |
      | Scoring method        | random           |
      | Report format         | random           |
      | Classification        | Commercial assay |
      | FDA 510K APPROVED KIT | false            |
      | FDA PMA APPROVED KIT  | false            |
      | IVD-CE                | false            |
      | RUO/IUO               | true             |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    When I clear 'Variants' field on Lab Test Biomarker form on Edit Assay page
    And I fill following fields in Lab Test Biomarker form on Edit Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And 'Lab assay updated.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name           |
        | Assay description    |
        | In-house or send-out |
        | Testing purpose      |
        | Detects              |
        | Specimens tested     |
        | Method name          |
        | Method description   |
        | Turnaround time      |
        | Ontology             |
        | Sensitivity          |
        | Scoring method       |
        | Report format        |
        | Classification       |
        | Commercial assay     |
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0033 Required fields validation
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
    And Red border is displayed for following fields on Add an Assay page:
      | Assay name |
      | Detects    |
      | Method     |
      | Ontology   |
      And Message 'Please enter an assay name' is displayed on required fields on Add an Assay page
      And Message 'Please provide at least one value' is displayed on required fields on Add an Assay page
      And Message 'Method is required' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Classification | Lab developed test (LDT) |
      Then Field 'FDA 510K APPROVED KIT' should be disabled on Add an Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Add an Assay page
      And Field 'IVD-CE' should be disabled on Add an Assay page
      And Field 'RUO/IUO' should be disabled on Add an Assay page
      And Field 'Commercial Assays' should be disabled on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | In-house or send-out? | Send-out |
      Then Field 'Send-out Lab' should be enabled on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | In-house or send-out? | Not specified |
      Then Field 'Send-out Lab' should be disabled on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | In-house or send-out? | In-house |
      Then Field 'Send-out Lab' should be disabled on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Turnaround time (TAT) | 0 |
      And Message 'Please enter a value greater than 0.' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay               |
      | Detects               | random                   |
      | Method                | random                   |
      | Turnaround time (TAT) | 5                        |
      | Ontology              | random                   |
      | Classification        | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I click 'Save and finish' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
      And Red border is displayed for 'Biomarker' field on Lab Test Biomarker form on Add an Assay page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0034 Sensitivity field validation on edit assay page
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name | Test Assay |
      | Detects    | random     |
      | Method     | random     |
      | Ontology   | random     |
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Sensitivity | -55 |
    And I click 'Proceed' on Edit Assay page
      Then Message 'Some items below need your attention.' is displayed on Edit Assay page
      And Message 'Please enter a value between 0 and 100.' is displayed on required fields on Edit Assay page
    When I fill following fields on Edit Assay page and save as 'assay':
      | Sensitivity | 155 |
    And I click 'Proceed' on Edit Assay page
      Then Message 'Some items below need your attention.' is displayed on Edit Assay page
      And Message 'Please enter a value between 0 and 100.' is displayed on required fields on Edit Assay page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0035 Filter an assay by keyword
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
      And I refresh page
    When I put a Assay 'assay' on search field 'Search assays' and press Search icon on Lab Profile page
      Then Assay 'assay' is displayed in Assays grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0036 Possibility to sort assays
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assayOne'
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayTwo.json'
      Then the status code is 200
      And I save from response lab test as 'assayTwo'
    When I refresh page
      Then Assay 'assayOne' is displayed in Assays grid on Lab Profile page
      And Assay 'assayTwo' is displayed in Assays grid on Lab Profile page
    When I sort data by alphabet in 'Assay name' column on Assays Grid
      Then Data in 'Assay name' column on Assays Grid sorted according to alphabet
    When I sort data by alphabet in 'Status' column on Assays Grid
      Then Data in 'Status' column on Assays Grid sorted according to alphabet
    When I sort data by alphabet in 'Classifications' column on Assays Grid
      Then Data in 'Classifications' column on Assays Grid sorted according to alphabet

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0037 Check number of assays
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assayOne'
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayTwo.json'
      Then the status code is 200
      And I save from response lab test as 'assayTwo'
    When I refresh page
      Then Assay 'assayOne' is displayed in Assays grid on Lab Profile page
      And Assay 'assayTwo' is displayed in Assays grid on Lab Profile page
    When I count the number of assays in the Assays grid and save as 'numberOfAssays'
      Then 'numberOfAssays' must be the same as a number stated in the Assays Grid title

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0038 Verification the classification Lab developed test
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay       |
      | Assay description     | Test description |
      | In-house or send-out? | In-house         |
      | Testing purpose       | random           |
      | Detects               | random           |
      | Specimens tested      | random           |
      | Method                | random           |
      | Method description    | Test description |
      | Turnaround time (TAT) | 5                |
      | Ontology              | random           |
      | Sensitivity           | 55               |
      | Scoring method        | random           |
      | Report format         | random           |
      | Classification        | Commercial assay |
      | FDA 510K APPROVED KIT | true             |
      | FDA PMA APPROVED KIT  | false            |
      | IVD-CE                | false            |
      | RUO/IUO               | false            |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | New Assay                |
      | Method                | random                   |
      | Method description    | Test Method description  |
      | Turnaround time (TAT) | 5                        |
      | Sensitivity           | 55                       |
      | Report format         | random                   |
      | Classification        | Lab developed test (LDT) |
    When I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    When I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And 'Lab assay updated.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name         |
        | Method name        |
        | Method description |
        | Report format      |
        | Classification     |
      And field 'Commercial assay' has value 'Not applicable' on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0039 Verification the classification Commercial assay
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay           |
      | Assay description     | Test description     |
      | Testing purpose       | random               |
      | Detects               | random               |
      | Specimens tested      | random               |
      | Method                | Real Time PCR        |
      | Method description    | Test description     |
      | Turnaround time (TAT) | 5                    |
      | Ontology              | random               |
      | Sensitivity           | 55                   |
      | Scoring method        | random               |
      | Report format         | random               |
      | Classification        | Commercial assay     |
      | FDA 510K APPROVED KIT | true                 |
      | FDA PMA APPROVED KIT  | false                |
      | IVD-CE                | false                |
      | RUO/IUO               | false                |
      | Commercial Assays     | Abbott RealTime IDH1 |
      And Message '1 Biomarkers with variants linked to this assay' is displayed on Add an Assay page
      And Commercial assay Biomarkers are added to this assay 'assay'
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name         |
        | Assay description  |
        | Method name        |
        | Method description |
        | Report format      |
        | Classification     |
        | Commercial assay   |
    And The following biomarkers are displayed on Assay description page:
      | IDH1 |

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0040 Possibility to add additional values for Commercial Assays
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Method                | Immunohistochemistry |
      | Classification        | Commercial assay     |
      | FDA 510K APPROVED KIT | false                |
      | FDA PMA APPROVED KIT  | false                |
      | IVD-CE                | true                 |
      | RUO/IUO               | false                |
      Then The following values should be available for 'Commercial Assays' field on Add an Assay page:
        | Roche SP263 RTU     |
        | Roche SP142         |
        | Agilent 22C3 RTU    |
        | Agilent 28-8 RTU    |
        | Leica 73-10 RTU     |
        | mAb clone 22C3      |
        | mAb clone E1L3N     |
        | mAb clone CAL10     |
        | mAb clone 28-8      |
        | mAb clone ZR3       |
        | mAb clone QR1       |
        | mAb clone BSR90     |
        | mAb clone SP142     |
        | mAb CAL10, API3171  |
        | mAb QR1, 2-PR292-13 |
        | mAb clone MXR003    |
        | mAb clone 73-10     |

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0041 Possibility to add additional values for Specimens tested
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
      And The following values should be available for 'Specimens tested' field on Add an Assay page:
        | Cell Blocks                                       |
        | Cell Smears                                       |
        | Cytology samples fixed in formalin                |
        | Cytology samples fixed in alcohol based fixatives |

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0042 Possibility to delete assay
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
      And I refresh page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click Delete on Edit Assay page
     Then Delete assay form on Edit Assay page is opened
    When I click 'Delete assay' on Delete assay form on Edit Assay page
      Then Lab Profile page is opened
      And 'The assay was successfully deleted' message is displayed on Lab Profile page
      And Assay 'assay' is not displayed in Assays grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0043 Delete assay feature for Admin
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
      And I refresh page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
      And Delete button is displayed on Edit Assay page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0044 Possibility to delete biomarker
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay               |
      | Detects               | random                   |
      | Specimens tested      | random                   |
      | Method                | random                   |
      | Turnaround time (TAT) | 5                        |
      | Ontology              | random                   |
      | Sensitivity           | 55                       |
      | Scoring method        | random                   |
      | Report format         | random                   |
      | Classification        | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerOne':
      | Biomarker | random |
      | Variants  | random |
    And I click '+ Add another' on Add an Assay page
    And I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerTwo':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Biomarker 'biomarkerOne' is displayed on Assay description page
      And Biomarker 'biomarkerTwo' is displayed on Assay description page
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I click remove Biomarker 'biomarkerOne' on Lab Test Biomarker form on Edit Assay page
      Then Biomarker 'biomarkerOne' is not displayed on Lab Test Biomarker form on Edit Assay page
    When I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And 'Lab assay updated.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Biomarker 'biomarkerOne' is not displayed on Assay description page
      And Biomarker 'biomarkerTwo' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0069 Validation Send out lab make a non-mandatory field
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
      Then Field 'Testing purpose' should be disabled on Add an Assay page
      And Field 'Detects' should be disabled on Add an Assay page
      And Field 'Specimens tested' should be disabled on Add an Assay page
      And Field 'Method' should be disabled on Add an Assay page
      And Field 'Method description' should be disabled on Add an Assay page
      And Field 'Turnaround time (TAT)' should be disabled on Add an Assay page
      And Field 'Ontology' should be disabled on Add an Assay page
      And Field 'Sensitivity' should be disabled on Add an Assay page
      And Field 'Scoring method' should be disabled on Add an Assay page
      And Field 'Report format' should be disabled on Add an Assay page
      And Field 'Commercial Assays' should be disabled on Add an Assay page
      And Field 'Classification' should be disabled on Add an Assay page
      And Field 'FDA 510K APPROVED KIT' should be disabled on Add an Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Add an Assay page
      And Field 'IVD-CE' should be disabled on Add an Assay page
      And Field 'RUO/IUO' should be disabled on Add an Assay page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0070 Possibility to add an assay with Send out lab
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name           |
        | In-house or send-out |
        | Classification       |
        | Commercial assay     |
      And Value 'Unspecified' for following fields is displayed on Assay description page:
        | Testing purpose    |
        | Detects            |
        | Specimens tested   |
        | Method name        |
        | Method description |
        | Turnaround time    |
        | Ontology           |
        | Sensitivity        |
        | Scoring method     |
        | Report format      |
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0071 Possibility to add an assay without specified Send out lab
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
    And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name           |
        | In-house or send-out |
      And field 'Send out lab' has value 'Not disclosed' on Assay description page
      And Value 'Unspecified' for following fields is displayed on Assay description page:
        | Testing purpose    |
        | Detects            |
        | Specimens tested   |
        | Method name        |
        | Method description |
        | Turnaround time    |
        | Ontology           |
        | Sensitivity        |
        | Scoring method     |
        | Report format      |
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0072 Sensitivity field validation
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Sensitivity | -0.54 |
    And I click 'Proceed' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
      Then Message 'Please enter a value between 0 and 100.' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Sensitivity | - |
    And I click 'Proceed' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
      Then Message 'Please enter a valid numeric value' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Sensitivity | test |
    And I click 'Proceed' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
      Then Message 'Please enter a valid numeric value' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Sensitivity | 102test |
    And I click 'Proceed' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
      Then Message 'Please enter a value between 0 and 100.' is displayed on required fields on Add an Assay page
      And Message 'Please enter a valid numeric value' is displayed on required fields on Add an Assay page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0064 Move assay from one lab to another
    When I execute the POST request Creates a new lab 'another lab' using following data:
      | name        | Test Lab move |
      | countryCode | ALB           |
      | type        | ACADEMIC      |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
    When I save from response lab test as 'assay'
    And I refresh page
    And I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Move this assay on Assay description page
      Then Move this assay form is opened on Assay description page
    When I click 'Save changes' button on Move this assay form
      Then Message 'Please select a lab' is displayed on required fields on Move assay form
      And Message 'Please confirm that you would like to move this assay' is displayed on required fields on Move assay form
    When I select new lab 'another lab' to move assay
    And I approve move assay
    And I click 'Save changes' button on Move this assay form
      Then Message 'assay' 'assay has been moved to' 'another lab' is displayed on Lab Profile page
      And Assay 'assay' is not displayed in Assays grid on Lab Profile page
    When I click 'Assay Management' on Data Tools Header form
      Then Assay Management page is opened
    When I put a Lab 'another lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'another lab' is displayed in filter results on Labs page
    When I select 'another lab' lab on Labs page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0073 Possibility to add Biomarker with multiple variants
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I set 'BRAF' value for Biomarker field in Lab Test Biomarker form on Add Assay page and save as 'biomarker'
    And I fill following values for Variants field in Lab Test Biomarker form on Add Assay page and add to 'biomarker'
      | Exon 15 V600E |
      | Exon 15 V600D |
      | Exon 15 V600K |
      Then Variants from 'biomarker' are displayed on Lab Test Biomarker form on Add an Assay page
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Variants for 'biomarker' are displayed in Biomarkers and variants grid on Assay description page
      And Variants for 'biomarker' sorted according to alphabet in Biomarkers and variants grid on Assay description page
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0074 Possibility to delete variant
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I set 'EGFR' value for Biomarker field in Lab Test Biomarker form on Add Assay page and save as 'biomarker'
    And I fill following values for Variants field in Lab Test Biomarker form on Add Assay page and add to 'biomarker'
      | Exon 18 G719A     |
      | Exon 19 Deletions |
      | Exon 20 T790M     |
      Then Variants from 'biomarker' are displayed on Lab Test Biomarker form on Add an Assay page
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    When I remove following variants for 'biomarker' in Lab Test Biomarker form on Edit Assay page:
      | Exon 18 G719A     |
      | Exon 19 Deletions |
      Then Variants from 'biomarker' are displayed on Lab Test Biomarker form on Edit Assay page
    When I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And 'Lab assay updated.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Variants for 'biomarker' are displayed in Biomarkers and variants grid on Assay description page
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0075 Possibility to clear field variants
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I set 'EGFR' value for Biomarker field in Lab Test Biomarker form on Add Assay page and save as 'biomarker'
    And I fill following values for Variants field in Lab Test Biomarker form on Add Assay page and add to 'biomarker'
      | Exon 19 Deletions |
      | Exon 20 T790M     |
      Then Variants from 'biomarker' are displayed on Lab Test Biomarker form on Add an Assay page
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    When I remove all Variants for 'biomarker' in Lab Test Biomarker form on Edit Assay page
    And I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And 'Lab assay updated.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Variants column for 'biomarker' is empty in Biomarkers and variants grid on Assay description page
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0055 Change commercial to non comm assay
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay               |
      | Assay description     | Test description         |
      | Testing purpose       | random                   |
      | Detects               | random                   |
      | Specimens tested      | random                   |
      | Method                | Real Time PCR            |
      | Method description    | Test description         |
      | Turnaround time (TAT) | 5                        |
      | Ontology              | random                   |
      | Sensitivity           | 55                       |
      | Scoring method        | random                   |
      | Report format         | random                   |
      | Classification        | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I click on 'Previous: Assay Details' biomarkers on Lab Test Biomarker form
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay1          |
      | Classification        | Commercial assay     |
      | Method                | Real Time PCR        |
      | FDA 510K APPROVED KIT | true                 |
      | FDA PMA APPROVED KIT  | false                |
      | IVD-CE                | false                |
      | RUO/IUO               | false                |
      | Commercial Assays     | Abbott RealTime IDH1 |
      Then Message '1 Biomarkers with variants linked to this assay' is displayed on Add an Assay page
      And Commercial assay Biomarkers are added to this assay 'assay'
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
    And I fill following fields on Edit Assay page and save as 'assay':
      | Classification | Lab developed test (LDT) |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0121 Edit assay with LDT and commercial assay
    When I click on 'Add assay' on Lab Profile Page
    Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay               |
      | Assay description     | Test description         |
      | Testing purpose       | random                   |
      | Detects               | random                   |
      | Specimens tested      | random                   |
      | Method                | Real Time PCR            |
      | Method description    | Test description         |
      | Turnaround time (TAT) | 5                        |
      | Ontology              | random                   |
      | Sensitivity           | 55                       |
      | Scoring method        | random                   |
      | Report format         | random                   |
      | Classification        | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerOne':
      | Biomarker | 19                   |
      | Variants  | Entire gene coverage |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
    When I fill following fields on Edit Assay page and save as 'assay':
      | In-house or send-out? | In-house   |
      Then Field 'FDA 510K APPROVED KIT' should be disabled on Edit Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Edit Assay page
      And Field 'IVD-CE' should be disabled on Edit Assay page
      And Field 'RUO/IUO' should be disabled on Edit Assay page
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay1   |
      | In-house or send-out? | Not specified |
      Then Field 'FDA 510K APPROVED KIT' should be disabled on Edit Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Edit Assay page
      And Field 'IVD-CE' should be disabled on Edit Assay page
      And Field 'RUO/IUO' should be disabled on Edit Assay page
    When I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    When I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened

  @ui @uiAndApi @Assay
  Scenario: DIAFE:0102 Possibility to transfer an assay to all exact labs
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
    When I save from response lab test as 'assay'
    And I refresh page
    And I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Move this assay on Assay description page
      Then Move this assay form is opened on Assay description page
    When I execute the GET request Returns Collection of Lab Names and their IDs
      Then the status code is 200
    When I save from response list of lab names as 'lab names'
      Then Field Select lab should be contains 'lab names' on Move this assay form on Assay description page

  @ui @uiAndApi @AssayManagement @Assay
  Scenario: DIAFE:0103 Checking the validation messages for TAT field
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Message 'Some items below need your attention.' is displayed on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Turnaround time (TAT) | 0 |
      Then Message 'Please enter a value greater than 0.' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Turnaround time (TAT) | 3.5 |
      Then Message 'Please enter a valid integer value' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Turnaround time (TAT) | -5 |
      Then Message 'Please enter a value greater than 0.' is displayed on required fields on Add an Assay page
    When I fill following fields on Add an Assay page and save as 'assay':
      | Turnaround time (TAT) | test |
      Then Message 'Please enter a valid integer value' is displayed on required fields on Add an Assay page
