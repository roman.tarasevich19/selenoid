Feature: Assay status

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I put a Lab 'lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'lab' is displayed in filter results on Labs page
    When I select 'lab' lab on Labs page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0104 Validation for assay with default status
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Value 'Active' for 'Status' field is displayed on Assay description page
      And Value '2019' for 'Year From' field is displayed on Assay description page
      And Value 'Jan' for 'Month From' field is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0105 Possibility to add assay with In Active Status
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To  | 2019   |
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Add an Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name           |
        | Status               |
        | Year To              |
        | Month To             |
        | In-house or send-out |
        | Detects              |
        | Method name          |
        | Ontology             |
        | Classification       |
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0106 Possibility to edit assays status from In Active to Active
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To  | 2020   |
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Add an Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I set status 'Active' for Assay 'assay' on Edit Assay page
      Then Reactivate this assay form is opened on Edit Assay page
    When I click 'Yes, set to active' on Reactivate this assay form on Edit Assay page
      Then Edit Assay page is opened
    When I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    When I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And 'Lab assay updated.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
        | Assay name |
      And Value 'Active' for 'Status' field is displayed on Assay description page
      And Value '2019' for 'Year From' field is displayed on Assay description page
      And Value 'Jan' for 'Month From' field is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0115 Validation for assay status filtering
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assayOne'
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayTwo.json'
      Then the status code is 200
      And I save from response lab test as 'assayTwo'
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'assayInActiveStatus.json'
      Then the status code is 200
      And I save from response lab test as 'assayThree'
    When I refresh page
      Then Lab Profile page is opened
      And Filter 'All' is selected in Assays grid on Lab Profile page
      And Value 'Active' for Assay 'assayOne' is displayed in 'Status' column in Assays Grid on Lab Profile page
      And Value 'Active' for Assay 'assayTwo' is displayed in 'Status' column in Assays Grid on Lab Profile page
      And Value 'Inactive' for Assay 'assayThree' is displayed in 'Status' column in Assays Grid on Lab Profile page
    When I count the number of assays in the Assays grid and save as 'numberOfAllAssays'
      Then 'numberOfAllAssays' must be the same as a number for 'All' filter in Assays grid on Lab Profile page
    When I click 'Active' filter in Assays grid on Lab Profile page
    And I count the number of assays in the Assays grid and save as 'numberOfActiveAssays'
      Then Filter 'Active' is selected in Assays grid on Lab Profile page
      And 'numberOfActiveAssays' must be the same as a number for 'Active' filter in Assays grid on Lab Profile page
      And Value 'Jan 2018' for Assay 'assayOne' is displayed in 'Active' column in Assays Grid on Lab Profile page
      And Value 'May 2019' for Assay 'assayTwo' is displayed in 'Active' column in Assays Grid on Lab Profile page
    When I click 'Inactive' filter in Assays grid on Lab Profile page
    And I count the number of assays in the Assays grid and save as 'numberOfInActiveAssays'
      Then 'numberOfInActiveAssays' must be the same as a number for 'Inactive' filter in Assays grid on Lab Profile page
      And Value 'Feb 2021' for Assay 'assayThree' is displayed in 'Inactive' column in Assays Grid on Lab Profile page
      And Filter 'Inactive' is selected in Assays grid on Lab Profile page
    When I execute the PUT request Updates an lab test 'assayTwo' using data from file 'assayInActiveStatusTwo.json'
      Then the status code is 200
      And I save from response lab test as 'assayTwo'
    When I refresh page
      Then Lab Profile page is opened
    When I click 'All' filter in Assays grid on Lab Profile page
      Then Value 'Active' for Assay 'assayOne' is displayed in 'Status' column in Assays Grid on Lab Profile page
      And Value 'Inactive' for Assay 'assayTwo' is displayed in 'Status' column in Assays Grid on Lab Profile page
      And Value 'Inactive' for Assay 'assayThree' is displayed in 'Status' column in Assays Grid on Lab Profile page
    When I click 'Inactive' filter in Assays grid on Lab Profile page
    And I count the number of assays in the Assays grid and save as 'numberOfInActiveAssays'
      Then 'numberOfInActiveAssays' must be the same as a number for 'Inactive' filter in Assays grid on Lab Profile page
      And Value 'Feb 2020' for Assay 'assayTwo' is displayed in 'Inactive' column in Assays Grid on Lab Profile page
    When I click 'Active' filter in Assays grid on Lab Profile page
    And I count the number of assays in the Assays grid and save as 'numberOfActiveAssays'
      Then 'numberOfActiveAssays' must be the same as a number for 'Active' filter in Assays grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0118 Validation for Assay status history
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I refresh page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click View status history link on Assay description page
      Then Status History form is opened
      And Name for Assay 'assay' is displayed on Status History form
      And Name for Lab 'lab' is displayed on Status History form
      Then I get statuses from Log of assay status grid and compare with statuses for 'assay' on Status History form
    When I execute the PUT request Updates an lab test 'assay' using data from file 'assayInActiveStatusTwo.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I refresh page
      Then Assay description page is opened
    When I click View status history link on Assay description page
      Then Status History form is opened
      And I get statuses from Log of assay status grid and compare with statuses for 'assay' on Status History form
    When I close Status History form
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click View status history link on Edit Assay page
      Then Status History form is opened
      And I get statuses from Log of assay status grid and compare with statuses for 'assay' on Status History form

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0119 Possibility to sort columns in Log of assay status changes grid
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'assayInActiveStatusTwo.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I refresh page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click View status history link on Assay description page
      Then Status History form is opened
    When I sort data by alphabet in 'Updated' column on Log of assay status changes grid
      Then Data in 'Updated' column on Log of assay status changes grid sorted according to alphabet
    When I sort data by alphabet in 'Status' column on Log of assay status changes grid
      Then Data in 'Status' column on Log of assay status changes grid sorted according to alphabet
    When I sort data by alphabet in 'Active' column on Log of assay status changes grid
      Then Data in 'Active' column on Log of assay status changes grid sorted according to alphabet
    When I sort data by alphabet in 'Inactive' column on Log of assay status changes grid
      Then Data in 'Inactive' column on Log of assay status changes grid sorted according to alphabet

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0121 Validation for required fields assay status on add assay page
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I click 'Change status' on Change assay status form
      Then Message 'Month is required' is displayed for required fields on Change assay status form
      And Message 'Year is required' is displayed for required fields on Change assay status form
      And Red border is displayed for following fields on Change assay status form:
        | Month To |
        | Year To  |
    When I close Change assay status form
      Then Change assay status form is closed
      And Status 'active' is displayed on Add an Assay page
    When I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To | random |
    And I click 'Change status' on Change assay status form
      Then Message 'Month is required' is displayed for required fields on Change assay status form
      And Red border is displayed for following fields on Change assay status form:
        | Month To |
    When I close Change assay status form
      Then Change assay status form is closed
    When I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Message 'Year is required' is displayed for required fields on Change assay status form
      And Red border is displayed for following fields on Change assay status form:
        | Year To |
    When I close Change assay status form
      Then Change assay status form is closed
    When I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To  | 2018   |
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Message 'Please ensure the active date predates the inactive date' is displayed on Add an Assay page
      And Status 'inactive' is displayed on Add an Assay page

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0122 Validation for cancel button on Change assay status form
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To  | 2021   |
      | Month To | random |
    When I click 'Cancel' on Change assay status form
      Then Change assay status form is closed
      And Status 'active' is displayed on Add an Assay page

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0123 Validation for cancel button on Reactivate this assay form
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    And I set status 'Inactive' for Assay 'assay' on Add an Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To  | 2020   |
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Add an Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I set status 'Active' for Assay 'assay' on Edit Assay page
      Then Reactivate this assay form is opened on Edit Assay page
    When I click 'Cancel' on Reactivate this assay form on Edit Assay page
      Then Reactivate this assay form is closed on Edit Assay page
      And Status 'inactive' is displayed on Edit Assay page

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0124 Validation for close button on Status History form
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I refresh page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click View status history link on Assay description page
      Then Status History form is opened
    When I close Status History form
      Then Status History form is closed
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click View status history link on Edit Assay page
      Then Status History form is opened
    When I close Status History form
      Then Status History form is closed

  @ui @uiAndApi @AssayManagement @AssayStatus
  Scenario: DIAFE:0125 Validation for required fields assay status on edit assay page
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay |
      | In-house or send-out? | Send-out   |
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I set status 'Inactive' for Assay 'assay' on Edit Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To | random |
    And I click 'Change status' on Change assay status form
      Then Message 'Month is required' is displayed for required fields on Change assay status form
      And Red border is displayed for following fields on Change assay status form:
        | Month To |
    When I close Change assay status form
      Then Change assay status form is closed
    When I set status 'Inactive' for Assay 'assay' on Edit Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Message 'Year is required' is displayed for required fields on Change assay status form
      And Red border is displayed for following fields on Change assay status form:
        | Year To |
    When I close Change assay status form
      Then Change assay status form is closed
    When I set status 'Inactive' for Assay 'assay' on Edit Assay page
      Then Change assay status form is opened
    When I fill following fields on Change assay status form and save as 'assay':
      | Year To  | 2018   |
      | Month To | random |
    And I click 'Change status' on Change assay status form
      Then Message 'Please ensure the active date predates the inactive date' is displayed on Edit Assay page
      And Status 'inactive' is displayed on Edit Assay page
