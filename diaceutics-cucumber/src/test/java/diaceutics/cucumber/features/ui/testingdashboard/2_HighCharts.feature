Feature: Testing Dashboard charts

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Testing Dashboard' tools
    And I select last tab
      Then Testing Dashboard Main page is opened
    When I fill following fields on Testing Dashboard Main page:
      | Project              | Full Access Project |
      | Country              | United States       |
      | Disease              | Breast Cancer       |
      | Biomarker / Analogue | ALK                 |
      | Year From            | 2018                |
      | Month From           | January             |
      | Year To              | 2018                |
      | Month To             | January             |
    And I click 'Start' on Testing Dashboard Main page
      Then Testing Dashboard page is opened

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0005 Download Country pie chart
    When I click Download chart 'Country pie chart' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Country pie chart' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0006 Download Country map chart
    When I click Download chart 'Country map chart' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Country map chart' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0007 Download Percentage of labs performing germline testing versus somatic testing
    When I click Download chart 'Percentage of labs performing germline testing versus somatic testing for <Biomarker> for <Disease> in <Country>' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Percentage of labs performing germline testing versus somatic testing for <Biomarker> for <Disease> in <Country>' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0008 Download DMS of labs performing germline versus somatic testing
    When I click Download chart 'Disease market share of labs performing germline versus somatic testing for <Biomarker> for <Disease> in <Country>' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Disease market share of labs performing germline versus somatic testing for <Biomarker> for <Disease> in <Country>' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0009 Download Platforms available within labs testing
    When I click Download chart 'Platforms available within labs testing for <Biomarker> for <Disease>' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Platforms available within labs testing for <Biomarker> for <Disease>' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0010 Download DMS represented by platforms
    When I click Download chart 'Disease market share represented by available platforms within labs testing for <Biomarker> for <Disease>' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Disease market share represented by available platforms within labs testing for <Biomarker> for <Disease>' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0011 Download DMS represented by labs offering in-house versus
    When I click Download chart 'Disease Market share represented by labs offering in-house versus send-out testing for <Biomarker> for <Disease>' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Disease Market share represented by labs offering in-house versus send-out testing for <Biomarker> for <Disease>' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0012 Download Percentage of labs performing in-house Biomarker testing
    When I click Download chart 'Percentage of labs performing in-house <Biomarker> testing versus sending out for testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Percentage of labs performing in-house <Biomarker> testing versus sending out for testing' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'Percentage of labs performing in-house <Biomarker> testing versus sending out for testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0013 Download Labs performing Biomarker testing by DMS
    When I click Download chart 'Labs performing <Biomarker> testing by disease market share' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Labs performing <Biomarker> testing by disease market share' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'Labs performing <Biomarker> testing by disease market share' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0014 Download Percentage of labs offering Biomarker testing
    When I click Download chart 'Percentage of labs offering <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Percentage of labs offering <Biomarker> testing' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'Percentage of labs offering <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0015 Download Disease market share represented by labs offering/not offering
    When I click Download chart '<Disease> market share represented by labs offering/not offering <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart '<Disease> market share represented by labs offering/not offering <Biomarker> testing' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart '<Disease> market share represented by labs offering/not offering <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0016 Download Disease market share analyzed by lab classification
    When I click Download chart '% <Disease> market share analyzed by lab classification' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart '% <Disease> market share analyzed by lab classification' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart '% <Disease> market share analyzed by lab classification' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0017 Download In-house methods used for Biomarker testing
    When I click Download chart 'In-house methods used for <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'In-house methods used for <Biomarker> testing' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'In-house methods used for <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0018 Download Disease market share represented by different in-house methods
    When I click Download chart '<Disease> market share represented by different in-house methods used for <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart '<Disease> market share represented by different in-house methods used for <Biomarker> testing' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart '<Disease> market share represented by different in-house methods used for <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0019 Download Top 15 labs Biomarker capabilities by Disease market share
    When I click Download chart 'Top 15 labs <Biomarker> capabilities by <Disease> market share' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Top 15 labs <Biomarker> capabilities by <Disease> market share' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0020 Download Number of different methods available for in-house Biomarker testing
    When I click Download chart 'Number of different methods available for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Number of different methods available for in-house <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0021 Download Disease market share represented by labs using 1 or more methods
    When I click Download chart '<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart '<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0022 Download LDT versus commercial assays
    When I click Download chart 'LDT versus commercial assays' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'LDT versus commercial assays' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'LDT versus commercial assays' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0023 Download Labs using LDTs versus commercial assays by DMS
    When I click Download chart 'Labs using LDTs versus commercial assays by DMS' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Labs using LDTs versus commercial assays by DMS' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'Labs using LDTs versus commercial assays by DMS' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0024 Download LDT versus commercial assays by method
    When I click Download chart 'LDT versus commercial assays by method' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'LDT versus commercial assays by method' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'LDT versus commercial assays by method' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0025 Download LDT versus commercial assays by method second
    When I click Download chart 'LDT versus commercial assays by method_2' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'LDT versus commercial assays by method_2' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'LDT versus commercial assays by method_2' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0026 Download Commercial assays used for in-house Biomarker testing
    When I click Download chart 'Commercial assays used for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Commercial assays used for in-house <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0027 Download Disease market share represented by commercial assays
    When I click Download chart '<Disease> market share represented by commercial assays used for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart '<Disease> market share represented by commercial assays used for in-house <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0028 Download Frequency histogram of average TATs for in-house Biomarker
    When I click Download chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0061 Download Frequency histogram of average TATs for in-house Biomarker testing split
    When I click Download chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Biomarker> market share' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Biomarker> market share' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Biomarker> market share' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0062 Download Segmentation plot showing assay TaTs for in-house
    When I click Download chart 'Segmentation plot showing assay TaTs for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'Segmentation plot showing assay TaTs for in-house <Biomarker> testing' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'Segmentation plot showing assay TaTs for in-house <Biomarker> testing' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardCharts @SingleThread @TestingDashboardChartsPDF
  Scenario: DIATD:0063 Download EQA providers utilized by labs
    When I click Download chart 'EQA providers utilized by labs' on Charts form on Testing Dashboard page
    And I save title to 'title' from chart 'EQA providers utilized by labs' on Testing Dashboard page
    And I save subtitle to 'subTitle' from chart 'EQA providers utilized by labs' on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'title'
      And File 'chart.pdf' contains a chart on Testing Dashboard with text 'subTitle'

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0029 Download Excel Country pie chart
    When I click Export data chart 'Country pie chart' on Charts form on Testing Dashboard page
      Then File 'testing-by-labs-graph-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0030 Download Excel Country map chart
    When I click Export data chart 'Country map chart' on Charts form on Testing Dashboard page
      Then File 'map-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0031 Download Excel Percentage of labs performing germline testing versus somatic testing
    When I click Export data chart 'Percentage of labs performing germline testing versus somatic testing for <Biomarker> for <Disease> in <Country>' on Charts form on Testing Dashboard page
      Then File 'germline-somatic-graph-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0032 Download Excel Disease market share of labs performing germline versus somatic testing
    When I click Export data chart 'Disease market share of labs performing germline versus somatic testing for <Biomarker> for <Disease> in <Country>' on Charts form on Testing Dashboard page
      Then File 'germline-somatic-dms-graph-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0033 Download Excel Platforms available within labs testing for Biomarker for Disease
    When I click Export data chart 'Platforms available within labs testing for <Biomarker> for <Disease>' on Charts form on Testing Dashboard page
      Then File 'platform-graph-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0034 Download Excel Disease market share represented by available platforms
    When I click Export data chart 'Disease market share represented by available platforms within labs testing for <Biomarker> for <Disease>' on Charts form on Testing Dashboard page
      Then File 'platform-dms-graph-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0035 Download Excel Disease Market share represented by labs offering in-house versus send-out
    When I click Export data chart 'Disease Market share represented by labs offering in-house versus send-out testing for <Biomarker> for <Disease>' on Charts form on Testing Dashboard page
      Then File 'in-house-send-out-graph-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0036 Download Excel Percentage of labs performing in-house Biomarker testing
    When I click Export data chart 'Percentage of labs performing in-house <Biomarker> testing versus sending out for testing' on Charts form on Testing Dashboard page
      Then File 'graph-1-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0037 Download Excel Labs performing Biomarker testing by DMS
    When I click Export data chart 'Labs performing <Biomarker> testing by disease market share' on Charts form on Testing Dashboard page
      Then File 'graph-1-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0038 Download Excel Percentage of labs offering Biomarker testing
    When I click Export data chart 'Percentage of labs offering <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-12-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0039 Download Excel Disease market share represented by labs offering/not offering
    When I click Export data chart '<Disease> market share represented by labs offering/not offering <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-12-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0040 Download Excel Disease market share analyzed by lab classification
    When I click Export data chart '% <Disease> market share analyzed by lab classification' on Charts form on Testing Dashboard page
      Then File 'graph-3-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0041 Download Excel In-house methods used for Biomarker testing
    When I click Export data chart 'In-house methods used for <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-2-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0042 Download Excel Disease market share represented by different in-house
    When I click Export data chart '<Disease> market share represented by different in-house methods used for <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-2-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0043 Download Excel Top 15 labs Biomarker capabilities by Disease market share
    When I click Export data chart 'Top 15 labs <Biomarker> capabilities by <Disease> market share' on Charts form on Testing Dashboard page
      Then File 'graph-4-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0044 Download Excel Number of different methods available for in-house
    When I click Export data chart 'Number of different methods available for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-6-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0045 Download Excel Disease market share represented by labs using 1 or more methods
    When I click Export data chart '<Disease> market share represented by labs using 1 or more methods for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-6-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0046 Download Excel LDT versus commercial assays
    When I click Export data chart 'LDT versus commercial assays' on Charts form on Testing Dashboard page
      Then File 'graph-7-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0047 Download Excel Labs using LDTs versus commercial assays by DMS
    When I click Export data chart 'Labs using LDTs versus commercial assays by DMS' on Charts form on Testing Dashboard page
      Then File 'graph-7-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0048 Download Excel LDT versus commercial assays by method
    When I click Export data chart 'LDT versus commercial assays by method' on Charts form on Testing Dashboard page
      Then File 'graph-8-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0049 Download Excel LDT versus commercial assays by method second
    When I click Export data chart 'LDT versus commercial assays by method_2' on Charts form on Testing Dashboard page
      Then File 'graph-8-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0050 Download Excel Commercial assays used for in-house Biomarker testing
    When I click Export data chart 'Commercial assays used for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'chart-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0051 Download Excel Disease market share represented by commercial assays
    When I click Export data chart '<Disease> market share represented by commercial assays used for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-9-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0052 Download Excel Frequency histogram of average TATs for in-house
    When I click Export data chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method' on Charts form on Testing Dashboard page
      Then File 'graph-10-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0053 Download Excel Frequency histogram of average TATs for in-house Biomarker testing
    When I click Export data chart 'Frequency histogram of average TATs for in-house <Biomarker> testing split by method and analysed by % <Biomarker> market share' on Charts form on Testing Dashboard page
      Then File 'graph-10-dms-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0054 Download Excel Segmentation plot showing assay TaTs for in-house Biomarker testing
    When I click Export data chart 'Segmentation plot showing assay TaTs for in-house <Biomarker> testing' on Charts form on Testing Dashboard page
      Then File 'graph-11-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @TestingDashboardChartsExcel
  Scenario: DIATD:0064 Download Excel EQA providers utilized by labs
    When I click Export data chart 'EQA providers utilized by labs' on Charts form on Testing Dashboard page
      Then File 'eqa-data.xlsx' is downloaded

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @SingleThread @TestingDashboardChartsExcel
  Scenario: DIATD:0055 Validation for Country Map HighChart downloading Excel Files
    When I read tooltips from Map chart and save as 'tooltips'
    And I click Export data chart 'Country map chart' on Charts form on Testing Dashboard page
      Then File 'map-data.xlsx' is downloaded
      And I get tooltips from UI from saved file 'tooltips' and compare with Excel file 'map-data.xlsx'

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @SingleThread
  Scenario: DIATD:0065 Download All Charts
    When I click Download all charts on Charts form on Testing Dashboard page
    And I save titles to 'titles' from all charts on Testing Dashboard page
    And I save subtitles to 'subtitles' from all charts on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains all charts values from 'titles' on Testing Dashboard page
      And File 'chart.pdf' contains all charts values from 'subtitles' on Testing Dashboard page

  @ui @TestingDashboard @TestingDashboardChartsNotClearDownload @SingleThread
  Scenario: DIATD:0066 Edit search and Download all charts
    When I click 'Edit' on Testing Dashboard page
      Then Edit settings form is opened on Testing Dashboard page
    When I fill following fields on Edit settings form on Testing Dashboard page and save 'search filter':
      | Disease              | Bladder Cancer      |
      | Biomarker / Analogue | EPCAM(DEL/DUP ONLY) |
      Then I click 'Apply' on Edit settings form on Testing Dashboard
    When I click Download all charts on Charts form on Testing Dashboard page
    And I save titles to 'titles' from all charts on Testing Dashboard page
    And I save subtitles to 'subtitles' from all charts on Testing Dashboard page
      Then File 'chart.pdf' is downloaded
      And File 'chart.pdf' contains all charts values from 'titles' on Testing Dashboard page
      And File 'chart.pdf' contains all charts values from 'subtitles' on Testing Dashboard page