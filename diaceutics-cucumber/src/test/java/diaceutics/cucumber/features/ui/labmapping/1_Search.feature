Feature: Lab Mapping search

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened

  @ui @LabMapping @LabMappingSearch
  Scenario: DIALM:0001 Required fields validation
    When I click 'Start' on Lab Mapping Main page
      Then Message 'Some items below need your attention.' is displayed on Lab Mapping Main page
      And Message 'Please select a project' is displayed on required fields on Lab Mapping Main page
    When I fill following fields on Lab Mapping Main page:
      | Project | Full Access Project |
      Then Message 'Please input a country' is displayed on required fields on Lab Mapping Main page
      And Message 'Disease is required' is displayed on required fields on Lab Mapping Main page
      And Message 'A biomarker is required' is displayed on required fields on Lab Mapping Main page
      And Message 'You specified an invalid date range' is displayed on required fields on Lab Mapping Main page
    When I fill following fields on Lab Mapping Main page:
      | Criteria type | Method |
    And I click 'Start' on Lab Mapping Main page
      Then Message 'A method is required' is displayed on required fields on Lab Mapping Main page
    When I fill following fields on Lab Mapping Main page:
      | Country    | random  |
      | Disease    | random  |
      | Method     | random  |
      | Year From  | 2018    |
      | Month From | April   |
      | Year To    | 2018    |
      | Month To   | January |
    And I click 'Start' on Lab Mapping Main page
      Then Message 'Some items below need your attention.' is displayed on Lab Mapping Main page
      And Message 'You specified an invalid date range' is displayed on required fields on Lab Mapping Main page

  @ui @LabMapping @LabMappingSearch
  Scenario: DIALM:0002 Search filter editing
    When I fill following fields on Lab Mapping Main page and save 'search filter':
      | Project          | Full Access Project |
      | Country          | random              |
      | Disease          | random              |
      | Biomarker/Analog | random              |
      | Year From        | 2018                |
      | Month From       | random              |
      | Year To          | 2019                |
      | Month To         | random              |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Lab Mapping page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Country          | random |
      | Disease          | random |
      | Biomarker/Analog | random |
      | Year From        | 2018   |
      | Month From       | random |
      | Year To          | 2019   |
      | Month To         | random |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Lab Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Lab Mapping page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |

  @ui @LabMapping @LabMappingSearch
  Scenario: DIALM:0142 Check inform messages when data loading
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2017                |
      | Month To         | March               |
    And I click 'Start' without wait on Lab Mapping Main page
      Then All three spinners are present on Lab Mapping page:
      | We're fetching your data  |
      | Performing calculations   |
      | Sorting your data         |
      | Populating the data table |

  @ui @LabMapping @LabMappingSearch
  Scenario: DIALM:0143 Lab mapping search using a specified method
    When I fill following fields on Lab Mapping Main page:
      | Project       | Full Access Project |
      | Country       | Albania             |
      | Disease       | random              |
      | Criteria type | Method              |
      | Method        | random              |
      | Year From     | 2018                |
      | Month From    | April               |
      | Year To       | 2021                |
      | Month To      | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened

  @ui @LabMapping @LabMappingSearch
  Scenario: DIALM:0144 Search filter editing using a specified method
    When I fill following fields on Lab Mapping Main page and save 'search filter':
      | Project       | Full Access Project |
      | Country       | Albania             |
      | Disease       | random              |
      | Criteria type | Method              |
      | Method        | random              |
      | Year From     | 2018                |
      | Month From    | April               |
      | Year To       | 2021                |
      | Month To      | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Lab Mapping page
        | Country    |
        | Disease    |
        | Method     |
        | Date Range |
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Country          | random    |
      | Disease          | random    |
      | Criteria type    | Biomarker |
      | Biomarker/Analog | random    |
      | Year From        | 2018      |
      | Month From       | random    |
      | Year To          | 2019      |
      | Month To         | random    |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Lab Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Lab Mapping page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |

  @ui @LabMapping @LabMappingSearch
  Scenario: DIALM:0146 Canceled search filter editing
    When I fill following fields on Lab Mapping Main page and save 'search filter':
      | Project          | Full Access Project |
      | Country          | random              |
      | Disease          | random              |
      | Biomarker/Analog | random              |
      | Year From        | 2018                |
      | Month From       | random              |
      | Year To          | 2019                |
      | Month To         | random              |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Lab Mapping page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I fill following fields on Edit settings form on Lab Mapping page and save 'new search filter':
      | Country          | random |
      | Disease          | random |
      | Biomarker/Analog | random |
      | Year From        | 2020   |
      | Month From       | random |
      | Year To          | 2021   |
      | Month To         | random |
    And I click 'Cancel' on Edit settings form on Lab Mapping
      Then Lab Mapping page is opened
      And Search filter 'search filter' with following fields is displayed on Lab Mapping page
        | Country    |
        | Disease    |
        | Biomarker  |
        | Date Range |

  @ui @LabMapping @LabMappingSearch
  Scenario: DIALM:0152 Required fields validation for edit settings form
    When I fill following fields on Lab Mapping Main page and save 'search filter':
      | Project          | Full Access Project |
      | Country          | random              |
      | Disease          | random              |
      | Biomarker/Analog | random              |
      | Year From        | 2018                |
      | Month From       | random              |
      | Year To          | 2019                |
      | Month To         | random              |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click 'Edit' on Lab Mapping page
      Then Edit settings form is opened on Lab Mapping page
    When I clear following fields on Edit settings form on Lab Mapping page
      | Country          |
      | Disease          |
      | Biomarker/Analog |
    And I click 'Apply' on Edit settings form on Lab Mapping
      Then Message 'Some items below need your attention.' is displayed on Edit settings form on Lab Mapping
      And Message 'Please input a country' is displayed on required fields on Edit settings form on Lab Mapping
      And Message 'Disease is required' is displayed on required fields on Edit settings form on Lab Mapping
      And Message 'A biomarker is required' is displayed on required fields on Edit settings form on Lab Mapping
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Criteria type | Method |
      Then Message 'A method is required' is displayed on required fields on Edit settings form on Lab Mapping
    When I fill following fields on Edit settings form on Lab Mapping page and save 'search filter':
      | Year From | 2020 |
      Then Message 'You specified an invalid date range' is displayed on required fields on Edit settings form on Lab Mapping
