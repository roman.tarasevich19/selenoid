Feature: Lab Mapping HighCharts
  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0120 Validation modal dataTable for chart Labs using LDTs versus commercial assays by DMS
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2018                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click 'Charts' tab on Lab Mapping page
      Then Charts form on Lab Mapping page is opened
    When I get percents for following legend from Pie chart 'Labs using LDTs versus commercial assays by DMS' save as 'Sum DMS %':
      | Commercial assay |
      | Both             |
    And I click by 'pink' pie sector in chart 'Labs using LDTs versus commercial assays by DMS' on Lab Mapping page
      Then Drill down of Commercial Assay segment form is opened
      And I get value for 'Unspecified' row in 'DMS %' column and compare with 'Sum DMS %' on Drill down of Commercial Assay segment form

  @ui @uiAndApi @LabMapping @LabMappingHighCharts @SingleThread
  Scenario Outline: <test count> Check number of labs for <chart> chart
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Country          | Italy               |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2019                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click 'Charts' tab on Lab Mapping page
      Then Charts form on Lab Mapping page is opened
    When I get number from 'labs assays for biomarker' heading banner on Lab Mapping page and save to 'number of labs'
      Then 'number of labs' must be the same as a total number of labs on '<chart>' chart on Charts form on Lab Mapping page
    When I click 'Data tools' on Master Header form
    And I click 'Assay Management' from dropdown menu on Master Header form
      Then Assay Management page is opened
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab for LM chart |
      | countryCode | ITA                   |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I put a Lab 'lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'lab' is displayed in filter results on Labs page
    When I select 'lab' lab on Labs page
      Then Lab Profile page is opened
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay for LM Chart  |
      | Detects               | Germline                 |
      | Method                | Activity assay           |
      | Turnaround time (TAT) | 30                       |
      | Ontology              | DELETION                 |
      | Classification        | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and add to 'assay':
      | Biomarker | ALK                  |
      | Variants  | Entire gene coverage |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on 'Add volume' on Lab Profile Page
      Then New volume page is opened
    When I fill following fields on New volume page and save as 'volume':
      | Year              | 2018                        |
      | Quarter           | Q1                          |
      | Disease           | Breast Cancer               |
      | Disease volume    | 5                           |
      | Biomarker details | No biomarker volumes to log |
    And I click 'Save and finish' on New volume page
      Then Lab Profile page is opened
      And 'Volume successfully logged' message is displayed on Lab Profile page
      And Volume 'volume' is displayed in Patient volume log grid on Lab Profile page
    When I click 'Lab Mapping' on Master Header form
      Then Lab Mapping Main page is opened
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Country          | Italy               |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2019                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click 'Charts' tab on Lab Mapping page
      Then Charts form on Lab Mapping page is opened
    When I get number from 'labs assays for biomarker' heading banner on Lab Mapping page and save to 'new number of labs'
      Then 'new number of labs' must be the same as a total number of labs on '<chart>' chart on Charts form on Lab Mapping page
      And 'new number of labs' was increment in one after new lab was created on Lab Mapping page

    Examples:
      | chart                                                                                         | test count |
      | Labs using LDTs versus commercial assays by DMS                                               | DIALM:0121 |
      | LDT versus commercial assays                                                                  | DIALM:0122 |
      | <Disease> market share represented by different in-house methods used for <Biomarker> testing | DIALM:0123 |

  @ui @LabMapping @LabMappingHighCharts @SingleThread
  Scenario: DIALM:0145 Validation modal dataTable for chart LDT versus commercial assays
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Disease          | Breast Cancer       |
      | Biomarker/Analog | ALK                 |
      | Country          | United States       |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2018                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click 'Charts' tab on Lab Mapping page
      Then Charts form on Lab Mapping page is opened
    When I get number of labs for 'Commercial assay' legend from Pie chart 'LDT versus commercial assays' save as 'N of labs'
    And I click by 'pink' pie sector in chart 'LDT versus commercial assays' on Lab Mapping page
      Then Drill down of Commercial Assay segment form is opened
      And I get value for 'Unspecified' row in 'N of labs' column and compare with 'N of labs' on Drill down of Commercial Assay segment form
