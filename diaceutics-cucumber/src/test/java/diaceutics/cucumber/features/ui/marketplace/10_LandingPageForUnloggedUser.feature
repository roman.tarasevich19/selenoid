Feature: Landing page for un logged user

  Background:
    Given Marketplace Main page is opened
    When I click 'Collaborations' on Master Header form
      Then Collaborations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened

  @ui @Marketplace @LandingPageForUnLoggedUser
  Scenario: DIAM:0054 Validation for category and keywords search filter
    When I set following fields on Search form and save as 'filter':
      | Location    | Afghanistan   |
      | Type        | Collaboration |
      | Sponsored   | true          |
      | Test Review | true          |
      | Offered     | true          |
      | Keywords    | test          |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I click DXRX logo on the top left on Master Header form
      Then Marketplace Main page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location               | Afghanistan   |
      | Type                   | Collaboration |
      | Requested              | true          |
      | Testing Icon Group one | true          |
      | Testing Icon Group two | true          |
      | Keywords               | new test      |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
      And Search filter 'filter' for following fields is displayed on Search form:
        | Location               |
        | Type                   |
        | Sponsored              |
        | Test Review            |
        | Offered                |
        | Requested              |
        | Testing Icon Group one |
        | Testing Icon Group two |
        | Keywords               |
    When I click 'Organizations' on Master Header form
      Then Organizations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Type           | Organization |
      | Laboratory     | true         |
      | Diagnostics    | true         |
      | Pharmaceutical | true         |
      | Keywords       | test         |
    And I click search button on Search form
      Then Organizations Listing page is opened
    When I click DXRX logo on the top left on Master Header form
      Then Marketplace Main page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location         | Afghanistan  |
      | Type             | Organization |
      | Service Provider | true         |
      | Diaceutics       | true         |
      | Other            | true         |
      | Keywords         | new test     |
    And I click search button on Search form
      Then Organizations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
      And Search filter 'filter' for following fields is displayed on Search form:
        | Location         |
        | Type             |
        | Laboratory       |
        | Diagnostics      |
        | Pharmaceutical   |
        | Service Provider |
        | Diaceutics       |
        | Other            |
        | Keywords         |

  @ui @Marketplace @Collaboration @LandingPageForUnLoggedUser
  Scenario: DIAM:0070 Validation search filter with change location
    When I set following fields on Search form and save as 'filter':
      | Location | Afghanistan   |
      | Type     | Collaboration |
    And I click search button on Search form
      Then Collaborations Listing page is opened
      And Collaboration 'Test collaboration' is displayed on Collaborations Listing page
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location | Albania       |
      | Type     | Collaboration |
    And I click search button on Search form
      Then Collaborations Listing page is opened
      And Collaboration 'Test collaboration two' is displayed on Collaborations Listing page
    When I click 'Organizations' on Master Header form
      Then Organizations Listing page is opened
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location | United States |
      | Type     | Organization  |
    And I click search button on Search form
      Then Organizations Listing page is opened
      And Organizations by location from 'filter' are displayed on Organizations Listing page
    When I open Search form on on Master Header form
      Then Search form is opened
    When I set following fields on Search form and save as 'filter':
      | Location | United Kingdom |
      | Type     | Organization   |
    And I click search button on Search form
      Then Organizations Listing page is opened
      And Organizations by location from 'filter' are displayed on Organizations Listing page
    When I open Search form on on Master Header form
      Then Search form is opened
    When I clear 'Location' field on Search form
    And I click search button on Search form
      Then Organizations Listing page is opened
      And Organizations are displayed on Organizations Listing page
