Feature: Import Biomarkers

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
    And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
    And I save Lab as 'lab' from response
    When I put a Lab 'lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
    And Lab 'lab' is displayed in filter results on Labs page
    When I select 'lab' lab on Labs page
      Then Lab Profile page is opened
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay       |
      | Detects               | random           |
      | Method                | random           |
      | Turnaround time (TAT) | 5                |
      | Ontology              | random           |
      | Classification        | Commercial assay |
      | FDA 510K APPROVED KIT | true             |
      | FDA PMA APPROVED KIT  | false            |
      | IVD-CE                | false            |
      | RUO/IUO               | false            |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0079 Biomarkers sorted by name ASC
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_valid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of invalid biomarkers from file 'Biomarkers_valid.csv' and save to 'invalidBiomarkers'
    And I get number of valid biomarkers from file 'Biomarkers_valid.csv' and save to 'validBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid.csv' are displayed in grid in Lab Test Biomarker form
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
    And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Biomarkers are sorted by name ASC on Assay description page

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0080 Import Biomarkers Modal Window is opened
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I click 'Cancel' on Upload biomarkers form
    And I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0081 Import Biomarkers Invalid file format
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'TestPDF.pdf' on Import Biomarkers form
      Then Message 'Please check your file is in the correct format and contains at least one valid biomarker' is present on Upload biomarkers form

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0082 Import Biomarkers File too big
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Listing.csv' on Import Biomarkers form
      Then Message 'Your file exceeds the upload limit of 40Kb' is present on Upload biomarkers form

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0083 Import Biomarkers File invalid content
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalidContent.csv' on Import Biomarkers form
      Then Message 'Please check your file is in the correct format and contains at least one valid biomarker' is present on Upload biomarkers form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0084 Import Biomarkers File upload
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_valid.csv' on Import Biomarkers form
    When I click 'Import file' on Upload biomarkers form
      And I get number of valid biomarkers from file 'Biomarkers_valid.csv' and save to 'validBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid.csv' are displayed in grid in Lab Test Biomarker form
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0085 Import Biomarkers File duplicate bio
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_valid_distinct.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_valid_distinct.csv' and save to 'validBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid_distinct.csv' are displayed in grid in Lab Test Biomarker form
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0086 Import Biomarkers already on assay
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_valid_distinct.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_valid_distinct.csv' and save to 'validBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid_distinct.csv' are displayed in grid in Lab Test Biomarker form
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_valid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_valid.csv' and save to 'validBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
    And Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      Then Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid.csv' are displayed in grid in Lab Test Biomarker form
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0087 Import Biomarkers Biomarker not exists
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_invalid.csv' and save to 'validBiomarkers'
    And I get number of invalid biomarkers from file 'Biomarkers_invalid.csv' and save to 'invalidBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Concat value from 'invalidBiomarkers' and check that message 'invalid biomarkers detected' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_invalid.csv' are displayed in grid in Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0088 Import Biomarkers Load more
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_100.csv' on Import Biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_100.csv' and save to 'validBiomarkers'
    And I get number of invalid biomarkers from file 'Biomarkers_100.csv' and save to 'invalidBiomarkers'
    And I click 'Import file' on Upload biomarkers form
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
      And I click 'Finish' on Upload biomarkers form
    When Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
    And Concat value from 'invalidBiomarkers' and check that message 'invalid' is present on Upload biomarkers form
      Then '21' rows of valid biomarkers are displayed on Lab Test Biomarker form
    When I click 'Load more' on Upload biomarkers form
      Then '41' rows of valid biomarkers are displayed on Lab Test Biomarker form
    When I click 'Load more' on Upload biomarkers form
      Then '61' rows of valid biomarkers are displayed on Lab Test Biomarker form
    When I click 'Load more' on Upload biomarkers form
      Then '81' rows of valid biomarkers are displayed on Lab Test Biomarker form
    When I click 'Load more' on Upload biomarkers form
      Then '96' rows of valid biomarkers are displayed on Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0089 Biomarkers Save and finish Discard
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_invalid.csv' and save to 'validBiomarkers'
    And I get number of invalid biomarkers from file 'Biomarkers_invalid.csv' and save to 'invalidBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
      And I click 'Finish' on Upload biomarkers form
      And Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Concat value from 'invalidBiomarkers' and check that message 'invalid biomarkers detected' is present on Upload biomarkers form
    When I click 'Save and finish' on Add an Assay page
      Then Message 'Your import contained invalid biomarkers. 2 of these biomarkers have not yet been resolved. If you proceed to save, these biomarkers will be discarded' is displayed on Add an Assay page
    When I click 'Review invalid' on Add an Assay page
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Concat value from 'invalidBiomarkers' and check that message 'invalid biomarkers detected' is present on Upload biomarkers form
    When I click 'Save and finish' on Add an Assay page
    And I click 'Discard and save' on Add an Assay page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0090 Biomarkers Discard all invalid biomarkers
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_invalid.csv' and save to 'validBiomarkers'
    And I get number of invalid biomarkers from file 'Biomarkers_invalid.csv' and save to 'invalidBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Concat value from 'invalidBiomarkers' and check that message 'invalid biomarkers detected' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and number of rows of invalid biomarkers are the same in 'Biomarkers_invalid.csv' and displayed on Lab Test Biomarker form
    When I click on 'Discard all' biomarkers on Lab Test Biomarker form
      Then Message 'This will discard all invalid biomarkers and cannot be undone.' is displayed on Add an Assay page
    When I click 'Discard invalid' on Add an Assay page
      Then Compare with 'allBiomarkers' and number of rows of invalid biomarkers are not the same in 'Biomarkers_invalid.csv' and displayed on Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0091 Biomarkers Discard one invalid biomarkers
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_invalid.csv' and save to 'validBiomarkers'
    And I get number of invalid biomarkers from file 'Biomarkers_invalid.csv' and save to 'invalidBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Concat value from 'invalidBiomarkers' and check that message 'invalid biomarkers detected' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
    And Compare with 'allBiomarkers' and number of rows of invalid biomarkers are the same in 'Biomarkers_invalid.csv' and displayed on Lab Test Biomarker form
    And I click 'Discard' on Add an Assay page
      Then Compare with 'allBiomarkers' and number of rows of invalid biomarkers are not the same in 'Biomarkers_invalid.csv' and displayed on Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0092 Biomarkers Change invalid to valid
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I click 'Finish' on Upload biomarkers form
    And I get number of invalid biomarkers from file 'Biomarkers_invalid.csv' and save to 'invalidBiomarkers'
    And I get number of valid biomarkers from file 'Biomarkers_invalid.csv' and save to 'validBiomarkers'
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Concat value from 'invalidBiomarkers' and check that message 'invalid biomarkers detected' is present on Upload biomarkers form
    When I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and number of rows of invalid biomarkers are the same in 'Biomarkers_invalid.csv' and displayed on Lab Test Biomarker form
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'valid':
      | Should be | ALK |
    When I click 'Add' on Add an Assay page
      Then Compare with 'allBiomarkers' and number of rows of invalid biomarkers are not the same in 'Biomarkers_invalid.csv' and displayed on Lab Test Biomarker form
      And Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid.csv' are displayed in grid in Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0093 Biomarkers Change invalid to valid duplicate
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
    And I get number of invalid biomarkers from file 'Biomarkers_invalid.csv' and save to 'invalidBiomarkers'
    And I get number of valid biomarkers from file 'Biomarkers_invalid.csv' and save to 'validBiomarkers'
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Concat value from 'invalidBiomarkers' and check that message 'invalid biomarkers detected' is present on Upload biomarkers form
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'valid':
      | Should be | 19 |
    And I click 'Add' on Add an Assay page
    And I get all biomarkers from application and save to 'allBiomarkers'
      Then Compare with 'allBiomarkers' and number of rows of invalid biomarkers are not the same in 'Biomarkers_invalid.csv' and displayed on Lab Test Biomarker form
      And Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid_shouldBe.csv' are displayed in grid in Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0094 Biomarkers Save and finish
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerOne':
      | Biomarker | random |
      | Variants  | random |
    And I click '+ Add another' on Add an Assay page
    And I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerTwo':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    And Biomarker 'biomarkerOne' is displayed on Assay description page
    And Biomarker 'biomarkerTwo' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0095 Biomarkers Back link
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerOne':
      | Biomarker | 19                   |
      | Variants  | Entire gene coverage |
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I click '+ Add another' on Add an Assay page
    And I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerTwo':
      | Biomarker | ALK |
      | Variants  | Entire gene coverage |
    When I click on 'Previous: Assay Details' biomarkers on Lab Test Biomarker form
      Then Edit Assay page is opened
    When I click on 'Back' biomarkers on Lab Test Biomarker form
      Then Assay description page is opened
    When I click on 'Back' biomarkers on Lab Test Biomarker form
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Biomarker 'biomarkerOne' is displayed on Assay description page
      And Biomarker 'biomarkerTwo' is not displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0096 Biomarkers Cancel from Lab Test form
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_valid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I get number of valid biomarkers from file 'Biomarkers_invalid.csv' and save to 'validBiomarkers'
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
    When I click 'Finish' on Upload biomarkers form
    And I get all biomarkers from application and save to 'allBiomarkers'
      Then Concat value from 'validBiomarkers' and check that message 'biomarkers imported successfully' is present on Upload biomarkers form
      And Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid.csv' are displayed in grid in Lab Test Biomarker form
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I click '+ Add another' on Add an Assay page
    And I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerTwo':
      | Biomarker | 19     |
      | Variants  | random |
    And I click 'Cancel' on Add an Assay page
    And Message 'All unsaved changes will be lost if you leave this page before saving.' is displayed on Add an Assay page
    And I click 'Cancel' on Discard form on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I click 'Cancel' on Add an Assay page
    And I click 'Leave anyway' on Discard form on Add an Assay page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
      And Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid.csv' are displayed in grid in Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0097 Biomarkers Search box
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_100.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
    And I click 'Finish' on Upload biomarkers form
    And I set 'EGFR' on search field 'Filter by biomarker or variant...' and press Search icon on Lab Test Biomarker form
      Then Message '0 matching results' is displayed on Add an Assay page
    When I set 'AD' on search field 'Filter by biomarker or variant...' and press Search icon on Lab Test Biomarker form
      And Each biomarker contains 'AD' after filtering on grid in Lab Test Biomarker form
    When I click on 'Clear filter' biomarkers on Lab Test Biomarker form
    And I click 'Discard' on Add an Assay page
    And I set 'ALK' on search field 'Filter by biomarker or variant...' and press Search icon on Lab Test Biomarker form
      Then Each biomarker contains 'ALK' after filtering on grid in Lab Test Biomarker form
    When I click on 'Clear filter' biomarkers on Lab Test Biomarker form
    And I click 'Remove all' on Add an Assay page
    And I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerOne':
      | Biomarker | 19                   |
      | Variants  | Entire gene coverage |
    And I click '+ Add another' on Add an Assay page
    And I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarkerTwo':
      | Biomarker | ALK |
      | Variants  | Entire gene coverage |
    And I click '+ Add another' on Add an Assay page
    And I set 'BRAF' value for Biomarker field in Lab Test Biomarker form on Add Assay page and save as 'biomarker'
    And I fill following values for Variants field in Lab Test Biomarker form on Add Assay page and add to 'biomarker'
      | Exon 15 V600E |
      | Exon 15 V600D |
      | Exon 15 V600K |
      Then Variants from 'biomarker' are displayed on Lab Test Biomarker form on Add an Assay page
    When I click '+ Add another' on Add an Assay page
    And I set 'EGFR' value for Biomarker field in Lab Test Biomarker form on Add Assay page and save as 'biomarker1'
    And I fill following values for Variants field in Lab Test Biomarker form on Add Assay page and add to 'biomarker1'
      | Exon 20 T790M |
      | Exon 18 G719A |
      | Exon 21 L861Q |
    And I set 'Entire' on search field 'Filter by biomarker or variant...' and press Search icon on Lab Test Biomarker form
      Then Each variant contains 'Entire' after filtering on Lab Test Biomarker form on Add an Assay page
      And '2' rows of valid biomarkers are displayed on Lab Test Biomarker form
    When I set '600' on search field 'Filter by biomarker or variant...' and press Search icon on Lab Test Biomarker form
      Then Message '1 matching result' is displayed on Add an Assay page
      And Each variant contains '600' after filtering on Lab Test Biomarker form on Add an Assay page
      And '1' rows of valid biomarkers are displayed on Lab Test Biomarker form
    When I set 'Ex' on search field 'Filter by biomarker or variant...' and press Search icon on Lab Test Biomarker form
      Then Each variant contains 'Ex' after filtering on Lab Test Biomarker form on Add an Assay page
      And '2' rows of valid biomarkers are displayed on Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0098 Biomarkers Cancel on assay details
    When I click on 'Previous: Assay Details' biomarkers on Lab Test Biomarker form
      Then Add an Assay page is opened
    When I click on 'Cancel' biomarkers on Lab Test Biomarker form
    And I click 'Cancel' on Discard form on Add an Assay page
      Then Add an Assay page is opened
    When I click on 'Cancel' biomarkers on Lab Test Biomarker form
      And I click 'Leave anyway' on Discard form on Add an Assay page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Biomarkers 
  Scenario: DIAFE:0099 Biomarkers Download template
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I click on 'Download template (CSV)' biomarkers on Lab Test Biomarker form
      Then File 'Biomarkers.csv' is downloaded
      And Downloaded CSV 'Biomarkers.csv' contains header 'Biomarkers' on Lab Test Biomarker form

  @ui @uiAndApi @AssayManagement @Biomarkers
  Scenario: DIAFE:0100 Biomarkers Save and finish Discard Edit assay
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_valid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
      Then Message 'biomarkers imported successfully' is present on Upload biomarkers form
      And I click 'Finish' on Upload biomarkers form
      And Message 'biomarkers imported successfully' is displayed on Add an Assay page
    When I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And I click Edit Details on Assay description page
      And I click 'Proceed' on Add an Assay page
      And Lab Test Biomarker form is opened on Add an Assay page
    When I click 'Import biomarkers' on Add an Assay page
      Then Upload biomarkers form is opened
    When I add a file 'Biomarkers_invalid.csv' on Import Biomarkers form
    And I click 'Import file' on Upload biomarkers form
      Then Message 'Biomarkers imported successfully' is present on Upload biomarkers form
      And I click 'Finish' on Upload biomarkers form
    When I click 'Save and finish' on Add an Assay page
      Then Message 'Your import contained invalid biomarkers. 2 of these biomarkers have not yet been resolved. If you proceed to save, these biomarkers will be discarded' is displayed on Add an Assay page
    When I click 'Review invalid' on Add an Assay page
    And I click 'Save and finish' on Add an Assay page
    And I click 'Discard and save' on Add an Assay page
      Then Lab Profile page is opened
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And I click Edit Details on Assay description page
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I get all biomarkers from application and save to 'allBiomarkers'
      And Compare with 'allBiomarkers' and valid biomarkers from imported file 'Biomarkers_valid.csv' are displayed in grid in Lab Test Biomarker form