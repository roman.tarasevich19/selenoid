Feature: Lab Mapping Export data

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Lab Mapping' tools
    And I select last tab
      Then Lab Mapping Main page is opened

  @ui @LabMapping @LabMappingExportData
  Scenario: DIALM:0020 Validation export data
    When I fill following fields on Lab Mapping Main page:
      | Project          | Full Access Project |
      | Country          | United States       |
      | Disease          | random              |
      | Biomarker/Analog | random              |
      | Year From        | 2017                |
      | Month From       | January             |
      | Year To          | 2020                |
      | Month To         | January             |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click Export data on Lab Mapping page
      Then File 'export.xlsx' is downloaded
    When I read file 'export.xlsx' with Labs and save data as 'labs from file'
      Then I read data from AG-Grid Lab summary on Lab Mapping page and compare with 'labs from file'

  @ui @LabMapping @LabMappingExportData
  Scenario: DIALM:0151 Validation export data using a specified method
    When I fill following fields on Lab Mapping Main page:
      | Project       | Full Access Project     |
      | Country       | United States           |
      | Disease       | Acute Myeloid Leukaemia |
      | Criteria type | Method                  |
      | Method        | Multiplex PCR           |
      | Year From     | 2020                    |
      | Month From    | January                 |
      | Year To       | 2021                    |
      | Month To      | January                 |
    And I click 'Start' on Lab Mapping Main page
      Then Lab Mapping page is opened
    When I click Export data on Lab Mapping page
      Then File 'export.xlsx' is downloaded
    When I read file 'export.xlsx' with Labs and save data as 'labs from file'
      Then I read data from AG-Grid Lab summary on Lab Mapping page and compare with 'labs from file'
