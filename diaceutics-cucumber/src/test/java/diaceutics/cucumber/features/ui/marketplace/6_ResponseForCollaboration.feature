Feature: Response for collaboration

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Responses' link on Collaboration Header form
      Then Responses page is opened
    When I remove all responses on Responses page
    And I click 'Home' on Master Header form
      Then Home page is opened
    When I click View all public collaborations on Home page
      Then Collaborations Listing page is opened
    When I open Search form on on Master Header form
    And I set following fields on Search form and save as 'filter':
      | Location | Afghanistan |
    And I click search button on Search form
      Then Collaborations Listing page is opened
    When I open Collaboration 'Test collaboration' on Collaborations Listing page
    And I select last tab
      Then Collaboration Show page is opened
    When I click Submit a response button on Collaborations Show page
      Then Your Response page is opened

  @ui @Marketplace @Collaboration @ResponseForCollaboration @SingleThread
  Scenario: DIAM:0036 Possibility to submit a response with valid data
    When I put random message save as 'message' on Your Response page
    And I set 'true' value for 'I accept Membership Terms & Privacy Policy' field on Your Response page
    And I click Continue button on Your Response page
      Then Response Show page is opened
      And Inform Form with following message is displayed on Response Show page:
        | Your request to participate in this collaboration has been sent. You will be contacted shortly. |
      And Location for collaboration 'Test collaboration' is displayed on Response Show page
      And Contact details for user 'User for testing collaboration' is displayed on Response Show page
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'User for testing collaboration' user
      Then Home page is opened
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Messages' link on Collaboration Header form
      Then Messages page is opened
      And Message 'message' from 'ADMIN' user is displayed on Messages page
      And Message 'message' from 'ADMIN' user was sent 'Today'
    When I click 'Responses' link on Collaboration Header form
      Then Responses page is opened
      And Response from collaboration 'Test collaboration' is displayed on Responses page
    When I click show button for response from collaboration 'Test collaboration' on Responses page
      Then Response Show page is opened
      And Location for collaboration 'Test collaboration' is displayed on Response Show page
      And Contact details for user 'ADMIN' is displayed on Response Show page

  @ui @Marketplace @Collaboration @ResponseForCollaboration @SingleThread
  Scenario Outline: <test count> Possibility to submit a response with <file type> file adding
    When I add a file '<file name>' on Question form on Your Response page
      Then File '<file name>' is added on Your Response page
    When I put random message save as 'message' on Your Response page
    And I set 'true' value for 'I accept Membership Terms & Privacy Policy' field on Your Response page
    And I click Continue button on Your Response page
      Then Response Show page is opened
      And Inform Form with following message is displayed on Response Show page:
        | Your request to participate in this collaboration has been sent. You will be contacted shortly. |
      And Location for collaboration 'Test collaboration' is displayed on Response Show page
      And Contact details for user 'User for testing collaboration' is displayed on Response Show page
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'User for testing collaboration' user
      Then Home page is opened
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Messages' link on Collaboration Header form
      Then Messages page is opened
      And Message 'message' from 'ADMIN' user is displayed on Messages page
      And Message 'message' from 'ADMIN' user was sent 'Today'
    When I click message button for 'message' from 'ADMIN' user on Messages page
      Then Conversation page is opened
      And File '<file name>' is attached to 'message' on Conversation page
      And Message 'message' was sent from a 'ADMIN' user on Conversation page
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Responses' link on Collaboration Header form
      Then Responses page is opened
      And Response from collaboration 'Test collaboration' is displayed on Responses page
    When I click show button for response from collaboration 'Test collaboration' on Responses page
      Then Response Show page is opened
      And Location for collaboration 'Test collaboration' is displayed on Response Show page
      And Contact details for user 'ADMIN' is displayed on Response Show page

    Examples:
      | file name     | test count | file type |
      | TestDOCX.docx | DIAM:0037  | docx      |
      | TestGIF.gif   | DIAM:0038  | gif       |
      | TestJPG.jpg   | DIAM:0039  | jpg       |
      | TestPDF.pdf   | DIAM:0040  | pdf       |
      | TestPNG.png   | DIAM:0041  | png       |
      | TestPPTX.pptx | DIAM:0042  | pptx      |
      | TestRTF.rtf   | DIAM:0043  | rtf       |
      | TestXLS.xls   | DIAM:0044  | xls       |

  @ui @Marketplace @Collaboration @ResponseForCollaboration @SingleThread
  Scenario: DIAM:0045 Submit a response with invalid data
    When I click Continue button on Your Response page
      Then Inform Form with message 'An error has occurred.' is displayed on Your Response page
      And Error container is displayed for 'I accept Membership Terms & Privacy Policy' field on Your Response page

  @ui @Marketplace @Collaboration @ResponseForCollaboration @SingleThread
  Scenario: DIAM:0052 Possibility to shortlist a response for collaboration
    When I put random message save as 'message' on Your Response page
    And I set 'true' value for 'I accept Membership Terms & Privacy Policy' field on Your Response page
    And I click Continue button on Your Response page
      Then Response Show page is opened
      And Inform Form with following message is displayed on Response Show page:
      | Your request to participate in this collaboration has been sent. You will be contacted shortly. |
    When I click 'Logout' on user menu on Master header
      Then Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'User for testing collaboration' user
      Then Home page is opened
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Responses' link on Collaboration Header form
      Then Responses page is opened
      And Response from collaboration 'Test collaboration' is displayed on Responses page
    When I click show button for response from collaboration 'Test collaboration' on Responses page
      Then Response Show page is opened
    When I click 'Shortlist' button on Response Show page
      Then Inform Form with following message is displayed on Response Show page:
      | Interest successfully shortlisted |
    When I click back on browser
      Then Responses page is opened
      And Response from collaboration 'Test collaboration' has status 'Shortlisted' on Responses page
    When I click show button for response from collaboration 'Test collaboration' on Responses page
      Then Response Show page is opened
    When I click 'Remove from shortlist' button on Response Show page
      Then Inform Form with following message is displayed on Response Show page:
        | Interest successfully  un-shortlisted |
    When I click back on browser
      Then Responses page is opened
      And Response from collaboration 'Test collaboration' has status 'Pending' on Responses page
