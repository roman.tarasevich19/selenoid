Feature: Find a Lab

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened

  @ui @uiAndApi @AssayManagement @FindLab
  Scenario: DIAFE:0022 Filter a lab by country and type
    When I execute the POST request Creates a new lab 'labOne' using following data:
      | name        | Test Lab |
      | countryCode | PER      |
      | type        | ACADEMIC |
      Then the status code is 200
    When I execute the POST request Creates a new lab 'labTwo' using following data:
      | name        | Test Lab |
      | countryCode | PER      |
      | type        | ACADEMIC |
      Then the status code is 200
    When I execute the POST request Creates a new lab 'labThree' using following data:
      | name        | Test Lab |
      | countryCode | PER      |
      | type        | ACADEMIC |
      Then the status code is 200
    When I choose a Country 'Peru' and press Search icon on Assay Management page
      Then Labs page is opened
    When I set '100' value for Per-page on Labs page
      Then All of the following labs for the specific country are displayed on Labs page:
        | labOne   |
        | labTwo   |
        | labThree |

  @ui @uiAndApi @AssayManagement @FindLab
  Scenario: DIAFE:0023 Filter a lab by keyword
    When I execute the POST request Creates a new lab 'labOne' using following data:
      | name        | Test Lab |
      | countryCode | PER      |
      | type        | ACADEMIC |
      Then the status code is 200
    When I put a Lab 'labOne' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'labOne' is displayed in filter results on Labs page

  @ui @AssayManagement @FindLab
  Scenario: DIAFE:0024 Validation labs count and country name
    When I click random country on Assay Management page and save 'country name' and 'labs count'
      Then Labs page is opened
      And 'country name' should be the same as a country name on Labs page
      And 'labs count' should be the same as a labs count on Labs page

  @ui @AssayManagement @FindLab
  Scenario: DIAFE:0025 Compare countries quantity in dropdown and page
    When I get list countries from dropdown on Assay Management page and save as 'countries'
      Then List 'countries' should be the same as a list countries on Assay Management

  @ui @AssayManagement @FindLab
  Scenario: DIAFE:0026 Validation range of results for labs quantity per pagination
    When I choose a Country 'United States' and press Search icon on Assay Management page
      Then Labs page is opened
    When I set '10' value for Per-page on Labs page
      Then Labs from '1' to '10' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '11' to '20' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '21' to '30' should be displayed on Labs page
    When I click First on Labs page
    And I set '25' value for Per-page on Labs page
      Then Labs from '1' to '25' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '26' to '50' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '51' to '75' should be displayed on Labs page
    When I click First on Labs page
    And I set '50' value for Per-page on Labs page
      Then Labs from '1' to '50' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '51' to '100' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '101' to '150' should be displayed on Labs page
    When I click First on Labs page
    And I set '100' value for Per-page on Labs page
      Then Labs from '1' to '100' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '101' to '200' should be displayed on Labs page
    When I click Next on Labs page
      Then Labs from '201' to '300' should be displayed on Labs page

  @ui @uiAndApi @AssayManagement @FindLab
  Scenario: DIAFE:0054 Filter a lab by country and keyword
    When I execute the POST request Creates a new lab 'labOne' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
    When I choose a Country 'Albania' on Assay Management page
    And I put a Lab 'labOne' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'labOne' is displayed in filter results on Labs page

  @ui @uiAndApi @AssayManagement @FindLab
  Scenario: DIAFE:0062 Check Assay Management back button
    When I choose a Country 'United States' and press Search icon on Assay Management page
      Then Labs page is opened
    When I click Next button 4 times on Labs page
    And I click random lab on Labs page
      Then Lab Profile page is opened
    When I click back button on Lab Profile page
      Then Labs page is opened
      And Labs page is opened on page 5
    When I choose a Country 'Albania' and press Search icon on Labs page
      Then Labs page is opened on page 1

  @ui @uiAndApi @AssayManagement @FindLab
  Scenario: DIAFE:0063 Validation for flexible search
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Testing-Lab |
      | countryCode | PER         |
      | type        | ACADEMIC    |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I put name from 'lab' without the hyphen on search field 'Enter keywords' and press Search on Assay Management page
      Then Labs page is opened
      And Lab 'lab' is displayed in filter results on Labs page

  @ui @AssayManagement @FindLab
  Scenario: DIAFE:0068 Full text search and partial search
    When I put a 'Philadelphia' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And I get result list and each entry contains search filter 'Philadelphia' on Labs page
    When I put a 'yo' on search field 'Enter keywords' and press Search icon on Labs page
      Then I get result list and each entry contains search filter 'yo' on Labs page
    When I put a 'spec' on search field 'Enter keywords' and press Search icon on Labs page
      Then I get result list and each entry contains search filter 'spec' on Labs page
    When I put a 'cs' on search field 'Enter keywords' and press Search icon on Labs page
      Then I get result list and each entry contains search filter 'cs' on Labs page
    When I put a 'phi' on search field 'Enter keywords' and press Search icon on Labs page
      Then I get result list and each entry contains search filter 'phi' on Labs page
    When I put a 'les' on search field 'Enter keywords' and press Search icon on Labs page
      Then I get result list and each entry contains search filter 'les' on Labs page
    When I put a 'Chicago' on search field 'Enter keywords' and press Search icon on Labs page
      Then I get result list and each entry contains search filter 'Chicago' on Labs page
    When I put a 'Miami' on search field 'Enter keywords' and press Search icon on Labs page
      Then I get result list and each entry contains search filter 'Miami' on Labs page
