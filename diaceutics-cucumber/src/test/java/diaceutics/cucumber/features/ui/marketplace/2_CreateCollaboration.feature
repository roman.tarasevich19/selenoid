Feature: Create collaboration

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I click Start a collaboration on Home page
      Then Description collaboration page is opened

  @ui @Marketplace @CreateCollaboration
  Scenario: DIAM:0010 Possibility to successfully create a Collaboration
    When I fill following fields on Description of the collaboration page and save as 'collaboration':
      | Title                  | Test title   |
      | Description            | Description  |
      | Additional information | Requirements |
      | Select a category      | random       |
    When I click 'Next step' on Description collaboration page
      Then Location of the collaboration page is opened
    When I fill following fields on Location of the collaboration page and save as 'collaboration':
      | Country                         | Belarus      |
      | City                            | Minsk        |
      | Zip                             | 1200         |
      | Number                          | 78           |
      | Route                           | Test route 1 |
      | Additional location information | Information  |
    And I click 'Submit for publishing' on Location of the collaboration page
      Then Edit collaboration page is opened
      And Collaboration 'collaboration' with following fields is displayed on Presentation form on Edit collaboration page
        | Title                  |
        | Description            |
        | Additional information |
    When I click 'Category' link on Edit collaboration page
      Then Categories form on Edit collaboration page is opened
      And Collaboration 'collaboration' with 'Category' field is displayed on Category form on Edit collaboration page
    When I click group 'Location' on Edit collaboration page
    And I click 'Location' link on Edit collaboration page
      Then My location form on Edit collaboration page is opened
      And Collaboration 'collaboration' with following fields is displayed on My location form on Edit collaboration page:
        | Country                         |
        | City                            |
        | Zip                             |
        | Number                          |
        | Route                           |
        | Additional location information |

  @ui @Marketplace @CreateCollaboration @SingleThread
  Scenario Outline: <test count> Possibility to successfully create a Collaboration with <file type> file
    When I fill following fields on Description of the collaboration page and save as 'collaboration':
      | Title                  | Test title        |
      | Description            | Test description  |
      | Additional information | Test requirements |
      | Select a category      | random            |
    And I upload file '<file name>' on Description collaboration page
      Then File '<file name>' is uploaded on Description collaboration page
    When I click 'Next step' on Description collaboration page
      Then Location of the collaboration page is opened
    When I fill following fields on Location of the collaboration page and save as 'collaboration':
      | Country                           | Belarus          |
      | City                              | Minsk            |
      | Zip                               | 1100             |
      | Number                            | 76               |
      | Route                             | Test route       |
      | Additional location information   | Test information |
    And I click 'Submit for publishing' on Location of the collaboration page
      Then Edit collaboration page is opened
      And Collaboration 'collaboration' with following fields is displayed on Presentation form on Edit collaboration page
        | Title                  |
        | Description            |
        | Additional information |
    When I click 'Category' link on Edit collaboration page
      Then Categories form on Edit collaboration page is opened
      And Collaboration 'collaboration' with 'Category' field is displayed on Category form on Edit collaboration page
    When I click group 'Location' on Edit collaboration page
    And I click 'Location' link on Edit collaboration page
      Then My location form on Edit collaboration page is opened
      And Collaboration 'collaboration' with following fields is displayed on My location form on Edit collaboration page:
        | Country                         |
        | City                            |
        | Zip                             |
        | Number                          |
        | Route                           |
        | Additional location information |

    Examples:
      | file name     | test count | file type |
      | TestDOCX.docx | DIAM:0011  | docx      |
      | TestGIF.gif   | DIAM:0012  | gif       |
      | TestJPG.jpg   | DIAM:0013  | jpg       |
      | TestPDF.pdf   | DIAM:0014  | pdf       |
      | TestPNG.png   | DIAM:0015  | png       |
      | TestPPTX.pptx | DIAM:0016  | pptx      |
      | TestRTF.rtf   | DIAM:0017  | rtf       |
      | TestXLS.xls   | DIAM:0018  | xls       |

  @ui @Marketplace @CreateCollaboration @SingleThread
  Scenario: DIAM:0019 Possibility to successfully create a Collaboration with video
    When I fill following fields on Description of the collaboration page and save as 'collaboration':
      | Title                  | Test title        |
      | Description            | Test description  |
      | Additional information | Test requirements |
      | Select a category      | random            |
    When I click by Videos on Description collaboration page
    And I fill following fields on Description of the collaboration page and save as 'collaboration':
      | Enter a Youtube URL | https://www.youtube.com/watch?v=AeTZuX9ZOj0&feature=emb_logo |
    And I click Add video on Description collaboration page
    And I click 'Next step' on Description collaboration page
      Then Location of the collaboration page is opened
    When I fill following fields on Location of the collaboration page and save as 'collaboration':
      | COUNTRY                         | Belarus          |
      | CITY                            | Minsk            |
      | ZIP                             | 1100             |
      | NUMBER                          | 76               |
      | ROUTE                           | Test route       |
      | Additional location information | Test information |
    And I click 'Submit for publishing' on Location of the collaboration page
      Then Edit collaboration page is opened
    And Collaboration 'collaboration' with following fields is displayed on Presentation form on Edit collaboration page
      | Title                  |
      | Description            |
      | Additional information |
    When I click 'Category' link on Edit collaboration page
      Then Categories form on Edit collaboration page is opened
      And Collaboration 'collaboration' with 'Category' field is displayed on Category form on Edit collaboration page
    When I click group 'Location' on Edit collaboration page
    And I click 'Location' link on Edit collaboration page
      Then My location form on Edit collaboration page is opened
      And Collaboration 'collaboration' with following fields is displayed on My location form on Edit collaboration page:
        | Country                         |
        | City                            |
        | Zip                             |
        | Number                          |
        | Route                           |
        | Additional location information |

  @ui @Marketplace @Collaboration
  Scenario: DIAM:0020 Required fields validation
    When I click 'Next step' on Description collaboration page
      Then Error container is displayed for 'Description' field on Description collaboration page
      And Form with error message 'Error creating your listing' is displayed on Description collaboration page
      And Form with error message '> This value should not be blank.' is displayed on Description collaboration page
      And Error container is displayed for 'Title' field on Description collaboration page
    When I fill following fields on Description of the collaboration page and save as 'collaboration':
      | Title | Test title |
    And I click 'Next step' on Description collaboration page
      Then Form with error message 'Error creating your listing' is displayed on Description collaboration page
      And Form with error message '> This value should not be blank.' is displayed on Description collaboration page
