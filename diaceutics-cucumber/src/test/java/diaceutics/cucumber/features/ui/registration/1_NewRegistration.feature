Feature: New user registration

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0055 Registration Organization type
    Given New registration page is opened
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |
    And I click 'Next' on new Registration page
    And I fill following fields for second step on Personal Details form on new Registration page and save as 'user':
      | Organization type | Organization             |
      | Your job title    | random string, length 10 |
      | Organization      | random string, length 10 |
    And I click 'Finish' on new Registration page
      Then Result message 'Thank you' is present on new Registration page

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0056 Organization type success page links
    Given New registration page is opened
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |
    And I click 'Next' on new Registration page
    And I fill following fields for second step on Personal Details form on new Registration page and save as 'user':
      | Organization type | Organization             |
      | Your job title    | random string, length 10 |
      | Organization      | random string, length 10 |
    And I click 'Finish' on new Registration page
    Then Result message 'Thank you' is present on new Registration page
    When I click 'Contact Support' link on new Registration page
    And I select last tab
      Then Link should contains 'Help URL'
    When I open first tab
    And I click 'Go to homepage' link on new Registration page
      Then Marketplace Main page is opened

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0057 Registration Other type
    Given New registration page is opened
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |
    And I click 'Next' on new Registration page
    And I fill following fields for second step on Personal Details form on new Registration page and save as 'user':
      | Organization type | Other                    |
      | Your position     | random string, length 10 |
    And I click 'Finish' on new Registration page
      Then Result message 'Thank you' is present on new Registration page
    When I click 'Go to homepage' link on new Registration page
      Then Marketplace Main page is opened

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0058 Other type success page links
    Given New registration page is opened
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |
    And I click 'Next' on new Registration page
    And I fill following fields for second step on Personal Details form on new Registration page and save as 'user':
      | Organization type | Other                    |
      | Your position     | random string, length 10 |
    And I click 'Finish' on new Registration page
      Then Result message 'Thank you' is present on new Registration page
    When I click 'Contact Support' link on new Registration page
    And I select last tab
      Then Link should contains 'Help URL'
    When I open first tab
    And I click 'Go to homepage' link on new Registration page
      Then Marketplace Main page is opened

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0059 Register duplicate user failed and restart sign up
    Given New registration page is opened
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |
    And I click 'Next' on new Registration page
    And I fill following fields for second step on Personal Details form on new Registration page and save as 'user':
      | Organization type | Other                    |
      | Your position     | random string, length 10 |
    And I click 'Finish' on new Registration page
      Then Result message 'Thank you' is present on new Registration page
    When I opened New registration page
      Then New registration page is opened
    When I fill following fields to create new user on Personal Details form on new Registration page using data from 'user'
      | First name                           |
      | Last name                            |
      | Work email                           |
      | I read and accept the Privacy Policy |
    And I click 'Next' on new Registration page
    Then I fill following fields to create new user on Personal Details form on new Registration page using data from 'user'
      | Organization type |
      | Your position     |
    And I click 'Finish' on new Registration page
      Then Result message 'Sign up failed' is present on new Registration page
    When I click 'Contact Support' link on new Registration page
    And I select last tab
      Then Link should contains 'Help URL'
    When I open first tab
    And I click 'restart sign up' link on new Registration page
      Then New registration page is opened

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0060 Registration with empty fields
    Given New registration page is opened
    When I click 'Next' on new Registration page
      Then Error container is displayed for 'First name' field on new Registration page
      And Form with error message 'Please enter your first name' is displayed on new Registration page
      And Error container is displayed for 'Last name' field on new Registration page
      And Form with error message 'Please enter your last name' is displayed on new Registration page
      And Error container is displayed for 'Work email' field on new Registration page
      And Form with error message 'Please enter a valid email address' is displayed on new Registration page
      And Form with error message 'Please read and accept our privacy policy' is displayed on new Registration page

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0061 Links on Registration page
    Given New registration page is opened
    And I click 'Privacy Policy' link on new Registration page
    And I select last tab
      Then Link should be 'Privacy Policy URL'
    When I open first tab
    And I click 'Login' link on new Registration page
      Then Login page is opened

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0062 Registration page back to first page
    Given New registration page is opened
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |
    And I click 'Next' on new Registration page
    And I fill following fields for second step on Personal Details form on new Registration page and save as 'user':
      | Organization type | Other                    |
      | Your position     | random string, length 10 |
    And I click 'Previous Step' link on new Registration page
      Then I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |

  @Marketplace @NewUserRegistration
  Scenario: DIAM:0063 Registration page invalid values
    Given New registration page is opened
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name | random string, length 41 |
      | Last name  | random string, length 81 |
    And I click 'Next' on new Registration page
      Then Form with error message 'maximum length is 40' is displayed on new Registration page
      And Form with error message 'maximum length is 80' is displayed on new Registration page
    When I fill following fields for first step on Personal Details form on new Registration page and save as 'user':
      | First name                           | random string, length 10 |
      | Last name                            | random string, length 10 |
      | Work email                           | @mailosaur.io            |
      | I read and accept the Privacy Policy | true                     |
    And I click 'Next' on new Registration page
    And I fill following fields for second step on Personal Details form on new Registration page and save as 'user':
      | Organization type | Organization              |
      | Your job title    | random string, length 130 |
      | Organization      | random string, length 260 |
    And I click 'Finish' on new Registration page
      Then Form with error message 'maximum length is 128' is displayed on new Registration page
      And Form with error message 'maximum length is 255' is displayed on new Registration page