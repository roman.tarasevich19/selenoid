Feature: My collaborations listing page

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in

  @ui @Marketplace @MyCollaborationsListingPage
  Scenario: DIAM:0023 Possibility to duplicate collaboration
    When I click Start a collaboration on Home page
      Then Description collaboration page is opened
    When I fill following fields on Description of the collaboration page and save as 'collaboration':
      | Title                  | Test title   |
      | Description            | Description  |
      | Additional information | Requirements |
      | Select a category      | random       |
    When I click 'Next step' on Description collaboration page
      Then Location of the collaboration page is opened
    When I fill following fields on Location of the collaboration page and save as 'collaboration':
      | Country                         | Belarus     |
      | City                            | Minsk       |
      | Zip                             | 1200        |
      | Number                          | 78          |
      | Route                           | Test route  |
      | Additional location information | Information |
    And I click 'Submit for publishing' on Location of the collaboration page
      Then Edit collaboration page is opened
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Duplicate' Collaboration 'collaboration' on My Collaborations page and save copy as a 'copy collaboration'
      Then Edit collaboration page is opened
      And Inform Form with message 'Listing duplicated successfully. You can now edit it and publish it.' is displayed on Edit collaboration page
      And Collaboration 'copy collaboration' with following fields is displayed on Presentation form on Edit collaboration page
        | Title                  |
        | Description            |
        | Additional information |
      And Status 'In review' is displayed on Edit collaboration page
    When I click 'Category' link on Edit collaboration page
      Then Categories form on Edit collaboration page is opened
      And Collaboration 'copy collaboration' with 'Category' field is displayed on Category form on Edit collaboration page
    When I click group 'Location' on Edit collaboration page
    And I click 'Location' link on Edit collaboration page
      Then My location form on Edit collaboration page is opened
      And Collaboration 'copy collaboration' with following fields is displayed on My location form on Edit collaboration page:
        | Country                         |
        | City                            |
        | Zip                             |
        | Number                          |
        | Route                           |
        | Additional location information |
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
      And Number of 2 collaborations as a 'copy collaboration' are displayed on My Collaborations page

  @ui @Marketplace @Collaboration @MyCollaborationsListingPage
  Scenario: DIAM:0024 Possibility to duplicate published collaboration
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I set 'Published' status and click Apply button on My Collaborations page
      Then My Collaborations page is opened
      And Collaboration 'admin collaboration' is displayed on My Collaborations page
    When I click 'Duplicate' Collaboration 'admin collaboration' on My Collaborations page and save copy as a 'copy collaboration'
      Then Edit collaboration page is opened
      And Inform Form with message 'Listing duplicated successfully. You can now edit it and publish it.' is displayed on Edit collaboration page
      And Status 'In review' is displayed on Edit collaboration page
      And Collaboration 'copy collaboration' with following fields is displayed on Presentation form on Edit collaboration page
        | Title                  |
        | Description            |
        | Additional information |
    When I click 'Category' link on Edit collaboration page
      Then Categories form on Edit collaboration page is opened
    And Collaboration 'copy collaboration' with 'Category' field is displayed on Category form on Edit collaboration page
    When I click group 'Location' on Edit collaboration page
    And I click 'Location' link on Edit collaboration page
      Then My location form on Edit collaboration page is opened
      And Collaboration 'copy collaboration' with following fields is displayed on My location form on Edit collaboration page:
        | Country                         |
        | City                            |
        | Zip                             |
        | Number                          |
        | Route                           |
        | Additional location information |
    When I click group 'Presentation' on Edit collaboration page
    And I click 'Presentation' link on Edit collaboration page
      Then Presentation form on Edit collaboration page is opened
    When I fill following fields on Presentation form on Edit collaboration page and save as 'copy collaboration':
      | Title | Copy collaboration |
    And I click Save changes button on Edit collaboration page
      Then Inform Form with message 'Update successful' is displayed on Edit collaboration page
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I click 'Edit' Collaboration 'copy collaboration' on My Collaborations page
      Then Edit collaboration page is opened
      And Status 'In review' is displayed on Edit collaboration page
    When I click back on browser
      Then My Collaborations page is opened
    When I click 'Edit' Collaboration 'admin collaboration' on My Collaborations page
      Then Edit collaboration page is opened
      And Status 'Published' is displayed on Edit collaboration page

  @ui @Marketplace @Collaboration @MyCollaborationsListingPage
  Scenario: DIAM:0025 Possibility to sort collaboration by status
    When I click 'My collaborations' on Master Header form
      Then My Collaborations page is opened
    When I set 'Published' status and click Apply button on My Collaborations page
      Then My Collaborations page is opened
    When I select random collaboration from My Collaborations page and save as 'random collaboration'
    And I click 'Edit' Collaboration 'random collaboration' on My Collaborations page
      Then Edit collaboration page is opened
      And Status 'Published' is displayed on Edit collaboration page
    When I click back on browser
      Then My Collaborations page is opened
    When I set 'In review' status and click Apply button on My Collaborations page
      Then My Collaborations page is opened
    When I select random collaboration from My Collaborations page and save as 'random collaboration'
    And I click 'Edit' Collaboration 'random collaboration' on My Collaborations page
      Then Edit collaboration page is opened
      And Status 'In review' is displayed on Edit collaboration page
    When I click back on browser
      Then My Collaborations page is opened
    When I set 'Invalidated' status and click Apply button on My Collaborations page
      Then My Collaborations page is opened
    When I select random collaboration from My Collaborations page and save as 'random collaboration'
    And I click 'Edit' Collaboration 'random collaboration' on My Collaborations page
      Then Edit collaboration page is opened
      And Status 'Invalidated' is displayed on Edit collaboration page
    When I click back on browser
      Then My Collaborations page is opened
    When I set 'Hidden' status and click Apply button on My Collaborations page
      Then My Collaborations page is opened
    When I select random collaboration from My Collaborations page and save as 'random collaboration'
    And I click 'Edit' Collaboration 'random collaboration' on My Collaborations page
      Then Edit collaboration page is opened
      And Status 'Hidden' is displayed on Edit collaboration page
