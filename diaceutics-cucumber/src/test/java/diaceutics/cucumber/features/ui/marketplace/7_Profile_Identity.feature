Feature: Profile Identity

  @ui @Marketplace @ProfileIdentity @SingleThread
   Scenario: DIAM:0046 Update user data with valid and invalid data
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'changePasswordUser' user
    And User should be logged in
    When I click 'My profile' on user menu on Master header
      Then MyProfile page is opened
    When Set up empty value for the following fields on User Edit form:
      | Email      |
      | First name |
      | Last name  |
    And Save changes on Edit Identify Form on MyProfilePage
    Then Error container is displayed for following fields on User Identity Form on MyProfile page:
      | Email      |
      | First name |
      | Last name  |
    And Form with error message 'Please enter an email' is displayed on User Identity Form on MyProfile page
    And Next error
    And Form with error message 'Please enter your first name' is displayed on User Identity Form on MyProfile page
    And Next error
    And Form with error message 'Please enter your last name' is displayed on User Identity Form on MyProfile page
    When Set up 'user.edit.data' user for the following fields:
      | Email      |
      | First name |
      | Last name  |
    And Save changes on Edit Identify Form on MyProfilePage
      Then User information was updated successfully
    And Data for user 'user.edit.data' is displayed on the following fields on User Edit Identity Form on MyProfile page:
      | Email      |
      | First name |
      | Last name  |
    When Set up 'changePasswordUser' user for the following fields:
      | Email      |
      | First name |
      | Last name  |
    And Save changes on Edit Identify Form on MyProfilePage
    Then User information was updated successfully
    And Data for user 'changePasswordUser' is displayed on the following fields on User Edit Identity Form on MyProfile page:
      | First name                     |
      | Last name                      |
      | Email                          |