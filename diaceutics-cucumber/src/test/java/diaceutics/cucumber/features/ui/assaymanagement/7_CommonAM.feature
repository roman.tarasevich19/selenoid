Feature: Assay Management common

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened

  @ui @AssayManagement @AssayManagementCommon
  Scenario: DIAFE:0056 Check links on AM footer
    When I click on 'Terms' link
      Then Terms page is opened
      And Link should be 'Terms URL'
    When I click back on browser
      Then Assay Management page is opened
    When I click on 'Privacy Statement' link
      Then Privacy Statement page is opened
      And Link should be 'Privacy statement URL'
    When I click back on browser
      Then Assay Management page is opened
    When I click on 'Cookies Policy' link
      Then Cookies Policy page is opened
      And Link should be 'Cookie Policy URL'
    When I click back on browser
      Then Assay Management page is opened
    When I click on 'Cookie Consent Tool' link
      Then Cookie Pro form on Marketplace Main page is opened
    When Close form
      Then Assay Management page is opened
    When I click on 'Contact Us' link
      Then Contact Us page is opened
      And Link should be 'Contact Us URL'
    When I click back on browser
      Then Assay Management page is opened
      And Copyright label is correct

  @ui @AssayManagement @AssayManagementCommon
  Scenario: DIAFE:0057 Check links on Master header AM
    When I click 'Home' on Master Header form
      Then Browser opened link 'Home URL'
    When I click back on browser
      Then Assay Management page is opened
    When I click 'Projects' on Master Header form
      Then Link should contains 'Login URL'
    When I click back on browser
      Then Assay Management page is opened
    When I click 'My collaborations' on Master Header form
      Then Browser opened link 'My collaborations URL'
    When I click 'Marketplace' on Master Header form
      Then Browser opened link 'Marketplace URL'
    When I click 'My organization' on Master Header form
      Then Browser opened link 'My organization URL'
    When I click 'My profile' on user menu on Master Header form
      Then Browser opened link 'My profile URL'
    When I click 'Help' on user menu on Master Header form
      Then Link should be 'Help URL'
    When I click back on browser
    And I click 'Logout' on user menu on Master Header form
      Then Marketplace Main page is opened
    When I click DXRX logo on the top left on Master Header form
      Then Marketplace Main page is opened

  @ui @AssayManagement @AssayManagementCommon
  Scenario: DIAFE:0058 Check tools from dropdown menu on Master header AM
    When 'Data tools' link on Master Header form is highlighted in 'purple' color
    And I click 'Data tools' on Master Header form
      Then Data tools context menu is displayed on Master Header form
    When I click 'Lab Mapping' from dropdown menu on Master Header form
      Then Lab Mapping Main page is opened
      And Browser opened link 'Lab Mapping URL'
      And 'Lab Mapping' link on Data Tools Header is highlighted in 'purple' color
    When I click 'Data tools' on Master Header form
    And I click 'Testing Dashboard' from dropdown menu on Master Header form
      Then Testing Dashboard Main page is opened
      And Browser opened link 'Testing Dashboard URL'
      And 'Testing Dashboard' link on Master Header form is highlighted in 'purple' color
    When I click 'Data tools' on Master Header form
    And I click 'Assay Management' from dropdown menu on Master Header form
      Then Assay Management page is opened
      And Browser opened link 'Assay Management URL'
      And 'Assay Management' link on Master Header form is highlighted in 'purple' color
    When I click 'Data tools' on Master Header form
    And I click 'Physician Mapping' from dropdown menu on Master Header form
      Then Physician Mapping Main page is opened
      And Browser opened link 'Physician Mapping URL'
      And 'Physician Mapping' link on Data Tools Header is highlighted in 'purple' color

  @ui @AssayManagement @AssayManagementCommon
  Scenario: DIAFE:0059 Check tools on Master header AM
    When I click 'Lab Mapping' on Master Header form
      Then Lab Mapping Main page is opened
      And Browser opened link 'Lab Mapping URL'
      And 'Lab Mapping' link on Data Tools Header is highlighted in 'purple' color
    When I click 'Testing Dashboard' on Master Header form
      Then Testing Dashboard Main page is opened
      And Browser opened link 'Testing Dashboard URL'
      And 'Testing Dashboard' link on Master Header form is highlighted in 'purple' color
    When I click 'Assay Management' on Master Header form
      Then Assay Management page is opened
      And Browser opened link 'Assay Management URL'
      And 'Assay Management' link on Master Header form is highlighted in 'purple' color
    When I click 'Physician Mapping' on Master Header form
      Then Physician Mapping Main page is opened
      And Browser opened link 'Physician Mapping URL'
      And 'Physician Mapping' link on Data Tools Header is highlighted in 'purple' color

  @ui @AssayManagement @AssayManagementCommon
  Scenario: DIAFE:0060 Check tools on Master header AM
    When I click 'Lab Mapping' on Master Header form
    And Lab Mapping Main page is opened
    And Browser opened link 'Lab Mapping URL'
      Then 'Lab Mapping' link on Data Tools Header is highlighted in 'purple' color
    And I click 'Testing Dashboard' on Master Header form
    And Testing Dashboard Main page is opened
    And Browser opened link 'Testing Dashboard URL'
      Then 'Testing Dashboard' link on Master Header form is highlighted in 'purple' color
    When I click 'Assay Management' on Master Header form
      Then Assay Management page is opened
      And Browser opened link 'Assay Management URL'
      And 'Assay Management' link on Master Header form is highlighted in 'purple' color
    When I click 'Physician Mapping' on Master Header form
      Then Physician Mapping Main page is opened
      And Browser opened link 'Physician Mapping URL'
      And 'Physician Mapping' link on Data Tools Header is highlighted in 'purple' color

  @ui @AssayManagement @AssayManagementCommon
  Scenario: DIAFE:0061 Check chat form section on AM
    When I open chat form
    And I switch to Chat form frame
    And Chat form is opened
      Then Start another conversation section is present
      Then Continue the conversation section is present
      Then Find your answers section is present
    When I click See all your conversation
      Then Your conversations section is opened
    And User can see all his previous conversations
    And I return back on Chat Form
    When I click Send us a message to open new chat
    When I set up a new message 'Test PM'
    When I send a new message
      Then Message 'Test PM' sent
    When I open GIF menu and select first gif to sent
      Then GIF sent successfully
    When I open Emoji menu and select first emoji to sent
      Then Emoji sent successfully
    When I return back on Chat Form
    And I continue last conversation
      Then Emoji sent successfully
    When I click on Upload document and upload 'TestImage.png'
      Then Document 'TestImage.png' uploaded
    When I return back on Chat Form
    When I set up find your answers search with 'lab'
      Then Search for find your answer section is present
    When I click we run on Intercom link
    And I select last tab
      Then Link should contains 'Intercom URL'
