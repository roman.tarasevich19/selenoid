Feature: Edit Assay

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I put a Lab 'lab' on search field 'Enter keywords' and press Search icon on Assay Management page
      Then Labs page is opened
      And Lab 'lab' is displayed in filter results on Labs page
    When I select 'lab' lab on Labs page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0107 Not specified Edit assay TAT
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name | Test Assay TAT |
      | Detects    | random         |
      | Method     | random         |
      | Ontology   | random         |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name | Test Assay TAT Edit |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    And I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Biomarker 'biomarker' is displayed on Assay description page
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TAT Edit |
      | Turnaround time (TAT) | 8                   |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
    And I click 'Save and finish' on Edit Assay page
      Then Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Turnaround time (TAT) | h |
    And I click 'Proceed' on Edit Assay page
      Then Message 'Please enter a valid integer value' is displayed on required fields on Edit Assay page
    When I clear following text fields on Edit Assay page
      | Turnaround time (TAT) |
    And I click 'Proceed' on Edit Assay page
      Then Message 'Turnaround time is required' is displayed on required fields on Edit Assay page
      And Message 'Some items below need your attention.' is displayed on Edit Assay page

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0108 In House Edit assay TAT
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | In-house or send-out? | In-house       |
      | Method                | random         |
      | Ontology              | random         |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
      | Assay name           |
      | Detects              |
      | Method name          |
      | Ontology             |
      | In-house or send-out |
      And Biomarker 'biomarker' is displayed on Assay description page
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name | Test Assay TAT Edit |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
      And I click 'Save and finish' on Edit Assay page
      And Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Biomarker 'biomarker' is displayed on Assay description page
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TAT Edit |
      | Turnaround time (TAT) | 8                   |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
      And I click 'Save and finish' on Edit Assay page
      And Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Turnaround time (TAT) | h |
    And I click 'Proceed' on Edit Assay page
      Then Message 'Please enter a valid integer value' is displayed on required fields on Edit Assay page
    When I clear following text fields on Edit Assay page
      | Turnaround time (TAT) |
    And I click 'Proceed' on Edit Assay page
      Then Message 'Turnaround time is required' is displayed on required fields on Edit Assay page
      And Message 'Some items below need your attention.' is displayed on Edit Assay page

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0109 New assay with TAT
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | Method                | random         |
      | Ontology              | random         |
      | Turnaround time (TAT) | 5              |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
      | Assay name           |
      | Detects              |
      | Method name          |
      | Ontology             |
      | Turnaround time      |

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0110 New in house assay with TAT
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | In-house or send-out? | In-house       |
      | Method                | random         |
      | Ontology              | random         |
      | Turnaround time (TAT) | 5              |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
      | Assay name           |
      | Detects              |
      | Method name          |
      | Ontology             |
      | In-house or send-out |
      | Turnaround time      |
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0111 Edit not specified to in house assay
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name | Test Assay house |
      | Detects    | random           |
      | Method     | random           |
      | Ontology   | random           |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
      | Assay name           |
      | Detects              |
      | Method name          |
      | Ontology             |
      And Biomarker 'biomarker' is displayed on Assay description page
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TA |
      | In-house or send-out? | In-house      |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
      And I click 'Save and finish' on Edit Assay page
      And Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0112 Edit in house to not specified assay
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | In-house or send-out? | In-house       |
      | Method                | random         |
      | Ontology              | random         |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
      | Assay name           |
      | Detects              |
      | Method name          |
      | Ontology             |
      And Biomarker 'biomarker' is displayed on Assay description page
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TA |
      | In-house or send-out? | Not specified |
    And I click 'Proceed' on Edit Assay page
      Then Lab Test Biomarker form is opened on Edit Assay page
      And I click 'Save and finish' on Edit Assay page
      And Lab Profile page is opened
      And Assay 'assay' is displayed in Assays grid on Lab Profile page

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0113 Not specified assay with LDT
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name     | Test Assay TAT           |
      | Detects        | random                   |
      | Method         | random                   |
      | Ontology       | random                   |
      | Classification | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
      And Assay 'assay' with following fields is displayed on Assay description page:
      | Assay name           |
      | Detects              |
      | Method name          |
      | Ontology             |
      And Biomarker 'biomarker' is displayed on Assay description page

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0114 in house assay with commercial assay
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay TAT   |
      | Detects               | random           |
      | In-house or send-out? | In-house         |
      | Method                | random           |
      | Ontology              | random           |
      | Classification        | Commercial assay |
      | FDA 510K APPROVED KIT | false            |
      | FDA PMA APPROVED KIT  | false            |
      | IVD-CE                | false            |
      | RUO/IUO               | false            |
    And I click 'Proceed' on Add an Assay page
      Then Message 'At least one commercial assay regulatory status is required' is displayed on required fields on Edit Assay page
      And Message 'Some items below need your attention.' is displayed on Edit Assay page

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0116 Edit not specified LDT Assay
    When I click on 'Add assay' on Lab Profile Page
    Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name     | Test Assay TAT           |
      | Detects        | random                   |
      | Method         | random                   |
      | Ontology       | random                   |
      | Classification | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | Method                | random         |
      | Ontology              | random         |
      | In-house or send-out? | In-house       |
      Then Field 'FDA 510K APPROVED KIT' should be disabled on Add an Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Add an Assay page
      And Field 'IVD-CE' should be disabled on Add an Assay page
      And Field 'RUO/IUO' should be disabled on Add an Assay page
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | Method                | random         |
      | Ontology              | random         |
      | In-house or send-out? | Send-out       |
      Then Field 'FDA 510K APPROVED KIT' should be disabled on Add an Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Add an Assay page
      And Field 'IVD-CE' should be disabled on Add an Assay page
      And Field 'RUO/IUO' should be disabled on Add an Assay page
    When I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened

  @ui @uiAndApi @AssayManagement @Assay @SingleThread
  Scenario: DIAFE:0117 Edit In house LDT Assay
    When I click on 'Add assay' on Lab Profile Page
      Then Add an Assay page is opened
    When I fill following fields on Add an Assay page and save as 'assay':
      | Assay name            | Test Assay TAT           |
      | Detects               | random                   |
      | Method                | random                   |
      | Ontology              | random                   |
      | In-house or send-out? | In-house                 |
      | Classification        | Lab developed test (LDT) |
    And I click 'Proceed' on Add an Assay page
      Then Lab Test Biomarker form is opened on Add an Assay page
    When I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
    And I click 'Save and finish' on Add an Assay page
      Then Lab Profile page is opened
      And 'New lab assay added.' message is displayed on Lab Profile page
      And Assay 'assay' is displayed in Assays grid on Lab Profile page
    When I click on Assay 'assay' in Assays grid on Lab Profile Page
      Then Assay description page is opened
    When I click Edit Details on Assay description page
      Then Edit Assay page is opened
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | Method                | random         |
      | Ontology              | random         |
      | In-house or send-out? | Not specified  |
      Then Field 'FDA 510K APPROVED KIT' should be disabled on Add an Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Add an Assay page
      And Field 'IVD-CE' should be disabled on Add an Assay page
      And Field 'RUO/IUO' should be disabled on Add an Assay page
    When I fill following fields on Edit Assay page and save as 'assay':
      | Assay name            | Test Assay TAT |
      | Detects               | random         |
      | Method                | random         |
      | Ontology              | random         |
      | In-house or send-out? | Send-out       |
      Then Field 'FDA 510K APPROVED KIT' should be disabled on Add an Assay page
      And Field 'FDA PMA APPROVED KIT' should be disabled on Add an Assay page
      And Field 'IVD-CE' should be disabled on Add an Assay page
      And Field 'RUO/IUO' should be disabled on Add an Assay page
      And I click 'Proceed' on Add an Assay page
    When Lab Test Biomarker form is opened on Add an Assay page
      Then I fill following fields in Lab Test Biomarker form on Add an Assay page and save as 'biomarker':
      | Biomarker | random |
      | Variants  | random |
      And I click 'Save and finish' on Add an Assay page
      And Lab Profile page is opened