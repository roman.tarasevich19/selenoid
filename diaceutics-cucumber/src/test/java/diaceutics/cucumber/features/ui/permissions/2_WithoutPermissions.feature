Feature: Without permissions

  @ui @UiPermissions @WithoutPermissions
  Scenario: DIAPFE:0024 Validation administrator message for user without permissions
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'user without permissions' user
      Then Following message is displayed on Home page:
        | I am sorry you currently do not have permission to access this page. |
      And Following message is displayed on Home page:
        | For help with this matter and to gain access to the page, please contact our support team via the support chat, or email us at help@dxrx.io |
