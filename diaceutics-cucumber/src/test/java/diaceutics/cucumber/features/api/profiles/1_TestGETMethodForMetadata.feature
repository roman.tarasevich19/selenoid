Feature: Test GET method for Metadata

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0001 Returns list of all Biomarkers for the selected Commercial Assay first request
    When I execute request Returns list of all Biomarkers for the selected Commercial Assay with Id 1:
      Then the status code is 200
      And response includes the biomarkers from file 'biomarkersForCommercialAssayWithID1.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0002 Returns pageable list of all Biomarkers
    When I execute request Returns pageable list of all biomarkers with parameters:
      | size | 2902 |
      | sort | true |
      Then the status code is 200
      And response includes the collection biomarkers from file 'biomarkers.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0003 Returns pageable list of all classifications
    When I execute request Returns pageable list of all classifications with parameters:
      | size | 5    |
      | sort | true |
      Then the status code is 200
      And response includes the classifications from file 'classifications.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0004 Returns list of all Biomarkers for the selected Commercial Assay second request
    When I execute second request Returns list of all Biomarkers for the selected Commercial Assay with Id 1:
      Then the status code is 200
      And response includes the biomarkers from file 'biomarkersForCommercialAssayWithID1.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0005 Returns pageable list of all countries with profile counts for Laboratories
    When I execute request Returns pageable list of all countries with profile counts for Laboratories with parameters:
      | size | 56   |
      | sort | true |
      Then the status code is 200
      And response includes the countries matches the profile type 'LAB'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0006 Returns pageable list of all DetectGermlineSomatics
    When I execute request Returns pageable list of all DetectGermlineSomatic with parameters:
      | size | 2    |
      | sort | true |
      Then the status code is 200
      And response includes the detects from file 'detects.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0007 Returns pageable list of all diseases for a Lab Test
    When I execute request Returns pageable list of all diseases for a Lab Test with parameters:
      | size | 104  |
      | sort | true |
      Then the status code is 200
      And response includes the diseases from file 'diseases.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0008 Returns pageable list of all Methods
    When I execute request Returns pageable list of all Methods with parameters:
      | size | 32   |
      | sort | true |
      Then the status code is 200
      And response includes the methods from file 'methods.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0009 Returns pageable list of all Ontologies
    When I execute request Returns pageable list of all Ontologies with parameters:
      | size | 9    |
      | sort | true |
      Then the status code is 200
      And response includes the ontologies from file 'ontologies.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0010 Returns pageable list of all platform manufacturers and equipment that can be associated with labs
    When I execute request Returns pageable list of all platform manufacturers and equipment with parameters:
      | size | 48 |
      Then the status code is 200
      And response includes the all platform manufacturers and equipment from file 'manufacturers.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0011 Returns pageable list of all Result Formats
    When I execute request Returns pageable list of all Result Formats with parameters:
      | size | 5    |
      | sort | true |
      Then the status code is 200
      And response includes the all Result Formats from file 'resultFormats.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0012 Returns pageable list of all Scoring Methodologies
    When I execute request Returns pageable list of all Scoring Methodologies with parameters:
      | size | 12   |
      | sort | true |
      Then the status code is 200
      And response includes the all Scoring Methodologies from file 'scoringMethodologies.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0013 Returns pageable list of all Specimen Requirements
    When I execute request Returns pageable list of all Specimen Requirements with parameters:
      | size | 17   |
      | sort | true |
      Then the status code is 200
      And response includes the all Specimen Requirements from file 'specimenRequirements.json'

  @api @ProfilesApi @MetadataApi @SingleThread
  Scenario: DIABE:0014 Returns pageable list of all Testing Purposes
    When I execute request Returns pageable list of all Testing Purposes with parameters:
      | size | 3    |
      | sort | true |
      Then the status code is 200
      And response includes the all Testing Purposes from file 'testingPurposes.json'
