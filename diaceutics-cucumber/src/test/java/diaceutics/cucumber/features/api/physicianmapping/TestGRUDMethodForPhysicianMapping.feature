Feature: Test GRUD method for Physician Mapping

  @api @ProfilesApi @PhysicianMappingApi @SingleThread
  Scenario: DIABE:0065 GET Returns a collection of subscriptions for the authenticated principle
    When I execute GET request Returns a collection of subscriptions for the authenticated principle
      Then the status code is 200
      And response includes the projects from file 'subscriptionDtoPhysicianMapping.json'

  @api @ProfilesApi @PhysicianMappingApi @SingleThread
  Scenario: DIABE:0021 POST Generates a physician mapping
    When I execute POST request Generates a physician mapping using following data:
      | projectId   | 1    |
      | biomarkerId | 1    |
      | diseaseId   | 1    |
      | fromYear    | 2020 |
      | fromMonth   | 1    |
      | toYear      | 2020 |
      | toMonth     | 1    |
      Then the status code is 200
      And response includes the list of Physician
