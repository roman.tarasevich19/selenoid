Feature: Test GRUD method for Lab Mapping

  @api @ProfilesApi @LabMappingApi @SingleThread
  Scenario: DIABE:0043 GET Returns a collection of subscriptions for the authenticated principle
    When I execute request Returns a collection of subscriptions for the authenticated principle
      Then the status code is 200
      And response includes list of all the projects from file 'subscriptionDtoLabMapping.json'

  @api @ProfilesApi @LabMappingApi @SingleThread
  Scenario: DIABE:0042 POST Generates a lab mapping
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute POST request Generates a lab mapping using following data:
      | projectId    | 1         |
      | countryCode  | ALB       |
      | criteriaType | BIOMARKER |
      | criteriaId   | 1         |
      | diseaseIds   | 10        |
      | fromYear     | 2017      |
      | fromMonth    | 2         |
      | toYear       | 2020      |
      | toMonth      | 2         |
      Then the status code is 200
      And response includes the Lab 'lab' with Assay 'assay'

  @api @ProfilesApi @LabMappingApi @SingleThread
  Scenario: DIABE:0067 POST Generates a lab mapping (testing dashboard)
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Lab |
      | countryCode | ALB      |
      | type        | ACADEMIC |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute POST request Generates a testing dashboard mapping using following data:
      | projectId    | 1         |
      | countryCode  | ALB       |
      | criteriaType | BIOMARKER |
      | criteriaId   | 1         |
      | diseaseIds   | 10        |
      | fromYear     | 2017      |
      | fromMonth    | 2         |
      | toYear       | 2020      |
      | toMonth      | 2         |
      Then the status code is 200
      And response includes the Lab 'lab' with Assay 'assay'

  @api @ProfilesApi @LabMappingApi @SingleThread
  Scenario: DIABE:0095 POST Generates a lab mapping using a specified method
    When I execute POST request Generates a lab mapping using following data:
      | projectId    | 1      |
      | countryCode  | USA    |
      | criteriaType | METHOD |
      | criteriaId   | 18     |
      | diseaseIds   | 10     |
      | fromYear     | 2020   |
      | fromMonth    | 1      |
      | toYear       | 2021   |
      | toMonth      | 1      |
      Then the status code is 200
      And response includes the Labs

  @api @ProfilesApi @LabMappingApi @SingleThread
  Scenario: DIABE:0096 POST Generates a lab mapping using a specified method (testing dashboard)
    When I execute POST request Generates a testing dashboard mapping using following data:
      | projectId    | 1      |
      | countryCode  | USA    |
      | criteriaType | METHOD |
      | criteriaId   | 18     |
      | diseaseIds   | 10     |
      | fromYear     | 2020   |
      | fromMonth    | 1      |
      | toYear       | 2021   |
      | toMonth      | 1      |
      Then the status code is 200
      And response includes the Labs
