Feature: Test POST method for Physician Mapping Controller

  @api @ProfilesApi @PhysicianMappingControllerApi @SingleThread
  Scenario: DIABE:0037 POST Physician Mapping Controller
    Given I execute the POST request Creates a new lab 'labOne' using following data:
      | name        | Test Api Lab One |
      | countryCode | IRL              |
      | type        | ACADEMIC         |
      Then the status code is 200
      And I save Lab as 'labOne' from response
    When I execute the POST request Creates a new lab 'labTwo' using following data:
      | name        | Test Api Lab Two |
      | countryCode | IRL              |
      | type        | HOSPITAL         |
      Then the status code is 200
      And I save Lab as 'labTwo' from response
    When I execute the POST request physician-mapping-controller using id from following lab:
      | labOne |
      | labTwo |
      Then the status code is 200
      And response includes the following labs:
      | labOne |
      | labTwo |
