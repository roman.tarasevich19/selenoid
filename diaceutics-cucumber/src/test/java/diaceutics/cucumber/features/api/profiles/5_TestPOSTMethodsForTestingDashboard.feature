Feature: Test POST method for Testing Dashboard

  @api @ProfilesApi @TestingDashboardApi @SingleThread
  Scenario: DIABE:0038 Returns data for lab testing charts
    When I execute POST request Returns data for lab testing charts using following data:
      | countryCode  | ALB  |
      | biomarkerIds | 1    |
      | diseaseIds   | 1    |
      | fromYear     | 2017 |
      | fromMonth    | 2    |
      | toYear       | 2020 |
      | toMonth      | 2    |
      Then the status code is 200
      And response includes the list of TestingDashboardVolumeDto objects

  @api @ProfilesApi @TestingDashboardApi @SingleThread
  Scenario: DIABE:0039 Returns data for versus charts
    When I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | BLR          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
    When I execute POST request Returns data for versus charts using following data:
      | countryCode  | BLR   |
      | biomarkerIds | 1     |
      | diseaseIds   | 10    |
      | fromYear     | 2017  |
      | fromMonth    | 2     |
      | toYear       | 2020  |
      | toMonth      | 2     |
      Then the status code is 200
      And response includes data with the Lab 'lab'
      And response includes the list of TestingDashboardVolumeDto objects with biomarkerID 1

  @api @ProfilesApi @TestingDashboardApi @SingleThread
  Scenario: DIABE:0040 Returns data for claims charts
    When I execute POST request Returns data for claims charts using following data and save 'filter':
      | countryCode  | USA  |
      | biomarkerIds | 1    |
      | diseaseIds   | 1    |
      | fromYear     | 2017 |
      | fromMonth    | 2    |
      | toYear       | 2020 |
      | toMonth      | 2    |
      Then the status code is 200
      And And response includes data matches the 'filter'

  @api @ProfilesApi @TestingDashboardApi @SingleThread
  Scenario: DIABE:0041 Returns data for platform charts
    When I execute POST request Returns data for platform charts using following data:
      | countryCode  | USA  |
      | biomarkerIds | 1    |
      | diseaseIds   | 2    |
      | fromYear     | 2017 |
      | fromMonth    | 1    |
      | toYear       | 2020 |
      | toMonth      | 6    |
      Then the status code is 200
      And response includes the list of TestingDashboardVolumeDto objects
