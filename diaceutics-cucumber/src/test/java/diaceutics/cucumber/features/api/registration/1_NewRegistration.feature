Feature: Tests for Registration

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0083 POST Register Organisation user
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15 |
      | lastName     | random string, length 15 |
      | email        | @mailosaur.io            |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 15 |
      Then the status code is 200

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0084 POST Register Other user
    When I register new user and save to 'user' using following data:
      | firstName | random string, length 15 |
      | lastName  | random string, length 15 |
      | email     | @mailosaur.io            |
      | userType  | OTHER                    |
      | comment   | random string, length 15 |
      Then the status code is 200

  @@ApiNewRegistration @SingleThread
  Scenario: DIABE:0085 POST Register duplicate user failed
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15 |
      | lastName     | random string, length 15 |
      | email        | @mailosaur.io            |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 15 |
      Then the status code is 200
    When I get saved user values from 'user' and register new user using these values
      Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0086 POST Register with invalid first lastName data
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 45 |
      | lastName     | random string, length 85 |
      | email        | @mailosaur.io            |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 15 |
      Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0087 POST Register with invalid organisation data
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15  |
      | lastName     | random string, length 15  |
      | email        | @mailosaur.io             |
      | userType     | ORGANISATION              |
      | jobTitle     | random string, length 130 |
      | organisation | random string, length 260 |
      Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0088 POST Register with empty firstName
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 0  |
      | lastName     | random string, length 15 |
      | email        | @mailosaur.io            |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 15 |
    Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0089 POST Register with empty lastName
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15 |
      | lastName     | random string, length 0  |
      | email        | @mailosaur.io            |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 15 |
    Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0090 POST Register with invalid email
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15     |
      | lastName     | random string, length 15     |
      | email        | @mailosaur.io, invalid@gmail |
      | userType     | ORGANISATION                 |
      | jobTitle     | random string, length 15     |
      | organisation | random string, length 15     |
      Then the status code is 400
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15        |
      | lastName     | random string, length 15        |
      | email        | @mailosaur.io, invalid@gmailcom |
      | userType     | ORGANISATION                    |
      | jobTitle     | random string, length 15        |
      | organisation | random string, length 15        |
      Then the status code is 400
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15 |
      | lastName     | random string, length 15 |
      | email        | @mailosaur.io, invalid   |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 15 |
      Then the status code is 400
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15   |
      | lastName     | random string, length 15   |
      | email        | @mailosaur.io, invalid.com |
      | userType     | ORGANISATION               |
      | jobTitle     | random string, length 15   |
      | organisation | random string, length 15   |
      Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0091 POST Register with empty userType
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15 |
      | lastName     | random string, length 15 |
      | email        | @mailosaur.io            |
      | userType     | random string, length 0  |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 15 |
      Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0092 POST Register with empty jobTitle
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15 |
      | lastName     | random string, length 15 |
      | email        | @mailosaur.io            |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 0  |
      | organisation | random string, length 15 |
      Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0093 POST Register with empty organisation
    When I register new user and save to 'user' using following data:
      | firstName    | random string, length 15 |
      | lastName     | random string, length 15 |
      | email        | @mailosaur.io            |
      | userType     | ORGANISATION             |
      | jobTitle     | random string, length 15 |
      | organisation | random string, length 0  |
    Then the status code is 400

  @ApiNewRegistration @SingleThread
  Scenario: DIABE:0094 POST Register with empty comment
    When I register new user and save to 'user' using following data:
      | firstName | random string, length 15 |
      | lastName  | random string, length 15 |
      | email     | @mailosaur.io            |
      | userType  | OTHER                    |
      | comment   | random string, length 0  |
      Then the status code is 400