Feature: Test GRUD methods for volumes

  @api @ProfilesApi @VolumesApi @SingleThread
  Scenario: DIABE:0029 GET Returns collection of lab volumes
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volumeOne'
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeTwo.json'
      Then the status code is 200
      And I save from response volume as 'volumeTwo'
    When I execute the GET request Returns collection volumes for Lab 'lab'
      Then the status code is 200
      And response includes for the Lab 'lab' following volumes:
        | volumeOne |
        | volumeTwo |

  @api @ProfilesApi @VolumesApi @SingleThread
  Scenario: DIABE:0030 GET Returns lab volume
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET request Return volume for Lab 'lab' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 200
      And response includes Volume 'volume'

  @api @ProfilesApi @VolumesApi @SingleThread
  Scenario: DIABE:0031 POST Creates a new lab volume
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And response includes the volume from file 'testVolumeOne.json'

  @api @ProfilesApi @VolumesApi @SingleThread
  Scenario: DIABE:0032 PUT Updates an existing volume
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the PUT request Updates an volume 'volume' for Lab 'lab' using data from file 'testVolumeTwo.json'
      Then the status code is 200
      And response includes the volume from file 'testVolumeTwo.json'

  @api @ProfilesApi @VolumesApi @SingleThread
  Scenario: DIABE:0033 DELETE an existing volume
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the request DELETE Volume 'volume' for Lab 'lab'
      Then the status code is 200
    When I execute the GET request Returns collection volumes for Lab 'lab'
      Then the status code is 200
      And response not includes Volume 'volume'

  @NoAutomated
  Scenario: DIABE:0034 POST Returns LabVolumesInternalDto objects
    When I execute POST Returns LabVolumesInternalDto objects using following data:
      | countryCode  | USA  |
      | biomarkerIds | 1    |
      | diseaseIds   | BRC  |
      | fromYear     | 2019 |
      | fromMonth    | 1    |
      | toYear       | 2020 |
      | toMonth      | 6    |
      Then the status code is 200
      And response includes the list of LabVolumesInternalDto objects

  @api @ProfilesApi @VolumesApi @SingleThread
  Scenario: DIABE:0035 POST Creates a new lab volume with Biomarker volume equals to zero
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'volumeWithValueZero.json'
      Then the status code is 200
      And response includes the volume from file 'volumeWithValueZero.json'

  @api @ProfilesApi @VolumesApi @SingleThread
  Scenario: DIABE:0036 PUT Update the Biomarker volume to zero
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the POST request Creates a new lab volume for Lab 'lab' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the PUT request Updates an volume 'volume' for Lab 'lab' using data from file 'volumeWithValueZero.json'
      Then the status code is 200
      And response includes the volume from file 'volumeWithValueZero.json'
