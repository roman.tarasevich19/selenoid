Feature: Test GRUD methods for labs

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0015 GET Returns collection of lab tests
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assayOne'
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayTwo.json'
      Then the status code is 200
      And I save from response lab test as 'assayTwo'
    When I execute the GET request Returns collection of lab tests for Lab 'lab'
      Then the status code is 200
      And response includes the following lab test:
        | assayOne |
        | assayTwo |

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0016 POST Creates a new lab test with required fields
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And response includes the lab test for Lab 'lab' from file 'testAssayOne.json'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0017 POST Creates a new lab test with extra fields
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayWithExtraFields.json'
      Then the status code is 200
      And response includes the lab test for Lab 'lab' from file 'testAssayWithExtraFields.json'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0018 GET Returns a lab test with given lab ID & lab test IDs
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
    And I save from response lab test as 'assay'
    When I execute the GET request Returns a lab test using IDs from 'assay'
      Then the status code is 200
      And response includes the lab test 'assay'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0019 PUT Updates an existing lab test
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testAssayTwo.json'
      Then the status code is 200
      And response includes the lab test for Lab 'lab' from file 'testAssayTwo.json'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0020 DELETE Marks a lab test with given lab ID & lab test IDs as deleted
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the DELETE request Marks a lab test using IDs from 'assay'
      Then the status code is 200
    When I execute the GET request Returns collection of lab tests for Lab 'lab'
      Then the status code is 200
      And response not includes lab test 'assay'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0022 PUT Updates an existing address
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the PUT request Updates an existing address for Lab 'lab' using following data and save as 'address':
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 200
      And response includes the address 'address'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0023 GET Returns Collection of Lab Names and their IDs
    When I execute the GET request Returns Collection of Lab Names and their IDs with parameters:
      Then the status code is 200
      And response includes the labs

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0024 GET Returns collection of labs
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I execute the GET request Returns collection of labs using following data and save criteria as 'criteria':
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 200
      And response includes the labs matches the criteria 'criteria'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0025 POST Creates a new lab
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And response includes the Lab 'lab'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0026 GET Returns lab with given ID
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the GET request Returns lab using ID from 'lab'
      Then the status code is 200
      And response includes the Lab 'lab'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0027 PUT Updates an existing lab
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the PUT request Updates an existing lab 'lab' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 200
    When I execute the GET request Returns lab using ID from 'lab'
      Then the status code is 200
      And response includes the Lab 'lab'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0028 POST Moving assays to another lab
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
    When I save Lab as 'lab' from response
    And I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the POST request Creates a new lab 'another lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'assay' to lab 'another lab'
      Then the status code is 200
    When I execute the GET request Returns collection of lab tests for Lab 'another lab'
      Then the status code is 200
      And response includes the following lab test:
        | assay |