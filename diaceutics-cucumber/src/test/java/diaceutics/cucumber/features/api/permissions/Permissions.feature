Feature: Permissions for APIs

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0044 Check APIs for user with permission: assay-management:labs:create
    Given I execute the POST request Create a lab for user 'am.labs.create' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.labs.create' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.labs.create' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.labs.create' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.labs.create' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.labs.create' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.labs.create' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.labs.create' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.labs.create' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.labs.create'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.labs.create'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.labs.create' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.labs.create'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.labs.create' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.labs.create' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.labs.create'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0045 Check APIs for user with permission: assay-management:labs:update
    Given I execute the POST request Create a lab for user 'am.labs.update' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.labs.update' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 200
    When I execute the GET request Returns collection of labs for user 'am.labs.update' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.labs.update' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.labs.update' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.labs.update' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.labs.update' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.labs.update' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.labs.update' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.labs.update'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.labs.update'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.labs.update' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.labs.update'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.labs.update' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.labs.update' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.labs.update'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0046 Check APIs for user with permission: assay-management:labs:update-own
    Given I execute the POST request Create a lab for user 'am.labs.update.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.labs.update.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.labs.update.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 200
    When I execute the GET request Returns collection of labs for user 'am.labs.update.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
    Then the status code is 400
    When I execute the GET request Returns lab for user 'am.labs.update.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.labs.update.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.labs.update.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.labs.update.own' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.labs.update.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.labs.update.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.labs.update.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.labs.update.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.labs.update.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.labs.update.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.labs.update.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.labs.update.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.labs.update.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0047 Check APIs for user with permission: assay-management:labs:read
    Given I execute the POST request Create a lab for user 'am.labs.read' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.labs.read' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.labs.read' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 200
    When I execute the GET request Returns lab for user 'am.labs.read' using ID from 'lab'
      Then the status code is 200
    When I execute the PUT request Update address for 'lab' using for user 'am.labs.read' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.labs.read' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.labs.read' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.labs.read' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.labs.read' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.labs.read'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.labs.read'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.labs.read' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.labs.read'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.labs.read' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.labs.read' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.labs.read'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0048 Check APIs for user with permission: assay-management:labs:read-own
    Given I execute the POST request Create a lab for user 'am.labs.read.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.labs.read.own' and save as 'lab'
    And I execute the PUT request Update lab 'lab' for user 'am.labs.read.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.labs.read.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.labs.read.own' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update address for 'lab' using for user 'am.labs.read.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.labs.read.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.labs.read.own' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.labs.read.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.labs.read.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.labs.read.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.labs.read.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.labs.read.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.labs.read.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.labs.read.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.labs.read.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.labs.read.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0049 Check APIs for user with permission: assay-management:addresses:update
    Given I execute the POST request Create a lab for user 'am.addresses.update' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.addresses.update' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.addresses.update' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.addresses.update' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.addresses.update' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 200
    When I execute the POST request Create lab test for 'lab' for user 'am.addresses.update' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.addresses.update' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.addresses.update' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.addresses.update' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.addresses.update'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.addresses.update' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.addresses.update'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.addresses.update'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.addresses.update' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.addresses.update' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.addresses.update'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0050 Check APIs for user with permission: assay-management:addresses:update-own
    Given I execute the POST request Create a lab for user 'am.addresses.update.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.addresses.update.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.addresses.update.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.addresses.update.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.addresses.update.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.addresses.update.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 200
    When I execute the POST request Create lab test for 'lab' for user 'am.addresses.update.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.addresses.update.own' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.addresses.update.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.addresses.update.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.addresses.update.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.addresses.update.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.addresses.update.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.addresses.update.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.addresses.update.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.addresses.update.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.addresses.update.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0051 Check APIs for user with permission: assay-management:assays:create
    Given I execute the POST request Create a lab for user 'am.assays.create' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.create' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.create' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.create' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.create' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.create' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.create' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.assays.create' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.assays.create' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.create'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.create'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.create' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.create'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.create' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.create' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.create'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0052 Check APIs for user with permission: assay-management:assays:create-own
    Given I execute the POST request Create a lab for user 'am.assays.create.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.assays.create.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.create.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.create.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.create.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.create.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.create.own' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.create.own' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.assays.create.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.assays.create.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.create.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.create.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.create.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.create.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.create.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.create.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.create.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0053 Check APIs for user with permission: assay-management:assays:update
    Given I execute the POST request Create a lab for user 'am.assays.update' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.update' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.update' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.update' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.update' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.update' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.update' using data from file 'testAssayTwo.json'
      Then the status code is 200
    When I execute the GET request Returns collection of lab tests for user 'am.assays.update' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.assays.update' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.update'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.update'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.update' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.update'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.update' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.update' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.update'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0054 Check APIs for user with permission: assay-management:assays:update-own
    Given I execute the POST request Create a lab for user 'am.assays.update.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.assays.update.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.update.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.update.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.update.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.update.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.update.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.update.own' using data from file 'testAssayTwo.json'
      Then the status code is 200
    When I execute the GET request Returns collection of lab tests for user 'am.assays.update.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.assays.update.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.update.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.update.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.update.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.update.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.update.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.update.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.update.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0055 Check APIs for user with permission: assay-management:assays:read
    Given I execute the POST request Create a lab for user 'am.assays.read' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.read' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.read' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.read' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.read' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.read' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.read' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.assays.read' for 'lab'
      Then the status code is 200
    When I execute the GET request Returns a lab test for user 'am.assays.read' using IDs from 'lab test'
      Then the status code is 200
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.read'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.read'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.read' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.read'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.read' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.read' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.read'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0056 Check APIs for user with permission: assay-management:assays:read-own
    Given I execute the POST request Create a lab for user 'am.assays.update.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.assays.read.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.read.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.read.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.read.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.read.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.read.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.read.own' using data from file 'testAssayTwo.json'
    Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.assays.read.own' for 'lab'
      Then the status code is 200
    When I execute the GET request Returns a lab test for user 'am.assays.read.own' using IDs from 'lab test'
      Then the status code is 200
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.read.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.read.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.read.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.read.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.read.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.read.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.read.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0057 Check APIs for user with permission: assay-management:assays:delete
    Given I execute the POST request Create a lab for user 'am.assays.delete' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.delete' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.delete' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.delete' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.delete' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.delete' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.delete' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.assays.delete' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.assays.delete' using IDs from 'lab test'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.delete'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.delete'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.delete' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.delete'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.delete' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.delete' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.delete'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0058 Check APIs for user with permission: assay-management:volumes:read
    Given I execute the POST request Create a lab for user 'am.volume.read' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.volume.read' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.volume.read' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.volume.read' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.volume.read' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.volume.read' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.volume.read' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.volume.read' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.volume.read' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.volume.read'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.volume.read'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.volume.read' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.volume.read'
      Then the status code is 200
    When I execute the GET request Return volume for Lab 'lab' for user 'am.volume.read' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 200
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.volume.read' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.volume.read'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0059 Check APIs for user with permission: assay-management:volumes:read-own
    Given I execute the POST request Create a lab for user 'am.volume.read.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.volume.read.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.volume.read.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.volume.read.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.volume.read.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.volume.read.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.volume.read.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
    And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.volume.read.own' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.volume.read.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.volume.read.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.volume.read.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.volume.read.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.volume.read.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.volume.read.own'
      Then the status code is 200
    When I execute the GET request Return volume for Lab 'lab' for user 'am.volume.read.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 200
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.volume.read.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.volume.read.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0060 Check APIs for user with permission: assay-management:volumes:create
    Given I execute the POST request Create a lab for user 'am.volume.create' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.volume.create' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.volume.create' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.volume.create' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.volume.create' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.volume.create' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.volume.create' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.volume.create' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.volume.create' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.volume.create'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.volume.create'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.volume.create' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.volume.create'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.volume.create' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.volume.create' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.volume.create'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0061 Check APIs for user with permission: assay-management:volumes:create-own
    Given I execute the POST request Create a lab for user 'am.volume.create.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.volume.create.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.volume.create.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.volume.create.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.volume.create.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.volume.create.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.volume.create.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.volume.create.own' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.volume.create.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.volume.create.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.volume.create.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.volume.create.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.volume.create.own' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.volume.create.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.volume.create.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.volume.create.own' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.volume.create.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0062 Check APIs for user with permission: assay-management:volumes:update
    Given I execute the POST request Create a lab for user 'am.volume.update' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.volume.update' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.volume.update' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.volume.update' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.volume.update' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.volume.update' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.volume.update' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.volume.update' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.volume.update' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.volume.update'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.volume.update'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.volume.update' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.volume.update'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.volume.update' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.volume.update' using data from file 'testVolumeTwo.json'
      Then the status code is 200
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.volume.update'
      Then the status code is 400

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0063 Check APIs for user with permission: assay-management:volumes:update-own
    Given I execute the POST request Create a lab for user 'am.volume.update.own' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I get own lab Id for user 'am.volume.update.own' and save as 'lab'
    And I execute the GET request Returns lab for user 'ADMIN' using ID from 'lab'
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.volume.update.own' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.volume.update.own' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.volume.update.own' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.volume.update.own' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.volume.update.own' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.volume.update.own' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.volume.update.own' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.volume.update.own' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.volume.update.own'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.volume.update.own'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'ADMIN'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.volume.update.own' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.volume.update.own'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.volume.update.own' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.volume.update.own' using data from file 'testVolumeTwo.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.volume.update.own'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'ADMIN'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0064 Check APIs for user with permission: assay-management:volumes:delete
    Given I execute the POST request Create a lab for user 'am.volume.delete' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.volume.delete' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.volume.delete' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.volume.delete' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.volume.delete' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.volume.delete' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.volume.delete' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.volume.delete' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.volume.delete' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.volume.delete'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.volume.delete'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'am.volume.delete' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.volume.delete'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.volume.delete' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.volume.delete' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.volume.delete'
      Then the status code is 200

  @api @ApiPermissions @SingleThread
  Scenario: DIABE:0066 Check APIs for user with permission: assay-management:assays:move
    Given I execute the POST request Create a lab for user 'am.assays.move' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response
    When I execute the PUT request Update lab 'lab' for user 'am.assays.move' using following data:
      | name        | Updated Test Api Lab |
      | countryCode | ALB                  |
      | type        | HOSPITAL             |
      Then the status code is 400
    When I execute the GET request Returns collection of labs for user 'am.assays.move' using following data:
      | type        | ACADEMIC     |
      | countryCode | IRL          |
      | keyword     | Test Api Lab |
      Then the status code is 400
    When I execute the GET request Returns lab for user 'am.assays.move' using ID from 'lab'
      Then the status code is 400
    When I execute the PUT request Update address for 'lab' using for user 'am.assays.move' following data:
      | addressLine1 | updated Test Street |
      | addressLine2 | updated Test Street |
      | city         | updated Test city   |
      | region       | du                  |
      | countryCode  | IRL                 |
      | postalCode   | updated Test code   |
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'am.assays.move' using data from file 'testAssayOne.json'
      Then the status code is 400
    When I execute the POST request Create lab test for 'lab' for user 'ADMIN' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'lab test'
    When I execute the PUT request Update lab test 'lab test' for user 'am.assays.move' using data from file 'testAssayTwo.json'
      Then the status code is 400
    When I execute the GET request Returns collection of lab tests for user 'am.assays.move' for 'lab'
      Then the status code is 400
    When I execute the GET request Returns a lab test for user 'am.assays.move' using IDs from 'lab test'
      Then the status code is 400
    When I execute the request DELETE lab test 'lab test' for user 'am.assays.move'
      Then the status code is 400
    When I execute the POST request Create a lab for user 'ADMIN' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'another lab' from response
    When I execute the POST request moving assays 'lab test' to lab 'another lab' for user 'am.assays.move'
      Then the status code is 200
    When I execute the POST request Creates volume for 'lab' for user 'am.assays.move' using data from file 'testVolumeOne.json'
      Then the status code is 400
    When I execute the POST request Creates volume for 'lab' for user 'ADMIN' using data from file 'testVolumeOne.json'
      Then the status code is 200
      And I save from response volume as 'volume'
    When I execute the GET Returns collection volumes for Lab 'lab' for user 'am.assays.move'
      Then the status code is 400
    When I execute the GET request Return volume for Lab 'lab' for user 'am.assays.move' by parameters:
      | diseaseId       | 10   |
      | year            | 2017 |
      | timePeriodValue | 3    |
      Then the status code is 400
    When I execute the PUT request Updates volume 'volume' for 'lab' for user 'am.assays.move' using data from file 'testVolumeTwo.json'
      Then the status code is 400
    When I execute the request DELETE volume 'volume' for 'lab' for user 'am.assays.move'
      Then the status code is 400
