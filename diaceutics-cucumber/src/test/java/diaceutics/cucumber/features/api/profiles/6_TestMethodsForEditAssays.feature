Feature: Tests methods for Edit Assays
  
  Background:
    Given I execute the POST request Creates a new lab 'lab' using following data:
      | name        | Test Api Lab |
      | countryCode | IRL          |
      | type        | ACADEMIC     |
      Then the status code is 200
      And I save Lab as 'lab' from response

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0067 POST Creates a new lab test with In Active Status
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'assayInActiveStatus.json'
      Then the status code is 200
      And response includes the lab test for Lab 'lab' from file 'assayInActiveStatus.json'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0068 PUT Updates lab test status from Active to In Active
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testAssayOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'assayInActiveStatus.json'
      Then the status code is 200
      And response includes the lab test for Lab 'lab' from file 'assayInActiveStatus.json'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0069 PUT Updates lab test status from In Active to Active
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'assayInActiveStatus.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testAssayOne.json'
      Then the status code is 200
      And response includes the lab test for Lab 'lab' from file 'testAssayOne.json'

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0070 POST Not specified Assay without TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testNotSpecifiedAssayWithoutTATOne.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0071 PUT Not specified Assay without TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testNotSpecifiedAssayWithoutTATOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testNotSpecifiedAssayWithoutTATTwo.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0072 PUT Not specified Assay with TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testNotSpecifiedAssayWithoutTATOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testNotSpecifiedAssayWithTAT.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0073 PUT Not specified Assay delete TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testNotSpecifiedAssayWithTAT.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testNotSpecifiedAssayWithoutTATOne.json'
      Then the status code is 400

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0074 POST Not specified Assay without TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testInHouseAssayWithoutTATOne.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0075 PUT Not specified Assay without TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testInHouseAssayWithoutTATOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testInHouseAssayWithoutTATTwo.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0076 PUT Not specified Assay with TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testInHouseAssayWithoutTATOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testInHouseAssayWithTAT.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0077 PUT Not specified Assay delete TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testInHouseAssayWithTAT.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testInHouseAssayWithoutTATOne.json'
      Then the status code is 400

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0078 POST Not specified Assay with TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testNotSpecifiedAssayWithTAT.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0079 POST In house Assay with TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testInHouseAssayWithTAT.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0080 PUT Not specified Assay without TAT to In house
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testNotSpecifiedAssayWithoutTATOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testInHouseAssayWithoutTATOne.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0081 PUT In house Assay without TAT to Not specified
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testInHouseAssayWithoutTATOne.json'
      Then the status code is 200
      And I save from response lab test as 'assay'
    When I execute the PUT request Updates an lab test 'assay' using data from file 'testNotSpecifiedAssayWithoutTATOne.json'
      Then the status code is 200

  @api @ProfilesApi @labsApi @SingleThread
  Scenario: DIABE:0082 POST Not specified assay with LDT without TAT
    When I execute the POST request Creates a new lab test for Lab 'lab' using data from file 'testNotSpecifiedAssayWithoutTATWithLDT.json'
      Then the status code is 200